<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
//error_reporting(E_ALL);
//ini_set('display_errors',1);
include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");
class CommonAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     *  Change Status
     */
    public function changeStatus(){
        try{

//            $status = $_REQUEST['status'];
//            $sql = "UPDATE active_inactive_status SET status=?";
//            $stmt= $this->companyConnection->prepare($sql);
//            $stmt->execute([$status]);
            return ['status'=>'success','code'=>200];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Deactivate Company
     */
    public function deactivate(){
        try{
            $company_property_type = $_REQUEST['id'];
            $sql = "UPDATE company_property_type SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([0,$company_property_type]);
            return ['status'=>'success','code'=>200,'data'=>'Company deactivated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Delete Company
     */
    public function delete(){
        try{
            $company_property_type = $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_property_type SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$company_property_type]);
            return ['status'=>'success','code'=>200,'data'=>'Company deleted successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    /**
     *  Delete Company
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                                die();
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $extra_columns = ",`user_id`, `is_default`, `status`, `is_editable`, `created_at`, `updated_at`";
                            $is_default = 0;
                            $status = 1;
                            $is_editable = 1 ;
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_values = ",'" . $login_user_id . "','" . $is_default . "','" . $status . "','". $is_editable . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $query = "insert into `company_property_type` (`property_type`, `description`$extra_columns) VALUES ";
                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                $query .= "(";
                                //Print each cell of the current row
                                foreach ($single_row[0] as $key => $value) {
                                    $query .= "'" . $value . "',";
                                }
                                $query = substr($query, 0, -1);
                                $query .= $extra_values . "),";
                            }
                            $query = substr($query, 0, -1);
                            try {
                                $stmt = $this->companyConnection->prepare($query);
                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data'];
                                printErrorLog($e->getMessage());
                                die();
                            }
                            $executed_query = $stmt->execute();
                            if ($executed_query) {
                                unlink($file);
                                return ['status'=>'success','code'=>200,'message'=>'Data Imported Successfully'];
                            }else{
                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file'];
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'* Only .xls/.xlsx files are accepted'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }

    /**
     *  function to get post data array
     */
    public function postArray($post) {
        $data = [];
        foreach ($post as $key => $value) {
            if (!empty($value['value'])) {
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$value['name']] = $dataValue;
        }
        return $data;
    }


    /**
     *  Server side validation function
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key => $value) {
            $columns .= $key . ',';
            $columnsValues .= ':' . "$key" . ',';
        }
        $columns = substr_replace($columns, "", -1);
        $columnsValues = substr_replace($columnsValues, "", -1);
        $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
        return $sqlData;
    }

    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'c_id'){
                //do nothing
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
            }
            //$columnsValuesPair .= $key = ".$value.".',';
        }
        $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }



    /**
     *  Export Excel
     */
    public function exportExcel(){

        include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];
        $query1 = "SELECT * FROM $table";
        $stmt = $this->companyConnection->prepare($query1);
        $stmt->execute();
        //Set header with temp array
        $tmparray =array("PropertyType","Description");
        //take new main array and set header array in it.
        $sheet =array($tmparray);
        while ($property_type_data = $stmt->fetch(PDO::FETCH_ASSOC))
        {

            $tmparray =array();
            $property_type = $property_type_data['property_type'];
            array_push($tmparray,$property_type);
            $description = $property_type_data['description'];
            array_push($tmparray,$description);
            array_push($sheet,$tmparray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="PropertyType.xlsx"');
        $objWriter->save('php://output');
    }


    /**
     *  Export Excel
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/PropertyType.xlsx";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
    }


    /**
     *  Change Status
     */
    public function showAnnouncement(){
        try{
            if(isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
                $query2 = "SELECT * FROM users Where id = '1'";
                $user_data = $this->companyConnection->query($query2)->fetch();
                $user_id = $user_data['admindb_id']; //$_SESSION[SESSION_DOMAIN]['cuser_id'];
                $announcement_ids = $_REQUEST["skip_announcement_ids"];
                $data = json_decode($announcement_ids, TRUE);
                $wherenotin = '';
                if (!empty($data)) {
                    $skip_announcement_ids = implode(",", $data);
                    $wherenotin = "AND id NOT IN ($skip_announcement_ids)";
                }
                $timeZone = convertTimeZoneDefaultSeting('', date('Y-m-d H:i:s'), $this->companyConnection);
                $s = strtotime($timeZone);
                $date = date('Y-m-d', $s);
                $time = date('H:i:s', $s);
                $query = "SELECT * FROM announcements  WHERE DATE(start_date) ='" . $date . "' AND DATE(end_date)='" . $date . "' AND TIME(start_time) <= '" . $time . "' AND TIME(end_time) >= '" . $time . "' AND status = '1' $wherenotin";
                $liveAnnouncementData = $this->conn->query($query)->fetchAll();
                //$user_id = !empty($_SESSION[SESSION_DOMAIN]['cuser_id']) ? $_SESSION[SESSION_DOMAIN]['cuser_id'] : null;
                if ($user_id) {
                    if (count($liveAnnouncementData) != 0) {
                        foreach ($liveAnnouncementData as $key => $value) {
                            $value = $value["id"];
                            $query1 = "SELECT * FROM announcements_user_status  WHERE user_id='" . $user_id . "' AND announcement_id = '" . $value . "'";
                            $user = $this->conn->query($query1)->fetch();
                            if ($user != null) {
                                $liveAnnouncementData[$key]['seenStatus'] = 'seen';
                            } else {
                                $liveAnnouncementData[$key]['seenStatus'] = 'unseen';
                            }
                        }
                        return $liveAnnouncementData;
                    }
                }
            }
            return;
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    /**
     *  Change Status
     */
    public function updateUserAnnouncement(){
        try{
            $announcement_id = $_REQUEST['announcement_id'];

            $query1 = "SELECT * FROM users Where id = '1'";
            $user_data = $this->companyConnection->query($query1)->fetch();
            $data['user_id'] = $user_data['admindb_id']; //$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['announcement_id'] = $announcement_id;
            $data['status'] = 1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['created_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO announcements_user_status (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->conn->prepare($query);
            $stmt->execute($data);
            return ['status'=>'success','code'=>200,'data'=>$data];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * function to get address from zip code
     * @return array
     */
    public function getZipcode(){
        try{
            return getSingleRecord($this->companyConnection ,['column'=>'zip_code','value'=>$_POST['zip_code']], 'zip_code_master');
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getBuildings(){
        try{
            $property_id = $_REQUEST['id'];
            $query1 = "SELECT * FROM building_detail Where property_id = $property_id";
            $buildingData = $this->companyConnection->query($query1)->fetchAll();
            if(!empty($buildingData)){
                return ['status'=>'success','code'=>200,'data'=>$buildingData];
            } else {
                return ['status'=>'success','code'=>400,'message'=>'No Record Found!'];
            }

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * function to get card detail data
     * @return array
     */
    public function getCustomerDetails(){
        $id = $_POST['id'];
        $user_data = getCustomerDetails($this->companyConnection,$id);
        if($user_data['code'] == 200){
            return ['status'=>SUCCESS,'code'=>SUCCESS_CODE,'data'=>$user_data];
        }
    }

    /**
     * Check subscription
     * @return array
     */
    public function checkSubscription(){
        try {
            $userData = getSingleRecord($this->companyConnection,['id'=>1], 'users');
            if($userData['code'] == 200){
                if(($userData['free_plan'] == '0' || empty($userData['free_plan'])) && $userData['payment_status'] == '0' ){
                    return array('code' => SUCCESS_CODE, 'status' => SUCCESS,'message' => SUBSCRIPTION_EXPIRED);
                } else if($userData['free_plan'] == '1') {
                    $date = strtotime(date('Y-m-d'));
                    if(!empty($userData['expiration_date'])) {
                        $expirationDate = strtotime($userData['expiration_date']);
                        if ($date >= $expirationDate) {
                            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUBSCRIPTION_EXPIRED);
                        }
                    }
                    if(!empty($userData['remaining_days'])) {
                        $created_at = $userData['created_at'];
                        $expirationDate = date('Y-m-d', strtotime($created_at. ' + '.$userData['remaining_days'].' days'));
                        if ($date >= strtotime($expirationDate)) {
                            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUBSCRIPTION_EXPIRED);
                        }
                    }
                }
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS,'message' => SUBSCRIPTION_VALID);
            }
            return array('code' => ERROR_CODE, 'status' => ERROR,'message' => ERROR_NO_RECORD_FOUND);
        } catch(Exception $exception){
            return array('code' => ERROR_CODE, 'status' => ERROR,'message' => $exception->getMessage());
        }
    }
    public function systemLogout(){
        $query = $this->companyConnection->query("SELECT timeout FROM default_settings");
        $data1 = $query->fetch();
        return $data1['timeout'];
    }

    public function getuserinfo(){
        $tabname=$_POST['table_name'];
        $colname=$_POST['column_name'];
        $id = $_POST['id'];
        $query = $this->companyConnection->query("SELECT ".$colname." FROM ".$tabname." where id=".$id);
        $data = $query->fetch();
        $key=[];
        $val=[];
        foreach ($data as $k=>$v){
            array_push($key,$k);
            array_push($val,$v);
        }
        return ['status'=>'success','code'=>200,'val'=>$val,'key'=>$key,'data'=>$data];
    }

    public function updateuserflag(){
        //die("here");
        $id=$_POST['id'];
        $table=$_POST['table'];
        $updateData = [];
        if($table=='general_property'){
            $updateData['update_at'] = date('Y-m-d H:i:s');
        }
        else{
            $updateData['updated_at'] = date('Y-m-d H:i:s');
        }
        
        $sqlData = createSqlColValPair($updateData);
        //dd($sqlData);
        $query = "UPDATE ".$table." SET ".$sqlData['columnsValuesPair']." where id=$id";
        //dd($query);
        $stmt =$this->companyConnection->prepare($query);
        $stmt->execute();
        return ['status'=>'success','code'=>200,];
    }
    
}



$common = new CommonAjax();

