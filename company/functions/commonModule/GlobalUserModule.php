<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
error_reporting(E_ALL);
ini_set('display_errors',1);
class GlobalUserModule extends DBConnection {

    /**
     * GlobalUserModule constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to create vendor with popup window
     * @return array
     */
    public function add_vendor(){
        try{
            $data = postArray($_POST['data'], 'true');
            //Required variable array
            $required_array = [];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = '1';
                $data['user_type'] = '3';
                $data['salutation'] = !empty($data['salutation'])?$data['salutation']:NULL;
                $data['gender'] = !empty($data['gender'])?$data['gender']:NULL;
                $data['created_at'] = date('Y-m-d');
                $data['updated_at'] = date('Y-m-d');
                $vendor_additional_details = [];
                $vendor_additional_details['vendor_random_id'] = $data['vendor_random_id'];
                $vendor_additional_details['vendor_type_id'] = ($data['vendor_type_id'] != 'default')?$data['vendor_type_id']:NULL;
                $vendor_additional_details['vendor_rate'] = $data['vendor_rate'];
                $vendor_additional_details['status'] = $data['status'];
                $data['referral_source'] = !empty($data['additional_referralSource'])?$data['additional_referralSource']:NULL;
                $vendor_additional_details['created_at'] = $data['created_at'];
                $vendor_additional_details['updated_at'] = $data['updated_at'];
                unset($data['vendor_random_id'],$data['vendor_type_id'],$data['vendor_rate'],$data['vendor_type'],$data['description'],$data['additional_referralSource']);
                //insert data into user table
                $userData = $data;
                $sqlData = createSqlColVal($userData);
                $query = "INSERT INTO  users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($userData);
                $id = $this->companyConnection->lastInsertId();
                updateUsername($id,$this->companyConnection);
                //insert data into vendor additional detail table
                $vendor_additional_details['vendor_id'] = $id;
                $sqlDataVendor = createSqlColVal($vendor_additional_details);
                $queryVendor = "INSERT INTO  vendor_additional_detail (" . $sqlDataVendor['columns'] . ") VALUES (" . $sqlDataVendor['columnsValues'] . ")";
                $stmtVendor = $this->companyConnection->prepare($queryVendor);
                $stmtVendor->execute($vendor_additional_details);
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_ADDED);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * function to get user on the bases of id
     * @return array
     */
    public function getUser(){
        try{
            $id = $_POST['id'];
            $sql = 'SELECT * FROM users where id='.$id;
            $data = $this->companyConnection->query($sql)->fetch();
            $address = '';
            $addressFormated = $data['name']."\n\n";
            $cityAddress = '';
            $stateAddress = '';
            $zipcode = '';
            $country = '';
            foreach ($data as $key1=>$value1){
                if($key1 == 'address1' || $key1 == 'address2' || $key1 == 'address3'){
                    if(!empty($value1)) {
                        $address .= $value1 . ',';
                        $addressFormated .= $value1.",\n";
                    }
                }
                if(!empty($value1) && $key1 == 'city') {
                    $cityAddress = $value1.',';
                }
                if(!empty($value1) && $key1 == 'state') {
                    $stateAddress = $value1;
                }
                if(!empty($value1) && $key1 == 'zipcode') {
                    $zipcode = $value1;
                }
                if(!empty($value1) && $key1 == 'country') {
                    $country = $value1;
                }
            }
            $addressFormated = $addressFormated.$cityAddress.$stateAddress.' '.$zipcode."\n".$country;
            $addressFormated = str_replace_last(',\n','',$addressFormated);
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_ADDED, 'data' => $data,'addressFormated'=>$addressFormated);
        } catch(Exception $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * function to get user on the bases of id
     * @return array
     */
    public function updateUserAddress(){
        try{
            $data = postArray($_POST['data']);
            $id = $data['id'];
            unset($data['id']);
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            $userData = getSingleRecord($this->companyConnection, ['column'=>'id','value'=>$id], 'users');
            $address = '';
            $addressFormated = $userData['data']['name']."\n\n";
            $cityAddress = '';
            $stateAddress = '';
            $zipcode = '';
            $country = '';
            foreach ($userData['data'] as $key1=>$value1){
                if($key1 == 'address1' || $key1 == 'address2' || $key1 == 'address3'){
                    if(!empty($value1)) {
                        $address .= $value1 . ',';
                        $addressFormated .= $value1.",\n";
                    }
                }
                if(!empty($value1) && $key1 == 'city') {
                    $cityAddress = $value1.',';
                }
                if(!empty($value1) && $key1 == 'state') {
                    $stateAddress = $value1;
                }
                if(!empty($value1) && $key1 == 'zipcode') {
                    $zipcode = $value1;
                }
                if(!empty($value1) && $key1 == 'country') {
                    $country = $value1;
                }
            }
            $addressFormated = $addressFormated.$cityAddress.$stateAddress.' '.$zipcode."\n".$country;
            $addressFormated = str_replace_last(',\n','',$addressFormated);
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_UPDATED, 'address' =>$addressFormated );
        } catch(Exception $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * function to save dynamic status for company users
     * @return array
     */
    public function saveModuleStatus(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $module = $_POST['data']['module'];
            $value = $_POST['data']['value'];
            if(!empty($module)) {
                $selectQuery = "SELECT id FROM active_inactive_status WHERE module='".$module."' AND user_id=".$user_id;
                $previousData = $this->companyConnection->query($selectQuery)->fetch();
                if (!empty($previousData)) {
                    $updateData = [];
                    $updateData['status'] = $value;
                    $updateData['updated_at'] = date('Y-m-d');
                    $sqlData = createSqlUpdateCase($updateData);
                    $query = "UPDATE active_inactive_status SET " . $sqlData['columnsValuesPair'] . " WHERE id=".$previousData['id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($sqlData['data']);
                    $data = $exe;
                } else {
                    $data = 'insert';
                    if (!empty($module)) {
                        $data = [];
                        $data['user_id'] = $user_id;
                        $data['module'] = $module;
                        $data['status'] = $value;
                        $data['created_at'] = date('Y-m-d');
                        $data['updated_at'] = date('Y-m-d');
                        $sqlDataVendor = createSqlColVal($data);
                        $queryVendor = "INSERT INTO  active_inactive_status (" . $sqlDataVendor['columns'] . ") VALUES (" . $sqlDataVendor['columnsValues'] . ")";
                        $stmtVendor = $this->companyConnection->prepare($queryVendor);
                        $data = $stmtVendor->execute($data);
                    }

                }
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS);
        } catch (Exception $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * function get status value of each module
     * @return array
     */
    public function getModuleStatus(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $module = $_POST['data']['module'];
            $query = 'SELECT status FROM active_inactive_status WHERE user_id='.$user_id.' AND module= "'.$module.'"';
            $data = $this->companyConnection->query($query)->fetch();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data_status' => $data['status']);
        } catch(Exception $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

}

$propertyDetail = new GlobalUserModule();
