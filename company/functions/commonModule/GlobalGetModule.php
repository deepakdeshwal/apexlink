<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class GlobalGetModule extends DBConnection {

    /**
     * GlobalGetModule constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * @return array
     */
    public function getAllProperties(){
        try {
            $portfolio_id = $_POST['portfolio_id'];
            $query = 'SELECT * FROM general_property WHERE portfolio_id="'.$portfolio_id.'" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

    public function getAllBuildings(){
        try {
            $property_id = $_POST['property_id'];
            $query = 'SELECT * FROM building_detail WHERE property_id="'.$property_id.'" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

    public function getAllUnits(){
        try {
            $building_id = $_POST['building_id'];
            $query = 'SELECT * FROM unit_details WHERE building_id="'.$building_id.'" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

    public function getAllWorkOrder(){
        try {
            $query = 'SELECT * FROM work_order WHERE deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

    public function getAllChartsOfAccounts(){
        try {
            $query = 'SELECT * FROM company_chart_of_accounts WHERE status="1" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

    public function getAllWorkOrderStatus(){
        try {
            $query = 'SELECT * FROM company_workorder_status WHERE status="1" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' => SUCCESS_FETCHED, 'data' => $data);
        } catch (PDOException $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' => $exception->getMessage());
        }
    }

}

$propertyDetail = new GlobalGetModule();
