<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
//include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class reports extends DBConnection
{

   /* public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        print_r($_REQUEST); die();
        echo json_encode($this->$action());

    }*/

    function __construct () {
        parent::__construct ();
        //$action = $_REQUEST['action'];
    }

    public static function getTenantPopUpDataArray(){
        $popUpArray=[];
        $popUpArray1=[];
        $popUpArray2=[];
        $popUpArray3=[];
        $popUpArray4=[];
        $popUpArray5=[];
        $popUpArray6=[];
        $popUpArray7=[];
        $popUpArray8=[];
        $popUpArray9=[];
        $popUpArray10=[];
        $popUpArray11=[];
        $popUpArray12=[];
        $popUpArray13=[];
        $popUpArray14=[];
        $popUpArray15=[];
        $popUpArray16=[];
        $popUpArray17=[];
        $popUpArray18=[];
        $popUpArray19=[];
       /* $mainHeading = [];*/

        /*------------------------------------------- Rent Roll Filters start --------------------------------------------------*/
        $title = "Rent Roll Filters";
        $label = [['name' => 'Date','is_required' => 'yes'],['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],['name' => 'Status','is_required' => 'no'],
            ['name' => 'Tenant','is_required' => 'no']];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','id'=>'status','changed_column'=>'tenant','name' => 'status','data_type_reports'=>'selectData'],
           /* ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'tenant_id','name' => 'tenant[]','data_type_reports'=>'multiselect']*/
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ] ];
        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'users',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '0'
        ]
        ];

        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[ [
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/RentRollReport';
//        $report_columns = array('Unit','Status','Lease Start Date','Lease End Date','Move Out Date','Tenant Charges ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Last Payment ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Balance Due ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
//        $report_db_query = "SELECT CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name,td.status,tld.start_date,tld.end_date,tld.move_out,tld.cam_amount AS amt,tld.rent_amount AS amt1,tld.balance AS amt2 FROM users u JOIN tenant_lease_details tld ON u.id=tld.user_id JOIN tenant_details td ON u.id=td.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN general_property gp ON tp.property_id=gp.id JOIN company_property_portfolio cpp ON gp.portfolio_id=cpp.id WHERE tld.record_status='1'";
        $report_columns = array('Tenant','Unit','Status','Sq. Ft.','Market Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Current Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Deposit ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Lease From','Lease To','Move In','Move Out','Past Due ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')', 'NSF (Count', 'Late Count');
        $report_db_query = "SELECT  REPLACE(u.name,' ','--') AS name,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name, ud.building_unit_status AS status, ud.square_ft, ud.market_rent as amt, tld.rent_amount as amt1, tld.security_deposite as amt2, tld.start_date, tld.end_date, tld.move_in, tld.move_out,tld.balance as amt3,(SELECT COUNT(tc.id) from tenant_charges tc WHERE tc.user_id = u.id AND tc.charge_code = 12) AS nsf_count, (SELECT COUNT(tc.id) from tenant_charges tc WHERE tc.user_id = u.id AND tc.charge_code = 6) AS late_count FROM users u JOIN tenant_lease_details tld ON u.id=tld.user_id JOIN tenant_details td ON u.id=td.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN general_property gp ON tp.property_id=gp.id JOIN company_property_portfolio cpp ON gp.portfolio_id=cpp.id";
        $report_join_table = "tp";
        $report_date_filter_table = "u";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main' => 'users','joins'=>$other_joins ,'where'=>$where,'onChange'=>$onChange],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $db_query_after_where = '';
        /*Data array*/
        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_join_table'] = $report_join_table;
        $popUpArray['db_query_after_where'] = $db_query_after_where;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*------------------------------------------- Rent Roll Filters End --------------------------------------------------------*/

        /*------------------------------------------- Rent Roll Consolidated Filters --------------------------------------------------*/
        $title1 = "Rent Roll Consolidated Filters";
        $label1 = [
            ['name' => 'Date','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes']
        ];
        $field1=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
        ] ;

        $data1 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];

        $report_url1 = '/Reporting/RentRollConsolidatedReport';
        $report_columns1 = array('Tenant','Property Name','Unit','Status','Sq. Ft.','Market Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Current Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Deposit ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Lease From','Lease To','Move In','Move Out','Past Due ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query1 = "SELECT REPLACE(u.name,' ','--') AS name, REPLACE(gp.property_name,' ','--') AS prop_name, CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name, td.status, ud.square_ft, ud.market_rent as amt, tld.rent_amount as amt1, tld.security_deposite as amt2, tld.start_date, tld.end_date, tld.move_in, tld.move_out,tld.balance as amt3 FROM users u JOIN tenant_lease_details tld ON u.id=tld.user_id JOIN tenant_details td ON u.id=td.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN general_property gp ON tp.property_id=gp.id JOIN company_property_portfolio cpp ON gp.portfolio_id=cpp.id WHERE u.deleted_at IS NULL ";
        $db_query_after_where1 = '';
        $report_date_filter_table1 = 'u';

        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['alternate_report_columns'] = '';
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = '';
        $popUpArray1['db_query_after_where'] = $db_query_after_where1;
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*------------------------------------------- Rent Roll Consolidated Filters --------------------------------------------------*/


        /*------------------------------------------- Tenant List Filters start--------------------------------------------------*/
        $title2 = "Tenant List Filters";
        $label2 = [['name' => 'Start Date','is_required' => 'no'],['name' => 'End Date','is_required' => 'no'],['name' => 'Portfolio','is_required' => 'yes'],['name' => 'Property','is_required' => 'no'],['name' => 'Status','is_required' => 'no'],['name' => 'Move Out','is_required' => 'no']];
        $field2=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'start_date','id' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'end_date','id' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'checkbox','class'=>'textfeild','name' => 'move_out','id' => 'move_out','data_type_reports'=>'checkbox']
        ];

        $joins2 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ] ];

        $onChange2 = ['table'=>'company_property_portfolio','joins'=>$joins2];

        $data2 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange2],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange2],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange2],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>$onChange2],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>'','column'=>[''], 'name'=>'move_out', 'id'=>'move_out', 'type'=>'checkbox']
        ];

        $report_url2 = '/Reporting/TenantListReport';
        $report_columns2 = array('Tenant Name','Phone Number','Email','Address','Rent Amount('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Move-In Date');
        $report_db_query2 = "SELECT REPLACE (u.name,' ','--') AS name, u.phone_number,u.email,CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--'),',', REPLACE(ud.unit_prefix,' ','--'),'-',REPLACE(ud.unit_no,' ','--'),',--',REPLACE(gp.city,' ','--'),',--',REPLACE(gp.state,' ','--'),'--',REPLACE(gp.zipcode,' ','--')) AS address ,tld.rent_amount as amt,tld.move_in FROM users u JOIN tenant_lease_details tld ON tld.user_id=u.id JOIN tenant_details td ON td.user_id=u.id JOIN tenant_property tp ON u.id=tp.user_id JOIN general_property gp ON gp.id=tp.property_id JOIN unit_details ud ON ud.id=tp.unit_id";
        $db_query_after_where2 = '';
        $report_date_filter_table2 = 'u';

        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        $popUpArray2['report_columns'] = $report_columns2;
        $popUpArray2['alternate_report_columns'] = '';
        $popUpArray2['report_url'] = $report_url2;
        $popUpArray2['report_db_query'] = $report_db_query2;
        $popUpArray2['report_join_table'] = '';
        $popUpArray2['db_query_after_where'] = $db_query_after_where2;
        $popUpArray2['report_date_filter_table'] = $report_date_filter_table2;
        /*------------------------------------------- Tenant List Filters end--------------------------------------------------*/

        /*--------------------------------------------  Tenant List Consolidated Filters -----------------------------------------------*/
        $title3 = "Tenant List Consolidated Filters";
        $label3 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Move out','is_required' => 'no']];

        $field3=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'start_date','id' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'end_date','id' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'checkbox','class'=>'textfeild','name' => 'move_out','id' => 'move_out','data_type_reports'=>'checkbox']

        ] ;

        /*Data array*/
        $data3 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>null],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>'','column'=>[''], 'name'=>'move_out', 'id'=>'move_out', 'type'=>'checkbox']
        ];
        /*Data array*/
        $report_url3='/Reporting/TenantListConsolidatedReport';
        $report_columns3 = array('Tenant Name','Phone Number','Email','Tenant Address','Unit#','Move-In Date','Move Out Date','Rent Amount('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Unit Area','Deposit('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Balance('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Status','LeaseDate');
        $report_db_query3 = "SELECT REPLACE (u.name,' ','--') AS name,u.phone_number,u.email,CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit,tld.move_in,tld.move_out,tld.rent_amount as amt,ud.square_ft,tld.cam_amount as amt1,tld.balance as amt2,if(td.status =1, 'Active', 'Inactive' ),tld.move_in AS lease_date FROM users u JOIN tenant_lease_details tld ON tld.user_id=u.id JOIN tenant_property tp ON u.id=tp.user_id JOIN general_property gp ON gp.id=tp.property_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN tenant_details td ON td.user_id=u.id ";
        $db_query_after_where3 = '';
        $report_date_filter_table3 = 'u';

        $popUpArray3['title'] = $title3;
        $popUpArray3['label'] = $label3;
        $popUpArray3['field'] = $field3;
        $popUpArray3['data'] = $data3;
        $popUpArray3['report_columns'] = $report_columns3;
        $popUpArray3['alternate_report_columns'] = '';
        $popUpArray3['report_url'] = $report_url3;
        $popUpArray3['report_db_query'] = $report_db_query3;
        $popUpArray3['report_join_table'] = '';
        $popUpArray3['db_query_after_where'] = $db_query_after_where3;
        $popUpArray3['report_date_filter_table'] = $report_date_filter_table3;
        /*--------------------------------------------  Tenant List Consolidated Filters End-------------------------------------------*/

        /*-------------------------------------------- Tenant Ledger Filters--------------------------------------------------------*/
        $title4 = "Tenant Ledger";
        $popUpArray4['title'] = $title4;
        /*-------------------------------------------- Tenant Ledger Filters End------------------------------------------------------*/
        /*-------------------------------------------- Tenant Statement Filters--------------------------------------------------------*/
        $title5 = "Tenant Statement";
        $popUpArray5['title'] = $title5;
        
        /*-------------------------------------------- Tenant Statement Filters -----------------------------------------------------*/
        /*-------------------------------------------- Tenant Payment Filters --------------------------------------------------------*/
        $title6 = "Tenant Payment Filters";
        $label6 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field6=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'startDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'endDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']

        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data6 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>'','column'=>[''],'type'=>'selectData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_columns6 = array('Date Paid','Memo','Tenant Name','Amount Paid(AFN)','Balance Due(AFN)');

        $popUpArray6['title'] = $title6;
        $popUpArray6['label'] = $label6;
        $popUpArray6['field'] = $field6;
        $popUpArray6['data'] = $data6;
        $popUpArray6['report_columns'] = $report_columns6;
        $popUpArray6['alternate_report_columns'] = '';
        /*-------------------------------------------- Tenant Payment Filters End -----------------------------------------------------*/

        /*-------------------------------------------- Tenant Consolidated Payment Filters -------------------------------------------*/
        $title7 = "Tenant Consolidated Payment Filters";
        $label7 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field7=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'startDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'endDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']

        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data7 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_columns7 = array('Tenant Name','Property','Building','Unit','Rent('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].') ','Taxes('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].') ','Pet rent('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].') ','Parking Charges('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].') ','Total('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].') ');

        $popUpArray7['title'] = $title7;
        $popUpArray7['label'] = $label7;
        $popUpArray7['field'] = $field7;
        $popUpArray7['data'] = $data7;
        $popUpArray7['report_columns'] = $report_columns7;
        /*-------------------------------------------- Tenant Consolidated Payment Filters End ----------------------------------*/

        /*-------------------------------------------- Tenant Online Payment Filters -------------------------------------------*/
        $title8 = "Tenant Online Payment Filters";
        $label8 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no'],
            ['name' => 'Payment Type','is_required' => 'no']
        ];

        $field8=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'startDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'endDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control paymentType','name' => 'paymentType','data_type_reports'=>'selectData'],
        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data8 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData'],
        ];
        $report_columns8 = array('Tenant Name','Transaction Date','Property','Building','Unit','Cardholder Name','Cardholder Type','Expiration Date','Bill#','Amount('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');

        $popUpArray8['title'] = $title8;
        $popUpArray8['label'] = $label8;
        $popUpArray8['field'] = $field8;
        $popUpArray8['data'] = $data8;
        $popUpArray8['report_columns'] = $report_columns8;
        /*-------------------------------------------- Tenant Online Payment Filters End ----------------------------------*/

        /*-------------------------------------------- Tenant Unpaid Charges Filters -------------------------------------------*/
        $title9 = "Tenant Unpaid Charges Filters";
        $label9 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field9=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'startDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'endDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data9 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];

        $popUpArray9['title'] = $title9;
        $popUpArray9['label'] = $label9;
        $popUpArray9['field'] = $field9;
        $popUpArray9['data'] = $data9;
        /*-------------------------------------------- Tenant Unpaid Charges Filters End ----------------------------------*/
        /*-------------------------------------------- Lease Expiration Filters -------------------------------------------*/
        $title10 = "Lease Expiration Filters";
        $label10 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field10=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data10 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $monthName='';
        $year  = (new DateTime)->format("Y");
        $monthNum1 = (new DateTime)->format("m");
        $report_columns11=array('property','unit');
        //print_r($count);
        for($i=$monthNum1;$i<=12;$i++) {
            //rint_r($i);
            $monthNum = ($i);
            $dateObj = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $monthName = substr($monthName, "0", "3");
            $yy = $monthName . "-" . $year;
            array_push($report_columns11, $yy);
            //  $report_columns11 = array('Property', 'Units', $yy);
            if ($i == '12') {
                $year=$year+1;

                //echo "chakko";
                $count = (11 - (12 - intval($monthNum1)));
                for ($j = 1; $j <= $count; $j++) {

                    $dateObj = DateTime::createFromFormat('!m', $j);
                    $monthName = $dateObj->format('F');
                    $monthName = substr($monthName, "0", "3");
                    $yyy = $monthName . "-" . $year;
                    $total='Total';
                    // $report_columns11 = array('Property', 'Units', $yy);
                    array_push($report_columns11, $yyy,$total);
                    // $report_columns11 = array('Property', 'Units', $yyy);
                }
       
            }
        }

        $popUpArray10['title'] = $title10;
        $popUpArray10['label'] = $label10;
        $popUpArray10['field'] = $field10;
        $popUpArray10['data'] = $data10;
        $popUpArray10['report_columns'] = $report_columns11;
        $popUpArray10['alternate_report_columns'] = '';
        $popUpArray10['report_url'] = $report_url2;
        $popUpArray10['report_db_query'] = $report_db_query2;
        $popUpArray10['report_join_table'] = '';
        $popUpArray10['db_query_after_where'] = $db_query_after_where2;
        /*-------------------------------------------- Lease Expiration Filters End ----------------------------------*/
        /*-------------------------------------------- Delinquent Tenant Filters -------------------------------------------*/
        $title11 = "Consolidated Delinquent Tenant Filters";
        $label11 = [
            ['name' => 'From Date','is_required' => 'no'],
            ['name' => 'To Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field11=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data11 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'type'=>'selectData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_url11='/Reporting/DelinquentTenantReport';
        $report_columns11 = array('Unit','Name','Date of Last Payment','Total(AFN)','0-30 Days (AFN)','31-60 Days (AFN)','61-90 Days (AFN)','Over 90 Days (AFN)');

        $popUpArray11['title'] = $title11;
        $popUpArray11['label'] = $label11;
        $popUpArray11['field'] = $field11;
        $popUpArray11['data'] = $data11;
        $popUpArray11['report_columns'] = $report_columns11;
        $popUpArray11['report_url'] = $report_url11;
        /*-------------------------------------------- Delinquent Tenant Filters End ----------------------------------*/

        /*-------------------------------------------- Delinquent Tenant Filters -------------------------------------------*/
        $title20 = "Consolidated Delinquent Tenant Filters";
        $label20 = [
            ['name' => 'From Date','is_required' => 'no'],
            ['name' => 'To Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field20=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data20 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'type'=>'selectData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_url20='/Reporting/DelinquentEmailTextReport';
        $report_columns20 = array('Unit','Name','Date of Last Payment','Total(AFN)','0-30 Days (AFN)','31-60 Days (AFN)','61-90 Days (AFN)','Over 90 Days (AFN)');

        $popUpArray20['title'] = $title20;
        $popUpArray20['label'] = $label20;
        $popUpArray20['field'] = $field20;
        $popUpArray20['data'] = $data20;
        $popUpArray20['report_columns'] = $report_columns20;
        $popUpArray20['report_url'] = $report_url20;
        /*-------------------------------------------- Delinquent Tenant Filters End ----------------------------------*/

        /*-------------------------------------------- Consolidated Delinquent Tenant Filters ---------------------------------------*/
        $title12 = "Consolidated Delinquent Tenant Filters";
        $label12 = [
            ['name' => 'From Date','is_required' => 'no'],
            ['name' => 'To Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field12=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data12 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'type'=>'selectData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_url12='/Reporting/ConsolidatedDelinquentTenantReport';

        $popUpArray12['title'] = $title12;
        $popUpArray12['label'] = $label12;
        $popUpArray12['field'] = $field12;
        $popUpArray12['data'] = $data12;
        $popUpArray12['report_url'] = $report_url12;
        /*-------------------------------------------- Consolidated Delinquent Tenant Filters End ----------------------------------*/

        /*-------------------------------------------- Tenant Insurance Filters ---------------------------------------*/
        $title13 = "Tenant Insurance Filters";
        $label13 = [
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Tenant Insurance','is_required' => 'no']
        ];
        $field13=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'tenant_insurance','data_type_reports'=>'selectData']
        ] ;

        $joins13 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $onChange13 = ['table'=>'company_property_portfolio','joins'=>$joins13];



        /*Data array*/
        $data13 = [
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange13],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange13],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'type'=>'selectData']
        ];
        $report_url13='/Reporting/Tenant_InsuranceReport';
        $report_columns13 = array('Tenant Name','Building','Unit','Date','Policy Info','Has Insurance');
        $report_db_query13 = "SELECT REPLACE(u.name,' ','--') AS name, REPLACE(bd.building_name,' ','--') AS build_name,concat(ud.unit_prefix,'-',ud.unit_no),DATE_FORMAT(u.created_at,'%Y-%m-%d') as available_date,REPLACE(tri.policy_no,' ', '--') FROM users u LEFT JOIN tenant_renter_insurance tri ON u.id=tri.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN building_detail bd ON tp.building_id=bd.id JOIN general_property gp ON gp.id=bd.property_id ";
        $report_join_table13 = "";
        $db_query_after_where13 = '';

        $report_date_filter_table13 = 'u';
        /*Data array*/
        $popUpArray13['title'] = $title13;
        $popUpArray13['label'] = $label13;
        $popUpArray13['field'] = $field13;
        $popUpArray13['data'] = $data13;
        $popUpArray13['report_columns'] = $report_columns13;
        $popUpArray13['alternate_report_columns'] = '';
        $popUpArray13['report_db_query'] = $report_db_query13;
        $popUpArray13['report_url'] = $report_url13;
        $popUpArray13['report_join_table'] = $report_join_table13;
        $popUpArray13['db_query_after_where'] = $db_query_after_where13;
        $popUpArray13['report_date_filter_table'] = $report_date_filter_table13;
        /*-------------------------------------------- Tenant Insurance Filters End ----------------------------------*/
        /*-------------------------------------------- Deposit Detail Filters ---------------------------------------*/
        $title14 = "Deposit Detail Filters";
        $label14 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no']
        ];

        $field14=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'startDateTimePicker','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'endDateTimePicker','data_type_reports'=>'dateTimeData']
        ] ;

        /*Data array*/
        $data14 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData']
        ];
        $report_url14='/Reporting/DepositDetailReport';
        $report_columns14 = array('Bank Account','Deposit Number','Date','Type','Amount(AFN)','Cleared');

        $popUpArray14['title'] = $title14;
        $popUpArray14['label'] = $label14;
        $popUpArray14['field'] = $field14;
        $popUpArray14['data'] = $data14;
        $popUpArray14['$report_columns'] = $report_columns14;
        $popUpArray14['report_url'] = $report_url14;
        /*-------------------------------------------- Deposit Detail Filters End ----------------------------------*/
        /*-------------------------------------------- Pet Detail Report Filters  ---------------------------------------*/
        $title15 = "Pet Detail Report Filters";
        $label15 = [
            ['name' => 'Pet Report','is_required' => 'no'],
            ['name' => 'Service/Companion Animal Report','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'yes']
        ];

        $field15=[
            ['type' => 'radio','class'=>'pet_report','name' => 'pet_detail','data_type_reports'=>'radio'],
            ['type' => 'radio','class'=>'service_companion_report','name' => 'pet_detail','data_type_reports'=>'radio'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;


        $joins15 = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'property_id',
            'primary' => 'id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ]];
        $where15 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]
        ];

        $onChange15 = ['table'=>'users','joins'=>$joins15];
        $other_joins15=[ [
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];


        /*Data array*/
        $data15 = [
            ['table'=> '','column'=>[''],'type'=>'radio'],
            ['table'=> '','column'=>[''],'type'=>'radio'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange15],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins1'=>$joins15 ,'where'=>$where15,'onChange'=>''],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
//            ['table'=>['main' => 'users','joins1'=>$other_joins15 ,'where'=>$where15,'onChange'=>$onChange15],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_url15='/Reporting/TenantPetDetailReport';
        $report_columns15 = array('Pet Name','Property Name','Pet ID','Breed','Pet Age','Pet Sex','Chip ID','Hospital Name','Next Visit','Shot Name','Shot Given Date','Rabies#','Rabies Given Date');
        $alternate_report_columns15 = array('Animal Name','Property Name','Animal ID','Breed','Animal Age','Animal Sex','Chip ID','Hospital Name','Next Visit','Shot Name','Shot Given Date','Rabies#','Rabies Given Date');
        $report_db_query15 = "SELECT  REPLACE(tp.name,' ','--') AS pet_name,REPLACE(gp.property_name,' ','--') AS name,REPLACE(tp.pet_id,' ','--'),REPLACE(tp.type,' ','--'),tp.age,if(tp.gender =1, 'Male', 'Female' ),REPLACE(tp.chip_id,' ','--'),REPLACE(tp.hospital_name,' ','--') AS hospital_name,tp.next_visit,REPLACE(tp.shots_name,' ','--'),tp.shots_given_date,tp.rabies,tp.rabies_given_date FROM users u JOIN tenant_pet tp on u.id=tp.user_id JOIN tenant_property tpr ON tpr.user_id=u.id JOIN general_property gp ON gp.id=tpr.property_id";
        $report_join_table15 = "";
        $report_date_filter_table15 = "";
//        $popUpArray15['report_db_query'] = $report_db_query;

        /*Data array*/
        $popUpArray15['title'] = $title15;
        $popUpArray15['label'] = $label15;
        $popUpArray15['field'] = $field15;
        $popUpArray15['data'] = $data15;
        $popUpArray15['report_columns'] = $report_columns15;
        $popUpArray15['alternate_report_columns'] = $alternate_report_columns15;
        $popUpArray15['report_db_query'] = $report_db_query15;
        $popUpArray15['report_url'] = $report_url15;
        $popUpArray15['report_join_table'] = $report_join_table15;
        $popUpArray15['db_query_after_where'] = '';
        $popUpArray15['report_date_filter_table'] = $report_date_filter_table15;

        /*-------------------------------------------- Pet Detail Report Filters End ----------------------------------*/
        /*-------------------------------------------- Short Term Renter List Filters -------------------------------------------*/
        $title16 = "Short Term Renter List Filters";
        $label16 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no']
        ];

        $field16=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;

        /*Data array*/
        $data16 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];

        $popUpArray16['title'] = $title16;
        $popUpArray16['label'] = $label16;
        $popUpArray16['field'] = $field16;
        $popUpArray16['data'] = $data16;
        /*-------------------------------------------- Short Term Renter List Filters End ----------------------------------*/
        /*-------------------------------------------- Tenant EEO Report Filters -------------------------------------------*/
        $title17 = "Tenant EEO Report Filters";
        $label17 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'no']
        ];

        $field17=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect']
        ] ;


        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data17 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect']
        ];
        $report_url17='/Reporting/TenantEEOReport';
        $report_columns17 = array('Tenant Name','Tenant Address','Sex','Birthday','Move In Date','Ethnicity','Veteran Status','Marital Status');

        $popUpArray17['title'] = $title17;
        $popUpArray17['label'] = $label17;
        $popUpArray17['field'] = $field17;
        $popUpArray17['data'] = $data17;
        $popUpArray17['report_columns'] = $report_columns17;
        $popUpArray17['alternate_report_columns'] = '';
        $popUpArray17['report_url'] = $report_url17;
        /*-------------------------------------------- Tenant EEO Report Filters End ----------------------------------*/
        /*-------------------------------------------- Tenant Mailing Label Filter ---------------------------------------*/

        $title18 = "Tenant Mailing Label Filter";
        $label18 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Mailing Label Type','is_required' => 'no']];

        $field18=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'mailing_label_type','data_type_reports'=>'selectData']
        ] ;

        /*Data array*/
        $report_url18 = '/Reporting/TenantMailingLabelReport';
        $report_columns18 = '';
        $report_db_query18 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(gpm.address1,' ','--') as address1,REPLACE(gpm.address2,' ','--') as address2,REPLACE(gpm.address3,' ','--') as address3,REPLACE(gpm.address4,' ','--') as address4 ,concat(REPLACE(gpm.city,' ','--'),',--',REPLACE(gpm.state,' ','--'),'--',gpm.zipcode) AS tenant_add FROM users u JOIN tenant_property tp ON tp.user_id=u.id JOIN tenant_details td ON td.user_id=u.id JOIN general_property gpm ON gpm.id=tp.property_id ";
        $report_join_table18 = "";
        $data18 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>$onChange2],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'mailing_label_type']
        ];
        /*Data array*/

        $db_query_after_where18 = '';
        $report_date_filter_table18 = 'u';
        $popUpArray18['title'] = $title18;
        $popUpArray18['label'] = $label18;
        $popUpArray18['field'] = $field18;
        $popUpArray18['data'] = $data18;
        $popUpArray18['report_columns'] = $report_columns18;
        $popUpArray18['alternate_report_columns'] = '';
        $popUpArray18['report_db_query'] = $report_db_query18;
        $popUpArray18['report_url'] = $report_url18;
        $popUpArray18['report_join_table'] = $report_join_table18;
        $popUpArray18['db_query_after_where'] = $db_query_after_where18;
        $popUpArray18['report_date_filter_table'] = $report_date_filter_table18;
        /*-------------------------------------------- Tenant Mailing Label Filter End ----------------------------------*/
        /*-------------------------------------------- Print Envelope for Tenant Filters ---------------------------------------*/
        $title19 = "Print Envelope for Tenant Filters";
        $label19 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'no']
        ];

        $field19=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property_id','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','id' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;


        $joins19 = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'property_id',
            'primary' => 'id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ]];
        $where19 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];
        $onChange19 = ['table'=>'users','joins'=>$joins19];
        $report_url19 = '/Reporting/PrintEnvelopeReport';
        $report_columns19 = '';
        $report_db_query19 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(gp.address1,' ','--') as address1,REPLACE(gp.address2,' ','--') as address2,REPLACE(gp.address3,' ','--') as address3,REPLACE(gp.address4,' ','--') as address4 ,CONCAT('Unit:',ud.unit_prefix,'-',ud.unit_no) AS unit ,concat(REPLACE(gp.city,' ','--'),',--',REPLACE(gp.state,' ','--'),'--',gp.zipcode) AS tenant_add FROM users u JOIN tenant_property tp ON tp.user_id=u.id JOIN tenant_details td ON td.user_id=u.id JOIN general_property gp ON gp.id=tp.property_id JOIN unit_details ud ON tp.unit_id=ud.id";
        $report_join_table19 = "tp";
        /*Data array*/
        $data19 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>$onChange2],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange19],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where19],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];

        $db_query_after_where19 = '';
        $report_date_filter_table19 = 'u';
        /*Data array*/
        $popUpArray19['title'] = $title19;
        $popUpArray19['label'] = $label19;
        $popUpArray19['field'] = $field19;
        $popUpArray19['data'] = $data19;
        $popUpArray19['report_columns'] = $report_columns19;
        $popUpArray19['alternate_report_columns'] = '';
        $popUpArray19['report_db_query'] = $report_db_query19;
        $popUpArray19['report_url'] = $report_url19;
        $popUpArray19['report_join_table'] = $report_join_table19;
        $popUpArray19['db_query_after_where'] = $db_query_after_where19;
        $popUpArray19['report_date_filter_table'] = $report_date_filter_table19;
        /*-------------------------------------------- Print Envelope for Tenant Filters End ----------------------------------*/

        /*Append data Sub heading*/

        $listName = [];
        $listName = [['Rent Roll' => $popUpArray],['Rent Roll Consolidate' => $popUpArray1],['Tenant List' => $popUpArray2],['Tenant List Consolidated' => $popUpArray3],['Tenant Ledger' => $popUpArray4],['Tenant Statement' => $popUpArray5],['Tenant Payment' => ''],['Tenant Consolidated Payment' => ''],['Tenant Online Payment Filters' => ''],['Tenant Unpaid Charges Filters' => ''],['Lease Expiration' => ''],['Delinquent Tenant' => ''],['Delinquent Tenant Email/Text' => ''],['Consolidated Delinquent Tenant' => ''],['Tenant Insurance' => $popUpArray13],['Deposit Detail' => $popUpArray14],['Pet Detail Report' => $popUpArray15],['Short Term Renter List' => $popUpArray16],['Tenant EEO Report' => ''],['Tenant Mailing Label' => $popUpArray18],['Print Envelope for Tenant' => $popUpArray19]];
        return $listName;
    }

    public static function popupsReportMaintenance()
    {


        $popUpArray=[];
        $popUpArray1=[];
        $popUpArray2=[];
        /*-------------------------------------------- Open Work Order Filters ---------------------------------------*/
        $title = "Open Work Order Filters";
        $label = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Tenant','is_required' => 'yes'],
            ['name' => 'Estimated Cost','is_required' => 'no'],
            ['name' => 'Work Order Number','is_required' => 'no']
        ];

        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'envelopeSize','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control textfield','name' => 'workOrderNumber','data_type_reports'=>'text']
        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'text'],
            ['table'=> '','column'=>[''],'type'=>'text']
        ];

        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        /*-------------------------------------------- Open Work Order Filters End ----------------------------------*/

        /*--------------------------------------------  Completed Work Order Filters ---------------------------------------*/
        $title1 = "Completed Work Order Filters";
        $label1 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => ' Vendor','is_required' => 'yes']
        ];

        $field1=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'vendor[]','id' => 'vendor','data_type_reports'=>'multiselect']
        ] ;

        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];
        $report_url1 = '/Reporting/Maintenance_OpenWorkOrderReport';
        $report_columns1 = array('Work Order Number','Building','Unit #','Created On','Date Scheduled','Date Completed','Description of Work','Days Active','Vendor Company','Vendor');
        $report_db_query1 = "SELECT wo.work_order_number,REPLACE(bd.building_name,' ','--'),CONCAT(ud.unit_prefix,'-',ud.unit_no) as unit_no,REPLACE(wo.created_on,' 00:00:00',''),REPLACE(wo.scheduled_on,' ','--'),REPLACE(wo.completed_on,' ','--'),REPLACE(wo.work_order_description,' ','--'),REPLACE(vad.use_company_name,' ','--') as days_active,REPLACE(vad.use_company_name,' ','--'),REPLACE(u.name,' ','--') FROM work_order wo JOIN building_detail bd ON wo.building_id=bd.id JOIN unit_details ud ON wo.unit_id=ud.id join vendor_additional_detail vad on wo.vendor_id=vad.vendor_id JOIN users u ON wo.vendor_id=u.id";
        $report_join_table1='wo';
        $db_query_after_where1='';
        $report_date_filter_table1='wo';

        /*Data array*/
        $data1 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id','portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main'=>'users','joins'=>null,'where'=>$where1],'column'=>['id','name'],'field_name'=>'vendor[]','type'=>'selectData'],
        ];

        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['alternate_report_columns'] = '';
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = $report_join_table1;
        $popUpArray1['db_query_after_where'] = $db_query_after_where1;
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*-------------------------------------------- Completed Work Order Filters End ----------------------------------*/
        /*--------------------------------------------  Consolidated Purchase Order starts ---------------------------------------*/
        $title2 = "Consolidated Purchase Orders";
        $label2 = [
            ['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Portfolio','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Vendor','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
        ];

        $field2=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'vendor[]','id' => 'vendor','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],

        ] ;

        $where2 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data2 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where2],'column'=>['id','name'],'field_name'=>'vendor[]', 'type'=>'multiselect'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>null],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main' => 'purchaseOrder','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
        ];

        $report_url2 = '/Reporting/PurchaseOrderReport';
        $report_columns2 = array('PO Number','Property','Building','Unit','Required By Date','Vendor','Work Orders','Approvers','Invoice No','PO Status');
        $report_db_query2 = "SELECT po.po_number,REPLACE(gp.property_name,' ','--'),REPLACE(bd.building_name,' ','--'),CONCAT(ud.unit_prefix,'-',ud.unit_no),REPLACE(po.required_by,' 00:00:0',''),REPLACE(u.name,' ','--'),REPLACE(po.work_order,' ','--'),REPLACE(u.name,' ','--') as approver,po.invoice_number,po.status FROM purchaseOrder po JOIN general_property gp ON po.property=gp.id JOIN building_detail bd ON po.building=bd.id JOIN unit_details ud ON po.unit=ud.id JOIN users u ON po.vendorid=u.id";
        $report_join_table2='';
        $db_query_after_where2=' ';
        $report_date_filter_table2='po';

        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        $popUpArray2['report_columns'] = $report_columns2;
        $popUpArray2['alternate_report_columns'] = '';
        $popUpArray2['report_db_query'] = $report_db_query2;
        $popUpArray2['report_url'] = $report_url2;
        $popUpArray2['report_join_table'] = $report_join_table2;
        $popUpArray2['db_query_after_where'] = $db_query_after_where2;
        $popUpArray2['report_date_filter_table'] = $report_date_filter_table2;
        /*--------------------------------------------  Consolidated Purchase Order ends ---------------------------------------*/
        $listName2=[];
        $listName2 = [['Open Work Order' => ' '],['Completed Work Order' => $popUpArray1],['Consolidated Purchase Order' => $popUpArray2]];
        return $listName2;
    }

    public static function propertyReportData(){

        $popUpArray = [];
        /*------------------------------------------- Property List Filters Filters --------------------------------------------------*/
        $title0   = "Property Listing Report";
        $label0 = '';
        $field0='';
        $data0 = '';
        $db_query_after_where0=' GROUP BY gp.property_name';
        $report_join_table0='';
        $report_url0 = '/Reporting/PropertyListingReport';
        $report_columns0 = array('Property Name','Property Address','Property Owner Name');
        $report_db_query0 = "SELECT REPLACE(gp.property_name,' ','--') AS name, CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address , GROUP_CONCAT(REPLACE(u.name, ' ', '--'), '--','(',opo.property_percent_owned,'.00%)') AS user_name FROM general_property gp LEFT JOIN owner_property_owned opo ON opo.property_id = gp.id LEFT JOIN users u ON opo.user_id = u.id";
        $popUpArray0['title'] = $title0;
        $popUpArray0['label'] = $label0;
        $popUpArray0['field'] = $field0;
        $popUpArray0['data'] = $data0;
        $popUpArray0['report_columns'] = $report_columns0;
        $popUpArray0['alternate_report_columns'] = '';
        $popUpArray0['report_db_query'] = $report_db_query0;
        $popUpArray0['report_url'] = $report_url0;
        $popUpArray0['report_join_table'] = $report_join_table0;
        $popUpArray0['db_query_after_where'] = $db_query_after_where0;
        /*------------------------------------------- Property List Filters Filters --------------------------------------------------*/

        /*------------------------------------------- Unit List Filters Filters --------------------------------------------------*/
        $title1   = "Unit List Filters";
        $label1 = [['name' => 'Date','is_required' => 'yes'],['name' => 'Portfolio','is_required' => 'yes'],['name' => 'Status','is_required' => 'no'],['name' => 'Property','is_required' => 'yes']];
        $field1=[
            ['type' => 'text','class'=>'form-control textfield','name' => 'current_date','data_type_reports'=>'datepicker'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
        ] ;
        $data1 = [
            ['table'=> '',' '=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];
        $db_query_after_where1='';
        $report_join_table1='';
        $report_url1 = '/Reporting/UnitListReport';
        $report_columns1 = array('Property','Unit Number','List Date','Delist Date','Available Date','Bedrooms','Bathrooms','Size','Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query1 = "SELECT  Replace(gp.property_name, ' ', '--') AS name,CONCAT(REPLACE(ud.unit_prefix,' ','--'),'-',ud.unit_no),
                             DATE_FORMAT(ud.updated_at, '%Y-%m-%d') as list_date, DATE_FORMAT(ud.updated_at,'%Y-%m-%d') as delist_date,DATE_FORMAT(ud.created_at,'%Y-%m-%d') as available_date,ud.bedrooms_no,
                             ud.bathrooms_no,ud.square_ft,ud.market_rent as amt FROM unit_details ud
                             JOIN general_property gp ON ud.property_id=gp.id WHERE ud.property_id";
        $report_date_filter_table='ud';

        $popUpArray['title'] = $title1;
        $popUpArray['label'] = $label1;
        $popUpArray['field'] = $field1;
        $popUpArray['data'] = $data1;
        $popUpArray['report_columns'] = $report_columns1;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_db_query'] = $report_db_query1;
        $popUpArray['report_url'] = $report_url1;
        $popUpArray['report_join_table'] = $report_join_table1;
        $popUpArray['db_query_after_where'] = ' ';
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*------------------------------------------- Unit List Filters Filters --------------------------------------------------*/

        /*------------------------------------- Unit Features Start -------------------------------------------------------------------------*/
        $title = "Unit Features Filters";
        $label = [
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/UnitFeatureReport';
        $report_columns = array('Property Name','Unit Number','Size','# Bedrooms','# Bathrooms','Available','Is Published','Amenities','Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query = "SELECT REPLACE(gp.property_name,' ','--') AS name,CONCAT(REPLACE(ud.unit_prefix,' ','--'),'-',ud.unit_no),ud.square_ft,ud.bedrooms_no,ud.bathrooms_no,ud.building_unit_status,'Published' AS virtual_columns,'' AS virtual_col1,ud.market_rent AS amt FROM unit_details ud JOIN general_property gp ON ud.property_id = gp.id join work_order wo ON gp.id = wo.property_id";
        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']]
        ];
        /*Data array*/
        $report_date_filter_table='ud';

        $popUpArray1['title'] = $title;
        $popUpArray1['label'] = $label;
        $popUpArray1['field'] = $field;
        $popUpArray1['data'] = $data;
        $popUpArray1['report_columns'] = $report_columns;
        $popUpArray1['alternate_report_columns'] = '';
        /*$popUpArray['report_db_query'] = $report_db_query;*/
        $popUpArray1['report_url'] = $report_url;
        $popUpArray1['report_db_query'] = $report_db_query;
        $popUpArray1['report_join_table'] = $report_join_table;
        $popUpArray1['db_query_after_where'] = '';
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table;

        /*-------------------------------------------------- Unit Features End --------------------------------------------------------------*/
        /*-------------------------------------------------- Vacant Unit Filters ------------------------------------------------------------*/
        $title = "Vacant Unit Filters";
        $label = [
            ['name' => 'Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/VacantUnitsReport';
        $report_columns = "";
        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];
        $report_date_filter_table='gp';

        /*Data array*/
        $popUpArray2['title'] = $title;
        $popUpArray2['label'] = $label;
        $popUpArray2['field'] = $field;
        $popUpArray2['data'] = $data;
        $popUpArray2['report_columns'] = $report_columns;
        $popUpArray2['alternate_report_columns'] = '';
        /*$popUpArray['report_db_query'] = $report_db_query;*/
        $popUpArray2['report_url'] = $report_url;
        $popUpArray2['report_join_table'] = $report_join_table;
        $popUpArray2['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Vacant Units Report End --------------------------------------------------------------*/
        /*-------------------------------------------------- Property Statement Filters ---------------------------------------------------*/
        $title = "Property Statement Filters";
        $label = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/PropertyStatementReport';
        $report_columns = "";
        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'typ-e'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']]
        ];
        /*Data array*/
        $popUpArray3['title'] = $title;
        $popUpArray3['label'] = $label;
        $popUpArray3['field'] = $field;
        $popUpArray3['data'] = $data;
        $popUpArray3['report_columns'] = $report_columns;
        $popUpArray3['alternate_report_columns'] = '';
        /*$popUpArray['report_db_query'] = $report_db_query;*/
        $popUpArray3['report_url'] = $report_url;
        $popUpArray3['report_join_table'] = $report_join_table;

        /*--------------------------------------------------Property Statement Filters End -------------------------------------------------------*/
        /*-------------------------------------------------- Property Summary Report Filters Start ---------------------------------*/
        $title4 = "Property Summary Report Filters";
        $label4 = [
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field4=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange4 = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];


        $report_db_query4 = "SELECT CONCAT(REPLACE(gp.address1,' ', '--'),'--',REPLACE(gp.address2,' ', '--'),'--',REPLACE(gp.address3,' ', '--'),'--',REPLACE(gp.address4,' ', '--')) AS name, (SELECT COUNT(ud.id) AS units FROM general_property gp JOIN unit_details ud ON ud.property_id=gp.id ) AS Total_units, (SELECT COUNT(ud.building_unit_status=1) as vacant FROM general_property gp JOIN unit_details ud ON ud.property_id=gp.id WHERE ud.building_unit_status=4) AS occupied, (SELECT COUNT(ud.building_unit_status=1) as occupied FROM general_property gp JOIN unit_details ud ON ud.property_id=gp.id WHERE ud.building_unit_status=1) AS vacant FROM general_property gp WHERE gp.deleted_at IS NULL ";
        $db_query_after_where4 = ' ';
        $report_url4 = '/Reporting/PropertySummaryReport';
        $report_columns4 = array('Property Address','Total Units','Occupied%','Vacant%');
        $report_join_table4 = "tp";
        /*Data array*/
        $data4 = [
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange4],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange4],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];
        /*Data array*/
        $report_date_filter_table4='gp';

        $popUpArray4['title'] = $title4;
        $popUpArray4['label'] = $label4;
        $popUpArray4['field'] = $field4;
        $popUpArray4['data'] = $data4;
        $popUpArray4['report_columns'] = $report_columns4;
        $popUpArray4['alternate_report_columns'] = '';
        /*$popUpArray['report_db_query'] = $report_db_query;*/
        $popUpArray4['report_url'] = $report_url4;
        $popUpArray4['report_join_table'] = $report_join_table4;
        $popUpArray4['report_db_query'] = $report_db_query4;
        $popUpArray4['db_query_after_where'] = $db_query_after_where4;
        $popUpArray4['report_date_filter_table'] = $report_date_filter_table4;
        /*-------------------------------------------------- Property summary report Filters Start ------------------------------------------*/
        /*-------------------------------------------------- Property Group Filters Start -------------------------------------------------------*/
        $title5 = "Property Group Filters";
        $label5 = [
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Group','is_required' => 'no']
        ];
        $field5=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'property_group_name','name' => 'property_group_name','data_type_reports'=>'selectData']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange5 = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url5 = '/Reporting/PropertyGroup';
        $report_columns5 = array('Property Group Name','Property Name','Address');
        $report_db_query5 = "SELECT gp.attach_groups as property_group, REPLACE(cpg.group_name,' ','--') AS name, REPLACE(gp.property_name,' ','--') AS prop_name,CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address FROM general_property gp JOIN company_property_groups cpg ON gp.id = cpg.id WHERE gp.deleted_at IS NULL ";
        $report_join_table = "tp";
        /*Data array*/
        $data5 = [
//            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange5],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange5],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'company_property_groups','joins'=>null ,'where'=>null,'onChange'=>$onChange5],'column'=>['id','group_name'],'field_name'=>'property_group_name','type'=>'selectData','defaultData'=>['default','0','1']],
//            ['table'=> '','column'=>[''],'type'=>'selectData'],
        ];
        $report_date_filter_table='gp';
        /*Data array*/
        $popUpArray5['title'] = $title5;
        $popUpArray5['label'] = $label5;
        $popUpArray5['field'] = $field5;
        $popUpArray5['data'] = $data5;
        $popUpArray5['report_columns'] = $report_columns5;
        $popUpArray5['alternate_report_columns'] = '';
        $popUpArray5['report_db_query'] = $report_db_query5;
        $popUpArray5['report_url'] = $report_url5;
        $popUpArray5['report_join_table'] = '';
        $popUpArray5['db_query_after_where'] = '';
        $popUpArray5['report_date_filter_table'] = $report_date_filter_table;

        /*--------------------------------------------------End --------------------------------------------------------------------*/
        /*-------------------------------------------------- Portfolio Filters Start ---------------------------------*/
        $title = "Portfolio Filters";
        $label = [
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/Portfolio';
        $report_columns = array('Portfolio Name','Property Name','Property Address','Total Units','Occupied %');
        $report_join_table = "tp";
        $report_db_query = "SELECT REPLACE(cpp.portfolio_name,' ','--') AS name, REPLACE(gp.property_name,' ','--') as prop_name , REPLACE(CONCAT(gp.address1,' ',gp.address2,' ',gp.address3,' ',gp.address4),' ','--'),gp.unit_exist AS Total_units,CONCAT(ROUND(((SELECT COUNT(ud.id) from unit_details ud WHERE ud.property_id=gp.id AND ud.building_unit_status='4') * 100 / gp.unit_exist),2),'%')AS occupied FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id=cpp.id";
        /*Data array*/
        $data = [
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
//            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']]
        ];
        $report_date_filter_table6='cpp';
        /*Data array*/
        $popUpArray6['title'] = $title;
        $popUpArray6['label'] = $label;
        $popUpArray6['field'] = $field;
        $popUpArray6['data'] = $data;
        $popUpArray6['report_columns'] = $report_columns;
        $popUpArray6['alternate_report_columns'] = '';
        $popUpArray6['report_db_query'] = $report_db_query;
        $popUpArray6['report_url'] = $report_url;
        $popUpArray6['report_join_table'] = $report_join_table;
        $popUpArray6['report_date_filter_table'] = $report_date_filter_table6;
        /*-------------------------------------------------- Portfolio Filters End ------------------------------------------*/

        /*-------------------------------------------------- Vacant Property Listing Filters Start ---------------------------------*/
        $title7 = "Vacant Property Listing Filters";
        $label7 = [
            ['name' => 'Date','is_required' => 'no'],
            ['name' => 'Vacant %','is_required' => 'no']
        ];
        $field7=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control','id'=>'','name' => '','data_type_reports'=>'textData']
        ] ;
        $joins7 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange7 = ['table'=>'company_property_portfolio','joins'=>$joins7];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url7 = '/Reporting/VacantPropertyListingReport';
        $report_columns7 = array('Property Name','Property Address','Number of Units','Number of Vacant Units','% of Units Vacant');
        $report_join_table7 = "";
        $report_db_query7="SELECT (SELECT REPLACE(gp.property_name,' ','--') AS name FROM general_property gp ) AS name, (SELECT CONCAT(gp.address1,'--',gp.address2,'--',gp.address3,'--',gp.address4) AS address FROM general_property gp) AS address, (SELECT COUNT(ud.id) AS units FROM general_property gp JOIN unit_details ud ON ud.property_id=gp.id ) AS Total_units, (SELECT COUNT(ud.building_unit_status=1) as vacant FROM general_property gp inner JOIN unit_details ud ON ud.property_id=gp.id WHERE ud.building_unit_status=4) AS occupied, (SELECT COUNT(ud.building_unit_status=1) as occupied FROM general_property gp inner JOIN unit_details ud ON ud.property_id=gp.id WHERE ud.building_unit_status=1) AS vacant";
        /*Data array*/
        $data7 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'textData']
        ];

        $report_date_filter_table7 = 'gp';

        /*Data array*/
        $popUpArray7['title'] = $title7;
        $popUpArray7['label'] = $label7;
        $popUpArray7['field'] = $field7;
        $popUpArray7['data'] = $data7;
        $popUpArray7['report_columns'] = $report_columns7;
        $popUpArray7['alternate_report_columns'] = '';
        $popUpArray7['report_db_query'] = $report_db_query7;
        $popUpArray7['report_url'] = $report_url7;
        $popUpArray7['report_join_table'] = $report_join_table7;
        $popUpArray7['report_date_filter_table'] = $report_date_filter_table7;
        /*-------------------------------------------------- Vacant  report Filters Start ------------------------------------------*/
        /*-------------------------------------------------- Inspection Report Filters ---------------------------------------------------*/
        $title8 = "Inspection Report Filters";
        $label8 = [
            ['name' => 'From Date','is_required' => 'no'],
            ['name' => 'To Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Building Name','is_required' => 'no'],
            ['name' => 'Unit Name','is_required' => 'no']
        ];
        $field8=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'portfolio_id','name' => 'portfolio[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'building_id','name' => 'building[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'unit_id','name' => 'unit[]','data_type_reports'=>'multiselect']
        ] ;
        $joins8 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange8 = ['table'=>'company_property_portfolio','joins'=>$joins8];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url8 = '/Reporting/InspectionReport';
        $report_columns8 = array('Property','Property Address','Building Name','Building Address','Unit Name','Inspector','Date Of Inspection','Inspection Name','Inspection Type');
        $report_db_query8 = "SELECT REPLACE(pi.property_name,' ','--') AS pName,REPLACE(pi.property_address,' ','--') AS paddress,REPLACE(pi.building_name,' ','--') AS bname,REPLACE(pi.building_address,' ','--') AS baddress,REPLACE(pi.unit_name,' ','--') AS uname,REPLACE(u.name,' ','--') AS userName,REPLACE(pi.inspection_date,' ','--') AS inspDate,REPLACE(ilp.inspection_name,' ','--') AS inspName,REPLACE(pi.inspection_type,' ','--') AS inspecType FROM property_inspection pi JOIN inspection_list_property ilp ON pi.inspection_name=ilp.id JOIN users u ON pi.user_id=u.id JOIN general_property gp  on pi.property_name=gp.property_name JOIN company_property_portfolio cpp ON gp.portfolio_id=cpp.id";
        /*$report_join_table8 = "tp";*/
        /*Data array*/
        $data8 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange8],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main'=>'building_detail','onChange'=>$onChange],'column'=>['id','building_name'],'field_name'=>'building[]'],
            ['table'=>['main'=>'unit_details','onChange'=>$onChange],'column'=>['id','unit_prefix','unit_no'],'field_name'=>'unit[]']
        ];
        $report_date_filter_table8='pi';
        /*Data array*/
        $popUpArray8['title'] = $title8;
        $popUpArray8['label'] = $label8;
        $popUpArray8['field'] = $field8;
        $popUpArray8['data'] = $data8;
        $popUpArray8['report_columns'] = $report_columns8;
        $popUpArray8['alternate_report_columns'] = '';
        $popUpArray8['report_db_query'] = $report_db_query8;
        $popUpArray8['report_url'] = $report_url8;
        $popUpArray8['report_join_table'] = "";
        $popUpArray8['report_date_filter_table'] = $report_date_filter_table8;
        /*--------------------------------------------------Inspection Report Filters -------------------------------------------------------*/
        /*-------------------------------------------------- Rental Application Filters Start ---------------------------------------------------*/
        $title = "Rental Application Filters";
        $label = [
            ['name' => 'From Date','is_required' => 'no'],
            ['name' => 'To Date','is_required' => 'no'],
            ['name' => 'Applicant Name','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'','name' => '','data_type_reports'=>'selectData']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/RentalApplicationReport';
        $report_columns = '';
        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>'','field_name'=>'','type'=>'selectData','defaultData'=>'']
        ];
        /*Data array*/
        $report_date_filter_table='';

        $popUpArray9['title'] = $title;
        $popUpArray9['label'] = $label;
        $popUpArray9['field'] = $field;
        $popUpArray9['data'] = $data;
        $popUpArray9['report_columns'] = $report_columns;
        $popUpArray9['alternate_report_columns'] = '';
        /*$popUpArray['report_db_query'] = $report_db_query;*/
        $popUpArray9['report_url'] = $report_url;
        $popUpArray9['report_join_table'] = $report_join_table;
        $popUpArray9['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Rental Application Filters End -------------------------------------------------------*/
        /*-------------------------------------------------- Inventory Report Filters ---------------------------------------------------*/
        $title = "Inventory Report Filters";
        $label = [
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Building','is_required' => 'no'],
            ['name' => 'Unit','is_required' => 'no']
        ];
        $field=[
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'building_id','name' => 'building[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'unit_id','name' => 'unit[]','data_type_reports'=>'multiselect'],
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/InventoryReport';
        $report_columns = array('Property Name','Building','Unit','Inventory Item','Warranty Date','Guarantee','Replacement Date','Serial#','Item#','Make');
        $report_join_table = "";
        /*Data array*/
        $data = [
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main'=>'building_detail','onChange'=>$onChange],'column'=>['id','building_name'],'field_name'=>'building[]'],
            ['table'=>['main'=>'unit_details','onChange'=>$onChange],'column'=>['id','unit_prefix','unit_no'],'field_name'=>'unit[]']
        ];
        $report_db_query="SELECT REPLACE(gp.property_name,' ','--') as name , REPLACE(bd.building_name,' ','--'), CONCAT(ud.unit_prefix,'-',ud.unit_no),pi.item_code as inven_item,CONCAT(pi.warranty_from,'-',pi.warranty_to),pi.guarantee,pi.replacement_date,pi.item_code as serial_no,pi.item_code as item_no,pi.item_code as make from property_inventory pi JOIN general_property gp ON gp.id=pi.property_id JOIN building_detail bd ON bd.id=pi.building_id JOIN unit_details ud ON ud.id=pi.unit_id";
        /*Data array*/
        $report_date_filter_table='pi';

        $popUpArray10['title'] = $title;
        $popUpArray10['label'] = $label;
        $popUpArray10['field'] = $field;
        $popUpArray10['data'] = $data;
        $popUpArray10['report_columns'] = $report_columns;
        $popUpArray10['alternate_report_columns'] = '';
        $popUpArray10['report_db_query'] = $report_db_query;
        $popUpArray10['report_url'] = $report_url;
        $popUpArray10['report_join_table'] = $report_join_table;
        $popUpArray10['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Inventory Report Filters End -------------------------------------------------------*/
        /*-------------------------------------------------- Lost Items Reports Filters ---------------------------------------------------*/
        $title = "Lost Items Reports Filters";
        $label = [
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'From','is_required' => 'yes'],
            ['name' => 'To','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/LostItemsReport';
        $report_columns = array('Item Number','Status','Description','Category','Date Lost','Location Lost','Match Info');
        $report_join_table = "";
        /*Data array*/
        $data = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];
        $report_db_query="SELECT mlf.item_number ,mlf.status,
                              REPLACE(mlf.description,' ','--'),REPLACE(mc.category_name,' ','--') as name,REPLACE(mlf.lost_date,' 00:00:00','') AS lost_date,mlf.lost_location,
                              mlf.matched_status FROM maintenance_lost_found mlf
                              JOIN maintenance_category mc ON mlf.category_id=mc.id JOIN general_property gp ON gp.id=mlf.property_id where mlf.type='L'";

        $report_date_filter_table='mlf';
        /*Data array*/
        $db_query_after_where='';
        $popUpArray11['title'] = $title;
        $popUpArray11['label'] = $label;
        $popUpArray11['field'] = $field;
        $popUpArray11['data'] = $data;
        $popUpArray11['report_columns'] = $report_columns;
        $popUpArray11['alternate_report_columns'] = '';
        $popUpArray11['report_db_query'] = $report_db_query;
        $popUpArray11['report_url'] = $report_url;
        $popUpArray11['report_join_table'] = $report_join_table;
        $popUpArray11['db_query_after_where'] = $db_query_after_where;
        $popUpArray11['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Lost Items Reports End -------------------------------------------------------*/
        /*-------------------------------------------------- Found Items Filters ---------------------------------------------------*/
        $title = "Found Items Filters";
        $label = [
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'From','is_required' => 'yes'],
            ['name' => 'To','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/FoundItemsReport';
        $report_columns = array('Item Number','Status','Description','Category','Date Found','Location Found','Match Info');
        $report_join_table = "";
        /*Data array*/
        $data = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];

        $report_db_query="SELECT mlf.item_number ,mlf.status,
                              REPLACE(mlf.description,' ','--'),REPLACE(mc.category_name,' ','--') as name,DATE_FORMAT(mlf.lost_date,'%Y-%m-%d') AS lost_date,REPLACE(mlf.lost_location,' ','--'),
                              mlf.matched_status FROM maintenance_lost_found mlf
                              JOIN maintenance_category mc ON mlf.category_id=mc.id JOIN general_property gp ON gp.id=mlf.property_id where mlf.type='F'";

        $report_date_filter_table='mlf';
        /*Data array*/
        $popUpArray12['title'] = $title;
        $popUpArray12['label'] = $label;
        $popUpArray12['field'] = $field;
        $popUpArray12['data'] = $data;
        $popUpArray12['report_columns'] = $report_columns;
        $popUpArray12['alternate_report_columns'] = '';
        $popUpArray12['report_db_query'] = $report_db_query;
        $popUpArray12['report_url'] = $report_url;
        $popUpArray12['report_join_table'] = $report_join_table;
        $popUpArray12['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Found Items End -------------------------------------------------------*/
        /*-------------------------------------------------- Matched Items Filters ---------------------------------------------------*/
        $title = "Matched Items Filters";
        $label = [
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'From','is_required' => 'yes'],
            ['name' => 'To','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/MatchedItemsReport';
        $report_columns = array('Item #','Status','Description','Category','Date Lost','Date Found','Matched','Match Date','Returned','Returned Date');

        $report_join_table = "";
        /*Data array*/
        $data = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];

        $report_db_query="SELECT mlf.item_number ,mlf.status, mlf.description,mc.category_name as name,DATE_FORMAT(mlf.lost_date,'%Y-%m-%d') AS lost_date,REPLACE(mlf.match_date,' 00:00:00','') AS found_date, mlf.matched_status,REPLACE(mlf.match_date,' 00:00:00',''), mlf.lost_location,REPLACE(mlf.return_date,' 00:00:00','') FROM maintenance_lost_found mlf INNER JOIN maintenance_category mc ON mlf.category_id=mc.id INNER JOIN general_property gp ON gp.id=mlf.property_id where mlf.matched_status IS NOT NULL AND mlf.match_date IS NOT NULL";
        $report_date_filter_table='mlf';
        /*Data array*/
        $popUpArray13['title'] = $title;
        $popUpArray13['label'] = $label;
        $popUpArray13['field'] = $field;
        $popUpArray13['data'] = $data;
        $popUpArray13['report_columns'] = $report_columns;
        $popUpArray13['alternate_report_columns'] = '';
        $popUpArray13['report_db_query'] = $report_db_query;
        $popUpArray13['report_url'] = $report_url;
        $popUpArray13['report_join_table'] = $report_join_table;
        $popUpArray13['report_date_filter_table'] = $report_date_filter_table;
        /*-------------------------------------------------- Matched Items Filters End -------------------------------------------------------*/
        /*-------------------------------------------------- Unclaimed Items Reports Filters ---------------------------------------------------*/
        $title = "Unclaimed Items Reports Filters";
        $label = [
            ['name' => 'From','is_required' => 'yes'],
            ['name' => 'To','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/LostItemsReport';
        $report_columns = array('Item #','Status','Description','Category','Date Lost','Date Found','Release Lost','Dispose Action Taken');
        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];
        /*Data array*/
        $report_db_query="SELECT mlf.item_number ,mlf.status,
                             REPLACE(mlf.description,' ','--'),REPLACE(mc.category_name,' ','--') as name, DATE_FORMAT(mlf.lost_date, '%Y-%m-%d') AS lost_date,DATE_FORMAT(mlf.return_date, '%Y-%m-%d'),
                              DATE_FORMAT(mlf.release_date, '%Y-%m-%d'),REPLACE(mlf.release_method,' ','--') as action_taken FROM maintenance_lost_found mlf
                              JOIN maintenance_category mc ON mlf.category_id=mc.id JOIN general_property gp ON gp.id=mlf.property_id";

        $db_query_after_where='';
        $report_date_filter_table='mlf';

        $popUpArray14['title'] = $title;
        $popUpArray14['label'] = $label;
        $popUpArray14['field'] = $field;
        $popUpArray14['data'] = $data;
        $popUpArray14['report_columns'] = $report_columns;
        $popUpArray14['alternate_report_columns'] = '';
        $popUpArray14['report_db_query'] = $report_db_query;
        $popUpArray14['report_url'] = $report_url;
        $popUpArray14['report_join_table'] = $report_join_table;
        $popUpArray14['db_query_after_where'] = $db_query_after_where;
        $popUpArray14['report_date_filter_table'] = $report_date_filter_table;

        /*------------------------------------------- Unclaimed Items Reports Filters End ------------------------------------------------*/

        /*-------------------------------------------------- Print Envelope For Property Filters -----------------------------------------*/
        $title15 = "Print Envelope For Property Filters";
        $label15 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'yes']
        ];
        $field15=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','id'=>'status','changed_column'=>'tenant','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;

        $report_url15 = '/Reporting/PrintEnvelopeReport';
        $report_columns15 = '';
        $report_db_query15 = "SELECT REPLACE(gp.property_name,' ','--') AS name, REPLACE(gp.address1,' ','--') as address1,REPLACE(gp.address2,' ','--') as address2,REPLACE(gp.address3,' ','--') as address3,REPLACE(gp.address4,' ','--') as address4,concat(REPLACE(gp.city,' ','--'),',--',REPLACE(gp.state,' ','--'),'--',gp.zipcode) AS tenant_add  FROM general_property gp WHERE gp.deleted_at IS NULL ";
        /*Data array*/
        $data15 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
//            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];
        /*Data array*/
        $report_date_filter_table='gp';
        $db_query_after_where10 = '';
        $popUpArray15['title'] = $title15;
        $popUpArray15['label'] = $label15;
        $popUpArray15['field'] = $field15;
        $popUpArray15['data'] = $data15;
        $popUpArray15['report_columns'] = $report_columns15;
        $popUpArray15['alternate_report_columns'] = '';
        $popUpArray15['report_db_query'] = $report_db_query15;
        $popUpArray15['report_url'] = $report_url15;
        $popUpArray15['report_join_table'] = '';
        $popUpArray15['db_query_after_where'] = '';
        $popUpArray15['report_date_filter_table'] = $report_date_filter_table;
        /*-----------------------------------------Print Envelope For Property End -------------------------------------------------------*/

        /*------------------------------------- Print Envelope For Building Filters ---------------------------------------------------*/
        $title16 = "Print Envelope For Building Filters";
        $label16 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Building','is_required' => 'no'],
            ['name' => 'Envelope Size','is_required' => 'yes']
        ];
        $field16 =[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','id'=>'status','changed_column'=>'tenant','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'building_id','name' => 'building[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;
        $joins16 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange16 = ['table'=>'building_detail','joins'=>$joins];

        $report_url16 = '/Reporting/PrintEnvelopeReport';
        $report_columns16 = "";
        $report_db_query16 = "SELECT REPLACE(bd.building_name,' ','--') AS name, CONCAT(REPLACE(bd.address,' ','--'),'-',REPLACE(gp.address1,' ','--')) as address1,REPLACE(gp.address2,' ','--') as address2,REPLACE(gp.address3,' ','--') as address3,REPLACE(gp.address4,' ','--') as address4,concat(REPLACE(gp.city,' ','--'),',--',REPLACE(gp.state,' ','--'),'--',gp.zipcode) AS tenant_add FROM building_detail bd JOIN general_property gp ON bd.property_id = gp.id WHERE gp.deleted_at IS NULL ";
        $report_join_table16 = " ";
        /*Data array*/
        $data16 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange16],'column'=>['id','property_name'],'field_name'=>'property[]','id'=>'property'],
            ['table'=>['main'=>'building_detail'],'column'=>['id','building_name'],'field_name'=>'building[]','id'=>'building'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];
        /*Data array*/
        $report_date_filter_table='bd';

        $popUpArray16['title'] = $title16;
        $popUpArray16['label'] = $label16;
        $popUpArray16['field'] = $field16;
        $popUpArray16['data'] = $data16;
        $popUpArray16['report_columns'] = $report_columns16;
        $popUpArray16['alternate_report_columns'] = '';
        $popUpArray16['report_db_query'] = $report_db_query16;
        $popUpArray16['report_url'] = $report_url16;
        $popUpArray16['report_join_table'] = $report_join_table16;
        $popUpArray16['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------------Print Envelope For Building End -------------------------------------------------------*/

        /*-------------------------------------------------- Print Envelope For Unit Filters ---------------------------------------------------*/
        $title17 = "Print Envelope For Unit Filters";
        $label17 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Building','is_required' => 'no'],
            ['name' => 'Unit','is_required' => 'no'],
            ['name' => 'Envelope Size','is_required' => 'yes']
        ];
        $field17 =[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','id'=>'status','changed_column'=>'tenant','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'building_id','name' => 'building[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'unit_id','name' => 'unit[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;
        $joins17 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ]];
        $where17 = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange17 = ['table'=>'building_detail','joins'=>$joins17];

        $report_url17 = '/Reporting/PrintEnvelopeReport';
        $report_columns17 = "";
        $report_db_query17 = "SELECT REPLACE(gp.property_name,' ','--') AS prop_name ,REPLACE(gp.address1,' ','--') as address1, CONCAT(REPLACE(ud.unit_prefix,' ','--'),'-',REPLACE(ud.unit_no,' ','--')), REPLACE(gp.address2,' ','--') as address2, REPLACE(gp.address3,' ','--') as address3, REPLACE(gp.address4,' ','--') as address4,concat(REPLACE(gp.city,' ','--'),',--',REPLACE(gp.state,' ','--'),'--',gp.zipcode) AS tenant_add FROM unit_details ud JOIN general_property gp ON ud.property_id = gp.id JOIN building_detail bd ON ud.building_id = bd.id WHERE ud.deleted_at IS NULL";
        $report_join_table17 = " ";
        /*Data array*/
        $data17 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'0','name'=>'Active'],['id'=>'1','name'=>'InActive']]],
            ['table'=>['main'=>'general_property','onChange'=>$onChange17],'column'=>['id','property_name'],'field_name'=>'property[]','id'=>'property'],
            ['table'=>['main'=>'building_detail'],'column'=>['id','building_name'],'field_name'=>'building[]','id'=>'building'],
            ['table'=>['main'=>'unit_details'],'column'=>['id','unit_prefix','unit_no'],'field_name'=>'unit[]','id'=>'unit'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];
        $report_date_filter_table17='ud';
        /*Data array*/
        $popUpArray17['title'] = $title17;
        $popUpArray17['label'] = $label17;
        $popUpArray17['field'] = $field17;
        $popUpArray17['data'] = $data17;
        $popUpArray17['report_columns'] = $report_columns17;
        $popUpArray17['alternate_report_columns'] = '';
        $popUpArray17['report_db_query'] = $report_db_query17;
        $popUpArray17['report_url'] = $report_url17;
        $popUpArray17['report_join_table'] = $report_join_table17;
        $popUpArray17['report_date_filter_table'] = $report_date_filter_table17;
        /*--------------------------------------------------Print Envelope For Unit End -------------------------------------------------------*/


        $listName2 = [
            ['Property List' => $popUpArray0],['Unit List' => $popUpArray],['Unit Features'=>$popUpArray1],['Vacant Unit'=>''],['Property Statement'=>''],['Unpaid Bills by Property'=>''],['Property Summary'=>$popUpArray4],['Property Group'=>$popUpArray5],['Portfolio'=>$popUpArray6],['Property Vacancy'=>$popUpArray7],['Marketing'=>''],['Inspection'=>$popUpArray8],['Rental Application'=>$popUpArray9],['Inventory'=>$popUpArray10],['Lost Items'=>$popUpArray11],['Found Items'=>$popUpArray12],['Matched Items'=>$popUpArray13],['Unclaimed Items'=>$popUpArray14],['Print Envelope for Property'=>$popUpArray15],['Print Envelope for Building'=>$popUpArray16],['Print Envelope For Unit'=>$popUpArray17]];
        return $listName2;
    }

    public static function popupsVendorData(){

        /*-------------------------------------------------- Vendor List Filters Start ---------------------------------------------------*/
        $title = "Vendor List Filters";
        $label = [
            ['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
        ] ;
        $joins = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url = '/Reporting/VendorList';
        $report_columns = array('Vendor Name','Phone Number','Email','Vendor Company Name','Address','Vendor Type');

        $report_db_query = "SELECT REPLACE(u.name,' ','--') AS name , u.phone_number ,u.email, REPLACE(u.company_name,' ','--'),concat(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--'),'--',REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',REPLACE(u.zipcode,' ','--')) AS address,REPLACE(cvt.vendor_type,' ','--') FROM users u JOIN vendor_additional_detail vad on u.id=vad.vendor_id JOIN company_vendor_type cvt ON cvt.id=vad.vendor_type_id";
        $report_join_table = "";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
        ];
        $db_query_after_where = '';
        $report_date_filter_table = 'u';
        /*Data array*/
        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_join_table'] = $report_join_table;
        $popUpArray['db_query_after_where'] = $db_query_after_where;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;

        /*--------------------------------------------------Vendor List Filters End -------------------------------------------------------*/
        /*-------------------------------------------------- Vendor Insurance Filters Start -------------------------------------------------------*/
        $title1 = "Vendor Insurance Filters";
        $label1 = [
            ['name' => 'Vendor Entity/Company','is_required' => 'no'],
            ['name' => 'Vendor Name','is_required' => 'no']
        ];
        $field1=[
            ['type' => 'text','class'=>'form-control','id'=>'','name' => '','data_type_reports'=>'textData'],
            ['type' => 'select','class'=>'form-control multiSelectfield','id'=>'','name' => 'vendor','data_type_reports'=>'selectData']
        ] ;
        $joins1 = [];
        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $report_url1 = '/Reporting/VendorInsurance';
        $report_columns1 = array('Vendor Name','Company Name','Credential Name','Acquire Date','Expiration Date');
        $report_join_table1 = " ";
        $report_date_filter_table1 = "u";
        $report_db_query1 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.company_name,' ','--') AS company_name/*,REPLACE(tc.credential_name) AS credential_name*/,REPLACE(tct.credential_type,' ','--') AS credential_type,REPLACE(tc.acquire_date,' 00:00:00',''),REPLACE(tc.expire_date,' 00:00:00','') FROM users u JOIN tenant_credential tc on tc.user_id=u.id JOIN tenant_credential_type tct ON tct.id=tc.credential_type";
        /*Data array*/
        $data1 = [
            ['table'=> '','column'=>[''],'type'=>'textData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where1],'column'=>['id','name'],'field_name'=>'vendor', 'type'=>'select']
        ];
        /*Data array*/
        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['alternate_report_columns'] = '';
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = $report_join_table1;
        $popUpArray['db_query_after_where'] = "";
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*--------------------------------------------------Vendor Insurance Filters -----------------------------------------------------------*/
        /*----------------------------------------- Vendor 1099 Summary Filters Start -------------------------------------------------------*/
        $title2 = "Vendor 1099 Summary Filters";
        $label2 = [
            ['name' => 'Tax Year','is_required' => 'yes'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Vendor','is_required' => 'yes']
        ];
        $field2=[
            ['type' => 'select','class'=>'form-control','id'=>'','name' => 'start_year','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','id'=>'','name' => 'vendor[]','data_type_reports'=>'multiselect']
        ] ;
        $joins2 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where2 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $onChange2 = ['table'=>'company_property_portfolio','joins'=>$joins2];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];
        $db_query_after_where="";
        $report_url2 = '/Reporting/Vendor1099SummaryReport';
        $report_columns2 = array('Vendor','Property','State/Province','Federal Tax ID','Amount Paid for Service');
        $report_db_query2 = "SELECT REPLACE(u.name,' ','--'),REPLACE(gp.property_name,' ','--'),REPLACE(u.state,' ','--'),REPLACE(u.tax_id,' ','--'),vad.vendor_rate AS amt from owner_blacklist_vendors obv JOIN users u ON u.id=obv.vendor_id JOIN general_property gp on gp.id=obv.property_id LEFT JOIN vendor_additional_detail vad ON u.id=vad.vendor_id";
        $report_join_table2 = "";
        $report_date_filter_table2 = "u";
        /*Data array*/
        $data2 = [
            ['table'=>'','joins'=>null,'column'=>null,'field_name'=>'','type'=>'selectData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where2],'column'=>['id','name'],'field_name'=>'vendor[]', 'type'=>'multiselect'],
        ];
        /*Data array*/
        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        $popUpArray2['report_columns'] = $report_columns2;
        $popUpArray2['alternate_report_columns'] = '';
        $popUpArray2['report_db_query'] = $report_db_query2;
        $popUpArray2['report_url'] = $report_url2;
        $popUpArray2['report_join_table'] = $report_join_table2;
        $popUpArray2['db_query_after_where'] = $db_query_after_where;
        $popUpArray2['report_date_filter_table'] = $report_date_filter_table2;
        /*--------------------------------------------------Vendor 1099 Summary Filters -----------------------------------------------------------*/
        /*----------------------------------------- Vendor 1099 Detail Filters Start -------------------------------------------------------*/
        $title3  = "Vendor 1099 Detail Filters";
        $label3 = [
            ['name' => 'Tax Year','is_required' => 'yes'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Vendor','is_required' => 'yes']
        ];
        $field3=[
            ['type' => 'select','class'=>'form-control','id'=>'','name' => 'start_year','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'','name' => 'vendor[]','data_type_reports'=>'multiselect']
        ] ;
        $joins3 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]];
        $where3 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $onChange3 = ['table'=>'company_property_portfolio','joins'=>$joins3];
        $other_joins3=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $report_url3 = '/Reporting/Vendor1099DetailReport';
        $report_columns3 = array('Vendor','Property','Check Number','Date Paid','Owner Name','Address','State/Province','Tax/ID','Amount Paid');
        $report_join_table3 = "";
        $report_date_filter_table3 = "u";
        $report_db_query3 ="SELECT REPLACE(u.name,' ','--') as name,REPLACE(gp.property_name,' ','--'),'-' AS abc ,'-' AS abcd,REPLACE(u.name,' ','--') as owner_name,REPLACE(CONCAT(u.address1,'--',u.address2,'--',u.address3,'--',u.address4),' ','--'),u.state,u.tax_id,vad.vendor_rate as amt FROM users u 
                          LEFT JOIN owner_blacklist_vendors obv ON u.id=obv.vendor_id 
                          LEFT JOIN general_property gp ON gp.id=obv.property_id LEFT JOIN owner_details od ON u.id=od.user_id
                          left JOIN vendor_additional_detail vad ON u.id=vad.vendor_id";
        /*Data array*/
        $data3 = [
            ['table'=>'','joins'=>null,'column'=>null,'field_name'=>'','type'=>'selectData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where3],'column'=>['id','name'],'field_name'=>'vendor[]', 'type'=>'multiselect'],
        ];
        /*Data array*/
        $popUpArray3['title'] = $title3;
        $popUpArray3['label'] = $label3;
        $popUpArray3['field'] = $field3;
        $popUpArray3['data'] = $data3;
        $popUpArray3['report_columns'] = $report_columns3;
        $popUpArray3['alternate_report_columns'] = '';
        $popUpArray3['report_db_query'] = $report_db_query3;
        $popUpArray3['report_url'] = $report_url3;
        $popUpArray3['report_join_table'] = $report_join_table3;
        $popUpArray3['report_date_filter_table'] = $report_date_filter_table3;
        /*--------------------------------------------------Vendor 1099 Detail Filters -----------------------------------------------------------*/

        /*--------------------------------------------  vendor work order-----------------------------------------------*/

        $title3 = "Vendor Work Order Filters";
        $label3 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Vendor','is_required' => 'no'],
            ['name' => 'Work Order Number','is_required' => 'no'],
            ['name' => 'Work Order Status','is_required' => 'no'],
            ['name' => 'Tenant Name','is_required' => 'no'],
            ['name' => 'Recurring','is_required' => 'no']
        ];


        $joins3 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];
        $where3 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'tenant_lease_details',
            'column' => 'record_status',
            'condition' => '=',
            'value' => '1'
        ]];
        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],
        [
        'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $onChange3 = ['table'=>'company_property_portfolio','joins'=>$joins3];
        $other_joins=[[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ]];

        $field3=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'vendor','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control','id'=>'','name' => '','data_type_reports'=>'textData'],
            ['type' => 'select','class'=>'form-control multiSelectfield','id'=>'work_order_status','name' => 'work_order_status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control multiSelectfield','name' => '','data_type_reports'=>'selectData'],
            ['type' => 'checkbox','class'=>'multiSelectfield','name' => 'checkboxData','data_type_reports'=>'checkbox']

        ] ;

        $report_url3 = '/Reporting/Vendor_WorkOrderReport';

        $report_columns3 = array('Property','Vendor','Warranty Expiration','Work Order Number','Status','Unit','Tenant','Created On','Scheduled on','Completed On','Amount ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Recurring');

        $report_db_query3 = "SELECT REPLACE(gp.property_name,' ','--') AS prop_name,REPLACE(us.name,' ','--') AS name,wi.warranty_expiration_date,wo.work_order_number,cws.work_order_status,concat(ud.unit_prefix,'-',ud.unit_no) AS unit,REPLACE(u.name,' ','--') AS tenant_name,DATE_FORMAT(wo.created_on,'%Y-%m-%d'),DATE_FORMAT(wo.scheduled_on,'%Y-%m-%d'),DATE_FORMAT(wo.completed_on,'%Y-%m-%d'),wo.estimated_cost AS amt,'Daily' AS virtual_col   FROM general_property gp JOIN work_order wo ON wo.property_id=gp.id JOIN warranty_information wi ON wi.property_id=gp.id JOIN unit_details ud ON ud.id=wo.unit_id JOIN users u ON u.id=wo.tenant_id JOIN company_workorder_status cws ON cws.id=wo.status_id JOIN users us on us.id=wo.vendor_id";

        /*Data array*/
        $data3 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange3],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange3],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where1,'onChange'=>null],'column'=>['id','name'],'field_name'=>'vendor', 'type'=>'multiselect'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'textData'],
            ['table'=>['main' => 'company_workorder_status','joins'=>null ,'where'=>null,'onChange'=>$onChange3],'column'=>['id','work_order_status'],'type'=>'selectData', 'field_name'=>'work_order_status'],
            ['table'=>['main' => 'users','joins1'=>$other_joins ,'where'=>$where3,'onChange'=>$onChange3],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'checkbox']
        ];


        /*Data array*/

        $popUpArray0['title'] = $title3;
        $popUpArray0['label'] = $label3;
        $popUpArray0['field'] = $field3;
        $popUpArray0['data'] = $data3;

        $report_date_filter_table0 = 'wo';
        /*Data array*/
        $popUpArray0['title'] = $title3;
        $popUpArray0['label'] = $label3;
        $popUpArray0['field'] = $field3;
        $popUpArray0['data'] = $data3;
        $popUpArray0['report_columns'] = $report_columns3;
        $popUpArray0['alternate_report_columns'] = '';
        $popUpArray0['report_db_query'] = $report_db_query3;
        $popUpArray0['report_url'] = $report_url3;
        $popUpArray0['report_join_table'] = '';
        $popUpArray0['db_query_after_where'] = '';
        $popUpArray0['report_date_filter_table'] = $report_date_filter_table0;
        /*-------------------------------------------- vendor work order End-------------------------------------------*/

        /*------------------------------------------- Vendor list consolidated Filters --------------------------------------------------*/
        $title4   = "Vendor List Consolidated";
        $popUpArray4['title'] = $title4;
        /*------------------------------------------- Vendor list consolidated Filters --------------------------------------------------*/


        /*--------------------------------------------  Vendor Mailing Label Filter -----------------------------------------------*/
        $title10 = "Vendor Mailing Label Filters";
        $label10 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Mailing Label Type','is_required' => 'no']];

        $field10 =[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'mailing_label_type','data_type_reports'=>'selectData']
        ] ;

        /*Data array*/
        $report_url10 = '/Reporting/VendorMailingLabelReport';
        $report_columns10 = '';
        $report_db_query10 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u WHERE u.user_type ='3'";
        $report_join_table10 = "";
        $data10 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'mailing_label_type']
        ];
        /*Data array*/

        $db_query_after_where10 = '';
        $report_date_filter_table10 = 'u';
        $popUpArray10['title'] = $title10 ;
        $popUpArray10['label'] = $label10 ;
        $popUpArray10['field'] = $field10 ;
        $popUpArray10['data'] = $data10 ;
        $popUpArray10['report_columns'] = $report_columns10 ;
        $popUpArray10['alternate_report_columns'] = '';
        $popUpArray10['report_db_query'] = $report_db_query10 ;
        $popUpArray10['report_url'] = $report_url10 ;
        $popUpArray10['report_join_table'] = $report_join_table10 ;
        $popUpArray10['db_query_after_where'] = $db_query_after_where10 ;
        $popUpArray10['report_date_filter_table'] = $report_date_filter_table10;
        /*-------------------------------------------- Vendor Mailing Label Filter-------------------------------------------*/

        /*--------------------------------------------  Print Envelope For Vendor Filters -----------------------------------------------*/

        $title11 = "Print Envelope For Vendor";
        $label11 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Vendor','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'no']
        ];

        $field11=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','id'=>'vendor','name' => 'vendor[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;

        $where11 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $report_url11 = '/Reporting/PrintEnvelopeReport';
        $report_columns11 = '';
        $report_db_query11 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u WHERE u.user_type ='3'";
        $report_join_table11 = "tp";
        /*Data array*/
        $data11 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where11],'column'=>['id','name'],'field_name'=>'vendor[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];

        $db_query_after_where11 = '';
        $report_date_filter_table11 = 'u';
        /*Data array*/
        $popUpArray11['title'] = $title11;
        $popUpArray11['label'] = $label11;
        $popUpArray11['field'] = $field11;
        $popUpArray11['data'] = $data11;
        $popUpArray11['report_columns'] = $report_columns11;
        $popUpArray11['alternate_report_columns'] = '';
        $popUpArray11['report_db_query'] = $report_db_query11;
        $popUpArray11['report_url'] = $report_url11;
        $popUpArray11['report_join_table'] = $report_join_table11;
        $popUpArray11['db_query_after_where'] = $db_query_after_where11;
        $popUpArray11['report_date_filter_table'] = $report_date_filter_table11;
        /*-------------------------------------------- Print Envelope For Vendor Filters End-------------------------------------------*/


        $listName3 = [
            ['Vendor List'=>$popUpArray],
            ['Expense by Vendor'=>''],
            ['Vendor Bills'=>''],
            ['Transaction by Vendor'=>''],
            ['Vendor Work Order'=>$popUpArray0],
            ['Vendor Insurance'=>$popUpArray1],
            ['Vendor 1099 Summary'=>$popUpArray2],
            ['Vendor 1099 Detail'=>$popUpArray3],
            ['Vendor List Consolidated '=>$popUpArray4],
            ['Vendor Mailing Label'=>$popUpArray10],
            ['Print Envelope For Vendor'=>$popUpArray11]
        ];
        return $listName3;
    }

    public static function popupsReportInventory()
    {
        $popUpArray=[];
        $popUpArray1=[];
        $popUpArray2=[];


        /*--------------------------------------------  Inventory Summary Filters Starts ---------------------------------------*/

        $title = "Inventory Summary Filters";
        $label = [
            ['name' => 'Inventory Item Status','is_required' => 'yes'],
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
        ];
        $field=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'start_date','id' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'end_date','id' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect'],
        ];

        $joins = [ ];

        $onChange = ['table'=>'company_property_portfolio','joins'=>$joins];

        $data = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive'],['id'=>'all','name'=>'All']]],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>''],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];

        $report_url = '/Reporting/InventorySummary';
        $report_columns = array('Category','Sub Category','Brand','Supplier','Property','Building','Inventory Sublocation');
        $report_db_query = "select REPLACE(cms.category,' ','--') as name,REPLACE(cmss.sub_category,' ','--'),REPLACE(cmbb.brand,' ','--'),REPLACE(cmas.supplier,' ','--'),REPLACE(gp.property_name,' ','--') AS prop_name,REPLACE(bd.building_name,' ','--'), REPLACE(mis.sublocation,' ','--')  FROM `maintenance_inventory` as mi inner join general_property as gp on gp.id=mi.property_id inner join building_detail as bd on bd.id=mi.building_id inner join maintenance_inventory_sublocation as mis on mis.id=mi.sub_location_id inner join company_maintenance_subcategory as cms on cms.id=mi.category_id inner join company_maintenance_subcategory as cmss on cmss.id=mi.sub_category_id INNER JOIN company_maintenance_brand as cmbb on mi.id=cmbb.id INNER JOIN company_maintenance_supplier cmas on cmas.id=mi.supplier_id";
        $db_query_after_where = '';
        $report_date_filter_table = 'mi';

        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_join_table'] = '';
        $popUpArray['db_query_after_where'] = $db_query_after_where;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Inventory Summary Filters End ----------------------------------*/
        /*--------------------------------------------  Inventory Detail Filters Starts ---------------------------------------*/
        $title1 = "Inventory Detail Filters";
        $label1 = [
          /*  ['name' => 'Status','is_required' => 'yes'],*/
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],

        ];

        $field1=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'start_date','id' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'end_date','id' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ];

        $joins1 = [
            [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ]
        ];

        $onChange1 = ['table'=>'company_property_portfolio','joins'=>$joins1];
        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data1 = [
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>$onChange1],'column'=>['id','property_name'],'field_name'=>'property[]']

        ];

        $report_url1 = '/Reporting/InventoryDetails';
        $report_columns1 = array('Item #','Category','Sub Category','Description','Brand','Supplier','Quantity In Stock','Building','Inventory SubLocation','Quantity on order','Per unit cost ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Total cost('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query1 = "SELECT REPLACE(mi.description,' ','--') AS descr, REPLACE(cms.category,' ','--') AS name, REPLACE(cms.sub_category,' ','--') AS subcategory, REPLACE(mi.description,' ','--') AS descr, REPLACE(cms.brand,' ','--') AS brand, REPLACE(cms.supplier,' ','--') AS supplierData, REPLACE(stock_reorder_level,' ','--') AS stock_reorder_level, REPLACE(bd.building_name,' ','--'), REPLACE(mis.sublocation,' ','--') AS sublocation, REPLACE(item_purchased,' ','--') AS item_purchased, REPLACE(cost_per_item,' ','--'), REPLACE(purchase_cost, ' ','--') as amt,'-' as virtual_column FROM maintenance_inventory mi LEFT JOIN company_maintenance_subcategory cms ON cms.parent_id = mi.category_id LEFT JOIN building_detail bd ON mi.building_id=bd.id LEFT JOIN maintenance_inventory_sublocation mis ON mi.sub_location_id=mis.id LEFT JOIN general_property gp ON gp.id=mi.property_id WHERE mi.deleted_at IS NULL ";
        $report_date_filter_table='gp';
        /*Data array*/
        $db_query_after_where = '';
        /*Data array*/
        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = '';
        $popUpArray1['db_query_after_where'] = '';
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Inventory Detail Filters End ----------------------------------*/

        /*--------------------------------------------  Damaged Inventory Filters Starts ---------------------------------------*/
        $title2 = "Damaged Inventory Filters";
        $label2 = [
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],

        ];

        $field2=[
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data2 = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],

        ];

        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        /*--------------------------------------------Damaged Inventory Filters End ----------------------------------*/

        /*--------------------------------------------  Inventory Transfer Filters Starts ---------------------------------------*/
        $title3 = "Inventory Transfer Filters";
        $label3 = [
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],

        ];

        $field3=[
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data3 = [
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],

        ];

        $popUpArray3['title'] = $title3;
        $popUpArray3['label'] = $label3;
        $popUpArray3['field'] = $field3;
        $popUpArray3['data'] = $data3;
        /*--------------------------------------------Inventory Transfer Filters End ----------------------------------*/

        /*--------------------------------------------  Stock Reordering Filters Starts ---------------------------------------*/
        $title4 = "Stock Reordering Filters";
        $label4 = [
            ['name' => 'Inventory Item Status','is_required' => 'yes'],
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],

        ];

        $field4=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'start_date','id' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','name' => 'end_date','id' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'property_id','name' => 'property[]','data_type_reports'=>'multiselect']
        ];

        /*Data array*/
        $data4 = [
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>''],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive'],['id'=>'all','name'=>'All']]],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['table'=>['main'=>'general_property','onChange'=>''],'column'=>['id','property_name'],'field_name'=>'property[]']
        ];

        $report_url4 = '/Reporting/InventoryReordering';
        $report_columns4 = array('Item #/Code','Category','Sub Category','Description','Brand','Supplier','Quantity In Stock','Order Date','Expected Delivery Date','Ordered By','Per unit cost ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Total cost('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
//        $report_db_query4 = "SELECT REPLACE(cms.category,' ','--') as name,REPLACE(cmss.sub_category,' ','--'),REPLACE(cmbb.brand,' ','--'),REPLACE(cmas.supplier,' ','--'),REPLACE(gp.property_name,' ','--'),REPLACE(bd.building_name,' ','--'), REPLACE(mis.sublocation,' ','--') FROM `maintenance_inventory` as mi inner join general_property as gp on gp.id=mi.property_id inner join building_detail as bd on bd.id=mi.building_id inner join maintenance_inventory_sublocation as mis on mis.id=mi.sub_location_id inner join company_maintenance_subcategory as cms on cms.id=mi.category_id inner join company_maintenance_subcategory as cmss on cmss.id=mi.sub_category_id INNER JOIN company_maintenance_brand as cmbb on mi.id=cmbb.id INNER JOIN company_maintenance_supplier cmas on cmas.id=mi.supplier_id";
        $report_db_query4 = "SELECT GROUP_CONCAT(mic.code) AS item, REPLACE(cmc.category,' ','--') AS name, REPLACE(cms.sub_category,' ','--') AS subcategory, REPLACE(mi.description,' ','--') AS descr, REPLACE(cmb.brand,' ','--') AS brand, REPLACE(cmsp.supplier,' ','--') AS supplierData, REPLACE(stock_reorder_level,' ','--') AS stock_reorder_level, mi.purchase_date, mi.expected_deliver_date, REPLACE(u.name,' ','--') AS userName, REPLACE(cost_per_item,' ','--') AS amt, REPLACE(purchase_cost, ' ','--')  AS amt1 FROM maintenance_inventory mi LEFT JOIN company_maintenance_subcategory cms ON cms.parent_id = mi.category_id LEFT JOIN company_maintenance_subcategory cmc ON cmc.id = mi.category_id JOIN company_maintenance_brand cmb ON cmb.id = mi.brand_id LEFT JOIN building_detail bd ON mi.building_id=bd.id LEFT JOIN maintenance_inventory_sublocation mis ON mi.sub_location_id=mis.id JOIN users u ON mi.user_id=u.id LEFT JOIN general_property gp ON gp.id=mi.property_id LEFT JOIN maintenance_inventory_item_code AS mic ON mic.inventory_id = mi.id  LEFT JOIN company_maintenance_supplier AS cmsp ON cmsp.id = mi.supplier_id ";
        $report_date_filter_table='mi';
        $db_query_after_where4=' GROUP BY mic.inventory_id';
        $popUpArray4['title'] = $title4;
        $popUpArray4['label'] = $label4;
        $popUpArray4['field'] = $field4;
        $popUpArray4['data'] = $data4;
        $popUpArray4['report_columns'] = $report_columns4;
        $popUpArray4['report_db_query'] = $report_db_query4;
        $popUpArray4['report_url'] = $report_url4;
        $popUpArray4['report_join_table'] = '';
        $popUpArray4['db_query_after_where'] = $db_query_after_where4;
        $popUpArray4['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Stock Reordering Filters End ----------------------------------*/

        /*--------------------------------------------  Suppliers Purchase Filters Starts ---------------------------------------*/
        $title5 = "Suppliers Purchase Filters";
        $label5 = [
            ['name' => 'Status','is_required' => 'yes'],
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],

        ];

        $field5=[
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data5 = [
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],

        ];
        $report_columns5=array('Supplier','Category','Sub Category','Description','Brand','Quantity Purchased','Purchased Date','Delivered Date','Price Per Unit','Total Price','Property','Ordered By');
        $report_db_query5="SELECT cms.supplier, mc.category_name,cmsu.sub_category,mi.description,cmb.brand,mi.item_purchased,mi.purchase_date, mi.expected_deliver_date,mi.cost_per_item,mi.purchase_cost,REPLACE(gp.property_name,' ','--'),u.name FROM maintenance_inventory mi LEFT JOIN company_maintenance_supplier cms ON cms.category_id=mi.category_id LEFT JOIN maintenance_category mc ON mc.id=mi.category_id LEFT JOIN company_maintenance_subcategory cmsu ON cmsu.parent_id=mi.sub_category_id LEFT JOIN company_maintenance_brand cmb ON cmb.category_id=mi.category_id LEFT JOIN general_property gp ON mi.property_id=gp.property_name LEFT JOIN users u ON u.id=mi.user_id LEFT JOIN general_property gp ON gp.id=mi.property_id";
        $report_url5='/Reporting/InventorySupplier_purchase';
        $report_join_table5="";
        $db_query_after_where5='';
        $report_date_filter_table='gp';

        $popUpArray5['title'] = $title5;
        $popUpArray5['label'] = $label5;
        $popUpArray5['field'] = $field5;
        $popUpArray5['data'] = $data5;
        $popUpArray5['report_columns'] = $report_columns5;
        $popUpArray5['alternate_report_columns'] = '';
        $popUpArray5['report_db_query'] = $report_db_query5;
        $popUpArray5['report_url'] = $report_url5;
        $popUpArray5['report_join_table'] = $report_join_table5;
        $popUpArray5['db_query_after_where'] = $db_query_after_where5;
        $popUpArray5['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Suppliers Purchase  Filters End ----------------------------------*/


        $listName8=[];
        $listName8 = [['Inventory Summary' => $popUpArray],['Inventory Detail' => $popUpArray1],['Damaged Inventory' => $popUpArray2],
            ['Inventory Transfer ' => ''],['Stock Reordering' => $popUpArray4],['Suppliers Purchase' => ' ']];
        return $listName8;
    }

    public static function popupsReportContact()
    {
        $popUpArray1=[];


        /*-------------------------------------------- Contact Report Starts---------------------------------------*/
        $title1 = "Print Envelope For Contact Filters";
        $label1 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Contact','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'no']
        ];

        $field1=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'contact','name' => 'contact[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;

        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '5'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $report_url = '/Reporting/PrintEnvelopeReport';
        $report_columns1 = '';
        $report_db_query1 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',REPLACE(u.zipcode,' ','--')) AS tenant_add FROM users u WHERE u.user_type ='5'";
        $report_join_table1 = "tp";
        /*Data array*/
        $data1 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where1],'column'=>['id','name'],'field_name'=>'contact[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];

        $db_query_after_where1 = '';
        $report_date_filter_table1 = 'u';
        /*Data array*/
        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['alternate_report_columns'] = '';
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url;
        $popUpArray1['report_join_table'] = $report_join_table1;
        $popUpArray1['db_query_after_where'] = $db_query_after_where1;
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*--------------------------------------------  Contact Report End ----------------------------------*/


        $listName8=[];
        $listName8 = [['Print Envelope For Contact' => $popUpArray1]];
        return $listName8;
    }

    public static function popupsReportGuestCard()
    {
        $popUpArray=[];


        /*--------------------------------------------  Guest Card Report Starts ---------------------------------------*/
        $title = "Print Envelope For Guest Card";
        $label = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Contact','is_required' => 'no'],
            ['name' => 'Envelope Size','is_required' => 'yes'],

        ];

        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'contact[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control multiSelectfield ','name' => 'envelope','data_type_reports'=>'selectData'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
        ];

        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        /*--------------------------------------------Guest Card Report End ----------------------------------*/

        $listName=[];
        $listName = [['Print Envelope For Guest Card' => '']];
        return $listName;

    }


    public static function popupsReportEmployeeVehicle()
    {
        $popUpArray=[];


        /*--------------------------------------------  Employee/Vehicle Report Starts ---------------------------------------*/
        $title = "Employee / Company Vehicles Filters";
        $label = [
            ['name' => 'Registration From','is_required' => 'no'],
            ['name' => 'Registration To','is_required' => 'no'],
            ['name' => 'Vehicle Name','is_required' => 'no'],
            ['name' => 'Vehicle Color','is_required' => 'no'],
            ['name' => 'Owner Type','is_required' => 'no'],

        ];

        $field=[
            ['type' => 'select','class'=>'form-control datefield ','id'=>'start_year','name' => 'start_year','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control datefield ','id'=>'last_year','name' => 'last_year','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control selectfield onchange','name' => 'vehicle_name[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'vehicle_color[]','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield ','name' => 'owner_type[]','data_type_reports'=>'multiselect'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'company_manage_vehicle','joins'=>null ,'where'=>null],'column'=>['id','user_id','vehicle_name'],'field_name'=>'vehicle_name[]','type'=>'selectData',],
            ['table'=>['main' => 'company_manage_vehicle','joins'=>null ,'where'=>null],'column'=>['id','user_id','color'],'field_name'=>'vehicle_color[]', 'type'=>'multiselect'],
            ['table'=>['main'=>'users','joins'=>null ,'where'=>null],'column'=>['id','name'],'id'=>'owner_type','field_name'=>'owner_type[]'],
        ];
        $report_url = '/Reporting/EmployeeCompanyVehicleReport';
        $report_columns = array('Owner Name','Owner type','Vehicle Name','Vehicle #','Make','VIN #','Registration #','License Plate #','Color','Yr of Vehicle','Mileage');
        $report_db_query = "SELECT REPLACE(u.name,' ','--'), u.user_type as vehicle_user_type,cmv.vehicle_name,cmv.vehicle,cmv.make,cmv.vin,cmv.registration,cmv.plate_number,cmv.color,cmv.year_of_vehicle,cmv.starting_mileage 
                             FROM company_manage_vehicle cmv LEFT JOIN users u ON u.id=cmv.user_id";
        $report_join_table = "";
        $db_query_after_where1 = '';
        $report_date_filter_table='';

        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_join_table'] = $report_join_table;
        $popUpArray['db_query_after_where'] = $db_query_after_where1;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Employee/Vehicle Report End ----------------------------------*/

        $listName=[];
        $listName = [['Employee  Company Vehicles' => $popUpArray]];
        return $listName;

    }

    public static function popupsReportEmployee()
    {
        $popUpArray=[];
        $popUpArray1=[];
        $popUpArray2=[];
        $popUpArray3=[];
        $popUpArray4=[];


        /*--------------------------------------------  Employee List Filters Starts ---------------------------------------*/
        $title = "Employee List Filters";
        $label = [

            ['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],


        ];

        $field=[

            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],


        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data = [

            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],


        ];
        $report_columns=array('Employee Name','Phone Number','Email','Address');
        $report_url='/Reporting/EmployeeListReport';
        $report_join_table='';
        $report_db_query="SELECT REPLACE(u.name,' ','--') AS name,phone_number,email,CONCAT(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--')) AS address FROM users u WHERE user_type='8'";
        $report_date_filter_table='u';

        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_join_table'] = $report_join_table;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*--------------------------------------------Employee List Filters End ----------------------------------*/
        /*--------------------------------------------  Employee EEO Filters Starts ---------------------------------------*/
        $title1 = " Employee EEO Filters";
        $label1 = [

            ['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],


        ];

        $field1=[

            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data1 = [

            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>null,'onChange'=>'$onChange'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'tenant','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],


        ];
        $report_columns1=array('Employee Name','Employee Address','Sex','Birthday','Ethnicity','Veteran Status','Marital Status');
        $report_url1='/Reporting/EmployeeEEOReport';
        $report_join_table1='';
        $report_db_query1="SELECT REPLACE(u.name,' ','--') AS name , CONCAT(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--')) AS address,if(u.gender = 1, 'Male', 'Female'),u.dob,REPLACE(te.title,' ','--') AS title,REPLACE(tvs.veteran,' ','--') AS veteran,REPLACE(tms.marital,' ','--') AS martial FROM users u JOIN tenant_ethnicity te on u.ethnicity=te.id JOIN tenant_veteran_status tvs ON u.veteran_status=tvs.id JOIN tenant_marital_status tms ON u.maritial_status=tms.id WHERE u.user_type='8'";
        $report_date_filter_table1 ='u';

        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = $report_join_table1;
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*--------------------------------------------Employee EEO Filters End ----------------------------------*/

        /*-------------------------------------------- Payment Plan Consolidated Filters Starts ---------------------------------------*/
        $title2 = "Payment Plan Consolidated Filters";
        $label2 = [
            ['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
        ];

        $field2=[

            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control multiSelectfield onchange','name' => 'property[]','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],

        ] ;

        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '2'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $data2 = [

            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],

        ];

        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        /*--------------------------------------------Payment Plan Consolidated Filters End ----------------------------------*/

        /*--------------------------------------------  Employee Mailing Label Filters Starts ---------------------------------------*/

        $title3 = "Employee Mailing Label Filters";
        $label3 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Mailing Label Type','is_required' => 'no']];

        $field3=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'mailing_label_type','data_type_reports'=>'selectData']
        ] ;

        /*Data array*/
        $report_url3 = '/Reporting/EmployeeMailingLabelReport';
        $report_columns3 = '';
        $report_db_query3 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u WHERE u.user_type ='8'";
        $report_join_table3 = "";
        $data3 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'mailing_label_type']
        ];
        /*Data array*/

        $db_query_after_where3 = '';
        $report_date_filter_table3 = 'u';
        $popUpArray3['title'] = $title3;
        $popUpArray3['label'] = $label3;
        $popUpArray3['field'] = $field3;
        $popUpArray3['data'] = $data3;
        $popUpArray3['report_columns'] = $report_columns3;
        $popUpArray3['alternate_report_columns'] = '';
        $popUpArray3['report_db_query'] = $report_db_query3;
        $popUpArray3['report_url'] = $report_url3;
        $popUpArray3['report_join_table'] = $report_join_table3;
        $popUpArray3['db_query_after_where'] = $db_query_after_where3;
        $popUpArray3['report_date_filter_table'] = $report_date_filter_table3;
        /*--------------------------------------------Employee Mailing Label Filters End ----------------------------------*/

        /*--------------------------------------------  Print Envelope for Employee Filters Starts ---------------------------------------*/
        $title4 = "Print Envelope For Employee Filters";
        $label4 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Employee','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'no']
        ];

        $field4=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','id'=>'vendor','name' => 'vendor[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;

        $where4 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '8'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        /*Data array*/
        $report_url4 = '/Reporting/PrintEnvelopeReport';
        $report_columns4 = '';
        $report_db_query4 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u WHERE u.user_type ='8'";
        $report_join_table4 = "tp";
        /*Data array*/
        $data4 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
//            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where4],'column'=>['id','name'],'field_name'=>'vendor[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];

        $db_query_after_where4 = '';
        $report_date_filter_table4 = 'u';
        /*Data array*/
        $popUpArray4['title'] = $title4;
        $popUpArray4['label'] = $label4;
        $popUpArray4['field'] = $field4;
        $popUpArray4['data'] = $data4;
        $popUpArray4['report_columns'] = $report_columns4;
        $popUpArray4['alternate_report_columns'] = '';
        $popUpArray4['report_db_query'] = $report_db_query4;
        $popUpArray4['report_url'] = $report_url4;
        $popUpArray4['report_join_table'] = $report_join_table4;
        $popUpArray4['db_query_after_where'] = $db_query_after_where4;
        $popUpArray4['report_date_filter_table'] = $report_date_filter_table4;
        /*--------------------------------------------Print Envelope for Employee Filters End ----------------------------------*/

        $listName=[];
        $listName = [
            ['Employee List' => $popUpArray],
            ['Employee EEO' => $popUpArray1],
            ['Payment Plan Consolidated' => $popUpArray2],
            ['Employee Mailing Label ' => $popUpArray3],
            ['Print Envelope for Employee' => $popUpArray4]
        ];
        return $listName;
    }

    public static function getOwnerPopUpDataArray(){
        $popUpArray=[];
        $popUpArray1=[];
        $popUpArray2=[];
        $popUpArray3=[];
        $popUpArray4=[];
        $popUpArray5=[];
        $popUpArray6=[];
        $popUpArray7=[];
        $popUpArray8=[];
        $popUpArray9=[];
        $popUpArray10=[];

        /*------------------------------------------- Property List Filters start --------------------------------------------------*/
        $title = "Property List Filters";
        $label = [
            ['name' => 'Date','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Owner','is_required' => 'no']
        ];
        $field=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'current_date','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','id'=>'owner_id','name' => 'owner[]','data_type_reports'=>'multiselect'],
        ] ;

        $joins = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'users',
            'column' => 'status',
            'condition' => '=',
            'value' => '1'
        ]
        ];
        $whereOther = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]
        ];

        $onChange = ['table'=>'users', 'joins'=>$joins];

        $report_url = '/Reporting/PropertyOwnerListingReport';
        $report_columns = array('Property Name','Property Address','Property Owner Name');
        $report_db_query = "SELECT REPLACE(gp.property_name,' ','--') AS prop_name, CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address , GROUP_CONCAT(REPLACE(u.name, ' ', '--'), '--','(',opo.property_percent_owned,'.00%)') AS name FROM owner_property_owned opo JOIN general_property gp ON opo.property_id = gp.id JOIN users u ON opo.user_id = u.id WHERE opo.user_id";
        $db_query_after_where = " GROUP BY gp.property_name";

        $report_join_table = "tp";
        /*Data array*/
        $data = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'users','joins'=>null ,'whereOther'=>$whereOther,'onChange'=>$onChange],'column'=>['status'],'field_name'=>'status','changed_column'=>'owner_id','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main' => 'users','joins1'=>'' ,'where'=>$where,'onChange'=>''],'column'=>['id','name'],'field_name'=>'owner[]', 'type'=>'multiselect'],
        ];
        $report_date_filter_table = 'u';
        /*Data array*/
        $popUpArray['title'] = $title;
        $popUpArray['label'] = $label;
        $popUpArray['field'] = $field;
        $popUpArray['data'] = $data;
        $popUpArray['report_columns'] = $report_columns;
        $popUpArray['alternate_report_columns'] = '';
        $popUpArray['report_db_query'] = $report_db_query;
        $popUpArray['report_url'] = $report_url;
        $popUpArray['report_join_table'] = $report_join_table;
        $popUpArray['db_query_after_where'] = $db_query_after_where;
        $popUpArray['report_date_filter_table'] = $report_date_filter_table;
        /*------------------------------------------- Property List Filters End --------------------------------------------------------*/

        /*------------------------------------------- Unit List Filters Filters --------------------------------------------------*/
        $title1 = "Unit List Filters";
        $label1 = [['name' => 'Date','is_required' => 'yes'],['name' => 'Portfolio','is_required' => 'yes'],['name' => 'Property','is_required' => 'yes'],['name' => 'Status','is_required' => 'no']];
        $field1=[
            ['type' => 'text','class'=>'form-control textfield','name' => 'current_date','data_type_reports'=>'datepicker'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio_id','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','id'=>'property','name' => 'property[]','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','id'=>'status','name' => 'status','data_type_reports'=>'selectData'],
        ] ;

        $joins1 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'tenant_property',
            'as' => 'tenant_property'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ], [
            'type' => 'LEFT',
            'table' => 'tenant_property',
            'column' => 'user_id',
            'primary' => 'user_id',
            'on_table' => 'tenant_lease_details',
            'as' => 'tenant_lease_details'
        ] ];

        $onChange1 = ['table'=>'company_property_portfolio','joins'=>$joins1];
        $data1 = [
            ['table'=> '',' '=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio', 'onChange'=>$onChange1],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'whereOther'=>$whereOther,'onChange'=>''],'column'=>['status'],'field_name'=>'status','changed_column'=>'owner_id','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]]
        ];
        $db_query_after_where1='';
        $report_date_filter_table1 = 'ud';
        $report_join_table1='';
        $report_url1 = '/Reporting/UnitListReport';
        $report_columns1 = array('Property','Unit Number','List Date','Delist Date','Available Date','Bedrooms','Bathrooms','Size','Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query1 = "SELECT  REPLACE(gp.property_name,' ','--') AS name,ud.unit_no,
                             DATE_FORMAT(ud.updated_at, '%Y-%m-%d') as list_date, DATE_FORMAT(ud.updated_at,'%Y-%m-%d') as delist_date,DATE_FORMAT(ud.created_at,'%Y-%m-%d') as available_date,ud.bedrooms_no,
                             ud.bathrooms_no,ud.square_ft,ud.market_rent as amt FROM unit_details ud
                             JOIN general_property gp ON ud.property_id=gp.id WHERE ud.property_id";


        $popUpArray1['title'] = $title1;
        $popUpArray1['label'] = $label1;
        $popUpArray1['field'] = $field1;
        $popUpArray1['data'] = $data1;
        $popUpArray1['report_columns'] = $report_columns1;
        $popUpArray1['alternate_report_columns'] = '';
        $popUpArray1['report_db_query'] = $report_db_query1;
        $popUpArray1['report_url'] = $report_url1;
        $popUpArray1['report_join_table'] = $report_join_table1;
        $popUpArray1['db_query_after_where'] = $db_query_after_where1;
        $popUpArray1['report_date_filter_table'] = $report_date_filter_table1;
        /*------------------------------------------- Unit List Filters Filters --------------------------------------------------*/


        /*------------------------------------------- Vacant Unit Filters start--------------------------------------------------*/
        $title2 = "Vacant Unit Filters";
        $label2 = [['name' => 'Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],['name' => 'Owner','is_required' => 'no']];
        $field2=[
            ['type' => 'text','class'=>'form-control datefield','name' => 'current_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'tenant[]','id' => 'tenant','data_type_reports'=>'multiselect'],

        ] ;
        $where1 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $data2 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],

            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where1],'column'=>['id','name'],'field_name'=>'tenant[]', 'type'=>'multiselect'],

        ];
        $popUpArray2['title'] = $title2;
        $popUpArray2['label'] = $label2;
        $popUpArray2['field'] = $field2;
        $popUpArray2['data'] = $data2;
        /*------------------------------------------- Vacant Unit Filters end--------------------------------------------------*/

        /*--------------------------------------------  Owner Withholdings Filters Filters -----------------------------------------------*/
        $title3 = "Owner Withholdings Filters";
        $label3 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => '% Ownership','is_required' => 'no'],
            ['name' => 'Owner Taxpayer Name','is_required' => 'no']];

        $field3=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio_id','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property_id','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onChangeStatus','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'text','class'=>'form-control','name' => 'checkboxData','data_type_reports'=>'text'],
            ['type' => 'multiselect','class'=>'form-control selectfield','name' => 'taxpayer_name[]','data_type_reports'=>'text'],

        ] ;

        $joins3 = [[
            'type' => 'LEFT',
            'table' => 'company_property_portfolio',
            'column' => 'id',
            'primary' => 'portfolio_id',
            'on_table' => 'general_property',
            'as' => 'general_property'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'owner_property_owned',
            'as' => 'owner_property_owned'
        ], [
            'type' => 'LEFT',
            'table' => 'owner_property_owned',
            'column' => 'user_id',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ]];
        $where3 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ],[
            'table' => 'users',
            'column' => 'status',
            'condition' => '=',
            'value' => '1'
        ]
        ];
        $whereOther3 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]
        ];

        $onChange3 = ['table'=>'company_property_portfolio', 'joins'=>$joins3];


        /*Data array*/
        $data3 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio','onChange'=>$onChange3],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'whereOther'=>$whereOther3,'where'=>null,'onChange'=>$onChange3],'column'=>['status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'text'],
            ['table'=>['main'=>'users','joins'=>null,'where'=>$where3],'column'=>['id','tax_payer_name'],'field_name'=>'taxpayer_name[]','type'=>'selectData'],
        ];

        $db_query_after_where3='';
        $report_date_filter_table3='u';
        $report_join_table3='';
        $report_url3 = '/Reporting/Owner_WithHoldingReport';
        $report_columns3 = array('Property','Owner Name','State','Rent ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Less Management Fees ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Net Amount ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Percent Ownership','Amount Withheld ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query3 = "SELECT REPLACE(gp.property_name,' ','--') as prop_name,REPLACE(u.name,' ','--') AS name,u.state,ud.base_rent AS amt,'-' AS virtual_column,ud.market_rent AS amt1,opo.property_percent_owned,'-' AS virtual_column1 FROM users u JOIN owner_property_owned opo ON u.id=opo.user_id JOIN general_property gp on gp.id=opo.property_id JOIN unit_details ud on gp.id=ud.property_id WHERE u.deleted_at IS NULL ";
        /*Data array*/

        $popUpArray3['title'] = $title3;
        $popUpArray3['label'] = $label3;
        $popUpArray3['field'] = $field3;
        $popUpArray3['data'] = $data3;
        $popUpArray3['report_columns'] = $report_columns3;
        $popUpArray3['alternate_report_columns'] = '';
        $popUpArray3['report_db_query'] = $report_db_query3;
        $popUpArray3['report_url'] = $report_url3;
        $popUpArray3['report_join_table'] = $report_join_table3;
        $popUpArray3['db_query_after_where'] = $db_query_after_where3;
        $popUpArray3['report_date_filter_table'] = $report_date_filter_table3;
        /*-------------------------------------------- Owner Withholdings Filters End-------------------------------------------*/

        /*-------------------------------------------- Owner Statement Filters --------------------------------------------------------*/
        $title4 = "Owner Statement Filters";
        $label4 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],

            ['name' => 'Owner Taxpayer Name','is_required' => 'no']];

        $field4=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],

            ['type' => 'select','class'=>'form-control selectfield','name' => 'owner','data_type_reports'=>'text'],

        ] ;

        /*Data array*/
        $data4 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_lease_details','joins'=>null ,'where'=>null,'onChange'=>$onChange],'column'=>['record_status'],'field_name'=>'status','type'=>'selectData','defaultData'=>['default','0','1']],

            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'text']
        ];
        /*Data array*/

        $popUpArray4['title'] = $title4;
        $popUpArray4['label'] = $label4;
        $popUpArray4['field'] = $field4;
        $popUpArray4['data'] = $data4;
        /*--------------------------------------------  Owner Statement Filters End------------------------------------------------------*/
        /*-------------------------------------------- Owner Work Orders Filters--------------------------------------------------------*/
        $title5 = "Owner Work Orders Filters";
        $label5 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Vendor Type','is_required' => 'no'],
            ['name' => 'Vendor Name','is_required' => 'no'],
            ['name' => 'Work Order Category','is_required' => 'no']];

        $field5=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'vendor_type','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield onchange','name' => 'vendor','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield ','name' => 'work_category','data_type_reports'=>'selectData'],

        ] ;

        $where5 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '3'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];
        /*Data array*/
        $data5 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main'=>'company_vendor_type','joins'=>null,'where'=>null],'column'=>['id','vendor_type'],'field_name'=>'vendor_type','type'=>'selectData'],
            ['table'=>['main'=>'users','joins'=>null,'where'=>$where5],'column'=>['id','name'],'field_name'=>'vendor','type'=>'selectData'],
            ['table'=>['main'=>'company_workorder_category','joins'=>null,'where'=>null],'column'=>['id','category'],'field_name'=>'work_category','type'=>'selectData'],
        ];
        /*Data array*/
        $db_query_after_where5='';
        $report_join_table5='';
        $report_url5 = '/Reporting/Owner_OpenWorkOrderReport';
        $report_columns5 = array('Property','Warranty Expiration','Work Order Number','Description','Status','Vendor','Unit','Tenant','Created On','Scheduled On','Completed On','Amount ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')','Recurring');
        $report_db_query5 = "SELECT REPLACE (gp.property_name,' ','--') AS name ,wi.warranty_expiration_date,wo.work_order_number,wo.work_order_description,cws.work_order_status,REPLACE(u.name,' ','--') AS vendor_name,CONCAT(REPLACE(ud.unit_prefix,' ','--'),'-',ud.unit_no),REPLACE(ut.name,' ','--') AS tenant_name,
                                                DATE_FORMAT(wo.created_on,'%Y-%m-%d'),wo.scheduled_on,wo.completed_on,tld.rent_amount AS amt,wo.rec_month FROM work_order wo JOIN general_property gp on gp.id=wo.property_id JOIN users u ON u.id=wo.vendor_id JOIN unit_details ud on ud.id=wo.unit_id JOIN users ut ON ut.id=wo.tenant_id JOIN warranty_information wi ON gp.id=wi.property_id JOIN company_workorder_status cws on wo.status_id=cws.id JOIN tenant_lease_details tld ON tld.user_id=wo.tenant_id";

        $report_date_filter_table5 = 'wo';
        $popUpArray5['title'] = $title5;
        $popUpArray5['label'] = $label5;
        $popUpArray5['field'] = $field5;
        $popUpArray5['data'] = $data5;
        $popUpArray5['report_columns'] = $report_columns5;
        $popUpArray5['alternate_report_columns'] = '';
        $popUpArray5['report_db_query'] = $report_db_query5;
        $popUpArray5['report_url'] = $report_url5;
        $popUpArray5['report_join_table'] = $report_join_table5;
        $popUpArray5['db_query_after_where'] = $db_query_after_where5;
        $popUpArray5['report_date_filter_table'] = $report_date_filter_table5;
        /*-------------------------------------------- Owner Work Orders Filter-----------------------------------------------------*/

        /*-------------------------------------------- Owner 1099 Summary Filters --------------------------------------------------------*/
        $title6 = "Owner 1099 Summary Filters";
        $label6 = [['name' => 'Tax Year','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Owner Name','is_required' => 'no'],
         ];

        $field6=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'start_year','name' => 'start_year','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'owner[]','id' => 'owner','data_type_reports'=>'multiselect'],

        ] ;

        $where6 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];
        /*Data array*/
        $data6 = [
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'users','joins'=>null,'where'=>$where6],'column'=>['id','name'],'field_name'=>'owner[]','type'=>'selectData'],
        ];

        $db_query_after_where6='';
        $report_join_table6='';
        $report_url6 = '/Reporting/Owner1099SummaryReport';
        $report_columns6 = array('Property','Owner Name','State/Province','Federal Tax ID','Income ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query6 = "SELECT REPLACE(gp.property_name,' ','--') AS prop_name, REPLACE(u.name,' ','--'), REPLACE(u.state,' ','--'), REPLACE(u.tax_payer_id,' ','--') ,'-' AS virtual_column FROM users u JOIN owner_property_owned opo ON u.id = opo.user_id JOIN general_property gp ON gp.id = opo.property_id";

        /*Data array*/

        $report_date_filter_table6 = 'u';
        $popUpArray6['title'] = $title6;
        $popUpArray6['label'] = $label6;
        $popUpArray6['field'] = $field6;
        $popUpArray6['data'] = $data6;
        $popUpArray6['report_columns'] = $report_columns6;
        $popUpArray6['alternate_report_columns'] = '';
        $popUpArray6['report_db_query'] = $report_db_query6;
        $popUpArray6['report_url'] = $report_url6;
        $popUpArray6['report_join_table'] = $report_join_table6;
        $popUpArray6['db_query_after_where'] = $db_query_after_where6;
        $popUpArray6['report_date_filter_table'] = $report_date_filter_table6;
        /*-------------------------------------------- Owner 1099 Summary Filters End -----------------------------------------------------*/

        /*-------------------------------------------- Owner 1099 Detail Filters Starts -------------------------------------------*/
        $title7 = "Owner 1099 Detail Filters";
        $label7 = [['name' => 'Tax Year','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'yes'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Owner Name','is_required' => 'no'],
        ];

        $field7=[
            ['type' => 'select','class'=>'form-control selectfield','id'=>'start_year','name' => 'start_year','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield','name' => 'owner[]','id' => 'owner','data_type_reports'=>'multiselect'],

        ] ;

        /*Data array*/
        $data7 = [
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'users','joins'=>null,'where'=>$where6],'column'=>['id','name'],'field_name'=>'owner[]','type'=>'selectData'],
        ];
        /*Data array*/
        $db_query_after_where7='';
        $report_join_table7='';
        $report_url7 = '/Reporting/Owner1099DetailReport';
        $report_columns7 = array('Owner Name','Property','% Owned','Address','State/Province','Federal Tax ID','Income ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')');
        $report_db_query7 = "SELECT  REPLACE(u.name,' ','--'), REPLACE(gp.property_name,' ','--') AS prop_name,opo.property_percent_owned,CONCAT(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--')) AS address,REPLACE(u.state,' ','--') AS state,REPLACE(u.tax_payer_id,' ','--') AS tax_payer_id, '-' AS virtual_column FROM users u JOIN owner_property_owned opo ON u.id = opo.user_id JOIN general_property gp ON gp.id = opo.property_id";

        $report_date_filter_table7 = 'u';
        $popUpArray7['title'] = $title7;
        $popUpArray7['label'] = $label7;
        $popUpArray7['field'] = $field7;
        $popUpArray7['data'] = $data7;
        $popUpArray7['report_columns'] = $report_columns7;
        $popUpArray7['alternate_report_columns'] = '';
        $popUpArray7['report_db_query'] = $report_db_query7;
        $popUpArray7['report_url'] = $report_url7;
        $popUpArray7['report_join_table'] = $report_join_table7;
        $popUpArray7['db_query_after_where'] = $db_query_after_where7;
        $popUpArray7['report_date_filter_table'] = $report_date_filter_table7;
        /*-------------------------------------------- Owner 1099 Detail Filters End ----------------------------------*/

        /*--------------------------------------------Owner List Consolidated Filters Filters -------------------------------------------*/
        $title8 = "Owner List Consolidated Filters";
        $label8 = [['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Portfolio','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],

            ];

        $field8=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],



        ] ;

        /*Data array*/
        $data8 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ];
        /*Data array*/
        $db_query_after_where8='';
        $report_join_table8='';
        $report_url8 = '/Reporting/Owner_ListingReport';
        $report_columns8 = array('Owner Name','Phone Number','Email','Address','Property Owned');
        $report_db_query8 = "SELECT REPLACE(u.name,' ','--') AS name ,u.phone_number,u.email,CONCAT(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--')) AS address,REPLACE(gp.property_name,' ','--') AS prop_name FROM users u JOIN owner_property_owned opo on opo.user_id=u.id JOIN general_property gp ON gp.id=opo.property_id";


        $report_date_filter_table8 = 'u';
        $popUpArray8['title'] = $title8;
        $popUpArray8['label'] = $label8;
        $popUpArray8['field'] = $field8;
        $popUpArray8['data'] = $data8;
        $popUpArray8['report_columns'] = $report_columns8;
        $popUpArray8['alternate_report_columns'] = '';
        $popUpArray8['report_db_query'] = $report_db_query8;
        $popUpArray8['report_url'] = $report_url8;
        $popUpArray8['report_join_table'] = $report_join_table8;
        $popUpArray8['db_query_after_where'] = $db_query_after_where8;
        $popUpArray8['report_date_filter_table'] = $report_date_filter_table8;
        /*--------------------------------------------Owner List Consolidated Filters End ----------------------------------*/

        /*-------------------------------------------- Owner Mailing Label Filters -------------------------------------------*/
        $title9 = "Owner Mailing Label Filters";
        $label9 = [['name' => 'Start Date','is_required' => 'yes'],
            ['name' => 'End Date','is_required' => 'yes'],
//            ['name' => 'Portfolio','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Mailing Label Type','is_required' => 'no']];

        $field9=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
//            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'portfolio[]','id' => 'portfolio','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'mailing_label_type','data_type_reports'=>'selectData']
        ] ;

        /*Data array*/
        $report_url9 = '/Reporting/OwnerMailingLabel';
        $report_columns9 = '';
        $report_db_query9 = "SELECT DISTINCT REPLACE(u.name,' ','--') as name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u JOIN owner_property_owned opo ON u.id = opo.user_id JOIN general_property gpm ON opo.property_id = gpm.id";
        $report_join_table9 = "";
        $data9 = [
            ['table'=> '','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
            ['table'=>'','column'=>['id' => 'Date','columnName' => ''],'type'=>'dateTimeData'],
//            ['table'=>['main'=>'company_property_portfolio'],'column'=>['id', 'portfolio_name'],'field_name'=>'portfolio[]'],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'mailing_label_type']
        ];
        /*Data array*/
        $report_date_filter_table9='u';

        $db_query_after_where9 = '';
        $popUpArray9['title'] = $title9;
        $popUpArray9['label'] = $label9;
        $popUpArray9['field'] = $field9;
        $popUpArray9['data'] = $data9;
        $popUpArray9['report_columns'] = $report_columns9;
        $popUpArray9['alternate_report_columns'] = '';
        $popUpArray9['report_db_query'] = $report_db_query9;
        $popUpArray9['report_url'] = $report_url9;
        $popUpArray9['report_join_table'] = $report_join_table9;
        $popUpArray9['db_query_after_where'] = $db_query_after_where9;
        $popUpArray9['report_date_filter_table'] = $report_date_filter_table9;
        /*-------------------------------------------- Owner Mailing Label Filters End ----------------------------------*/

        /*-------------------------------------------- Print Envelope For Owner -------------------------------------------*/
        $title10 = "Print Envelope For Owner";
        $label10 = [
            ['name' => 'Start Date','is_required' => 'no'],
            ['name' => 'End Date','is_required' => 'no'],
            ['name' => 'Status','is_required' => 'no'],
            ['name' => 'Property','is_required' => 'yes'],
            ['name' => 'Owner','is_required' => 'yes'],
            ['name' => 'Envelope Size','is_required' => 'no']
        ];

        $field10=[
            ['type' => 'text','class'=>'form-control datefield','id'=>'start_date','name' => 'start_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'text','class'=>'form-control datefield','id'=>'end_date','name' => 'end_date','data_type_reports'=>'dateTimeData'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'status','data_type_reports'=>'selectData'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield onchange','name' => 'property[]','id' => 'property','data_type_reports'=>'multiselect'],
            ['type' => 'multiselect','class'=>'form-control multiSelectfield ','name' => 'owner[]','id' => 'owner','data_type_reports'=>'multiselect'],
            ['type' => 'select','class'=>'form-control selectfield','name' => 'envelope_size','data_type_reports'=>'selectData']
        ] ;

        $where10 = [[
            'table' => 'users',
            'column' => 'user_type',
            'condition' => '=',
            'value' => '4'
        ],[
            'table' => 'users',
            'column' => 'deleted_at',
            'condition' => 'IS NULL',
            'value' => ''
        ]];

        $report_url10 = '/Reporting/PrintEnvelopeReport';
        $report_columns10 = '';
        $report_db_query10 = "SELECT REPLACE(u.name,' ','--') AS name,REPLACE(u.address1,' ','--') as address1,REPLACE(u.address2,' ','--') as address2,REPLACE(u.address3,' ','--') as address3,REPLACE(u.address4,' ','--') as address4,concat(REPLACE(u.city,' ','--'),',--',REPLACE(u.state,' ','--'),'--',u.zipcode) AS tenant_add FROM users u WHERE u.user_type ='4'";
        $report_join_table10 = "tp";
        /*Data array*/
        $data10 = [
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=> '','column'=>[''],'type'=>'dateTimeData'],
            ['table'=>['main' => 'tenant_details','joins'=>null ,'where'=>null,'onChange'=>'$onChange2'],'column'=>['record_status'],'field_name'=>'status','changed_column'=>'','type'=>'selectData','defaultData'=>[['id'=>'1','name'=>'Active'],['id'=>'0','name'=>'InActive']]],
            ['table'=>['main'=>'general_property'],'column'=>['id','property_name'],'field_name'=>'property[]'],
            ['table'=>['main' => 'users','joins'=>null ,'where'=>$where10],'column'=>['id','name'],'field_name'=>'owner[]', 'type'=>'multiselect'],
            ['table'=> '','column'=>[''],'type'=>'selectData','field_name'=>'envelope_size','id'=>'envelope_size']
        ];
        /*Data array*/
        $report_date_filter_table10='u';

        $db_query_after_where10 = '';
        $popUpArray10['title'] = $title10;
        $popUpArray10['label'] = $label10;
        $popUpArray10['field'] = $field10;
        $popUpArray10['data'] = $data10;
        $popUpArray10['report_columns'] = $report_columns10;
        $popUpArray10['alternate_report_columns'] = '';
        $popUpArray10['report_db_query'] = $report_db_query10;
        $popUpArray10['report_url'] = $report_url10;
        $popUpArray10['report_join_table'] = $report_join_table10;
        $popUpArray10['db_query_after_where'] = $db_query_after_where10;
        $popUpArray10['report_date_filter_table'] = $report_date_filter_table10;
        /*-------------------------------------------- Print Envelope For Owner End ----------------------------------*/

        $listName = [];
        $listName = [['Property List' => $popUpArray],['Unit List' => $popUpArray1],['Vacant Unit' => $popUpArray2],['Owner Withholdings' => $popUpArray3],['Owner Statement' => ''],['Owner Work Orders' => $popUpArray5],['Owner 1099 Summary' => $popUpArray6],['Owner 1099 Detail' => $popUpArray7],['Owner List Consolidated' => $popUpArray8],['Owner Mailing Label' => $popUpArray9],['Print Envelope For Owner' => $popUpArray10]];
        return $listName;
    }




    public static function popupsReportSecond($calledKey)
    {

        $links = [];

        /*Sub heading*/

        /*passing sub heading*/

        /*$links = ['Tenant Report'=>self::getTenantPopUpDataArray(),'Maintenance Reports'=>self::popupsReportMaintenance(),'Property Reports'=>self::propertyReportData(),'Vendor Reports'=>self::popupsVendorData()];*/
        $links = [
            'Tenant Report'=>self::getTenantPopUpDataArray(),
            'Maintenance Reports'=>self::popupsReportMaintenance(),
            'Property Reports'=>self::propertyReportData(),
            'Vendor Reports'=>self::popupsVendorData(),
            'Inventory Reports'=>self::popupsReportInventory(),
            'Contact Reports'=>self::popupsReportContact(),
            'Guest Card Reports'=>self::popupsReportGuestCard(),
            'Company Vehicle Reports'=>self::popupsReportEmployeeVehicle(),
            'Employee Reports'=>self::popupsReportEmployee(),
            'Owner Reports'=>self::getOwnerPopUpDataArray()
        ];

        $html = "";

        foreach ($links[$calledKey] as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $key4 => $v) {
                    $vals = htmlspecialchars(json_encode($v), ENT_QUOTES, 'UTF-8');
                    $html .= '<li class="tenantList" data-value="' . $vals . '"><a href="javascript:;">' . $key4 . '</a></li>';
                }
            }

        }


        return $html;
    }





}
$reports = new reports();