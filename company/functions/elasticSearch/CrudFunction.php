<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
//use Elasticsearch\ClientBuilder;
//require 'vendor/autoload.php';
//include(ROOT_URL . "/config.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
//include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
//include( ROOT_URL .'/elastic_search.php');

/** CRUD FUNCTIONALITY ElASTIC SEARCH **/

/**
 * function to add/update document based on module
 * @param $module
 * @param $action
 * @param $data
 * @return array
 */
function insertDocument($module,$action,$data,$connection){
    try{
        return array('code' => 200, 'status' => 'success', 'message' => 'Done');
        $domain =getDomain();
        switch ($module) {
            case 'PROPERTY':
                $data = getUnserializedData($data);
                $managerString = '';
                $schoolDistrict = '';
                $addressString = '';

                if(!empty($data['manager_id'])){
                    foreach ($data['manager_id'] as $value) {
                        $managerData = getSingleRecord($connection, ['column' => 'id', 'value' => $value], 'users');
                        if ($managerData['code'] == 200) {
                            $managerString .= $managerData['data']['name'].',';
                        }
                    }
                    $managerString = str_replace_last(',','',$managerString);
                }
                if(!empty($data['school_district_municipality'])){
                    foreach ($data['school_district_municipality'] as $value) {
                        $schoolDistrict .= $value.',';
                    }
                    $schoolDistrict = str_replace_last(',','',$schoolDistrict);
                }
                if(!empty($data)){
                    foreach ($data as $key=>$value) {
                        if($key == 'address1' || $key == 'address2' || $key == 'address3' || $key == 'address4'){
                            if(!empty($value)){
                                $addressString .= $value.',';
                            }
                        }
                    }
                    $addressString = str_replace_last(',','',$addressString);
                }
                $propertyData = [];
                $propertyData['id'] = $data['id'];
                $propertyData['Property'] = $data['property_name'];
                $propertyData['State'] = $data['state'];
                $propertyData['City'] = $data['city'];
                $propertyData['Address'] = $addressString;
                $propertyData['No_of_Units'] = $data['no_of_units'];
                $propertyData['No_of_Buildings'] = $data['no_of_buildings'];
                $propertyData['Zip'] = $data['zipcode'];
                $propertyData['PropertyID'] = $data['property_id'];
                $propertyData['Key_Code'] = $data['key_access_codes_info'];
                $propertyData['Last_Renovation'] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ?$data['last_renovation_date']:null;//(isset($data['last_renovation_date']) && !empty($data['last_renovation_date']) ? mySqlDateFormat($data['last_renovation_date'],null,$connection) : null);
                $propertyData['Property_Manager'] = $managerString;
                $propertyData['School_District'] = $schoolDistrict;
                $propertyData['Status'] = $data['status'];
                $index = addDocument($propertyData,$action,$domain.'_property');
                return $index;
            break;
            case 'BUILDING':

                $propertyQuery = 'SELECT zipcode,state,city,school_district_municipality,property_name FROM general_property WHERE id='.$data['property_id'];
                $propertyData = $connection->query($propertyQuery)->fetch();
                $schoolDistrict = '';
                $propertyData = getUnserializedData($propertyData);
                if(!empty($propertyData['school_district_municipality'])){
                    foreach ($propertyData['school_district_municipality'] as $value) {
                        $schoolDistrict .= $value.',';
                    }
                    $schoolDistrict = str_replace_last(',','',$schoolDistrict);
                }

                $buildignData = [];
                $buildignData['id'] = $data['id'];
                $buildignData['Building'] = $data['building_name'];
                $buildignData['BuildingID'] = $data['building_id'];
                $buildignData['Status'] = $data['status'];
                $buildignData['Property_Name'] = $propertyData['property_name'];
                $buildignData['State'] = $propertyData['state'];
                $buildignData['City'] = $propertyData['city'];
                $buildignData['Address'] = $data['address'];
                $buildignData['No_of_Units'] = $data['no_of_units'];
                $buildignData['Zip'] = $propertyData['zipcode'];
                $buildignData['Key_Code'] = $data['key_access_codes_info'];
                $buildignData['Last_Renovation'] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ?$data['last_renovation_date']:null;//(isset($data['last_renovation_date']) && !empty($data['last_renovation_date']) ? mySqlDateFormat($data['last_renovation_date'],null,$connection) : null);
                $buildignData['School_District'] = $schoolDistrict;
                $index = addDocument($buildignData,$action,$domain.'building');
                return $index;
                break;
            case 'UNIT':
                $data = getUnserializedData($data);
                $buildingQuery = 'SELECT building_name FROM building_detail WHERE id='.$data['building_id'];
                $buildingData = $connection->query($buildingQuery)->fetch();
                $schoolDistrict = '';
                $keyAccessCode = '';

                if(!empty($data['key_access_codes'])){
                    foreach ($data['key_access_codes'] as $value) {
                        $keyAccessCode .= $value.',';
                    }
                    $keyAccessCode = str_replace_last(',','',$keyAccessCode);
                }

                if(!empty($data['school_district_municipality'])){
                    foreach ($data['school_district_municipality'] as $value) {
                        $schoolDistrict .= $value.',';
                    }
                    $schoolDistrict = str_replace_last(',','',$schoolDistrict);
                }

                $unitData = [];
                $unitData['id'] = $data['id'];
                $unitData['Unit_Name'] = $data['unit_no'];
                $unitData['Property_Name'] = $data['property_name'];
                $unitData['Building_Name'] = $buildingData['building_name'];
                $unitData['Address'] = $data['address'];
                $unitData['Status'] = $data['status'];
                $unitData['Key_Code'] = $keyAccessCode;
                $unitData['Last_Renovation'] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ?$data['last_renovation_date']:null;//(isset($data['last_renovation_date']) && !empty($data['last_renovation_date']) ? mySqlDateFormat($data['last_renovation_date'],null,$connection) : null);
                $unitData['School_District'] = $schoolDistrict;
                $index = addDocument($unitData,$action,$domain.'unit');
                return $index;
                break;
            case 'VENDOR':
                $data = getUnserializedData($data);
                $vendorType = '';
                $vendorEmail = '';
                $vendorPhone = '';
                $keyAccessCode = '';
                if(!empty($data['vendor_type_id'])) {
                    $vendorTypeQuery = 'SELECT vendor_type FROM company_vendor_type WHERE id=' . $data['vendor_type_id'];
                    $vendorTypeData = $connection->query($vendorTypeQuery)->fetch();
                    $vendorType = $vendorTypeData['vendor_type'];
                }

                if(!empty($data['additional_email'])){
                    foreach ($data['additional_email'] as $value) {
                        $vendorEmail .= $value.',';
                    }
                    $vendorEmail = str_replace_last(',','',$vendorEmail);
                }

                if(!empty($data['emergency_phone'])){
                    foreach ($data['emergency_phone'] as $value) {
                        $vendorPhone .= $value.',';
                    }
                    $vendorPhone = str_replace_last(',','',$vendorPhone);
                }

                $vendorData = [];
                $vendorData['id'] = $data['id'];
                $vendorData['VendorID'] = $data['vendor_random_id'];
                $vendorData['Vendor_Name'] = trim($data['first_name']).' '.trim($data['last_name']);
                $vendorData['YTD_Payment'] = '';
                $vendorData['Vendor_Type'] = $vendorType;
                $vendorData['Phone'] = $vendorPhone;
                $vendorData['Email'] = $vendorEmail;
                $vendorData['Key_Code'] = $keyAccessCode;
                $vendorData['Status'] = $data['status'];
                $index = addDocument($vendorData,$action,$domain.'vendor');
                return $index;
                break;
            case 'CONTACT':
                $data = getUnserializedData($data);
                $contactData = [];
                $contactData['id'] = $data['id'];
                $contactData['Vendor_Name'] = $data['name'];
                $contactData['Phone'] = $data['phone_number'];
                $contactData['Email'] = $data['email'];
                $contactData['Zipcode'] = $data['zipcode'];
                $contactData['Status'] = $data['status'];
                $index = addDocument($contactData,$action,$domain.'contact');
                return $index;
                break;
            case 'EMPLOYEE':
                $data = getUnserializedData($data);
                $employeeEmail = '';
                $employeePhone = '';
                if(!empty($data['email'])){
                    foreach ($data['email'] as $value) {
                        $employeeEmail .= $value.',';
                    }
                    $employeeEmail = str_replace_last(',','',$employeeEmail);
                }

                if(!empty($data['email'])){
                    foreach ($data['email'] as $value) {
                        $employeePhone .= $value.',';
                    }
                    $employeePhone = str_replace_last(',','',$employeePhone);
                }
                $employeeData = [];
                $employeeData['id'] = $data['id'];
                $employeeData['Employee_Name'] = $data['name'];
                $employeeData['Phone'] = $employeePhone;
                $employeeData['Email'] = $employeeEmail;
                $employeeData['Zipcode'] = $data['zipcode'];
                $employeeData['Status'] = $data['status'];
                $index = addDocument($employeeData,$action,$domain.'employee');
                return $index;
                break;
            case 'OWNER':
                $data = getUnserializedData($data);
                $ownerData = [];
                $ownerData['id'] = $data['id'];
                $ownerData['Owner_Name'] = $data['name'];
                $ownerData['Company_Name'] = $data['company_name'];
                $ownerData['Email'] = $data['email'];
                $ownerData['Phone'] = $data['phone_number'];
                $ownerData['Zipcode'] = $data['zipcode'];
                $ownerData['Status'] = $data['status'];
                $index = addDocument($ownerData,$action,$domain.'owner');
                return $index;
                break;
            case 'TENANT':
                $data = getUnserializedData($data);
                $tenantData = [];
                $tenantData['id'] = $data['id'];
                if($action == 'ADD') {
                    $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                    $unitData = $connection->query('SELECT unit_prefix,unit_no FROM unit_details WHERE id =' . $data['unit_id'])->fetch();
                    $tenantData['Property_Name'] = $propertyData['property_name'];
                    $tenantData['Unit_Name'] = $unitData['unit_prefix'].'-'.$unitData['unit_no'];
                }
                $tenantEmail = '';
                $tenantPhone = '';
                if(!empty($data['email'])){
                    foreach ($data['email'] as $value) {
                        $tenantEmail .= $value.',';
                    }
                    $tenantEmail = str_replace_last(',','',$tenantEmail);
                }
                if(!empty($data['phone_number'])){
                    foreach ($data['phone_number'] as $value) {
                        $tenantPhone .= $value.',';
                    }
                    $tenantPhone = str_replace_last(',','',$tenantPhone);
                }

                $tenantData['Tenant_Name'] = trim($data['name']);
                $tenantData['Email'] = $tenantEmail;
                $tenantData['Phone'] = $tenantPhone;
                $tenantData['S.no'] = $data['id'];
                $tenantData['Status'] = $data['status'];
                $index = addDocument($tenantData,$action,$domain.'tenant');
                return $index;
                break;
            case 'RENTAL_APPLICATION':
                $data = getUnserializedData($data);
                $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                $unitData = $connection->query('SELECT unit_prefix,unit_no FROM unit_details WHERE id =' . $data['unit_id'])->fetch();
                $rentalData = [];
                $rentalData['id'] = $data['id'];
                $rentalData['Property_Name'] = $propertyData['property_name'];
                $rentalData['Unit_Name'] = $unitData['unit_prefix'].'-'.$unitData['unit_no'];
                $rentalData['Applicant_Name'] = $data['name'];
                $rentalData['Move_In_Date'] = $data['move_in'];
                $rentalData['Status'] = '1';
                $index = addDocument($rentalData,$action,$domain.'rental_application');
                break;
            case 'COMMUNICATION':
                $data = getUnserializedData($data);
                $communicationData = [];
                $communicationData['id'] = $data['id'];
                $communicationData['Letter_Name'] = $data['letter_name'];
                $communicationData['Description'] = $data['description'];
                $index = addDocument($communicationData,$action,$domain.'communication');
                return $index;
                break;
            case 'LEASE':
                $data = getUnserializedData($data);
                $tenantData = $connection->query('SELECT name FROM users WHERE id =' . $data['user_id'])->fetch();
                $propertyData = $connection->query('SELECT general_property.property_name,concat(unit_details.unit_prefix,'-',unit_details.unit_no) as unit_name FROM tenant_property LEFT JOIN general_property ON tenant_property tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property tenant_property.unit_id=unit_details.id WHERE tenant_property.user_id =' . $data['user_id'])->fetch();
                $leaseData = [];
                $leaseData['id'] = $data['id'];
                $leaseData['Tenant_Name'] = $tenantData['name'];
                $leaseData['Property_Name'] = $propertyData['property_name'];
                $leaseData['Unit'] = $propertyData['unit_name'];
                $leaseData['Move_In_Date'] = $data['move_in'];
                $leaseData['Rent'] = $data['rent_amount'];
                $leaseData['Days_Remaining'] = $data['days_remaining'];
                $index = addDocument($leaseData,$action,$domain.'leases');
                return $index;
                break;
            case 'INSTRUMENT_REGISTER':
                $data = getUnserializedData($data);
                dd($data);
                $instrumentData = [];
                $instrumentData['id'] = $data['id'];
                $instrumentData['Date'] = $data['letter_name'];
                $instrumentData['RefNo'] = $data['description'];
                $instrumentData['Payment_Type'] = $data['description'];
                $instrumentData['BatchID'] = $data['description'];
                $instrumentData['Property'] = $data['description'];
                $instrumentData['Unit'] = $data['description'];
                $instrumentData['Tenant_Name'] = $data['description'];
                $instrumentData['Amount'] = $data['description'];
                $instrumentData['Status'] = $data['description'];
                $index = addDocument($instrumentData,$action,$domain.'instrument_register');
                return $index;
                break;
            case 'DEPOSIT_REGISTER':
                $data = getUnserializedData($data);
                dd($data);
                $depositData = [];
                $depositData['BatchID'] = $data['description'];
                $depositData['Date'] = $data['letter_name'];
                $depositData['Item_Count'] = $data['description'];
                $depositData['Amount'] = $data['description'];
                $depositData['Bank'] = $data['description'];
                $depositData['A/C_NO'] = $data['description'];
                $depositData['Created_By'] = $data['description'];
                $depositData['Status'] = $data['description'];
                $depositData['id'] = $data['id'];
                $index = addDocument($depositData,$action,$domain.'deposit_register');
                return $index;
                break;
            case 'JOURNAL_ENTRY':
                $data = getUnserializedData($data);
                $unitName = '';
                $portfolioData = $connection->query('SELECT portfolio_name FROM company_property_portfolio WHERE id =' . $data['portfolio_id'])->fetch();
                $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                $bankData = $connection->query('SELECT bank_name FROM company_accounting_bank_account WHERE id =' . $data['bank_account_id'])->fetch();
                if(isset($data['unit_id']) && !empty($data['unit_id'])){
                    $unitName = $connection->query('SELECT concat(unit_prefix,'-',unit_no) as unit_name FROM unit_details WHERE id =' . $data['unit_id'])->fetch();
                    $unitName = $unitName['unit_name'];
                }
                $journalData = [];
                $journalData['id'] = $data['id'];
                $journalData['Journal_Entry_ID'] = $data['id'];
                $journalData['Date'] = $data['created_at'];
                $journalData['Portfolio'] = $portfolioData['portfolio_name'];
                $journalData['Property'] = $propertyData['property_name'];
                $journalData['Unit'] = $unitName;
                $journalData['Account'] = $bankData['bank_name'];
                $journalData['Debit'] = isset($data['debit'])?$data['debit']:'';
                $journalData['Credit'] = isset($data['credit'])?$data['credit']:'';
                $journalData['Frequency'] = $data['journal_entry_type'];
                $index = addDocument($journalData,$action,$domain.'journal_entry');
                return $index;
                break;
            case 'BUDGET':
                $data = getUnserializedData($data);
                $portfolioData = $connection->query('SELECT portfolio_name FROM company_property_portfolio WHERE id =' . $data['portfolio_id'])->fetch();
                $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                $budgetData = [];
                $budgetData['id'] = $data['id'];
                $budgetData['Budget_Name'] = $data['budget_name'];
                $budgetData['Portfolio'] = $portfolioData['portfolio_name'];
                $budgetData['Property'] = $propertyData['property_name'];
                $budgetData['Year'] = $data['starting_year'];
                $budgetData['Created_Date'] = date('Y-m-d');
                $budgetData['Income'] = $data['income_amount'];
                $budgetData['Credit'] = $data['expense_amount'];
                $budgetData['Net_Income'] = $data['net_income'];
                $index = addDocument($budgetData,$action,$domain.'budget');
                return $index;
                break;
            case 'TICKET':
                $data = getUnserializedData($data);
                $userData = $connection->query('SELECT name FROM users WHERE id =' . $data['user_id'])->fetch();
                $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                $unitName = $connection->query('SELECT concat(unit_prefix,'-',unit_no) as unit_name FROM unit_details WHERE id =' . $data['unit_id'])->fetch();
                $ticketData = [];
                $ticketData['id'] = $data['id'];
                $ticketData['Ticket_Number'] = $data['ticket_number'];
                $ticketData['Tenant_Name'] = $userData['name'];
                $ticketData['Property'] = $propertyData['property_name'];
                $ticketData['Unit'] = $unitName['unit_name'];
                $ticketData['Category_Name'] = $data['category'];
                $index = addDocument($ticketData,$action,$domain.'ticket');
                return $index;
                break;
            case 'INVOICE':
                $data = getUnserializedData($data);
                $invoiceData = [];
                $userData = $connection->query('SELECT name FROM users WHERE id =' . $data['user_id'])->fetch();
                if(isset($data['property_id']) && isset($data['invoice_number'])) {
                    $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property_id'])->fetch();
                    $invoiceData['Invoice_No'] = $data['invoice_number'];
                    $invoiceData['Property_Name'] = $propertyData['property_name'];
                }
                $invoiceData['id'] = $data['id'];
                $invoiceData['Name'] = $userData['name'];
                $invoiceData['Invoice_Date'] = $data['invoice_date'];
                $invoiceData['Amount'] = '';
                $invoiceData['Due_Date'] = $data['late_date'];
                $index = addDocument($invoiceData,$action,$domain.'invoice');
                return $index;
            case 'PAYMENT_REGISTER':
                $data = getUnserializedData($data);
                dd($data);
                $paymentData = [];
                $paymentData['id'] = $data['id'];
                $paymentData['Due_Date'] = $data['description'];
                $paymentData['Invoice_To'] = $data['description'];
                $paymentData['Property_Name'] = $data['description'];
                $paymentData['Building_Name'] = $data['description'];
                $paymentData['Unit_Name'] = $data['description'];
                $paymentData['Invoice'] = $data['description'];
                $paymentData['Invoice_Amount'] = $data['description'];
                $paymentData['Paid_Amount'] = $data['description'];
                $paymentData['Balance'] = $data['description'];
                $index = addDocument($paymentData,$action,$domain.'payment_register');
                return $index;
                break;
            case 'CHECK_REGISTER':
                $data = getUnserializedData($data);
                dd($data);
                $checkData = [];
                $checkData['id'] = $data['id'];
                $checkData['Date'] = $data['description'];
                $checkData['Payee'] = $data['description'];
                $checkData['Bill_Number'] = $data['description'];
                $checkData['Check_Number'] = $data['description'];
                $checkData['Amount'] = $data['description'];
                $checkData['Property_Name'] = $data['description'];
                $checkData['Description'] = $data['description'];
                $index = addDocument($checkData,$action,$domain.'check_register');
                return $index;
                break;
            case 'PURCHASE_ORDER':
                $data = getUnserializedData($data);
                $userData = $connection->query('SELECT name FROM users WHERE id =' . $data['vendor'])->fetch();
                $propertyData = $connection->query('SELECT property_name FROM general_property WHERE id =' . $data['property'])->fetch();
                $buildingData = $connection->query('SELECT building_name FROM building_detail WHERE id='.$data['building_id'])->fetch();
                $unitName = $connection->query('SELECT concat(unit_prefix,'-',unit_no) as unit_name FROM unit_details WHERE id =' . $data['unit'])->fetch();
                $orderData = [];
                $orderData['id'] = $data['id'];
                $orderData['PO_Number'] = $data['po_number'];
                $orderData['Property_Name'] = $propertyData['property_name'];
                $orderData['Building_Name'] = $buildingData['building_name'];
                $orderData['Unit_Name'] = $unitName['unit_name'];
                $orderData['Vendor_Name'] = $userData['name'];
                $orderData['Approver_Name'] = $data['approver_name'];
                $orderData['Work_Order_Number'] = $data['work_order'];
                $index = addDocument($orderData,$action,$domain.'purchase_order');
                return $index;
                break;
            default:
        }
    } catch(Exception $exception) {
        return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
    }
}

/**
 * function to add Document
 * @param $data
 * @param $action
 * @param $index
 * @return array
 */
function addDocument($data,$action,$index){
    try {
        $client = ClientBuilder::create()->build();
        $params = [];
        switch ($action) {
            case 'ADD':
                $params = [
                    'index' => $index,
                    'type'  => 'my_type',
                    'id'    =>  $data['id'],
                    'body'  => $data
                ];

                $response = $client->index($params);
                break;
            case 'UPDATE':
                $params = [
                    'index' => $index,
                    'id'    => $data['id'],
                    'body'  => [
                        'doc' => $data
                    ]
                ];
                $response = $client->update($params);
                break;
            default:
        }
        return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => $response);
    } catch (Exception $exception) {
        $exceptionMessage = json_decode($exception->getMessage());
        if($exceptionMessage->status == 404){
            return addDocument($data,'ADD',$index);
        }
        return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
    }
}