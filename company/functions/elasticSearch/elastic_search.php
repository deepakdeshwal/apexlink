<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
use Elasticsearch\ClientBuilder;

require 'vendor/autoload.php';


include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include( ROOT_URL .'/elastic_search.php');

class ElasticSearch extends DBConnection
{
    public function __construct()
    {
        parent::__construct();
        $domain = getDomain();
        $indexArray = [$domain.'_property',$domain.'_building',$domain.'_unit',$domain.'_vendor',$domain.'_contact',$domain.'_employee',$domain.'_owner',$domain.'_tenant',$domain.'_leases',$domain.'_communication',$domain.'_instrument_register',$domain.'_deposit_register',$domain.'_journal_entry',$domain.'_budget',$domain.'_ticket',$domain.'_invoice',$domain.'_payment_register',$domain.'_check_register',$domain.'_purchase_order'];

        $this->insertTenantData($domain);
        $this->insertPropertyData($domain);
        $this->insertBuildingData($domain);
        $this->insertUnitData($domain);
        $this->insertOwnerData($domain);
        $this->insertVendorData($domain);
        $this->insertContactData($domain);
        $this->insertWaitingListData($domain);
        $this->insertGuestCardData($domain);
        $this->insertRentalApplicationData($domain);
        $this->insertLeaseData($domain);
        $this->insertMoveInData($domain);
        $this->insertWorkOrderData($domain);
        $this->insertBillRegisterData($domain);
        $this->insertCommunicationData($domain);
        $this->insertInstrumentRegisterData($domain);
        $this->insertDepositRegisterData($domain);
        $this->insertJournalEntryData($domain);
        $this->insertBudgetData($domain);
        $this->insertTicketsData($domain);
        $this->insertInvoiceData($domain);
        $this->insertPaymentRegisterData($domain);
        $this->insertCheckRegisterData($domain);
        $this->insertEmployeeData($domain);
        $this->insertPurchaseOrderData($domain);
        $this->insertUsersData($domain);
        $this->mapping();
          //$this->newtestfunction($domain);
//        $indexes=array($domain."_property",$domain."_building",$domain."_unit",$domain."_tenant",$domain."_owner",$domain."_vendor",$domain."_contact",$domain."_WaitingList",$domain."_tickets",$domain."_Employee",$domain."_PurchaseOrder");
//$this->deleteIndexajax($indexes);
        // print_r($_REQUEST); die();
        //echo json_encode($this->$action());
    }

    public function insertTenantData($domain){
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_tenant';
        $result= $client->indices()->exists($indexParams);
        if($result){
            $this->deleteIndex($domain.'_tenant');
        }
        $query1 = $this->companyConnection->query("SELECT  users.id,users.status,  users.name AS Tenant_Name,users.phone_number AS PhoneNumber, users.email AS Email, general_property.property_name AS Property_Name, concat(unit_details.unit_prefix,'-',unit_details.unit_no) as Unit_Name,users.id as Sno FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1')");
        $data = $query1->fetchAll();
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_tenant',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("Tenant Data Uploaded Successfully!");
    }
    public function insertPropertyData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_property';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_property');
        }
        $query1 = $this->companyConnection->query("SELECT general_property.id,general_property.status, general_property.property_name AS Property_Name,general_property.state AS State,general_property.city AS City,concat(general_property.address1,' ',general_property.address2,' ',general_property.address3,' ',general_property.address4) as Address, general_property.no_of_units AS No_of_units,general_property.no_of_buildings As No_of_buildings, general_property.manager_list AS Manager_list,general_property.zipcode AS Zipcode, general_property.property_id,'' as Keycode,'' as Lastrenovation,'' as School_District, '' as code FROM general_property LEFT JOIN company_property_type ON general_property.property_type=company_property_type.id WHERE ( general_property.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_property',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Property Data Uploaded Successfully!");
    }
    public function insertBuildingData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_building';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_building');
        }
        $query1 = $this->companyConnection->query("SELECT building_detail.id,building_detail.status, building_detail.building_name AS Building_Name,general_property.property_name AS Property_Name,general_property.state AS State,general_property.city AS City,concat(general_property.address1,' ',general_property.address2,' ',general_property.address3,' ',general_property.address4) AS Address,building_detail.no_of_units AS No_of_units,general_property.zipcode AS Zipcode,building_detail.key_access_codes_info AS Key_access_codes_info,building_detail.last_renovation_date AS Last_renovation,building_detail.school_district_municipality AS School_district_municipality,building_detail.key_access_code_desc AS Key_access_code_desc FROM building_detail JOIN general_property ON general_property.id=building_detail.property_id WHERE ( building_detail.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_building',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Building Data Uploaded Successfully!");
    }
    public function insertUnitData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_unit';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex('unit');
        }
        $query1 = $this->companyConnection->query("SELECT unit_details.id,IF(unit_details.building_unit_status=4,'1','0') as status,concat(unit_details.unit_prefix,'-',unit_details.unit_no) AS Unit_name,general_property.property_name AS Property_Name, unit_details.building_name_and_id AS Building_name,concat(general_property.address1,' ',general_property.address2,' ',general_property.address3,' ',general_property.address4) AS Address,unit_details.building_unit_status AS Building_unit_status,unit_details.base_rent AS Rent,unit_details.security_deposit As Security_Deposit,unit_details.key_access_codes as Access_Code,unit_details.last_renovation As Last_renovation,unit_details.school_district_municipality AS School_District_Municipality,unit_details.school_district_code AS District_code FROM unit_details LEFT JOIN building_detail ON unit_details.building_id=building_detail.id JOIN general_property on general_property.id=unit_details.property_id WHERE ( unit_details.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_unit',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Unit Data Uploaded Successfully!");
    }
    public function insertOwnerData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_owner';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_owner');
        }
        $query1 = $this->companyConnection->query("SELECT  users.id,users.status,  users.name AS Owner_name, users.company_name AS Company_name, users.email AS Email,users.phone_number AS Phone_Number,users.zipcode AS Zipcode FROM users LEFT JOIN owner_details ON users.id=owner_details.user_id WHERE ( users.deleted_at IS NULL AND users.status = '1' AND users.user_type = '4')");
        $data = $query1->fetchAll();
        //dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_owner',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Owner Data Uploaded Successfully!");
    }
    public function insertVendorData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_vendor';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_vendor');
        }
        $query1 = $this->companyConnection->query("SELECT vendor_additional_detail.id,vendor_additional_detail.status,vendor_additional_detail.vendor_id AS Vendor_ID,users.name AS Vendor_Name,vendor_additional_detail.vendor_rate AS Vendor_rate,users.phone_number AS Phone_number, users.email AS Email,company_vendor_type.vendor_type AS Vendor_type,users.zipcode AS Zipcode FROM vendor_additional_detail LEFT JOIN users ON vendor_additional_detail.vendor_id=users.id LEFT JOIN company_vendor_type ON vendor_additional_detail.vendor_type_id=company_vendor_type.id WHERE ( vendor_additional_detail.deleted_at IS NULL AND users.user_type = '3')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_vendor',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Vendor Data Uploaded Successfully!");
    }
    public function insertContactData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_contact';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_contact');
        }
        $query1 = $this->companyConnection->query("SELECT  users.id,users.status,  users.name AS Name,users.email AS Email, users.phone_number AS Phone_number,users.zipcode AS Zipcode FROM users LEFT JOIN employee_details ON users.id=employee_details.user_id WHERE ( users.deleted_at IS NULL AND users.user_type = '5')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_contact',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Contact Data Uploaded Successfully!");
    }
    public function insertWaitingListData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_waiting_list';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_waiting_list');
        }
        $query1 = $this->companyConnection->query("SELECT users.id,users.status, users.name AS Name, users.company_name AS Company_Name, general_property.property_name AS Property_Name,users.email AS Email,users.phone_number AS Phone_number,users.zipcode as Zipcode FROM users LEFT JOIN waiting_list ON users.id=waiting_list.user_id LEFT JOIN general_property ON waiting_list.property_id=general_property.id LEFT JOIN building_detail ON waiting_list.property_id=building_detail.id LEFT JOIN unit_details ON waiting_list.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '11')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_waiting_list',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Waiting List Data Uploaded Successfully!");
    }
    public function insertGuestCardData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_guest_card';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_guest_card');
        }
        $query1 = $this->companyConnection->query("SELECT  lease_guest_card.id,users.status, users.name AS Proppect_Name, general_property.property_name AS Property_Name, lease_guest_card.id as Guest_Card_Id, lease_guest_card.expected_move_in AS Move_in_date, lease_guest_card.status AS Status FROM lease_guest_card LEFT JOIN users ON lease_guest_card.user_id=users.id LEFT JOIN unit_details ON lease_guest_card.unit_id=unit_details.id LEFT JOIN general_property ON unit_details.property_id=general_property.id WHERE ( lease_guest_card.deleted_at IS NULL AND users.user_type = '6')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_guest_card',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Guest Card Data Uploaded Successfully!");
    }
    public function insertRentalApplicationData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_rental_application';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_rental_application');
        }
        $query1 = $this->companyConnection->query("SELECT users.id,users.status,  users.name AS Applicant_Name, general_property.property_name AS Property_Name, CONCAT(unit_details.unit_prefix,'-',unit_details.unit_no) as  Unit, company_rental_applications.exp_move_in AS Move_in_date, company_rental_applications.status AS Status FROM users LEFT JOIN company_rental_applications ON users.id=company_rental_applications.user_id LEFT JOIN general_property ON company_rental_applications.prop_id=general_property.id LEFT JOIN unit_details ON company_rental_applications.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '10' AND users.record_status = '1')");
        $data = $query1->fetchAll();
        //dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_rental_application',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Rental Application Data Uploaded Successfully!");
    }
    public function insertLeaseData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_leases';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_leases');
        }
        $query1 = $this->companyConnection->query("SELECT users.id,users.status,users.name AS Tenant_Name, general_property.property_name AS Property_Name,concat(unit_details.unit_prefix,'-',unit_details.unit_no) as Unit_name, tenant_lease_details.move_in AS Move_in_date,tenant_lease_details.rent_amount AS Rent,tenant_lease_details.days_remaining AS Days_remaining FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1')");
        $data = $query1->fetchAll();
        //  dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_leases',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Leases Data Uploaded Successfully!");
    }
    public function insertMoveInData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_move_in';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_move_in');
        }
        $query1 = $this->companyConnection->query("SELECT  users.id,users.status, users.name AS Tenant_Name, general_property.property_name AS Property_Name,CONCAT(unit_details.unit_prefix,'-',unit_details.unit_no) as Unit_Name,tenant_lease_details.move_in AS Move_in_Date, tenant_lease_details.rent_amount AS Rent, tenant_lease_details.days_remaining AS Days_Remaining FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN movein_list_record ON users.id=movein_list_record.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_move_in',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Move In Data Uploaded Successfully!");
    }
    public function insertWorkOrderData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_work_order';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_work_order');
        }
        $query1 = $this->companyConnection->query("SELECT work_order.id,work_order.status_id as status,work_order.work_order_number AS Work_Order_Number, company_workorder_category.category AS Category, users.name AS Vendor_Name, general_property.property_name AS Property_name, work_order.request_id AS Requested_By, work_order.estimated_cost AS Amount, company_priority_type.priority AS Priority FROM work_order LEFT JOIN company_workorder_category ON work_order.work_order_cat=company_workorder_category.id LEFT JOIN general_property ON work_order.property_id=general_property.id LEFT JOIN company_priority_type ON work_order.priority_id=company_priority_type.id LEFT JOIN company_workorder_status ON work_order.status_id=company_workorder_status.id LEFT JOIN users ON work_order.vendor_id=users.id WHERE ( work_order.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_work_order',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Move In Data Uploaded Successfully!");
    }
    public function insertBillRegisterData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_bill_register';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_bill_register');
        }
        $query1 = $this->companyConnection->query("SELECT company_bills.id,users.status, users.name as Vendor_Name,company_bills.due_date AS Due_Date, company_bills.refrence_number AS Reference, company_bills.amount as Original_Amount, company_bills.memo, company_bills.term, company_bills.amount, company_bills.status FROM company_bills LEFT JOIN users ON company_bills.vendor_id=users.id WHERE ( company_bills.deleted_at IS NULL)");
        $data = $query1->fetchAll();
//        dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_bill_register',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Bill Register Data Uploaded Successfully!");
    }
    public function insertCommunicationData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_communication';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_communication');
        }
        $query1 = $this->companyConnection->query("SELECT letters_notices_template.id,  letters_notices_template.letter_name AS Letter_Name, letters_notices_template.description AS Description FROM letters_notices_template WHERE ( letters_notices_template.deleted_at IS NULL AND letters_notices_template.letter_type = '1')");
        $data = $query1->fetchAll();
        //  dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_communication',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Communication Data Uploaded Successfully!");
    }
    public function insertInstrumentRegisterData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_instrument_register';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_instrument_register');
        }
        $query1 = $this->companyConnection->query("SELECT letters_notices_template.id,  letters_notices_template.letter_name AS Letter_Name, letters_notices_template.description AS Description FROM letters_notices_template WHERE ( letters_notices_template.deleted_at IS NULL AND letters_notices_template.letter_type = '1')");
        $data = $query1->fetchAll();
        //  dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_instrument_register',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Instrument Register Data Uploaded Successfully!");
    }
    public function insertDepositRegisterData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_deposit_register';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_deposit_register');
        }
        $query1 = $this->companyConnection->query("SELECT  accounting_journal_entries.id,accounting_journal_entries.created_at, accounting_journal_entries.accounting_period, company_property_portfolio.portfolio_name, general_property.property_name, accounting_journal_entries.id, accounting_journal_entries.id, accounting_journal_entries.id, general_property.property_for_sale, unit_details.unit_prefix, unit_details.floor_no, accounting_journal_entries.id FROM accounting_journal_entries LEFT JOIN company_property_portfolio ON accounting_journal_entries.portfolio_id=company_property_portfolio.id LEFT JOIN general_property ON accounting_journal_entries.property_id=general_property.id LEFT JOIN unit_details ON accounting_journal_entries.unit_id=unit_details.id WHERE ( accounting_journal_entries.deleted_at IS NULL AND accounting_journal_entries.status = '1')");
        $data = $query1->fetchAll();
        //  dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_deposit_register',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Deposit Register Data Uploaded Successfully!");
    }
    public function insertJournalEntryData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_journal_entry';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_journal_entry');
        }
        $query1 = $this->companyConnection->query("SELECT  accounting_journal_entries.id,accounting_journal_entries.id AS Journal_Entry_ID, accounting_journal_entries.created_at AS Date,company_property_portfolio.portfolio_name AS Portfolio, general_property.property_name AS Property,CONCAT( unit_details.unit_prefix,'-',unit_details.unit_no) AS  Unit,'' AS Account,'' as  Debit , '' as Credit ,'' as Frequency FROM accounting_journal_entries LEFT JOIN company_property_portfolio ON accounting_journal_entries.portfolio_id=company_property_portfolio.id LEFT JOIN general_property ON accounting_journal_entries.property_id=general_property.id LEFT JOIN unit_details ON accounting_journal_entries.unit_id=unit_details.id WHERE ( accounting_journal_entries.deleted_at IS NULL AND accounting_journal_entries.status = '1')");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_journal_entry',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Journal Entry Data Uploaded Successfully!");
    }
    public function insertBudgetData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_budget';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_budget');
        }
        $query1 = $this->companyConnection->query("SELECT  company_budget.id,  company_budget.budget_name AS Budget_Name, company_property_portfolio.portfolio_name AS Portfolio, general_property.property_name AS Property,Concat( company_budget.starting_year ,'-', company_budget.starting_month) AS Year, company_budget.created_at AS Created_Date, company_budget.income_amount AS Income, company_budget.expense_amount AS Credit, company_budget.net_income AS Net_Income FROM company_budget LEFT JOIN company_property_portfolio ON company_budget.portfolio_id=company_property_portfolio.id LEFT JOIN general_property ON company_budget.property_id=general_property.id WHERE ( company_budget.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        // dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_budget',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Budget Data Uploaded Successfully!");
    }
    public function insertTicketsData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_ticket';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_ticket');
        }
        $query1 = $this->companyConnection->query("SELECT tenant_maintenance.id,tenant_maintenance.status,tenant_maintenance.ticket_number AS Ticket_Number, tp.first_name AS Tenant_Name, tp3.property_name AS Proterty, CONCAT(tp1.unit_prefix,'-',tp1.unit_no) as Unit, tenant_maintenance.category AS Category_Name FROM tenant_maintenance LEFT JOIN users as tp  ON tenant_maintenance.user_id=tp.id LEFT JOIN unit_details as tp1  ON tenant_maintenance.unit_id=tp1.id LEFT JOIN general_property as tp3  ON tenant_maintenance.property_id=tp3.id WHERE ( tenant_maintenance.deleted_at IS NULL)");
        $data = $query1->fetchAll();
//         dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_ticket',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Tickets Data Uploaded Successfully!");
    }
    public function insertInvoiceData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_invoice';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_invoice');
        }
        $query1 = $this->companyConnection->query("SELECT accounting_invoices.id,accounting_invoices.status,  accounting_invoices.invoice_number AS Invoice_No, general_property.property_name AS Property_Name, users.name AS Name,  accounting_invoices.invoice_date as Invoice_Date, accounting_invoices.amount_due AS Amount,accounting_invoices.late_date AS Due_Date FROM  accounting_invoices LEFT JOIN tenant_property ON accounting_invoices.invoice_to=tenant_property.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id LEFT JOIN users ON accounting_invoices.invoice_to=users.id LEFT JOIN general_property as OwnerProperty  ON accounting_invoices.property_id=OwnerProperty.id WHERE (  accounting_invoices.deleted_at IS NULL AND  accounting_invoices.module_type IS NULL)");
        $data = $query1->fetchAll();
//        dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_invoice',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Invoice Data Uploaded Successfully!");
    }
    public function insertPaymentRegisterData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_payment_register';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_payment_register');
        }
        $query1 = $this->companyConnection->query("SELECT accounting_invoices.id,accounting_invoices.status,  accounting_invoices.invoice_number, general_property.property_name AS name, users.name AS name1,  accounting_invoices.invoice_date, accounting_invoices.amount_due,  accounting_invoices.late_date FROM  accounting_invoices LEFT JOIN tenant_property ON accounting_invoices.invoice_to=tenant_property.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id LEFT JOIN users ON accounting_invoices.invoice_to=users.id LEFT JOIN general_property as OwnerProperty  ON accounting_invoices.property_id=OwnerProperty.id WHERE (  accounting_invoices.deleted_at IS NULL AND  accounting_invoices.module_type IS NULL)");
        $data = $query1->fetchAll();
//        dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_payment_register',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Payment Register Data Uploaded Successfully!");
    }
    public function insertCheckRegisterData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_check_register';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_check_register');
        }
        $query1 = $this->companyConnection->query("SELECT accounting_invoices.id,accounting_invoices.status,  accounting_invoices.invoice_number, general_property.property_name AS name, users.name AS name1,  accounting_invoices.invoice_date, accounting_invoices.amount_due,  accounting_invoices.late_date FROM  accounting_invoices LEFT JOIN tenant_property ON accounting_invoices.invoice_to=tenant_property.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id LEFT JOIN users ON accounting_invoices.invoice_to=users.id LEFT JOIN general_property as OwnerProperty  ON accounting_invoices.property_id=OwnerProperty.id WHERE (  accounting_invoices.deleted_at IS NULL AND  accounting_invoices.module_type IS NULL)");
        $data = $query1->fetchAll();
//        dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_check_register',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Check Register Data Uploaded Successfully!");
    }
    public function insertEmployeeData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_employee';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_employee');
        }
        $query1 = $this->companyConnection->query("SELECT  users.id,users.status,  users.name AS Employee_Name, users.email AS Email, users.phone_number AS Phone,users.zipcode AS Zipcode FROM users LEFT JOIN employee_details ON users.id=employee_details.user_id WHERE ( users.deleted_at IS NULL AND users.user_type = '8')");
        $data = $query1->fetchAll();
        //   dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_employee',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Employee Data Uploaded Successfully!");
    }
    public function insertPurchaseOrderData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_purchase_order';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_purchase_order');
        }
        $query1 = $this->companyConnection->query("SELECT  purchaseOrder.id,purchaseOrder.status, purchaseOrder.po_number AS PO_Number, general_property.property_name AS Property_Name, building_detail.building_name AS Building_Name,CONCAT( unit_details.unit_prefix,'-', unit_details.unit_no) as Unit_Name, purchaseOrder.vendor AS Vendor_Name, users.name AS Approved_Name, purchaseOrder.invoice_number AS Work_Order_Number FROM purchaseOrder LEFT JOIN general_property ON purchaseOrder.property=general_property.id LEFT JOIN building_detail ON purchaseOrder.building=building_detail.id LEFT JOIN unit_details ON purchaseOrder.unit=unit_details.id LEFT JOIN users ON purchaseOrder.owners=users.id WHERE ( purchaseOrder.deleted_at IS NULL)");
        $data = $query1->fetchAll();
        //dd($data);
        for($i = 0; $i < count($data); $i++) {
            $id=$i+1;
            $params['body'][] = [
                'index' => [
                    '_index' => $domain.'_purchase_order',
                    '_type' => 'my_type',
                    '_id'=>$id
                ]
            ];
            $params['body'][] = $data[$i];
        }
        $client = ClientBuilder::create()->build();
        $responses = $client->bulk($params);
        print_r("<br>Purchase Order Data Uploaded Successfully!");
    }
    public function insertUsersData($domain)
    {
        $client = ClientBuilder::create()->build();
        $indexParams['index']  = $domain.'_users';
        $result= $client->indices()->exists($indexParams);
        if($result=='1'){
            $this->deleteIndex($domain.'_users');
        }
        $query1 = $this->companyConnection->query("SELECT users.user_type,users.status,users.updated_at, users.id,  users.name, users.email, users.mobile_number, users.last_login, company_user_roles.role_name, users.status FROM users LEFT JOIN company_user_roles ON users.role=company_user_roles.id WHERE ( users.id != '1' AND users.deleted_at IS NULL AND users.status = '0' AND users.user_type = '1' AND users.created_by = '1')");
        $data = $query1->fetchAll();
         dd($data);
        if(!empty($data)){
            for($i = 0; $i < count($data); $i++) {
                $id=$i+1;
                $params['body'][] = [
                    'index' => [
                        '_index' => $domain.'_users',
                        '_type' => 'my_type',
                        '_id'=> $id
                    ]
                ];
                $params['body'][] = $data[$i];
            }
            $client = ClientBuilder::create()->build();
            $responses = $client->bulk($params);
            print_r("<br>Users Data Uploaded Successfully!");
        }else{
            print_r("<br>Users Data Uploaded Successfully but empty!");
        }

    }
    public function newtestfunction($domain)
    {
        /*Search a index*/
        $words="Hello";
        $params = [
            'type' => 'my_type',
            'body' => [
                'query' => [
                    'query_string'=>[
                        "query"=> "*23*"
                    ]
                ]
            ]
        ];
        $client = ClientBuilder::create()->build();
        $response = $client->search($params);
        echo "<pre>";  print_r($response);

    }
    public function deleteIndex($indexName)
    {
            $deleteParams = [
                'index' => $indexName
            ];

            $client = ClientBuilder::create()->build();
            if ($client->indices()->exists($deleteParams)) {
                $response = $client->indices()->delete($deleteParams);
            }

    } public function deleteIndexajax($indexName)
    {
        foreach ($indexName as $value) {
            $deleteParams = [
                'index' => $value
            ];

            $client = ClientBuilder::create()->build();
            if ($client->indices()->exists($deleteParams)) {
                $response = $client->indices()->delete($deleteParams);
            }
        }
    }
    public function mapping(){
        $domain = getDomain();

        $params['type']  = 'my_type';

// Adding a new type to an existing index
        $myTypeMapping2 = array(
            '_source' => array(
                'enabled' => true
            ),
            'properties' => array(
                'name' => array(
                    'type' => 'text',
                    "fielddata"=> True


                )
            )
        );
        $params['body']['my_type'] = $myTypeMapping2;
// Update the index mapping
        $client = ClientBuilder::create()->build();
        $client->indices()->putMapping($params);
    }
}
$ElasticSearch = new ElasticSearch();