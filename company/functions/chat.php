<?php
/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 1/17/2019
 * Time: 3:30 PM
 */
include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
//include_once ('constants.php');
//include_once (ROOT_URL.'/vendor/jqGridPHP-master/php/jqGridLoader.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
};

/**
 * Socket IO Server Side Implementation
 * Class chat
 */
class chat extends DBConnection
{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function chat(){

    }

    public function curl($chatData){
        if(empty($chatData)){
            return 'error';
        }
        $ch = curl_init('http://localhost:8080');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // we send JSON encoded data to the Node.JS server
        $jsonData = json_encode([
            'name' => $chatData['name'],
            'message' => $chatData['message']
        ]);
        $query = http_build_query(['data' => $jsonData]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }
}

$helper = new chat();