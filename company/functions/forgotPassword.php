<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class forgotPasswordAjax extends DBConnection {

    /**
     * forgotPassword constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for company user forgot password
     */
    public function forgotPassword() {
        try {
            $email = $_POST['email'];
            $domain = SUBDOMAIN_URL;

            $domain = explode('.', $domain)[0];
            $domain = explode('/', $domain);
            $sub_domain = end($domain);
            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND domain_name='$sub_domain' AND user_type='1'");
            $user = $query->fetch();

            if ($user) {
                $forgot_password_token = randomTokenString(50);
                $email = $user['email'];

                $update_forgot_password_token = $this->companyConnection
                    ->prepare("UPDATE users SET reset_password_token=? WHERE email=?")
                    ->execute([$forgot_password_token, $email]);

                //Send reset password token in super admin's user table also
                $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND domain_name='$sub_domain' AND user_type='1'");
                $super_admin_user = $query->fetch();
                if ($super_admin_user){
                    $update_forgot_password_token = $this->conn
                        ->prepare("UPDATE users SET reset_password_token=? WHERE email=?")
                        ->execute([$forgot_password_token, $email]);
                }

                $IP_address = $_SERVER['SERVER_ADDR'];
                $user_name = userName($user['id'], $this->companyConnection);
                $body = $this->getForgotPasswordHtml($user_name, $user, $forgot_password_token);
                $body = str_replace("#logo#",SUBDOMAIN_URL.'/company/images/logo.png',$body);
//                $body = str_replace("#IPAddress#",$IP_address,$body);
//                print_r($body);die();
                $user['message'] = $body;

                return (['status' => 'success',$user, $update_forgot_password_token]);
            } else {
                return array('status' => 'error', 'message' => 'Details corresponding to this email-Id does not exist.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * forgot password mail functionality
     * @param $user
     * @param $forgot_password_token
     * @return string
     */
    public function getForgotPasswordHtml($user_name, $user, $forgot_password_token) {

        $html = '<table id="background-table" style="background-color: #ececec; width: 100%;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center" bgcolor="#ececec">
                <table class="w640" style="margin: 0px 10px; width: 640px;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="background-color: #00b0f0; height: 20px;">
                <td>&nbsp;</td>
                </tr>
                <tr style="border-collapse: collapse; background-color: #fff;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <div style="margin-top: 1px; margin-bottom: 10px; text-align: center">
                <img alt="Apexlink" src="#logo#"/>
                </div>
                </td>
                </tr>
                <tr style="background-color: #00b0f0; height: 20px;">
                <td>&nbsp;</td>
                </tr>
                <tr id="simple-content-row" style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" bgcolor="#ffffff" width="640">
                <table class="w640" style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="left">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <table class="w580" style="width: 580px;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <p class="article-title" style="font-size: 22px; line-height: 24px; color: #ff0000; font-weight: bold; text-align: center; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;">Forgot your password?</p>
                <div class="article-content" style="font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left">
                <p style="margin-bottom: 15px;">Dear ' . $user_name . ',</p>    
                <p style="margin-bottom: 15px;">This email was sent automatically to you by Apexlink in response to your request to recover your password. This is done for your protection; only you, the recipient of this email can take the necessary step in your password recover process.</p>
                <p style="margin-bottom: 15px;">To reset your password and access your account either click on the following link below or copy link and paste the link into the address bar of your browser:</p>
                <p style="margin-bottom: 15px;"><!--{Url}--> <a href="'.SUBDOMAIN_URL.'/User/ResetPassword?token='. $forgot_password_token . '">Click here to reset your password.</a></p>
                <p style="margin-bottom: 15px;">This request was made from:</p>
                <!--<p style="margin-bottom: 15px;">IP address: #IPAddress#</p>-->
                <p style="margin-bottom: 15px;">Thank you</p>
                <p style="margin-bottom: 15px;">Apexlink Team</p>
                </div>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" height="10">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640">
                <table id="footer" class="w640" style="border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #00b0f0; color: #ffffff; width: 640px;" border="0" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" colspan="4" valign="top" width="360">
                <p class="footer-content-left" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 13px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; width: 630px;" align="center">ApexLink Property Manager ● <a style="color: #fff; text-decoration: none;" href="javascript:;">support@apexlink.com&nbsp;● 772-212-1950</a></p>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640" height="60">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>';
        return $html;
    }

    /**
     *  function for super admin user reset password
     */
    public function resetPassword() {
        try {
            $actual_password = $_POST['password'];
            $password = md5($_POST['password']);
            $forgot_password_token = $_POST['forgot_password_token'];
            $empty_forgot_password_token = NULL;
            $query = $this->companyConnection->query("SELECT * FROM users WHERE reset_password_token='$forgot_password_token' ");
            $user_email = $query->fetch();
            $email = $user_email['email'];
            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' ");
            $forgot_password_user = $query->fetch();

            if ($user_email) {
                if ($forgot_password_user) {
                    $email = $forgot_password_user['email'];
                    $forgot_password = $password;

                    $update_forgot_password = $this->companyConnection
                        ->prepare("UPDATE users SET password=?, actual_password=? WHERE email=?")
                        ->execute([$forgot_password, $actual_password, $email]);

                    if ($update_forgot_password) {
                        //Reset password in super admin's user table also
                        $query = $this->conn->query("SELECT * FROM users WHERE reset_password_token='$forgot_password_token' ");
                        $super_admin_user_email = $query->fetch();
                        if ($super_admin_user_email) {
                            $update_password_super_admin = $this->conn
                                ->prepare("UPDATE users SET password=?, actual_password=? WHERE email=?")
                                ->execute([$forgot_password, $actual_password, $email]);

                            if ($update_password_super_admin) {
//                                $_SESSION[SESSION_DOMAIN]["message"] = "Password changed successfully.";
                                return array('status' => 'success', 'message' => 'Password changed successfully.');
                            }
                        }
                    } else {
                        return array('status' => 'warning', 'message' => 'Password not changed successfully.');
                    }
                    //return json_decode($response);
                } else {
                    return array('status' => 'error', 'message' => 'This password reset token is invalid.');
                }
            } else {
                return array('status' => 'error', 'message' => 'No user exists with this email-Id.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }



}

$user = new forgotPasswordAjax();
