<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};
class contactPopup extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];

        echo json_encode($this->$action());
    }

    /**
     *  function for Referral Source
     */
    public function savePopUpData()
    {

        try {
            $column = $_POST['colName'];
            $tableName = $_POST['tableName'];
            $value = $_POST['val'];
            $query = "SELECT * FROM `$tableName` WHERE $column = '$value'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();

            if ($checkIfExist !== false) {
                return array('code' => 504, 'status' => 'error', 'message' => 'Record already exist!');
            }

            $data[$_POST['colName']] = $_POST['val'];
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = $_POST['val'];
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Record added successfully.');
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

// getting data from DB


    public function getIntialData()
    {

        try {
            $data = [];

            $data['propertylist'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property")->fetchAll();
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity")->fetchAll();
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
//            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            return ['code' => 200, 'status' => 'success', 'data' => $data];
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * function to get address from zip code
     * @return array
     */
    public function getZipcode()
    {
        try {
            return getSingleRecord($this->companyConnection, ['column' => 'zip_code', 'value' => $_POST['zip_code']], 'zip_code_master');
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



    /**
     * User Table
     */

    public function InsertData()
    {
        try {
            $data = $_POST;
            unset($data['action']);
            unset($data['class']);
            unset($data['custom_field']);
            $files = $_FILES;
            $libraryFiles = $_FILES;
            unset($libraryFiles['file_library']);
            $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];
            if(!empty($custom_field)){
                foreach($custom_field as $key=>$value){
                    $custom_field[$key] = (array) $value;
                }
            }

            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);

            if(!empty($custom_field)){
                foreach ($custom_field as $key=>$value){
                    if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                    if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                    continue;
                }
            }

            $data1 = [];
            $data1['salutation'] = $data['salutation'];
            $data1['referral_source'] = $data['referral'];
            $data1['first_name'] = $data['first_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['nick_name'] = $data['nick_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name'] . ' ' . $data['last_name'];
            $data1['email'] =(!is_array($data['email']))?$data['email']:$data['email'][0];
            $data1['gender'] = $data['gender'];
            $data1['custom_fields'] = (count($custom_field)>0) ? serialize($custom_field):null;
            $data1['ssn_sin_id'] = serialize($data['ssn_sin_id']);
            $data1['address1'] = $data['address1'];
            $data1['address2'] = $data['address2'];
            $data1['address3'] = $data['address3'];
            $data1['address4'] = $data['address4'];
            $data1['veteran_status'] = $data['veteran'];
            $data1['maritial_status'] = $data['marital'];
            $data1['zipcode'] = $data['zipcode'];
            $data1['country'] = $data['country'];
            $data1['state'] = $data['state'];
            $data1['city'] = $data['city'];
            $data1['phone_number_note'] = $data['phone_number_note'];
            $data1['phone_number'] =(!is_array($data['phoneNumber']))?$data['phone_number']:$data['phoneNumber'][0];

            $data1['hobbies'] = (!empty($data['hobbies'])) ? serialize($data['hobbies']):null;

            if(!empty($data['dob']))
            {
                $data1['dob'] = mySqlDateFormat($data['dob'],null,$this->companyConnection);

            }
            else
            {
                $data1['dob'] = null;
            }
            $data1['ethnicity'] = $data['title'];
            $data1['status'] = '1';
            $data1['user_type'] = '5';
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO users(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $idd = $this->companyConnection->lastInsertId();

            $custom_data = ['is_deletable' => '0','is_editable' => '0'];
            $updateColumn = createSqlColValPair($custom_data);
            if(count($custom_field)> 0){
                foreach ($custom_field as $key=>$value){
                    updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                }
            }

            if( isset($data['email'] ) != "") {

                $this->addAdditionalEmail($idd,$data);
            }

            if( isset($data['phoneNumber'] ) != "") {

                $this->addPhoneData($idd,$data);
            }

            if( isset($data['emergency_contact_name'] )  != "") {

                $this->addEmergencyContact($idd,$data);
            }

            if( isset($data['credential_name'] ) != "") {

                $this->addContactDetail($idd,$data);
            }

            if( isset($data['contact_notes'] )!= "") {

                $this->addContactNotes($idd,$data);
            }


            $this->saveContactPropertyData($idd,$data);

            $this->addFileLibrary($idd,$libraryFiles);

            return array('code' => 200,'status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'users' , $idd => 'id');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'users');
            printErrorLog($e->getMessage());
        }


    }

    public function addAdditionalEmail($idd,$data)
    {
        try{



            $data1['user_id'] = $idd;
            $email = $data['email'];


            $email =  $data['email'];
            if (is_array($email)){

                $Count =  count($email);
                if($Count==3){
                    $email1 = $email[0];
                    $email2 = $email[1];
                    $email3 = $email[2];
                }else if($Count==2){
                    $email1 = $email[0];
                    $email2 = $email[1];
                    $email3 = "";
                }else{
                    $email1 = $email[0];
                    $email2 = "";
                    $email3 = "";
                }
                $data1['email1'] = $email1;
                $data1['email2'] = $email2;
                $data1['email3'] = $email3;
            }else{


                $data1['email'] = $data['email'];

            }


            $sqlData = createSqlColVal($data1);

            $query = "INSERT INTO tenant_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            return array('code' => 200, 'status' => 'success','message' => "Data");
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => "something Went wrong on tenant property");
            printErrorLog($e->getMessage());
        }

    }


    public function addEmergencyContact($idd,$data)

    {
        $i = 0;
        if(!empty($data['emergency_contact_name'])){
            if(is_array($data['emergency_contact_name'])) {
                foreach ($data['emergency_contact_name'] as $k => $val1) {

                    $data1 = [];
                    $data1['user_id'] = $idd;
                    $data1['parent_id'] = 0;
                    $data1['emergency_contact_name'] = $data['emergency_contact_name'][$k];
                    $data1['emergency_relation'] = $data['relation'][$k];
                    $data1['emergency_country_code'] = $data['insurance_country_code'][$k];
                    $data1['emergency_phone_number'] = $data['emergency_phone_number'][$k];
                    $data1['emergency_email'] = $data['emergency_email'][$k];
                    $data1['created_at'] = date('Y-m-d H:i:s');
                    $data1['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data1);
                    $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data1);
                    $i++;
                }
            }else{
                $data1['user_id'] = $idd;
                $data1['parent_id'] = 0;
                $data1['emergency_contact_name'] = $data['emergency_contact_name'];
                $data1['emergency_relation'] = $data['relation'][0];
                $data1['emergency_country_code'] = $data['insurance_country_code'][0];
                $data1['emergency_phone_number'] = $data['emergency_phone_number'];
                $data1['emergency_email'] = $data['emergency_email']    ;
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
            }
        }
        return "data inserted";


    }

//

    public function addContactDetail($idd,$data)
    {
        $i = 0;
        // print_r($data);die;
        if(is_array($data['credential_name'])) {

            foreach ($data['credential_name'] as $k => $val1) {
                $data1['user_id'] = $idd;
                $data1['credential_name'] = $data['credential_name'][$k];
                $data1['credential_type'] = $data['credential_type'][$k];
                $data1['notice_period'] = $data['notice_period'][$k];
                if(!empty($data['acquire_date']))
                {
                    $data1['acquire_date'] = mySqlDateFormat($data['dob'],null,$this->companyConnection);

                }
                else
                {
                    $data1['acquire_date'] = null;
                }

                if(!empty($data['expire_date']))
                {
                    $data1['expire_date'] = mySqlDateFormat($data['dob'],null,$this->companyConnection);

                }
                else
                {
                    $data1['expire_date'] = null;
                }
                // $acquire = $data['acquire_date'][$k];
                // print_r($acquire1);die;
                // $ac_date =  mySqlDateFormat($acquire,null,$this->companyConnection);
                // $data1['acquire_date'] = $ac_date;
                // $expire = $data['expire_date'][$k];
                // $ec_date =  mySqlDateFormat($expire,null,$this->companyConnection);
                // $data1['expire_date'] = $ec_date; $data1['notice_period'] = $data['notice_period'][$k];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $i++;
            }
        }

        else{
            $data1['user_id'] = $idd;
            $data1['credential_name'] = $data['credential_name'];
            $data1['credential_type'] = $data['credential_type'];
            if(!empty($data['acquire_date']))
            {
                $data1['acquire_date'] = mySqlDateFormat($data['dob'],null,$this->companyConnection);

            }
            else
            {
                $data1['acquire_date'] = null;
            }

            if(!empty($data['expire_date']))
            {
                $data1['expire_date'] = mySqlDateFormat($data['dob'],null,$this->companyConnection);

            }
            else
            {
                $data1['expire_date'] = null;
            }
//            $acquire = $data['acquire_date'];
//            $ac_date =  mySqlDateFormat($acquire,null,$this->companyConnection);
//            $data1['acquire_date'] = $ac_date;
//            $expire = $data['expire_date'];
//            $ec_date =  mySqlDateFormat($expire,null,$this->companyConnection);
//            $data1['expire_date'] = $ec_date;
            $data1['notice_period'] = $data['notice_period'];
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);

        }
        return "data inserted";

    }



    public function addPhoneData($idd,$data)
    {
        $i = 0;
        if(is_array($data['phoneNumber'])) {

            foreach ($data['phoneNumber'] as $k => $val1) {

                $data1['user_id'] = $idd;
                $data1['parent_id'] = '0';
                $data1['user_type'] = '5';
                $data1['phone_type'] = $data['phoneType'][$k];
                $data1['carrier'] = $data['carrier'][$k];
                $data1['country_code'] = $data['insurance_country_code'][$k];
                $data1['phone_number'] = $data['phoneNumber'][$k];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $i++;
            }
        }

        else{

            $data1['user_id'] = $idd;
            $data1['parent_id'] = '0';
            $data1['user_type'] = '5';
            $data1['phone_type'] = $data['phoneType'];
            $data1['carrier'] = $data['carrier'];
            $data1['country_code'] = $data['insurance_country_code'][0];
            $data1['phone_number'] = $data['phoneNumber'];
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);

        }
        return "data inserted";



    }

    public function addContactNotes($idd,$data)
    {
        $i = 0;
        if(is_array($data['contact_notes'])) {
            foreach ($data['contact_notes'] as $k=>$val1) {

                $data1['user_id'] = $idd;
                $data1['type'] = 1;
                $data1['record_status'] = 2;
                $data1['contact_notes'] = $data['contact_notes'][$k];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO contact_notes(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $i++;
            }
        }
        else{
            $data1['user_id'] = $idd;
            $data1['type'] = 1;
            $data1['record_status'] = 2;
            $data1['contact_notes'] = $data['contact_notes'];
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO contact_notes(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
        }
        return "data inserted";
    }

    /* Save Building Property Unit*/
    public function saveContactPropertyData($idd,$data)
    {
        try{
            $data1['user_id'] = $idd;
            $data1['record_status'] = 0;
            $data1['contact'] = $idd;
            $data1['property_id'] = (!empty($data['property']) && $data['property'] != 'Select')? $data['property']:null;
            $data1['building_id'] = (!empty($data['building']) && $data['building'] != 'Select')? $data['building']:null;
            $data1['unit_id'] = (!empty($data['unit']) && $data['unit'] != 'Select')? $data['unit']:null;
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);

            $query = "INSERT INTO tenant_property(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            return array('code' => 200, 'status' => 'success','message' => "Data");
        }
        catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => "something Went wrong on tenant property");
            printErrorLog($e->getMessage());
        }

    }

    /** Download Sample Contact type excel  */

    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/ContactSample.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }


    /**
     * Import AccountChargeCode Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = ROOT_URL."/uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL. "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `status`, `created_at`, `updated_at`";
                            $status = 1;
                            $extra_values = ",'" . $login_user_id . "','" . $status . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            //Loop through each row of the worksheet
                            $rowData = array();
                            for ($row = 0; $row <= $total_rows; $row++){
                                //  Read a row of data into an array
                                $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
                            }
//
                            $rowRecords = array();
                            $postRecords = array();
                            for ($i = 2; $i < count($rowData); $i++) {
                                $rowRecords['users']['salutation'] = $rowData[$i][0][0];
                                $rowRecords['users']['first_name'] = $rowData[$i][0][1];
                                $rowRecords['users']['middle_name'] = $rowData[$i][0][2];
                                $rowRecords['users']['last_name'] = $rowData[$i][0][3];
                                $rowRecords['users']['domain_name'] = $rowData[$i][0][4];
                                $rowRecords['users']['zipcode'] = $rowData[$i][0][5];
                                $rowRecords['users']['country'] = $rowData[$i][0][6];
                                $rowRecords['users']['state'] = $rowData[$i][0][7];
                                $rowRecords['users']['city'] = $rowData[$i][0][8];
                                $rowRecords['users']['address1'] = $rowData[$i][0][9];
                                $rowRecords['tenant_phone']['phone_type'] = $rowData[$i][0][10];
                                $rowRecords['tenant_phone']['carrier'] = $rowData[$i][0][11];
                                $rowRecords['users']['phone_number'] = $rowData[$i][0][12];
                                $rowRecords['users']['email'] = $rowData[$i][0][13];
                                $rowRecords['contact_notes']['contact_notes'] = $rowData[$i][0][14];
                                $rowRecords['users']['created_at'] = date("Y-m-d h:i:s");
                                $rowRecords['users']['updated_at'] = date("Y-m-d h:i:s");

                                $postRecords[] = $rowRecords;
                                if(empty($rowData[$i][0][1]))
                                {
                                    return ['status'=>'failed','code'=>503,'message'=>'Empty Records!'];

                                }
                                if(empty($rowData[$i][0][3]))
                                {
                                    return ['status'=>'failed','code'=>503,'message'=>'Empty Records!'];

                                }
                                if(empty($rowData[$i][0][12]))
                                {
                                    return ['status'=>'failed','code'=>503,'message'=>'Empty Records!'];

                                }
                                if(empty($rowData[$i][0][13]))
                                {
                                    return ['status'=>'failed','code'=>503,'message'=>'Empty Records!'];

                                }
                            }

                            $returnArray = [];
                            for($i=2;$i< count($rowData);$i++) {
                            //   $first_name=($rowRecords['users']['first_name']);

                                $email=($rowRecords['users']['email']);
                                $last_name=($rowRecords['users']['last_name']);
                                $query = $this->companyConnection->query("SELECT email FROM users WHERE email='$email'");
                                $user_val = $query->fetch();
                                if($user_val !== false){
                                    continue;
                                }

                                $last_id= $this->saveImportRecordUser($rowRecords['users'],'users');
                                $returnArray[] = $this->saveImportRecordPhone($rowRecords['tenant_phone'],'tenant_phone',$last_id);
                                $returnArray[] = $this->saveImportRecord($rowRecords['contact_notes'],'contact_notes',$last_id);
                                //$name = $this->userName($last_id, 'users');

                            }

                            try {
                                if (!empty($returnArray)){
                                    return ['status'=>'success','code'=>200,'message'=>'Contact Imported Successfully!'];
                                }else{
                                    return ['status'=>'failed','code'=>503,'message'=>'Duplicate records found!'];
                                }
                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data1'];
                                printErrorLog($e->getMessage());
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }

    function saveImportRecordUser($rowRecords, $tableName){

        $rowRecords['user_type'] ='5';
        $rowRecords['status'] ='1';

        if($rowRecords['salutation']='Dr')
        {
            $type='1';
        }
        elseif ($rowRecords['salutation']='Mr')
        {
            $type='2';
        }
        elseif ($rowRecords['salutation']='Mrs.')
        {
            $type='3';
        }
        elseif ($rowRecords['salutation']='Mr. & Mrs.')
        {
            $type='4';
        }
        elseif ($rowRecords['salutation']='Ms')
        {
            $type='5';
        }
        elseif ($rowRecords['salutation']='Sir')
        {
            $type='6';
        }
        elseif ($rowRecords['salutation']='Madam')
        {
            $type='7';
        }
        elseif ($rowRecords['salutation']='Brother')
        {
            $type='8';
        }
        elseif ($rowRecords['salutation']='Sister')
        {
            $type='9';
        }
        elseif ($rowRecords['salutation']='Father')
        {
            $type='10';
        }
        elseif ($rowRecords['salutation']='Mother')
        {
            $type='11';
        }
        $rowRecords1['salutation']=$type;
        $rowRecords['first_name'] =$rowRecords['first_name'];
        $rowRecords['middle_name'] =$rowRecords['middle_name'];
        $rowRecords['last_name'] =$rowRecords['last_name'];
        $rowRecords['name'] =$rowRecords['first_name'].' '.$rowRecords['last_name'];
        $rowRecords['domain_name'] = $rowRecords['domain_name'];
        $rowRecords['zipcode'] = $rowRecords['zipcode'];
        $rowRecords['country'] = $rowRecords['country'];
        $rowRecords['state'] = $rowRecords['state'];
        $rowRecords['phone_number'] = $rowRecords['phone_number'];
        $rowRecords['city'] = $rowRecords['state'];
        $rowRecords['address1'] = $rowRecords['address1'];

        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return $this->companyConnection->lastInsertId();

    }


    function saveImportRecordPhone($rowRecords, $tableName,$last_id){
        $carrier=$rowRecords['carrier'];

        $query = $this->companyConnection->query("SELECT id FROM carrier WHERE carrier='$carrier'");
        $data = $query->fetch();
        $rowRecords1['carrier']=$data['id'];

        if($rowRecords['phone_type']='Mobile')
        {
            $p_type='2';
        }
        elseif ($rowRecords['phone_type']='Work')
        {
            $p_type='3';
        }
        elseif ($rowRecords['phone_type']='Fax')
        {
            $p_type='4';
        }
        elseif ($rowRecords['phone_type']='Home')
        {
            $p_type='5';
        }
        elseif ($rowRecords['phone_type']='Other')
        {
            $p_type='6';
        }
        $rowRecords1['phone_type']=$p_type;
        // $rowRecords1['phone_number']=$rowRecords['phone_number'];
        $rowRecords1['user_type'] ='5';
        $rowRecords1['user_id']=$last_id;

        $sqlData = createSqlColVal($rowRecords1);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);

        $stmt->execute($rowRecords1);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }

    function saveImportRecord($rowRecords, $tableName,$last_id){
        $rowRecords['user_id'] = $last_id;
        //  $rowRecords['parent_id'] = '0';
        $rowRecords['record_status'] = '1';
        $rowRecords['type'] = '1';
        $rowRecords['contact_notes'] = $rowRecords['contact_notes'];
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }

    /**
     * function for fetching Data
     * @return array
     */
    public function view(){

        try {
            $id = $_POST['id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            if(isset($login_user_id) && !empty($login_user_id))
            {
                return getDataById($this->companyConnection, 'users', $id);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /*delete data from Contact table select option*/
    public function deleteDataContact() {

        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');

            $sql = "UPDATE users SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];

        }
    }

    /* Full Data For Contact*/

    public function getContactDetail() {

        try{
            $id = $_POST['id'];

            $sql_query1 = "SELECT users. *,
                           tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_veteran_status.veteran as veteran_name
                        FROM users 
                        LEFT JOIN tenant_ethnicity ON users.ethnicity = tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                        WHERE users.id=$id";

            $data = $this->companyConnection->query($sql_query1)->fetch();

            $ssn = unserialize($data['ssn_sin_id']);
            if (is_array($ssn)){
                $ssnValue = implode(',',$ssn);
            }else{
                $ssnValue = $ssn;
            }
           // $data['dob'] = isset($data['dob']) ? dateFormatUser($data['dob'],null,$this->companyConnection) : '';

            if(isset($data['hobbies']) && !empty($data['hobbies'])) {
                $hobbies = isset($data['hobbies']) ? unserialize($data['hobbies']) : '';
                $hobby_name='';

                if(!empty($hobbies)){
                    foreach ($hobbies as $key => $value){
                        $query = 'SELECT hobby FROM hobbies WHERE id='.$value;
                        $groupData = $this->companyConnection->query($query)->fetch();
                        $name = $groupData['hobby'];
                        $hobby_name .= $name.',';
                    }
                    $data["hobbies"] = $hobby_name;
                }else{
                    $data["hobbies"] = "";
                }
            }else{
                $data["hobbies"] = "";
            }
         //   $data['dob'] = isset($data['dob']) ? dateFormatUser($data['dob'],null,$this->companyConnection) : '';

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully','ssn'=>$ssnValue );
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getMaritalData($id) {
        $sql = "SELECT marital FROM  tenant_marital_status WHERE id=".$id;
        $data = $this->companyConnection->query($sql)->fetch();
        // return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
        return $data['marital'];
    }

    public function getEmegencyContactDetail() {


        try{

            $id = $_POST['id'];

            $sql = "SELECT * FROM  emergency_details WHERE user_id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getContactCredentialDetail() {


        try{

            $id = $_POST['id'];

            $sql = "SELECT * FROM  tenant_credential WHERE user_id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();
            $data['acquire_date'] = isset($data['acquire_date']) ? dateFormatUser($data['acquire_date'],null,$this->companyConnection) : '';
            $data['expire_date'] = isset($data['expire_date']) ? dateFormatUser($data['expire_date'],null,$this->companyConnection) : '';
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    /*Contact Property Detail*/

    public function PropertyContactDetail()
    {
        try{

            $query = $this->companyConnection->query("SELECT * FROM general_property");
            $settings = $query->fetchAll();
            $dataAl1['propertyDetails'] =$settings;
            return array('code' => 200, 'status' => 'success', 'data' => $dataAl1, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    /*Contact Building Detail*/
    public function getContactBuildingDetail(){
        $query = "SELECT id, building_name FROM `building_detail` WHERE property_id = ".$_POST['propertyID'];
        $buildingData = $this->companyConnection->query($query)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'data' => $buildingData,'message' => 'Building loaded successfully!');
    }
    /*Contact Unit Detail*/
    public function getContactUnitDetail(){
        $query = "SELECT id, unit_prefix, unit_no FROM `unit_details` WHERE property_id = ".$_POST['propertyID']." && building_id = ".$_POST['buildingID'];
        $unitData = $this->companyConnection->query($query)->fetchAll();
        $newArray = array();
        foreach ($unitData as $key => $value) {
            $query1 = "SELECT unit_id FROM `tenant_property` WHERE unit_id = ".$value['id'];
            $unitData1 = $this->companyConnection->query($query1)->fetch();
            if ($unitData1) {
                continue;
            }
            $newArray[] = $value;
        }

        return array('code' => 200, 'status' => 'success', 'data' => $newArray,'message' => 'Units loaded successfully!');
    }

    /*Method to get maritial status by id*/


    public function getMaritialStatus() {


        try{

            $id = $_POST['id'];
            $sql = "SELECT maritial FROM  emergency_details WHERE id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function addFileLibrary($idd, $fileData) {
        try {
            $files = $fileData;

            $user_id = $idd;

            if (!empty($files)) {

                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                foreach ($files as $key => $value) {

                    $file_name = $value['name'];
                    $fileData = getSingleRecord($this->companyConnection, ['column' => 'name', 'value' => $file_name], 'tenant_file_library');
                    if ($fileData['code'] == '200') {
                        return array('code' => 500, 'status' => 'warning', 'message' => 'Document allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    //  $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $data['user_id'] = $user_id;
                    $data['name'] = $file_name;
//                    $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
//                    $data['marketing_site'] = '0';
//                    $data['file_type'] = '2';
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }

    /* Fetch value by name from id*/

    /*Change Status*/
//Harjinder mam
    public function changeStatusApexNewUser() {
        try {
            //dd("here");
            $data=[];
            $contactuser_id = $_POST['contactuser_id'];
            $status_type = $_POST['status_type'];
            //dd($status_type);
            if ($status_type == "Archive") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = createSqlColValPair($data);
                // dd($sqlData);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='".$contactuser_id."'";
                // dd($query);
                $stmt = $this->companyConnection->prepare($query);
                //$stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Status Updated Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


//Message kaurharjinder


    /* ends*/
    /*count Data*/

    public function getcount() {


        try{

            $data=[];
            $data['tenant'] = $this->companyConnection->query("SELECT COUNT(users.id)  as 'count' FROM users INNER JOIN tenant_details 
     ON tenant_details.user_id = users.id INNER JOIN tenant_lease_details 
     ON tenant_lease_details.user_id = users.id where users.user_type='2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1' ")->fetch();
            $data['owners'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where user_type='4'")->fetch();
            $data['vendors'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where user_type='3'")->fetch();
            $data['users'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where id='1'")->fetch();
            $data['others'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where (user_type!='1' or user_type!='3' or user_type!='4'or user_type!='5') ")->fetch();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

}

$contactPopup = new contactPopup();
