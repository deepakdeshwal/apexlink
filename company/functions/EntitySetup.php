<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/functions/FlashMessage.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class EntitySetup extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * function to add custom field.
     * @return array|void
     */
    public function view(){
        try {
            $user = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
            $company_name = !empty($user['data']['company_name'])?$user['data']['company_name']:'';
            $first_name = !empty($user['data']['first_name'])?$user['data']['first_name']:'';
            $last_name = !empty($user['data']['last_name'])?$user['data']['last_name']:'';
            $middle_name = !empty($user['data']['middle_name'])?$user['data']['middle_name']:'';
            $maiden_name = !empty($user['data']['maiden_name'])?$user['data']['maiden_name']:'';
            $nick_name = !empty($user['data']['nick_name'])?$user['data']['nick_name']:'';
            $plan_size = !empty($user['data']['plan_size'])?$user['data']['plan_size']:'';
            $website = !empty($user['data']['website'])?$user['data']['website']:'';
            $company_logo = !empty($user['data']['company_logo'])?$user['data']['company_logo']:'';
            $signature = !empty($user['data']['signature'])?$user['data']['signature']:'';
            $zipcode = !empty($user['data']['zipcode'])?$user['data']['zipcode']:'';
            $city = !empty($user['data']['city'])?$user['data']['city']:$_SESSION[SESSION_DOMAIN]['default_city'];
            $country = !empty($user['data']['country'])?$user['data']['country']:'';
            $state = !empty($user['data']['state'])?$user['data']['state']:'';
            $address1 = !empty($user['data']['address1'])?$user['data']['address1']:'';
            $address2 = !empty($user['data']['address2'])?$user['data']['address2']:'';
            $address3 = !empty($user['data']['address3'])?$user['data']['address3']:'';
            $address4 = !empty($user['data']['address4'])?$user['data']['address4']:'';
            $tax_id = !empty($user['data']['tax_id'])?$user['data']['tax_id']:'';
            $account_admin_phone_number = !empty($user['data']['account_admin_phone_number'])?$user['data']['account_admin_phone_number']:$user['data']['phone_number'];
            $account_admin_name = !empty($user['data']['account_admin_name'])?$user['data']['account_admin_name']:$first_name.''.$last_name;
            $tax_payer_name = !empty($user['data']['tax_payer_name'])?$user['data']['tax_payer_name']:'';
            $transmission_control_code = !empty($user['data']['transmission_control_code'])?$user['data']['transmission_control_code']:'';
            $account_admin_email_address = !empty($user['data']['account_admin_email_address'])?$user['data']['account_admin_email_address']:$user['data']['email'];
            $application_fees = !empty($user['data']['application_fees'])?$user['data']['application_fees']:$_SESSION[SESSION_DOMAIN]['default_application_fee'];
            $contact_name_1099_efile = !empty($user['data']['contact_name_1099_efile'])?$user['data']['contact_name_1099_efile']:'';
            $rent_amount = !empty($user['data']['rent_amount'])?$user['data']['rent_amount']:$_SESSION[SESSION_DOMAIN]['default_rent'];
            $fax = !empty($user['data']['fax'])?$user['data']['fax']:'';
            $default_payment_method = !empty($user['data']['default_payment_method'])?$user['data']['default_payment_method']:$_SESSION[SESSION_DOMAIN]['payment_method'];
            $number_of_units = !empty($user['data']['number_of_units'])?$user['data']['number_of_units']:'';
            $plan_size = !empty($user['data']['plan_size'])?$user['data']['plan_size']:'';
            $company_logo = !empty($user['data']['company_logo'])?$user['data']['company_logo']:'';
            $signature = !empty($user['data']['signature'])?$user['data']['signature']:'';
            $currency = getCurrencyData($this->companyConnection);
            $default_currency = isset($_SESSION[SESSION_DOMAIN]['default_currency'])? $_SESSION[SESSION_DOMAIN]['default_currency']:'';
            $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$';
            $currencyData = !empty($user['data']['currency'])?$user['data']['currency']:$default_currency;

            $redirect = VIEW_COMPANY."admin/entityCompanySetup/entityAccountSetup.php";
            return require $redirect;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    public function update(){
        try {

            $data = $_REQUEST['data'];
            $data = postArray(serializeToPostArray($data));
            //Required variable array
            $required_array = ['company_name','first_name','last_name','zipcode','state','address1','city','country'];
            /* Max length variable array */
            $maxlength_array = ['company_name'=>50,'first_name'=>20,'last_name'=>100,'zipcode'=>20,'address1'=>250,'city'=>50,'country'=>20,'state'=>50];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            $files = $_FILES;
//            if(empty($files)){
//                if(!isset($files['company_logo']) && !isset($data['previous_logo']))array_push($err_array,['company_logoErr'=>['This field is required']]);
//                if(!isset($files['signature']) && !isset($data['previous_signature']))array_push($err_array,['signatureErr'=>['This field is required']]);
//            }
            if (checkValidationArray($err_array)) {
                echo json_encode(array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!'));
                return;
            } else {
                $user = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                $company_id = CompanyId($this->companyConnection);
                $adminData =[];
                if(isset($data['no_logo'])) $data['company_logo'] = $data['no_logo'];
                //file uploading
                if(isset($data['previous_logo'])) $data['company_logo'] = $data['previous_logo'];
                if(isset($data['previous_signature'])) $data['signature'] = $data['previous_signature'];
                if(!empty($files)) {
                    foreach ($files as $key => $value) {
                        $file_name = $value['name'];
                        $file_tmp = $value['tmp_name'];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                        $name = time() . uniqid(rand());
                        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                    print_r($path);die;
                        //Check if the directory already exists.
                        if (!is_dir(ROOT_URL . '/company/' . $path)) {
                            //Directory does not exist, so lets create it.
                            mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                        }
                        move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                        $data[$key] = $path . '/' . $name . '.' . $ext;
                    }
                }
                //unset data
                unset($data['no_logo']);
                unset($data['previous_logo']);
                unset($data['previous_signature']);
                //declaring additional variables
                $data['name'] = $data['first_name'].' '.$data['last_name'];
                $adminData['name'] = $data['first_name'].' '.$data['last_name'];
                $adminData['first_name'] = $data['first_name'];
                $adminData['company_name'] = $data['company_name'];
                $adminData['middle_name'] = $data['middle_name'];
                $adminData['last_name'] = $data['last_name'];
                $adminData['maiden_name'] = $data['maiden_name'];
                $adminData['nick_name'] = $data['nick_name'];
                $adminData['website'] = $data['website'];
                $adminData['city'] = $data['city'];
                $adminData['state'] = $data['state'];
                $adminData['country'] = $data['country'];
                $adminData['address1'] = $data['address1'];
                $adminData['address2'] = $data['address2'];
                $adminData['address3'] = $data['address3'];
                $adminData['fax'] = $data['fax'];

                //updating user table
                $sqlData = createSqlColValPair($data);
                $company_id = $company_id['data'];



                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$company_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                //updating super admin user table
                $sqlData = createSqlColValPair($adminData);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where email='".$user['data']["email"]."'";
                $stmt = $this->conn->prepare($query);

                if($stmt->execute()) {
                    $defaultData['application_fees']= $data['application_fees'];
                    $defaultData['default_rent']= $data['rent_amount'];
                    $defaultData['zip_code']= $data['zipcode'];
                    $defaultData['country']= $data['country'];
                    $defaultData['state']= $data['state'];
                    $defaultData['city']= $data['city'];
                    $defaultData['currency']= $data['currency'];
                    $defaultData['payment_method']= $data['default_payment_method'];
                    $this->updateDefaultSettings($defaultData);
                    $_SESSION[SESSION_DOMAIN]["message"] = 'Record updated successfully.';
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($company_id,$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                    echo json_encode(array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully'));
                    return;
                } else {
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'Internal Server Error!'));
                    return;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /**
     * function to update default setting on account setup update
     * @param $data
     * @return array|void
     */
    public function updateDefaultSettings($data){
        try{
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];
            $user = getSingleRecord($this->companyConnection ,['column'=>'user_id','value'=>$company_id], 'default_settings');
            $user_id = $company_id;
            $data['user_id'] = $user_id;
            if($user['code'] == 400){
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO default_settings (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $msg = 'Record added successfully';
            } else {
                //updating user table
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE default_settings SET " . $sqlData['columnsValuesPair'] . " where user_id='$company_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $msg = 'Record updated successfully';
            }

            //Default setting session data
            $_SESSION[SESSION_DOMAIN]['default_rent'] = defaultRent($company_id,$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['default_application_fee'] = defaultApplicationfee($company_id,$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['default_zipcode'] = defaultZipCode($company_id,$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['default_city'] = defaultCity($company_id,$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['default_currency'] = defaultCurrency($company_id,$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN]['default_currency'],$this->companyConnection);
            $_SESSION[SESSION_DOMAIN]['payment_method'] = defaultPaymentMethod($company_id,$this->companyConnection);
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg);
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }
}

$user = new EntitySetup();
