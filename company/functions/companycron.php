<?php
/**
 *  Change Status
 */
include_once(ROOT_URL.'/DBConnection.php');
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");

 updatePastAnnouncement();
 function updatePastAnnouncement(){
     $connection = new DBConnection();
    try{
        $timeZone= convertTimeZoneDefaultSeting('',date('Y-m-d H:i:s'),$connection->companyConnection);
        $s = strtotime($timeZone);
        $date = date('Y-m-d', $s);
        $time = date('H:i:s', $s);
        $status = 0;
        $sql = "UPDATE announcements SET status=? WHERE DATE(start_date) <='".$date."' AND TIME(end_time) <'". $time."'";
        $stmt= $connection->prepare($sql);
        $stmt->execute([$status]);
        return ['status'=>'success','code'=>200];
    }catch (Exception $exception)
    {
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        printErrorLog($exception->getMessage());
    }
}