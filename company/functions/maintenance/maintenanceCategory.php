<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class MaintenanceCategoryTypeAjax extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get post data array
     * @param $post
     * @return array
     */
    public function getcategoryList(){


        $data = $_POST['user_id'];
        $query = $this->conn->query("SELECT * FROM company_category_type WHERE id='$data'");
        $user = $query->fetch();

        if(!empty($users)){
            foreach ($users as $key=>$value){
                if(!empty($value['date'])) $users[$key]['date'] = dateFormatUser($value['date'], null);
                continue;
            }
        }
        echo json_encode($user);
        die();
    }







    public function changeStatuscategoryType() {
        try {
            $data=[];
            $company_category_type_id = $_POST['company_category_type_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Disable") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE company_category_type SET ".$sqlData['columnsValuesPair']." where id='".company_category_type_id."'";
                /* echo '<pre>';
                 print_r($query);
                 echo '</pre>';*/


                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Disable Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    /**
     * Create Apexlink List!
     * Use this endpoint to create the apexlink list.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function insert(){
        try {


            $data = [];

            if (isset($_POST['user_id_hidden']) && $_POST['user_id_hidden'] != "") {
                $editId = $_POST['user_id_hidden'];
            } else {
                $editId = '';
            }


            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['category'] = $_POST['category_type'];
            $data['is_default'] = $_POST['is_default'];
            $data['status'] = 1;
            $data['description'] = $_POST['desc'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');


            //Required variable array
            $required_array = ['category', 'description'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            }
            //Checking server side validation

          /*  if (!empty($data['category'])) {
                $id = null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_category_type', 'category', $_POST['category_type'], $id);
                // dd($duplicate);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Category already exists!');
                    }
                }
            }*/

            $dataCategory_type = $_POST['category_type'];
            $duplicateData = $this->companyConnection->query("SELECT * FROM company_category_type WHERE category LIKE '$dataCategory_type'")->fetch();

            if (!empty($duplicateData)) {
                return array('code' => 600, 'status' => 'success', 'message' => 'Category name already exists!');
            } else {
            if ($editId != '') {

                $categoy = getDataById($this->companyConnection, 'company_category_type', $editId);

                $checkStatus = $categoy['data']['status'];

                if ($checkStatus == 0 && $data['is_default']) {

                    return array('code' => 503, 'status' => 'error', 'message' => 'Inactive value can\'t be set as default.');
                }

                $checkIsDefault = $categoy['data']['is_default'];
                if ($checkIsDefault == 1 && $data['is_default'] == 0) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'One default value is required.');
                }
                if ($data['is_default'] == 1) {
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_category_type SET " . $sqlData['columnsValuesPair'];
                    $stmt1 = $this->companyConnection->prepare($query);
                    $stmt1->execute();
                }
                $data1['category'] = $_POST['category_type'];
                $data1['description'] = $_POST['desc'];
                $data1['is_default'] = $_POST['is_default'];
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE company_category_type SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                $stmt1 = $this->companyConnection->prepare($query);
                $stmt1->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'The record updated successfully.');


            }

        }
            if($data['is_default'] == 1){
                $update_data['is_default'] = 0;
                $sqlData = createSqlColValPair($update_data);
                $query = "UPDATE company_category_type SET ".$sqlData['columnsValuesPair'];
                $stmt1 =$this->companyConnection->prepare($query);
                $stmt1->execute();
            }
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO company_category_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record added successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getcategoryType(){
        $data = $_POST['cuser_id'];
        $query = $this->companyConnection->query("SELECT * FROM company_category_type WHERE id='$data'");
        $user = $query->fetch();

        if(!empty($users)){
            foreach ($users as $key=>$value){
                if(!empty($value['date'])) $users[$key]['date'] = dateFormatUser($value['date'], null);
                continue;
            }
        }
        echo json_encode($user);
        die();
    }
   /* public function getbootbox(){


        $query = $this->companyConnection->query("SELECT * FROM company_category_type where is_default='1'");
        $user = $query->fetchAll();



        echo json_encode($user);
        die();
    }*/

    public function update_category_status() {
        try {
            $data=[];
            $cuser_id = $_POST['user_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Activate") {
                $status = 1;
            }else{
                $status = 0;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_category_type WHERE id=?");
                $checkSetDefault->execute([$cuser_id]);
                $checkSetDefault = $checkSetDefault->fetch();

                if(isset($checkSetDefault) || !empty($checkSetDefault)){
                    $isDefault = $checkSetDefault['is_default'];

                    if($isDefault == 1 && $status == 0){
                        return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.');
                    }
                }
                $data['status'] = $status;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_category_type SET ".$sqlData['columnsValuesPair']." where id='".$cuser_id."'";
                /* echo '<pre>';
                 print_r($query);
                 echo '</pre>';*/


                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Disable Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function deletecategoryType() {
        try {
            $data=[];
            $cuser_id = $_POST['user_id'];
            $apexnewuser_id = $_POST['user_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_category_type WHERE id=?");
                $checkSetDefault->execute([$cuser_id]);
                $checkSetDefault = $checkSetDefault->fetch();

                if(isset($checkSetDefault) || !empty($checkSetDefault)){
                    $isDefault = $checkSetDefault['is_default'];

                    if($isDefault == 1){
                        return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deleted.');
                    }
                }
                $data['deleted_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_category_type SET ".$sqlData['columnsValuesPair']." where id='$apexnewuser_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restorepropertyType()
    {

        try {
            $data = $_POST['cuser_id'];
            //  $data = $this->postArray($data);
            //print_r($data); exit;
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];

            $data = array();
            $data['deleted_at'] = NULL ;
            //$data['id'] = $_POST['cuser_id'];

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_category_type SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];

            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}



$categoryTypeAjax = new MaintenanceCategoryTypeAjax();

