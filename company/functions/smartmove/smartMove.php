<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
}

class SmartMove extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getEmailId(){
        try{
            $id = $_POST['id'];
            $data['tenantInfo'] =$this->companyConnection->query("SELECT ud.unit_no,ud.unit_prefix,gp.property_name,gp.address1,
                gp.address2,gp.city,gp.state,gp.zipcode,u.email 
                FROM users u 
                LEFT JOIN tenant_property tp ON tp.user_id = u.id 
                LEFT JOIN general_property gp ON tp.property_id=gp.id 
                LEFT JOIN unit_details ud ON ud.id=tp.unit_id
                WHERE u.id=$id")->fetch();
            $data['company_name'] = $this->companyConnection->query("SELECT company_name FROM users where id=1")->fetch();
            return array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Email found");
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function saveRenterSignUp(){
        try{
            $user_id = 0;

            if (isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && $_SESSION[SESSION_DOMAIN]['cuser_id'] != "") {
                $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            }
            $data['user_id'] = $user_id;
            $data['tenant_id'] = $_POST['tenantid'];
            $data['user_type'] = $_POST['renter'];
            $data['currenttimestamp'] = date('Y-m-d H:i:s');
            $data['status'] = '1';
            $data['email_id'] = $_POST['email'];
            $data['is_agrmnt_accepted'] = $_POST['accept_term'];
            $data['renter_created'] = '0';

            $sqlData = createSqlColVal($data);
            $query = "SELECT * From `sm_landlord` WHERE `user_id` =0 AND `tenant_id` = '".$_POST['tenantid']."' AND `user_type` = 2 AND `email_id` = '".$_POST['email']."'";
            $retult  = $this->companyConnection->query($query)->fetch();
            $last_insert_id = 0;
            if (!$retult) {
                $query = "INSERT INTO `sm_landlord` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $last_insert_id = $this->companyConnection->lastInsertId();
            }
            $return = array('code' => 200, 'status' => 'success', 'data' => $data, 'last_insert_id' => $last_insert_id, 'message' => 'Timestamp saved successfully');
        }catch (Exception $exception){
            $return = array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
        echo json_encode($return); die();
    }

    public function saveRenterAccountdetail(){
        try {
            $rowid = $_POST['rowid'];
            $password = $_POST['sm_password'];
            $data['sm_password'] = md5($password);
            $data['security_question_1'] = $_POST['security_question_1'];
            $data['security_question_2'] = $_POST['security_question_2'];
            $data['security_question_3'] = $_POST['security_question_3'];
            $data['secQstnAns1'] = $_POST['secQstnAns1'];
            $data['secQstnAns2'] = $_POST['secQstnAns2'];
            $data['secQstnAns3'] = $_POST['secQstnAns3'];

            $sqlData = createSqlColValPair($data);
            $query = "UPDATE `sm_landlord` SET ".$sqlData['columnsValuesPair']." where id=".$rowid;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data['last_id'] = $_POST['rowid'];
            $data['tenantid'] = $_POST['tenantid'];
            return ['code' => 200, 'status' => 'success','data' => $data, 'message' => 'Account has been created!'];


        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function getRenterDetails2(){
        try {
            $id = $_POST['id'];
            $rowId = $_POST['rowId'];
            $sql = "SELECT u.id,u.first_name,u.last_name,u.middle_name,u.email,u.dob,u.ssn_sin_id,t.address1,t.address2,t.city,t.state,
            t.zipcode,tphon.work_phone_extension,tphon.phone_number,ll.income,ll.income_term,ll.otherincome,ll.otherincome_term,ll.assets,ll.empmntstatus FROM users u
            JOIN tenant_phone tphon ON u.id = tphon.user_id LEFT JOIN tenant_property tp ON u.id=tp.user_id LEFT JOIN general_property t ON tp.property_id= t.id LEFT JOIN sm_landlord ll ON u.id= ll.tenant_id WHERE u.id=$id AND ll.user_id=0";

            $tenantDetail = $this->companyConnection->query($sql)->fetch();

            $return = array('code' => 200,'status' => 'success', 'data' => '', 'message' => "record not found");
            if (!empty($tenantDetail)){
                $ssn_sin_id = unserialize($tenantDetail['ssn_sin_id']);
                $tenantDetail['ssn_sin_id'] = $ssn_sin_id[0];
                $tenantDetail['dob'] = $tenantDetail['dob'];
                //$tenantDetail['dob'] = $tenantDetail['dob'];
                $tenantDetail['state'] = $this->getStateSortForm($tenantDetail['state']);
                $tenantDetail['phone_number'] = str_replace("-","",$tenantDetail['phone_number']);

                if ($tenantDetail['income_term'] == "1"){
                    $tenantDetail['income_term'] = "Monthly";
                }else{
                    $tenantDetail['income_term'] = "Annual";
                }

                if ($tenantDetail['otherincome_term'] == "1"){
                    $tenantDetail['otherincome_term'] = "Monthly";
                }else{
                    $tenantDetail['otherincome_term'] = "Annual";
                }

                if ($tenantDetail['ssn_sin_id'] != ""){
                    $tenantDetail['ssn_sin_id'] = $tenantDetail['ssn_sin_id'];
                }else{
                    $tenantDetail['ssn_sin_id'] = $this->generateCode(9);
                }

                $curlRequestRes = $this->curlRequests($tenantDetail);
                if ($curlRequestRes['status'] == "success"){

                    $this->saveCreatedSMRenterRecord($id, $rowId);

                    $return = array('code' => $curlRequestRes['code'],'status' => $curlRequestRes['status'], 'data' => $curlRequestRes['data'], 'message' => "Renter created successfully!");
                }else{
                    $return = array('code' => $curlRequestRes['code'],'status' => $curlRequestRes['status'], 'data' => $curlRequestRes);
                }
            }
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function generateCode($limit){
        $code = '';
        for($i = 0; $i < $limit; $i++) { $code .= mt_rand(0, 9); }
        return $code;
    }

    public function getRenterDetails(){
        try {
            $id = $_POST['id'];
            $sql = "SELECT u.id,u.first_name,u.last_name,u.middle_name,u.email,u.dob,u.ssn_sin_id,t.address1,t.address2,t.city,t.state,
            t.zipcode,tphon.work_phone_extension,tphon.phone_number,ll.income,ll.income_term,ll.otherincome,ll.otherincome_term,ll.assets,ll.empmntstatus FROM users u
            JOIN tenant_phone tphon ON u.id = tphon.user_id LEFT JOIN tenant_property tp ON u.id=tp.user_id LEFT JOIN general_property t ON tp.property_id= t.id LEFT JOIN sm_landlord ll ON u.id= ll.tenant_id WHERE u.id=$id";

            $tenantDetail = $this->companyConnection->query($sql)->fetch();

            $return = array('code' => 200,'status' => 'success', 'data' => '', 'message' => "record not fetched");
            if (!empty($tenantDetail)){
                $ssn_sin_id = unserialize($tenantDetail['ssn_sin_id']);
                $tenantDetail['ssn_sin_id'] = $ssn_sin_id[0];
                $return = array('code' => 200,'status' => 'success', 'data' => $tenantDetail, 'message' => "record fetched");
            }
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function saveRenterPersonalInfo(){
        try {
            $rowid = $_POST['rowid'];
            $data['income'] = $_POST['income'];
            $data['income_term'] = $_POST['incomePeriod'];
            $data['otherincome'] = $_POST['otherincome'];
            $data['otherincome_term'] = $_POST['otherIncomePeriod'];
            $data['assets'] = $_POST['assets'];
            $data['empmntstatus'] = $_POST['empmntstatus'];

            $sqlData = createSqlColValPair($data);
            $query = "UPDATE `sm_landlord` SET ".$sqlData['columnsValuesPair']." where id=".$rowid;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data['last_id'] = $_POST['rowid'];
            return ['code' => 200, 'status' => 'success','data' => $data, 'message' => 'Information has been updated!'];
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function saveRenterIdentityVerify(){
        try {
            $cmpny = $this->companyConnection->query("SELECT company_name FROM users where id=1")->fetch();
            $company = base64_encode($cmpny['company_name']);
            $data['company_name'] = $company;
            return ['status' =>'success','code' => 200, 'data' => $data, 'message' => "Email found!"];
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
        }
    }

    public function saveRenterPayment(){
        try {
            $id =$_POST['tenantid'];
            $sql = "SELECT * FROM tenant_property WHERE user_id=$id";
            $tenantPorperty = $this->companyConnection->query($sql)->fetch();
            if (!empty($tenantPorperty)) {
                $propertyId = $tenantPorperty['property_id'];
                $sql = "SELECT * FROM general_property WHERE id=$propertyId";
                $propertyOwner = $this->companyConnection->query($sql)->fetch();
                if (!empty($propertyOwner['owner_id'])) {
                    $oUser = unserialize($propertyOwner['owner_id']);
                }
                if (!empty($oUser)) {
                    if (!empty($oUser[0])) {
                        $Ownername = userName($oUser[0], $this->companyConnection);
                    }
                    $sql = "SELECT id,first_name, last_name,state,city,zipcode,email,address1,address2,address3 FROM users WHERE id=$oUser[0]";
                    $Ownerinfo = $this->companyConnection->query($sql)->fetch();
                    $address3 = $Ownerinfo['city'] . "," . $Ownerinfo['state'] . "," . $Ownerinfo['zipcode'];
                }

                $url = SITE_URL.'company';
                $fileUrls = COMPANY_DIRECTORY_URL . '/views/smartmove/renter/renterEmailTemplate.php';
                $body = file_get_contents($fileUrls);
                $body = str_replace("OWNERNAME", $Ownername, $body);
                $body = str_replace("HOUSENAME", $Ownerinfo['address1'], $body);
                $body = str_replace("STREETADDRESS", $Ownerinfo['address2'], $body);
                $body = str_replace("CITYSTATEZIP", $address3, $body);
                $body = str_replace("SITEURL", $url, $body);
                //  print_r($body); die;
                $email = [];
                array_push($email, $Ownerinfo['email']);
                $request['action'] = 'SendMailPhp';
                $request['to'] = $email;
                $request['subject'] = 'Welcome in SmartMove Renter Screening!';
                $request['message'] = $body;
                $request['portal'] = '1';
                if (!empty($email)) {
                    curlRequest($request);
                    return ['status' => 'success', 'code' => 200, 'data' => $request, 'message' => 'Email sent successfully'];
                } else {
                    return ['status' => 'failed', 'code' => 503, 'data' => 'To email cannot be blank!'];
                }
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }
    public function saveRenterInfo(){
        try{
            $userId = 0;
            if (isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && $_SESSION[SESSION_DOMAIN]['cuser_id'] != "") {
                $userId = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            }
            $data['user_id'] = $userId;
            $data['tenant_id'] = $_POST['tenantid'];
            $data['user_type'] = $_POST['renter'];
            $data['currenttimestamp'] = date('Y-m-d H:i:s');
            $data['status'] = '1';
            $data['email_id'] = $_POST['email'];
            $data['is_agrmnt_accepted'] = $_POST['accept_term'];

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO `sm_landlord` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $return = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record saved successfully');
        }catch (Exception $exception){
            $return = array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
        echo json_encode($return); die();
    }

    public function generateSmartMoveSecurityHeader(){
        $partnerId = 212;
        if (isset($_POST['securityKey']) && $_POST['securityKey'] != ""){
            $securityKey = $_POST['securityKey'];
        }else{
            $securityKey = '';
        }

        $serverTime = $_POST['serverTime'];
        $serverTime = explode('.',$serverTime)[0];
        $message = "{$partnerId}{$serverTime}";
        //Generate HMAC SHA1 hash
        $hmac = hash_hmac("sha1", $message, $securityKey , true);
        $hash = base64_encode($hmac);
        echo json_encode(array('hash_key'=>$hash)); die();
    }

    public function getApplicationStatusData(){
        try{
            $id = $_POST['id'];
            $sql = "SELECT id, application_id, applicants, landlord_pays FROM sm_application WHERE tenant_id=$id";
            $data['appInfo'] =$this->companyConnection->query($sql)->fetch();
            $unserializeEmails = unserialize($data['appInfo']['applicants']);
            $data['appInfo']['applicants'] = $unserializeEmails[0];
            $applicationStatus = $this->checkApplicationStatus($data['appInfo']);
            return ['code' => 200,'status' => 'success', 'data' => $data, 'message' => "",'applicationStatus' => $applicationStatus];
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function checkApplicationStatus($data){
        /*Array(
            [id] => 13
            [application_id] => 311523
            [applicants] => amy44@yopmail.com
            [landlord_pays] => false
        )*/
        $partnerId = 212;
        $serverTime = $this->getServerTime();
        $serverTime = trim($serverTime, '"');
        $serverTime = explode(".",$serverTime)[0];
        $token = $this->generateSmartMoveSecurityHeader2($serverTime, $partnerId);

        $headers = array();
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Accept: application/json, */*; q=0.01';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Authorization: smartmovepartner partnerId="'.$partnerId.'", serverTime="'.$serverTime.'", securityToken="'.$token.'"';
        $headers[] = 'Host: smlegacygateway-integration.mysmartmove.com';
        $headers[] = 'User-Agent: '.$_SERVER['HTTP_USER_AGENT'];

        $url = 'https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/ApplicationRenterStatus';
        $url = $url.'?email='.$data['applicants'].'&applicationId='.$data['application_id'];
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
        $content = curl_exec( $ch );
        $content = json_decode($content);
        curl_close ( $ch );

        if ($content->ApplicationStatus == "NoActivity"){
            $acceptRenterApplicationStatus = $this->acceptOrDeclineRenterApplicationStatus($content, "Accept");
            if ($acceptRenterApplicationStatus != ''){
                return array( 'status' => 'success', 'data' => $acceptRenterApplicationStatus, 'content' => $content);
            }else{
                return array( 'status' => 'error', 'data' => $acceptRenterApplicationStatus, 'content' => $content);
            }
        }
        return array( 'status' => 'error', 'data' => 'No data', 'content' => $content);
        /*0: "{"ApplicationStatus":"NoActivity","IdmaVerificationStatus":null,"RenterEmailAddress":"amy44@yopmail.com","RenterFirstName":null,
        "RenterLastName":null,"ApplicationId":311523}"*/
    }

    public function acceptOrDeclineRenterApplicationStatus($content, $status){
        try{
            $partnerId = 212;
            $serverTime = $this->getServerTime();
            $serverTime = trim($serverTime, '"');
            $serverTime = explode(".",$serverTime)[0];
            $token = $this->generateSmartMoveSecurityHeader2($serverTime, $partnerId);
            $email = $content->RenterEmailAddress;
            $applicationId = $content->ApplicationId;
            $data = array("email" => $email, "applicationId" => $applicationId);
            $data_json = json_encode($data);
            $headers = array();
            $headers[] = 'Connection: Keep-Alive';
            $headers[] = 'Content-Length: '.strlen($data_json);
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Accept: application/json, */*; q=0.01';
            $headers[] = 'Accept-Encoding: gzip, deflate';
            $headers[] = 'Accept-Language: en-US,en;q=0.5';
            $headers[] = 'Authorization: smartmovepartner partnerId="'.$partnerId.'", serverTime="'.$serverTime.'", securityToken="'.$token.'"';
            $headers[] = 'Host: smlegacygateway-integration.mysmartmove.com';
            $headers[] = 'User-Agent: '.$_SERVER['HTTP_USER_AGENT'];

            $url = 'https://smlegacygateway-integration.mysmartmove.com/renterapi/v3/ApplicationRenterStatus/Accept/';
            $url = $url.'?email='.$email.'&applicationId='.$applicationId;
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt( $ch, CURLOPT_TIMEOUT , 200000 );

            $content = curl_exec( $ch );
            $content = json_decode($content);
            $response = curl_getinfo( $ch );
            curl_close ( $ch );
            return $content;
        } catch (Exception $exception) {
            return ['code' => 503, 'status' => 'error', 'data' => '', 'message' => $exception->getMessage()];
        }
    }

    public function saveCreatedSMRenterRecord($tenantId, $rowId){
        try{
            $data['renter_created'] = '1';
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE `sm_landlord` SET ".$sqlData['columnsValuesPair']." WHERE id=".$rowId;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data['last_id'] = $rowId;
            $data['tenantid'] = $tenantId;
            return ['code' => 200, 'status' => 'success','data' => $data, 'message' => 'Renter has been created!'];
        } catch (Exception $exception) {
            return ['code' => 503, 'status' => 'error', 'data' => '', 'message' => $exception->getMessage()];
        }
    }

    public function curlRequests($renterObject) {
        $partnerId = 212;
        $serverTime = $this->getServerTime();
        $serverTime = trim($serverTime, '"');
        $serverTime = explode(".",$serverTime)[0];
        $token = $this->generateSmartMoveSecurityHeader2($serverTime, $partnerId);
        $getRenterObj = $this->getRenterObject($renterObject);
        $getRenterObj = (object)$getRenterObj;

        $url = "https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/Renter";

        try {
            $headr = $this->getHeaders($serverTime,$token, $getRenterObj);
            $resturnRes = $this->get_url( $url, $headr, $getRenterObj);
            if ($resturnRes['status'] == 'success'){
                return array('code' => $resturnRes['data'][1]['http_code'],'status' => $resturnRes['status'], 'data' => $getRenterObj, 'message' => 'Renter created successfully!');
            }else{
                $decodeRes = json_decode($resturnRes['data'][0]);
                $message = $decodeRes->Errors[0];
                return array('code' => $resturnRes['data'][1]['http_code'], 'status' => $resturnRes['status'], 'message' => $message);
            }
        } catch (Exception $exception) {
            echo "<pre>"; print_r($exception->getMessage()); die();
        }
    }

    public function getHeaders($serverTime,$token, $getRenterObj){
        $headers = array();
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-Length: '.strlen(http_build_query($getRenterObj));
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
        $headers[] = 'Accept: application/json, */*; q=0.01';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Accept-Language: en-US,en;q=0.5';
        $headers[] = 'Authorization: smartmovepartner partnerId="212", serverTime="'.$serverTime.'", securityToken="'.$token.'"';
        $headers[] = 'Host: smlegacygateway-integration.mysmartmove.com';
        $headers[] = 'User-Agent: '.$_SERVER['HTTP_USER_AGENT'];

        return $headers;
    }

    public function generateSmartMoveSecurityHeader2($serverTime, $partnerId){
        $securityKey = "387KmgTwB6eUZx8Uwqllp9aao5nNGtMNBRiEYGZc+wAX383gfieO/L5HiNvEE5Lf7DH448ujhItBuU7BJ8CvqA==";
        $message = "{$partnerId}{$serverTime}";
        $hmac = hash_hmac("sha1", $message, $securityKey , true);
        return base64_encode($hmac);
    }

    public function getServerTime(){
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/ServerTime',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 200000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET'
        ));
        return curl_exec($ch);
    }

    public function getRenterObject($renter){
        $renterObj = [];
        $renterObj['Email'] = $renter['email'];
        $renterObj['FirstName'] = "adamtbst";
        $renterObj['MiddleName'] = $renter['middle_name'];
        $renterObj['LastName'] = $renter['last_name'];
        $renterObj['DateOfBirth'] = $renter['dob'];
        $renterObj['SocialSecurityNumber'] = $renter['ssn_sin_id'];
        $renterObj['EmploymentStatus'] = $renter['empmntstatus'];
        $renterObj['StreetAddressLineOne'] = $renter['address1'];
        $renterObj['StreetAddressLineTwo'] = $renter['address2'];
        $renterObj['City'] = $renter['city'];
        $renterObj['State'] = $renter['state'];
        $renterObj['Zip'] = $renter['zipcode'];
        $renterObj['HomePhoneNumber'] = '9841785458';
        $renterObj['OfficePhoneNumber'] = "";
        $renterObj['OfficePhoneExtension'] = "";
        $renterObj['MobilePhoneNumber'] = "";
        $renterObj['Income'] = $renter['income'];
        $renterObj['IncomeFrequency'] = $renter['income_term'];
        $renterObj['OtherIncome'] = $renter['otherincome'];
        $renterObj['OtherIncomeFrequency'] = $renter['otherincome_term'];
        $renterObj['AssetValue'] = $renter['assets'];
        $renterObj['FcraAgreementAccepted'] = 'TRUE';

        return $renterObj;

        /*$renterObj = [];
        $renterObj['Email'] = $renter['email'];
        $renterObj['FirstName'] = $renter['first_name'];
        $renterObj['MiddleName'] = $renter['middle_name'];
        $renterObj['LastName'] = $renter['last_name'];
        $renterObj['DateOfBirth'] = $renter['dob'];
        $renterObj['SocialSecurityNumber'] = $renter['ssn_sin_id'];
        $renterObj['EmploymentStatus'] = $renter['empmntstatus'];
        $renterObj['StreetAddressLineOne'] = $renter['address1'];
        $renterObj['StreetAddressLineTwo'] = $renter['address2'];
        $renterObj['City'] = $renter['city'];
        $renterObj['State'] = $renter['state'];
        $renterObj['Zip'] = $renter['zipcode'];
        $renterObj['HomePhoneNumber'] = $renter['phone_number'];
        $renterObj['OfficePhoneNumber'] = "";
        $renterObj['OfficePhoneExtension'] = "";
        $renterObj['MobilePhoneNumber'] = "";
        $renterObj['Income'] = $renter['income'];
        $renterObj['IncomeFrequency'] = $renter['income_term'];
        $renterObj['OtherIncome'] = $renter['otherincome'];
        $renterObj['OtherIncomeFrequency'] = $renter['otherincome_term'];
        $renterObj['AssetValue'] = $renter['assets'];
        $renterObj['FcraAgreementAccepted'] = 'TRUE';
        return $renterObj;*/
    }

    public function get_url( $url, $headr, $getRenterObj, $javascript_loop = 0, $timeout = 180 ){
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headr );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($getRenterObj) );
        $content = curl_exec( $ch );
        $response = curl_getinfo( $ch );
        curl_close ( $ch );
        if ($response['http_code'] != 201){
            return array( 'status' => 'error', 'data' => array($content, $response));
        }else{
            return array( 'status' => 'success', 'data' => array($content, $response));
        }
    }

    public function getStateSortForm($state){
        $stateArray = array("AL" => "Alabama","AK" => "Alaska","AZ" => "Arizona","AR" => "Arkansas","CA" => "California","CO" => "Colorado","CT" => "Connecticut","DE" => "Delaware","FL" => "Florida","GA" => "Georgia","HI" => "Hawaii","ID" => "Idaho","IL" => "Illinois","IN" => "Indiana","IA" => "Iowa","KS" => "Kansas","KY" => "Kentucky","LA" => "Louisiana","ME" => "Maine","MD" => "Maryland","MA" => "Massachusetts","MI" => "Michigan","MN" => "Minnesota","MS" => "Mississippi","MO" => "Missouri","MT" => "Montana","NE" => "Nebraska","NV" => "Nevada","NH" => "New Hampshire","NJ" => "New Jersey","NM" => "New Mexico","NY" => "New York","NC" => "North Carolina","ND" => "North Dakota","OH" => "Ohio","OK" => "Oklahoma","OR" => "Oregon","PA" => "Pennsylvania","RI" => "Rhode Island","SC" => "South Carolina","SD" => "South Dakota","TN" => "Tennessee","TX" => "Texas","UT" => "Utah","VT" => "Vermont","VA" => "Virginia","WA" => "Washington","WV" => "West Virginia","WI" => "Wisconsin","WY" => "Wyoming","AS" => "American Samoa","DC" => "District of Columbia","FM" => "Federated States of Micronesia","GU" => "Guam","MH" => "Marshall Islands","MP" => "Northern Mariana Islands","PW" => "Palau","PR" => "Puerto Rico","VI" => "Virgin Islands");
        foreach ($stateArray as $key => $value){
            if ($state == $value){
                return $key;
            }
        }
        return "NY";
    }

    public function saveTransactionDetails(){
        try{
            $charge_id = $_POST['charge_id'];
            $chargeData = $_POST['charge_data'];
            $userId = $_POST['user_id'];

            $propertyId =$this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id=$userId")->fetch();
            $propertyId = $propertyId['property_id'];
            $data['transaction_id'] = $chargeData['balance_transaction'];
            $data['charge_id'] = $charge_id;
            $data['user_type'] = 'TENANT';
            $data['user_id'] = $userId;
            $data['amount'] = $chargeData['amount'];
            $paymentStatus = "ERROR";
            if(isset($chargeData['status']) && $chargeData['status'] == "succeeded"){
                $paymentStatus = "SUCCESS";
            }
            $data['payment_status'] = $paymentStatus;
            $data['payment_response'] = 'Payment has been done successfully';
            $data['payment_mode'] = 'one_time';
            $data['property_id'] = $propertyId;
            $data['type'] = 'SM';
            $data['payment_type'] = 'card';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO `transactions` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record saved successfully');
        } catch (Exception $exception) {
            return ['code' => 503, 'status' => 'error', 'data' => '', 'message' => $exception->getMessage()];
        }
    }

    public function getIDMAExams(){
        $id = $_POST['id'];
        $sql = "SELECT u.id,u.first_name,u.last_name,u.middle_name,u.email,u.dob,u.ssn_sin_id,t.address1,t.address2,t.city,t.state,
            t.zipcode,tphon.work_phone_extension,tphon.phone_number,ll.income,ll.income_term,ll.otherincome,ll.otherincome_term,ll.assets,ll.empmntstatus FROM users u
            JOIN tenant_phone tphon ON u.id = tphon.user_id LEFT JOIN tenant_property tp ON u.id=tp.user_id LEFT JOIN general_property t ON tp.property_id= t.id LEFT JOIN sm_landlord ll ON u.id= ll.tenant_id WHERE u.id=$id AND ll.user_id=0";
        $tenantDetail = $this->companyConnection->query($sql)->fetch();
        if (!empty($tenantDetail)){
            $ssn_sin_id = unserialize($tenantDetail['ssn_sin_id']);
            $tenantDetail['ssn_sin_id'] = $ssn_sin_id[0];
            $tenantDetail['dob'] = $tenantDetail['dob'];
            $tenantDetail['state'] = $this->getStateSortForm($tenantDetail['state']);
            $tenantDetail['phone_number'] = str_replace("-","",$tenantDetail['phone_number']);

            if ($tenantDetail['income_term'] == "1"){
                $tenantDetail['income_term'] = "Monthly";
            }else{
                $tenantDetail['income_term'] = "Annual";
            }

            if ($tenantDetail['otherincome_term'] == "1"){
                $tenantDetail['otherincome_term'] = "Monthly";
            }else{
                $tenantDetail['otherincome_term'] = "Annual";
            }

            if ($tenantDetail['ssn_sin_id'] != ""){
                $tenantDetail['ssn_sin_id'] = $tenantDetail['ssn_sin_id'];
            }else{
                $tenantDetail['ssn_sin_id'] = $this->generateCode(9);
            }

            $partnerId = 212;
            $serverTime = $this->getServerTime();
            $serverTime = trim($serverTime, '"');
            $serverTime = explode(".",$serverTime)[0];
            $token = $this->generateSmartMoveSecurityHeader2($serverTime, $partnerId);
            $getRenterObj = $this->getRenterObject($tenantDetail);
            $getRenterObj = (object)$getRenterObj;
            $headr = $this->getHeaders($serverTime,$token, $getRenterObj);

            $url = 'https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/Exam/Retrieve';
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headr );
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($getRenterObj) );
            $content = curl_exec( $ch );
            $content = json_decode($content);
            $response = curl_getinfo( $ch );
            curl_close ( $ch );
            if ($response['http_code'] != 200){
                return array( 'code' => 503, 'status' => 'error', 'content' => $content);
            }else{
                return array('code' => 200,  'status' => 'success', 'content' => $content);
            }
        }
        return array( 'status' => 'error', 'code' => 503, 'response' => '');
    }
}
$smartMove = new SmartMove();