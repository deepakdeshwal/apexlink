<?php

/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 1/17/2019
 * Time: 3:30 PM
 */
include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");

//include_once ('constants.php');
//include_once (ROOT_URL.'/vendor/jqGridPHP-master/php/jqGridLoader.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
};

/**
 * JqGrid Server Side Implementation
 * Class jqGrid
 */
class jqGrid extends DBConnection
{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        $tableArray =  ['general_property' => ['parent_module'=>'Properties','module'=>'View Property']];
       // dd($_REQUEST);
        echo json_encode($this->$action());
    }

    /**
     * function to display jqGrid listing dynamic
     */
    public function listing_ajax()
    {
        try {
        //Required params
        $page = $_REQUEST['page']; // get the requested page
        $limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
        $sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
        $sord = $_REQUEST['sord']; // get the direction
        $deleted_at = isset($_REQUEST['deleted_at'])? $_REQUEST['deleted_at'] : 'true';
        $groupBy = isset($_REQUEST['groupBy'])?$_REQUEST['groupBy']:NULL;
        $groupByCondition = !empty($groupBy)?' GROUP BY '.$groupBy.' ':'';
        // Db details for jq grid
        $table = $_REQUEST['table'];
        $select = isset($_REQUEST['select'])?$_REQUEST['select']:null;
        $columns_options = $_REQUEST['columns_options'];
        $columns = $this->get_columns_info($columns_options);
        $sidx = $this->get_sidx($sidx,$columns);
        $splitUpdate =  explode('.',$sidx);

        if(isset($splitUpdate[1]) && ($splitUpdate[1] == 'updated_at' || $splitUpdate[1] == 'update_at')){
            $trimColumn = $columns[0]['table'].'.'.$columns[0]['original'];
            $sidx = ' '.$sidx.'=(SELECT MAX('.$splitUpdate[1].') FROM '.$splitUpdate[0].') DESC, TRIM('.$trimColumn.') ';
            $sord = ' ASC ';
        }

        $extra_dropdown = (isset($_REQUEST['extra_dropdown']) && !empty($_REQUEST['extra_dropdown']))?$_REQUEST['extra_dropdown']:'0';
        if (!$sidx) $sidx = 1;
        $status = isset($_REQUEST['status'])?$_REQUEST['status']:'All';
        $deleted_at_where = '';
        $extra_where = isset($_REQUEST['extra_where'])?$_REQUEST['extra_where']:null;

        //Specifying joins if any
        $joins = (isset($_REQUEST['joins']) && !empty($_REQUEST['joins']))?$this->build_join_query($_REQUEST['joins']):$this->build_join_query([]);


        $extra_columns = (isset($_REQUEST['extra_columns']) && !empty($_REQUEST['extra_columns']))? $this->build_extra_columns($_REQUEST['extra_columns']):'';

        //building single where condition for ignore list and status
        $ignore_data = (isset($_REQUEST['ignore']) && !empty($_REQUEST['ignore']))? $_REQUEST['ignore']: null;
        if ($deleted_at == 'true') $deleted_at_where = ' WHERE deleted_at IS NULL';
        if ($deleted_at == 'false') $deleted_at_where = ' WHERE deleted_at IS NOT NULL';
        $combined_where = $this->get_combined_where($ignore_data,$status,$table,$deleted_at_where,$extra_dropdown,$extra_where);
        //building columns data
        $columns_string = $this->build_column_string($columns);
        //dd($columns_string);
        // filter data.
        if ($_REQUEST['_search'] == 'true')
        {
            $filters = (isset($_REQUEST['filters']) && !empty($_REQUEST['filters']))? json_decode($_REQUEST['filters']) : null;
            if(!empty($filters)){
                $whereString = " WHERE (";
                $allempty = 'true';
                $con = ' AND';
                foreach ($filters as $key=>$value) {
                    if($value->data == 'all') continue;
                    $searchField = ($this->getAdvanceSearchColumn($value->field, $columns) != '') ? $this->getAdvanceSearchColumn($value->field, $columns) : $value->field;
                    $searchString = $value->data;
                    $searchString2 = (isset($value->data2) && !empty($value->data2))?$value->data2:null;
                    if ((strtolower($value->field) == 'status' && strtolower($searchString) == 'active') || (strtolower($value->field) == 'is_default' && strtolower($searchString) == 'yes') || (strtolower($value->field) == 'posting_status' && strtolower($searchString) == 'posting')) $searchString = 1;
                    if ((strtolower($value->field) == 'status' && strtolower($searchString) == 'inactive') || (strtolower($value->field) == 'is_default' && strtolower($searchString) == 'no') || (strtolower($value->field) == 'posting_status' && strtolower($searchString) == 'nonposting')) $searchString = 0;
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'tenant')) $searchString = '2';
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'vendor')) $searchString = '3';
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'owner')) $searchString = '4';
                    $searchOper = $value->op;
                    $int = isset($value->int)?$value->int:null;
                    //Advance search where condition
                    $whereFilters = $this->advance_search_filters($searchField, $searchString, $searchOper,$int,$searchString2);
                    $whereString .= $whereFilters;
                    if($value->data != 'all'){
                        $allempty = 'false';
                    }

                    if(isset($value->con)){
                        $con = $value->con;
                    } else {
                        $con = 'AND';
                    }
                    $whereString .= ' '.$con;
                }
                $whereStringBefore = str_replace_last(' '.$con, '', $whereString);
                $whereStringBefore .= ")";
                if($allempty == 'true'){
                    $whereStringBefore = "";
                }
                $where = $whereStringBefore;

            } else {
                $searchField = $this->getAdvanceSearchColumn($_REQUEST['searchField'], $columns);
                $searchString = $_REQUEST['searchString'];
                if((strtolower($_REQUEST['searchField']) == 'status' && strtolower($searchString) == 'active') || (strtolower($_REQUEST['searchField']) == 'is_default' && strtolower($searchString) == 'yes') || (strtolower($_REQUEST['searchField']) == 'posting_status' && strtolower($searchString) == 'posting')) $searchString = 1;
                if((strtolower($_REQUEST['searchField']) == 'status' && strtolower($searchString) == 'inactive') || (strtolower($_REQUEST['searchField']) == 'is_default' && strtolower($searchString) == 'no') || (strtolower($_REQUEST['searchField']) == 'posting_status' && strtolower($searchString) == 'nonposting')) $searchString = 0;
                if ((strtolower($_REQUEST['searchField']) == 'announcement_for' && strtolower($searchString) == 'tenant')) $searchString = '2';
                if ((strtolower($_REQUEST['searchField']) == 'announcement_for' && strtolower($searchString) == 'vendor')) $searchString = '3';
                if ((strtolower($_REQUEST['searchField']) == 'announcement_for' && strtolower($searchString) == 'owner')) $searchString = '4';
                if($searchField == 'unit_details.building_unit_status') $searchString = buildingStatusFormatter($searchString);
                if($searchField == 'task_reminders.status') $searchString = taskReminderStatusSearch($searchString);
                if($searchField == 'phone_call_logs.follow_up_needed') $searchString = phoneCallLogNeededSearch($searchString);
                if($searchField == 'phone_call_logs.incoming_outgoing') $searchString = phoneCallLogIncomingOutgoingSearch($searchString);
                $searchOper = $_REQUEST['searchOper'];
                //Advance search where condition
                $where = $this->advance_search($searchField,$searchString,$searchOper);
            }
        }

        $totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;


        if ($totalrows) {
            $limit = $totalrows;
        }


        if(empty($combined_where)){
            $sql = 'SELECT '.$extra_columns.' '.$table.'.id, '.$columns_string.' FROM '.$table. $joins.$groupByCondition.$deleted_at_where;
        } else {
            $sql = 'SELECT '.$extra_columns.' '.$table.'.id, '.$columns_string.' FROM '.$table. $joins.$groupByCondition.$combined_where;
        }

        if(isset($where)){
            if(empty($combined_where)){
                if($deleted_at === true){
                    $where = str_replace("WHERE ","",$where);
                    $sql = $sql.' AND '.$where;
                    if(empty($where)) {
                        $sql = str_replace_last(' AND ', '', $sql);
                    }
                } else {
                    $sql = $sql . $where;
                }
            } else {

                $where = str_replace("WHERE ","",$where);
                $sql = $sql.' AND '.$where;
                if(empty($where)) {
                    $sql = str_replace_last(' AND ', '', $sql);
                }
            }
        }

     // dd($sql);

        $result = $this->companyConnection->query($sql);
        $users = $result->fetchAll();
        $row = $users;
        $count = count($row);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }


        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
        if(isset($where)){
            $SQL = $this->companyConnection->query("$sql $groupByCondition ORDER BY $sidx $sord LIMIT $limit");
        } else {
            if(empty($combined_where)){
                $SQL = $this->companyConnection->query("$sql $groupByCondition ORDER BY $sidx $sord LIMIT $start , $limit");
            } else {
                $wq = "$sql $groupByCondition ORDER BY $sidx $sord LIMIT $start , $limit";
            
                $SQL = $this->companyConnection->query("$sql $groupByCondition ORDER BY $sidx $sord LIMIT $start , $limit");
            }
        }
        $data = $SQL->fetchAll();
      //  dd($SQL);

        //  dd($data);
        //making column data
        $json_data = [];
        if (count($data) > 0){
            foreach ($data as $key => $value) {
                // dd($value['gprop_name']);
                //buid dynamic data
                $attr = null;
                $json_data[$key]['id'] = $value['id'];
                $change_type = (isset($value['is_editable']) && $value['is_editable'] == 0)? $value['is_editable'] : 1;
                $type = isset($value['codec'])? $value['codec'] : 0;

                foreach ($columns as $key1 => $value1){
                    //Dynamic attr
                    $attr = (!empty($value1['attr']))?$value1['attr']:$attr;
                    $original_columns_name = !empty($value1['original_index'])?$value1['original_index']:$value1['original'];
                    $alias_column = (isset($value1['alias']) && !empty($value1['alias']))?$value1['alias']:$original_columns_name;
                    if(($value1['original'] == 'created_at' || $value1['original'] == 'updated_at' || $value1['original'] == 'deleted_at') && empty($value1['change'])){
                        if($value[$value1['original']] != null) {
                            if($value1['change'] == 'date_time_format'){
                                $json_data[$key][$value1['display']] = ($value[$value1['original']] != null && $value[$value1['original']] != '') ? dateFormatUser($value[$alias_column], null,$this->companyConnection).' '.timeFormat($value[$alias_column], null,$this->companyConnection) :'';
                            } else if($value1['change'] == 'updated_at_time'){
                                $json_data[$key][$value1['display']] = ($value[$value1['original']] != null && $value[$value1['original']] != '') ? timeFormat($value[$alias_column], null,$this->companyConnection) :'';
                            } else {
                                $json_data[$key][$value1['display']] = dateFormatUser($value[$value1['original']], '1',$this->companyConnection);
                            }
                        }
                        continue;
                    }
                    // dd($value);
                    if(!empty($value1['change'])){
                        if($value1['change'] == 'date'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? dateFormatUser($value[$alias_column], '1',$this->companyConnection) :'';
                            continue;
                        } elseif($value1['change'] == 'time'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? timeFormat($value[$alias_column], null,$this->companyConnection) :'';
                            continue;
                        } elseif($value1['change'] == 'name'){
                            $idName = (isset($value[$value1['name_id']]) && !empty($value[$value1['name_id']]))?$value[$value1['name_id']]:$value['id'];
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? userName($idName,$this->companyConnection,$value1['table']) :'';
                            continue;
                        } elseif($value1['change'] == 'line'){
                            $column = !empty($value1['index2'])?$value1['index2']:$value1['original'];
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $this->getchilds($value['id'],$value1['table'],$column,'line',$value1['join']):'';
                            continue;
                        } elseif($value1['change'] == 'serialize'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ?  $this->changeTypeSerialize($value[$alias_column],$value1,$value1['type'],$value1['join'],$value['id']):'';
                            continue;
                        } elseif($value1['change'] == 'combine_column_line'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ?  $this->combineColumnline($value1['extra_columns'],$value,$value1):'';
                            continue;
                        } elseif($value1['change'] == 'last_login'){
                            $json_data[$key][$value1['display']] = lastLogin($value['id'], $this->companyConnection);
                            continue;
                        } elseif($value1['change'] == 'combine_column_hyphen'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ?  $this->combineColumnHyphen($value1['extra_columns'],$value,$value1):'';
                            continue;
                        } elseif($value1['change'] == 'combine_column_hyphen2'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ?  $this->combineColumnHyphen2($value1['extra_columns'],$value,$value1):'';
                            continue;
                        } elseif($value1['change'] == 'tenantname1'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ?  $this->tenantname1($value1['extra_columns'],$value):'';
                            continue;
                        } elseif($value1['change'] == 'last_name_first'){
                            $json_data[$key][$value1['display']] = ($value[$value1['original']] != null && $value[$value1['original']] != '') ? lastNameFirst($value['email'],$this->companyConnection) :'';
                            continue;
                        } elseif($value1['change'] == 'line_multiple'){
                            $idName = (isset($value[$value1['name_id']]) && !empty($value[$value1['name_id']]))?$value[$value1['name_id']]:$value['id'];
                            $json_data[$key][$value1['display']] = ($value[$value1['original']] != null && $value[$value1['original']] != '') ? $this->multipleTypeLine($idName,$value1['join'],$value1['index2'],$value1['index3']) :'';
                            continue;
                        } elseif($value1['change'] == 'property_vacant'){
                            $json_data[$key][$value1['display']] = $this->propertyVacant($value,$value1);
                            continue;
                        } elseif($value1['change'] == 'subscription_date'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? subscriptionDate($value[$alias_column]) :'';
                            continue;
                        } elseif($value1['change'] == 'check_week_date'){
                            // dd($value);
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $this->checkWeekDate($value['user_id'],$value1['weekType']) :'';
                            continue;
                        } elseif($value1['change'] == 'Count_tenant_move_out'){
                            // dd($value);
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $this->Count_tenant_move_out($value1['weekType']) :'';
                            continue;
                        } elseif($value1['change'] == 'count_workOrders'){
                            //dd($value);
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $this->count_workOrders($value['property_id'],$value1['type']) :'';
                            continue;
                        } elseif($value1['change'] == 'invoice_amount_due'){
                            $json_data[$key][$value1['display']] = $this->invoiceAmountDate($value['id'],$value1['secondTable'],$value1);
                            continue;
                        } elseif($value1['change'] == 'count_remaining_days'){
                            $json_data[$key][$value1['display']] = $this->getdaysremaining($value['id'],$value['user_id']);
                            continue;
                        } elseif($value1['change'] == 'short_term_status'){
                            $json_data[$key][$value1['display']] = $this->shorttermstatus($value['id'],$value['user_id']);
                            continue;
                        } elseif($value1['change'] == 'payment_plan_status'){
                            $json_data[$key][$value1['display']] = $this->paymentplanstatus($value['id'],$value['installments']);
                            continue;
                        } elseif($value1['change'] == 'amount_due'){
                            $json_data[$key][$value1['display']] = $this->amountdue($value['id']);
                            continue;
                        } elseif($value1['change'] == 'nextamount'){
                            $json_data[$key][$value1['display']] = $this->nextamount($value['id']);
                            continue;
                        } elseif($value1['change'] == 'next_date'){
                            $json_data[$key][$value1['display']] = $this->nextdate($value['id']);
                            continue;
                        } elseif($value1['change'] == 'journal_entry_account'){
                            $json_data[$key][$value1['display']] = $this->getJournalEntries($value['id'],$value1['table'],'Account');
                            continue;
                        } elseif($value1['change'] == 'journal_entry_debit'){
                            $json_data[$key][$value1['display']] = $this->getJournalEntries($value['id'],$value1['table'],'Debit');
                            continue;
                        } elseif($value1['change'] == 'journal_entry_credit'){
                            $json_data[$key][$value1['display']] = $this->getJournalEntries($value['id'],$value1['table'],'Credit');
                            continue;
                        } elseif($value1['change'] == 'periodTotal'){

                            $json_data[$key][$value1['display']] = $this->paidTotal($value['id'],$value['owner_id'],$value['property_id'],$value1['dateBetween']);
                            continue;
                        } elseif($value1['change'] == 'recommendedDisbursement'){
                            $json_data[$key][$value1['display']] = $this->recommendedDisbursement($value['id'],$value['owner_id'],$value['property_id'], $value1['dateBetween']);
                            continue;
                        }  elseif($value1['change'] == 'count_Amount'){
                            //dd($value);
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $this->count_Amount($value['id']) :'';
                            continue;
                        }  elseif($value1['change'] == 'unit_vacant_count'){
                            //dd($value);
                            $json_data[$key][$value1['display']] = $this->unit_vacant_count($value['id']);
                            continue;
                        } elseif($value1['change'] == 'flagged_for_name'){
//                            dd($value['object_type']);
                            $json_data[$key][$value1['display']] = $this->flagged_for_name($value['object_id'], $value['object_type']);
                            continue;
                        } elseif($value1['change'] == 'bank_balance'){
                            $json_data[$key][$value1['display']] = $this->current_bank_balance($value['bank_id'],$value['initial_amount']);
                            continue;
                        } elseif($value1['change'] == 'dynamic_property'){
                            $json_data[$key][$value1['display']] = $this->dynamic_property($value[$alias_column]);
                            continue;
                        } elseif($value1['change'] == 'defaultBankAccount'){
                            $json_data[$key][$value1['display']] = property_default_bank_account($this->companyConnection,$value[$alias_column]);
                            continue;
                        } elseif($value1['change'] == 'defaultBankAccountId'){
                            $json_data[$key][$value1['display']] = property_default_bank_account_id($this->companyConnection,$value[$alias_column]);
                            continue;
                        } elseif($value1['change'] == 'property_owner'){
                            $json_data[$key][$value1['display']] = $this->propertyOwner($value[$alias_column]);
                            continue;
                        }
                    }
                    $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $value[$alias_column] :'';
                }
                $status = isset($value['status'])? $value['status']: null;
                $json_data[$key]['Action'] = $this->build_select($select ,$value['id'],$status,$change_type,$type,$attr,$value);
            }
        }

        $response = [];
        $response['page'] = $page;
        $response['total'] = $total_pages;
        $response['records'] = $count;
        $response['rows'] = $json_data;
        echo json_encode($response);
        die;
        } catch (Exception $exc){
            dd($exc);
        }
    }

    /**
     * Building select box for jqGrid rows
     * @param $select
     * @param $id
     * @param null $status
     * @return string
     */
    public function build_select($select ,$id,$status=null,$change_type,$type,$attr,$value){
        if(empty($select)) return '';
        $displayAttrArray = [];
        if (!empty($attr)) {
            foreach ($attr as $key=>$dataValue) {
                switch ($dataValue['name']) {
                    case "flag":
                        $optional_id = isset($dataValue['optional_id']) && !empty($dataValue['optional_id'])?$value[$dataValue['optional_id']]:$id;
                        $flag = $this->propertyFlag($optional_id, $dataValue['name'], $dataValue['value']);
                        $displayAttrName = $dataValue['name'];
                        $displayAttr = $flag;
                        break;
                    default:
                        $displayAttrName = $dataValue['name'];
                        $attrValue = isset($value[$dataValue['value']]) && !empty($value[$dataValue['value']]) ? $value[$dataValue['value']] : 'null';
                        $displayAttr = $attrValue;
                        break;
                }
                array_push($displayAttrArray,$displayAttrName.'='.$displayAttr);
            }
        }
        $attrAction = '';

        if(!empty($displayAttrArray)) {
            foreach ($displayAttrArray as $key => $value) {
                $attrAction .= $value.' ';
            }
            $attrAction = str_replace_last(' ', '', $attrAction);
        }

        $data = '<select '.$attrAction.' type="'.$type.'" editable="'.$change_type.'" class="form-control select_options" data_id="'.$id.'"><option value="default">SELECT</option>';
        foreach ($select as $key=>$value){
            if($change_type == '0' && ($value == 'Delete' || $value == 'delete')) continue;
            if($status != null && $value == 'status') {
                $value = $status==0?'Activate':'Deactivate';
            }
            $data .= '<option value="'.$value.'">'.strtoupper($value).'</option>';
        }
        $data .= '</select>';
        return $data;
    }

    /**
     * Extracting displayed and origingal columns names
     * @param $columns_options
     * @return array
     */
    public function get_columns_info($columns_options){
        $data = [];
        foreach ($columns_options as $key=>$value){
            $table = isset($value['table'])?$value['table']:'';
            $column_alias = '';
            if($value['name'] == 'Action') continue;
            if(!empty($value['as'])) $table = $value['as'];
            if(!empty($value['alias'])) $column_alias = ' AS '.$value['alias'];
            $arr = ['original'=>$value['index'],'display'=> $value['name'], 'columns'=>$table.".".$value['index'].$column_alias, 'table'=>$value['table'],'change'=>isset($value['change_type'])?$value['change_type']:null,'as'=>isset($value['as'])?$value['as']:null,'join'=>isset($value['join'])?$value['join']:null,'index2'=>isset($value['index2'])?$value['index2']:null,'index3'=>isset($value['index3'])?$value['index3']:null,'secondTable'=>isset($value['secondTable'])?$value['secondTable']:null,'extra_columns'=>isset($value['extra_columns'])?$value['extra_columns']:null,'search_type'=>isset($value['search_type'])?$value['search_type']:null,'update_column'=>isset($value['update_column'])?$value['update_column']:null,'original_index'=>isset($value['original_index'])?$value['original_index']:null,'attr'=>isset($value['attr'])?$value['attr']:null,'alias'=>isset($value['alias'])?$value['alias']:null,'name_id'=>isset($value['name_id'])?$value['name_id']:null,'type'=>isset($value['type'])?$value['type']:null,'weekType'=>isset($value['weekType'])?$value['weekType']:null,'dateBetween'=>isset($value['dateBetween'])?$value['dateBetween']:null,'accountType'=>isset($value['accountType'])?$value['accountType']:'company'];
            array_push($data,$arr);
        }
        return $data;
    }

    /**
     * Advance Search functionality for jqGrid
     * @param $searchField
     * @param $searchString
     * @param $searchOper
     * @return string
     */
    public function advance_search($searchField,$searchString,$searchOper){
        $where = " WHERE (";
        $tmpDataField = "";
        $tmpFilterOperator = "";

        // get the filter's value.
        $filterValue = trim($searchString);
        // get the filter's column.
        $filterDataField = $searchField;
        // get the filter's operator.
        $filterOperator = $searchOper;

        if ($tmpDataField == "")
        {
            $tmpDataField = $filterDataField;
        }
        else if ($tmpDataField <> $filterDataField)
        {
            $where .= ")AND(";
        }
        else if ($tmpDataField == $filterDataField)
        {
            if ($tmpFilterOperator == 0)
            {
                $where .= " AND ";
            }
            else $where .= " OR ";
        }

        // build the "WHERE" clause depending on the filter's condition, value and datafield.
        switch($searchOper)
        {
            case "cn":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."%'";
                break;
            case "nc":
                $where .= " " . $filterDataField . " NOT LIKE '%" . $filterValue ."%'";
                break;
            case "eq":
                $where .= " " . $filterDataField . " = '" . $filterValue ."'";
                break;
            case "bw":
                $where .= " " . $filterDataField . " LIKE '" . $filterValue ."%'";
                break;
            case "ew":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."'";
                break;
            case "in":
                $string = "'".$filterValue."'";
                if(strpos($filterValue, ',') !== false){
                    $data = explode(",",$filterValue);
                    $string = '';
                    foreach ($data as $key=>$value){
                        $semi = '';
                        if($key != 0) $semi = ",";
                        $string .= $semi." '".$value."'";
                    }
                }
                $where .= " " . $filterDataField . " IN  (" . $string .")";
                break;
        }
        $where .= ")";

        $tmpFilterOperator = $filterOperator;
        $tmpDataField = $filterDataField;
        // build the query.
        return $where;
    }


    /**
     * Advance Search functionality for jqGrid
     * @param $searchField
     * @param $searchString
     * @param $searchOper
     * @return string
     */

    /**
     * @param $searchField
     * @param $searchString
     * @param $searchOper
     * @param null $int
     * @param null $searchString2
     * @return string
     * @throws Exception
     */
    public function advance_search_filters($searchField,$searchString,$searchOper,$int=null,$searchString2=null){
        // dd($searchString);
        $where = "";
        $tmpDataField = "";
        $tmpFilterOperator = "";

        // get the filter's value.
        $filterValue = (is_array($searchString))?$searchString:trim($searchString);
        $filterValue2 = !empty($searchString2)?$searchString2:null;
        // get the filter's column.
        $filterDataField = $searchField;
        // get the filter's operator.
        $filterOperator = $searchOper;

        if ($tmpDataField == "")
        {
            $tmpDataField = $filterDataField;
        }
        else if ($tmpDataField <> $filterDataField)
        {
            $where .= ")AND(";
        }
        else if ($tmpDataField == $filterDataField)
        {
            if ($tmpFilterOperator == 0)
            {
                $where .= " AND ";
            }
            else $where .= " OR ";
        }
        // build the "WHERE" clause depending on the filter's condition, value and datafield.
        switch($searchOper)
        {
            case "cn":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."%'";
                break;
            case "nc":
                $where .= " " . $filterDataField . " NOT LIKE '%" . $filterValue ."%'";
                break;
            case "eq":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " = " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " = '" . $filterValue . "'";
                }
                break;
            case "bw":
                $where .= " " . $filterDataField . " LIKE '" . $filterValue ."%'";
                break;
            case "ew":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."'";
                break;
            case "gr":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " >= " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " >= '" . $filterValue ."'";
                }
                break;
            case "lt":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " <= " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " <= '" . $filterValue ."'";
                }
                break;
            case "in":
                if(is_array($filterValue)){
                    $string = '';
                    foreach ($filterValue as $key => $value) {
                        $semi = '';
                        if ($key != 0) $semi = ",";
                        $string .= $semi . " '" . $value . "'";
                    }
                } else {
                    $string = "'" . $filterValue . "'";
                    if (strpos($filterValue, ',') !== false) {
                        $data = explode(",", $filterValue);
                        $string = '';
                        foreach ($data as $key => $value) {
                            $semi = '';
                            if ($key != 0) $semi = ",";
                            $string .= $semi . " '" . $value . "'";
                        }
                    }
                }
                $where .= " " . $filterDataField . " IN  (" . $string .")";
                break;
            case "dateByString":
                $date = $this->x_week_range(date('Y-m-d'));
                if($filterValue == 'Weekly'){
                    $start = $date[0];
                    $end = $date[1];
                } elseif($filterValue == 'Bi-Weekly'){
                    $strData = strtotime($date[0]);
                    $start = date('Y-m-d', strtotime('-7 days', $strData));
                    $end = $date[1];
                } else {
                    $timestamp    = strtotime($filterValue);
                    $start = date('Y-m-01', $timestamp);
                    $end  = date('Y-m-t', $timestamp);
                }
                $where .= " DATE(". $filterDataField.") BETWEEN '".$start."' AND '".$end."'";
                break;
            case "dateBetween":
                $start = !empty($filterValue) && $filterValue != 'all'?mySqlDateFormat($filterValue,null,$this->companyConnection):date('Y-m-d');
                $end = !empty($filterValue2) && $filterValue2 != 'all'?mySqlDateFormat($filterValue2,null,$this->companyConnection):date('Y-m-d');
                $where .= " DATE(". $filterDataField.") BETWEEN '".$start."' AND '".$end."'";
                break;
            case "dateMonth":
                $where .= " MONTH(". $filterDataField.") = ".$filterValue." ";
                break;
            case "date":
                $date = !empty($filterValue) && $filterValue != 'all'?mySqlDateFormat($filterValue,'1',$this->companyConnection):date('Y-m-d');
                $where .= " DATE(". $filterDataField.") = '".$date."' ";
                break;
        }
        $where .= "";

        $tmpFilterOperator = $filterOperator;
        $tmpDataField = $filterDataField;

        // build the query.
        return $where;
    }

    /**
     * @param null $ignore_list
     * @param $status
     * @param $table
     * @param $deleted_at
     * @param $extra_dropdown
     * @param null $extra_where
     * @return string
     * @throws Exception
     */
//    public function get_combined_where($ignore_list=null,$status,$table,$deleted_at,$extra_dropdown,$extra_where=null){
//        $ignore_list = (isset($ignore_list) && !empty($ignore_list))? $ignore_list: [];
////        dd($ignore_list);
//        if($deleted_at == ' WHERE deleted_at IS NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NULL']);
//        if($deleted_at == ' WHERE deleted_at IS NOT NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NOT NULL']);
////        dd($ignore_list);
//        if( strtolower($status) != 'all'){
//            array_push($ignore_list,['column'=>'status','value'=>$status]);
//        }
//        if($extra_dropdown != '0'){
//            array_push($ignore_list,['column'=>$extra_dropdown['column'],'value'=>$extra_dropdown['value'],'include'=>'1']);
//        }
//        $where = '';
////        dd($ignore_list);
//        if(!empty($ignore_list)) {
//            $where = " WHERE (";
//            foreach ($ignore_list as $key => $value) {
//                if ($key > 0) $where .= ' AND';
//                if (isset($value['include']) && $value['include'] == '1') {
//                    $where .= " " . $value['column'] . " = '" . $value['value'] . "'";
//                } else if($value['column'] == 'deleted_at'){
//                    $where .= " " . $table.".".$value['column'] . $value['value'];
//                } else if ($value['column'] != 'status') {
//                    $where .= " " . $value['column'] . " != '" . $value['value'] . "'";
//                } else {
//                    $where .= " " . $table.".".$value['column'] . " = '" . $value['value'] . "'";
//                }
//            }
//            if(empty($extra_where)) $where .= ")";
//        }
//        if(!empty($extra_where)) {
//            if(empty($ignore_list)) $where .= " WHERE (";
//            foreach ($extra_where as $key => $value) {
//                $table = isset($value['table']) && !empty($value['table'])?$value['table']:$table;
//                if (!empty($ignore_list) || $key > 0) $where .= ' AND';
//                if ($value['condition'] == 'IS NULL') {
//                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
//                } else if ($value['condition'] == 'IS NOT NULL') {
//                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
//                } else if ($value['condition'] == 'IN') {
//                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." " . $value['value'];
//                }  else if ($value['condition'] == 'INT') {
//                    $where .= " " . $table . "." . $value['column'] . " = " . $value['value'];
//                } else if ($value['column'] == 'end_date') {
//                    $end_date = mySqlDateFormat($value['value'], null, $this->companyConnection);
//                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." " . "'".$end_date."'";
//                } else {
//                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." '" . $value['value'] . "'";
//                }
//            }
//            $where .= ")";
//        }
//        return $where;
//    }


    public function get_combined_where($ignore_list=null,$status,$table,$deleted_at,$extra_dropdown,$extra_where=null){
        $ignore_list = (isset($ignore_list) && !empty($ignore_list))? $ignore_list: [];
//        dd($ignore_list);
        if($deleted_at == ' WHERE deleted_at IS NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NULL']);
        if($deleted_at == ' WHERE deleted_at IS NOT NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NOT NULL']);
//        dd($ignore_list);
        if( strtolower($status) != 'all'){
            array_push($ignore_list,['column'=>'status','value'=>$status]);
        }
        if($extra_dropdown != '0'){
            array_push($ignore_list,['column'=>$extra_dropdown['column'],'value'=>$extra_dropdown['value'],'include'=>'1']);
        }
        $where = '';
//        dd($ignore_list);
        if(!empty($ignore_list)) {
            $where = " WHERE (";
            foreach ($ignore_list as $key => $value) {
                if ($key > 0) $where .= ' AND';
                if (isset($value['include']) && $value['include'] == '1') {
                    $where .= " " . $value['column'] . " = '" . $value['value'] . "'";
                } else if($value['column'] == 'deleted_at'){
                    $where .= " " . $table.".".$value['column'] . $value['value'];
                } else if ($value['column'] != 'status') {
                    $where .= " " . $value['column'] . " != '" . $value['value'] . "'";
                } else {
                    $where .= " " . $table.".".$value['column'] . " = '" . $value['value'] . "'";
                }
            }
            if(empty($extra_where)) $where .= ")";
        }
        if(!empty($extra_where)) {
            if(empty($ignore_list)) $where .= " WHERE (";
            foreach ($extra_where as $key => $value) {
                $type = isset($value['type']) && !empty($value['type'])?' '.$value['type']:' AND';
                $table = isset($value['table']) && !empty($value['table'])?$value['table']:$table;
                if (!empty($ignore_list) || $key > 0) $where .= $type;
                if ($value['condition'] == 'IS NULL') {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
                } else if ($value['condition'] == 'IS NOT NULL') {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
                } else if ($value['condition'] == 'IN') {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." " . $value['value'];
                }  else if ($value['condition'] == 'INT') {
                    $where .= " " . $table . "." . $value['column'] . " = " . $value['value'];
                } else if ($value['column'] == 'end_date') {
                    $end_date = mySqlDateFormat($value['value'], null, $this->companyConnection);
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." " . "'".$end_date."'";
                } else {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." '" . $value['value'] . "'";
                }
            }
            $where .= ")";
        }
        return $where;
    }

    /**
     * function to build join query for the Select statement
     * @param $join
     * @return string
     */
    public function build_join_query($join){
        $join_query = '';
        if(!empty($join)){
            foreach ($join as $key=>$value){
                $as = '';
                $alisa = $value['on_table'];
                $type = isset($value['type']) && !empty($value['type'])?$value['type']:'LEFT JOIN';
                if(isset($value['as']) && !empty($value['as'])){
                    $as = ' as '.$value['as'].' ';
                    $alisa = $value['as'];
                }
                $join_query .= " ".$type." ".$value['on_table'].$as." ON ".$value['table'].".".$value['column']."=".$alisa.".".$value['primary'];
            }
        }
        // dd($join_query);
        return $join_query;
    }

    /**
     * function to build column string for select query
     * @param $columns
     * @return string
     */
    public function build_column_string($columns){
        $data = '';
        foreach ($columns as $key=>$value){
            $semi = '';
            if($key != 0) $semi = ",";
            $data .= $semi." ".$value['columns'];
        }
        return $data;
    }

    /**
     * Get Dynamic  column name for sorting
     * @param $sidx
     * @param $columns
     * @return mixed
     */
    public function get_sidx($sidx,$columns){
        if($sidx == 'updated_at') return $sidx;
        foreach ($columns as $key => $value) {
            if ($sidx == $value['original']) return $value['columns'];
        }
        return $sidx;
    }

    /**
     * function to specify extra columns
     * @param $columns
     * @return string
     */
    public function build_extra_columns($columns){
        $data = '';
        foreach ($columns as $key=>$value){
            if(empty($value)) continue;
            $semi = ',';
            $data .= $value.$semi;
        }
        return $data;
    }

    public function alphabeticSearchTenant(){
        try {

            $sql = "SELECT DISTINCT LEFT(u.name, 1) as letter FROM users AS u JOIN tenant_lease_details ON u.id = tenant_lease_details.user_id JOIN tenant_details ON tenant_details.user_id = u.id WHERE ( u.record_status = '0' AND u.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1') AND u.deleted_at IS NULL ORDER BY letter";
            // dd($sql);
            // dd($sql);
            $result = $this->companyConnection->query($sql);
            $data = $result->fetchAll();
            $alphas = range('A', 'Z');
            $response = [];
            $data_alpha = [];
            if (count($data) > 0) {
                foreach ($data as $key => $value) {
                    if (empty($value['letter'])) continue;
                    array_push($data_alpha, strtoupper($value['letter']));
                }
            }
            foreach ($alphas as $key => $value) {
                if (in_array($value, $data_alpha, TRUE)) {
                    $response[$value] = 1;
                } else {
                    $response[$value] = 0;
                }
            }
            return array('code' => 200, 'status' => 'success', 'data' => $response, 'message' => 'Alphabatic search record retrieved successfully.');
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    /**
     * function to show Alphabt nheighlighted
     * @return array
     */
    public function alphabeticSearch(){
        try {
            //Required params
            $table = (isset($_REQUEST['table']) && !empty($_REQUEST['table'])) ? $_REQUEST['table'] : null;
            $where = (isset($_REQUEST['where']) && !empty($_REQUEST['where'])) ? $_REQUEST['where'] : [];
            $whereCondition = '';
            array_push($where,['column'=>'deleted_at','value'=>'','condition'=>'IS NULL','table'=>$table]);
            if (!empty($where)) {
                $whereCondition .= " WHERE (";
                foreach ($where as $key => $value) {
                    $table = isset($value['table']) && !empty($value['table']) ? $value['table'] : $table;
                    if (!empty($ignore_list) || $key > 0) $whereCondition .= ' AND';
                    if ($value['condition'] == 'IS NULL') {
                        $whereCondition .= " " . $table . "." . $value['column'] . " " . $value['condition'];
                    } else if ($value['condition'] == 'IS NOT NULL') {
                        $whereCondition .= " " . $table . "." . $value['column'] . " " . $value['condition'];
                    } else if ($value['condition'] == 'IN') {
                        $whereCondition .= " " . $table . "." . $value['column'] . " " . $value['condition'] . " " . $value['value'];
                    } else {
                        $whereCondition .= " " . $table . "." . $value['column'] . " " . $value['condition'] . " '" . $value['value'] . "'";
                    }
                }
                $whereCondition .= ")";
            }

            $column = (isset($_REQUEST['column']) && !empty($_REQUEST['column'])) ? $_REQUEST['column'] : null;// get the requested page
            if (empty($table) && empty($column)) {
                return array('code' => 400, 'status' => 'error', 'message' => 'please specify table and column');
            }
            // $deleted_at = ' AND deleted_at IS NULL';
            $sql = 'SELECT DISTINCT LEFT(' . $column . ', 1) as letter FROM ' . $table . ' ' . $whereCondition . ' ORDER BY letter';
            // dd($sql);
            $result = $this->companyConnection->query($sql);
            $data = $result->fetchAll();
            $alphas = range('A', 'Z');
            $response = [];
            $data_alpha = [];
            if (count($data) > 0) {
                foreach ($data as $key => $value) {
                    if (empty($value['letter'])) continue;
                    array_push($data_alpha, strtoupper($value['letter']));
                }
            }
            foreach ($alphas as $key => $value) {
                if (in_array($value, $data_alpha, TRUE)) {
                    $response[$value] = 1;
                } else {
                    $response[$value] = 0;
                }
            }
            return array('code' => 200, 'status' => 'success', 'data' => $response, 'message' => 'Alphabatic search record retrieved successfully.');
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    /**
     * function to get specific advance column
     * @param $searchField
     * @param $columns
     * @return mixed
     */
    public function getAdvanceSearchColumn($searchField,$columns){
        foreach ($columns as $key=>$value){
            if($value['original'] == $searchField) return $value['columns'];
        }
    }

    /**
     * function to get child based on parent from table
     * @param $searchField
     * @param $columns
     * @return mixed
     */
    public function getchilds($parent_id,$table,$column,$type,$join=null){
        $line ='';
        if($type == 'line') {
            $joins = (isset($join) && !empty($join))?$this->build_join_query($join):$this->build_join_query([]);
            $data = $this->companyConnection->query("SELECT $column FROM $table $joins WHERE $table.parent_id = $parent_id")->fetchAll();
            if(!empty($data)){
                foreach ($data as $key=>$value){
                    $line .= $value[$column].',';
                }
            }
            $line = str_replace_last(',','',$line);
        }

        return $line;
    }

    /**
     * change serlize data and convert it according to type
     * @param $columnValue
     * @param $column
     * @param $type
     * @param null $join
     * @return mixed|string
     */
    public function changeTypeSerialize($columnValue,$column,$type=null,$join=null,$id=null){
        $line ='';
        if(empty($columnValue)) return $columnValue;
        $columnValue = unserialize($columnValue);

        if($columnValue[0] == 'default' || $columnValue[0] == '') return $line;
        if(empty($type)) 'line';
        if($type == 'line') {
            $joins = (isset($join) && !empty($join))?$this->build_join_query($join):$this->build_join_query([]);
            foreach ($columnValue as $key=>$value){
                if($value == 'default' || $value == '') return '';
                $data = $this->companyConnection->query("SELECT ".$column['index2']." FROM ".$column['secondTable']." $joins WHERE ".$column['secondTable'].".id = ".$value)->fetch();
                if(!empty($data)){
                    $line .= $data[$column['index2']].',';
                }
            }
            $line = str_replace_last(',','',$line);
        } else if ($type == 'task&r'){
            $joins = (isset($join) && !empty($join))?$this->build_join_query($join):$this->build_join_query([]);
            foreach ($columnValue as $key=>$value){
                if($value == 'default' || $value == '') return '';
                $data = $this->companyConnection->query("SELECT unit_details.unit_prefix,unit_details.unit_no FROM ".$column['secondTable']." $joins WHERE ".$column['secondTable'].".id = ".$value)->fetch();
                if(!empty($data)){
                    if(!empty($data['unit_prefix'])){
                        $line .= $data['unit_prefix'].'-'.$data['unit_no'].',';
                    } else {
                        $line .= $data['unit_no'].',';
                    }
                }
            }
            $line = str_replace_last(',','',$line);
        }
        if(!empty($column['update_column'])) {
            $this->updateColumn($column, $line, $id);
        }
        return $line;
    }

    /**
     * function to combile diffrent columns to single line
     * @param $column
     * @param $data
     * @return mixed|string
     */
    public function combineColumnline($column,$data,$columnData){
        $line ='';
        foreach ($column as $key=>$value){
            if(!empty($data[$value])){
                $line .= $data[$value].',';
            }
        }
        $line = str_replace_last(',','',$line);
        if(!empty($columnData['update_column'])) {
            $this->updateColumn($columnData, $line, $data['id']);
        }
        return $line;
    }

    /**
     * function to combine different columns to single line
     * @param $column
     * @param $data
     * @return mixed|string
     */
    public function combineColumnHyphen($column,$data,$columnData){
        $line ='';
        foreach ($column as $key=>$value){
            if(!empty($data[$value])){
                $line .= $data[$value].'-';
            }
        }
        $line = str_replace_last('-','',$line);
        if(!empty($columnData['update_column'])) {

            $this->updateColumn($columnData, $line, $data['id']);
        }
        return $line;
    }

    /**
     * function to combine different columns to single line
     * @param $column
     * @param $data
     * @return mixed|string
     */
    public function combineColumnHyphen2($column,$data,$columnData){
        $line ='';
        foreach ($column as $key=>$value){
            if(!empty($data[$value])){
                $line .= $data[$value].'-';
            }
        }
        $line = str_replace_last('-','',$line);
        return $line;
    }

    /**
     * function to combine different columns to single line
     * @param $column
     * @param $data
     * @return mixed|string
     */
    public function tenantname1($column,$data){
        $return = userName($data['id'], $this->companyConnection,'users');
        return ucwords($return);
    }

    /**
     * function to serach columnData with column
     * @param $column
     * @param $data
     * @return bool
     */
    public function searchThroughColumns($column,$data){
        foreach ($data as $key=>$value){
            if($column == $value['original']){
                return $value;
            }
        }
        return false;
    }

    public function updateColumn($columnData,$data,$id){
        $query = "UPDATE ".$columnData['table']." SET ".$columnData['update_column']."=? where id=".$id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute([$data]);
    }

    public function propertyFlag($propertyId,$name,$value){

        $query = "SELECT * FROM flags Where object_id=".$propertyId." AND object_type='".$value."' AND completed='0' AND deleted_at IS NULL";
        $stmt = $this->companyConnection->query($query)->fetchAll();
        if(!empty($stmt)){
            return 'yes';
        } else {
            return 'no';
        }
    }

    public function multipleTypeLine($id,$join,$column,$column2){
        $extraColumn = '';
        if(!empty($column2)){
            $extraColumn = ','.$column2;
        }
        $query = 'SELECT '.$column.$extraColumn.' FROM '.$join["on_table"].' WHERE '.$join['primary'].'='.$id.' ORDER BY id ASC';
        $stmt = $this->companyConnection->query($query)->fetch();
        $line ='';
        if(!empty($stmt)) {
//            foreach ($stmt as $key => $value) {
            if (!empty($stmt['phone_number']) && $stmt['phone_number'] != '') {
                if(!empty($column2) && !empty($stmt[$column2])){
                    $line .= $stmt[$column] .' Ext: '.$stmt[$column2].',';
                } else {
                    $line .= $stmt[$column] . ',';
                }
//                }
            }
            $line = str_replace_last(',', '', $line);
        }
        return $line;
    }

    /**
     * function to get week date range
     * @param $date
     * @return array
     */
    public function x_week_range($date) {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next saturday', $start)));
    }

    /**
     * function to get and update vacant property field
     * @param $column
     * @param $columnValue
     * @return int
     */
    public function propertyVacant($column,$columnValue){
        $total = !empty($column['no_of_units'])?$column['no_of_units']:0;
        $exists = !empty($column['unit_exist'])?$column['unit_exist']:0;

        $vacant = ($total-$exists);
//        dd($exists);
        $this->updateColumn($columnValue,$vacant,$column['id']);
        return $vacant;
    }

    public function checkWeekDate($id,$weekType){
        //  if(empty($id)) return '0';
        $date = date('Y-m-d');

        $current_day = date('w');
        $week_start_day = date('Y-m-d', strtotime('-'.$current_day.' days'));
        $week_end_day = date('Y-m-d', strtotime('+'.(6-$current_day).' days'));
        if($weekType == '2weekago') {
            $start_date =  date('Y-m-d', strtotime($week_start_day.'-14 days'));
            $end_date = $week_start_day;
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in BETWEEN "'.$start_date.'" AND "'.$end_date.'" ' ;
        } else if($weekType == '1weekago'){
            $date7 = date('Y-m-d', strtotime($week_start_day.'-7 days'));
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in BETWEEN "'.$date7.'" AND "'.$week_start_day.'" ';
        } else if($weekType == 'currentWeek'){
            $date = $this->x_week_range(date('Y-m-d'));
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in BETWEEN "'.$week_end_day.'" AND "'.$week_start_day.'" ';
        } else if($weekType == 'nextWeek'){
            $end_date =  date('Y-m-d', strtotime($week_end_day.'+7 days'));
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in BETWEEN "'.$week_end_day.'" AND "'.$end_date.'" ';

        } else if($weekType == 'twoWeeksAhead'){
            $end_date = date('Y-m-d', strtotime($week_end_day.'+14 days'));
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in BETWEEN "'.$week_end_day.'" AND "'.$end_date.'" ';
        }
        else {
            $query = 'SELECT COUNT(id) FROM tenant_move_in where actual_move_in = "'.$date.'" ';
        }
        // AND user_id='.$id
        $result = $this->companyConnection->query($query)->fetch();
        if(empty( $result))
            return '0';
        return $result['COUNT(id)'];
    }

    public function Count_tenant_move_out($weekType){
        //  if(empty($id)) return '0';
        $date = date('Y-m-d');

        $current_day = date('w');
        $week_start_day = date('Y-m-d', strtotime('-'.$current_day.' days'));
        $week_end_day = date('Y-m-d', strtotime('+'.(6-$current_day).' days'));

        if($weekType == '2weekago') {
            $start_date =  date('Y-m-d', strtotime($week_start_day.'-14 days'));
            $end_date = $week_start_day;
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
        } else if($weekType == '1weekago'){
            $date7 = date('Y-m-d', strtotime($week_start_day.'-7 days'));
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate BETWEEN "'.$date7.'" AND "'.$week_start_day.'" ';
        } else if($weekType == 'currentWeek'){
            // $date = $this->x_week_range(date('Y-m-d'));
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate BETWEEN "'.$week_start_day.'" AND "'.$week_end_day.'" ';
        } else if($weekType == 'nextWeek'){
            $end_date =  date('Y-m-d', strtotime($week_end_day.'+7 days')); //
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate BETWEEN "'.$week_end_day.'" AND "'.$end_date.'" ';
        } else if($weekType == 'twoWeeksAhead'){
            $end_date = date('Y-m-d', strtotime($week_end_day.'+14 days'));
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate BETWEEN "'.$week_end_day.'" AND "'.$end_date.'" ';
        }
        else {
            $query = 'SELECT COUNT(id) FROM moveouttenant where actualMoveOutDate = "'.$date.'" ';
        }

        $result = $this->companyConnection->query($query)->fetch();
        return $result['COUNT(id)'];
    }
    public function count_workOrders($pid,$type){
        if($type == 'Open') {
            $query = 'SELECT COUNT(id) FROM work_order where status_id = "1" AND property_id ="'.$pid.'" ';
        } else if($type == 'InProgress'){
            $query = 'SELECT COUNT(id) FROM work_order where status_id = "3" AND property_id ="'.$pid.'" ';
        } else if($type == 'Closed'){
            $query = 'SELECT COUNT(id) FROM work_order where status_id = "2" AND property_id ="'.$pid.'" ';
        } else {
            $query = 'SELECT COUNT(id) FROM work_order where status_id <>"" ';
        }
        //    dd($query);
        $result = $this->companyConnection->query($query)->fetch();
        return $result['COUNT(id)'];
    }

    /**
     * @param $columnValue
     * @param $column
     * @param null $type
     * @param null $join
     * @param null $id
     * @return mixed|string
     */
    public function invoiceAmountDate($columnValue,$table,$column){
        $line ='';
        if(empty($columnValue)) return $columnValue;
        $query = 'SELECT SUM(amount) AS Invoice_Amount,SUM(amount_paid) AS Amount_Paid,SUM(amount_due) AS Amount_Due,SUM(amount_refunded) AS Amount_Refunded FROM '.$table.' WHERE invoice_id ='.$columnValue;
        $data = $this->companyConnection->query($query)->fetch();
        $invoice_data = [];
        if(!empty($data)) {
            foreach ($data as $key=>$value){
                $key = str_replace_last('_',' ',$key);
                $val = !empty($value)?$value:0;
                $line_data = $key.':'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format($val,2).' ';
                $line .= $line_data;
                array_push($invoice_data,$line_data);
            }
            $line = str_replace_last(' ','',$line);
        }
        if(!empty($column['update_column'])) {
            $this->updateColumn($column, $line, $columnValue);
        }
        return $invoice_data;
    }


    public function getdaysremaining($id,$user_id){
        $date=date("Y-m-d");
        $user_id="'".$user_id."'";
        $query = $this->companyConnection->query("SELECT count(strb.date) as total FROM short_term_rental_booking_dates strb WHERE '".$date."' < strb.date and strb.unit_booked_id='".$id."' and strb.user_id=".$user_id);
        $data = $query->fetchAll();
        return $data[0]['total'];
    }

    public function shorttermstatus($id,$user_id){
        $date=date("Y-m-d");
        $user_id="'".$user_id."'";
        $query = $this->companyConnection->query("SELECT count(strb.date) as total FROM short_term_rental_booking_dates strb WHERE '".$date."' < strb.date and strb.unit_booked_id='".$id."' and strb.user_id=".$user_id);
        $data = $query->fetchAll();
        $sta="";
        if($data[0]['total'] <= 0){
            $sta="Inactive";
        }
        else{
            $sta="Active";
        }
        return $sta;
    }

    public function paymentplanstatus($id,$installments){
        $query = $this->companyConnection->query("SELECT pp.amount_due as due from payment_plan pp where id=".$id);
        // dd($query);
        $data = $query->fetchAll();
        $sta="";

        if($data[0]['due'] <=0){
            $sta="Completed";
        }
        else if($data[0]['due'] > 0){
            $sta="In Process";
        }
        return $sta;
    }

    public function amountdue($id){

        $query1 = $this->companyConnection->query("SELECT amount_due as due from payment_plan pp where pp.id=".$id);
        $data1 = $query1->fetchAll();
        //$due= $data1[0]['due'] - ($data[0]['total'] + $data[0]['extra']);
        return $data1[0]['due'];
    }
    public function nextamount($id){
       // dd("SELECT amount_paid as amt from payment_plan_dates_is_paid where is_paid='No' AND payment_id=".$id);
        $query = $this->companyConnection->query("SELECT amount_paid as amt from payment_plan_dates_is_paid where is_paid='No' AND payment_id=".$id);
        $data = $query->fetchAll();
        if(!empty($data)){
            return $data[0]['amt'];
        }else{
         return number_format((float)0, 2, '.', '');;
        }

    }
    public function nextdate($id){
      //  dd("SELECT dates as date from payment_plan_dates_is_paid where is_paid='No' AND payment_id=".$id);
        $query = $this->companyConnection->query("SELECT dates as date from payment_plan_dates_is_paid where is_paid='No' AND payment_id=".$id);
        $data = $query->fetchAll();
        if(!empty($data)){
            return dateFormatUser($data[0]['date'],null,$this->companyConnection);
        }else{
            return "";
        }

    }
    public function count_Amount($id)
    {
        $query = $this->companyConnection->query("SELECT sum(amount) as totalAmt from tenant_charges where invoice_id=$id");
        $data = $query->fetchAll();
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
         $amt = number_format($data[0]['totalAmt'],2);
        return  $default_symbol.$amt;
    }

    public function getJournalEntries($id,$table,$type){
        try {
            $table = 'journal_entry_details';
            $query = 'SELECT '.$table.'.*,c1.account_name FROM '.$table.' LEFT JOIN company_chart_of_accounts as c1 ON c1.id='.$table.'.chart_account_id WHERE '.$table.'.journal_id ='.$id ;
            $dataQuery = $this->companyConnection->query($query)->fetchAll();
            $row = [];
            switch($type)
            {
                case "Account":
                    foreach ($dataQuery as $key=>$value){
                        array_push($row,$value['account_name']);
                    }
                    break;
                case "Debit":
                    foreach ($dataQuery as $key=>$value){
                        array_push($row,$value['debit']);
                    }
                    break;
                case "Credit":
                    foreach ($dataQuery as $key=>$value){
                        array_push($row,$value['credit']);
                    }
                    break;
            }
            return $row;
        } catch (PDOException $exception) {
            return $exception;
        }
    }

    public function paidTotal($id,$owner_id,$property,$dateBetween){
        try {
//            dd($dateBetween);
            $start = !empty($dateBetween)?mySqlDateFormat($dateBetween['start'],null,$this->companyConnection):date('Y-m-d');
            $end = !empty($dateBetween)?mySqlDateFormat($dateBetween['end'],null,$this->companyConnection):date('Y-m-d');
            $where = " DATE( created_at ) BETWEEN '".$start."' AND '".$end."'";
            $q = 'Select * FROM owner_draw WHERE'.$where.' AND property_id='.$property.' AND owner_id='.$owner_id;
//            dd($q);
            $already_done_payment = $this->companyConnection->query($q)->fetch();
            $created_date = dateFormatUser($already_done_payment['created_at'], null, $this->companyConnection);
            if (!empty($already_done_payment)){
                $row  = '<table class="income-owner-table">';
                $row .= '<tr><td>Payment already done for this month on '.$created_date.'.</td></tr>';
                $row .= '</table>';
                return $row;
            }

            $query = 'SELECT sum(total_charge_amount) FROM transactions WHERE user_type="TENANT" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $incomeData = $this->companyConnection->query($query)->fetch();
            $query1 = 'SELECT sum(amount) FROM company_bills WHERE property_id ='.$property.' AND status="1" AND'.$where;
            $paidData = $this->companyConnection->query($query1)->fetch();
            $query2 = 'SELECT sum(amount) FROM company_bills WHERE property_id ='.$property.' AND status="0" AND '.$where;
            $unpaidData = $this->companyConnection->query($query2)->fetch();

            $query3 = 'SELECT sum(total_charge_amount) FROM transactions WHERE type="OWNER_DRAW" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $disbursementData = $this->companyConnection->query($query3)->fetch();

            $query4 = "SELECT process_management_fee FROM process_management_fee WHERE paid_status = '1' AND property_id=".$property." AND  DATE( paid_date ) BETWEEN '".$start."' AND '".$end."'";
            $pmfData = $this->companyConnection->query($query4)->fetch();

            $query5 = "SELECT process_management_fee FROM process_management_fee WHERE paid_status = '0' AND property_id=".$property." AND  DATE( updated_at ) BETWEEN '".$start."' AND '".$end."'";
            $prmfData = $this->companyConnection->query($query5)->fetch();

            $ownerDisbursementAmount = floatFormatter($disbursementData['sum(total_charge_amount)']);

            $income = floatFormatter($incomeData['sum(total_charge_amount)']);

            $paidBills = floatFormatter((float)$paidData['sum(amount)']);
            $unpaidBills = floatFormatter($unpaidData['sum(amount)']);
            $pmf = floatFormatter($pmfData['process_management_fee']);
            $prmf = floatFormatter($prmfData['process_management_fee']);

            $total_bills = floatFormatter($paidBills+$unpaidBills);
            $total_pmfs = $pmf + $prmf;

            // Expenses: it will show all bill amount (Include all Paid/Unpaid Bill or Paid/Unpaid PMF) during selected period.
            $expense = floatFormatter($total_bills+$total_pmfs);
            $amount = floatFormatter($income - $expense);

            //Suggested Amount = (Amount * Percentage) – (Minimum Reserve) - (Owner disbursements during period).

            $row  = '<table class="income-owner-table">';
            $row .= '<tr><td>Income</td><td class="text-right">'.$income.'</td></tr>';
            $row .= '<tr style="font-weight: bold;"><td>Expenses</td><td class="text-right">'.$expense.'</td></tr>';
            $row .= '<tr><td>Paid Bill</td><td class="text-right">'.$paidBills.'</td></tr>';
            $row .= '<tr><td>Un-Paid Bill</td><td class="text-right">'.$unpaidBills.'</td></tr>';
            $row .= '<tr><td>Paid Management Fees</td><td class="text-right">'.$pmf.'</td></tr>';
            $row .= '<tr><td>Process Management Fees</td><td class="text-right">'.$prmf.'</td></tr>';
            $row .= '<tr><td>Amount ( from start_date - end_date )</td><td class="text-right">'.$amount.'</td></tr>';
            $row .= '<tr><td>Payment made to <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">owner_name</span> during period: </td><td class="text-right">'.$ownerDisbursementAmount.'</td></tr>';
            $row .= '</table>';
            return $row;
        } catch (PDOException $exception) {
            return $exception;
        }
    }

    public function recommendedDisbursement($id,$owner_id,$property,$dateBetween){
        try {
            $start = !empty($dateBetween)?mySqlDateFormat($dateBetween['start'],null,$this->companyConnection):date('Y-m-d');
            $end = !empty($dateBetween)?mySqlDateFormat($dateBetween['end'],null,$this->companyConnection):date('Y-m-d');
            $where = " DATE( created_at ) BETWEEN '".$start."' AND '".$end."'";

            $q = 'Select * FROM owner_draw WHERE'.$where.' AND property_id='.$property.' AND owner_id='.$owner_id;
//            dd($q);
            $already_done_payment = $this->companyConnection->query($q)->fetch();
            $paid_amount = $already_done_payment['amount'];
            if (!empty($already_done_payment)){
                $row  = '<table class="income-owner-table">';
                $row .= '<tr><td>Owner disbursement made for this month is '.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$paid_amount.'.</td></tr>';
                $row .= '</table>';
                return $row;
            }
            $query = 'SELECT sum(total_charge_amount) FROM transactions WHERE user_type="TENANT" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $incomeData = $this->companyConnection->query($query)->fetch();

            $query = 'SELECT sum(total_charge_amount) FROM transactions WHERE user_type="OWNER" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $ownerExpenseData = $this->companyConnection->query($query)->fetch();

            $query = 'SELECT sum(total_charge_amount) FROM transactions WHERE user_type="VENDOR" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $vendorExpenseData = $this->companyConnection->query($query)->fetch();

            $query1 = 'SELECT bank_account_min_reserve_amount FROM property_bank_details WHERE is_default = 1 AND property_id ='.$property;
            $minReserveAmountData = $this->companyConnection->query($query1)->fetch();

            $query2 = 'SELECT property_percent_owned FROM owner_property_owned WHERE property_id ='.$property;
            $propertyPercentOwned = $this->companyConnection->query($query2)->fetch();

            $query3 = 'SELECT sum(total_charge_amount) FROM transactions WHERE type="OWNER_DRAW" AND payment_status = "SUCCESS" AND property_id='.$property.' AND '.$where;
            $disbursementData = $this->companyConnection->query($query3)->fetch();

            $minReserveAmount =  floatFormatter($minReserveAmountData['bank_account_min_reserve_amount']);
            $totalExpense = $ownerExpenseData['sum(total_charge_amount)'] + $vendorExpenseData['sum(total_charge_amount)'];
            $currentBankBalance = floatFormatter($incomeData['sum(total_charge_amount)'] - $totalExpense);
            $disbursementAmount = floatFormatter($disbursementData['sum(total_charge_amount)']);

            $row  = '<table class="income-owner-table">';
            $row .= '<tr><td>Current Bank Balance</td><td class="current_bank_balance">'.$currentBankBalance.'</td></tr>';
            $row .= '<tr><td>Minimum Reserve</td><td class="minimum_reserve">'.$minReserveAmount.'</td></tr>';
            $row .= '<tr><td>Owner Disbursements during period</td><td>'.$disbursementAmount.'</td></tr>' ;
            $row .= '<tr><td>* Ownership Percentage</td><td>'.$propertyPercentOwned['property_percent_owned'].'%</td></tr>';
            $row .= '<tr style="font-weight: bold;"><td>Total ('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].')</td><td><input type="text" name="owner_draw_amount" class="form-control money number_only owner_draw_amount" value="0.00"></td></tr>';
            $row .= '</table>';
            return $row;
        } catch (PDOException $exception) {
            return $exception;
        }
    }

    public function unit_vacant_count($id){
        try{
            $query = "SELECT count(id) FROM unit_details WHERE property_id='$id' AND building_unit_status=1";
            $dataQuery = $this->companyConnection->query($query)->fetch();
            $vacant=[];
            $vacant['table']='general_property';
            $vacant['update_column']='vacant';
            $this->updateColumn($vacant,$dataQuery['count(id)'],$id);
            return $dataQuery['count(id)'];
        } catch(PDOException $exception) {
            return $exception;
        }
    }

    public function dynamic_property($id){
        try{
            $query = "SELECT name,user_type FROM users WHERE id='$id'";
            $dataQuery = $this->companyConnection->query($query)->fetch();
            $propertyName = '';
            if($dataQuery['user_type'] == '2'){
                $query = 'SELECT general_property.property_name FROM tenant_property LEFT JOIN general_property On tenant_property tenant_property.property_id=general_property.id WHERE tenant_property.user_id='.$id;
                $propertyName = $this->companyConnection->query($query)->fetch();
            } elseif($dataQuery['user_type'] == '3'){

            } elseif($dataQuery['user_type'] == '4'){
                $query = 'SELECT general_property.property_name FROM owner_property_owned LEFT JOIN general_property On owner_property_owned owner_property_owned.property_id=general_property.id WHERE owner_property_owned.user_id='.$id;
                $propertyName = $this->companyConnection->query($query)->fetch();
            }
            if(!empty($propertyName)){
                return $propertyName['property_name'];
            }
            return $propertyName;
        } catch(PDOException $exception) {
            return $exception;
        }
    }

    public function flagged_for_name($id, $object_type){
        try{
//            dd($object_type);
            if($object_type == 'property'){
                $query = "SELECT general_property.property_name as name FROM general_property WHERE id = $id";
                $dataQuery = $this->companyConnection->query($query)->fetch();
            } else if($object_type == 'building'){
                $query = "SELECT building_detail.building_name as name FROM building_detail WHERE id = $id";
                $dataQuery = $this->companyConnection->query($query)->fetch();
            } else if($object_type == 'unit'){
                $query = "SELECT unit_details.unit_prefix, unit_details.unit_no FROM unit_details WHERE id = $id";
                $dataQuery = $this->companyConnection->query($query)->fetch();
                if (!empty($dataQuery['unit_prefix'])){
                    $dataQuery['name'] = $dataQuery['unit_prefix'].'-'. $dataQuery['unit_no'];
                } else {
                    $dataQuery['name'] = $dataQuery['unit_no'];
                }
            } else {
                $query = "SELECT users.name as name FROM users WHERE id = $id";
                $dataQuery = $this->companyConnection->query($query)->fetch();
            }

            return $dataQuery['name'];
        } catch(PDOException $exception) {
            return $exception;
        }
    }

    public function current_bank_balance($id,$ini_amt)
    {
        $ini_amt=  str_replace(',','',$ini_amt);
      //  dd("SELECT cab.initial_amount,trans.total_charge_amount FROM transactions trans LEFT JOIN company_accounting_bank_account cab ON cab.id=trans.bank_id WHERE trans.user_type='PM' and trans.bank_id="."'$id'");
       /* $query = $this->companyConnection->query("SELECT trans.total_charge_amount as pay,trans.bank_id,user_type FROM transactions trans LEFT JOIN company_accounting_bank_account cab ON cab.id=trans.bank_id WHERE trans.user_type='PM' and trans.bank_id="."'$id'");*/
        $query = $this->companyConnection->query("SELECT trans.total_charge_amount as pay,trans.bank_id,user_type FROM transactions trans LEFT JOIN company_accounting_bank_account cab ON cab.id=trans.bank_id WHERE trans.bank_id="."'$id'");
        $data = $query->fetchAll();
        $query1 = $this->companyConnection->query("SELECT trans.bank_id,trans.total_charge_amount as dep FROM transactions trans LEFT JOIN company_accounting_bank_account cab ON cab.id=trans.bank_id WHERE (trans.user_type='TENANT' OR trans.user_type='VENDOR' OR trans.user_type='OWNER') AND trans.bank_id="."'$id'");
        $data1 = $query1->fetchAll();
        $tot_pay=0;
        $tot_dep=0;

        //echo"<pre>"; print_r($ini_amt);
        foreach ($data as $key=>$val){
            $tot_pay=floatval($tot_pay)+floatval($val['pay']);
        }
        foreach ($data1 as $k=>$v){
            $tot_dep=floatval($tot_dep)+floatval($v['dep']);
        }
        $tot_pay=number_format((float)$tot_pay, 2, '.', '');
        $tot_dep=number_format((float)$tot_dep, 2, '.', '');
        $bal=(floatval($ini_amt)+floatval($tot_dep))-(floatval($tot_pay));
        $bal=number_format((float)$bal, 2, '.', '');
        $amount_paid=[];
        $bank_id=[];
        $iniAmt=[];
        $user_type=[];
        //dd($data);
        for($i=0;$i<count($data);$i++){
            array_push($amount_paid,$data[$i]['pay']);
            array_push($bank_id,$data[$i]['bank_id']);
            array_push($iniAmt,$ini_amt);
            array_push($iniAmt,$ini_amt);
            array_push($user_type,$data[$i]['user_type']);
        }
       // echo "<pre>";print_r($user_type);
       for($j=0;$j<count($bank_id);$j++){
           if($user_type!='PM'){
               $test=($iniAmt[$j]+$amount_paid[$j]);
           }else{
               $test=($iniAmt[$j]-$amount_paid[$j]);
           }
       }

       // echo"<pre>"; print_r($id);
       // echo"<pre>"; print_r($ini_amt);
     /*   echo"<pre>"; print_r($tot_dep);
        echo"<pre>"; print_r($tot_pay);*/
       // echo"<pre>"; print_r($data);
        $init_amt=$data[0]['initial_amount'];

        return $test;
    }


    /**
     * function to get default bank account name of property
     * @param $data
     * @return string
     */
    function default_bank_account($data){
        try{
            $sqlQuery = 'SELECT * FROM property_bank_details WHERE property_id='.$data.' AND is_default="1"';
            $data = $this->companyConnection->query($sqlQuery)->fetch();
            if(!empty($data)){
                return $data['bank_name'];
            }
            return '';
        } catch(Exception $exception) {
            dd($exception);
        }
    }

    public function propertyOwner($id){
        try {
            $query = 'SELECT general_property.property_name FROM owner_property_owned LEFT JOIN general_property ON owner_property_owned.property_id=general_property.id WHERE user_id='.$id;
            $data = $this->companyConnection->query($query)->fetchAll();
            $property = '';
            if(!empty($data)){
                foreach ($data as $value){
                    $property .= $value['property_name'].', ';
                }
            }
            $property = str_replace_last(', ','',$property);
            return $property;
        } catch (Exception $exception){
            dd($exception);
        }
    }

}

$helper = new jqGrid();