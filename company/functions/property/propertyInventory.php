<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class PropertyInventory extends DBConnection {

    /**
     * PropertyInventory constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get sublocation of property
     * @return array
     */
    public function getSublocation(){
        try {
            $sql = "SELECT * FROM maintenance_inventory_sublocation";
            $data = $this->companyConnection->query($sql)->fetchAll();
            if (!empty($data) && count($data) > 0) {
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception){
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to get buildings of property
     * @return array
     */
    public function getBuilding(){
        try {
            $property_id = $_POST['property_id'];
            $sql = "SELECT * FROM building_detail WHERE property_id=".$property_id." AND deleted_at is NULL";
            $data = $this->companyConnection->query($sql)->fetchAll();
            if (!empty($data) && count($data) > 0) {
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to get units based on property
     * @return array
     */
    public function getUnit(){
        try {
            $building_id = $_POST['building_id'];
            $sql = "SELECT * FROM unit_details WHERE building_id=".$building_id." AND deleted_at is NULL";
            $data = $this->companyConnection->query($sql)->fetchAll();
            if (!empty($data) && count($data) > 0) {
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to get category
     * @return array
     */
    public function getCategory(){
        try {
            $sql = "SELECT * FROM maintenance_inventory_category";
            $data = $this->companyConnection->query($sql)->fetchAll();
            if (!empty($data) && count($data) > 0) {
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to get sub category on category
     * @return array
     */
    public function getSubCategory(){
        try {
            $sql = "SELECT * FROM maintenance_inventory_subcategory";
            $data = $this->companyConnection->query($sql)->fetchAll();
            if (!empty($data) && count($data) > 0) {
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to add inventory to property
     * @return array
     */
    public function addInventory(){
        try {
            $data = $_POST;
            $inventory_image1 = json_decode($_POST['inventory_image1']);
            $inventory_image2 = json_decode($_POST['inventory_image2']);
            $inventory_image3 = json_decode($_POST['inventory_image3']);
            $photo_uploaded = $_POST['pic_data'];
            unset($data['pic_data'],$data['inventory_image1'],$data['inventory_image2'],$data['inventory_image3'],$data['action'],$data['class']);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            $number_array = [];
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'data' => $err_array, 'message' => ERROR_VALIDATION);
            } else {
                $id = (isset($data['id']) && !empty($data['id']))?$data['id']:null;
                $data['select_type'] = $data['select_type'];
                $data['building_id'] = $data['building_id'];
                $data['photo_uploaded'] = ($photo_uploaded == 'yes')?'1':'0';
                $data['unit_id'] = ($data['unit_id'] != '0') ? $data['unit_id'] : null;
                $data['inventory_img1'] = $inventory_image1;
                $data['inventory_img2'] = $inventory_image2;
                $data['inventory_img3'] = $inventory_image3;
                $data['sublocation'] = $data['sublocation'];
                $data['guarantee'] = $data['guarantee'];
                $data['category'] = $data['category'];
                $data['sub_category'] = $data['sub_category'];
                $data['item_code'] = $data['item_code'];
                $data['warranty_from'] = !empty($data['warranty_from']) ? mySqlDateFormat($data['warranty_from'], null, $this->companyConnection) : null;
                $data['warranty_to'] = !empty($data['warranty_to']) ? mySqlDateFormat($data['warranty_to'], null, $this->companyConnection) : null;
                $data['installation_date'] = !empty($data['installation_date']) ? mySqlDateFormat($data['installation_date'], null, $this->companyConnection) : null;
                $data['replacement_date'] = !empty($data['replacement_date']) ? mySqlDateFormat($data['replacement_date'], null, $this->companyConnection) : null;
                unset($data['id']);
                if(!empty($id)){
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE property_inventory SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($sqlData['data']);
                    $message = SUCCESS_UPDATED;
                } else {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO  property_inventory (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $message = SUCCESS_ADDED;
                }
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => $message);
            }
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to get inventory based on id
     * @return array
     */
    public function getInventory(){
        try {
            $id = $_POST['id'];
            $sql = 'SELECT * FROM property_inventory WHERE id='.$id;
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data)) {
                foreach ($data as $key => $value) {
                    if($key == 'warranty_from' || $key == 'warranty_to' || $key == 'installation_date' || $key == 'replacement_date'){
                        $data[$key] = !empty($value)?dateFormatUser($value, null, $this->companyConnection):null;
                    }
                }
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'data' => $data, 'message' => SUCCESS_FETCHED);
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to delete property inventory.
     * @return array
     */
    public function deleteInventory(){
        try {
            $id = $_POST['id'];
            $sql = "DELETE FROM property_inventory where id=".$id;
            $data = $this->companyConnection->prepare($sql);
            $data->execute();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_DELETE);
        } catch(Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to print property inventory data
     * @return array
     */
    public function getPropertyInventoryPrintData() {
        dd($_POST);
        $html = '';
        $current_date = date("Y-m-d");
        $formated_date = dateFormatUser($current_date,null,$this->companyConnection);
        $next_date = date('Y-m-d', strtotime("+1 day"));
        $previous_date = date('Y-m-d', strtotime("-1 days"));
        $sql =  "SELECT  time_sheet.id,  time_sheet.employee_id, time_sheet.position, time_sheet.email, time_sheet.mobile_number, time_sheet.date, time_sheet.last_login, time_sheet.total_hours,users.name,company_user_roles.role_name FROM time_sheet LEFT JOIN users ON time_sheet.employee_id=users.id LEFT JOIN company_user_roles ON time_sheet.position=company_user_roles.id WHERE ( time_sheet.deleted_at IS NULL AND DATE(time_sheet.created_at) BETWEEN '".$previous_date."' AND '".$next_date."')";
        // $sql = "SELECT * FROM  phone_call_logs WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= '<div id="TestPhoneHeight" class="" style=" max-width: 1348px; ">
                                                    <div class="div-full3">
                                                        <div class="div-full3">
                                                            <div id="PrintPhone" style="page-break-after: always;">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<style type="text/css">
	body{ margin: 0; padding:0;  }
</style>
	<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tbody><tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; "><img src="../../image/avatar/" width="150" height="50" alt=""></td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;">
				<table style="width: 100%; padding: 0 0 20px 0">
					<tbody><tr>
						<td width="33.33%"></td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">Employee TimeSheet</td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">'.$formated_date.'</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</tbody></table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
					<tbody><tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858;  font-weight: bold; font-family: arial;  font-size: 16px;">Employee Name</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Role</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Email</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Phone</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Last Login</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; color: #585858; font-weight: bold; font-family: arial;  font-size: 16px;">Time</th>
					</tr>';
        if(!empty($data)) {
//           dd($data);
            //dd(SESSION_DOMAIN);
            foreach ($data as $key => $calllogdata) {
                $due_date= '';
                $call_category = '';
                $follow_up = '';
                $role_data = (isset($calllogdata["role_name"]) && !empty($calllogdata["role_name"]))? $calllogdata["role_name"]: 'Employee';
                $due_date = (isset($calllogdata["date"]) && !empty($calllogdata["date"]))? dateFormatUser($calllogdata["date"],null,$this->companyConnection): '';
                $last_date = (isset($calllogdata["last_login"]) && !empty($calllogdata["last_login"]))? dateFormatUser($calllogdata["last_login"],null,$this->companyConnection): '';
                $time_hours = (isset($calllogdata["total_hours"]) && !empty($calllogdata["total_hours"]))? $calllogdata["total_hours"].' Hours': '';
                $html .= '<tr><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["name"].'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$role_data.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["email"] .'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$calllogdata["mobile_number"]. '</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$due_date.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$last_date.'</td><td style="border:2px solid #e9e9e9; padding: 10px 5px; font-family: arial;  font-size: 14px; text-align: center;">'.$time_hours.'</td></tr>';
            }
        }
        $html.='</tbody></table>

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</tbody></table>

</div>
                                                        </div>
                                                        <div class="btn-outer3" style="padding-top: 2%;">
                                                        </div>
                                                    </div>
                                                    <!--/ END panel -->
                                                </div>';
        return array('data' => $html, 'status' => 'success','code' => 200);
    }


}

$PropertyInventory = new PropertyInventory();
?>