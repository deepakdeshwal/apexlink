<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");

class propertyView extends DBConnection
{

    /**
     * propertyView constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get property details
     */
    public function getPropertyDetail()
    {

        $id = $_POST['property_id'];
        $table = 'general_property';
        $columns = [$table . '.property_id', $table . '.property_name', $table . '.legal_name', $table . '.property_year', $table . '.phone_number', $table . '.property_tax_ids', $table . '.fax', $table . '.manager_list', $table . '.is_short_term_rental', $table . '.property_style', $table . '.property_price', $table . '.attach_groups', $table . '.property_type', $table . '.no_of_units', $table . '.property_subtype', $table . '.smoking_allowed', $table . '.no_of_buildings', 'company_pet_friendly.pet_friendly', $table . '.property_squareFootage', $table . '.key_access_codes_info', $table . '.key_access_code_desc', $table . '.amenities', $table . '.property_parcel_number',$table. '.no_of_vehicles',$table. '.garage_note', $table . '.description', 'company_garage_avail.garage_avail', 'company_property_style.property_style', 'company_property_type.property_type', 'company_property_subtype.property_subtype',$table . '.online_listing', $table . '.online_application', $table . '.disable_sync','company_property_portfolio.portfolio_id','company_property_portfolio.portfolio_name'];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'property_type',
            'primary' => 'id',
            'on_table' => 'company_property_type',
            'as' => 'company_property_type'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'property_style',
            'primary' => 'id',
            'on_table' => 'company_property_style',
            'as' => 'company_property_style'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'property_subtype',
            'primary' => 'id',
            'on_table' => 'company_property_subtype',
            'as' => 'company_property_subtype'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'pet_friendly',
            'primary' => 'id',
            'on_table' => 'company_pet_friendly',
            'as' => 'company_pet_friendly'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'garage_available',
            'primary' => 'id',
            'on_table' => 'company_garage_avail',
            'as' => 'company_garage_avail'
        ], [
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'portfolio_id',
            'primary' => 'id',
            'on_table' => 'company_property_portfolio',
            'as' => 'company_property_portfolio'
        ]];
        $where = [[
            'table' => 'general_property',
            'column' => 'id',
            'condition' => '=',
            'value' => $id
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);

        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');

        $unserializeData['smoking_allowed'] =  ($unserializeData['smoking_allowed'] == 'N/A')?'No':'Yes';
        $unserializeData['garage_available'] =  $unserializeData['garage_avail'];
        if($unserializeData['key_access_codes_info'] != 'N/A'){
            $key_code = '';
            foreach ($unserializeData['key_access_codes_info'] as $key => $value) {
                $sql = 'SELECT key_code FROM company_key_access WHERE id='.$value;
                $queryData = $this->companyConnection->query($sql)->fetch();
                $key_code .= $queryData['key_code'] . ',';
            }
            $unserializeData['key_access_codes_info'] = str_replace_last(',', '', $key_code);
        }

        $string = '';
        if(!empty($data['key_access_codes_info'])) {
            $string = '';
            $key_info_data = unserialize($data['key_access_codes_info']);
            $key_description_data = unserialize($data['key_access_code_desc']);
            //   $key_info_new_data = array_combine($key_description_data, $key_info_data);
            $info_data_count = count($key_info_data);
            $i=1;
            foreach ($key_info_data as $key => $value) {
                $key_description ='';
                $info_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $value], 'company_key_access');
                $key_description = (isset($key_description_data[$key] )&& !empty($key_description_data[$key]))?$key_description_data[$key]:'';
                $string .= $info_data['data']['key_code'] . ' : ' . $key_description;
                $string .= ($i < $info_data_count) ? ' , ' : ' ';
                $i++;

            }
        }
        if ($unserializeData['phone_number'] != 'N/A') {
            $phone_number = '';
            foreach ($unserializeData['phone_number'] as $key => $value) {
                $phone_number .= $value . ',';
            }
            $unserializeData['phone_number'] = str_replace_last(',', '', $phone_number);
        }
        if ($unserializeData['property_tax_ids'] != 'N/A') {
            $property_tax_ids = '';
            foreach ($unserializeData['property_tax_ids'] as $key => $value) {
                $property_tax_ids .= $value . ',';
            }
            $unserializeData['property_tax_ids'] = str_replace_last(',', '', $property_tax_ids);
        }
        if ($unserializeData['attach_groups'] != 'N/A') {
            $attach_groups = '';
            foreach ($unserializeData['attach_groups'] as $key => $value) {
                $where = [['table' => 'company_property_groups', 'column' => 'id', 'condition' => '=', 'value' => $value]];
                $query = selectQueryBuilder(['company_property_groups.group_name'], 'company_property_groups', null, $where);
                $groupData = $this->companyConnection->query($query)->fetch();
                $attach_groups .= $groupData['group_name'] . ',';
            }
            $unserializeData['attach_groups'] = str_replace_last(',', '', $attach_groups);
        }
        $unserializeData['is_short_term_rental'] = ($unserializeData['is_short_term_rental'] == '1') ? 'Yes' : 'No';
        $unserializeData['online_listing'] = ($unserializeData['online_listing'] == 'on') ? 'Yes' : 'No';
        $unserializeData['online_application'] = ($unserializeData['online_application'] == 'on') ? 'Yes' : 'No';
        $unserializeData['disable_sync'] = ($unserializeData['disable_sync'] == 'on') ? 'Yes' : 'No';


        if ($unserializeData['amenities'] != 'N/A') {
            $amenities = '';
            foreach ($unserializeData['amenities'] as $key=>$value){
                $query = 'SELECT name FROM company_property_amenities WHERE id='.$value;
                $groupData = $this->companyConnection->query($query)->fetch();
                $name = $groupData['name'];
                $amenities .= $name.',';
            }
            $unserializeData['amenities'] = str_replace_last(',', '', $amenities);
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData,'key_access_codes_info_string'=>@$string, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get school district Info
     */
    public function getSchoolDistrictInfo()
    {
        $id = $_POST['property_id'];
        $table = 'general_property';
        $columns = [$table . '.school_district_municipality', $table . '.school_district_code', $table . '.school_district_notes'];
        $where = [['table' => 'general_property', 'column' => 'id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        $schoolDistrict = [];
        if ($unserializeData['school_district_municipality'] != 'N/A') {
            foreach ($unserializeData['school_district_municipality'] as $key => $value) {
                $schoolDistrict[$key] = ['school_district_municipality' => $unserializeData['school_district_municipality'][$key], 'school_district_code' => $unserializeData['school_district_code'][$key], 'school_district_notes' => $unserializeData['school_district_notes'][$key]];
            }
        }
        return array('code' => 200, 'status' => 'success', 'data' => $schoolDistrict, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get link owner Info
     */
    public function getLinkOwnerInfo()
    {
        $id = $_POST['property_id'];
        $table = 'general_property';
        $columns = [$table . '.owner_percentowned', $table . '.owner_id', $table . '.payment_type', $table . '.vendor_1099_payer', $table . '.fiscal_year_end'];
        $where = [['table' => 'general_property', 'column' => 'id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        $ownerDetails = [];
        if ($unserializeData['owner_id'] != 'N/A') {
            foreach ($unserializeData['owner_id'] as $key => $value) {
                $where = [['table' => 'users', 'column' => 'id', 'condition' => '=', 'value' => $value]];
                $query = selectQueryBuilder(['users.name'], 'users', null, $where);
                $userData = $this->companyConnection->query($query)->fetch();
                $ownerDetails[$key] = ['name' => $userData['name'], 'owner_percentowned' => $unserializeData['owner_percentowned'][1]];
            }
        }
        unset($unserializeData['owner_percentowned'], $unserializeData['owner_id']);
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'ownedData' => $ownerDetails, 'message' => 'Record retrieved Successfully.');
    }


    /**
     * function to get late Fee Management
     */
    public function getLateFeeManagement()
    {
        $id = $_POST['property_id'];
        $table = 'late_fee_management';
        $columns = [$table . '.apply_one_time_flat_fee', $table . '.apply_one_time_percentage', $table . '.apply_daily_flat_fee', $table . '.apply_daily_percentage'];
        $where = [['table' => 'late_fee_management', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Management_fees
     */
    public function getManagementFees()
    {
        $id = $_POST['property_id'];
        $table = 'management_fees';
        $columns = [$table . '.management_type', $table . '.management_value',$table . '.minimum_management_fee'];
        $where = [['table' => 'management_fees', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
//        dd($unserializeData);
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Management Info
     */
    public function getManagementInfo()
    {
        $id = $_POST['property_id'];
        $table = 'management_info';
        $columns = [$table . '.management_company_name', $table . '.management_start_date', $table . '.management_end_date', $table . '.reason_management_end', $table . '.description', 'company_management_reason.reason'];
        $where = [['table' => 'management_info', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'management_info',
            'column' => 'reason_management_end',
            'primary' => 'id',
            'on_table' => 'company_management_reason',
            'as' => 'company_management_reason'
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        if (!empty($unserializeData)) {
            $unserializeData['management_end_date'] = ($unserializeData['management_end_date'] != 'N/A') ? dateFormatUser($unserializeData['management_end_date'], null, $this->companyConnection) : $unserializeData['management_end_date'];
            $unserializeData['management_start_date'] = ($unserializeData['management_start_date'] != 'N/A') ? dateFormatUser($unserializeData['management_start_date'], null, $this->companyConnection) : $unserializeData['management_start_date'];
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Maintenance Info
     */
    public function getMaintenanceInfo()
    {
        $id = $_POST['property_id'];
        $table = 'maintenance_information';
        $columns = [$table . '.maintenance_speed_time', $table . '.amount', $table . '.Insurance_expiration', $table . '.warranty_expiration', $table . '.special_instructions'];
        $where = [['table' => 'maintenance_information', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        if (!empty($unserializeData)) {
            $unserializeData['Insurance_expiration'] = ($unserializeData['Insurance_expiration'] != 'N/A') ? dateFormatUser($unserializeData['Insurance_expiration'], null, $this->companyConnection) : $unserializeData['Insurance_expiration'];
            $unserializeData['warranty_expiration'] = ($unserializeData['warranty_expiration'] != 'N/A') ? dateFormatUser($unserializeData['warranty_expiration'], null, $this->companyConnection) : $unserializeData['warranty_expiration'];
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Mortgage Info
     */
    public function getMortgageInfo()
    {
        $id = $_POST['property_id'];
        $table = 'mortgage_information';
        $columns = [$table . '.mortgage_amount', $table . '.property_assessed_amount', $table . '.mortgage_start_date', $table . '.term', $table . '.interest_rate', $table . '.financial_institution'];
        $where = [['table' => 'mortgage_information', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        if (!empty($unserializeData)) {
            $unserializeData['mortgage_start_date'] = ($unserializeData['mortgage_start_date'] != 'N/A') ? dateFormatUser($unserializeData['mortgage_start_date'], null, $this->companyConnection) : $unserializeData['mortgage_start_date'];
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Property Loan
     */
    public function getPropertyLoan()
    {
        $id = $_POST['property_id'];
        $table = 'property_loan';
        $columns = [$table . '.loan_status', $table . '.vendor_name', $table . '.loan_amount', $table . '.loan_start_date', $table . '.loan_maturity_date', $table . '.monthly_due_date', $table . '.payment_amount', $table . '.loan_terms', $table . '.loan_rate', $table . '.loan_number', $table . '.interest_only'];
        $where = [['table' => 'property_loan', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        if (!empty($unserializeData)) {
            $unserializeData['loan_start_date'] = ($unserializeData['loan_start_date'] != 'N/A') ? dateFormatUser($unserializeData['loan_start_date'], null, $this->companyConnection) : $unserializeData['loan_start_date'];
            $unserializeData['loan_maturity_date'] = ($unserializeData['loan_maturity_date'] != 'N/A') ? dateFormatUser($unserializeData['loan_maturity_date'], null, $this->companyConnection) : $unserializeData['loan_maturity_date'];
            $unserializeData['loan_status'] = ($unserializeData['loan_status'] == '1') ? 'Yes' : 'No';
            $unserializeData['interest_only'] = ($unserializeData['interest_only'] == 'yes') ? 'Yes' : 'No';
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Property Custom Fields
     */
    public function getCustomFields()
    {
        $id = $_POST['property_id'];
        $table = 'property_custom_fields';
        $columns = [$table . '.custom_field'];
        $where = [['table' => 'property_custom_fields', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Property Custom Fields
     */
    public function getDetails()
    {
        $id = $_POST['property_id'];
        $table = 'general_property';
        $columns = [$table . '.property_name', $table . '.property_id', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4'];
        $where = [['table' => 'general_property', 'column' => 'id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();

        $imageTable = 'property_file_uploads';
        $imageColumns = [$imageTable . '.file_location'];
        $imageWhere = [['table' => 'property_file_uploads', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $imageQuery = selectQueryBuilder($imageColumns, $imageTable, null, $imageWhere);
        $imageData = $this->companyConnection->query($imageQuery)->fetchAll();
        $images_count = count($imageData);
        if ($images_count <= 3) {
            $newdata = array(
                'file_name' => 'no-image.png',
                'file_location' => 'images/no-image.png'
            );
            for ($i = $images_count; $i < 3; $i++) {
                $imageData[$i] = $newdata;
            }
        }
        return array('code' => 200, 'status' => 'success', 'data' => $data, 'imageData' => $imageData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * function to get Property Custom Fields
     */
    public function getPropertyComplaints()
    {
        $id = $_POST['property_id'];
        $table = 'complaints';
        $columns = [$table . '.property_name', $table . '.property_id', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4'];
        $where = [['table' => 'general_property', 'column' => 'id', 'condition' => '=', 'value' => $id]];
        $query = selectQueryBuilder($columns, $table, null, $where);
        $data = $this->companyConnection->query($query)->fetch();

        $imageTable = 'property_file_uploads';
        $imageColumns = [$imageTable . '.file_location'];
        $imageWhere = [['table' => 'property_file_uploads', 'column' => 'property_id', 'condition' => '=', 'value' => $id]];
        $imageQuery = selectQueryBuilder($imageColumns, $imageTable, null, $imageWhere);
        $imageData = $this->companyConnection->query($imageQuery)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'data' => $data, 'imageData' => $imageData, 'message' => 'Record retrieved Successfully.');
    }

    /**
     * Import function
     */
    public function importExcel()
    {
        if (isset($_FILES['file'])) {
            if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls", "xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if (in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if ($file_size < 3000) {
                        $file = "uploads/" . $_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if ($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status' => 'failed', 'code' => 503, 'data' => $error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                            $extra_columns = ",`status`,`created_at`, `updated_at`";
//                            $status = 1;
//                            $extra_values = ",'" . $status . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
//                            $query = "insert into `general_property` (`property_name`,`legal_name`,`portfolio_id`,`zipcode`,`country`,`state`,`city`,`address1`,`address2`,`property_price`,`attach_groups`,`property_type`,`property_style`,`property_subtype`,`property_year`,`property_squareFootage`,`no_of_buildings`,`no_of_units`,`amenities`,`description`$extra_columns) VALUES ";
                            //Loop through each row of the worksheet
//                            dd($total_rows);
                            for ($row = 2; $row <= $total_rows; $row++) {
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                //$data = $this->companyConnection->query("SELECT property_name FROM general_property")->fetchAll();
                                $duplicate_property = false;
                                if ($first_row_header == 'PropertyName') {

                                    foreach ($single_row[0] as $key=>$value) {

                                        if(empty($value)) continue;
                                        if($key == 0){
                                            $propertyName = checkNameAlreadyExists($this->companyConnection,'general_property','property_name',$value,null);
                                            if($propertyName['is_exists'] == 1){
                                                if($total_rows == 2){
                                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Property with same name already exists.'];
                                                } else {
                                                    $duplicate_property = true;
                                                }
                                            }
                                        }
                                        switch ($key) {
                                            case "2":
                                                $portfolioData = getSingleRecord($this->companyConnection, ['column' => 'portfolio_name', 'value' => $value], 'company_property_portfolio');
                                                if($portfolioData['code'] == 200){
                                                    $single_row[0][$key] = $portfolioData['data']['id'];
                                                } else {
                                                    $insert = [];
                                                    $insert['portfolio_id'] = randomString();
                                                    $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                    $insert['portfolio_name'] = $value;
                                                    $insert['is_default'] = '0';
                                                    $insert['is_editable'] = '1';
                                                    $insert['status'] = '1';
                                                    $insert['created_at'] = date('Y-m-d H:i:s');
                                                    $insert['updated_at'] = date('Y-m-d H:i:s');
                                                    $sqlData = createSqlColVal($insert);
                                                    $query = "INSERT INTO company_property_portfolio (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                    $stmt = $this->companyConnection->prepare($query);
                                                    $stmt->execute($insert);
                                                    $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                }
                                                break;
                                            case "12":
                                                $groupArray = [];
                                                if (strpos($value, ',') !== false) {
                                                    $dataGroup = explode(',',$value);
                                                    foreach ($dataGroup as $key1=>$value1) {
                                                        $groupData = getSingleRecord($this->companyConnection, ['column' => 'group_name', 'value' => $value1], 'company_property_groups');
                                                        if($groupData['code']== 200){
                                                            array_push($groupArray, $groupData['data']['id']);
                                                        } else {
                                                            $insert = [];
                                                            $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                            $insert['group_name'] = $value1;
                                                            $insert['status'] = '1';
                                                            $insert['is_default'] = '0';
                                                            $insert['created_at'] = date('Y-m-d H:i:s');
                                                            $insert['updated_at'] = date('Y-m-d H:i:s');
                                                            $sqlData = createSqlColVal($insert);
                                                            $query = "INSERT INTO company_property_groups (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                            $stmt = $this->companyConnection->prepare($query);
                                                            $stmt->execute($insert);
                                                            array_push($groupArray, $this->companyConnection->lastInsertId());
                                                        }
                                                    }
                                                    $single_row[0][$key] = serialize($groupArray);
                                                } else {
                                                    $groupData = getSingleRecord($this->companyConnection, ['column' => 'group_name', 'value' => $value], 'company_property_groups');
                                                    if($groupData['code']== 200){
                                                        array_push($groupArray, $groupData['data']['id']);
                                                    } else {
                                                        $insert = [];
                                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                        $insert['group_name'] = $value;
                                                        $insert['is_default'] = 0;
                                                        $insert['status'] = '1';
                                                        $insert['is_default'] = '1';
                                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                                        $sqlData = createSqlColVal($insert);
                                                        $query = "INSERT INTO company_property_groups (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                        $stmt = $this->companyConnection->prepare($query);
                                                        $stmt->execute($insert);
                                                        array_push($groupArray, $this->companyConnection->lastInsertId());
                                                    }
                                                }
                                                $single_row[0][$key] = serialize($groupArray);
                                                break;
                                            case "13":
                                                $portfolioType = getSingleRecord($this->companyConnection, ['column' => 'property_type', 'value' => $value], 'company_property_type');
                                                if($portfolioType['code'] == 200){
                                                    $single_row[0][$key] = $portfolioType['data']['id'];
                                                } else {
                                                    $insert = [];
                                                    $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                    $insert['property_type'] = $value;
                                                    $insert['is_default'] = '0';
                                                    $insert['status'] = '1';
                                                    $insert['created_at'] = date('Y-m-d H:i:s');
                                                    $insert['updated_at'] = date('Y-m-d H:i:s');
                                                    $sqlData = createSqlColVal($insert);
                                                    $query = "INSERT INTO company_property_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                    $stmt = $this->companyConnection->prepare($query);
                                                    $stmt->execute($insert);
                                                    $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                }
                                                break;
                                            case "14":
                                                $portfolioStyle = getSingleRecord($this->companyConnection, ['column' => 'property_style', 'value' => $value], 'company_property_style');
                                                if($portfolioStyle['code'] == 200){
                                                    $single_row[0][$key] = $portfolioStyle['data']['id'];
                                                } else {
                                                    $insert = [];
                                                    $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                    $insert['property_style'] = $value;
                                                    $insert['is_default'] = '0';
                                                    $insert['status'] = '1';
                                                    $insert['created_at'] = date('Y-m-d H:i:s');
                                                    $insert['updated_at'] = date('Y-m-d H:i:s');
                                                    $sqlData = createSqlColVal($insert);
                                                    $query = "INSERT INTO company_property_style (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                    $stmt = $this->companyConnection->prepare($query);
                                                    $stmt->execute($insert);
                                                    $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                }
                                                break;
                                            case "15":
                                                $portfolioSubType = getSingleRecord($this->companyConnection, ['column' => 'property_subtype', 'value' => $value], 'company_property_subtype');
                                                if($portfolioSubType['code'] == 200){
                                                    $single_row[0][$key] = $portfolioSubType['data']['id'];
                                                } else {
                                                    $insert = [];
                                                    $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                    $insert['property_subtype'] = $value;
                                                    $insert['is_default'] = '0';
                                                    $insert['status'] = '1';
                                                    $insert['created_at'] = date('Y-m-d H:i:s');
                                                    $insert['updated_at'] = date('Y-m-d H:i:s');
                                                    $sqlData = createSqlColVal($insert);
                                                    $query = "INSERT INTO company_property_subtype (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                    $stmt = $this->companyConnection->prepare($query);
                                                    $stmt->execute($insert);
                                                    $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                }
                                                break;
                                            case "20":
                                                $amenitiesArray = [];
                                                if (strpos($value, ',') !== false) {
                                                    $dataGroup = explode(',',$value);
                                                    foreach ($dataGroup as $key1=>$value1) {
                                                        $amenitiesData = getSingleRecord($this->companyConnection, ['column' => 'name', 'value' => $value1], 'company_property_amenities');
                                                        if($amenitiesData['code']== 200){
                                                            array_push($groupArray, $amenitiesData['data']['id']);
                                                        } else {
                                                            $insert = [];
                                                            $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                            $insert['name'] = $value1;
                                                            $insert['status'] = '1';
                                                            $insert['created_at'] = date('Y-m-d H:i:s');
                                                            $insert['updated_at'] = date('Y-m-d H:i:s');
                                                            $sqlData = createSqlColVal($insert);
                                                            $query = "INSERT INTO company_property_amenities (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                            $stmt = $this->companyConnection->prepare($query);
                                                            $stmt->execute($insert);
                                                            array_push($amenitiesArray, $value1);
                                                        }
                                                    }
                                                    $single_row[0][$key] = serialize($amenitiesArray);
                                                } else {
                                                    $amenitiesData = getSingleRecord($this->companyConnection, ['column' => 'name', 'value' => $value], 'company_property_amenities');
                                                    if($amenitiesData['code']== 200){
                                                        array_push($groupArray, $amenitiesData['data']['id']);
                                                    } else {
                                                        $insert = [];
                                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                        $insert['name'] = $value;
                                                        $insert['status'] = '1';
                                                        $insert['type'] = '1';
                                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                                        $sqlData = createSqlColVal($insert);
                                                        $query = "INSERT INTO company_property_amenities (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                        $stmt = $this->companyConnection->prepare($query);
                                                        $stmt->execute($insert);
                                                        array_push($amenitiesArray, $value);
                                                    }
                                                    $single_row[0][$key] = serialize($amenitiesArray);
                                                }
                                                break;
                                            default:
                                                continue;
                                        }
                                    }
                                }
                                if($duplicate_property === false) {
                                    $generalProperty = [];
                                    $generalProperty['property_id'] = randomString();
                                    $generalProperty['property_name'] = $single_row[0][0];
                                    $generalProperty['legal_name'] = $single_row[0][1];
                                    $generalProperty['portfolio_id'] = $single_row[0][2];
                                    $generalProperty['status'] = '1';
                                    $generalProperty['zipcode'] = $single_row[0][3];
                                    $generalProperty['country'] = $single_row[0][4];
                                    $generalProperty['state'] = $single_row[0][5];
                                    $generalProperty['city'] = $single_row[0][6];
                                    $generalProperty['address1'] = $single_row[0][7];
                                    $generalProperty['address2'] = $single_row[0][8];
                                    $generalProperty['address3'] = $single_row[0][9];
                                    $generalProperty['address4'] = $single_row[0][10];
                                    $generalProperty['property_price'] = $single_row[0][11];
                                    $generalProperty['attach_groups'] = $single_row[0][12];
                                    $generalProperty['property_type'] = $single_row[0][13];
                                    $generalProperty['property_style'] = $single_row[0][14];
                                    $generalProperty['property_subtype'] = $single_row[0][15];
                                    $generalProperty['property_year'] = $single_row[0][16];
                                    $generalProperty['property_squareFootage'] = $single_row[0][17];
                                    $generalProperty['no_of_buildings'] = $single_row[0][18];
                                    $generalProperty['no_of_units'] = $single_row[0][19];
                                    $generalProperty['amenities'] = $single_row[0][20];
                                    $generalProperty['description'] = $single_row[0][21];
                                    $generalProperty['created_at'] = date('Y-m-d H:i:s');
                                    $generalProperty['update_at'] = date('Y-m-d H:i:s');
                                    $sqlData = createSqlColVal($generalProperty);
                                    $query = "INSERT INTO general_property (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                    $stmt = $this->companyConnection->prepare($query);
                                    $executed_query = $stmt->execute($generalProperty);
                                }
                            }
                            if (isset($executed_query)) {
                                unlink($file);
                                return ['status' => 'success', 'code' => 200, 'message' => 'Data imported successfully.'];
                            } else {
                                return ['status' => 'failed', 'code' => 503, 'message' => 'Property with same name already exists.'];
                            }
                        } else {
                            return ['status' => 'failed', 'code' => 503, 'message' => 'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status' => 'failed', 'code' => 503, 'message' => 'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status' => 'failed', 'code' => 503, 'message' => 'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status' => 'failed', 'code' => 503, 'message' => 'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }

    /**
     *  Export Excel
     */
    public function exportExcel()
    {
        include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];


        $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

//        $query1 = "SELECT * FROM $table";
        $columns = [$table . '.*', 'company_property_type.property_type','company_property_style.property_style','company_property_subtype.property_subtype','company_property_portfolio.portfolio_name'];
        $where = [['table' => $table, 'column' => 'deleted_at', 'condition' => 'IS NULL', 'value' =>'']];
        $joins = [
            [
                'type' => 'LEFT',
                'table' => $table,
                'column' => 'portfolio_id',
                'primary' => 'id',
                'on_table' => 'company_property_portfolio',
                'as' => 'company_property_portfolio'
            ],
            [
            'type' => 'LEFT',
            'table' => $table,
            'column' => 'property_type',
            'primary' => 'id',
            'on_table' => 'company_property_type',
            'as' => 'company_property_type'
            ],
            [
                'type' => 'LEFT',
                'table' => $table,
                'column' => 'property_style',
                'primary' => 'id',
                'on_table' => 'company_property_style',
                'as' => 'company_property_style'
            ],
            [
                'type' => 'LEFT',
                'table' => $table,
                'column' => 'property_subtype',
                'primary' => 'id',
                'on_table' => 'company_property_subtype',
                'as' => 'company_property_subtype'
            ]
        ];
        $orderBy = ['column'=>'property_name','sort'=>'ASC'];
        $query = selectQueryBuilder($columns, $table, $joins, $where,$orderBy);
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        //Set header with temp array
        $tmparray = array("PropertyName", "LegalName", "PortfolioName", "ZipCode", "Country", "State", "City", "Address1", "Address2", "Address3", "Address4", "PropertyPrice", "GroupName", "PropertyType", "PropertyStyle", "PropertySubType", "YearBuilt", "TotalSqFootage", "NumberOfBuildings", "NumberofUnits", "Amenities", "Descriptions");
        //take new main array and set header array in it.
        $sheet = array($tmparray);
        while ($property_detail_data = $stmt->fetch(PDO::FETCH_ASSOC)) {
//            dd($property_detail_data);
            if(!empty($property_detail_data['attach_groups'])){
                $property_detail_data['attach_groups'] = $this->getAttachGroups($property_detail_data['attach_groups']);
            }
            if(!empty($property_detail_data['amenities'])){
                $property_detail_data['amenities'] = $this->getAmenities($property_detail_data['amenities']);
            }
            $tmparray = array();
            $property_name = $property_detail_data['property_name'];
            array_push($tmparray, $property_name);
            $LegalName = $property_detail_data['legal_name'];
            array_push($tmparray, $LegalName);
            $PortfolioName = $property_detail_data['portfolio_name'];
            array_push($tmparray, $PortfolioName);
            $ZipCode = $property_detail_data['zipcode'];
            array_push($tmparray, $ZipCode);
            $Country = $property_detail_data['country'];
            array_push($tmparray, $Country);
            $State = $property_detail_data['state'];
            array_push($tmparray, $State);
            $City = $property_detail_data['city'];
            array_push($tmparray, $City);
            $Address1 = $property_detail_data['address1'];
            array_push($tmparray, $Address1);
            $Address2 = $property_detail_data['address2'];
            array_push($tmparray, $Address2);
            $Address3 = $property_detail_data['address3'];
            array_push($tmparray, $Address3);
            $Address4 = $property_detail_data['address4'];
            array_push($tmparray, $Address4);
            $PropertyPrice = $property_detail_data['property_price'];
            array_push($tmparray, $PropertyPrice);
            $GroupName = $property_detail_data['attach_groups'];
            array_push($tmparray, $GroupName);
            $PropertyType = $property_detail_data['property_type'];
            array_push($tmparray, $PropertyType);
            $PropertyStyle = $property_detail_data['property_style'];
            array_push($tmparray, $PropertyStyle);
            $PropertySubType = $property_detail_data['property_subtype'];
            array_push($tmparray, $PropertySubType);
            $YearBuilt = $property_detail_data['property_year'];
            array_push($tmparray, $YearBuilt);
            $TotalSqFootage = $property_detail_data['property_squareFootage'];
            array_push($tmparray, $TotalSqFootage);
            $NumberOfBuildings = $property_detail_data['no_of_buildings'];
            array_push($tmparray, $NumberOfBuildings);
            $NumberofUnits = $property_detail_data['no_of_units'];
            array_push($tmparray, $NumberofUnits);
            $Amenities = $property_detail_data['amenities'];
            array_push($tmparray, $Amenities);
            $Descriptions = $property_detail_data['description'];
            array_push($tmparray, $Descriptions);
            array_push($sheet, $tmparray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach ($sheet as $row => $columns) {
            foreach ($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="PropertySample.xlsx"');
        $objWriter->save('php://output');
        die();
    }

    /**
     * function to get attach group string
     * @param $attachGroups
     * @return mixed
     */
    public function getAttachGroups($attachGroups){
        $attachGroups = unserialize($attachGroups);
        $data = '';
        foreach ($attachGroups as $key=>$value){
            $query = 'SELECT group_name FROM company_property_groups WHERE id='.$value;
            $groupData = $this->companyConnection->prepare($query);
            $groupData->execute();
            $groupData = $groupData->fetch();
            $data .= $groupData['group_name'].',';
        }
        return str_replace_last(',','',$data);
    }

    /**
     * function to get attach group string
     * @param $amenities
     * @return mixed
     */
    public function getAmenities($amenities){
        try{
            $amenities = unserialize($amenities);
            $data = '';
            foreach ($amenities as $key=>$value){
//                echo "<pre>";print_r($value);
                $query = 'SELECT name FROM company_property_amenities WHERE id='.$value;
                $groupData = $this->companyConnection->prepare($query);
                $groupData->execute();
                $groupData = $groupData->fetch();
                $data .= $groupData['name'].',';
            }
//            die;
            return str_replace_last(',','',$data);
        } catch (PDOException $e) {
            dd($e);
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }

    }

    /**
     * download Sample
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/PropertySample.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }
}

$propertyDetail = new propertyView();
?>