<?php

include(ROOT_URL . "/config.php");
include_once( ROOT_URL . "/company/functions/FlashMessage.php");
include_once( ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class propertyEdit extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * function to add custom field.
     * @return array|void
     */
    public function view() {
        try {
//            dd($_GET['id']);
            $current_date = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
            $data = propertyEdit::editPropertyData($_GET['id']);
            $datarenovationdetails = propertyEdit::getRenovationDetails($_GET['id']);
            $schooldistrictdata = propertyEdit::getSchoolDistrictData($_GET['id']);
            $KeyaccesscodeData = propertyEdit::getKeyaccesscodeData($_GET['id']);
            $managementFeeData = propertyEdit::managementFeeData($_GET['id']);
            $getBankdetails = propertyEdit::getBankdetails($_GET['id']);
            $getManagementInfo = propertyEdit::getManagementInfo($_GET['id']);
            $getMaintenanceInformation = propertyEdit::getMaintenanceInformation($_GET['id']);
            $getMortgageInformation = propertyEdit::getMortgageInformation($_GET['id']);
            $getPropertyLoan = propertyEdit::getPropertyLoan($_GET['id']);
            $getLateFeemanagement = propertyEdit::getLateFeemanagement($_GET['id']);
            $getLinkOwnerData = propertyEdit::getLinkOwnerData($_GET['id']);
            $getkeycheckout=propertyEdit::getkeycheckout($_GET['id']);

            $redirect = VIEW_COMPANY . "company/properties/edit-property.php";
            return require $redirect;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function editPropertyData($id) {
        $property_tax_ids = [];
        $phone_number = [];
        $amenties = [];
        $sql = "SELECT * FROM general_property WHERE id='" . $id . "' AND deleted_at IS NULL";
        $data = $this->companyConnection->query($sql)->fetch();
        //renovation detail
        $unserdata = getUnserializedData($data);
        return $unserdata;
    }

    public function getRenovationDetails($id) {
        $renovationInfo = "SELECT * FROM renovation_details WHERE object_id='" . $id . "' AND deleted_at IS NULL ORDER BY id DESC";
        $renovationdetails = $this->companyConnection->query($renovationInfo)->fetch();

        return $renovationdetails;
    }

    public function getSchoolDistrictData($id) {

        $schoolsql = "SELECT * FROM general_property WHERE id='" . $id . "' AND deleted_at IS NULL";
        $dataschool = $this->companyConnection->query($schoolsql)->fetch();
        $school_district_muncipiality = [];
        $school_district_code = [];
        $school_district_notes = [];
        $resultSchooldata = [];
        if (isset($dataschool["school_district_municipality"]) && !empty($dataschool["school_district_municipality"]))
            $school_district_muncipiality = unserialize($dataschool["school_district_municipality"]);
        if (isset($dataschool["school_district_code"]) && !empty($dataschool["school_district_code"]))
            $school_district_code = unserialize($dataschool["school_district_code"]);
        if (isset($dataschool["school_district_notes"]) && !empty($dataschool["school_district_notes"]))
            $school_district_notes = unserialize($dataschool["school_district_notes"]);
        $resultSchooldata = array_map(function ($name, $type, $price) {
            return array_combine(
                    ['school_district', 'code', 'notes'], [$name, $type, $price]
            );
        }, $school_district_muncipiality, $school_district_code, $school_district_notes);


        return $resultSchooldata;
    }

    public function getKeyaccesscodeData($id) {

        $schoolsql = "SELECT * FROM general_property WHERE id='" . $id . "' AND deleted_at IS NULL";
        $dataschool = $this->companyConnection->query($schoolsql)->fetch();
        $key_access_codes_info = [];
        $key_access_codes_desc = [];
        $resultSchooldata = [];
        if (isset($dataschool["key_access_codes_info"]) && !empty($dataschool["key_access_codes_info"]))
            $key_access_codes_info = unserialize($dataschool["key_access_codes_info"]);
        if (isset($dataschool["key_access_codes_desc"]) && !empty($dataschool["key_access_codes_desc"]))
            $key_access_codes_desc = unserialize($dataschool["key_access_codes_desc"]);

        $resultSchooldata = array_map(function ($name, $type) {
            return array_combine(
                    ['keyaccess', 'keycode'], [$name, $type]
            );
        }, $key_access_codes_info, $key_access_codes_desc);


        return $resultSchooldata;
    }

    public function managementFeeData($id) {
        $schoolsql = "SELECT * FROM management_fees WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();

        return $resmanagementdata;
    }

    public function getBankdetails($id) {
        $schoolsql = "SELECT * FROM property_bank_details WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetchAll();

//        dd($resmanagementdata);
        return $resmanagementdata;
    }

    public function getManagementInfo($id) {
        $schoolsql = "SELECT * FROM management_info WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();



        return $resmanagementdata;
    }

    public function getMaintenanceInformation($id) {
        $schoolsql = "SELECT * FROM maintenance_information WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();

        return $resmanagementdata;
    }

    public function getMortgageInformation($id) {
        $schoolsql = "SELECT * FROM mortgage_information WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();

        return $resmanagementdata;
    }

    public function getPropertyLoan($id) {
        $schoolsql = "SELECT * FROM property_loan WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();

        return $resmanagementdata;
    }

    public function getLateFeemanagement($id) {
        $schoolsql = "SELECT * FROM  late_fee_management WHERE property_id='" . $id . "' AND deleted_at IS NULL";
        $resmanagementdata = $this->companyConnection->query($schoolsql)->fetch();

        return $resmanagementdata;
    }

    public function getLinkOwnerData($id) {

        $linkowner = "SELECT * FROM  general_property WHERE id='" . $id . "' AND deleted_at IS NULL";
        $linkownerData = $this->companyConnection->query($linkowner)->fetch();
        $ownerDetail = [];
        $owner_id = unserialize($linkownerData['owner_id']);
        $owner_percentowned = unserialize($linkownerData['owner_percentowned']);
        $odd = array();
        $even = array();
        if(!empty($owner_percentowned)){
        foreach ($owner_percentowned as $k => $owner) {
            if ($k % 2 == 0) {
                $even[] = $owner;
            } else {
                $odd[] = $owner;
            }
            
        }       
        }
        $linkodss=array_combine($even,$odd);
        $evenidss= $even;
        return array('linkodss' =>$linkodss,'evenidss' =>$evenidss);
        
//        return array_combine($even,$odd);
    }
    public function getkeycheckout($id){
        $keytracker = "SELECT * FROM  key_tracker WHERE property_id='" . $id . "'";
        $reskeytracker = $this->companyConnection->query($keytracker)->fetch();  
        
        $keycheckout = "SELECT * FROM  key_checkout WHERE key_id='" . $reskeytracker['id'] . "'  and checkout_keys !=0";
        $reskeycheckout = $this->companyConnection->query($keycheckout)->fetchAll(); 
        return $reskeycheckout;
    }

}

$user = new propertyEdit();
