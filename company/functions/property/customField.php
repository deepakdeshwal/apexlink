<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class propertyCustomfield extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function addCustomFields(){
        try {
        
            if(isset($_POST['form'])){
                $data = $_POST['form'];
                //declaring additional variables
                $property_id = $_POST['property_id'];
                if(!empty($data)){
                    foreach ($data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $data[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $data[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['property_id'] = $property_id;
                $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $insert['custom_field'] = (!empty($data) && count($data)>0) ? serialize($data):null;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');
    
                //check if custom field allready exists or not
    
                //Save Data in Company Database
                $sqlData = createSqlColVal($insert);
                $custom_data = ['is_deletable' => '0','is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if(!empty($data) && count($data) > 0){
                    foreach ($data as $key=>$value){
                        updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                    }
                }
                $check = getSingleRecord($this->companyConnection, ['column'=>'property_id','value'=>$property_id], 'property_custom_fields');
    
                if($check['code'] == 400) {
                    $query = "INSERT INTO property_custom_fields (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($insert);
                    $msg = 'Record added successfully';
                } else {
    
                    $sqlData = createSqlColValPair($insert);
                    $query = "UPDATE property_custom_fields SET ".$sqlData['columnsValuesPair']." where property_id='$property_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $msg = 'Record updated successfully';
                }
                return array('code' => 200, 'status' => 'success', 'data' => $insert, 'message' => $msg);
            }
            return array('code' => 200, 'status' => 'success', 'data' => [], 'message' => 'Record added successfully');
            
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }

    /**
     * function to return all the custom fields.
     */
    public function get(){
        try {
            $id = $_POST['property_id'];
            //check if custom field allready exists or not
            $sql = "SELECT * FROM property_custom_fields WHERE property_id=".$id." AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data) && count($data) > 0){
                $custom_data = !empty($data['custom_field']) ? unserialize($data['custom_field']) : null;
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'custom_data'=>$custom_data,'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error','message' => 'No Record Found!');
            }
            return;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }
}

$propertyDetail = new propertyCustomfield();
?>