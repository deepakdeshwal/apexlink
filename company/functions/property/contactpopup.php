<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};
class contactPopup extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for add pet
     */
    public function addReferralSourcePopup() {
        try {
            $data = $_POST['form'];
            print_r($data); die;
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['referral'];
            //Max length variable array
            $maxlength_array = ['referral' => 150];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'referral_source', 'referral', $data['referral'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Record already Exist');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['update_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   referral_source (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchZipcode() {
        $zip = $_POST['zip'];
        $sql = "SELECT * FROM zip_code_master WHERE zip_code=" . $zip . "";
        $data = $this->companyConnection->query($sql)->fetch();
        return array('zip_code' => $data['zip_code'], 'city' => $data['city'], 'state' => $data['state'], 'country' => $data['country'], 'status' => 'success');
    }

//     public function addKeyPopup() {
//         try {
//             $data = $_POST['form'];
//             //Required variable array
//             $required_array = [];
//             //Max length variable array
//             $maxlength_array = [];
//             //Number variable array
//             $number_array = [];
//             //Server side validation
//             //Save Data in Company Database

//             $query = "INSERT INTO   company_key_access (`user_id`,`key_code`) VALUES (" . $_SESSION[SESSION_DOMAIN]['cuser_id'] . ",'" . $data . "')";
// //                echo $query;
//             $stmt = $this->companyConnection->prepare($query);
//             $stmt->execute();
//             $id = $this->companyConnection->lastInsertId();
//             return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
//         } catch (PDOException $e) {
//             echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
//             return;
//         }
//     }

//     public function addAmenityPopup() {
//         try {
//             $data = $_POST['form'];
//             $data = postArray($data, 'true');
//             //Required variable array
//             $required_array = ['amenity_code', 'name'];
//             //Max length variable array
//             $maxlength_array = ['code' => 30, 'name' => 30];
//             //Number variable array
//             $number_array = [];
//             //Server side validation
//             $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
//             if (checkValidationArray($err_array)) {
//                 return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
//             } else {
//                 //declaring additional variables
//                 $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
//                 //check if custom field allready exists or not
//                 $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_amenities', 'name', $data['name'], $id);
//                 if ($duplicate['is_exists'] == 1) {
//                     if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
//                         return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Name allready exists!');
//                     }
//                 }
//                 //Save Data in Company Database
//                 $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                 $data['type'] = "1,2,3";
//                 $data['created_at'] = date('Y-m-d H:i:s');
//                 $data['updated_at'] = date('Y-m-d H:i:s');
//                 $sqlData = createSqlColVal($data);
//                 $query = "INSERT INTO   company_property_amenities (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
//                 $stmt = $this->companyConnection->prepare($query);
//                 $stmt->execute($data);
//                 $id = $this->companyConnection->lastInsertId();
//                 return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'lastid' => $id, 'message' => 'Record added successfully');
//             }
//         } catch (PDOException $e) {
//             echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
//             return;
//         }
//     }

//     public function fetchPetdata() {
//         $html = '';
//         if (!isset($_POST['propertyEditid'])):
//             $sql = "SELECT * FROM company_pet_friendly";
//             $data = $this->companyConnection->query($sql)->fetchAll();
//             $html.= " <option value='' selected disabled hidden>Select</option>";
//             foreach ($data as $d) {
//                 $html.= '<option value=' . $d["id"] . '>' . $d['pet_friendly'] . '</option>';
//             }
//         else:
//             $propertyEditInfo = "SELECT * FROM general_property WHERE id=" . $_POST['propertyEditid'] . " AND deleted_at IS NULL";
//             $getpropertyEditInfo = $this->companyConnection->query($propertyEditInfo)->fetch();
//             $sql = "SELECT * FROM company_pet_friendly";
//             $data = $this->companyConnection->query($sql)->fetchAll();
//             $html.= " <option value='' selected disabled hidden>Select</option>";
//             foreach ($data as $d) {
//                 $html.= '<option value="' . $d['id'] . '" ' . (($getpropertyEditInfo['pet_friendly'] == $d['id']) ? "selected" : "") . '>' . $d['pet_friendly'] . '</option>';
//             }
//         endif;


//         return array('data' => $html, 'status' => 'success');
//     }

//     public function fetchkeydata() {
//         $html = '';
//         $sql = "SELECT * FROM company_key_access";
//         $data = $this->companyConnection->query($sql)->fetchAll();

//         foreach ($data as $d) {
//             $html.= '<option value=' . $d['id'] . '>' . $d['key_code'] . '</option>';
//         }


//         return array('data' => $html, 'status' => 'success');
//     }

//     public function fetchAllAmenities() {
//         $html = '';

//         $sql = "SELECT * FROM   company_property_amenities";
//         $data = $this->companyConnection->query($sql)->fetchAll();
//             $html .="<div id='selectAllamennity' class='commonCheckboxClass'><input type='checkbox' value='selectall'  class='all'>Select all</div>";
//             foreach ($data as $d) {
//                 $name = $d['name'];
//                 $idd = $d['id'];


//                 $html.= "<div class='commonCheckboxClass'><input class='inputAllamenity' type='checkbox' value=$name  id=$idd name='amenities'> $name </div>";
//             }




//         return array('data' => $html, 'status' => 'success');
//     }

}

$contactPopup = new contactPopup();
