<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:30 PM
 */

include("$_SERVER[DOCUMENT_ROOT]/config.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class ApexlinkNewAjax extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function whatsNewList()
    {
          try{
              //$query = $this->conn->query("SELECT * FROM `apexlink_list` ORDER BY ID DESC limit 0,10");
              $query = $this->conn->query("SELECT * FROM `apexlink_list` where deleted_at IS NULL and status != '0' ORDER BY ID DESC");
              $whatsnew = $query->fetchAll();
              //echo '<pre>';print_r($whatsnew);echo '</pre>';
              return ['code'=>200, 'status'=>'success', 'data'=>$whatsnew];
          }catch (Exception $exception)
          {
              echo $exception->getMessage();
              printErrorLog($exception->getMessage());
          }
    }

    public function apexnewfeedback(){
        try {
            $data=[];
            $data['user_id'] = $_POST['id'];
            $data['list_id'] = $_POST['list_id'];
            $data['feedback'] = $_POST['feedback_message'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $id = $data['user_id'];

                $query1 = $this->conn->query("SELECT * FROM users where id='$id'");
                $userRecord = $query1->fetch();

                $data['company_name'] = $userRecord['company_name'];
                $data['company_admin'] = $userRecord['name'];
                $user_email = $userRecord['email'];
                //echo  "<pre>";print_r($user_email);echo "</pre>";die;
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO apexlink_list_feedback (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                //$lastInsertId = $this->conn->lastInsertId();
                $this->sendMail($id);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Feedback has been Sent!');
            }
        }catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function sendMail($id) {
        try{
            $data = $_REQUEST;
            $request = [];
            $listdata = $this->conn->query("SELECT users.email FROM `apexlink_list` LEFT JOIN users ON apexlink_list.user_id = users.id WHERE apexlink_list.id = ".$_REQUEST['list_id'] );
            $listdata = $listdata->fetch();
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/whatsnew_feedback.php');
           // $body = getEmailTemplateData($this->companyConnection,'newCompanyUser_Key');
       //     dd($body);
            if(isset($data['data']))
            {
                foreach ($data['data'] as $k=>$v)
                {
                    if(isset($v['name']) && ($v['name'] == 'list_id'))
                    {
                        $request['id'] = $v['value'];
                        $query = $this->conn->query("SELECT * FROM `apexlink_list` WHERE id = ".$v['value']);
                        $whatsnew = $query->fetch();
                        $body = str_replace("#title#",$whatsnew['title'],$body);
                        $body = str_replace("#description#",$whatsnew['description'],$body);

                    } else {
                      //  $err_array['feedback_messageErr'] = "Please fill required fields";
                    }



                    if(isset($v['name']) && ($v['name'] == 'feedback_message'))
                    {
                        if($v['value'])
                        {
                            $body = str_replace("#feedback#",$v['value'],$body);
                        } else {
                            $err_array['feedback_message_err'] = "Please fill required fields";
                            return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
                        }
                    }
                }

            }
            $request['to[]'] = $listdata['email'];
            $request['action']  = 'SendMailPhp';
            $request['subject'] = 'What\'s New Feedback';
            $request['portal']  = '1';
            $request['message'] = $body;
            curlRequest($request);
            return ['code'=>200, 'status'=>'success', 'data'=>$request, 'message' => 'Mail is send successfully'];
        }catch (Exception $exception)
        {
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


}

$apexlinknew = new ApexlinkNewAjax();
