<?php/** * Created by PhpStorm. * User: ranavivek2567 * Date: 1/18/2019 * Time: 11:19 AM */if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {    $url = DOMAIN_URL;    header('Location: ' . $url);}?>
<?phpinclude_once(ROOT_URL . "/superadmin/views/layouts/admin_header.php");include_once(ROOT_URL . "/superadmin/views/settings/apexlink_new/modal.php");?>
<!-- HTML Start -->

<body>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php    include_once(ROOT_URL . "/superadmin/views/layouts/top_navigation.php");    ?>
    <!-- Top navigation end -->
    <!-- Content Start-->
    <section class="main-content super-admin">
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar start-->
                <?php                include_once(ROOT_URL . "/superadmin/views/layouts/sidebar.php");                ?>
                <!-- Sidebar end -->
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9"> Welcome: Sonny Kesseben (ACL Properties), June 01, 2018 </div>
                            <div class="col-xs-3"> <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a> </div>
                        </div>
                    </div>
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-md-8">
                                <div class="breadcrumb-outer"> Admin >> <span>Apexlink New</span> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text" /> </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!--Tabs Starts -->
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <div class="tab-content add_apexlink_new">
                                    <div role="" class="" id="">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <form method="post" id="addapexnewform">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">                                                                        <a>Apexlink New</a>                                                                    </h4> </div>
                                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="form-outer form_outer">
                                                                                <input type="hidden" name="id" value="">
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Title <em class="red-star">*</em></label>
                                                                                    <input class="form-control" placeholder="Title" name="title" maxlength="150" type="text" /> </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Date <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="date" type="text" placeholder="Date" /> </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Time <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="time" type="text" placeholder="Time" /> </div>
                                                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                                                    <label>Description (Max. 1000 Characters)</label>
                                                                                    <textarea placeholder="Description" class="form-control" name="description" rows="6" cols="65" maxlength="1000"></textarea> <span class="maiden_nameErr error"></span> </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer">
                                                                <button type="submit" class="blue-btn saveapexbtn">Save</button>
                                                                <button type="button" class="grey-btn cancelapexnew">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content apexlink_new_listing">
                                    <div role="tabpanel" class="tab-pane active" id="">
                                        <div class="accordion-form">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="title-color">Apexlink New</a>                                                                    </h4> </div>
                                                                <div class="col-sm-12 text-right newapexlinkbtn"> <a href="javascript:void(0)" class="blue-btn addapexlinknewbtn">Add What’s New in Apexlink</a> </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <!-- Data table start -->
                                                                                <table id="apexlink_new_table" class="table table-bordered"> </table>
                                                                                <!-- Data table end -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content apexlink_new_view list_hide">
                                    <div role="tabpanel" class="tab-pane active" id="">
                                        <div class="accordion-form">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="title-color">Apexlink New</a>                                                                        <a class="back right" href="/Setting/ApexLinkNew"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>                                                                    </h4> </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="grid-outer">
                                                                                <form id="edit_apexlink_new" method="post">
                                                                                    <div class="table-responsive">
                                                                                        <div class="form-outer">
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <input type="hidden" id="user_id_hidden" name="user_id_hidden">
                                                                                                <label>Title <em class="red-star">*</em></label>
                                                                                                <input disabled class="form-control" placeholder="Title" name="title" maxlength="150" type="text" /> </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Date <em class="red-star">*</em></label>
                                                                                                <input disabled class="form-control" name="date" type="text" placeholder="Date" /> </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Time <em class="red-star">*</em></label>
                                                                                                <input disabled class="form-control" name="time" type="text" placeholder="Time" /> </div>
                                                                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                                                                <label>Description (Max. 1000 words)</label>
                                                                                                <textarea disabled placeholder="Description" class="form-control" name="description" rows="6" cols="65" maxlength="1000"></textarea> <span class="maiden_nameErr error"></span> </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="collapseOne" class="feedback_div panel-collapse collapse  in">
                                                                                        <div class="panel-body">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <!-- Data table start -->
                                                                                                    <h2>Feedback(s)</h2>
                                                                                                    <table id="apexNew_feedback" class="table table-bordered">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th>Company Name</th>
                                                                                                            <th>Company Admin</th>
                                                                                                            <th>Feedback</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody> </tbody>
                                                                                                    </table>
                                                                                                    <!-- Data table end -->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-outer text-right apx-border-row edit_apex_new_btn"> <a href="javascript:void(0)" class="disable_remove_new apx-edt-btn" id="disable_remove"> <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/edit-icon.png"> Edit</a> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer apex-btn-block left hide_btn"> <a href="/Setting/ApexLinkNew" class="blue-btn updateForm">Update</a> <a href="/Setting/ApexLinkNew" class="grey-btn cancel_action">Cancel</a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Tabs Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content End-->
</div>
<?php include_once(ROOT_URL . "/superadmin/views/layouts/admin_footer.php"); ?>
<script type="text/javascript">
    var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
</script>
<script src="<?php echo SITE_URL; ?>/superadmin/js/validation/users/apexlinkNew.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>/superadmin/js/super_admin/apexlink_new.js" type="text/javascript"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/users/manageUser.js" type="text/javascript"></script>
<script>
    $('#leftnav3').addClass('in');
    $('.apexlink_new ').addClass('active');
</script>
<!--<script>    $(document).ready(function(){        //intializing jqGrid        jqGrid();        /**         * jqGrid Intialization function         * @param status         */        function jqGrid(status) {            var table = 'apexlink_list_feedback';            var columns = ['Company Name', 'Company Admin', 'Feedback'];            var select_column = [];            var joins = [];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];            var conditions = ["eq","bw","ew","cn","in"];            var extra_columns = ['apexlink_list_feedback.deleted_at'];            var columns_options = [                {name:'Company Name',index:'company_name', width:300,align:"center",searchoptions: {sopt: conditions},table:table},                {name:'Company Admin',index:'company_admin', width:300,align:"center",searchoptions: {sopt: conditions},table:table},                {name:'Feedback',index:'feedback', width:500, align:"center",searchoptions: {sopt: conditions},table:table},            ];            var ignore_array = [ ];            jQuery("#apexNew_feedback_table").jqGrid({                url: '/Companies/List/jqgrid',                datatype: "json",                height: '100%',                autowidth: true,                colNames: columns,                colModel: columns_options,                pager: true,                mtype: "POST",                postData: {                    q: 1,                    class: 'jqGrid',                    action: "listing_ajax",                    table: table,                    select: select_column,                    columns_options: columns_options,                    /*status: status,*/                    ignore:ignore_array,                    joins:joins,                    extra_columns:extra_columns                },                viewrecords: true,                sortname: 'updated_at',                sortorder: "desc",                sorttype:'date',                sortIconsBeforeText: true,                headertitles: true,                rowNum: 10,                rowList: [5, 10, 20, 30, 50, 100, 200],                caption: "Feedback",                pginput: true,                pgbuttons: true,                navOptions: {                    edit: false,                    add: false,                    del: false,                    search: true,                    filterable: true,                    refreshtext: "Refresh",                    reloadGridOptions: {fromServer: true}                },                // onSelectRow: function (id) {                //     console.log('res>>>>', id);                //     window.location.href = '/Plans/ViewPlan/'+id;                // },            }).jqGrid("navGrid",                {                    edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}                },                {}, // edit options                {}, // add options                {}, //del options                {top:100,left:200,drag:false,resize:false} // search options            );        }    });</script>-->
</body>
<!-- HTML END -->