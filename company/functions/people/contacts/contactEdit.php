<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/functions/FlashMessage.php");
include_once( ROOT_URL."/company/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class ContactEdit extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * function to add custom field.
     * @return array|void
     */
    public function view(){
        try {
            $renovation_detail_html="";
            $data = [];
            $custom_data=[];
            $id= $_GET["id"];
            $sql = "SELECT u.salutation,u.veteran_status,u.first_name,u.last_name,u.middle_name,u.maiden_name,u.nick_name,u.gender,u.hobbies,u.dob,e.phone_number_note,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.country,u.zipcode,u.maritial_status, e.employee_image,e.employee_license_state,e.employee_license_number,e.current_company,e.previous_company,e.custom_field FROM users as u
 LEFT JOIN employee_details as e ON u.id=e.user_id   
 WHERE u.id=".$id;
            $stmt = $this->companyConnection->prepare($sql); // Prepare the statement
            $data = [];
            $custom_data=[];
            $emergency_detail= '';
            if($stmt->execute()) {
                $data = $stmt->fetch();
                $stmt->closeCursor();
                if(!empty($data)){


                    if(!empty($data["custom_field"])){
                        $custom_data = unserialize($data["custom_field"]);
                    }



                    if(isset($data["hobbies"]) && !empty($data["hobbies"])) {
                        $newhobbies='';
                        $hobbies = unserialize($data["hobbies"]);
                        if(!empty($hobbies)){
                            foreach ($hobbies as $key=>$value){
                                $query = 'SELECT hobby FROM hobbies WHERE id='.$value;
                                $groupData = $this->companyConnection->query($query)->fetch();
                                $name = $groupData['hobby'];
                                $newhobbies .= $name.', ';
                            }
                            $data["hobbies"] = rtrim($newhobbies, ', ');;
                            //$data["saved_amenities"] = $amenities;
                        }else{
                            $data["hobbies"] = "N/A";
                            //$data["saved_amenities"] = [];
                        }
                    }else{

                        $data["hobbies"] = "N/A";
                      //  $data["saved_amenities"] = [];
                    }


                    if(isset($data["maritial_status"]) && !empty($data["maritial_status"])) {
                                $query = 'SELECT marital FROM tenant_marital_status WHERE id='.$data["maritial_status"];
                                $maritalData = $this->companyConnection->query($query)->fetch();
                                $name = $maritalData['marital'];
                                $data["maritial_status"] = $name;

                    }
                    if(isset($data["veteran_status"]) && !empty($data["veteran_status"])) {
                        $query = 'SELECT veteran FROM tenant_veteran_status WHERE id='.$data["veteran_status"];
                        $veteranData = $this->companyConnection->query($query)->fetch();
                        $name = $veteranData['veteran'];
                        $data["veteran_status"] = $name;

                    }
                    if(!empty($data["dob"])){
                        $data["dob"] =   dateFormatUser($data["dob"],null,$this->companyConnection);
                    }



                    $sql1 = "SELECT * FROM emergency_details WHERE user_id=".$id ;
                    $stmt1 = $this->companyConnection->prepare($sql1); // Prepare the statement
                    if($stmt1->execute())
                    {
                        $emergency_detail =$stmt1->fetchAll();
                    }

                    $sql2 = "SELECT * FROM tenant_credential WHERE user_id=".$id ;
                    $stmt2 = $this->companyConnection->prepare($sql2); // Prepare the statement
                    if($stmt2->execute())
                    {
                        $credential_control =$stmt2->fetchAll();
                    }

                    $data =getUnserializedData($data,'true');
                    $salutation_id = $data["salutation"];
                    $satulation_value = $this->getSatulationValue($salutation_id);
                    if(!empty($satulation_value)){
                        $data["salutation"] = $satulation_value;
                    }else{
                        $data["salutation"] = "N/A";
                    }

                    $gender_id = $data["gender"];
                    $gender_value = $this->getGenderValue($gender_id);
                    if(!empty($gender_value)){
                        $data["gender"] = $gender_value;
                    }else{
                        $data["gender"] = "N/A";
                    }
                    //dd($data);
                    $redirect = VIEW_COMPANY."company/contacts/view.php";
                    return require $redirect;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }


    /**
     * function to edit employee
     * @return array|void
     */
    public function edit(){
        try {
            $renovation_detail_html="";
            $data = [];
            $custom_data=[];
            $id= $_GET["id"];
            $sql = "SELECT tp.building_id,tp.property_id,tp.unit_id,u.salutation,u.updated_at,u.veteran_status,u.email,u.ethnicity,u.referral_source,u.ssn_sin_id,u.first_name,u.last_name,u.middle_name,u.maiden_name,u.name,u.phone_number,u.country_code,u.nick_name,u.gender,u.hobbies,u.dob,u.phone_number_note,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.country,u.zipcode,u.maritial_status, e.employee_image,e.employee_license_state,e.employee_license_number,e.current_company,e.previous_company,e.custom_field,e.email1,e.email2,e.email3 FROM users as u
 LEFT JOIN employee_details as e ON u.id=e.user_id 
 LEFT JOIN tenant_property as tp ON u.id=tp.user_id   
 WHERE u.id=".$id;
            $stmt = $this->companyConnection->prepare($sql); // Prepare the statement
            $data = [];
            $custom_data=[];
            $email_array=[];
            $emergency_detail= '';
            if($stmt->execute()) {
                $data = $stmt->fetch();
                $stmt->closeCursor();
                if(!empty($data)){
                    if(!empty($data["custom_field"])){
                        $custom_data = unserialize($data["custom_field"]);
                    }

                    if(isset($data["hobbies"]) && !empty($data["hobbies"])) {
                        $newhobbies='';
                        $hobbies = unserialize($data["hobbies"]);
                        $data["hobbies"] = serialize($hobbies);
                    }
                    if(!empty($data["ssn_sin_id"])){
                        $data["ssn_sin_id"] =  unserialize($data["ssn_sin_id"]);
                    }
                    $email_array = [$data['email1'],$data['email2'],$data['email3']];
                    $email_array = array_values(array_filter($email_array));
                    $sql1 = "SELECT * FROM emergency_details WHERE user_id=".$id ;
                    $stmt1 = $this->companyConnection->prepare($sql1); // Prepare the statement
                    $country_codes = [];
                    if($stmt1->execute())
                    {
                        $emergency_detail =$stmt1->fetchAll();
                        if(!empty($emergency_detail)){
                            foreach($emergency_detail as $key => $value){
                                $country_codes[$key] = $value['emergency_country_code'];
                            }
                        }
                    }
                    $country_codes = serialize($country_codes);
                    $sql2 = "SELECT * FROM tenant_credential WHERE user_id=".$id ;
                    $stmt2 = $this->companyConnection->prepare($sql2); // Prepare the statement
                    $acquire_dates = [];
                    $expire_dates = [];
                    $credential_type = [];

                    if($stmt2->execute())
                    {
                        $credential_control =$stmt2->fetchAll();
                        if(!empty($credential_control)){
                            foreach($credential_control as $key => $value){
                                $adt = new DateTime($value['acquire_date']);
                                $acquire_date = $adt->format('Y-m-d');
                                $edt = new DateTime($value['expire_date']);
                                $expire_date = $edt->format('Y-m-d');

                                $acquire_dates[$key] = $acquire_date;
                                $expire_dates[$key] = $expire_date;
                                $credential_type[$key]= $value['credential_type'];
                            }



                        }
                    }
                    $sql3 = "SELECT * FROM tenant_chargenote WHERE user_id=".$id ;
                    $stmt3 = $this->companyConnection->prepare($sql3); // Prepare the statement
                    if($stmt3->execute())
                    {
                        $charge_notes =$stmt3->fetchAll();
                    }
                    $carrier = [];
                    $phone_country_codes=[];
                    $sql4 = "SELECT * FROM tenant_phone WHERE user_id=".$id ;
                    $stmt4 = $this->companyConnection->prepare($sql4); // Prepare the statement
                    if($stmt4->execute())
                    {
                        $tenant_phone=$stmt4->fetchAll();
                        if(!empty($tenant_phone)){
                            foreach($tenant_phone as $key => $value){
                                $carrier[$key] = $value['carrier'];
                                $phone_country_codes[$key] = $value['country_code'];
                            }

                        }
                    }

                    $carrier = serialize($carrier);
                    $phone_country_codes = serialize($phone_country_codes);
                    $current_date = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
                    $redirect = VIEW_COMPANY."company/contacts/edit.php";
                    return require $redirect;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /*
 * function to get salutation
 */
    public function getSatulationValue($salutation){
        $salutation_value = '';
        if($salutation == "1"){
            $salutation_value = "Dr.";
        }else if($salutation == "2"){
            $salutation_value = "Mr.";
        }else if($salutation == "3"){
            $salutation_value = "Mrs.";
        }else if($salutation == "4"){
            $salutation_value = "Mr. & Mrs.";
        }else if($salutation == "5"){
            $salutation_value = "Ms.";
        }else if($salutation == "6"){
            $salutation_value = "Sir";
        }else if($salutation == "7"){
            $salutation_value = "Madam";
        }
        return  $salutation_value;
    }

    /*
* function to get gender
*/
    public function getGenderValue($gender){
        $gender_value = '';
        if($gender == "1"){
            $gender_value = "Male";
        }else if($gender == "2"){
            $gender_value = "Female";
        }else if($gender == "3"){
            $gender_value = "Prefer Not To Say";
        }else if($gender == "4"){
            $gender_value = "Other";
        }
        return  $gender_value;
    }


    /**
     * function to add new renovation
     *
     *
     */

    public function  addNewVehicle(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"false");
            $employee_id = $_POST["employee_id"];
            //dd($employee_id);
            $vehicle_id = (isset($data["vehicle_id"]) && !empty($data["vehicle_id"]))? $data["vehicle_id"] : '' ;
            if(isset($employee_id) && !empty($employee_id) && empty($vehicle_id)) {
                $vehicle_data = [];
                $vehicle_data["vehicle_name"] = (isset($data['vehicle_name']) && !empty($data['vehicle_name'])) ? $data['vehicle_name'] : '';
                $vehicle_data["vehicle_number"] = (isset($data['vehicle_number']) && !empty($data['vehicle_number'])) ? $data['vehicle_number'] : '';
                $vehicle_data['user_id'] = $employee_id;
                $vehicle_data["model"] = (isset($data['vehicle_model']) && !empty($data['vehicle_model'])) ? $data['vehicle_model'] : '';
                $vehicle_data["date_purchased"] = (isset($data['vehicle_date_purchased']) && !empty($data['vehicle_date_purchased'])) ? mySqlDateFormat($data['vehicle_date_purchased'], null, $this->companyConnection) : NULL;
                $vehicle_data["type"] = (isset($data['vehicle_type']) && !empty($data['vehicle_type'])) ? $data['vehicle_type'] : NULL;
                $vehicle_data["make"] = (isset($data['vehicle_make']) && !empty($data['vehicle_make'])) ? $data['vehicle_make'] : '';
                $vehicle_data["license"] = (isset($data['vehicle_plate_number']) && !empty($data['vehicle_plate_number'])) ? $data['vehicle_plate_number'] : '';
                $vehicle_data['color'] = (isset($data['vehicle_color']) && !empty($data['vehicle_color'])) ? $data['vehicle_color'] : '';
                $vehicle_data['year'] = (isset($data['vehicle_year']) && !empty($data['vehicle_year'])) ? $data['vehicle_year'] : '';
                $vehicle_data['vin'] = (isset($data['vehicle_vin']) && !empty($data['vehicle_vin'])) ? $data['vehicle_vin'] : '';
                $vehicle_data['registration'] = (isset($data['vehicle_registration']) && !empty($data['vehicle_registration'])) ? $data['vehicle_registration'] : '';
                $vehicle_data["created_at"] = date('Y-m-d H:i:s');
                $vehicle_data["updated_at"] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($vehicle_data);
                //Save Data in Company Database
                $query = "INSERT INTO  tenant_vehicles (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($vehicle_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    echo json_encode( array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully'));
                } else {
                    echo json_encode( array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully'));
                }
            } else{
                $vehicle_data = [];
                $vehicle_data["vehicle_name"] = (isset($data['vehicle_name']) && !empty($data['vehicle_name'])) ? $data['vehicle_name'] : '';
                $vehicle_data["vehicle_number"] = (isset($data['vehicle_number']) && !empty($data['vehicle_number'])) ? $data['vehicle_number'] : '';
                $vehicle_data['user_id'] = $employee_id;
                $vehicle_data["model"] = (isset($data['vehicle_model']) && !empty($data['vehicle_model'])) ? $data['vehicle_model'] : '';
                $vehicle_data["date_purchased"] = (isset($data['vehicle_date_purchased']) && !empty($data['vehicle_date_purchased'])) ? mySqlDateFormat($data['vehicle_date_purchased'], null, $this->companyConnection) : NULL;
                $vehicle_data["type"] = (isset($data['vehicle_type']) && !empty($data['vehicle_type'])) ? $data['vehicle_type'] : NULL;
                $vehicle_data["make"] = (isset($data['vehicle_make']) && !empty($data['vehicle_make'])) ? $data['vehicle_make'] : '';
                $vehicle_data["license"] = (isset($data['vehicle_plate_number']) && !empty($data['vehicle_plate_number'])) ? $data['vehicle_plate_number'] : '';
                $vehicle_data['color'] = (isset($data['vehicle_color']) && !empty($data['vehicle_color'])) ? $data['vehicle_color'] : '';
                $vehicle_data['year'] = (isset($data['vehicle_year']) && !empty($data['vehicle_year'])) ? $data['vehicle_year'] : '';
                $vehicle_data['vin'] = (isset($data['vehicle_vin']) && !empty($data['vehicle_vin'])) ? $data['vehicle_vin'] : '';
                $vehicle_data['registration'] = (isset($data['vehicle_registration']) && !empty($data['vehicle_registration'])) ? $data['vehicle_registration'] : '';
                $vehicle_data["updated_at"] = date('Y-m-d H:i:s');
                $sqlData = createSqlUpdateCase($vehicle_data);
                $query = "UPDATE tenant_vehicles SET " . $sqlData['columnsValuesPair'] . " where id='$vehicle_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                if ( $stmt->execute()) {
                    echo json_encode( array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully'));
                } else {
                    echo json_encode( array('code' => 400, 'status' => 'error', 'message' => 'Record Not Updated successfully'));
                }

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }

    }


    public function getVehicleDetail() {
        try {
            $id = $_POST['vehicle_id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'tenant_vehicles');
          if(!empty($record['data']['date_purchased'])){
              $record['data']['date_purchased'] =   dateFormatUser($record['data']['date_purchased'],null,$this->companyConnection);
          }
            echo json_encode(['status' => 'success', 'code' => 200,'data'=>$record, 'message' => 'Record deleted successfully.']);
            return ;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }


    public function addVehicleInfo($employee_id)
    {

        try{
            $vehicleType = $_POST['vehicle_type'];
            $i = 0;
            foreach($vehicleType as $vehicleType)
            {
                $data['vehicle_name'] = $_POST['vehicle_name'][$i];
                $data['vehicle_number'] = $_POST['vehicle_number'][$i];
                $data['model'] = $_POST['vehicle_model'][$i];
                $data['user_id'] = $employee_id;
                $data['type'] = $vehicleType;
                $data['make'] = $_POST['vehicle_make'][$i];
                $data['license'] = $_POST['vehicle_plate_number'][$i];
                $data['color'] = $_POST['vehicle_color'][$i];
                $data['year'] = $_POST['vehicle_year'][$i];
                $data['vin'] = $_POST['vehicle_vin'][$i];
                $data['registration'] = $_POST['vehicle_registration'][$i];
                $data['date_purchased'] = empty($_POST['vehicle_date_purchased'][$i])?NULL:mySqlDateFormat($_POST['vehicle_date_purchased'][$i],null,$this->companyConnection);
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_vehicles(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_vechcle');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_vechile');
            printErrorLog($e->getMessage());
        }

    }

    /**
     *  Delete Vehicle detail
     */
    public function deleteVehicleDetail(){

        try{
            $vehicle_id= $_REQUEST['vehicle_id'];
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $vehicle_id], 'tenant_vehicles');
            if(!empty($data["data"])){
                $count=$this->companyConnection->prepare("DELETE FROM tenant_vehicles WHERE id=:id");
                $count->bindParam(":id",$vehicle_id,PDO::PARAM_INT);
                $count->execute();
                echo json_encode( ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.']);
            }else{
                echo json_encode( ['status'=>'failed','code'=>503,'message'=>'Record not deleted successfully']);
            }

        }catch (Exception $exception)
        {
            echo json_encode( ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()]);
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Insert data to company amenities table
     * @return array
     */
    public function addLibraryFiles()
    {

        $files = $_FILES;
        unset($files['file_library']);
        $employee_id = $_POST['employee_id'];


        if (!empty($files)) {
            $addFiles = $this->addFiles($employee_id, $files);
            if ($addFiles['status'] != 'success') {
                //   $this->deleteUnsaveRecord('users', $employee_id);
                echo json_encode( array('code' => 400, 'status' => 'error','message'=>$addFiles['message'], 'function' => 'addFiles', 'employe_id' => $employee_id));
            }else{
                echo json_encode(array('status' => 'success', 'message' => 'Files Added successfully.', 'table' => 'all', 'employee_id' => $employee_id));
            }
        }else{
            echo json_encode(array('status' => 'success', 'message' => 'Data not updated successfully.', 'table' => 'all', 'employee_id' => $employee_id));
        }



    }

    public function addFiles($employee_id,$files){
        try{
            if(!empty($files)) {

                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');

                foreach ($files as $key => $value) {

                    $file_name = $value['name'];
                    $fileData = getSingleWithAndConditionRecord($this->companyConnection ,['column'=>'filename','value'=>$file_name], 'tenant_chargefiles',['column'=>'user_id','value'=>$employee_id]);
                    if($fileData['code'] == '200'){
                        return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['user_id'] = $employee_id;
                    $data['filename'] = $file_name;
                    // $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['file_location'] = $path . '/' . $name . '.' . $ext;
                    $data['file_extension'] = $ext;
                    //$data['codec'] = $value['type'];
                    if(strstr($value['type'], "video/")){
                        $type = 3;

                    }else if(strstr($value['type'], "image/")){
                        $type = 1;
                    }else{
                        $type = 2;
                    }
                    $data['file_type'] = $type;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargefiles (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_chargefiles');
            }

        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_vechile');
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Delete Building
     */
    public function deleteLibraryFiles(){
        try{
            $file_id= $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $file_id], 'tenant_chargefiles');
            if(!empty($data["data"])){
                $count=$this->companyConnection->prepare("DELETE FROM tenant_chargefiles WHERE id=:id");
                $count->bindParam(":id",$file_id,PDO::PARAM_INT);
                $count->execute();
                echo json_encode( ['status'=>'success','code'=>200,'message'=>'File deleted successfully.']);
            }else{
                echo json_encode(['status'=>'failed','code'=>503,'message'=>'File not deleted successfully']);
            }

        }catch (Exception $exception)
        {
            echo json_encode( ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()]);
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Insert data to company amenities table
     * @return array
     */
    public function update(){
        $files = $_FILES;
        $contact_email = $_POST['additional_email'];
        unset($files['employee_image']);
        unset($files['file_library']);
        $data = $_POST;
        $employee_id = $data['employee_id'];
        $img =  json_decode($_POST['employee_image']);
        $domain = getDomain();
        $table="users";
        $employee_email = $_POST['additional_email'];

        $columns = [$table . '.first_name', $table . '.last_name', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4', $table . '.middle_name', $table . '.dob', $table . '.ssn_sin_id'];

        if (isset($_POST['date_of_birth']) && $_POST['date_of_birth'] != "") {
            $data['dob'] = mySqlDateFormat($_POST['date_of_birth'],null,$this->companyConnection);
        }else{
            $data['dob'] ='';
        }
        $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data["firstname"]],['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["lastname"]],['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middlename"]],['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $data["address1"]],['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $data["address2"]],['table' => 'users', 'column' => 'address3', 'condition' => '=', 'value' => $data["address3"]],['table' => 'users', 'column' => 'address4', 'condition' => '=', 'value' => $data["address4"]],['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $_POST['additional_phone'][0]],['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data['dob']]];
//        $duplicate_record = checkDuplicateRecord($this->companyConnection, 'users',$columns,$where,$data['ssn'], $employee_id);
//        if ($duplicate_record['is_exists'] == 1) {
//            echo json_encode( array('code' => 400, 'status' => 'error', 'message' => 'Record already exists!'));
//            die;
//        }
//        $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'email', $contact_email[0], $employee_id);
//        if ($duplicate['is_exists'] == 1) {
//            echo json_encode(array('code' => 400, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!'));
//            die;
//        }
        $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, $_POST['additional_email'][0],$_POST['additional_phone'][0],$_POST['ssn'],'','','',$employee_id);
        if(isset($duplicate_record_check)){
            if($duplicate_record_check['code'] == 503){
                echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                die();
            }
        }
        $addEmployee = $this->updateEmployee();//save employee in users table

        $updatePrimaryEmployee = $this->updatePrimaryEmployeeinfo($employee_id); //tenant_details table
        if($updatePrimaryEmployee['status']=='success'){
            $delete_records = $this->deleteRecords('tenant_phone',$employee_id);
            $addPhoneInfo = $this->addPhoneInfo($employee_id); //tenant_phone table
            if($addPhoneInfo['status']='success'){
                if( (isset($_POST['emergency_contact_name'] ) && $_POST['emergency_contact_name'][0] != "")|| (isset($_POST['emergency_relation'] ) && $_POST['emergency_relation'][0] != "")||(isset($_POST['emergency_country'] ) && $_POST['emergency_country'][0] != "")|| (isset($_POST['emergency_phone'] ) && $_POST['emergency_phone'][0] != "") || (isset($_POST['emergency_email'] ) && $_POST['emergency_email'][0] != "")){
                    $delete_records = $this->deleteRecords('emergency_details',$employee_id);
                    $returnTenantEmergency = $this->addEmergencyContact($employee_id);
                    if($returnTenantEmergency['status'] != 'success'){
                        //   $this->deleteUnsaveRecord('users', $employee_id);
                        echo json_encode( array('code' => 400,  'status' => 'error','function'=>'addEmergencyContact','tenant_id'=>$employee_id));
                    }
                }
                $delete_tenant_credential_records = $this->deleteRecords('tenant_credential',$employee_id);
                $addCredentialControls =  $this->addCredentialsControl($employee_id);
                if($addCredentialControls['status'] != 'success'){
                    //   $this->deleteUnsaveRecord('users', $employee_id);
                    echo json_encode( array('code' => 400,  'status' => 'error','function'=>'addCredentialsControl','employe_id'=>$employee_id));
                }
                $delete_ttenant_chargenote = $this->deleteRecords('tenant_chargenote',$employee_id);
                $addEmployeeNotes =  $this->addEmployeeNotes($employee_id);

                $this->saveupdateContactPropertyData($employee_id,$data);
                if($addEmployeeNotes['status'] != 'success'){
                    //    $this->deleteUnsaveRecord('users', $employee_id);
                    echo json_encode( array('code' => 400,  'status' => 'error','function'=>'addEmployeeNotes','employe_id'=>$employee_id));
                }
                updateUsername($employee_id,$this->companyConnection);
             echo json_encode(array('status' => 'success',  'message' => 'Data updated successfully.','table'=>'all','employee_id'=>$employee_id));
            }else{
                //$this->deleteUnsaveRecord('users', $employee_id);
                echo json_encode( array('code' => 400,  'status' => 'error','table'=>'employee_details','employe_id'=>$employee_id));
            }

        }else{
            // $this->deleteUnsaveRecord('users', $employee_id);
            echo json_encode( array('code' => 400,  'status' => 'error','function'=>'addPrimaryTenantinfo','employe_id'=>$employee_id));
        }
    }

        public function updateEmployee()
    {
        try{
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $tenant_email = $_POST['additional_email'];
            $tenant_phone_number = $_POST['additional_phone'];
            $data['name'] = $_POST['firstname'].' '. $_POST['lastname'];
            $data['email'] = $tenant_email[0];
            $data['phone_number']=  $tenant_phone_number[0];
            $data['phone_type']             = (isset($_POST['additional_phoneType']))? $_POST['additional_phoneType'][0] : '';
            $data['carrier']                = (isset($_POST['additional_carrier']))? $_POST['additional_carrier'][0] : '';
            if ( $data['phone_type'] == 5 ||  $data['phone_type']  == 2){
                $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension']))? $_POST['other_work_phone_extension'][0] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }
            $data['salutation'] = $_POST['salutation'];
            $data['zipcode'] = $_POST['zip_code'];
            $data['city'] = $_POST['city'];
            $data['state'] = $_POST['state'];
            $data['country'] = $_POST['country'];
            $data['first_name'] = $_POST['firstname'];
            $data['last_name'] = $_POST['lastname'];
            $data['middle_name'] = $_POST['middlename'];
            $data['address1'] = $_POST['address1'];
            $data['address2'] = $_POST['address2'];
            $data['address3'] = $_POST['address3'];
            $data['address4'] = $_POST['address4'];
            if (isset($_POST['date_of_birth']) && $_POST['date_of_birth'] != "") {
                $data['dob'] = mySqlDateFormat($_POST['date_of_birth'],null,$this->companyConnection);
            }
            if (isset($_POST['gender']) && $_POST['gender'] != "") {
                $data['gender'] = $_POST['gender'];
            }else{
                $data['gender'] = '0';
            }
            $note = $_POST['add_note_phone_number'];
            $data['nick_name'] = $_POST['nickname'];
            $data['phone_number_note'] = $note;
            $data['maiden_name'] = $_POST['maidenname'];
            $data['status'] = '1';
            $data['user_type'] = '5';
            $data['domain_name'] = $subdomain;
            if (isset($_POST['hobbies'])) {
                $data['hobbies'] = serialize($_POST['hobbies']);
            }
            $data['maritial_status'] = $_POST['maritalStatus'];
            $data['veteran_status'] = $_POST['veteranStatus'];
            $data['ethnicity'] = $_POST['ethncity'];
            $data['status'] = '1';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['record_status'] = '1';
            foreach($_POST['ssn'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn'][$key]);
            }
            $data['ssn_sin_id'] = !empty($_POST['ssn'])? serialize($_POST['ssn']):NULL;
            $referralSource = empty($_POST['additional_referralSource'])? NULL:$_POST['additional_referralSource'];
            $data['referral_source'] = $referralSource;
            //$sqlData = createSqlColValPair($data);
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='".$_POST["employee_id"]."'";
            $stmt = $this->companyConnection->prepare($query);
            $contactData = $data;
            $contactData['id'] = $_POST["employee_id"];
            $ElasticSearchSave = insertDocument('CONTACT','UPDATE',$contactData,$this->companyConnection);
            if($stmt->execute($sqlData['data'])) {
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

            } else {
              return array('code' => 400, 'status' => 'error', 'message' => 'Internal Server Error!');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'users');
            printErrorLog($e->getMessage());
        }


    }


    /*to add tenant primary information*/
    public function updatePrimaryEmployeeinfo($user_id)
    {
        $custom_field =  json_decode($_POST['custom_field']);
        $custom_fields = array();
        foreach ($custom_field as $key => $value) {
            # code...
            $customData = (array)$value;

            $custom_fields[] = $customData;
        }

        if(!empty($custom_fields)){
            foreach ($custom_fields as $key=>$value){
                if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                continue;
            }
        }

        $custom_data = ['is_deletable' => '0','is_editable' => '0'];
        $updateColumn = createSqlColValPair($custom_data);
        if(count($custom_fields)> 0){
            foreach ($custom_fields as $key=>$value){
                updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
            }
        }

        // $insert['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
        try{
            $tenant_email = $_POST['additional_email'];
            $emailCount =  count($tenant_email);
            if($emailCount==3)
            {
                $email1 = $tenant_email[0];
                $email2 = $tenant_email[1];
                $email3 = $tenant_email[2];
            }
            else if($emailCount==2)
            {
                $email1 = $tenant_email[0];
                $email2 = $tenant_email[1];
                $email3 = "";
            }
            else
            {
                $email1 = $tenant_email[0];
                $email2 = "";
                $email3 = "";
            }

         //   $note = $_POST['add_note_phone_number'];

//            $driverProvince = $_POST['driving_license_state'];
//            $driverLicense = $_POST['driving_license'];
//            $vehicle = $_POST['vehicle'];
            $data=[];
            $data['user_id'] = $user_id;
            $data['employee_image'] = json_decode($_POST['employee_image']);
         //   $data['phone_number_note'] = $note;
            $data['email1'] = $email1;
            $data['email2'] = $email2;
            $data['email3'] = $email3;
//            $data['current_company']= $_POST['current_company'];
//            $data['previous_company'] = $_POST['previous_company'];

//            $data['employee_license_state'] = $driverProvince;
//            $data['employee_license_number'] = $driverLicense;
//            $data['vehicle'] = $vehicle;
            $data['status'] = '1';
            $data['custom_field'] =  (count($custom_fields)>0) ? serialize($custom_fields):null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE employee_details SET ".$sqlData['columnsValuesPair']." where user_id='".$_POST["employee_id"]."'";
            $stmt = $this->companyConnection->prepare($query);
            if($stmt->execute()) {
                return array('status' => 'success', 'message' => 'Data Updated successfully.');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }


    }


        public function saveupdateContactPropertyData($id,$data)
    {
        try{
            $data1['user_id'] = $id;
            $data1['record_status'] = 0;
            $data1['contact'] = $id;
            $data1['property_id'] = (!empty($data['property']) && $data['property'] != 'Select')? $data['property']:null;
            $data1['building_id'] = (!empty($data['building']) && $data['building'] != 'Select')? $data['building']:null;
            $data1['unit_id'] = (!empty($data['unit']) && $data['unit'] != 'Select')? $data['unit']:null;
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data1);
            $query = "UPDATE tenant_property SET " . $sqlData['columnsValuesPair'] ." where user_id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData["data"]);

            return array('code' => 200, 'status' => 'success','message' => "Data");
        }
        catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => "something Went wrong on tenant property");
            printErrorLog($e->getMessage());
        }

    }


    /**
     *  Delete Vehicle detail
     */
    public function deleteRecords($table,$employee_id){

        try{

                $count=$this->companyConnection->prepare("DELETE FROM $table WHERE user_id=:id");
                $count->bindParam(":id",$employee_id,PDO::PARAM_INT);
                $count->execute();
                return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];


        }catch (Exception $exception)
        {
            echo json_encode( ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()]);
            printErrorLog($exception->getMessage());
        }
    }


    public function addPhoneInfo($employee_id)
    {
        $phoneType = $_POST['additional_phoneType'];
        $i=0;
        foreach($phoneType as $phone)
        {
            $data['user_id'] = $employee_id;
            $data['parent_id'] = 0;
            $data['phone_type'] = empty($phone)?'0':$phone;
            $data['carrier'] = empty($_POST['additional_carrier'][$i])?'0':$_POST['additional_carrier'][$i];
            $data['phone_number'] = $_POST['additional_phone'][$i];
            $data['country_code'] = $_POST['additional_countryCode'][$i];
            $data['user_type'] = 0; //0 value for main tenant
//            if($phone == 2 || $phone == 5) {
//                $data['work_phone_extension'] = @$_POST['work_phone_extension'][$i];
//                $data['other_work_phone_extension'] = @$_POST['other_work_phone_extension'][$i];
//            }else{
//                $data['work_phone_extension'] = NULL;
//                $data['other_work_phone_extension'] = NULL;
//            }
            if ( $_POST['additional_phoneType'][$i] == 5 ||  $_POST['additional_phoneType'][$i] == 2){
                $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension'][$i]))? $_POST['other_work_phone_extension'][$i] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $i++;
        }
        return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_phone');
    }



    public function addEmergencyContact($employee_id)
    {
        try{

            $i=0;
            foreach($_POST['emergency_contact_name'] as $credentialName)
            {

                $data['user_id'] = $employee_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $id = $this->companyConnection->lastInsertId();
                $i++;

            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'emergency_details');
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }



    }



    public function addCredentialsControl($employee_id)
    {
        try{
            $i=0;
            foreach($_POST['credentialName'] as $credentialName)
            {
                $acquire_date = $_POST['acquireDate'][$i];
                $expireDate = $_POST['expirationDate'][$i];
                $data['user_id'] = $employee_id;
                $data['credential_name'] = $_POST['credentialName'][$i];
                $data['credential_type'] = !empty($_POST['credentialType'][$i])?$_POST['credentialType'][$i]:NULL;
                $data['acquire_date'] = !empty($acquire_date)?mySqlDateFormat($acquire_date,null,$this->companyConnection):NULL;
                $data['expire_date'] = !empty($expireDate)?mySqlDateFormat($expireDate,null,$this->companyConnection):NULL;
                $data['notice_period'] = $_POST['noticePeriod'][$i];
                /*  return  $data; */

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt1 = $this->companyConnection->prepare($query);
                $stmt1->execute($data);

                $i++;
            }


            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_credentials');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }

    public function addEmployeeNotes($employee_id)
    {
        try{
            if(isset($_POST['employee_notes'])){
                $chargeNote = $_POST['employee_notes'];
                $i = 0;
                foreach($chargeNote as $note){
                    if($note!=""){
                        $data['user_id'] = $employee_id;
                        $data['note'] = $note;
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                    $i++;
                }
            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_chargenote');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }





}

$contact = new ContactEdit();
