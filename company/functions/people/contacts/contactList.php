<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class ContactListAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $user_columns = "`salutation`, `first_name`, `middle_name`, `last_name`, `company_name`,`zipcode`,`country`,`city`,`state`,`address1`,`address2`,`address3`,`address4`,`email`,`phone_number_note`, `user_type`, `status`, `created_at`, `updated_at`,`name`";
                            $phone_columns = "`phone_type`, `carrier`, `phone_number`,`user_id`,`created_at`, `updated_at`";
                            $status = 1;
                            $user_type=5;
                            $user_id=1;

                            $extra_phone_values = ",'" . $user_id ."','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $query = "insert into `users` ($user_columns) VALUES ";
                            $query1 = "insert into `tenant_phone` ($phone_columns) VALUES ";
                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {


                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                if(!empty($single_row[0][16])) {

                                    $query3 = "select email from users  where user_type='5' AND email='" . $single_row[0][16] . "'";
                                    $data3 = $this->companyConnection->query($query3)->fetch();
                                   if(!empty($data3)){
                                       return ['status'=>'failed','code'=>503,'message'=>'Email Already exists.'];
                                   }
                                }

                                $extra_values = ",'" . $user_type . "','" . $status .  "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") ."','" .$single_row[0][1] . ' ' . $single_row[0][3] . "'";
//                                dd($single_row);
                                //Creating a dynamic query based on the rows from the excel file
                                $data =$this->companyConnection->query("SELECT first_name FROM users")->fetchAll();
                                if ($first_row_header == 'Salutation') {
                                    $query .= "(";
                                    $query1 .= "(";
                                    //Print each cell of the current row
                                    foreach ($single_row[0] as $key => $value) {
                                        if($key == 13 || $key == 14 || $key == 15 ){
                                            if($key == 13){

                                                if(!empty($value)) {
                                                    $value = $this->phoneTypeCode($value);
                                                }else{
                                                    $value = 0;
                                                }

                                            }
                                            if($key== 14){

                                                if(!empty($value)) {
                                                    $query3 = $this->companyConnection->query("SELECT * FROM carrier WHERE carrier='" . $value . "'");
                                                    $car_name = $query3->fetch();
                                                    $value = $car_name['id'];
                                                }else{
                                                    $value = 0;
                                                }
                                            }
                                            $query1 .= "'" . $value . "',";
                                            continue;
                                        }

                                        $query .= "'" . $value . "',";
//                                        foreach ($data as $key1 => $value1) {
//                                            if (in_array($value, $value1)) {
//                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data.'];
//                                                printErrorLog($e->getMessage());
//                                            }
//                                        }
                                    }
                                    $query = substr($query, 0, -1);
                                    $query .= $extra_values . "),";
                                    $query1 = substr($query1, 0, -1);
                                    $query1 .= $extra_phone_values . "),";
//                                    dd($query1);

                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the datas'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            $query = substr($query, 0, -1);
                            $query1 = substr($query1, 0, -1);
                            try {

                                $stmt = $this->companyConnection->prepare($query);
                                $stmt1 = $this->companyConnection->prepare($query1);

                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data.'];
                                printErrorLog($e->getMessage());
                            }
                            $executed_query = $stmt->execute();
                            $employee_last_insert_id =  $this->companyConnection->lastInsertId();
                            $executed_query1 = $stmt1->execute();
                            $phone_data_last_insert_id = $this->companyConnection->lastInsertId();
                            if($phone_data_last_insert_id){
                                $sql = "UPDATE tenant_phone SET user_id=? WHERE id=?";
                                $this->companyConnection->prepare($sql)->execute([$employee_last_insert_id, $phone_data_last_insert_id]);
                            }
                            if ($executed_query) {
                                unlink($file);
                                return ['status'=>'success','code'=>200,'message'=>'Data imported successfully.'];
                            }else{
                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file.'];
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }


    public function exportExcel(){
        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
        $objPHPExcel = new PHPExcel();
        $users = $_REQUEST['table'];
        $tenant_phone = $_REQUEST['table1'];
        $employee_details = $_REQUEST['table2'];
        $query1 = "select u.id,u.salutation,u.phone_number_note,u.first_name,u.last_name,u.email,u.ssn_sin_id,u.middle_name,u.company_name,u.zipcode,u.country,u.state,u.city,u.address1,u.address2,u.address3,u.address4 from users as u where u.user_type='5' ORDER BY first_name ASC ";
        $data = $this->companyConnection->query($query1)->fetchAll();
        //Set header with temp array
        $columnHeadingArr =array("Salutation","First Name","Middle Name","Last Name","Company","Zip Code","Country","City","State","Address1","Address2","Address3","Address4","Phone Type","Carrier","Phone  Number","Email","Notes");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);
        for($i = 0; $i < count($data); $i++){



            $query2 = "select email1 from employee_details where user_id=".$data[$i]['id'];
            $data2 = $this->companyConnection->query($query2)->fetch();

            $query3 = "select phone_type,carrier,phone_number from tenant_phone where user_id=".$data[$i]['id'];
            $data3 = $this->companyConnection->query($query3)->fetch();

            $tmpArray =array();
            $salutation =  $this->getSalutation($data[$i]['salutation']);
            array_push($tmpArray,$salutation);
            $first_name = $data[$i]['first_name'];
            array_push($tmpArray,$first_name);
            $middle_name = $data[$i]['middle_name'];
            array_push($tmpArray,$middle_name);
            $last_name = $data[$i]['last_name'];
            array_push($tmpArray,$last_name);
            $company_name = $data[$i]['company_name'];
            array_push($tmpArray,$company_name);
            $zipcode = $data[$i]['zipcode'];
            array_push($tmpArray,$zipcode);
            $country = $data[$i]['country'];
            array_push($tmpArray,$country);
            $city = $data[$i]['city'];
            array_push($tmpArray,$city);
            $state = $data[$i]['state'];
            array_push($tmpArray,$state);
            $address1 = $data[$i]['address1'];
            array_push($tmpArray,$address1);
            $address2 = $data[$i]['address2'];
            array_push($tmpArray,$address2);
            $address3 = $data[$i]['address3'];
            array_push($tmpArray,$address3);
            $address4 = $data[$i]['address4'];
            array_push($tmpArray,$address4);
            $phone_type =  $this->getPhoneType($data3['phone_type']);
            array_push($tmpArray,$phone_type);
            if(!empty($data3['carrier'])) {
                $query2 = $this->companyConnection->query("SELECT carrier FROM carrier WHERE id=" . $data3['carrier']);
                $car_name = $query2->fetch();
                $carrier = $car_name['carrier'];
            }else{
                $carrier = '';
            }
            array_push($tmpArray,$carrier);
            $phone_number = $data3['phone_number'];
            array_push($tmpArray,$phone_number);
            $email1 = $data[$i]['email'];
            array_push($tmpArray,$email1);
            $phone_number_note= $data[$i]['phone_number_note'];
            array_push($tmpArray,$phone_number_note);
            array_push($sheet,$tmpArray);
        }
//        dd($sheet);
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Contact_details.xlsx"');
        $objWriter->save('php://output');
        die();
    }

    /*
     * function to get salutation
     */
    public function getSalutation($salutation){
        $salutation_value = '';
        if($salutation == "1"){
            $salutation_value = "Dr.";
        }else if($salutation == "2"){
            $salutation_value = "Mr.";
        }else if($salutation == "3"){
            $salutation_value = "Mrs.";
        }else if($salutation == "4"){
            $salutation_value = "Mr. & Mrs.";
        }else if($salutation == "5"){
            $salutation_value = "Ms.";
        }else if($salutation == "6"){
            $salutation_value = "Sir";
        }else if($salutation == "7"){
            $salutation_value = "Madam";
        }
        return  $salutation_value;
    }

    /*
 * function to get salutation
 */
    public function getPhoneType($phoneType){
        $phonetype_value = '';
        if($phoneType == "1"){
            $phonetype_value = "Mobile";
        }else if($phoneType == "2"){
            $phonetype_value = "Work";
        }else if($phoneType == "3"){
            $phonetype_value = "Fax";
        }else if($phoneType == "4"){
            $phonetype_value = "Home";
        }else if($phoneType == "5"){
            $phonetype_value = "Other";
        }
        return  $phonetype_value;
    }

    /*
* function to get salutation
*/
    public function phoneTypeCode($phoneType){
        $phonetype_value = '';
        if($phoneType == "Mobile"){
            $phonetype_value = "1";
        }else if($phoneType == "Work"){
            $phonetype_value = "2";
        }else if($phoneType == "Fax"){
            $phonetype_value = "3";
        }else if($phoneType == "Home"){
            $phonetype_value = "4";
        }else if($phoneType == "Other"){
            $phonetype_value = "5";
        }
        return  $phonetype_value;
    }



    public function deleteContact() {
        try {
            $data=[];
            $contact_id = $_POST['user_id'];
            
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                //$query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $query = "DELETE FROM users WHERE id='$contact_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function changeStatus() {
        try {
            $data=[];
            $employee_id = $_POST['user_id'];
            $status = $_POST['status'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$employee_id'";
                //$query = "DELETE FROM users WHERE id='$employee_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                if($status == 1){
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record activated successfully.');
                }else{
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deactivated successfully.');
                }

            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  @return array|void
     * function to get login company data
     */

    public function getEmployeeData() {
        try {
            $id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $employee_id = $_POST['employee_id'];
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');
            $employee_detail = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $employee_id], 'users');
            return array('status' => 'success', 'code' => 200, 'data' => $data,'employee'=>$employee_detail, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /* ends*/
    /*count Data*/

    public function getcount() {
        try{
            $data=[];
//            $data['tenant'] = $this->companyConnection->query("SELECT COUNT(users.id) as 'count' FROM users LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id WHERE users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1'")->fetch();

            $data['tenant'] = $this->companyConnection->query("SELECT COUNT(users.id) as 'count' FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN tenant_phone ON users.id=tenant_phone.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1')")->fetch();

            $data['owners'] = $this->companyConnection->query("SELECT COUNT(u.id) as 'count' FROM users u JOIN owner_details od ON u.id = od.user_id where u.user_type='4' AND u.deleted_at IS NULL")->fetch();
            $data['vendors'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where user_type='3'")->fetch();
            $data['users'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where user_type='1' AND id != '1'")->fetch();
            $data['others'] = $this->companyConnection->query("SELECT COUNT(id) as 'count' FROM users where status='1' AND (deleted_at IS NULL) AND(user_type='5' OR user_type='6' OR user_type='7')")->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => $exception->getMessage());
        }
    }
}

$ContactListAjax = new ContactListAjax();
