<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class EmployeeAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getIntialData(){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['propertylist'] = $this->companyConnection->query("SELECT id,property_id, property_name FROM general_property")->fetchAll();
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity where title != 'Other' order by title asc")->fetchAll();
            array_push($data['ethnicity'],array("id"=>9,"title" =>"Other"));
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries ORDER BY name ASC")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users` WHERE id=$id")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types`")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public function insert(){
        $files = $_FILES;

        unset($files['employee_image']);
        unset($files['file_library']);
        $data = $_POST;
        $img =  json_decode($_POST['employee_image']);
        $domain = getDomain();
        $table="users";
        $employee_email = $_POST['additional_email'];

        $columns = [$table . '.first_name', $table . '.last_name', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4', $table . '.middle_name', $table . '.dob', $table . '.ssn_sin_id'];

        if (isset($_POST['date_of_birth']) && $_POST['date_of_birth'] != "") {
            $data['dob'] = mySqlDateFormat($_POST['date_of_birth'],null,$this->companyConnection);
        }else{
            $data['dob'] ='';
        }
        $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data["firstname"]],['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["lastname"]],['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middlename"]],['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $data["address1"]],['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $data["address2"]],['table' => 'users', 'column' => 'address3', 'condition' => '=', 'value' => $data["address3"]],['table' => 'users', 'column' => 'address4', 'condition' => '=', 'value' => $data["address4"]],['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $_POST['additional_phone'][0]],['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data['dob']]];
        $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $_POST['additional_email'][0],$_POST['additional_phone'][0],$_POST['ssn'],'','','');
        if(isset($duplicate_record_check)){
            if($duplicate_record_check['code'] == 503){
                echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                die();
            }
        }
        $addEmployee = $this->addEmployee();//save employee in users table
        if($addEmployee['status']=='success'){
            $employee_id = $addEmployee['employee_id'];
        }
        $addPrimaryEmployeeInfo = $this->addPrimaryEmployeeinfo($employee_id); //tenant_details table

        if($addPrimaryEmployeeInfo['status']=='success'){
            $addPhoneInfo = $this->addPhoneInfo($employee_id); //tenant_phone table
            if($addPhoneInfo['status']='success'){

                $getTenantInfo = $this->getEmployeeInfo($employee_id); //get tenant info from tenant_details table
                if(empty($getTenantInfo)){
                 //   $this->deleteUnsaveRecord('users', $employee_id);
                    return array('code' => 400,  'status' => 'error','function'=>'getTenantInfo','employee_id'=>$employee_id);
                }else{
                    if(isset($data['vehicle']) && $data['vehicle']=='1'){
                        if((isset($data['vehicle_name'] ) && $data['vehicle_name'][0] != "")|| (isset($data['vehicle_number'] ) && $data['vehicle_number'][0] != "")||(isset($data['vehicle_type'] ) && $data['vehicle_type'][0] != "")||(isset($data['vehicle_make'] ) && $data['vehicle_make'][0] != "")||(isset($data['vehicle_model'] ) && $data['vehicle_model'][0] != "")||(isset($data['vehicle_vin'] ) && $data['vehicle_vin'][0] != "")||(isset($data['vehicle_registration'] ) && $data['vehicle_registration'][0] != "")||(isset($data['vehicle_plate_number'] ) && $data['vehicle_plate_number'][0] != "")||(isset($data['vehicle_color'] ) && $data['vehicle_color'][0] != "")||(isset($data['vehicle_year'] ) && $data['vehicle_year'][0] != "")||(isset($data['vehicle_date_purchased'] ) && $data['vehicle_date_purchased'][0] != "")) {
                            $returnVehicle = $this->addVehicleInfo($employee_id); //done
                            if($returnVehicle['status'] != 'success'){
                              //  $this->deleteUnsaveRecord('users', $employee_id);
                                return array('code' => 400,  'status' => 'error','function'=>'addVehicleInfo','employee_id'=>$employee_id);
                            }
                        }

                    }
                }

                if( (isset($_POST['emergency_contact_name'] ) && $_POST['emergency_contact_name'][0] != "")|| (isset($_POST['emergency_relation'] ) && $_POST['emergency_relation'][0] != "")||(isset($_POST['emergency_country'] ) && $_POST['emergency_country'][0] != "")|| (isset($_POST['emergency_phone'] ) && $_POST['emergency_phone'][0] != "") || (isset($_POST['emergency_email'] ) && $_POST['emergency_email'][0] != "")){
                    $returnTenantEmergency = $this->addEmergencyContact($employee_id);
                    if($returnTenantEmergency['status'] != 'success'){
                     //   $this->deleteUnsaveRecord('users', $employee_id);
                        return array('code' => 400,  'status' => 'error','function'=>'addEmergencyContact','tenant_id'=>$employee_id);
                    }
                }

                $addCredentialControls =  $this->addCredentialsControl($employee_id);
                if($addCredentialControls['status'] != 'success'){
                 //   $this->deleteUnsaveRecord('users', $employee_id);
                    return array('code' => 400,  'status' => 'error','function'=>'addCredentialsControl','employe_id'=>$employee_id);
                }

                $addEmployeeNotes =  $this->addEmployeeNotes($employee_id);
                if($addEmployeeNotes['status'] != 'success'){
                //    $this->deleteUnsaveRecord('users', $employee_id);
                    return array('code' => 400,  'status' => 'error','function'=>'addEmployeeNotes','employe_id'=>$employee_id);
                }




                if(!empty($files)) {
                    $addFiles = $this->addFiles($employee_id,$files);
                    if ($addFiles['status'] != 'success') {
                     //   $this->deleteUnsaveRecord('users', $employee_id);
                        return array('code' => 400, 'status' => 'error', 'function' => 'addFiles', 'employe_id' => $employee_id);
                    }
                }
                updateUsername($employee_id,$this->companyConnection);
                return array('status' => 'success',  'message' => 'Data updated successfully.','table'=>'all','employee_id'=>$employee_id);
            }else{
                //$this->deleteUnsaveRecord('users', $employee_id);
                return array('code' => 400,  'status' => 'error','table'=>'employee_details','employe_id'=>$employee_id);
            }

        }else{
           // $this->deleteUnsaveRecord('users', $employee_id);
            return array('code' => 400,  'status' => 'error','function'=>'addPrimaryTenantinfo','employe_id'=>$employee_id);
        }
    }

    public function getEmployeeInfo($employee_id)
    {
        return $this->companyConnection->query("SELECT * FROM employee_details WHERE user_id ='".$employee_id."'")->fetch();
    }


    public function addEmployee()
    {
        try{
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $tenant_email = $_POST['additional_email'];
            $tenant_phone_number = $_POST['additional_phone'];
            $data['name'] = $_POST['firstname'].' '. $_POST['lastname'];
            $data['email'] = $tenant_email[0];
            $data['phone_number'] = $tenant_phone_number[0];

            $data['phone_type']             = (isset($_POST['additional_phoneType']))? $_POST['additional_phoneType'][0] : '';
            $data['carrier']                = (isset($_POST['additional_carrier']))? $_POST['additional_carrier'][0] : '';
            if ( $data['phone_type'] == 5 ||  $data['phone_type']  == 2){
                $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension']))? $_POST['other_work_phone_extension'][0] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }

            $data['country_code']           = (isset($_POST['additional_countryCode']))? $_POST['additional_countryCode'][0] : '';
            $data['salutation'] = $_POST['salutation'];
            $data['zipcode'] = $_POST['zip_code'];
            $data['city'] = $_POST['city'];
            $data['state'] = $_POST['state'];
            $data['country'] = $_POST['country'];
            $data['first_name'] = $_POST['firstname'];
            $data['last_name'] = $_POST['lastname'];
            $data['middle_name'] = $_POST['middlename'];
            $data['address1'] = $_POST['address1'];
            $data['address2'] = $_POST['address2'];
            $data['address3'] = $_POST['address3'];
            $data['address4'] = $_POST['address4'];
            if (isset($_POST['date_of_birth']) && $_POST['date_of_birth'] != "") {
                $data['dob'] = mySqlDateFormat($_POST['date_of_birth'],null,$this->companyConnection);
            }
            if (isset($_POST['gender']) && $_POST['gender'] != "") {
                $data['gender'] = $_POST['gender'];
            }
            $data['nick_name'] = $_POST['nickname'];
            $data['maiden_name'] = $_POST['maidenname'];
            $data['status'] = '1';
            $data['user_type'] = '8';
            $data['domain_name'] = $subdomain;
            if (isset($_POST['hobbies'])) {
                $data['hobbies'] = serialize($_POST['hobbies']);
            }
            $note = $_POST['add_note_phone_number'];
            $data['phone_number_note'] = $note;
            $data['maritial_status'] = $_POST['maritalStatus'];
            $data['veteran_status'] = $_POST['veteranStatus'];
            $data['ethnicity'] = $_POST['ethncity'];
            $data['status'] = '1';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['record_status'] = '1';
            foreach($_POST['ssn'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn'][$key]);
            }
            $data['ssn_sin_id'] = !empty($_POST['ssn'])? serialize($_POST['ssn']):NULL;
            $referralSource = empty($_POST['additional_referralSource'])? NULL:$_POST['additional_referralSource'];
            $data['referral_source'] = $referralSource;
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO users(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $id = $this->companyConnection->lastInsertId();
            $employeeData = $data;
            $employeeData['id'] = $id;
            $employeeData['email'] = $tenant_email;
            $employeeData['phone_number'] = $tenant_phone_number;
            $ElasticSearchSave = insertDocument('EMPLOYEE','ADD',$employeeData,$this->companyConnection);
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'users','employee_id'=>$id);
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'users');
            printErrorLog($e->getMessage());
        }


    }

    /*to add tenant primary information*/
    public function addPrimaryEmployeeinfo($user_id)
    {
        $custom_field =  json_decode($_POST['custom_field']);
        $custom_fields = array();
        foreach ($custom_field as $key => $value) {
            # code...
            $customData = (array)$value;

            $custom_fields[] = $customData;
        }

        if(!empty($custom_fields)){
            foreach ($custom_fields as $key=>$value){
                if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                continue;
            }
        }

        $custom_data = ['is_deletable' => '0','is_editable' => '0'];
        $updateColumn = createSqlColValPair($custom_data);
        if(count($custom_fields)> 0){
            foreach ($custom_fields as $key=>$value){
                updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
            }
        }

        // $insert['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
        try{
            $tenant_email = $_POST['additional_email'];
            $emailCount =  count($tenant_email);
            if($emailCount==3)
            {
                $email1 = $tenant_email[0];
                $email2 = $tenant_email[1];
                $email3 = $tenant_email[2];
            }
            else if($emailCount==2)
            {
                $email1 = $tenant_email[0];
                $email2 = $tenant_email[1];
                $email3 = "";
            }
            else
            {
                $email1 = $tenant_email[0];
                $email2 = "";
                $email3 = "";
            }

            //$note = $_POST['add_note_phone_number'];
            $driverProvince = $_POST['driving_license_state'];
            $driverLicense = $_POST['driving_license'];
            $vehicle = $_POST['vehicle'];
            $data=[];
            $data['user_id'] = $user_id;
            $data['employee_image'] = json_decode($_POST['employee_image']);
            //$data['phone_number_note'] = $note;
            $data['email1'] = $email1;
            $data['email2'] = $email2;
            $data['email3'] = $email3;
            $data['current_company']= $_POST['current_company'];
            $data['previous_company'] = $_POST['previous_company'];

            $data['employee_license_state'] = $driverProvince;
            $data['employee_license_number'] = $driverLicense;
            $data['vehicle'] = $vehicle;
            $data['status'] = '1';
            $data['custom_field'] =  (count($custom_fields)>0) ? serialize($custom_fields):null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO employee_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('status' => 'success',  'message' => 'Data added successfully.');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    public function addPhoneInfo($employee_id)
    {
        $phoneType = $_POST['additional_phoneType'];
        $i=0;
        foreach($phoneType as $phone)
        {
            $data['user_id'] = $employee_id;
            $data['parent_id'] = 0;
            $data['phone_type'] = empty($phone)?'0':$phone;
            $data['carrier'] = empty($_POST['additional_carrier'][$i])?'0':$_POST['additional_carrier'][$i];
            $data['phone_number'] = $_POST['additional_phone'][$i];
            $data['country_code'] = $_POST['additional_countryCode'][$i];
            $data['user_type'] = 0; //0 value for main tenant
//            $data['work_phone_extension'] = @$_POST['work_phone_extension'][$i];
//            $data['other_work_phone_extension'] = @$_POST['other_work_phone_extension'][$i];
            if ( $_POST['additional_phoneType'][$i] == 5 ||  $_POST['additional_phoneType'][$i] == 2){
                $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension'][$i]))? $_POST['other_work_phone_extension'][$i] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $i++;
        }
        return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_phone');
    }

    public function addVehicleInfo($employee_id)
    {

        try{
            $vehicleType = $_POST['vehicle_type'];
            $i = 0;
            foreach($vehicleType as $vehicleType)
            {
                $data['vehicle_name'] = $_POST['vehicle_name'][$i];
                $data['vehicle_number'] = $_POST['vehicle_number'][$i];
                $data['model'] = $_POST['vehicle_model'][$i];
                $data['user_id'] = $employee_id;
                $data['type'] = $vehicleType;
                $data['make'] = $_POST['vehicle_make'][$i];
                $data['license'] = $_POST['vehicle_plate_number'][$i];
                $data['color'] = $_POST['vehicle_color'][$i];
                $data['year'] = $_POST['vehicle_year'][$i];
                $data['vin'] = $_POST['vehicle_vin'][$i];
                $data['registration'] = $_POST['vehicle_registration'][$i];
                $data['date_purchased'] = empty($_POST['vehicle_date_purchased'][$i])?NULL:mySqlDateFormat($_POST['vehicle_date_purchased'][$i],null,$this->companyConnection);
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_vehicles(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_vechcle');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_vechile');
            printErrorLog($e->getMessage());
        }

    }


    public function addEmergencyContact($employee_id)
    {
        try{

            $i=0;
            foreach($_POST['emergency_contact_name'] as $credentialName)
            {

                $data['user_id'] = $employee_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $id = $this->companyConnection->lastInsertId();

                $i++;

            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'emergency_details');
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }



    }

    public function addFiles($employee_id,$files){
        try{
            if(!empty($files)) {

                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');

                foreach ($files as $key => $value) {

                    $file_name = $value['name'];
                    $fileData = getSingleWithAndConditionRecord($this->companyConnection ,['column'=>'filename','value'=>$file_name], 'tenant_chargefiles',['column'=>'user_id','value'=>$employee_id]);
                    if($fileData['code'] == '200'){
                        return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['user_id'] = $employee_id;
                    $data['filename'] = $file_name;
                    // $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['file_location'] = $path . '/' . $name . '.' . $ext;
                    $data['file_extension'] = $ext;
                    //$data['codec'] = $value['type'];
                    if(strstr($value['type'], "video/")){
                        $type = 3;

                    }else if(strstr($value['type'], "image/")){
                        $type = 1;
                    }else{
                        $type = 2;
                    }
                    $data['file_type'] = $type;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargefiles (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_chargefiles');
            }

        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_vechile');
            printErrorLog($e->getMessage());
        }
    }


    public function addCredentialsControl($employee_id)
    {
        try{
            $i=0;
            foreach($_POST['credentialName'] as $credentialName)
            {
                $acquire_date = $_POST['acquireDate'][$i];
                $expireDate = $_POST['expirationDate'][$i];
                $data['user_id'] = $employee_id;
                $data['credential_name'] = $_POST['credentialName'][$i];
                $data['credential_type'] = !empty($_POST['credentialType'][$i])?$_POST['credentialType'][$i]:NULL;
                $data['acquire_date'] = !empty($acquire_date)?mySqlDateFormat($acquire_date,null,$this->companyConnection):NULL;
                $data['expire_date'] = !empty($expireDate)?mySqlDateFormat($expireDate,null,$this->companyConnection):NULL;
                $data['notice_period'] = $_POST['noticePeriod'][$i];
                /*  return  $data; */

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt1 = $this->companyConnection->prepare($query);
                $stmt1->execute($data);

                $i++;
            }


            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_credentials');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }

    public function addEmployeeNotes($employee_id)
    {
        try{
            if(isset($_POST['employee_notes'])){
                $chargeNote = $_POST['employee_notes'];
                $i = 0;
                foreach($chargeNote as $note){
                    if($note!=""){
                        $data['user_id'] = $employee_id;
                        $data['note'] = $note;
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                    $i++;
                }
            }
            return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_chargenote');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }




    public function deleteUnsaveRecord($table, $id){
        $query = "DELETE from $table WHERE id = $id";
        $stmt = $this->companyConnection->prepare($query);
        $return = $stmt->execute();
        return $return;
    }
}
$employeeAjax = new EmployeeAjax();