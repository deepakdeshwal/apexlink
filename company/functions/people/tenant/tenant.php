<?php

/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
//error_reporting(E_ALL);
//ini_set("display_errors",1);
include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class tenantAjax extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company amenities table
     * @return array
     */
    public function insert()
    {

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        define('UPLOAD_DIR', 'images/');
        $img = $_POST['tenant_image'];
        $this->companyConnection;
        $requestData = $_REQUEST;
        $_REQUEST;
        $addTenant = $this->addTenant();//users table


        if ($addTenant['status'] == 'success') {
            $tenant_id = $addTenant['tenant_id'];
            updateUsername($tenant_id ,$this->companyConnection);
        }else{
            return array('code' => 400, 'status' => 'error', 'message' => 'Record already exist!');
        }

        $addTenantProperty = $this->addTenantProperty($tenant_id); //tenant_property table
        if ($addTenantProperty['status'] != 'success') {
            $this->deleteUnsaveRecord('users', $tenant_id);
            return array('code' => 400, 'status' => 'error', 'function' => 'addTenantProperty', 'tenant_id' => $tenant_id);
        }

        $addPrimaryTenantInfo = $this->addPrimaryTenantinfo($tenant_id); //tenant_details table

        if ($addPrimaryTenantInfo['status'] == 'success') {
            $addPhoneInfo = $this->addPhoneInfo($tenant_id, 'T'); //tenant_phone table
            if ($addPhoneInfo['success'] = 'success') {

                $getTenantInfo = $this->getTenantInfo($tenant_id); //get tenant info from tenant_details table
                if (empty($getTenantInfo)) {
                    return array('code' => 400, 'status' => 'error', 'function' => 'getTenantInfo', 'tenant_id' => $tenant_id);
                } else {
                    if ($getTenantInfo['pet'] == '1') {
                        $returnPet = $this->addPetInfo($tenant_id);//done 
                        if ($returnPet['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addPetInfo', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['vehicle'] == '1') {

                        $returnVehicle = $this->addVehicleInfo($tenant_id); //done
                        if ($returnVehicle['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addVehicleInfo', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['animal'] == '1') {
                        $returnAnimal = $this->addCompanionAnimals($tenant_id); //done 
                        if ($returnAnimal['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addCompanionAnimals', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['medical_allergy'] == '1') {
                        $returnMedical = $this->addMedicalAllergies($tenant_id); //done 
                        if ($returnMedical['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addMedicalAllergies', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['medical_allergy'] == '1') {
                        $returnParking = $this->addParkingInfo($tenant_id);
                        if ($returnParking['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addParkingInfo', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['guarantor'] == '1') {
                        $returnGuarantor = $this->addGuarantor($tenant_id);

                        if ($returnGuarantor['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addGuarantor', 'tenant_id' => $tenant_id);
                        }
                    }

                    if ($getTenantInfo['collection'] == '1') {
                        $returnCollection = $this->addCollection($tenant_id);
                        if ($returnCollection['status'] != 'success') {
                            return array('code' => 400, 'status' => 'error', 'function' => 'addCollection', 'tenant_id' => $tenant_id);
                        }
                    }
                }

                if (isset($_POST['addAdditionalTenant'])) {
                    $returnAdditionTenant = $this->addAdditionalTenant($tenant_id);
                    if ($returnAdditionTenant['status'] != 'success') {
                        return array('code' => 400, 'status' => 'error', 'function' => 'addAdditionalTenant', 'tenant_id' => $tenant_id);
                    }
                }

                if (isset($_POST['emergency_contact_name']) && $_POST['emergency_contact_name'][0] != "") {
                    $returnTenantEmergency = $this->addEmergencyContact($tenant_id);
                    if ($returnTenantEmergency['status'] != 'success') {
                        return array('code' => 400, 'status' => 'error', 'function' => 'addEmergencyContact', 'tenant_id' => $tenant_id);
                    }
                }

                $updateKey = $this->updateAdditionalKey($tenant_id);

                 if(!empty($_POST['ccard_number'])) {
                    $VendorPaymentStep1 = $this->VendorPaymentStep1($tenant_id, $_POST['ccard_number'], $_POST['ccvv']
                        ,$_POST['cexpiry_year'], $_POST['cexpiry_month']);
                }

                return array('status' => 'success', 'message' => 'Data updated successfully.', 'table' => 'all', 'tenant_id' => $tenant_id);
            } else {
                $this->deleteUnsaveRecord('users', $tenant_id);
                return array('code' => 400, 'status' => 'error', 'table' => 'tenant_details', 'tenant_id' => $tenant_id);
            }
            $addCredentialControls = $this->addCredentialsControl($tenant_id);
            if ($addCredentialControls['status'] != 'success') {
                return array('code' => 400, 'status' => 'error', 'function' => 'addCredentialsControl', 'tenant_id' => $tenant_id);
            }
        } else {
            return array('code' => 400, 'status' => 'error', 'function' => 'addPrimaryTenantinfo', 'tenant_id' => $tenant_id);
        }



    }

    public function checkDuplicateRecordForContact($companyConnection, $email = "", $phoneNumber = "", $ssn, $driveLicence = "", $propertyParcel = "",$vin ="",$contact_id) {
        try{
            if ($email != ""){
                $getData = $companyConnection->query('SELECT email FROM users where email="'.$email.'" AND id!='.$contact_id)->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Email, this email already Exists!"));
                }
                // return true;
            }
            if ($phoneNumber != ""){
                $getData = $companyConnection->query('SELECT phone_number FROM users where phone_number="'.$phoneNumber.'"AND id!='.$contact_id)->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Phone Number, this number already Exists!"));
                }
            }

            if (!empty($ssn)){
                if (is_array($ssn)){
                    foreach ($ssn as $ss){
                        $regVariable = '.*;s:[0-9]+:"'.$ss.'".*';
                        $regVariable = "'".$regVariable."'";

                        $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable." AND id!=".$contact_id)->fetch();
                        //  dd("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable);
                        if (!empty($getData)){
                            return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                        }
                    }
                } else{
                    $regVariable = '.*;s:[0-9]+:"'.$ssn.'".*';
                    $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP $regVariable AND id!=$contact_id")->fetch();
                    if (!empty($getData)){
                        return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                    }
                }
            }
            if ($driveLicence != ""){
                $getData = $companyConnection->query('SELECT id FROM tenant_details WHERE tenant_license_number ="'.$driveLicence.'" AND id!='.$contact_id)->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
                }else{
                    $getData1 = $companyConnection->query('SELECT id FROM employee_details WHERE employee_license_number ="'.$driveLicence.'" AND id!='.$contact_id)->fetch();
                    if (!empty($getData1)){
                        return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
                    }
                    //  return true;
                }
            }
            if ($propertyParcel != ""){
                $getData = $companyConnection->query('SELECT id FROM genera_property where property_parcel_number="'.$propertyParcel.'" AND id!='.$contact_id)->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Phone Number, this number already Exists!"));
                }
                //  return true;
            }
            if ($vin != ""){
                $getData = $companyConnection->query('SELECT id FROM tenant_vehicles where vin="'.$vin.'" AND id!='.$contact_id)->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Vehicle ID Number, this VIN number already Exists!"));
                }
                //  return true;
            }
        } catch (PDOException $e) {
            return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
        }

     }


     public function checkNameAlreadyExistsForContact($connection, $table, $column_name, $column_value, $edit_id,$contact_id)
     {
            try {
        if ($edit_id) {
            $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $column_name . " = '" . $column_value . "' AND id != " . $edit_id)->fetch();
        } else {
            $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $column_name . " = '" . $column_value . "' AND id!=$contact_id")->fetch();
        }


        if ($data) {
            return ['is_exists' => 1, 'data' => $data];
        } else {
            return ['is_exists' => 0];
        }
    } catch (PDOException $e) {
        return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
    }

     }

    public function addTenant()
    {
        try {
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $tenant_email = $_POST['email'];
            $tenant_emails = $tenant_email[0];
            $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.dob', 'users.ssn_sin_id'];
            if (isset($_POST['birth']) && $_POST['birth'] != "") {
                $dataa['dob'] = mySqlDateFormat($_POST['birth'],null,$this->companyConnection);
            }else{
                $dataa['dob'] ='';
            }
            $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $_POST["firstname"]],['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $_POST["lastname"]],['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $_POST["middlename"]],['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $_POST['phoneNumber'][0]],['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $dataa['dob']]];

            if($_POST['contact']!="0")
            {
              $contact_id =  $_POST['contact'];
               $duplicate_record = $this->checkDuplicateRecordForContact($this->companyConnection, $_POST['email'][0],$_POST['phoneNumber'][0],$_POST['ssn'],$_POST['driverLicense'],'',$_POST['vehicle_vin'][0],$contact_id);
                if($duplicate_record['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record['message']));
                    die();
                }
            } else {
                if(isset($_POST['vehicle_vin'])){
                    for($i=0;$i<count($_POST['vehicle_vin']);$i++){
                        $duplicate_record['is_exists']=0;
                        $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $_POST['email'][0],$_POST['phoneNumber'][0],$_POST['ssn'],$_POST['driverLicense'],'',$_POST['vehicle_vin'][$i]);
                        if(isset($duplicate_record_check)){
                            if($duplicate_record_check['code'] == 503){
                                echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                                die();
                            }
                        }
                    }

                }else{
                    $duplicate_record['is_exists']=0;
                    $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $_POST['email'][0],$_POST['phoneNumber'][0],$_POST['ssn'],$_POST['driverLicense'],'','');
                    if(isset($duplicate_record_check)){
                        if($duplicate_record_check['code'] == 503){
                            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                            die();
                        }
                    }
                }

            }





           /*  if($_POST['contact']!="0")
            {
              $contact_id =  $_POST['contact'];
               $duplicate = $this->checkNameAlreadyExistsForContact($this->companyConnection, 'users', 'email', $tenant_emails, '',$contact_id);

            }else
            {

            $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'email', $tenant_emails, '');
            }



            if ($duplicate['is_exists'] == 1) {
                return array('code' => 400, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!');
            }
*/





            $data['email'] = $tenant_email[0];

            $data['salutation'] = $_POST['salutation'];
            if (isset($_POST['contactTenant']) && $_POST['contactTenant'] != "") {
                $data['first_name'] = $_POST['contactTenant'];
                $data['name'] = $_POST['contactTenant'];
            } else {
                $data['first_name'] = $_POST['firstname'];
                $data['last_name'] = $_POST['lastname'];
                 $data['name'] = $_POST['firstname'].' '.$_POST['lastname'];

            }
            $data['middle_name'] = $_POST['middlename'];
            if (isset($_POST['birth']) && $_POST['birth'] != "") {
                $data['dob'] = mySqlDateFormat($_POST['birth'], null, $this->companyConnection);
            }
            if (isset($_POST['gender']) && $_POST['gender'] != "") {
                $data['gender'] = $_POST['gender'];
            }
            $data['nick_name'] = $_POST['nickname'];
            $data['maiden_name'] = $_POST['maidenname'];
            $data['status'] = '1';
            $data['user_type'] = '2';
            $data['domain_name'] = $subdomain;
            $data['country_code'] = $_POST['maidenname'];
            if (isset($_POST['hobbies'])) {
                $data['hobbies'] = serialize($_POST['hobbies']);
            }
            $data['maritial_status'] = $_POST['maritalStatus'];
            $data['veteran_status'] = $_POST['veteranStatus'];
            $data['ethnicity'] = $_POST['ethncity'];
            $data['status'] = '1';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['record_status'] = '1';
            $contactTenant = $_POST['contactTenant'];


            
            foreach($_POST['ssn'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn'][$key]);
            }
            $data['ssn_sin_id'] = !empty($_POST['ssn'])? serialize($_POST['ssn']):NULL;

            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO users(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $id = $this->companyConnection->lastInsertId();

            $tenantData = $data;
            $tenantData['email'] = $tenant_email;
            $tenantData['phone_number'] = $_POST['phoneNumber'];
            $tenantData['property_id'] = $_POST['property'];
            $tenantData['building_id'] = $_POST['building'];
            $tenantData['unit_id'] = $_POST['unit'];
            $tenantData['id'] = $id;
            $ElasticSearchSave = insertDocument('TENANT','ADD',$tenantData,$this->companyConnection);
            if($_POST['contact']!="0") {
                $contact_id =  $_POST['contact'];
                $this->deleteContactData($contact_id);
            }

            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'users', 'tenant_id' => $id);
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'users');
            printErrorLog($e->getMessage());
        }


    }


    public function deleteContactData($contact_id)
    {

        $deleteFromEmployee_details = $this->companyConnection->prepare("DELETE FROM employee_details WHERE user_id=$contact_id");
        $deleteFromEmployee_details->execute();
        $deleteFromTenant_phone = $this->companyConnection->prepare("DELETE FROM tenant_phone WHERE user_id=$contact_id");
         $deleteFromTenant_phone->execute();
        $deleteFromEmergency_details = $this->companyConnection->prepare("DELETE FROM emergency_details WHERE user_id=$contact_id");
         $deleteFromEmergency_details->execute();
        $deleteFromTenant_credential = $this->companyConnection->prepare("DELETE FROM tenant_credential WHERE user_id=$contact_id");
         $deleteFromTenant_credential->execute();
        $deleteFromTenant_chargenote = $this->companyConnection->prepare("DELETE FROM tenant_chargenote WHERE user_id=$contact_id");
         $deleteFromTenant_chargenote->execute();
        $deleteFromTenant_property = $this->companyConnection->prepare("DELETE FROM tenant_property WHERE user_id=$contact_id");
         $deleteFromTenant_property->execute();
        $deleteFromTenant_chargefiles = $this->companyConnection->prepare("DELETE FROM tenant_chargefiles WHERE user_id=$contact_id");
         $deleteFromTenant_chargefiles->execute();
        $deleteFromUsers = $this->companyConnection->prepare("DELETE FROM users WHERE id=$contact_id");
         $deleteFromUsers->execute();
    }

    public function addTenantProperty($user_id)
    {

        try {
            $property_id = $_POST['property'];
            $build_id = $_POST['building'];
            $unit = $_POST['unit'];
            $contact = $_POST['contact'];
            if (isset($_POST['contact']) && $_POST['contact'] != "0" && $_POST['contact'] != "") {
                $query = "INSERT INTO tenant_property(`user_id`,`property_id`,`building_id`,`unit_id`,`contact`) VALUES ($user_id,$property_id,$build_id,$unit,$contact)";
            } else {
                $query = "INSERT INTO tenant_property(`user_id`,`property_id`,`building_id`,`unit_id`) VALUES ($user_id,$property_id,$build_id,$unit)";
            }

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "Data");
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => "something Went wrong on tenant property");
            printErrorLog($e->getMessage());
        }

    }

    public function addPrimaryTenantinfo($user_id)
    {



        // $insert['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;


        try {
            try {
                $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];

                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        $custom_field[$key] = (array)$value;
                    }
                }

                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }

                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }


                $random = uniqid();
                if ($_FILES['tenant_image']['name'] != '') {
                    $tenant_image = $random . $_FILES['tenant_image']['name'];
                    $tmp_name = $_FILES['tenant_image']['tmp_name'];
                } else {
                    $tenant_image = "";
                }
            }catch (PDOException $e) {
                $return =  array('code' => 403, 'status' => 'failed', 'message' => $e->getMessage());
                echo "<pre>";
                print_r($return);
                die();
            }

            try{
                $tenant_email = $_POST['email'];
                $emailCount = count($tenant_email);


                if ($emailCount == 3) {
                    $email1 = $tenant_email[0];
                    $email2 = $tenant_email[1];
                    $email3 = $tenant_email[2];
                } else if ($emailCount == 2) {
                    $email1 = $tenant_email[0];
                    $email2 = $tenant_email[1];
                    $email3 = "";
                } else {
                    $email1 = $tenant_email[0];
                    $email2 = "";
                    $email3 = "";
                }


                $note = $_POST['note'];
                $referralSource = $_POST['referralSource'];

                $moveIn = $_POST['moveInDate'];

                $moveInDate = mySqlDateFormat($moveIn, null, $this->companyConnection);

                $driverProvince = $_POST['driverProvince'];
                $driverLicense = $_POST['driverLicense'];

                if ($_POST['parking_keys'] == "") {
                    $movein_key_signed = 0;
                } else {
                    $movein_key_signed = $_POST['parking_keys'];
                }


                if (isset($_POST['entity']) && $_POST['entity']=='1') {
                    $pet = 0;
                    $vehicle = 0;
                    $companion = 0;
                    $parkingSpace = 0;
                    $smocker = 0;
                    $guarantor = 0;
                    $collection = 0;
                    $recordStatus = 0;
                    $animal = 0;
                    $medical = 0;
                    $contactTenant = $_POST['contactTenant'];
                     $record_status = 0;

                } else {
                    $pet = $_POST['pet'];
                    $vehicle = $_POST['vehicle'];
                    $animal = $_POST['companion'];
                    $parkingSpace = $_POST['space'];
                    $smocker = $_POST['smocker'];
                    $guarantor = $_POST['guarantor'];
                    $collection = $_POST['select_property_collection'];
                    $record_status = 0;
                    $medical = $_POST['medical'];
                    $recordStatus = 0;
                    $contactTenant = "";
                }
            }catch (PDOException $e) {
                $return =  array('code' => 404, 'status' => 'failed', 'message' => $e->getMessage());
                echo "<pre>";
                print_r($return);
                die();
            }

            try{
                $data = [];
                $data['user_id'] = $user_id;
                $data['is_additional_tenant'] = 0;
                $data['tenant_image'] = json_decode($_POST['tenant_image']);
                $data['tenant_contact'] = $contactTenant;
                $data['phone_number_note'] = $note;
                $data['email1'] = $email1;
                $data['email2'] = $email2;
                $data['email3'] = $email3;
                $data['move_in'] = $moveInDate;
                $data['referral_source'] = $referralSource;
                $data['tenant_license_state'] = $driverProvince;
                $data['tenant_license_number'] = $driverLicense;
                $data['movein_key_signed'] = $movein_key_signed;
                $data['vehicle'] = $vehicle;
                $data['guarantor'] = $guarantor;
                $data['pet'] = $pet;
                $data['parking_space'] = $parkingSpace;
                $data['medical_allergy'] = $medical;
                $data['animal'] = $animal;
                $data['collection'] = $collection;
                $data['status'] = '1';
                $data['smoker'] = $smocker;
                $data['record_status'] = $record_status;
              //  $data['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;


                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('status' => 'success', 'message' => 'Data added successfully.');
            }catch (PDOException $e) {
                $return =  array('code' => 405, 'status' => 'failed', 'message' => $e->getMessage());
                echo "<pre>";
                print_r($return);
                die();
            }
        } catch (PDOException $e) {
            //return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            $return = array('code' => 406, 'status' => 'failed', 'message' => $e->getMessage());
            echo "<pre>";
            print_r($return);
            die();
        }


    }


    /*
    public function generateImage($img)
        {



             $folderPath = "/var/www/html/apex/company/uploads/";
             $image_parts = explode(";base64,", $img);
             $image_type_aux = explode("uploads/", $image_parts[0]);
             $image_base64 = base64_decode($image_parts[1]);
             $name = uniqid() . '.png';
             $file = $folderPath . $name;
             file_put_contents($folderPath, $image_base64);
             $name;

        }

    */

    public function addPhoneInfo($tenant_id, $user_type)
    {
        $phoneType = $_POST['phoneType'];
        $userphoneNumber = $_POST['phoneNumber'];

        if ($user_type = 'T') {
            $parent_id = 0;
        }
        $i = 0;
        foreach ($phoneType as $phone) {
            $data['user_id'] = $tenant_id;
            $data['parent_id'] = $parent_id;
            $data['phone_type'] = $phone;
            $data['carrier'] = $_POST['carrier'][$i];
            $data['work_phone_extension'] = $_POST['Extension'][$i];
            $data['phone_number'] = $_POST['phoneNumber'][$i];
            $data['country_code'] = $_POST['countryCode'][$i];
            $data['user_type'] = 0; //0 value for main tenant

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);


            $i++;
        }

            $data1['phone_number'] = $userphoneNumber[0];
            $sqlData1 = createSqlColValPair($data1);
            $query1 = "UPDATE users SET " . $sqlData1['columnsValuesPair'] . " where id='$tenant_id'";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();

        return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_phone');

    }

    public function getTenantInfo($tenant_id)
    {
        return $this->companyConnection->query("SELECT * FROM tenant_details WHERE user_id ='" . $tenant_id . "'")->fetch();


    }


    /*for emergency content details*/

    public function addPetInfo($tenant_id)
    {
        try {
            $petName = $_POST['pet_name'];
            $i = 0;
            foreach ($petName as $pet_name) {
                $data['user_id'] = $tenant_id;
                $data['name'] = $_POST['pet_name'][$i];
                $data['pet_id'] = $_POST['pet_id'][$i];
                $data['type'] = $_POST['pet_type'][$i];
                $dob = $_POST['pet_birth'][$i];
                $data['dob'] = mySqlDateFormat($dob, null, $this->companyConnection);
                $data['age'] = $_POST['pet_age'][$i];
                $data['gender'] = $_POST['pet_gender'][$i];
                $data['weight'] = $_POST['pet_weight'][$i];
                $data['weight_unit'] = $_POST['pet_weight_unit'][$i];
                $data['note'] = $_POST['pet_note'][$i];
                $data['color'] = $_POST['pet_color'][$i];
                $data['chip_id'] = $_POST['pet_chipid'][$i];
                $data['hospital_name'] = $_POST['pet_vet'][$i];
                $data['phone_number'] = $_POST['pet_phoneNumber'][$i];


                $next_visit = $_POST['pet_nextVisit'][$i];
                $last_visit = $_POST['pet_lastVisit'][$i];


                $data['next_visit'] = mySqlDateFormat($next_visit, null, $this->companyConnection);
                $data['last_visit'] = mySqlDateFormat($last_visit, null, $this->companyConnection);


                $data['medical_condition'] = $_POST['pet_medical'][$i];
                if ($data['medical_condition'] == '1') {
                    $data['medical_condition_note'] = $_POST['pet_medical_condition_note'][$i];
                } else {
                    $data['medical_condition_note'] = "";
                }

                $data['shots'] = $_POST['pet_shots'][$i];

                if ($data['shots'] == '1') {

                    $data['shots_name'] = $_POST['pet_name_shot'][$i];
                    $shots_given_date = $_POST['pet_date_given'][$i];
                    $shots_expire_date = $_POST['pet_expiration_date'][$i];
                    $shots_followup_date = $_POST['pet_follow_up'][$i];
                    $data['shots_given_date'] = mySqlDateFormat($shots_given_date, null, $this->companyConnection);
                    $data['shots_expire_date'] = mySqlDateFormat($shots_expire_date, null, $this->companyConnection);
                    $data['shots_followup_date'] = mySqlDateFormat($shots_followup_date, null, $this->companyConnection);
                    $data['shots_note'] = $_POST['pet_shots_note'][$i];
                }


                $data['rabies'] = $_POST['pet_rabies'][$i];

                if ($data['rabies'] == '1') {

                    $data['rabies_name'] = $_POST['pet_rabies_name'][$i];
                    $rabies_given_date = $_POST['pet_rabies_given_date'][$i];
                    $rabies_expire_date = $_POST['pet_rabies_expiration_date'][$i];

                    $data['rabies_given_date'] = mySqlDateFormat($rabies_given_date, null, $this->companyConnection);
                    $data['rabies_expire_date'] = mySqlDateFormat($rabies_expire_date, null, $this->companyConnection);
                    $data['rabies_note'] = $_POST['pet_rabies_note'][$i];

                }


                $image1 = json_decode($_POST['pet_image1']);
                $image2 = json_decode($_POST['pet_image2']);
                $image3 = json_decode($_POST['pet_image3']);


                $data['image1'] = $image1[$i];
                $data['image2'] = $image2[$i];
                $data['image3'] = $image3[$i];


                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_pet(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_pet');

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_pet');
            printErrorLog($e->getMessage());
        }


    }

    public function addVehicleInfo($tenant_id)
    {

        try {
            $vehicleType = $_POST['vehicle_type'];
            $vehicle_image1 = json_decode($_POST['vehicle_image1']);
            $vehicle_image2 = json_decode($_POST['vehicle_image2']);
            $vehicle_image3 = json_decode($_POST['vehicle_image3']);

            $i = 0;
            foreach ($vehicleType as $vehicleType) {
                $data['user_id'] = $tenant_id;
                $data['type'] = $vehicleType;
                $data['make'] = $_POST['vehicle_make'][$i];
                $data['license'] = $_POST['vevicle_license'][$i];
                $data['color'] = $_POST['vehicle_color'][$i];
                $data['year'] = $_POST['vevicle_year'][$i];
                $data['vin'] = $_POST['vehicle_vin'][$i];
                $data['registration'] = $_POST['vehicle_registration'][$i];

                $data['photo1'] = $vehicle_image1[$i];
                $data['photo2'] = $vehicle_image2[$i];
                $data['photo3'] = $vehicle_image3[$i];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_vehicles(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_vechcle');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_vechile');
            printErrorLog($e->getMessage());
        }

    }

    public function addCompanionAnimals($tenant_id)
    {

        try {
            $petName = $_POST['service_name'];
            $i = 0;
            foreach ($petName as $pet_name) {
                $data['user_id'] = $tenant_id;
                $data['name'] = $_POST['service_name'][$i];
                $data['animal_id'] = $_POST['service_id'][$i];
                $data['type'] = $_POST['service_type'][$i];
                $dob = $_POST['service_birth'][$i];
                $data['dob'] = mySqlDateFormat($dob, null, $this->companyConnection);
                $data['age'] = $_POST['service_year'][$i];

                $data['gender'] = $_POST['service_gender'][$i];
                $data['weight'] = $_POST['service_weight'][$i];
                $data['weight_unit'] = $_POST['service_weight_unit'][$i];
                $data['note'] = $_POST['service_note'][$i];
                $data['color'] = $_POST['service_color'][$i];
                $data['chip_id'] = $_POST['service_chipid'][$i];
                $data['hospital_name'] = $_POST['service_vet'][$i];
                $data['phone_number'] = $_POST['service_countryCode'][$i];

                $next_visit = $_POST['service_nextVisit'][$i];
                $last_visit = $_POST['service_lastVisit'][$i];


                $data['next_visit'] = mySqlDateFormat($next_visit, null, $this->companyConnection);
                $data['last_visit'] = mySqlDateFormat($last_visit, null, $this->companyConnection);


                $data['medical_condition'] = $_POST['service_medical'][$i];
                if ($data['medical_condition'] == '1') {
                    $data['medical_condition_note'] = $_POST['service_medical_condition_note'][$i];
                } else {
                    $data['medical_condition_note'] = "";
                }

                $data['shots'] = $_POST['service_shots'][$i];

                if ($data['shots'] == '1') {

                    $data['shots_name'] = $_POST['service_name_shot'][$i];

                    $shots_given_date = $_POST['service_date_given'][$i];
                    $shots_expire_date = $_POST['service_expiration_date'][$i];
                    $shots_followup_date = $_POST['service_follow_up'][$i];

                    $data['shots_given_date'] = mySqlDateFormat($shots_given_date, null, $this->companyConnection);
                    $data['shots_expire_date'] = mySqlDateFormat($shots_expire_date, null, $this->companyConnection);
                    $data['shots_followup_date'] = mySqlDateFormat($shots_followup_date, null, $this->companyConnection);


                    $data['shots_note'] = $_POST['service_note1'][$i];
                }


                $data['rabies'] = $_POST['service_rabies'][$i];

                if ($data['rabies'] == '1') {

                    $data['rabies_name'] = $_POST['animal_rabies'][$i];
                    $rabies_given_date = $_POST['animal_date_given'][$i];
                    $rabies_expire_date = $_POST['animal_expiration_date'][$i];

                    $data['rabies_given_date'] = mySqlDateFormat($rabies_given_date, null, $this->companyConnection);
                    $data['rabies_expire_date'] = mySqlDateFormat($rabies_expire_date, null, $this->companyConnection);


                    $data['rabies_note'] = $_POST['animal_rabies'][$i];

                }
                $image1 = json_decode($_POST['service_image1']);
                $image2 = json_decode($_POST['service_image2']);
                $image3 = json_decode($_POST['service_image3']);

                $data['image1'] = $image1[$i];
                $data['image2'] = $image2[$i];
                $data['image3'] = $image3[$i];

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_service_animal(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;

            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_animals');


        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_service_animal');
            printErrorLog($e->getMessage());
        }


    }

    public function addMedicalAllergies($tenant_id)
    {
        try {

            $medical_issue = $_POST['medical_issue'];
            $i = 0;
            foreach ($medical_issue as $issue) {
                $data['user_id'] = $tenant_id;
                $data['allergy'] = $_POST['medical_issue'][$i];
                $data['note'] = $_POST['medical_note'][$i];
                $data['date'] =  mySqlDateFormat($_POST['medical_date'][$i], null, $this->companyConnection);
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_medical_allergies(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_medical');

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_medical');
            printErrorLog($e->getMessage());
        }


    }

    public function addParkingInfo($tenant_id)
    {

        try {
            $tenant_parking = $_POST['parking_number'];
            $i = 0;
            foreach ($tenant_parking as $parking) {
                $data['user_id'] = $tenant_id;
                $data['permit_number'] = $parking;
                $data['space_number'] = $_POST['parking_space'][$i];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_parking(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;

            }

            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_medical');

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_medical');
            printErrorLog($e->getMessage());
        }


    }

    public function addGuarantor($tenant_id)
    {

        $random = uniqid();
        if (isset($_POST['guarantor_entity'])) {

            try {
                $i = 0;
                $j = 1;
                foreach ($_POST['guarantor_form2_entity'] as $entity) {

                    $data['user_id'] = $tenant_id;
                    $data['company_name'] = $_POST['guarantor_form2_entity'][$i];
                    $data['is_guarantor_company'] = 1;
                    $data['zip_code'] = $_POST['guarantor_form2_postalcode'][$i];
                    $data['country'] = $_POST['guarantor_form2_country'][$i];
                    $data['state'] = $_POST['guarantor_form2_province'][$i];
                    $data['city'] = $_POST['guarantor_form2_city'][$i];
                    $data['address1'] = $_POST['guarantor_form2_address1'][$i];
                    $data['address2'] = $_POST['guarantor_form2_address2'][$i];
                    $data['address3'] = $_POST['guarantor_form2_address3'][$i];
                    $data['address4'] = $_POST['guarantor_form2_address4'][$i];
                    $data['relationship'] = $_POST['guarantor_form2_relationship'][$i];
                    $data['mc_first_name'] = $_POST['guarantor_form2_mainContact'][$i];
                    $data['mc_mi'] = $_POST['guarantor_form2_middlename'][$i];
                    $data['mc_last_name'] = $_POST['guarantor_form2_lastname'][$i];
                    $data['note'] = $_POST['guarantor_form2_note'][$i];
                    $data['guarantee_years'] = $_POST['guarantor_form2_guarantee'][$i];
                    if (isset($_FILES['guarantor_form2_files']['name'][$i])) {
                        $tmp_name = $_FILES["guarantor_form2_files"]["tmp_name"][$i];
                        $data['file_name'] = $random . $_FILES['guarantor_form2_files']['name'][$i];
                        move_uploaded_file($tmp_name, ROOT_URL . "/company/uploads/tenant_files/" . $data['file_name']);

                    } else {
                        $data['file_name'] = "";
                    }

                    $guarantor_email = $_POST['guarantor_form2_email_' . $j];


                    $emailCount = count($guarantor_email);


                    if ($emailCount == 3) {
                        $email1 = $guarantor_email[0];
                        $email2 = $guarantor_email[1];
                        $email3 = $guarantor_email[2];
                    } else if ($emailCount == 2) {
                        $email1 = $guarantor_email[0];
                        $email2 = $guarantor_email[1];
                        $email3 = "";
                    } else {
                        $email1 = $guarantor_email[0];
                        $email2 = "";
                        $email3 = "";
                    }
                    $data['email1'] = $email1;
                    $data['email2'] = $email2;
                    $data['email3'] = $email3;

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_guarantor(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    $k = 0;

                    foreach ($_POST['guarantor_countryCode_' . $j] as $countryCode) {
                        $data1['user_id'] = $id;
                        $data1['parent_id'] = $tenant_id;
                        $data1['phone_type'] = $_POST['guarantor_form2_phoneType_' . $j][$k];
                        $data1['carrier'] = $_POST['guarantor_form2_carrier_' . $j][$k];
                        //  $data1['country_code'] =  $_POST['guarantor_form2_countryCode_'.$j][$k];
                        $data1['phone_number'] = $_POST['guarantor_form2_phone_' . $j][$k];
                        $data1['user_type'] = 2; //2 value for guarantor

                        $sqlData1 = createSqlColVal($data1);

                        $query1 = "INSERT INTO tenant_phone(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query1);
                        $stmt1->execute($data1);
                        $k++;
                    }


                    $j++;
                    $i++;
                }
                return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_guarantor');

            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_guarantor');
            }

        } else {
            $i = 0;
            try {
                $j = 1;
                foreach ($_POST['guarantor_salutation'] as $salutation) {
                    $guarantor_email = $_POST['guarantor_email_' . $j];
                    $emailCount = count($guarantor_email);
                    if ($emailCount == 3) {
                        $email1 = $guarantor_email[0];
                        $email2 = $guarantor_email[1];
                        $email3 = $guarantor_email[2];
                    } else if ($emailCount == 2) {
                        $email1 = $guarantor_email[0];
                        $email2 = $guarantor_email[1];
                        $email3 = "";
                    } else {
                        $email1 = $guarantor_email[0];
                        $email2 = "";
                        $email3 = "";
                    }


                    $data['user_id'] = $tenant_id;
                    $data['salutation'] = $_POST['guarantor_salutation'][$i];
                    $data['first_name'] = $_POST['guarantor_firstname'][$i];
                    $data['middle_name'] = $_POST['guarantor_middlename'][$i];
                    $data['last_name'] = $_POST['guarantor_lastname'][$i];
                    $data['relationship'] = $_POST['guarantor_relationship'][$i];
                    $data['zip_code'] = $_POST['guarantor_zipcode'][$i];
                    $data['country'] = $_POST['guarantor_country'][$i];
                    $data['state'] = $_POST['guarantor_province'][$i];
                    $data['city'] = $_POST['guarantor_city'][$i];
                    $data['address1'] = $_POST['guarantor_address1'][$i];
                    $data['address2'] = $_POST['guarantor_address2'][$i];
                    $data['address3'] = $_POST['guarantor_address3'][$i];
                    $data['address4'] = $_POST['guarantor_address3'][$i];
                    $data['is_guarantor_company'] = 0;

                    $data['email1'] = $email1;
                    $data['email2'] = $email2;
                    $data['email3'] = $email3;
                    $data['guarantee_years'] = $_POST['guarantor_guarantee'][$i];
                    $data['note'] = $_POST['guarantor_note'][$i];
                    $data['entity_fid_number'] = $_POST['guarantor_ssn'][$i];


                    $sqlData = createSqlColVal($data);

                    $query = "INSERT INTO tenant_guarantor(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();


                    $k = 0; /*this is for phone data entry for particular guarantor, */

                    foreach ($_POST['guarantor_countryCode_' . $j] as $countryCode) {
                        $data1['user_id'] = $id;
                        $data1['parent_id'] = $tenant_id;
                        $data1['phone_type'] = $_POST['guarantor_phoneType_' . $j][$k];
                        $data1['carrier'] = $_POST['guarantor_carrier_' . $j][$k];
                        $data1['country_code'] = $_POST['guarantor_countryCode_' . $j][$k];
                        $data1['phone_number'] = $_POST['guarantor_phone_' . $j][$k];
                        $data1['user_type'] = 2; //2 value for guarantor

                        $sqlData1 = createSqlColVal($data1);

                        $query1 = "INSERT INTO tenant_phone(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query1);
                        $stmt1->execute($data1);
                        $k++;
                    }
                    $j++;
                    $i++;
                }
                return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_guarantor');
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_guarantor');
                printErrorLog($e->getMessage());
            }
        }
    }

    public function addCollection($tenant_id)
    {
        try {
            $data['user_id'] = $tenant_id;
            $data['collection_id'] = $_POST['collection_collectionId'];
            $data['reason'] = $_POST['collection_reason'];
            $data['description'] = $_POST['collection_description'];
            $data['status'] = $_POST['collection_status'];
            $data['amount_due'] = $_POST['collection_amountDue'];
            $data['notes'] = $_POST['collection_amountDue'];

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_collection(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);

            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_collection');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }

    }

    public function addAdditionalTenant($tenant_id = null)
    {

        //dd($_POST);

        if (isset($_POST['tenantId'])) {
            $tenant_id = $_POST['tenantId'];
        }

        try {
            if ($_POST['additionalTenantKey'] == "") {
                $additionalKey = uniqid();

            } else {
                $additionalKey = $_POST['additionalTenantKey'];

            }


            $data['user_id'] = $tenant_id;

            $data['additional_key'] = $additionalKey;
            $data['salutation'] = $_POST['additional_salutation'];
            $data['first_name'] = $_POST['additional_firstname'];
            $data['mi'] = $_POST['additional_middlename'];
            $data['last_name'] = $_POST['additional_lastname'];
            $data['maiden_name'] = $_POST['additional_maidenname'];
            $data['nick_name'] = $_POST['additional_nickname'];
            $data['gender'] = $_POST['additional_gender'];
            $data['relationship'] = $_POST['additional_relationship'];
            $data['ethnicity'] = $_POST['additional_ethncity'];
           /* if (isset($_POST['additional_hobbies'])) {
                $data['hobbies'] = serialize($_POST['additional_hobbies']);
            }*/
            $data['marital_status'] = $_POST['additional_maritalStatus'];
            $data['veteran_status'] = $_POST['additional_veteranStatus'];


            $additional_email = $_POST['additional_email'];
            $emailCount = count($additional_email);


            if ($emailCount == 3) {
                $email1 = $additional_email[0];
                $email2 = $additional_email[1];
                $email3 = $additional_email[2];
            } else if ($emailCount == 2) {
                $email1 = $additional_email[0];
                $email2 = $additional_email[1];
                $email3 = "";
            } else {
                $email1 = $additional_email[0];
                $email2 = "";
                $email3 = "";
            }
            $data['email1'] = $email1;
            $data['email2'] = $email2;
            $data['email3'] = $email3;
            $data['referral_source'] = $_POST['additional_referralSource'];
            $data['tenant_status'] = $_POST['additional_tenantStatus'];
            if (isset($_POST['additional_financallyResponsible'])) {
                $data['financial_responsible'] = 1;
            } else {
                $data['financial_responsible'] = 0;
            }


            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_additional_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);

            $id = $this->companyConnection->lastInsertId();

            $k = 0;

            // $_POST['additional_countryCode'];

            foreach ($_POST['additional_countryCode'] as $countryCode) {
                $data1['user_id'] = $id;
                $data1['parent_id'] = $tenant_id;
                $data1['phone_type'] = $_POST['additional_phoneType'][$k];
                $data1['carrier'] = $_POST['additional_carrier'][$k];
                $data1['country_code'] = $_POST['additional_countryCode'][$k];
                $data1['phone_number'] = $_POST['additional_phone'][$k];
                $data1['additional_key'] = $additionalKey;
                $data1['user_type'] = 1; //1 value for additional tenant
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');

                $sqlData1 = createSqlColVal($data1);

                $query1 = "INSERT INTO tenant_phone(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($data1);
                $k++;
            }
            $returnAdditionalEmergency = $this->addAdditionalEmergencyContact($tenant_id, $id, $additionalKey);
            if ($returnAdditionalEmergency['status'] != 'success') {
                return false;
            }

            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_additional_details for additional information', 'key' => $additionalKey, 'table' => 'all');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_phone');
        }


    }


    /*to add tenant primary information*/

    public function addAdditionalEmergencyContact($tenant_id, $additionalId, $additionalKey)
    {
        try {

            $i = 0;
            foreach ($_POST['additional_emergency'] as $credentialName) {
                if ($tenant_id == 0) {
                    $parent_id = 0;
                    $user_id = $additionalId;

                } else {
                    $parent_id = $tenant_id;
                    $user_id = $additionalId;
                }

                $data['user_id'] = $user_id;
                $data['parent_id'] = $tenant_id;
                $data['emergency_contact_name'] = $_POST['additional_emergency'][$i];
                $data['emergency_relation'] = $_POST['emergency_additional_relationship'][$i];
                $data['emergency_country_code'] = $_POST['emergency_additional_countryCode'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_additional_phoneNumber'][$i];
                $data['emergency_email'] = $_POST['additional_emergency_email'][$i];
                $data['additional_key'] = $additionalKey;

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $id = $this->companyConnection->lastInsertId();
                return array('status' => 'success', 'message' => 'Data added sSuccessfully.', 'table' => 'emergency_details');
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }


    }

    public function addEmergencyContact($tenant_id)
    {
        try {

            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $credentialName) {

                $data['user_id'] = $tenant_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $id = $this->companyConnection->lastInsertId();

            }
            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'emergency_details');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }


    }

    public function updateAdditionalKey($tenant_id)
    {
        if ($_POST['additional_key'] != "") {

            $key = $_POST['additional_key'];
            $data['user_id'] = $tenant_id;
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE tenant_additional_details SET " . $sqlData['columnsValuesPair'] . " where additional_key='$key'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            $this->updateEmergencyKey($tenant_id, $key);
            $this->updatePhoneKey($tenant_id, $key);

        }

    }

    public function updateEmergencyKey($tenant_id, $key)
    {
        $data['parent_id'] = $tenant_id;

        $sqlData = createSqlColValPair($data);

        $query = "UPDATE emergency_details SET " . $sqlData['columnsValuesPair'] . " where additional_key='$key'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }

    public function updatePhoneKey($tenant_id, $key)
    {
        $data['parent_id'] = $tenant_id;

        $sqlData = createSqlColValPair($data);

        $query = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] . " where additional_key='$key'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }


    /*to add property*/

    public function addCredentialsControl($tenant_id)
    {
        try {

            $i = 0;
            foreach ($_POST['credentialName'] as $credentialName) {
                $acquire_date = $_POST['acquireDate'][$i];
                $expireDate = $_POST['expirationDate'][$i];
                $data['user_id'] = $tenant_id;
                $data['credential_name'] = $_POST['credentialName'][$i];
                $data['credential_type'] = $_POST['credentialType'][$i];
                $data['acquire_date'] = mySqlDateFormat($acquire_date, null, $this->companyConnection);
                $data['expire_date'] = mySqlDateFormat($expireDate, null, $this->companyConnection);
                $data['notice_period'] = $_POST['noticePeriod'][$i];


                /*  return  $data; */

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt1 = $this->companyConnection->prepare($query);
                $stmt1->execute($data);

                $i++;
            }


            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'tenant_credentials');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }

    public function insertCharge()
    {
        try {

            $tenant_id = $_POST['tenant_id'];
            $data['user_id'] = $_POST['tenant_id'];
            if (isset($_POST['dailyRadio'])) {
                $data['charge_type'] = $_POST['dailyRadio'];
            }

            if (isset($_POST['oneTimeRadio'])) {
                $data['charge_type'] = $_POST['oneTimeRadio'];
            }

            if (isset($_POST['daily'])) {
                $data['apply_type'] = $_POST['daily'];
            }

            if (isset($_POST['oneTime'])) {
                $data['apply_type'] = $_POST['oneTime'];
            }

            if (!isset($_POST['lateFeeCharge'])) {
              //  return array('code' => 402, 'status' => 'failed', 'message' => "Please Fill all required fields");
            } else {
                $data['fee'] = $_POST['lateFeeCharge'];
            }


            $data['grace_period'] = $_POST['gracePeriod'];


            $userData = $this->companyConnection->query("SELECT * FROM tenant_charge WHERE user_id ='" . $tenant_id . "'")->fetch();
            if (!empty($userData)) {

                $sqlData = createSqlColValPair($data);

                $query = "UPDATE tenant_charge SET " . $sqlData['columnsValuesPair'] . " where user_id=$tenant_id";


                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('status' => 'success', 'message' => 'Record updated successfully.', 'table' => 'tenant_charge');
            } else {

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_charge(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_charge');

            }


        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => "something went wrong on tenant_charges");
            printErrorLog($e->getMessage());
        }


    }


    public function insertChargeFormData()
    {
        $data['user_id'] = $_POST['tenant_id'];
        $data['charge_code'] = $_POST['chargeCode'];
        $data['frequency'] = $_POST['frequency'];
        $data['amount'] = $_POST['amount'];
        $data['amount_due'] = $_POST['amount'];
        $data['start_date'] = mySqlDateFormat($_POST['startDate'], null, $this->companyConnection);
        $data['end_date'] = mySqlDateFormat($_POST['endDate'], null, $this->companyConnection);
        $data['record_status'] = 1;
        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO tenant_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);

        $getChargeAmount = $this->addChargeAmount($data['user_id']);
        echo $this->chargeDetails();
        exit;
        array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_charges');

    }


    public function addChargeAmount($user_id)
    {
      
      $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due FROM tenant_charges WHERE user_id ='" . $user_id . "' and frequency='One Time'")->fetch();

    if($getCharges['total_amount']=='')
      {
        $totalAmount = 0;
        $data['total_amount'] = $totalAmount;
      }
      else
      {
        $totalAmount = $getCharges['total_amount'];
        $data['total_amount'] = $totalAmount;
      }


    if($getCharges['amount_paid']=='')
      {
        $amount_paid = 0;
        $data['total_amount_paid'] = $amount_paid;
      }
      else
      {
        $amount_paid = $getCharges['amount_paid'];
        $data['total_amount_paid'] = $amount_paid;
      }

       if($getCharges['amount_refunded']=='')
      {
        $amount_refunded = 0;
         $data['total_refunded_amount'] = $amount_refunded;
      }
      else
      {
        $amount_refunded = $getCharges['amount_refunded'];
         $data['total_refunded_amount'] = $amount_refunded;
      }
       $data['created_at'] = date('Y-m-d H:i:s');
       $data['updated_at'] = date('Y-m-d H:i:s');

      $checkManageCharge  = $this->companyConnection->query("SELECT *  FROM accounting_manage_charges WHERE user_id ='" . $user_id . "'")->fetch();
      if(empty($checkManageCharge))
      {
         $data['total_due_amount'] = $totalAmount;
         $data['user_id'] = $user_id;
          $sqlData = createSqlColVal($data);
           $query = "INSERT INTO accounting_manage_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
      }
      else
      {
        $data['user_id'] = $user_id;
         $data['total_due_amount'] = $getCharges['amount_due'];
        $sqlData = createSqlColValPair($data);

            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $user_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
      }

   
    }

    public function chargeDetails()
    {
        $tenant_id = $_REQUEST['tenant_id'];
        $getChargeList = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE user_id ='" . $tenant_id . "'")->fetchAll();
$default_symbolCurrency = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

        $html = '';
       

        $html .= "<form id='editChargeDateAmount'>";
      
     
       
        $html .= "<table border= '1px'>";
        $html .= "<tr>
  <th>Charge</th>
  <th>Frequency</th>
  <th>Start Date</th>
  <th>End Date</th>
  <th>Amount(".$default_symbolCurrency.")</th>
            </tr>";

        foreach ($getChargeList as $list) {

            $id = $list['id'];
            $charge = $this->getChargeCode($list['charge_code']);
            $frequency = $list['frequency'];
            $start_date = $list['start_date'];
            $startDate = dateFormatUser($start_date, null, $this->companyConnection);


            $end_date = $list['end_date'];
            $endDate = dateFormatUser($end_date, null, $this->companyConnection);

            $amount = $list['amount'];
            $amount = number_format($amount,2);


            $html .= "<input type='hidden' name='chargeId[]' value='$id'>";
            $html .= "<tr>";
            $html .= "<td>" . $charge . "</td>";
            $html .= "<td>" . $frequency . "</td>";
            $html .= "<td>" . $startDate . "</td>";
            $html .= "<td><div class='endDate_$id'><input type='hidden' name='editDate[]'><a href='JavaScript:Void(0);' class='editEndDate' data-id='" . $id . "'>" . $endDate . "<i class='fas fa-highlighter'></i></a></div></td>";
            $html .= "<td><div class='endAmount_$id'><input type='hidden' name='editAmount[]'><a href='JavaScript:Void(0);' class='editAmount amount_$id' data-id='" . $id . "'>" . $amount . "<i class='fas fa-highlighter'></i></a></div></td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";
        if (!empty($getChargeList)) {
            $html .= "<input type='button' class='blue-btn editChargeInfo' value='Apply'>";

        }

        


        echo $html;
        exit;

    }





    public function getChargeCode($codeId)
    {
        $chargeCode = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE id ='" . $codeId . "'")->fetch();
        return $chargeCode['charge_code'];

    }

    public function insertTaxDetails()
    {
        try {

            $data = $_POST;
            $tenant_id = $_POST['tenant_id'];
            $data['user_id'] = $_POST['tenant_id'];
            unset($data['action']);
            unset($data['class']);
            unset($data['tenant_id']);

            $userData = $this->companyConnection->query("SELECT * FROM tenant_taxdetails WHERE user_id ='" . $tenant_id . "'")->fetch();
            if (!empty($userData)) {

                $sqlData = createSqlColValPair($data);

                $query = "UPDATE tenant_taxdetails SET " . $sqlData['columnsValuesPair'] . " where user_id=$tenant_id";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('status' => 'success', 'message' => 'Record update successfully.', 'table' => 'tenant_TaxDetails');
            } else {

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_taxdetails(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_TaxDetails');

            }


        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => "something went wrong on tenant_charges");
            printErrorLog($e->getMessage());
        }

    }

    public function insertChargeFileNote()
    {
        $tenant_id = $_POST['tenant_id'];
        if (isset($_POST['chargeNote'])) {
            $chargeNote = $_POST['chargeNote'];
            $i = 0;
            foreach ($chargeNote as $note) {
                if ($note != "") {
                    $data['user_id'] = $tenant_id;
                    $data['note'] = $note;
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                $i++;
            }
        }
        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $tenant_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }
        return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargenote');
    }

    public function editChargeInfo()
    {
        $chargeId = $_POST['chargeId'];

        $i = 0;
        try {
            foreach ($chargeId as $id) {
                if ($_POST['editDate'][$i] != "") {
                    $editDate = $_POST['editDate'][$i];
                    $data['end_date'] = mySqlDateFormat($editDate, null, $this->companyConnection);
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] . " where id=$id";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                }

                if ($_POST['editAmount'][$i] != "") {

                    $data1['amount'] = $_POST['editAmount'][$i];
                    $sqlData1 = createSqlColValPair($data1);
                    $query1 = "UPDATE tenant_charges SET " . $sqlData1['columnsValuesPair'] . " where id=$id";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute();
                }


                $i++;
            }
            echo $this->chargeDetails();
            exit;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => "something went wrong on tenant_charges");
            printErrorLog($e->getMessage());
        }

    }


    public function chargeCode()
    {
        $getCharge = $this->companyConnection->query("SELECT id,charge_code FROM company_accounting_charge_code")->fetchAll();
        $html = "";
        foreach ($getCharge as $charge) {
            $id = $charge['id'];
            $chargeCode = $charge['charge_code'];
            $html .= "<option value='$id'>" . $chargeCode . "</option>";

        }

        echo $html;
        exit;


    }

    public function deleteUnsaveRecord($table, $id)
    {
        $query = "DELETE from $table WHERE id = $id";
        $stmt = $this->companyConnection->prepare($query);
        $return = $stmt->execute();
        return $return;
    }

    public function getContact(){
        $query1 = $this->companyConnection->query("SELECT name,id from users WHERE user_type='5' ORDER BY name ASC" );
        $user1 = $query1->fetchAll();

        return ['code'=>200, 'status'=>'success', 'data'=>$user1];
    }
    public function AddContact(){
        try {
           $formData=$_POST['form_data'];
            $formData= postArray($formData);
            $formData['user_type']='5';
            $formData['name']=$formData['first_name'].' '.$formData['last_name'];

            $required_array = ['first_name'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($formData,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColVal($formData);
                $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($formData);
                $last_id=$this->companyConnection->lastInsertId();
                return array('code' => 200,'data'=>$last_id, 'status' => 'success', 'message' => 'Records updated successfully!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function VendorPaymentStep1($id,$card,$cvv,$year,$month)
    {
        try{
            $customer_id = $this->createCustomerNew($id);
            $request['customer_id']=$customer_id;
            $createdData=createCard($card,$cvv,$year,$month,$request);
            return $createdData;
        }catch(\Stripe\Error\RateLimit $e){
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];
            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];
            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;
        } catch (\Stripe\Error\Authentication $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        } catch (\Stripe\Error\Base $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";
            $body = $e->getJsonBody();
            $err  = $body['error'];
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }
    }
    function createCustomerNew($user_id)
    {

        $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email['email'] = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer = createCustomer($email);
            $customer_id = $customer['account_data'];
            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";
           /* dd($upateData);*/
            
            $stmt = $this->companyConnection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }


  public function getContantData()
  {
    if(isset($_POST['contact_id']))
    {
      $contact_id = $_POST['contact_id'];
      $getInfo = $this->getContactInfo($contact_id);
      $ssn_id = '';
      $salutation='Mr.';
      $ssnArry = array();
      if(!empty($getInfo['ssn_sin_id']))
      {
        $ssnArry = unserialize($getInfo['ssn_sin_id']);
        $ssn_id =  $ssnArry[0];

      }

      if($getInfo['salutation']=='1')
      {
         $salutation = 'Dr.';
      }
      else  if($getInfo['salutation']=='2')
      {
          $salutation = 'Mr.';
      }
      else  if($getInfo['salutation']=='3')
      {
          $salutation = 'Mrs.';
      }
      else  if($getInfo['salutation']=='4')
      {
          $salutation = 'Mr. & Mrs.';
      }
      else  if($getInfo['salutation']=='5')
      {
          $salutation = 'Ms.';
      }
      else  if($getInfo['salutation']=='6')
      {
          $salutation = 'Sir';
      }
      else  if($getInfo['salutation']=='7')
      {
          $salutation = 'Madam';
      }
      else  if($getInfo['salutation']=='10')
      {
          $salutation = 'Brother';
      }
      else  if($getInfo['salutation']=='8')
      {
          $salutation = 'Sister';
      }
      else  if($getInfo['salutation']=='11')
      {
          $salutation = 'Father';
      }
      else  if($getInfo['salutation']=='9')
      {
          $salutation = 'Mother';
      }

      return array('status'=>'true','data'=>$getInfo,'ssn_id'=>$ssn_id,'salutation'=>$salutation);

    }
  }

  public function getContactInfo($contact_id)
  {
   $data = $this->companyConnection->query("SELECT ssn_sin_id,salutation,first_name,last_name,middle_name,nick_name,maiden_name,company_name,address1,address2,address3,address4,zipcode,city,state,country,email FROM users WHERE id ='".$contact_id."'")->fetch();
   return $data;


  }


}

$UnitTypeAjax = new tenantAjax();