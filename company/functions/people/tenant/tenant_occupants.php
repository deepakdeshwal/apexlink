<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class TenantOccupantsAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function getEmergencyDetails(){
        $tenant_id=$_POST['tenant_id'];
        $query1 = $this->companyConnection->query("SELECT * from emergency_details where parent_id=".$tenant_id);
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
    }

    public function addOccupant(){
        try {

            $data = $_POST['formData'];
            $data= postArray($data);
            $data1['salutation']= $data['salutation_occupancy'];
            $data1['first_name']= $data['firstname_occupancy'];
            $data1['mi']= $data['middlename_occupancy'];
            $data1['last_name']= $data['lastname_occupancy'];
            $data1['maiden_name']= $data['medianName_occupancy'];
            $data1['nick_name']= $data['nickname_occupancy'];
            $data1['gender']= $data['gender_occupancy'];
            $data1['ethnicity']= $data['ethncity_occupancy'];
            $data1['email1']= $data['email_occupancy'];
            $data1['marital_status']= $data['maritalStatus_occupancy'];
            $data1['veteran_status']= $data['edit_general_veteran_occupancy'];
            $data1['relationship']= $data['rela_occupancy'];
            $data1['referral_source']= $data['referal_occupancy'];
            $data1['user_id']=$_POST['tenant_id'];
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');

            if(isset($data['responsible_occupants']) && $data['responsible_occupants'] =="1") {
                $data1['financial_responsible'] = $data['responsible_occupants'];
            }
            //Required variable array
            $required_array = ['first_name','last_name'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if ($data['hidden_id'] == "") {
                    /*INSERT CASE*/
                    $sqlData = createSqlColVal($data1);
                    $query = "INSERT INTO tenant_additional_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data1);
                    $last_add_id = $this->companyConnection->lastInsertId();
                    $data2['ssn'] = $data['sinn_occupancy'];
                    $data2['user_id'] = $last_add_id;
                    $data2['parent_id'] = $_POST['tenant_id'];
                    $data2['created_at'] = date('Y-m-d H:i:s');
                    $data2['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data2);

                    $query1 = "INSERT INTO tenant_ssn_id (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($data2);

                    $data3['hobby'] = $data['edit_general_hobby_occupancy'];
                    $data3['user_id'] = $last_add_id;
                    $data3['user_type'] = '1';
                    $data3['created_at'] = date('Y-m-d H:i:s');
                    $data3['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data3);
                    $query2 = "INSERT INTO tenant_hobbies (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt2 = $this->companyConnection->prepare($query2);
                    $stmt2->execute($data3);

                    $tenant_id = $_POST['tenant_id'];
                    $data4['emergency_contact_name'] = $data['contact_emer_occupancy'];
                    $data4['emergency_relation'] = $data['rela_emer_occupancy'];
                    $data4['emergency_country_code'] = $data['countryCode_emer_occupancy'];
                    $data4['emergency_phone_number'] = $data['ph_emer_occupancy'];
                    $data4['emergency_email'] = $data['email_emer_occupancy'];
                    $data4['created_at'] = date('Y-m-d H:i:s');
                    $data4['updated_at'] = date('Y-m-d H:i:s');
                    $data4['user_id'] = $last_add_id;
                    $data4['parent_id'] = $tenant_id;
                    $sqlData = createSqlColVal($data4);
                    $query3 = "INSERT INTO emergency_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt3 = $this->companyConnection->prepare($query3);
                    $stmt3->execute($data4);

                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records inserted successfully!');
                } else {

                    /*UPDATE CASE*/
                    $upd_id=$data['hidden_id'];
                    $data1['salutation']= $data['salutation_occupancy'];
                    $data1['first_name']= $data['firstname_occupancy'];
                    $data1['mi']= $data['middlename_occupancy'];
                    $data1['last_name']= $data['lastname_occupancy'];
                    $data1['maiden_name']= $data['medianName_occupancy'];
                    $data1['nick_name']= $data['nickname_occupancy'];
                    $data1['gender']= $data['gender_occupancy'];
                    $data1['ethnicity']= $data['ethncity_occupancy'];
                    $data1['marital_status']= $data['maritalStatus_occupancy'];
                    $data1['veteran_status']= $data['edit_general_veteran_occupancy'];
                    $data1['relationship']= $data['rela_occupancy'];
                    $data1['email1']= $data['email_occupancy'];
                    $data1['referral_source']= $data['referal_occupancy'];
                    $data1['created_at'] =  date('Y-m-d H:i:s');
                    $data1['updated_at'] =  date('Y-m-d H:i:s');
                    if(isset($data['responsible_occupants']) && $data['responsible_occupants'] =="1") {
                        $data1['financial_responsible'] = $data['responsible_occupants'];
                    }
                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE tenant_additional_details SET ".$sqlData['columnsValuesPair']." where id=".$upd_id ;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                    $data2['ssn'] = $data['sinn_occupancy'];
                    $data2['created_at'] = date('Y-m-d H:i:s');
                    $data2['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data2);
                    $query1 = "UPDATE tenant_ssn_id SET ".$sqlData['columnsValuesPair']." where user_id=".$upd_id;
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute();

                    $data3['hobby'] = $data['edit_general_hobby_occupancy'];
                    $data3['created_at'] = date('Y-m-d H:i:s');
                    $data3['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColValPair($data3);
                    $query2 = "UPDATE tenant_hobbies SET ".$sqlData['columnsValuesPair']." where user_id=".$upd_id ;
                    $stmt2 = $this->companyConnection->prepare($query2);
                    $stmt2->execute();

                    $data4['emergency_contact_name'] = $data['contact_emer_occupancy'];
                    $data4['emergency_relation'] = $data['rela_emer_occupancy'];
                    $data4['emergency_country_code'] = $data['countryCode_emer_occupancy'];
                    $data4['emergency_phone_number'] = $data['ph_emer_occupancy'];
                    $data4['emergency_email'] = $data['email_emer_occupancy'];
                    $data4['created_at'] = date('Y-m-d H:i:s');
                    $data4['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data4);
                    $query3 = "UPDATE emergency_details SET ".$sqlData['columnsValuesPair']." where user_id=".$upd_id ;
                    $stmt3 = $this->companyConnection->prepare($query3);
                    $stmt3->execute();

                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records updated successfully!');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getoccupants(){
        $tenant_id=$_POST['tenant_id'];
        $id=$_POST['id'];
        $query1 = $this->companyConnection->query("SELECT * from tenant_additional_details where id=".$id);
        $user1 = $query1->fetch();
        $query2 = $this->companyConnection->query("SELECT ssn from tenant_ssn_id where user_id=".$id." and parent_id=".$tenant_id);
        $user2 = $query2->fetch();
        $query3 = $this->companyConnection->query("SELECT hobby from tenant_hobbies where user_id=".$id);
        $user3 = $query3->fetch();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1,'ssn'=>$user2,'hob'=>$user3]];
    }

    public function decativeOccupant() {
        try {
            $data=[];
            $id=$_POST['id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_additional_details SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function addNotes(){
        try {

            $data = $_POST['formData'];
            $data= postArray($data);
            $loop=$data['notes_text'];
            $count=(count($loop));
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                for($i=0;$i<$count;$i++) {
            if($data['notes_text'][$i]!="") {

                $data1['notes']=$data['notes_text'][$i];
                $data1['user_id']=$_POST['tenant_id'];
                $data1['type']='1';
                $data1['record_status']='1';
                $data1['created_at'] =  date('Y-m-d H:i:s');
                $data1['updated_at'] =  date('Y-m-d H:i:s');
            /*INSERT CASE*/
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
                }
                    }
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records inserted successfully!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
   }

$TenantOccupantsAjax = new TenantOccupantsAjax();

