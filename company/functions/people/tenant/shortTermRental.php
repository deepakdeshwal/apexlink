<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");

class TenantShortTermAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function searchingProp(){
        $query1 = $this->companyConnection->query("SELECT id,property_name from general_property where is_short_term_rental='1'");
        $user1 = $query1->fetchAll();
        return ['code' => 200, 'status' => 'success', 'data' => ['prop' => $user1]];
    }
    public function getunitamount(){
        $start_date=mySqlDateFormat($_POST['from_date'],NULL,$this->companyConnection);
        $to_date=mySqlDateFormat($_POST['to_date'],NULL,$this->companyConnection);
        $frequency1=$_POST['frequency1'];
        $reoccurance_pattern=$_POST['reoccurance_pattern'];
        if($frequency1=='2') {
            if($reoccurance_pattern=='1'){
                $value= $this->
                getdatesdaily();
            }
            if($reoccurance_pattern=='2'){
                $value= $this->getdatesweekly();
            }
            if($reoccurance_pattern=='3'){
                $value= $this->getdatesmonthly();
            }
        }else{
            $value = $this->getDatesFromRange($start_date, $to_date);
        }
        $id=$_POST['id'];
        $query1 = $this->companyConnection->query("SELECT base_rent from unit_details where id=".$id);
        $rent = $query1->fetch();
        //print_r($rent['market_rent']);die;
        return $rent['base_rent']*count($value);
    }
    public function searchingBuilding(){
        $id=$_POST['id'];
        $query1 = $this->companyConnection->query("SELECT id,building_name from building_detail where property_id=".$id);
        $user1 = $query1->fetchAll();
        return ['code' => 200, 'status' => 'success', 'data' => ['prop' => $user1]];
    }
    public function searchingUnit(){
        $id=$_POST['id'];
        $query1 = $this->companyConnection->query("SELECT id,unit_prefix,unit_no from unit_details where building_unit_status='1' AND building_id=".$id);
        $user1 = $query1->fetchAll();
        return ['code' => 200, 'status' => 'success', 'data' => ['prop' => $user1]];
    }
    public function savePersonalInformation(){
        try {
            //dd($_POST['image']);
            $data = $_POST['formData'];
            $data= postArray($data);
            $formData2=$_POST['formData2'];
            $formData2= postArray($formData2);
            //dd($formData2);
            $data1['salutation'] = $data['salutation'];
            $data1['first_name'] = $data['first_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name']." ". $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['nick_name'] = $data['nickname'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['company_name'] = $data['comapany_name'];
            //dd($data['comapany_name']);
            if(isset($data['comapany_name']) && !empty($data['comapany_name'])){
                $data1['name'] = $data['comapany_name'];
            }
            $data1['phone_number_note'] = $data['notePhone'];
            $data1['referral_source'] = $data['referralSource'];
            $data1['ethnicity'] = $data['ethncity'];
            $data1['maritial_status'] = $data['maritalStatus'];
            $data1['veteran_status'] = $data['veteranStatus'];
            if(isset($data['ssn_id']) && !empty($data['ssn_id'])) {
                $data1['ssn_sin_id'] = serialize($data['ssn_id']);
            } else{
                $data1['ssn_sin_id'] = null;
            }
            $data1['user_type'] = '9';
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                        $sqlData = createSqlColVal($data1);
                        $query1 = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query1);
                        $stmt1->execute($data1);
                        $last_id=$this->companyConnection->lastInsertId();
                $this->savebookinginfo($last_id);

                $user_id['user_id'] = $last_id;
                //$user_id['image'] = $image;
                $sqlData = createSqlColVal($user_id);
                $user = "INSERT INTO short_term_rental (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($user);
                $stmt1->execute($user_id);
                $this->checkAvailability($last_id, $formData2);
                if(is_array($data['email'])){
                    for($i=1;$i<count($data['email']);$i++) {
                        $data2['email'] = $data['email'][0];
                        $sqlData = createSqlColValPair($data2);
                        $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query2);
                        $stmt->execute();
                        $n=$i+1;
                        $data3['email'.$n] = $data['email'][$i];
                        $data3['user_id'] = $last_id;
                        $sqlData = createSqlColValPair($data3);
                        $query3 = "UPDATE short_term_rental SET " . $sqlData['columnsValuesPair'] . " where user_id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query3);
                        $stmt->execute();
                    }
                        }
                else{
                    $data2['email'] = $data['email'];
                    $sqlData = createSqlColValPair($data2);
                    $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query2);
                    $stmt->execute();
                }
                if(is_array($data['phone_type'])) {
                    $data4['phone_type'] = $data['phone_type'][0];
                 //   dd($data['phone_type']);
                    $data4['country'] = $data['country'][0];
                    $data4['carrier'] = $data['carrier'][0];
                    $data4['phone_number'] = $data['phone_number'][0];
                    $sqlData = createSqlColValPair($data4);
                    $query5 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query5);
                    $stmt->execute();
                    for($i=1;$i<count($data['phone_type']);$i++) {
                        $data5['phone_type'] = $data['phone_type'][$i];
                        $data5['country_code'] = $data['country'][$i];
                        $data5['carrier'] = $data['carrier'][$i];
                        $data5['phone_number'] = $data['phone_number'][$i];
                        $data5['user_id'] = $last_id;
                        $sqlData = createSqlColVal($data5);
                        $query6 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query6);
                        $stmt1->execute($data5);
                    }
                }
                else{
                    $data6['phone_type'] = $data['phone_type'];
                    $data6['country'] = $data['country'];
                    $data6['carrier'] = $data['carrier'];
                    $data6['phone_number'] = $data['phone_number'];
                    $sqlData = createSqlColValPair($data6);
                    $query7 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query7);
                    $stmt->execute();
                }
                if(isset($data['hobbies'])) {
                    if (is_array($data['hobbies'])) {
                        $data8['hobbies'] = $data['hobbies'][0];
                        $sqlData = createSqlColValPair($data8);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        for ($i = 1; $i < count($data['hobbies']); $i++) {
                            $data9['hobby'] = $data['hobbies'][$i];
                            $data9['user_id'] = $last_id;
                            $sqlData = createSqlColVal($data9);
                            $query10 = "INSERT INTO tenant_hobbies (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query10);
                            $stmt1->execute($data9);
                        }
                    }
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Records updated successfully!');
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function checkAvailability($last_id)
    {
        $prop_id=$_POST['prop_id'];
        $build_id=$_POST['build_id'];
        $unit_id=$_POST['unit_id'];
            $check['property_id'] = $prop_id;
            $check['building_id'] = $build_id;
            $check['unit_id'] = $unit_id;
            $check['frequency'] = $_POST['frequency1'];
            if(isset($_POST['image'])){
                $check['image']=$_POST['image'];
            }
            $check['end_date'] = mySqlDateFormat($_POST['to_date'],NULL,$this->companyConnection);
            $sqlData = createSqlColValPair($check);
            $query12 = "UPDATE short_term_rental SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $last_id;
            $stmt = $this->companyConnection->prepare($query12);
            $stmt->execute();

    }
    public function checkbookingavail()
    {
        $unit_id=$_POST['unit_id'];
        $start_date=mySqlDateFormat($_POST['from_date'],NULL,$this->companyConnection);
        $to_date=mySqlDateFormat($_POST['to_date'],NULL,$this->companyConnection);
        $frequency=$_POST['frequency'];
        $frequency1=$_POST['frequency1'];
        $reoccurance_pattern=$_POST['reoccurance_pattern'];
        if($frequency1=='2') {
            if($reoccurance_pattern=='1'){
                $value= $this->getdatesdaily();
                $value = implode("','", $value);
                $value = "'" . $value . "'";
            }
            if($reoccurance_pattern=='2'){
                $value= $this->getdatesweekly();
                $value = implode("','", $value);
                $value = "'" . $value . "'";
            }
            if($reoccurance_pattern=='3'){
                $value= $this->getdatesmonthly();
                $value = implode("','", $value);
                $value = "'" . $value . "'";
            }
        }else{
            $value = $this->getDatesFromRange($start_date, $to_date);
            $value = implode("','", $value);
            $value = "'" . $value . "'";
        }
        $query1 = $this->companyConnection->query("SELECT `date` from short_term_rental_booking_dates WHERE `date` IN (".$value.") AND unit_booked_id=".$unit_id);
        $user1 = $query1->fetchAll();
        if(!empty($user1)) {
            $date = [];
            for ($i = 0; $i < count($user1); $i++) {
                array_push($date, dateFormatUser($user1[$i]['date'], NULL, $this->companyConnection));
            }
        }else{
            $date="null";
        }
        return $date;
    }
    public function goGreen()
    {
        //$unit_id=$_POST['unit_id'];
        $start_date=mySqlDateFormat($_POST['from_date'],NULL,$this->companyConnection);
        $to_date=mySqlDateFormat($_POST['to_date'],NULL,$this->companyConnection);
        $frequency=$_POST['frequency'];
        $frequency1=$_POST['frequency1'];
        $reoccurance_pattern=$_POST['reoccurance_pattern'];
        if($frequency1=='2') {
            if($reoccurance_pattern=='1'){
                $value= $this->getdatesdaily();
            }
            if($reoccurance_pattern=='2'){
                $value= $this->getdatesweekly();
            }
            if($reoccurance_pattern=='3'){
                $value= $this->getdatesmonthly();
            }
        }else{
            $value = $this->getDatesFromRange($start_date, $to_date);
        }
        //dd($value);

        return $value;
    }


    public function geteditdata(){

        $id=$_POST['id'];

        $query = $this->companyConnection->query("SELECT u.id,u.salutation,u.first_name,u.middle_name,u.last_name,
                                                 u.maiden_name,u.nick_name,u.hobbies,u.phone_number,u.email,u.ssn_sin_id,u.phone_number_note,u.phone_type,u.country as country_user,
                                                 pt.id as phone_typ,pt.type,ca.id as career,ca.carrier,co.id as country,co.name,co.code,trs.id as referal_source,trs.referral,
                                                 te.id as ethnicity,te.title,tvs.id as vet_status,tvs.veteran,tms.id as marital_status,tms.marital,str.email2,str.email3,str.image,
                                                tp.phone_number as phone_numberr,ptt.type as typee,caa.carrier as carrierr, coo.code as codee
                                                   from users u
                                                 left join phone_type pt on pt.id=u.phone_type
                                                 left join carrier ca on ca.id=u.carrier
                                                 left join countries co on co.id=country_code
                                                 left join tenant_referral_source trs on trs.id=u.referral_source
                                                 left join tenant_ethnicity te on te.id=u.ethnicity
                                                 left join tenant_veteran_status tvs on tvs.id=u.veteran_status
                                                 left join tenant_marital_status tms on tms.id=u.maritial_status
                                                 left join short_term_rental str on str.user_id=u.id
                                                 left join tenant_phone tp on tp.user_id=u.id
                                                 left join phone_type ptt on ptt.id=tp.phone_type
                                                 left join carrier caa on caa.id=tp.carrier
                                                 left join countries coo on coo.id=tp.country_code
                                                 where u.id=".$id);

                                        $data = $query->fetchAll();
                                        //dd(unserialize($data[0]['ssn_sin_id']));

                                        $data[0]['ssn_sin_id']=unserialize($data[0]['ssn_sin_id']);

                 return ['code' => 200, 'status' => 'success', 'data' => $data];
            }
public function cancelbooking(){
        $user_id=$_POST['user_id'];
    $query = "DELETE from short_term_rental WHERE user_id =". $user_id;
    $stmt = $this->companyConnection->prepare($query);
    $return = $stmt->execute();
    $query = "DELETE from short_term_rental_booking_dates WHERE user_id =".$user_id;
    $stmt = $this->companyConnection->prepare($query);
    $return1 = $stmt->execute();
    return "Booking cancelled successfully";
       // dd($user_id);
}



 public function update_genInformation(){
        try {
            $id=$_POST['id'];
           // dd($id);
            $data = $_POST['formData'];
            $data= postArray($data);
            //dd($data);
            $data1['salutation'] = $data['salutation'];
            $data1['first_name'] = $data['first_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name']." ". $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['nick_name'] = $data['nickname'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['company_name'] = $data['comapany_name'];
            $data1['phone_number_note'] = $data['notePhone'];
            $data1['referral_source'] = $data['referralSource'];
            $data1['ethnicity'] = $data['ethncity'];
            $data1['maritial_status'] = $data['maritalStatus'];
            $data1['veteran_status'] = $data['veteranStatus'];
            if(isset($data['ssn_id']) && !empty($data['ssn_id'])) {
                $data1['ssn_sin_id'] = serialize($data['ssn_id']);
            } else{
                $data1['ssn_sin_id'] = null;
            }
            $data1['updated_at'] =  date('Y-m-d H:i:s');

            if(is_array($data['phone_typpe'])) {
                $data4['phone_type'] = $data['phone_typpe'][0];
                //   dd($data['phone_type']);
                $data4['country'] = $data['country'][0];
                $data4['carrier'] = $data['carriers'][0];
                $data4['phone_number'] = $data['phone_number'][0];
                $sqlData = createSqlColValPair($data4);
                $query5 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
                $stmt = $this->companyConnection->prepare($query5);
                $stmt->execute();
                for($i=1;$i<count($data['phone_typpe']);$i++) {
                    $data5['phone_type'] = $data['phone_typpe'][$i];
                    $data5['country_code'] = $data['country'][$i];
                    $data5['carrier'] = $data['carriers'][$i];
                    $data5['phone_number'] = $data['phone_number'][$i];
                    $data5['user_id'] = $id;
                    $sqlData = createSqlColValPair($data5);
                    $query6 = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] . " where user_id=".$id;
                    $stmt = $this->companyConnection->prepare($query6);
                    $stmt->execute();
                }
            }
            else{
                $data6['phone_type'] = $data['phone_typpe'];
                $data6['country'] = $data['country'];
                $data6['carrier'] = $data['carriers'];
                $data6['phone_number'] = $data['phone_number'];
                $sqlData = createSqlColValPair($data6);
                $query7 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
                $stmt = $this->companyConnection->prepare($query7);
                $stmt->execute();
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                        $sqlData = createSqlColValPair($data1);
                        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'message' => 'Records updated successfully!');
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function savebookinginfo($last_id){
        $frequency=$_POST['frequency1'];
        if($frequency=='2'){
        $reoccurance_pattern=$_POST['reoccurance_pattern'];
        if($reoccurance_pattern=="1"){
           $array1=$this->getdatesdaily();
            for($i=0;$i<count($array1);$i++){
                $data['date']=$array1[$i];
                $data['user_id']=$last_id;
                $data['unit_booked_id']=$_POST['unit_id'];
                $sqlData = createSqlColVal($data);
                $query10 = "INSERT INTO short_term_rental_booking_dates (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query10);
                $stmt1->execute($data);
            }
        }
            elseif($reoccurance_pattern=="2"){
                $insert_val=$this->getdatesweekly();
                for($p=0;$p<count($insert_val);$p++){
                    $data['date']=$insert_val[$p];
                    $data['user_id']=$last_id;
                    $data['unit_booked_id']=$_POST['unit_id'];
                    $sqlData = createSqlColVal($data);
                    $query10 = "INSERT INTO short_term_rental_booking_dates (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query10);
                    $stmt1->execute($data);
                }
            }
        elseif($reoccurance_pattern=="3"){
            $insert=$this->getdatesmonthly();
            for($p=0;$p<count($insert);$p++){
                $data['date']=$insert[$p];
                $data['user_id']=$last_id;
                $data['unit_booked_id']=$_POST['unit_id'];
                $sqlData = createSqlColVal($data);
                $query10 = "INSERT INTO short_term_rental_booking_dates (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query10);
                $stmt1->execute($data);
            }
        }
        }else{
            $start_date=mySqlDateFormat($_POST['from_date'],NULL,$this->companyConnection);
            $to_date=mySqlDateFormat($_POST['to_date'],NULL,$this->companyConnection);
            $value=$this->getDatesFromRange($start_date,$to_date);
            for($i=0;$i<count($value);$i++){
                $data['date']=$value[$i];
                $data['user_id']=$last_id;
                $data['unit_booked_id']=$_POST['unit_id'];
                $sqlData = createSqlColVal($data);
                $query10 = "INSERT INTO short_term_rental_booking_dates (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query10);
                $stmt1->execute($data);
            }
        }
    }


    function getDatesFromRange($start, $end, $format = 'Y-m-d') {

        // Declare an empty array
        $array = array();

        // Variable that store the date interval
        // of period 1 day
        $interval = new DateInterval('P1D');


        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        // Use loop to store date into array
        foreach($period as $date) {
            $array[] = $date->format($format);
        }

        // Return the array elements
        return $array;
    }
    function getAllDaysInAMonth($year, $month, $day = 'Monday', $daysError = 3) {
        $dateString = 'first '.$day.' of '.$year.'-'.$month;

        if (!strtotime($dateString)) {
            throw new \Exception('"'.$dateString.'" is not a valid strtotime');
            $startDay = new \DateTime($dateString);

            if ($startDay->format('j') > $daysError) {
                $startDay->modify('- 7 days');
            }
            $days = array();
            while ($startDay->format('Y-m') <= $year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT)) {
                $days[] = clone($startDay);
                $startDay->modify('+ 7 days');
            }

            return $days;
            }
        }

    public function getunitdata(){

        $query = $this->companyConnection->query("SELECT ud.unit_no,ud.unit_prefix,ud.id,ud.base_rent,bd.building_name,gp.property_name from unit_details ud JOIN building_detail bd on ud.building_id=bd.id JOIN general_property gp ON ud.property_id=gp.id WHERE gp.is_short_term_rental='1' ORDER BY id ASC");
        $data = $query->fetchAll();
        $data1=count($data);

        return ['code' => 200, 'status' => 'success', 'data' => $data,'count'=>$data1];
    }

    public function getdaysremaining(){
        $date=$_POST['date'];
        $id=$_POST['id'];
        $user_id=$_POST['user_id'];
       // dd($date);
        $query = $this->companyConnection->query("SELECT count(strb.date) as total FROM short_term_rental_booking_dates strb WHERE '$date' < strb.date and strb.unit_booked_id='$id' and strb.user_id='$user_id'");
        $data = $query->fetchAll();
     //   dd($data);
        return ['code' => 200, 'status' => 'success', 'data' => $data];
    }

    public function getcalendardata(){

        $query = $this->companyConnection->query("SELECT u.name,strb.date, ud.id,ud.unit_prefix,ud.unit_no FROM short_term_rental_booking_dates strb JOIN users u ON u.id=strb.user_id JOIN unit_details ud ON ud.id=strb.unit_booked_id");
        $data = $query->fetchAll();

        return ['code' => 200, 'status' => 'success', 'data' => $data];
    }

    public function getsearchdata(){

        $start=$_POST['st_date'];
        $start = mySqlDateFormat($start,null,$this->companyConnection);
        $end=$_POST['end_date'];
        $end = mySqlDateFormat($end,null,$this->companyConnection);
        $query = $this->companyConnection->query("SELECT ud.unit_no,ud.unit_prefix,ud.id,ud.base_rent,bd.building_name,DATE_FORMAT(ud.created_at,'%Y-%m-%d'),gp.property_name from unit_details ud JOIN building_detail bd on ud.building_id=bd.id JOIN general_property gp ON ud.property_id=gp.id WHERE gp.is_short_term_rental='1' AND DATE_FORMAT(ud.created_at,'%Y-%m-%d') BETWEEN '$start' and '$end' ORDER BY id ASC");
      //dd($query);
        $data = $query->fetchAll();
        $data1=count($data);
        $total_ids=[];
        foreach ($data as $key=>$val){
            array_push($total_ids,$val['id']);
        }
        $total_ids=implode("','",$total_ids);
        $total_ids= "'".$total_ids."'";

        $query1 = $this->companyConnection->query("SELECT u.name,strb.date, ud.id,ud.unit_prefix,ud.unit_no FROM short_term_rental_booking_dates strb JOIN users u ON u.id=strb.user_id JOIN unit_details ud ON ud.id=strb.unit_booked_id where ud.id IN($total_ids) ");
        $data2 = $query1->fetchAll();

        return ['code' => 200, 'status' => 'success', 'data' => $data,'count'=>$data1,'data2'=>$data2];
    }

    public function getinputdata(){
        $a=$_POST['a'];
        $data=[];
        $data2=[];
        $data1="";
        if($a != "") {
            $query = $this->companyConnection->query("SELECT ud.unit_no,ud.unit_prefix,ud.id,ud.base_rent,bd.building_name,gp.property_name from unit_details ud JOIN building_detail bd on ud.building_id=bd.id JOIN general_property gp ON ud.property_id=gp.id WHERE gp.is_short_term_rental='1' AND (gp.property_name LIKE '%$a%' OR bd.building_name LIKE '%$a%' OR ud.unit_no LIKE '%$a%' OR ud.base_rent LIKE '%$a%')");
            //dd($query);
            $data = $query->fetchAll();
            $data1 = count($data);
          //  dd($data);
            $total_ids=[];
            foreach ($data as $key=>$val){
                array_push($total_ids,$val['id']);
            }
            $total_ids=implode("','",$total_ids);
           $total_ids= "'".$total_ids."'";
          //  dd($total_ids);
            $query1 = $this->companyConnection->query("SELECT u.name,strb.date, ud.id,ud.unit_prefix,ud.unit_no FROM short_term_rental_booking_dates strb JOIN users u ON u.id=strb.user_id JOIN unit_details ud ON ud.id=strb.unit_booked_id where ud.id IN($total_ids) ");
         //   dd($query1);
            $data2 = $query1->fetchAll();
       // dd($data2);
        }
        elseif ($a == "") {
            $query = $this->companyConnection->query("SELECT ud.unit_no,ud.unit_prefix,ud.id,ud.base_rent,bd.building_name,gp.property_name from unit_details ud JOIN building_detail bd on ud.building_id=bd.id JOIN general_property gp ON ud.property_id=gp.id WHERE gp.is_short_term_rental='1' ORDER BY id ASC");
            $data = $query->fetchAll();
            $data1=count($data);

            $query1 = $this->companyConnection->query("SELECT u.name,strb.date, ud.id,ud.unit_prefix,ud.unit_no FROM short_term_rental_booking_dates strb JOIN users u ON u.id=strb.user_id JOIN unit_details ud ON ud.id=strb.unit_booked_id");
            $data2 = $query1->fetchAll();
        }

        return ['code' => 200, 'status' => 'success', 'data' => $data,'count'=>$data1,'data2'=>$data2];
    }
    public function getdatesdaily(){
        $start_from_reocc=mySqlDateFormat($_POST['start_from_reocc'],NULL,$this->companyConnection);
        $end_after_reocc=$_POST['end_after_reocc'];
        $every_reocc=$_POST['every_reocc'];
        // Variable that store the date interval
        // of period 1 day
        $array1 = array();
        $array2 = array();
        $format = 'Y-m-d';
        $interval = new DateInterval('P'.$every_reocc.'D');
        $realEnd1 = new DateTime($start_from_reocc);
        $days=($end_after_reocc-1)*$_POST['every_reocc'];
        /*$days=$days+1;*/
        $realEnd2=date('Y-m-d H:i:s', strtotime('+'.$days.' day', time($realEnd1)));
        $realEnd = new DateTime($realEnd2);
        $realEnd->add($interval);
        $period = new DatePeriod(new DateTime($start_from_reocc), $interval, $realEnd);
        // Use loop to store date into array
        foreach($period as $date) {
            $array2[] = $date->format($format);
        }

        for($ik=1;$ik<count($array2);$ik++){
        array_push($array1,$array2[$ik]);
        }
       // dd(count($array1));
        return $array1;
    }
    public function getdatesweekly(){
        $start_from_reocc=mySqlDateFormat($_POST['start_from_reocc'],NULL,$this->companyConnection);
        $end_after_reocc=$_POST['end_after_reocc'];
        $every_month_reocc=$_POST['every_month_reocc'];
        $selected_days=$_POST['selected_days'];
        $array1 = array();
        $array2 = array();
        $format = 'Y-m-d';
        $interval = new DateInterval('P1D');
        $realEnd1 = new DateTime($start_from_reocc);
        $days=($end_after_reocc-1)*$_POST['every_month_reocc'];
        $days=$days+1;
        $days=$days*7;
        $realEnd2=date('Y-m-d H:i:s', strtotime('+'.$days.' day', time($realEnd1)));
        $realEnd = new DateTime($realEnd2);
        $realEnd->add($interval);
        $period = new DatePeriod(new DateTime($start_from_reocc), $interval, $realEnd);
        // Use loop to store date into array
        foreach($period as $date) {
            $array1[] = $date->format($format);
            for($s=0;$s<count($selected_days);$s++){
                if(date('D', strtotime($date->format($format)))==$selected_days[$s]){
                    $array2[]=$date->format($format);
                }

            }
        }
        //  echo"<pre>";dd($array2);
        $newarry=array_chunk($array2,count($selected_days));
        // echo"<pre>";  print_r($newarry);die;
        $test=[];
        for($i=0;$i<count($newarry);$i++){
            if($i%$every_month_reocc=='0'){
                array_push($test,$newarry[$i]);
            }
        }
        $insert_val=[];
        $count=count($test);
        if($every_month_reocc=="1"){
            $count=(count($test))-1;
        }
        for($k=0;$k<$count;$k++){
            for($l=0;$l<count($test[$k]);$l++){
                (array_push($insert_val,$test[$k][$l]));
            }
        }

        return $insert_val;
    }
public function getdatesmonthly(){
    $start_from_reocc=mySqlDateFormat($_POST['start_from_reocc'],NULL,$this->companyConnection);
    $end_after_reocc=$_POST['end_after_reocc'];
    $monthly_week=$_POST['monthly_week'];
    $monthly_days=$_POST['monthly_days'];
    $monthly_every=$_POST['monthly_every'];
    $array1 = array();
    $format = 'Y-m-d';
    $interval = new DateInterval('P1D');
    $realEnd1 = new DateTime($start_from_reocc);
    $days=($end_after_reocc)*($monthly_every);
    $realEnd2=date('Y-m-d H:i:s', strtotime('+'.$days.' months', time($realEnd1)));
    $realEnd = new DateTime($realEnd2);
    $realEnd->add($interval);
    $period = new DatePeriod(new DateTime($start_from_reocc), $interval, $realEnd);
    // Use loop to store date into array
    $array2=[];
    $array3=[];
    foreach($period as $date) {
        $array1[] = $date->format($format);
        $date1=new DateTime($date->format($format));
        $date1->modify($monthly_week.' '.$monthly_days.' of this month');
        $date1 = $date1->format('Y-m-d H:i:s');
        $date1 = date_create($date1);
        $date1 =date_format($date1, 'Y-m-d');
        if($date1==$date->format($format)){
            array_push($array3,$date->format($format));
        }
    }
    $insert=[];
    for($s=0;$s<count($array3);$s++){
        if($s%$monthly_every==0){
            array_push($insert,$array3[$s]);
        }
    }
    return $insert;
}
public function viewshortterm(){
        $user_id=$_POST['user_id'];
        $query = $this->companyConnection->query("SELECT u.id,u.salutation,u.name as user_name,u.first_name,u.middle_name,u.last_name,u.company_name,str.image,
                                                 u.maiden_name,u.nick_name,u.hobbies,u.phone_number,u.email,u.ssn_sin_id,u.phone_number_note,u.phone_type,
                                                 pt.id as phone_typ,pt.type,ca.id as career,ca.carrier,co.id as country,co.name,co.code,trs.id as referal_source,trs.referral,
                                                 te.id as ethnicity,te.title,tvs.id as vet_status,tvs.veteran,tms.id as marital_status,tms.marital,str.email2,str.email3,str.image,
                                                tp.phone_number as phone_numberr,ptt.type as typee,caa.carrier as carrierr, coo.code as codee,hob.hobby,teth.title,gp.property_name,bd.building_name,CONCAT(ud.unit_prefix,'-',ud.unit_no) as unit,if(str.frequency= 1, 'One Time', 'Recurring') as frequency
                                                   from users u
                                                 left join phone_type pt on pt.id=u.phone_type
                                                 left join carrier ca on ca.id=u.carrier
                                                 left join countries co on co.id=country_code
                                                 left join tenant_referral_source trs on trs.id=u.referral_source
                                                 left join tenant_ethnicity te on te.id=u.ethnicity
                                                 left join tenant_veteran_status tvs on tvs.id=u.veteran_status
                                                 left join tenant_marital_status tms on tms.id=u.maritial_status
                                                 left join short_term_rental str on str.user_id=u.id
                                                 left join tenant_phone tp on tp.user_id=u.id
                                                 left join phone_type ptt on ptt.id=tp.phone_type
                                                 left join carrier caa on caa.id=tp.carrier
                                                 left join countries coo on coo.id=tp.country_code
                                                 left join hobbies hob on u.hobbies=hob.id
                                                 left join tenant_ethnicity teth on u.ethnicity=teth.id
                                                 left join general_property gp on gp.id=str.property_id
                                                 left join building_detail bd on bd.id=str.building_id
                                                 left join unit_details ud on ud.id=str.unit_id
                                                 where u.id=".$user_id);
        $data = $query->fetch();
       // dd($data);
    $query = $this->companyConnection->query("SELECT `date` from short_term_rental_booking_dates where user_id=".$user_id);
        $data1 = $query->fetchAll();
    $query = $this->companyConnection->query("SELECT `hobby` from tenant_hobbies where user_id=".$user_id);
        $data2 = $query->fetchAll();
        for($i=0;$i<count($data2);$i++){
            $query = $this->companyConnection->query("SELECT `hobby` from hobbies where id=".$data2[$i]['hobby']);
            $data3 = $query->fetchAll();

        }
      //  dd($data3);
        $date=[];
    for ($i = 0; $i < count($data1); $i++) {
        array_push($date, dateFormatUser($data1[$i]['date'], NULL, $this->companyConnection));
    }
   $date= implode(',',$date);
    return ['data' => $data,'data1'=>$date];
}
public function makepayment(){
    $token_id = $_REQUEST['token_id'];
    $currency = $_REQUEST['currency'];
    $amount = $_REQUEST['amount'];
    $amount = str_replace( $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ,'',$amount);
    $amount = str_replace( '.00','',$amount);
    $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
    $user_type = $_REQUEST['user_type'];
    $query = $this->companyConnection->query("SELECT stripe_account_id from users where id=1");
    $stripe_account_id = $query->fetch();
    $account_id=$stripe_account_id['stripe_account_id'];
    if(empty($account_id)){
        return array('code' => 200, 'status' => 'accountError','message' => "Project manager is not registered for payment.");
       // return "Project manager is not registered";
    }
    else{

    }
    \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
    try {

        $charge = \Stripe\Charge::create([
            "amount" => $amount * 100,
            "currency" => $currency,
            "source" => $token_id,
            "destination" => array("account"  => $account_id)
        ]);

        //dd($charge);

        $charge_id =$charge['id'];
        $transaction_id =$charge['balance_transaction'];
        $stripe_status  = $charge;
        $payment_response  = 'Payment has been done successfully.';



        $data['transaction_id'] = $charge_id;
        $data['charge_id'] = $transaction_id;
        $data['user_id'] = $user_id;
        $data['user_type'] = $user_type;
        $data['amount'] = $amount;
        $data['total_charge_amount'] = $amount;
        $data['stripe_status'] = $stripe_status;
        $data['payment_response'] = $payment_response;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        // dd($data);
        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);
/*
        $qry1 = "UPDATE users SET payment_status='1',free_plan='0' where id=".$user_id;
        $stmt1 = $this->companyConnection->prepare($qry1);
        $stmt1->execute();
        $domainAll = getDomain();
        $qry2 = "UPDATE users SET payment_status='1',free_plan='0' where domain_name='".$domainAll."'";
        $stmt2 = $this->conn->prepare($qry2);
        $stmt2->execute();*/
        return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}
}

$TenantShortTermAjax = new TenantShortTermAjax();

