<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");
/*include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");*/
include_once (ROOT_URL . "/company/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class moveOutTenant extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /*Get Tenant Details*/
    public function getTenantDetailsMoveOut()
    {
        try{
            $id = $_POST['id'];
            $dataAll['TenantName'] = userName($id, $this->companyConnection,$table='users');
            $query = $this->companyConnection->query("SELECT `company_name`,`phone_number` FROM users WHERE id='$id'");
            $settings = $query->fetch();
            $dataAll['TenantDetails'] = $settings;

            $query1 = $this->companyConnection->query("SELECT `start_date`,`balance`,`security_deposite`,`rent_amount`,`notice_date`,`move_out`,`notice_period` FROM tenant_lease_details WHERE user_id='$id'");
            $settings1 = $query1->fetch();
           // print_r($settings1);die;
            $dataAll['TenantLeaseDetails']= $settings1;
            $dataAll['TenantLeaseDate'] = dateFormatUser($settings1['start_date'], $id, $this->companyConnection);
            $dataAll['TenantLeaseNoticeDate'] = dateFormatUser($settings1['notice_date'], $id, $this->companyConnection);
            $dataAll['TenantLeaseMoveOutDate'] = dateFormatUser($settings1['move_out'], $id, $this->companyConnection);

            $query2 = $this->companyConnection->query("SELECT g.property_name FROM tenant_property as t LEFT JOIN general_property as g ON g.id = t.property_id  WHERE t.user_id='$id'");
            $settings2 = $query2->fetch();
            $dataAll['TenantPropertyName'] =$settings2;

            $query3 = $this->companyConnection->query("SELECT CONCAT( u.unit_prefix,'-', u.unit_no ) AS unitName FROM tenant_property as t LEFT JOIN unit_details as u ON u.id = t.unit_id  WHERE t.user_id='$id'");
            $settings3 = $query3->fetch();
            $dataAll['TenantUnitName'] =$settings3;

            $query4 = $this->companyConnection->query("SELECT `movein_key_signed` FROM  tenant_details WHERE user_id='$id'");
            $settings4 = $query4->fetch();
            $dataAll['TenantMoveInSigned']= $settings4;

            $query5 = $this->companyConnection->query("SELECT CONCAT( d.unit_prefix,'-', d.unit_no ) AS unitName, CONCAT( u.first_name,' ', u.last_name ) AS username,u.email,u.phone_number,t.rent_amount, p.property_id FROM users as u LEFT JOIN tenant_lease_details as t on u.id = t.user_id LEFT JOIN tenant_property as p on u.id = p.user_id LEFT JOIN general_property as g ON g.id = p.property_id LEFT JOIN unit_details as d ON d.id = p.unit_id WHERE u.user_type='2' AND t.record_status='1'");
            $settings5 = $query5->fetch();
            $dataAll['TenantMoveOutTable']= $settings5;

            $query6 = $this->companyConnection->query("SELECT * FROM moveouttenant WHERE user_id='$id'");
            $settings6 = $query6->fetch();
           // $dataAll['TenantMoveOutData']= $settings6;
            $dataAll['TenantMoveOutscheduledDate'] = dateFormatUser($settings6['scheduledMoveOutDate'], $id, $this->companyConnection);
            $dataAll['TenantMoveOutNoticeDate'] = dateFormatUser($settings6['noticeDate'], $id, $this->companyConnection);
            $dataAll['TenantMoveOutScheduledDate'] = dateFormatUser($settings6['scheduledMoveOutDate'], $id, $this->companyConnection);
            //$dataAll['TenantMoveOutData'] = $settings6;

            return array('code' => 200, 'status' => 'success', 'data' => $dataAll, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getReasonForLeavingDetails()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM reasonmoveout_tenant");
            $settings = $query->fetchAll();
            //dd($settings);
            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function updateNoticePeriodData()
    {
        try{
            $data11 = $_POST['form'];
            $data1 = postArray($data11);
            //dd($data1);
            $data['noticeDate'] = (isset($data1['noticeDate']) && !empty($data1['noticeDate'])) ? mySqlDateFormat($data1['noticeDate'], null, $this->companyConnection) : '';
            $data['scheduledMoveOutDate'] = (isset($data1['scheduledMoveOutDate']) && !empty($data1['scheduledMoveOutDate'])) ? mySqlDateFormat($data1['scheduledMoveOutDate'], null, $this->companyConnection) : '';
            $data['reason_id'] = $data1['reasonForLeaving'];
            //dd($data['reason_id']);
            $data['user_id'] = $_POST['id'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $required_array = ['noticeDate','scheduledMoveOutDate','reason_id'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $query11 = "SELECT `id` FROM moveouttenant WHERE user_id = ". $data['user_id'];
                $record = $this->companyConnection->query($query11)->fetch();
                $userid = $record['id'];
//                dd($record);
                if(!empty($record)){
                    //update
                   // dd($record['id']);
                    $sqlData = createSqlColValPair($data);
                    //dd($sqlData);
                    $query = "UPDATE moveouttenant SET ".$sqlData['columnsValuesPair']." where id='$userid'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    //print_r($data);die;
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Update Saved successfully');

                }else{
                    //dd("there");
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO moveouttenant (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    /*echo '<pre>';print_r($stmt);die;*/
                    $stmt->execute($data);
                     return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record Saved successfully');
                }
               /* $moveOutId = $id['id'];
                $moveOutUserId = $id['user_id'];*/
               // print_r($moveOutUserId);die;
               /* if ($id == '' && $moveOutUserId != "'.$moveOutUserId.'") {
                    dd("here");
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO moveouttenant (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    /*echo '<pre>';print_r($stmt);die;*/
                    //$stmt->execute($data);
                   // return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record Saved successfully');
                //}else{
                  //  dd("there");
                  //  $sqlData = createSqlColValPair($data);
                    //dd($sqlData);
                  //  $query = "UPDATE moveouttenant SET ".$sqlData['columnsValuesPair']." where id='$moveOutId'";
                  //  $stmt = $this->companyConnection->prepare($query);
                   // $stmt->execute();*/
                    //print_r($data);die;
                   // return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Update Saved successfully');
              //  }


            }

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getActualMoveOutDate()
    {
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT `scheduledMoveOutDate` FROM moveouttenant WHERE user_id='$id'");
            $settings = $query->fetch();
            $dataAll['TenantMoveOutscheduledDate'] = dateFormatUser($settings['scheduledMoveOutDate'], $id, $this->companyConnection);

            return array('code' => 200, 'status' => 'success', 'data' => $dataAll, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function updateRecordMoveOutData()
    {
        try {
            $user_id = $_POST['id'];
            $data11 = $_POST['form'];
            $data1 = postArray($data11);
            $data['actualMoveOutDate'] = (isset($data1['actualMoveOutDate']) && !empty($data1['actualMoveOutDate'])) ? mySqlDateFormat($data1['actualMoveOutDate'], null, $this->companyConnection) : '';
            $data['unitAvailableDate'] = (isset($data1['unitAvailableDate']) && !empty($data1['unitAvailableDate'])) ? mySqlDateFormat($data1['unitAvailableDate'], null, $this->companyConnection) : '';
            $data['noOfKeysSigned_movein'] = $data1['noOfKeysSigned_movein'];
            $data['noOfKeysSigned_moveout'] = $data1['noOfKeysSigned_moveout'];

            $required_array = ['actualMoveOutDate', 'unitAvailableDate', 'noOfKeysSigned_moveout'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE moveouttenant SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);
                // echo '<pre>';print_r($stmt);die;

                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record Update successfully');
            }
        }catch(Exception $exception){
                echo $exception->getMessage();
                printErrorLog($exception->getMessage());
        }
    }


    public function getTenantChargeCodeMoveOut()
    {
        $user_id = $_POST['id'];
        //$getChargeList = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE user_id ='" . $user_id . "'")->fetchAll();
        $query = $this->companyConnection->query("SELECT c.charge_code,c.description,t.amount,t.frequency,c.created_at,c.id FROM tenant_charges as t LEFT JOIN company_accounting_charge_code as c ON c.id = t.charge_code  WHERE t.user_id='$user_id'");
        $settings = $query->fetchAll();
        $html = '';

        if(!empty($settings)){
                foreach ($settings as $list) {
                    $id = $list['id'];
                    //print_r($id);die;
                    $charge = $list['charge_code'];
                    //dd($charge);
                    $frequency = $list['frequency'];
                    $date = $list['created_at'];
                    $Date = dateFormatUser($date, null, $this->companyConnection);
                    $amount = $list['amount'];
                    $description = $list['description'] ;

                    $html .= "<input type='hidden' name='chargeId[]' value='$id'>";
                    $html .= "<tr>";
                    $html .= "<td>" . $charge . "</td>";
                    $html .= "<td>" . $description . "</td>";
                    $html .= "<td>" . $frequency . "</td>";
                    $html .= "<td>" . $amount . "</td>";
                    $html .= "<td>" . $Date . "</td>";
                    $html .= "<td> No Data</td>";
                  /*  $html .= "<td><div class='endDate_$id'><input type='hidden' name='editDate[]'><a href='JavaScript:Void(0);' class='editEndDate' data-id='" . $id . "'>" . $endDate . "<i class='fas fa-highlighter'></i></a></div></td>";
                    $html .= "<td><div class='endAmount_$id'><input type='hidden' name='editAmount[]'><a href='JavaScript:Void(0);' class='editAmount amount_$id' data-id='" . $id . "'>" . $amount . "<i class='fas fa-highlighter'></i></a></div></td>";*/
                    $html .= "</tr>";

                }
        }else{
            $html .= "<tr>";
            $html .= "<td colspan='9'> <div align='center' style='padding:6px'>No Record found</div></td>";
            $html .= "</tr>";
        }
        //var_dump($html);die;
        return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'Record retrieved successfully');
    }


    public function insertForwardAddressMvOut()
    {
        $data11 = $_POST['form'];
        $data1 = postArray($data11);
       // dd($data1);
        $data['user_id'] = $_POST['id'];
        $data['frwdAddress_FName'] = $data1['FrwdAddress_FName'];
        $data['frwdAddress_LName'] = $data1['FrwdAddress_LName'];
        $data['frwdAddress_Address'] = $data1['FrwdAddress_Address'];
        $data['frwdAddress_Zip'] = $data1['FrwdAddress_Zip'];
        $data['frwdAddress_Country'] = $data1['FrwdAddress_Country'];
        //dd($data['FrwdAddress_Country']);
        $data['frwdAddress_State'] = $data1['FrwdAddress_State'];
        $data['frwdAddress_City'] = $data1['FrwdAddress_City'];
        $data['frwdAddress_Phone'] = $data1['FrwdAddress_Phone'];
        //var_dump($data['frwdAddress_Phone']);die;
        $data['email'] = $data1['FrwdAddress_Email'];

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $required_array = ['FrwdAddress_FName', 'FrwdAddress_LName', 'FrwdAddress_Address','frwdAddress_Zip','frwdAddress_Country','frwdAddress_State','frwdAddress_City','frwdAddress_Phone','email'];
        /* Max length variable array */
        $maxlength_array = [];
        //Number variable array
        $number_array = [];
        //Server side validation

        $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
        //Checking server side validation
        if (checkValidationArray($err_array)) {
            return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
        } else {
            $sqlData = createSqlColVal($data);
           // print_r($sqlData);die;
            $query = "INSERT INTO forwardadress_moveout (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            //echo '<pre>';print_r($stmt);die;
            $stmt->execute($data);

            // var_dump($data['FrwdAddress_Email']);die;
            return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record retrieved successfully');
        }
    }

    public function getTenantForwordAddress()
    {
        $user_id = $_POST['id'];
        //$getChargeList = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE user_id ='" . $user_id . "'")->fetchAll();
        $query = $this->companyConnection->query("SELECT * FROM forwardadress_moveout WHERE user_id='$user_id'");
        $settings = $query->fetchAll();
       // print_r($settings);
        $html = '';

        if(!empty($settings)){
            foreach ($settings as $list) {
                $id = $list['id'];
                //print_r($id);die;
                $firstName = $list['frwdAddress_FName'];
                $lastName = $list['frwdAddress_LName'];
                $adress = $list['frwdAddress_Address'];
                $phone = $list['frwdAddress_Phone'];
                $email = $list['email'];


                //$html .= "<input type='hidden' name='chargeId[]' value='$id'>";
                $html .= "<tr>";
                $html .= "<td>" . $firstName . "</td>";
                $html .= "<td>" . $lastName . "</td>";
                $html .= "<td>" . $adress . "</td>";
                $html .= "<td>" . $phone . "</td>";
                $html .= "<td>" . $email . "</td>";
                $html .= "<td><select data_id='" . $id . "' class='form-control select_options'><option value='default'>Select</option><option class='editShowDiv' value='Edit'>Edit</option></select></td>";
                $html .= "</tr>";

            }
        }else{
            $html .= "<tr>";
            $html .= "<td colspan='9'> <div class='NoRecordFound' align='center' style='padding:6px'>No Record found</div></td>";
            $html .= "</tr>";
        }
        //var_dump($html);die;
        return array('code' => 200, 'status' => 'success', 'data' => $html, 'dataDetails'=> $settings, 'message' => 'Record retrieved successfully');
    }


    public function insertbalanceChargeCode()
    {
        try{
            $data11 = $_POST['form'];
            $data1 = postArray($data11);
            $userId =$_POST['id'];
            $data['user_id'] = $userId;
            $data['charge_code'] = $data1['tax_chargeCode'];
            $data['amount'] = $data1['amount'];
            $data['description'] = $data1['new_chargeCode_description'];
            $data['amount_due'] = $data1['amount'];
            $data['amount_paid'] = "0";


            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $required_array = ['charge_code','amount','description'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_charges (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                /*echo '<pre>';print_r($stmt);die;*/
                $stmt->execute($data);
                $this->addChargeAmount($userId,'user');
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record Saved successfully');
            }
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function addChargeAmount($id,$type)
    {
        if($type=='user')
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE user_id ='" . $id . "'")->fetch();
        }
        else
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE invoice_id ='" . $id . "'")->fetch();
        }


        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $data['total_amount'] = $totalAmount;
        }


        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $data['total_amount_paid'] = $amount_paid;
        }

        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $data['total_refunded_amount'] = $amount_refunded;
        }

        /*if($getCharges['waive_of_amount']=='')
        {
            $waive_of_amount = 0;
            $data['total_waive_amount'] = $waive_of_amount;
        }
        else
        {
            $waive_of_amount = $getCharges['waive_of_amount'];
            $data['total_waive_amount'] = $waive_of_amount;
        }*/


        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');




        $data['total_due_amount'] = $getCharges['amount_due'];
        $sqlData = createSqlColValPair($data);
        if($type=='user')
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        else
        {

            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }


    }

    public function updateTenantForwordAddress()
    {
        try {
            $user_id = $_POST['id'];
            $data11 = $_POST['form'];
            $data = postArray($data11);
            //print_r($data1);die;

            $data['updated_at'] = date('Y-m-d H:i:s');
            $required_array = ['frwdAddress_FName', 'frwdAddress_LName', 'frwdAddress_Address','frwdAddress_Zip','frwdAddress_Country','frwdAddress_State','frwdAddress_City','frwdAddress_Phone','email'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $sqlData = createSqlColValPair($data);

                $query = "UPDATE forwardadress_moveout SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id'";

                $stmt = $this->companyConnection->prepare($query);

                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record Update successfully');
            }
        }catch(Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function getTenantMoveOutDetails()
    {
        try{
            $keyValueData = '';
            $query = $this->companyConnection->query("SELECT CONCAT( d.unit_prefix,'-', d.unit_no ) AS unitName, CONCAT( u.first_name,' ', u.last_name )  AS username,u.email,u.phone_number,u.id,t.rent_amount,t.balance, g.property_id FROM users as u LEFT JOIN tenant_lease_details as t on u.id = t.user_id LEFT JOIN tenant_property as p on u.id = p.user_id LEFT JOIN general_property as g ON g.id = p.property_id LEFT JOIN unit_details as d ON d.id = p.unit_id WHERE u.user_type='2' AND t.record_status='1' AND u.first_name like '%{$keyValueData}%'");

            $settings = $query->fetchAll();
            $html = '';

            if(!empty($settings)){
                $i = 1;
                foreach ($settings as $list) {
                    $id = $list['id'];
                    $TenantName = $list['username'];
                    $email = $list['email'];
                    $phone_number = $list['phone_number'];
                    $property_id = $list['property_id'];
                    $unitName = $list['unitName'];
                    $rent_amount = $list['rent_amount'];
                    $balance = $list['balance'];



                    $html .= "<tr>";
                    if($i == 1){
                    $html .= "<input class='tenantIdget' type='hidden' name='TenantId' value='$id'>";
                    $html .= "<input class='tenantIdget' type='hidden' name='TenantId' value='$TenantName'>";
                    }
                    $html .= "<input class='tenantIdget' type='hidden' name='TenantId' value='$id'>";
                    $html .= "<td class='nameTeanant'>" . $TenantName . "</td>";
                    $html .= "<td>" . $email . "</td>";
                    $html .= "<td>" . $phone_number . "</td>";
                    $html .= "<td>" . $property_id . "</td>";
                    $html .= "<td>" . $unitName . "</td>";
                    $html .= "<td>" . $rent_amount . "</td>";
                    $html .= "<td>" . $balance . "</td>";
                    $html .= "</tr>";
   $i++;
                }
            }

            //print_r($settings);die;
            return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getTenantMoveOutSearch()
    {
        try{
            $keyValueData = $_POST['keyValue'];
            // print_r($keyValueData);die;

            $query = $this->companyConnection->query("SELECT CONCAT( d.unit_prefix,'-', d.unit_no ) AS unitName, CONCAT( u.first_name,' ', u.last_name )  AS username,u.email,u.phone_number,u.id,t.rent_amount,t.balance, g.property_id FROM users as u LEFT JOIN tenant_lease_details as t on u.id = t.user_id LEFT JOIN tenant_property as p on u.id = p.user_id LEFT JOIN general_property as g ON g.id = p.property_id LEFT JOIN unit_details as d ON d.id = p.unit_id WHERE u.user_type='2' AND t.record_status='1' AND u.first_name like '%{$keyValueData}%'");
            $settings = $query->fetchAll();
            $html = '';

            if(!empty($settings)){
                $i = 1;
                foreach ($settings as $list) {
                    $id = $list['id'];
                    $TenantName = $list['username'];
                    $email = $list['email'];
                    $phone_number = $list['phone_number'];
                    $property_id = $list['property_id'];
                    $unitName = $list['unitName'];
                    $rent_amount = $list['rent_amount'];
                    $balance = $list['balance'];

                    $html .= "<tr>";
                    if($i == 1){
                    $html .= "<input class='tenantIdget' type='hidden' name='TenantId' value='$id'>";
                    $html .= "<input class='tenantNameget' type='hidden' name='TenantName' value='$TenantName'>";
                    }
                    $html .= "<input class='tenantIdget' type='hidden' name='TenantId' value='$id'>";
                    $html .= "<td class='nameTeanant'>" . $TenantName . "</td>";
                    $html .= "<td>" . $email . "</td>";
                    $html .= "<td>" . $phone_number . "</td>";
                    $html .= "<td>" . $property_id . "</td>";
                    $html .= "<td>" . $unitName . "</td>";
                    $html .= "<td>" . $rent_amount . "</td>";
                    $html .= "<td>" . $balance . "</td>";
                    $html .= "</tr>";
$i++;
                }
            }else{
                $html .= "";
            }

            //print_r($settings);die;
            return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getTabDataRecord() {
        try{
            $keyId = $_POST["id"];
           $query = $this->companyConnection->query("Select `noOfKeysSigned_moveout` from moveouttenant WHERE user_id = '$keyId'");
           $stmt = $query->fetch();
           // print_r($stmt);die;
            return array('code'=>200, 'status' => 'success', 'data' => $stmt,'message' =>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog();
        }
    }

    public function updateMoveOutListData()
    {
        try {
            $user_id = $_POST['id'];
            //$data11 = $_POST['form'];
            //$data1 = postArray($data11);
            $data['status'] = "2";
            $data['record_status'] = "1";
            $data1['record_status'] = "0";
            $data2['building_unit_status']= "1";
            $data3['record_status']= "0";

            $queryN = $this->companyConnection->query("SELECT unit_id FROM tenant_property WHERE user_id='$user_id'");
            $settings1 = $queryN->fetch();
            //echo "<pre>"; print_r($settings1['unit_id']);die;
            $unit_id_tenant = $settings1['unit_id'];

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColValPair($data1);
                $query1 = "UPDATE  tenant_lease_details SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id' AND record_status = '1'";
                $stmt1 = $this->companyConnection->prepare($query1);
                //echo '<pre>';print_r($stmt1);die;
                $stmt1->execute();

                $sqlData = createSqlColValPair($data3);
                $query2 = "UPDATE tenant_property SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id' AND record_status = '1'";
                $stmt2 = $this->companyConnection->prepare($query2);
                //echo '<pre>';print_r($stmt1);die;
                $stmt2->execute();

                $sqlData = createSqlColValPair($data2);
                $query3 = "UPDATE unit_details SET " . $sqlData['columnsValuesPair'] . " where id='$unit_id_tenant'";
                $stmt3 = $this->companyConnection->prepare($query3);
                //echo '<pre>';print_r($stmt1);die;
                $stmt3->execute();



                $sqlData = createSqlColValPair($data);
                $query = "UPDATE moveouttenant SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'The Record Update successfully');
            }
        }catch(Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function checkDataUnitDetails() {
        try{

            $unitId = $_POST["id"];
            $query = $this->companyConnection->query("Select building_unit_status from unit_details WHERE id = '$unitId'");
            $stmt = $query->fetch();
            //echo "<pre>";print_r($stmt);die;
            // print_r($stmt);die;
            return array('code'=>200, 'status' => 'success', 'data' => $stmt,'message' =>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog();
        }
    }

    public  function undoMoveoutTenant(){
        try{

            $moveoutId = $_POST["id"];
            $todayDate = date('Y-m-d');


            /*$queryU = $this->companyConnection->query("SELECT `user_id` FROM moveouttenant WHERE user_id='$moveoutId'");
            echo "<pre>";print_r($queryU);die;
            $settingsU = $queryU->fetch();
            $user_id_tenant = $settingsU['user_id'];*/


            $data['status'] = "0";
            $data['record_status'] = "0";
            $data2['building_unit_status']= "5";
            $data3['record_status']= "1";

            $queryN = $this->companyConnection->query("SELECT unit_id FROM tenant_property WHERE user_id='$moveoutId'");
            $settings1 = $queryN->fetch();
            $unit_id_tenant = $settings1['unit_id'];

            $queryL = $this->companyConnection->query("SELECT * FROM tenant_lease_details WHERE user_id='$moveoutId' ORDER BY id LIMIT 1");
            $settingsL = $queryL->fetch();
            $endTeantDate  = $settingsL['end_date'];

            /*Difference of start and End date */
            $date1=date_create($todayDate);
            $date2=date_create($endTeantDate);
            $diff=date_diff($date1,$date2);
            $remainingDays = $diff->format("%R%a days");
            /*Difference of start and End date */

            /* 1 year later From start date*/
            $dateEndLease = strtotime($todayDate);
            $new_date = strtotime('+ 1 year', $dateEndLease);
            $LeaseEndDate = date('Y-m-d', $new_date);
            //echo "<pre>";print_r($todayDate);die;
            /* 1 year later From start date*/

            /*Update Data Lease Details Tenant*/
            $data1['record_status'] = "1";
            $data1['move_in'] = $todayDate;
            $data1['start_date'] = $todayDate;
            $data1['end_date']= $LeaseEndDate;
            $data1['move_out'] = $LeaseEndDate;
            /*Update Data Lease Details Tenant*/

            /*lease insert Data Lease Details Tenant*/
            $data5['record_status'] = "1";
            $data5['user_id'] = $moveoutId;
            $data5['balance'] = $settingsL['balance'];
            $data5['move_in'] = $todayDate;
            $data5['move_out'] = $LeaseEndDate;
            $data5['actual_move_out'] = $settingsL['actual_move_out'];
            $data5['start_date'] = $todayDate;
            $data5['term'] = $settingsL['term'];
            $data5['tenure']= $settingsL['tenure'];
            $data5['end_date']= $LeaseEndDate;
            $data5['notice_period']= $settingsL['notice_period'];
            $data5['notice_date'] = $settingsL['notice_date'];
            $data5['rent_due_day'] = $settingsL['rent_due_day'];
            $data5['rent_amount'] = $settingsL['rent_amount'];
            $data5['cam_amount'] = $settingsL['cam_amount'];
            $data5['security_deposite']= $settingsL['security_deposite'];
            $data5['next_rent_incr'] = $settingsL['next_rent_incr'];
            $data5['flat_perc'] = $settingsL['flat_perc'];
            $data5['amount_incr']= $settingsL['amount_incr'];
            $data5['days_remaining']= $settingsL['days_remaining'];
            /*lease insert Data Lease Details Tenant*/



            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if($remainingDays > 0){
                    //echo "harjinder"; die;
                    $sqlData = createSqlColValPair($data1);
                    $query1 = "UPDATE  tenant_lease_details SET " . $sqlData['columnsValuesPair'] . " where user_id='$moveoutId' AND record_status = '0'";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute();
                }else{
					$data6['record_status'] = '0';
                    $sqlData = createSqlColValPair($data6);
                    $query6 = "UPDATE  tenant_lease_details SET " . $sqlData['columnsValuesPair'] . " where user_id='$moveoutId' AND record_status = '0'";
                    $stmt6 = $this->companyConnection->prepare($query6);
                    $stmt6->execute();
                    //echo "harjinder1"; die;
                    $data5['created_at'] = date('Y-m-d H:i:s');
                    $data5['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data5);
                    $query = "INSERT INTO tenant_lease_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data5);
                    //echo '<pre>';print_r($query);die;
                }


                $sqlData = createSqlColValPair($data3);
                $query2 = "UPDATE tenant_property SET " . $sqlData['columnsValuesPair'] . " where user_id='$moveoutId' AND record_status = '0'";
                $stmt2 = $this->companyConnection->prepare($query2);
                //echo '<pre>';print_r($stmt1);die;
                $stmt2->execute();

                $sqlData = createSqlColValPair($data2);
                $query3 = "UPDATE unit_details SET " . $sqlData['columnsValuesPair'] . " where id='$unit_id_tenant'";
                $stmt3 = $this->companyConnection->prepare($query3);
                //echo '<pre>';print_r($stmt1);die;
                $stmt3->execute();



                $sqlData = createSqlColValPair($data);
                $query = "UPDATE moveouttenant SET " . $sqlData['columnsValuesPair'] . " where user_id='$moveoutId'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'The Record Update successfully');
            }


        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog();
        }
    }

    /*Clear Balance sheet Moveout out*/

    public function getAllChargesData()
    {
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

        $html =  "";
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th >Date</th>
        <th>Description</th>
        <th>Charge Code</th>
        <th>Original Amount($default_symbol)</th>
        <th>Amount paid($default_symbol)</th>
        <th>Amount Due($default_symbol)</th>
        <th>Amount Waived off($default_symbol)</th>
        <th>Waive off Amount($default_symbol)</th>
        <th>Waive off comment</th>
        <th>Write off Amount($default_symbol)</th>
        <th>Bad Debt($default_symbol)</th>
        <th>Current Payment($default_symbol)</th>
        <th>Action</th>


        </tr>";
            $user_id = $_POST['user_id'];
            $idByType = $user_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority,tc.write_of_amount ,tc.bad_debt_amount FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.user_id='$user_id'")->fetchAll();

        $html .= "<input type='hidden' name='typeofcharge' value=''>
                <input type='hidden' name='idbytype' value='$idByType'>";
        foreach($getData as $data)
        {
            $created_at = $data['created_at'];
            $date = dateFormatUser($created_at,null,$this->companyConnection);
            $description = $data['description'];
            $amount_due = $data['amount_due'];
            if($amount_due=='')
            {
                $amount_due = 0;
            }

            $totalamount = $data['amount'];
            $amount_paid = $data['amount_paid'];
            if($amount_paid=='')
            {
                $amount_paid = 0;
            }
            $amount_refunded = $data['amount_refunded'];
            $status = $data['status'];
            $charge_code = $data['charge_code'];
            $waive_of_amount = $data['waive_of_amount'];
            $write_of_amount = $data['write_of_amount'];
            $bad_debt_amount = $data['bad_debt_amount'];
            if($waive_of_amount=='')
            {
                $waive_of_amount = 0;
            }
            if($write_of_amount=='')
            {
                $write_of_amount = 0;
            }
            if($bad_debt_amount=='')
            {
                $bad_debt_amount = 0;
            }
            $waive_of_comment = $data['waive_of_comment'];
            $chargeId = $data['id'];

            if($status=='1')
            {
                $disabled = 'disabled=disabled';
            }
            else
            {
                $disabled = '';
            }
            $priority = $data['priority'];

            $html .= "<tr>
       
        <td>$date</td>
        <td>$description</td>
        <td>$charge_code</td>
        <td>$totalamount</td>
        <td id='amtPaid'>$amount_paid</td>
        <td class='current_due_amt_".$chargeId."'>$amount_due</td>
        <td>$waive_of_amount</td>
        <td><input type='text' class='amount_num waiveOfAmount_".$chargeId." numberonly' value='$waive_of_amount' name='waiveOfAmount[]' $disabled></td>
        <td><textarea class='waiveOfComment_".$chargeId."' name='waiveOfComment[]' $disabled>$waive_of_comment</textarea></td>
        <td><input type='text' class='amount_num writeOfAmount_".$chargeId." numberonly' value='$waive_of_amount' name='writeOfAmount[]' $disabled></td>
        <td><input type='text' class='amount_num badDebtOfAmount_".$chargeId." numberonly' value='$bad_debt_amount' name='bad_debt_amount[]' $disabled></td>
        <td><input type='text' class='amount_num currentPayment_".$chargeId." numberonly' value='$amount_due' name='currentPayment[]' $disabled readonly></td>
        <td><select class='form-control apply-data'  data-id='".$chargeId."' $disabled><option value='Select'>Select</option><option class='applyPopupdata' value='Apply'>Apply</option></select></td>";



            /*if($status=='1')
            {
                $html .= "<td><input type='checkbox' name='pay[]' disabled='disabled' checked='checked' value='$chargeId' class='pay_checkbox'></td>";

            }
            else
            {
                $html .= "<td><input type='checkbox' name='pay[]' checked='checked' value='$chargeId' class='pay_checkbox'></td>";
            }*/


            $html .=   "</tr>";

        }


        $html .= "</table>";
        /*$html .=   "<div class='footer-btn-outer'> <input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save & New'>";
        $html .=   "<input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save'>";
        $html .=   "<input type='button' class='CancelBtn blue-btn' value='Cancel'>";
        $html .=   "<input type='button'  class='reallocate blue-btn' value='Re-allocate'></div>";*/
        echo $html;
        exit;

    }


    function getCharges()
    {
        try {
            $id = $_POST['id'];
            $user_id = $_POST['userId'];
            $idByType = $user_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority,tc.write_of_amount ,tc.bad_debt_amount,amc.overpay_underpay FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id LEFT join accounting_manage_charges as amc on amc.user_id = tc.user_id where tc.user_id='$user_id' AND tc.id = '$id'")->fetch();

                $created_at = $getData['created_at'];
                $overpay_underpay = $getData['overpay_underpay'];
                /*$date = dateFormatUser($created_at,null,$this->companyConnection);*/
                $description = $getData['description'];
                $amount_due = $getData['amount_due'];
                if($amount_due=='')
                {
                    $amount_due = 0;
                }

                $totalamount = $getData['amount'];
                $amount_paid = $getData['amount_paid'];
                if($amount_paid=='')
                {
                    $amount_paid = 0;
                }
                $amount_refunded = $getData['amount_refunded'];
                $status = $getData['status'];
                $charge_code = $getData['charge_code'];
                $waive_of_amount = $getData['waive_of_amount'];
                $write_of_amount = $getData['write_of_amount'];
                $bad_debt_amount = $getData['bad_debt_amount'];
                $type = "Deposit";
                if($waive_of_amount=='')
                {
                    $waive_of_amount = 0;
                }
                if($write_of_amount=='')
                {
                    $write_of_amount = 0;
                }
                if($bad_debt_amount=='')
                {
                    $bad_debt_amount = 0;
                }
                if($overpay_underpay=='')
                {
                    $overpay_underpay = 0;
                }
                $waive_of_comment = $getData['waive_of_comment'];
                $chargeId = $getData['id'];

                $priority = $getData['priority'];

            $html  = "";
           /* $html .= "<input type='hidden' name='typeofcharge' value=''><input type='hidden' name='idbytype' value='$idByType'>";*/
            $html .= "<input type='hidden' name='remainingCdataOriginal' id='remainingCdataOriginal' value='$overpay_underpay'><table class='table table-bordered credit-debt-tbl'  border= '1px'><thead><tr><th>Code</th><th>Description</th><th>Type</th><th>Available Amount<span>(AFN)</span></th><th>Applied Amount<span>(AFN)</span></th><th>Waive off<span>(AFN)</span></th><th>Remaining Credit/deposit<span>(AFN)</span></th></tr></thead><tbody><tr><td>$charge_code</td><td>$description</td><td>$type</td><td>$amount_due</td><td><input type='text' class='amount_num creditGetData appliedAmount_".$chargeId." numberonly' value='' name='appliedAmount'></td><td><input type='text' id='wavieddAmount_".$chargeId."' class='amount_num wavieddAmount_".$chargeId." numberonly' value='' name='appliedAmount' disabled></td><td><input type='text' class='remaining_due_amt_".$chargeId."' name='remaining_due_amt' value='$overpay_underpay' readonly></td></tr></tbody></table>";
           /* return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'The Record Update successfully');*/
            echo $html;
            exit;

        } catch (Exception $exception) {
            echo $exception->getMessage();
            printErrorLog();
        }
    }

    function insertRemainingCharges()
    {
        try {
            $id = $_POST['id'];
            $userId = $_POST['userId'];
            $remainingCdataOriginal = $_POST['remainingCdataOriginal'];
            $amtPaid = $_POST['amtPaid'];
            //dd($amtPaid);
            $data = $_POST['formData'];
            $data1 = postArray($data);
            $overPay = $data1['remaining_due_amt'];

            if(!empty($data1)){
                $data2['waive_of_amount'] = $data1['waiveOfAmount'];
                if($data1['waiveOfComment'] = ''){
                    $data2['waive_of_comment'] = "";
                }else{
                    $data2['waive_of_comment'] = $data1['waiveOfComment'];
                }
                $data2['write_of_amount'] = $data1['writeOfAmount'];
                $data2['bad_debt_amount'] = $data1['badDebtOfAmount'];
                $data2['amount_paid'] = $data1['appliedAmountHidden'];
                $data2['amount_due'] = "0";
                $data2['status'] = "1";
            }
            $sqlData = createSqlColValPair($data2);
            $query = "UPDATE tenant_charges SET ".$sqlData['columnsValuesPair']." where id='$id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $this->addChargeAmountMoveOut($userId,"user",$overPay);
            return array('code' => 200, 'status' => 'success', 'data' => $data2, 'message' => 'The record update successfully');
            /*$query = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE userId='$user_id'");*/
        } catch (Exception $exception) {
            echo $exception->getMessage();
            printErrorLog();
        }
    }

    public function addChargeAmountMoveOut($id,$type,$overPay)
    {
        if($type=='user')
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE user_id ='" . $id . "'")->fetch();
        }
        else
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE invoice_id ='" . $id . "'")->fetch();
        }
        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $totalAmount =  str_replace(',', '', $totalAmount);
            $data['total_amount'] = $totalAmount;
        }
        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $amount_paid = str_replace(',', '', $amount_paid);
            $data['total_amount_paid'] = $amount_paid;
        }
        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $amount_refunded =  str_replace(',', '', $amount_refunded);
            $data['total_refunded_amount'] = $amount_refunded;
        }
        if($getCharges['waive_of_amount']=='')
        {
            $waive_of_amount = 0;
            $data['total_waive_amount'] = $waive_of_amount;
        }
        else
        {
            $waive_of_amount = $getCharges['waive_of_amount'];
            $waive_of_amount =    str_replace(',', '', $waive_of_amount);
            $data['total_waive_amount'] = $waive_of_amount;
        }
        $data['overpay_underpay'] = str_replace(',', '', $overPay);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['total_due_amount'] = str_replace(',', '', $getCharges['amount_due']);
        $sqlData = createSqlColValPair($data);
        if($type=='user')
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        else
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
    }


}

$moveOutTenant = new moveOutTenant();