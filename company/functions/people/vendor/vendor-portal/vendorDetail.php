<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");

class vendorDetail extends DBConnection
{

    /**
     * vendor portal view constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get property details
     */
    public function getVendorDetail()
    {
        $id = $_POST['vendor_id'];
        $table = 'users';
        $columns = [$table . '.vendor_id', $table . '.property_name', $table . '.legal_name', $table . '.property_year', $table . '.phone_number', $table . '.property_tax_ids', $table . '.fax', $table . '.manager_list', $table . '.is_short_term_rental', $table . '.property_style', $table . '.property_price', $table . '.attach_groups', $table . '.property_type', $table . '.no_of_units', $table . '.property_subtype', $table . '.smoking_allowed', $table . '.no_of_buildings', 'company_pet_friendly.pet_friendly', $table . '.property_squareFootage', $table . '.key_access_codes_info', $table . '.key_access_code_desc', $table . '.amenities', $table . '.property_parcel_number',$table. '.no_of_vehicles',$table. '.garage_note', $table . '.description', 'company_garage_avail.garage_avail', 'company_property_style.property_style', 'company_property_type.property_type', 'company_property_subtype.property_subtype',$table . '.online_listing', $table . '.online_application', $table . '.disable_sync','company_property_portfolio.portfolio_id','company_property_portfolio.portfolio_name'];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'id',
            'on_table' => 'vendor_additional_detail',
            'as' => 'vendor_additional_detail'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'id',
            'condition' => '=',
            'value' => $id
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);

        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');

        $unserializeData['smoking_allowed'] =  ($unserializeData['smoking_allowed'] == 'N/A')?'No':'Yes';
        $unserializeData['garage_available'] =  $unserializeData['garage_avail'];
        $string = '';

        if ($unserializeData['phone_number'] != 'N/A') {
            $phone_number = '';
            foreach ($unserializeData['phone_number'] as $key => $value) {
                $phone_number .= $value . ',';
            }
            $unserializeData['phone_number'] = str_replace_last(',', '', $phone_number);
        }
        if ($unserializeData['property_tax_ids'] != 'N/A') {
            $property_tax_ids = '';
            foreach ($unserializeData['property_tax_ids'] as $key => $value) {
                $property_tax_ids .= $value . ',';
            }
            $unserializeData['property_tax_ids'] = str_replace_last(',', '', $property_tax_ids);
        }
        if ($unserializeData['attach_groups'] != 'N/A') {
            $attach_groups = '';
            foreach ($unserializeData['attach_groups'] as $key => $value) {
                $where = [['table' => 'company_property_groups', 'column' => 'id', 'condition' => '=', 'value' => $value]];
                $query = selectQueryBuilder(['company_property_groups.group_name'], 'company_property_groups', null, $where);
                $groupData = $this->companyConnection->query($query)->fetch();
                $attach_groups .= $groupData['group_name'] . ',';
            }
            $unserializeData['attach_groups'] = str_replace_last(',', '', $attach_groups);
        }
        $unserializeData['is_short_term_rental'] = ($unserializeData['is_short_term_rental'] == '1') ? 'Yes' : 'No';
        $unserializeData['online_listing'] = ($unserializeData['online_listing'] == 'on') ? 'Yes' : 'No';
        $unserializeData['online_application'] = ($unserializeData['online_application'] == 'on') ? 'Yes' : 'No';
        $unserializeData['disable_sync'] = ($unserializeData['disable_sync'] == 'on') ? 'Yes' : 'No';


        if ($unserializeData['amenities'] != 'N/A') {
            $amenities = '';
            foreach ($unserializeData['amenities'] as $key=>$value){
                $query = 'SELECT name FROM company_property_amenities WHERE id='.$value;
                $groupData = $this->companyConnection->query($query)->fetch();
                $name = $groupData['name'];
                $amenities .= $name.',';
            }
            $unserializeData['amenities'] = str_replace_last(',', '', $amenities);
        }
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData,'key_access_codes_info_string'=>@$string, 'message' => 'Record retrieved Successfully.');
    }
}

$vendorDetail = new vendorDetail();
?>