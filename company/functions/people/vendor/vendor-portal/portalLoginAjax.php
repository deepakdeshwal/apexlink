<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class portalLoginAjax extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function for super admin user login
     * @return array
     */
    public function login() {   ///this function is not in use for owner portal login
        // printErrorLog('error');
        try {

            $page_url = $_SERVER['HTTP_REFERER'];

            $email = $_POST['email'];
            $password = md5($_POST['password']);

            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $domain = array_shift($domain);

            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
            $user = $query->fetch();

            /*dd($user);*/
            $query = $this->companyConnection->query("SELECT * FROM active_inactive_status");
            $active_inactive_status = $query->fetch();

            if ($user['id']) {
                if ($user['parent_id'] == 0) {
//                    dd($user['parent_id']);
                    $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
                    $super_admin_user = $query->fetch();

                    if ($super_admin_user['status'] == 0) {
                        return array('status' => 'error', 'code' => 503, 'message' => 'Your account has been deactivated by Admin. Please contact to System Administrator.');
                    }
                } else {
                    $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
                    $main_company_admin_user = $query->fetch();

                    if ($main_company_admin_user['status'] == 0) {
                        return array('status' => 'error', 'code' => 503, 'message' => 'Your account has been deactivated by Admin. Please contact to System Administrator.');
                    }
                }

                //If company login is from go to site link
                $var1 = strrpos($page_url, '?');
                if (!empty($var1) || $user['enabled_2fa'] == 0) {

//                    $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];
                    $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];
                    $_SESSION[SESSION_DOMAIN]['default_name'] = $user['first_name'].' '.$user['last_name'];
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_name'] = $user['company_name'];
                    $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];
                    $_SESSION[SESSION_DOMAIN]['phone_number'] = $user['phone_number'];
                    $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $user['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';


                    //Default setting session data
                    $_SESSION[SESSION_DOMAIN]['default_notice_period'] = defaultNoticePeriod($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_rent'] = defaultRent($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_application_fee'] = defaultApplicationfee($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_zipcode'] = defaultZipCode($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_city'] = defaultCity($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_currency'] = defaultCurrency($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN]['default_currency'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['pagination'] = pagination($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['payment_method'] = defaultPaymentMethod($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['property_size'] = defaultPropertySize($user['id'],$this->companyConnection);
                    if (!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                        $remember_login_data['email'] = $user["email"];
                        $remember_login_data['password'] = $user["actual_password"];
                        $remember_login_data['remember_token'] = $user["remember_token"];

                        $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                        setcookie("remember_login", serialize($remember_login_data), $expire);
                    } else {
                        setcookie("remember_login", '');
                    }

                    //insert user detail in audit trail
                    $this->audiTrail($user,'login');

                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status, 'go_to_site_link' => 'on');
                } else {


                    if ($user['enabled_2fa'] == 1) {
                        $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];

                        if (!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                            $remember_login_data['email'] = $user["email"];
                            $remember_login_data['password'] = $user["actual_password"];
                            $remember_login_data['remember_token'] = $user["remember_token"];

                            $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                            setcookie("remember_login", serialize($remember_login_data), $expire);
                        } else {
                            setcookie("remember_login", '');
                        }

                        //2Fa Authorization mail for OTP
                        $this->sendOTP($user);

                        //insert user detail in audit trail
                        return array('status' => 'success', 'data' => $user, 'active_inactive_status' => $active_inactive_status);

                    }

                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status);
                }
            } else {
                return array('status' => 'error', 'message' => 'Invalid Email or password');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Send OTP
     * @param $user
     */
    public function sendOTP($user){
        $OTP = randomTokenString(6);
        $OTP_mail = $this->sendMailForOTP($user, $OTP);
        if($OTP_mail->code == 200){
            $token_2fa = md5($OTP);
            $token_2fa_expiry = date('Y-m-d H:i:s');
            $update_token_company= $this->companyConnection
                ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                ->execute([$token_2fa, $token_2fa_expiry, $user['id']]);

            $update_token_superadmin= $this->conn
                ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                ->execute([$token_2fa, $token_2fa_expiry, $user['id']]);
        }
    }

    /**
     * Send mail to the user for OTP
     * @param $userData
     * @param $OTP
     * @return array|mixed|string
     */
    public function sendMailForOTP($userData, $OTP)
    {
        try{
            $user_name = userName($userData['id'], $this->companyConnection);
            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/two_fa_email.php');
            $body = str_replace("#logo#",$logo,$body);
            $body = str_replace("#user_name#",$user_name,$body);
            $body = str_replace("#token_2fa#",$OTP,$body);

            $request['to'][]    = $userData['email'];
            $request['action']  = 'SendMailPhp';
            $request['subject'] = '2Fa OTP for Apexlink';
            $request['portal']  = '1';
            $request['message'] = $body;
            $response = curlRequest($request);
            return $response;
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Check OTP is valid or not
     * @return array
     */
    public function checkTwoFaOTP( )
    {
        try{
            $token_2fa = md5($_POST['two_fa_otp']);
            $email = $_SESSION[SESSION_DOMAIN]['email'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $domain = array_shift($domain);

            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND token_2fa= '$token_2fa' AND domain_name='$domain' AND user_type='1'");
            $user = $query->fetch();
            if ($user['id']){

                $token_2fa_expiry_time = $user['token_2fa_expiry'];
                $minutes_to_add = 5;
                $time = new DateTime($token_2fa_expiry_time);
                $token_2fa_expiry_tme = $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

                $token_2fa_expiry_tme = $token_2fa_expiry_tme->format('Y-m-d H:i:s');
                $current_date = date('Y-m-d H:i:s');

                if($current_date > $token_2fa_expiry_tme){
                    $this->sendOTP($user);
                    return array('code' => 504, 'status' => 'error','message' => 'Your OTP has been expired! Please check your mail.');
                } else {
                    $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_name'] = $user['company_name'];
                    $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];
                    $_SESSION[SESSION_DOMAIN]['phone_number'] = $user['phone_number'];
                    $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $user['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($user['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['super_admin_name'] = '';
                    //$connection = DBConnection::dynamicDbConnection($user['host'], $user['database_name'], $user['db_username'], $user['db_password']);

                    $query = $this->companyConnection->query("SELECT * FROM active_inactive_status");

                    $active_inactive_status = $query->fetch();

                    $update_token_company= $this->companyConnection
                        ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                        ->execute([null, null, $user['id']]);

                    $update_token_superadmin= $this->conn
                        ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                        ->execute([null, null, $user['id']]);

                    $this->audiTrail($user,'login');

                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status);
                }

            } else  {
                return array('code' => 504, 'status' => 'error','message' => 'Your OTP is incorrect! Please check your mail.');
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * Function for company user logout
     * @return array
     */
    public function logout(){
        $url = $_POST['data'];
        $condition = ['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']];
//        print_r($condition); die('aaaa');
        $user = getSingleRecord($this->companyConnection,$condition,'users');

        if(!empty($url)){
            $update_last_url_company= $this->companyConnection
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['cuser_id']]);

            $update_last_url_super_admin= $this->conn
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['cuser_id']]);
        }
        $this->audiTrail($user['data'],'logout');
        unset($_SESSION[SESSION_DOMAIN]['cuser_id']);
        return array('status' => 'success', 'data' =>[$update_last_url_company, $update_last_url_super_admin]);
    }


    /**
     * function to create a user for super admin
     * @return array
     */
    public function createUser() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name','email','status','mobile_number'];
            /* Max length variable array */
            $maxlength_array = ['first_name'=>20,
                'mi'=>2,
                'last_name'=>20,
                'maiden_name'=>20,
                'nick_name'=>20,
                'email'=>50,
                'work_phone'=>12,
                'mobile_number'=>12,
                'fax'=>12];
            //Number variable array
            $number_array = ['work_phone', 'mobile_number', 'fax'];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['domain_name'] = $_SERVER['REQUEST_URI'];
                $password = randomString();
                $data['actual_password'] = $password;
                $data['password'] = md5($password);
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['user_type'] = 0;
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to update a user for super admin
     * @return array
     */



    /**
     * Update user password!
     * Use this endpoint to update the user password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changePassword(){
        try {
            $data=[];
            if ($_POST['id'] == "") {
                $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            }else{
                $data['id'] = $_POST['id'];
            }
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];


            //Required variable array
            $required_array = ['npassword','cpassword'];
            /* Max length variable array */
            $maxlength_array = ['npassword'=>50,'cpassword'=>50];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = createSqlColValPair($record);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * validate user current password!
     * Use this endpoint to validate the user current password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function checkCurrentPassword(){
        try {
            $data=[];
            $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['password'] = $_POST['password'];

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
                $id = $data['id'];

                $query = $this->conn->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Password do not match!');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    /**
     * Server side Validations
     * @author Deepak
     * @param $data
     * @param $db
     * @param array $required_array
     * @param array $maxlength_array
     * @param array $number_array
     * @return array
     */
    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }


    public function checkSecretKey()
    {
        try{
            $owner_id = $_POST['id'];
            $secretKey = $_POST['secretKey'];

            $getKey =  $this->companyConnection->query("SELECT portal_password_key FROM `users` where id =  $owner_id")->fetch();
            if(!empty($getKey) && $getKey['portal_password_key'] == $secretKey)
            {
                return array('status'=>'error','message'=>'Link has been expired.');
            } else {
                return array('status'=>'success','message'=>'Please set a new password for your portal.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function setNewPassword()
    {
        try{
            $owner_id = $_POST['id'];
            $secretKey = $_POST['secretkey'];
            $updateSecretKey = randomTokenString(8);
            $actual_Password = $_POST['password'];
            $confirm_password = $_POST['confirm_password'];
            $new_password = md5($_POST['password']);

            if($actual_Password!=$confirm_password) {
                return array('code' => 503, 'message'=>'New Password and Confirm Password are not same.','status'=>'error');
            }

            $getKey =  $this->companyConnection->query("SELECT portal_password_key FROM `users` where id =  $owner_id")->fetch();
            if($getKey['portal_password_key'] == $secretKey) {
                return array('status'=>'error','message'=>'Link has been expired.');
            }

            $query = "UPDATE users SET password='$new_password',actual_password='$actual_Password', portal_password_key='$secretKey' , portal_status='1' where id=".$owner_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code'=> 200, "message"=>"Password has been setup successfully","status"=>"success","function"=>'editTenantImage');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for owner forgot password
     */
    public function forgotPasswordEmail(){
        try {
            $email = $_POST['email'];
            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND user_type='3'");
            $user = $query->fetch();

            if ($user) {
                $forgot_password_token = randomTokenString(50);
                $email = $user['email'];

                $update_forgot_password_token = $this->companyConnection
                    ->prepare("UPDATE users SET reset_password_token=? WHERE email=?")
                    ->execute([$forgot_password_token, $email]);

                $reset_password_link = SUBDOMAIN_URL.'/VendorPortal/Login/ResetPassword?vendor_id='.$user['id'].'&token='.$forgot_password_token;
//                $logo = SUBDOMAIN_URL."/company/images/logo.png";
                $logo_url = SITE_URL.'company/images/logo.png';
                $body = getEmailTemplateData($this->companyConnection, 'forgetPasswordEmail_Key');
                $user_name = userName($user['id'],$this->companyConnection);;

                $body = str_replace("{CompanyLogo}",$logo_url,$body);
                $body = str_replace("{username}",$user_name,$body);
                $body = str_replace("{Url}",$reset_password_link,$body);

                $request['action']  = 'SendMailPhp';
                $request['to[]']    = $user['email'];
                $request['subject'] = 'Forgot Password!';
                $request['message'] = $body;
                $request['portal']  = '1';
                curlRequest($request);
                return (['status' => 'success', 'code' => 200, $update_forgot_password_token]);
            } else {
                return array( 'code' => 503, 'status' => 'error', 'message' => 'Details corresponding to this email-Id does not exist.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function portalLogout(){
        try {
            $url = $_POST['data'];
//            $condition = ['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']];
////            dd($condition);
////        print_r($condition); die('aaaa');
//            $user = getSingleRecord($this->companyConnection,$condition,'users');
//
//            if(!empty($user)){
//                $update_last_url= $this->companyConnection
//                    ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
//                    ->execute([$url, $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['portal_id']]);
//            }
            if(isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal'])){
                unset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']);
            }
            return array('status' => 'success', 'data' =>'', 'message' => 'Vendor Portal logout successfully');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for owner portal reset password
     */
    public function resetPassword() {
        try {
            $owner_id = $_POST['id'];
            $actual_password = $_POST['password'];
            $password = md5($_POST['password']);
            $forgot_password_token = $_POST['forgot_password_token'];
            $empty_forgot_password_token = NULL;

            $query = $this->companyConnection->query("SELECT * FROM users WHERE reset_password_token='$forgot_password_token' ");
            $user_email = $query->fetch();
            $email = $user_email['email'];
            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' ");
            $forgot_password_user = $query->fetch();

            if ($user_email) {
                if ($forgot_password_user) {
                    $email = $forgot_password_user['email'];

                    $update_forgot_password = $this->companyConnection
                        ->prepare("UPDATE users SET password=?, actual_password=? WHERE email=?")
                        ->execute([$password, $actual_password, $email]);

                    if ($update_forgot_password) {
                        return array('status' => 'success', 'message' => 'Password changed successfully.');
                    } else {
                        return array('status' => 'warning', 'message' => 'Password not changed successfully.');
                    }
                    //return json_decode($response);
                } else {
                    return array('status' => 'error', 'message' => 'This password reset token is invalid.');
                }
            } else {
                return array('status' => 'error', 'message' => 'No user exists with this email-Id.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }




}

$portalLoginAjax = new portalLoginAjax();
