<?php
include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
class billsAjax extends DBConnection {

    /**
     * billsAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Create new bill
     * @table company_bills
     * @return array
     * @throws Exception
     */
    public function create_new_bill(){
        try {
            $fileData = $_FILES;
            unset($fileData['file_library']);
            $billdata = $_POST;
            unset($billdata['action'],$billdata['class']);
            $billItemData = [];
            foreach($billdata as $key=>$value){
                if($key == 'building' || $key == 'unit' || $key == 'account' || $key == 'item_amount' || $key == 'item_description'){
                    $billItemData[$key] = $value;
                    unset($billdata[$key]);
                }
            }
            $noteData = $_POST['note'];
            unset($billdata['note']);
            $id = (isset($billdata['bill_id']) && !empty($billdata['bill_id']))?$billdata['bill_id']:NULL;
            unset($billdata['bill_id']);
            $billdata['bill_date'] = !empty($billdata['bill_date'])?mySqlDateFormat($billdata['bill_date'],NULL,$this->companyConnection):NULL;
            $billdata['due_date'] = !empty($billdata['due_date'])?mySqlDateFormat($billdata['due_date'],NULL,$this->companyConnection):NULL;
            $billdata['work_order'] = !empty($billdata['work_order'])?serialize($billdata['work_order']):NULL;
            $billdata['amount'] = !empty($billdata['amount'])?$this->parse_number($billdata['amount']):NULL;
            $billdata['tenant_id'] = !empty($billdata['tenant_id'])?$billdata['tenant_id']:NULL;
            $billdata['vendor_id'] = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];

            if(isset($billdata['imgName0'])){
                unset($billdata['imgName0']);
            }
            if(!empty($id)){
                $billdata['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlUpdateCase($billdata);
                $query = "UPDATE company_bills SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $exe = $stmt->execute($sqlData['data']);
                $message = SUCCESS_UPDATED;
            } else {
                $billdata['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $billdata['user_type'] = 'Vendor';
                $billdata['created_at'] = date('Y-m-d H:i:s');
                $billdata['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($billdata);
                $query = "INSERT INTO  company_bills (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($billdata);
                $message = SUCCESS_ADDED;
            }
            $last_id = !empty($id)?$id:$this->companyConnection->lastInsertId();
            //insert/update bill file library
            $this->file_library($fileData,$last_id);
            //insert/update bill notes
            $this->bill_notes($noteData,$last_id);
            //insert/update item data
            if(!empty($billItemData)) {
                $this->bill_items($billItemData, $last_id);
            }

            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => $message);
        } catch (PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * Save company files and document
     * @table company_bill_files
     * @param $files
     * @param $id
     * @return array
     */
    public function file_library($files,$id){
        try {
            unset($files['file_library']);
            if(!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
               //    print_r($file_name); die;
                    $fileData = getSingleRecord($this->companyConnection ,['column'=>'name','value'=>$file_name], 'company_bill_files');
                   // print_r($fileData);
                    if($fileData['code'] == '200'){
                        if($fileData['data']['id'] == $id) {
                            return array('code' => 500, 'status' => 'warning', 'message' => 'Document already exists.');
                        }
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }

                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['bill_id'] = $id;
                    $data['name'] = $file_name;
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_bill_files (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * Save Company bill notes
     * @table company_bill_notes
     * @param $notes
     * @param $id
     * @return array
     */
    public function bill_notes($notes,$id){
        try {
            if(!empty($notes)) {
                $query = "DELETE FROM company_bill_notes WHERE bill_id=$id";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                foreach ($notes as $key => $value) {
                    if(!empty($value)) {
                        $data = [];
                        $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $data['bill_id'] = $id;
                        $data['note'] = $value;
                        $data['created_at'] = date('Y-m-d H:i:s');
                        $data['updated_at'] = date('Y-m-d H:i:s');
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO company_bill_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                }
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_ADDED);
            }
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * Save company bill items
     * @table company_bill_items
     * @param $items
     * @param $id
     * @return array
     */
    public function bill_items($items,$id){
        try {
            if(!empty($items)) {
                $query = "DELETE FROM company_bill_items WHERE bill_id=$id";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
              //  dd($items['building']);
                foreach ($items['building'] as $key => $value) {
//                    dd($value);
                        $data = [];
                        $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $data['bill_id'] = $id;
                        $data['building_id'] = $items['building'][$key];
                        $data['unit_id'] = $items['unit'][$key];
                        $data['account'] = $items['account'][$key];
                        $data['amount'] = $this->parse_number($items['item_amount'][$key]);
                        $data['description'] = $items['item_description'][$key];
                        $data['created_at'] = date('Y-m-d H:i:s');
                        $data['updated_at'] = date('Y-m-d H:i:s');
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO company_bill_items (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
//                        dd($query);
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);

                }
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_ADDED);
            }
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * fetch all company bill details
     * @table company_bills
     * @param $notes
     * @param $id
     * @return array
     */
    public function getBillDetail(){
        try {
            $id = $_POST['id'];
            $query = 'SELECT company_bills.*,users.name,u1.name as tenant_name FROM company_bills LEFT JOIN users ON company_bills.vendor_id = users.id LEFT JOIN users as u1 ON company_bills.tenant_id = u1.id WHERE company_bills.id='.$id;
            $data = $this->companyConnection->query($query)->fetch();
            if(!empty($data)){
                foreach($data as $key=>$value){
                    if(!empty($value)) {
                        if ($key == 'bill_date' || $key == 'due_date') {
                            $data[$key] = dateFormatUser($value,NULL,$this->companyConnection);
                        } else if($key == 'work_order') {
                            $data[$key] = unserialize($value);
                        }
                    }
                }
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' =>$data);
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * fetch all company bill items
     * @table company_bill_items
     * @return array
     */
    public function getBillItems(){
        try {
            $id = $_POST['id'];
            $query = 'SELECT company_bill_items.*,company_bills.property_id FROM company_bill_items LEFT JOIN company_bills ON company_bill_items.bill_id=company_bills.id WHERE company_bill_items.bill_id='.$id;
            $data = $this->companyConnection->query($query)->fetchAll();
            $html = '';
            if(!empty($data)){
                foreach($data as $key=>$value){
                    $building_query = 'SELECT * FROM building_detail WHERE property_id='.$value['property_id'];
                    $building_data = $this->companyConnection->query($building_query)->fetchAll();
                    $building_data = $this->buildSelectOptions($value['building_id'],$building_data,'building_name');
                    $unit_query = 'SELECT * FROM unit_details WHERE building_id='.$value['building_id'];
                    $unit_data = $this->companyConnection->query($unit_query)->fetchAll();
                    $unit_data = $this->buildSelectOptions($value['unit_id'],$unit_data,'unit_no');
                    $account_query = 'SELECT concat(account_code,"-",account_name) AS account_name,id FROM company_chart_of_accounts WHERE status="1" AND deleted_at IS NULL';
                    $account_data = $this->companyConnection->query($account_query)->fetchAll();
                    $account_data = $this->buildSelectOptions($value['account'],$account_data,'account_name');

                    $html .= '<tr class="tableData">';
                    $html .= '<td width="20%"><select class="form-control building_select customBuildingValidation" name="building[]" data_required="true">'.$building_data.'</select><span class="error red-star customError"></span></td>';
                    $html .= '<td width="10%"><select class="form-control unit_select customUnitValidation" name="unit[]" data_required="true">'.$unit_data.'</select><span class="error red-star customError"></span></td>';
                    $html .= '<td width="30%"><select class="form-control account_select customAccountValidation" name="account[]" data_required="true">'.$account_data.'</select><span class="error red-star customError"></span></td>';
                    $html .= '<td width="15%"><input value="'.$value["amount"].'" placeholder="0.00" type="text" class="form-control customItemAmountValidation amount_num number_only item_amount" id="item_amount" name="item_amount[]" data_required="true"><span class="error red-star customError"></span></td>';
                    $html .= '<td width="15%"><input value="'.$value["description"].'" placeholder="Description" type="text" class="form-control customItemDescriptionValidation item_description" name="item_description[]" data_required="true"><span class="error red-star customError"></span></td>';
                    $html .= '<td width="10%"><i class="fa fa-times cursor crossRow" aria-hidden="true"></i></td>';
                    $html .= '</tr>';

                }
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' =>$html);
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * Build Select option for given data with selected option
     * @param $selected_id
     * @param $data
     * @param $column
     * @return string
     */
    public function buildSelectOptions($selected_id,$data,$column){
        try{
            if(!empty($data)){
                $html = '<option value="">Select</option>';
                foreach($data as $key=>$value){
                    if($value['id'] == $selected_id){
                        $html .= '<option value="' . $value['id'] . '" Selected>' . $value[$column] . '</option>';
                    } else {
                        $html .= '<option value="' . $value['id'] . '">' . $value[$column] . '</option>';
                    }
                }
                return $html;
            } else {
                $html = '<option value="">Select</option>';
            }
            return $html;
        } catch (PDOException $exception ) {

        }
    }

    /**
     * fetch all company bill note records
     * @return array
     */
    public function getBillNotes(){
        try {
            $id = $_POST['id'];
            $query = 'SELECT * FROM company_bill_notes WHERE bill_id='.$id;
            $data = $this->companyConnection->query($query)->fetchAll();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' =>$data);
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    /**
     * Parse string number to float
     * @param $number
     * @param null $dec_point
     * @return float
     */
    public function parse_number($number, $dec_point=null) {
        $number = str_replace(',','',$number);
        return floatval($number);
    }

    /**
     * function to delete company file and unlink
     * @return array
     */
    public function deleteFile(){
        try {
            $id = $_POST['id'];
            $data = getSingleRecord($this->companyConnection,['column'=>'id','value'=>$id],'company_bill_files');
            $query = "DELETE FROM company_bill_files WHERE id=$id";
            $stmt = $this->companyConnection->prepare($query);
            if($stmt->execute()){
                $location = isset($data['data']['file_location'])?$data['data']['file_location']: $data['data']['path'];
                $path = ROOT_URL . '/company/'.$location;
                unlink($path);
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_DELETE_DOC);
            } else {
                return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => ERROR_DELETE_DOC);
            }
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR,'message' => $e->getMessage());
        }
    }


    public function save_Recurring_bill(){
        try {

            $fileData = $_FILES;
            //print_r($fileData); die;
         //   unset($fileData['file_library']);
            $billdata = $_POST;
            unset($billdata['action'],$billdata['class']);
            $billItemData = [];

            foreach($billdata as $key=>$value){
                if($key == 'building' || $key == 'unit' || $key == 'account' || $key == 'item_amount' || $key == 'item_description'){
                    $billItemData[$key] = $value;
                    unset($billdata[$key]);
                }
            }
          // print_r($billdata['memo']); die;
            $noteData = !empty($_POST['note'])?$_POST['note']:'';
            unset($billdata['note']);
            $id = (isset($billdata['bill_id']) && !empty($billdata['bill_id']))?$billdata['bill_id']:NULL;
            unset($billdata['bill_id']);
           $billdata['bill_date'] = !empty($billdata['bill_date'])?mySqlDateFormat($billdata['bill_date'],NULL,$this->companyConnection):NULL;
            $billdata['due_date'] = !empty($billdata['due_date'])?mySqlDateFormat($billdata['due_date'],NULL,$this->companyConnection):NULL;
            $billdata['portfolio_id'] = !empty($billdata['portfolio'])?$billdata['portfolio']:NULL;
            $billdata['property_id'] = !empty($billdata['property'])?$billdata['property']:NULL;
            $billdata['work_order'] = !empty($billdata['work_order'])?serialize($billdata['work_order']):NULL;
            $billdata['amount'] = !empty($billdata['amount'])?$this->parse_number($billdata['amount']):NULL;
            $billdata['tenant_id'] = !empty($billdata['tenant_id'])?$billdata['tenant_id']:NULL;
            $billdata['address'] = !empty($billdata['address'])?$billdata['address']:NULL;
            $billdata['refrence_number'] = !empty($billdata['refrence_number'])?$billdata['refrence_number']:NULL;
            $billdata['change_to'] = !empty($billdata['change_to'])?$billdata['change_to']:NULL;
            $billdata['memo'] = !empty($billdata['memo'])?$billdata['memo']:NULL;
            $billdata['term'] = !empty($billdata['term'])?$billdata['term']:NULL;
            $billdata['status'] = 1;
                unset($billdata['portfolio']);
                unset($billdata['property']);
              //  unset($billdata['duration']);
            if(isset($billdata['imgName0'])){
                unset($billdata['imgName0']);
            }
        //    print_r($billdata); die;
            if(!empty($id)){
                $billdata['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlUpdateCase($billdata);
                $query = "UPDATE company_bills SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $exe = $stmt->execute($sqlData['data']);
                $message = SUCCESS_UPDATED;
            } else {
                $billdata['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $billdata['user_type'] = 'Vendor';
                $billdata['created_at'] = date('Y-m-d H:i:s');
                $billdata['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($billdata);
                $query = "INSERT INTO  company_bills (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($billdata);
                $message = SUCCESS_ADDED;
            }
           // $message = SUCCESS_ADDED;
            $last_id = !empty($id)?$id:$this->companyConnection->lastInsertId();
            //insert/update bill file library

           if(empty($fileData['file_library']['name'][0])){
               $query = "DELETE FROM company_bill_files WHERE id=$last_id";
               $stmt = $this->companyConnection->prepare($query);
               $stmt->execute();
           }
            $this->file_library($fileData,$last_id);
            //insert/update item data
            if(!empty($billItemData)) {
                $this->bill_items($billItemData, $last_id);
            }
            //insert/update bill notes
           $this->bill_notes($noteData,$last_id);
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => $message);
        } catch (PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function RecurringView(){
         try {
             $id = !empty($_POST['id'])?$_POST['id']:'';
             $Recurr['BillInfo'] = $this->companyConnection->query("SELECT cb.*,u.name  FROM `company_bills` as cb,`users` as u WHERE cb.vendor_id=u.id AND cb.id=$id")->fetch();
             $Recurr['BillItems'] = $this->companyConnection->query("SELECT *  FROM `company_bill_items` WHERE bill_id =$id")->fetch();
             $Recurr['BillNotes'] = $this->companyConnection->query("SELECT *  FROM `company_bill_notes` WHERE bill_id=$id")->fetch();
             $return = array('code' => 200, 'status' => 'success','data' => $Recurr, 'message' => 'Documents uploaded successfully.');
             echo json_encode($return); die();
         } catch (PDOException $e) {
                    return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
                }
    }

    public  function RecurringRecord_delete(){
        try {
            $id = !empty($_POST['id'])?$_POST['id']:'';
            $date = date('Y-m-d H:i:s');
            $query = "UPDATE `company_bills` set deleted_at = '$date'  WHERE id = $id";
          $stmt = $this->companyConnection->prepare($query);
            $recurr_delete  = $stmt->execute();
             $return = array('code' => 200, 'status' => 'success','data' => $recurr_delete, 'message' => 'Recurring Record delete successfully.');
            echo json_encode($return); die();
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }

    public function get_RecurringTransactionInfo(){
        try {
            $id = !empty($_POST['id'])?$_POST['id']:'';
            $Recurr['BillInfo'] = $this->companyConnection->query("SELECT cb.*,u.name  FROM `company_bills` as cb,`users` as u WHERE cb.vendor_id=u.id AND cb.id=$id")->fetch();
            $items = $this->getBillItems($_POST);
              $Recurr['BillItems'] = $items['data'];
         $notes   = $this->companyConnection->query("SELECT *  FROM `company_bill_notes` WHERE bill_id=$id")->fetchAll();
           $files  = $this->companyConnection->query("SELECT *  FROM `company_bill_files` WHERE bill_id=$id")->fetchAll();
           $html='';
            if(!empty($files)) {
                foreach ($files as $key => $value) {
                    $html .= '<div class="filesdata">';
                    $url = SITE_URL.'company/'.$value['path'];
                    $pdfimgurl = SITE_URL.'company/images/pdf.png';
                    $html .= '<a href="'.$url.'">';
                    $html .= '<img width="100" height="100" src="'.$pdfimgurl.'"></a>';
                    $html .= '</div>';
                }
            }

            $Recurr['BillFiles'] = $html;
            $notehtml ='';
             if(!empty($notes)){
             foreach ($notes as $key => $value) {
                 $notehtml .= '<textarea class="notes" placeholder="" name="note[]" >'.$value['note'].'</textarea>';
                 $notehtml .= '&nbsp;';
                 }
            }
             $Recurr['BillNotes'] = $notehtml;
             $return = array('code' => 200, 'status' => 'success','data' => $Recurr, 'message' => 'View data fetched successfully.');
            echo json_encode($return); die();
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }
    public function getVendorDetail(){
        try {
          //  dd("SELECT u.name,cb.bill_date,cb.amount,cb.term,cb.work_order,cb.due_date,cb.refrence_number,cb.invoice_number FROM company_bills as cb JOIN users as u ON u.id= cb.vendor_id WHERE cb.vendor_id=".$_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']);
            $query = "SELECT u.name,cb.bill_date,cb.amount,cb.term,cb.work_order,cb.due_date,cb.memo,cb.refrence_number,cb.invoice_number FROM company_bills as cb JOIN users as u ON u.id= cb.vendor_id WHERE cb.vendor_id=".$_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
            $data = $this->companyConnection->query($query)->fetch();

            if($data['work_order'] !="" && $data['work_order'] !="undefined"){
                $data['work_order']=unserialize($data['work_order']);
            }
            $data['bill_date']=dateFormatUser($data['bill_date'],null,$this->companyConnection);
            $data['due_date']=dateFormatUser($data['due_date'],null,$this->companyConnection);
        //   dd($data);
            return array('code' => 200, 'status' => "success", 'data' =>$data);
        } catch (PDOException $e) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $e);
        }
    }
}



$addVendor = new billsAjax();
?>
