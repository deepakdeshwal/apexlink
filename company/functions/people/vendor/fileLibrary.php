<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class fileLibrary extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function file_library(){
        try {
            $files = $_FILES;
            $vendor_id = $_POST['vendor_edit_id'];
            if(!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
                    $fileData = $this->companyConnection->query('SELECT name FROM tenant_file_library WHERE name="'.$file_name.'" AND user_id='.$vendor_id)->fetch();
                    if(!empty($fileData)){
                        return array('code' => 500, 'status' => 'warning', 'message' => 'File already exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['user_id'] = $vendor_id;
                    $data['name'] = $file_name;
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function deleteFile(){
        try {
            $id = $_POST['id'];
            $data = getSingleRecord($this->companyConnection,['column'=>'id','value'=>$id],'tenant_file_library');
            $query = "DELETE FROM tenant_file_library WHERE id=$id";
            $stmt = $this->companyConnection->prepare($query);
            if($stmt->execute()){
                $location = isset($data['data']['file_location'])?$data['data']['file_location']: $data['data']['path'];
                $path = ROOT_URL . '/company/'.$location;
                unlink($path);
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents deleted successfully.');
            } else {
                return array('code' => 500, 'status' => 'error', 'message' => 'Error occured while deleting document.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

}

$fileLibrary = new fileLibrary();
?>