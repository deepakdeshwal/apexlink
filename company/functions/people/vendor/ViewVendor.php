<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class ViewVendor extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public  function getGender($gen) {
        $gender = '';
        if ($gen == "1") {
            $gender = "Male";
        } else if ($gen == "2") {
            $gender = "Female";
        } else if ($gen == "3") {
            $gender = "Prefer Not To Say";
        } else if ($gen == "4") {
            $gender = "Other";
        }
        return $gender;
    }

    public function getVendorDetail() {
        $vendor_id = $_POST['vendor_id'];
        $id = (isset($_POST['vendor_id']) && !empty($_POST['vendor_id'])) ? $_POST['vendor_id'] : null;
        $sql_query1 = "SELECT users. * ,
                         tenant_phone.phone_number as phone,
                          tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_veteran_status.veteran as veteran_name
                        FROM users 
                         LEFT JOIN tenant_phone ON tenant_phone.user_id=users.id
                         LEFT JOIN tenant_ethnicity ON users.ethnicity=tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                       WHERE users.id=$vendor_id";

        $dataVendors['vendor_detail'] = $this->companyConnection->query($sql_query1)->fetch();
        $dataVendors['vendor_detail'] = getUnserializedData($dataVendors['vendor_detail'],'true');
        $dataVendors['vendor_detail']['custom_address'] = '';

        foreach ($dataVendors['vendor_detail'] as $key=>$value){
            if($key == 'address1' && $value != 'N/A'){
                $dataVendors['vendor_detail']['custom_address'] .= $value.', ';
            }

            if($key == 'address2' && $value != 'N/A'){
                $dataVendors['vendor_detail']['custom_address'] .= $value.', ';
            }

            if($key == 'address3' && $value != 'N/A'){
                $dataVendors['vendor_detail']['custom_address'] .= $value.', ';
            }

            if($key == 'address4' && $value != 'N/A'){
                $dataVendors['vendor_detail']['custom_address'] .= $value.', ';
            }
        }
        if($dataVendors['vendor_detail']['custom_address'] != '') {
            $dataVendors['vendor_detail']['custom_address'] = str_replace_last(', ', '', $dataVendors['vendor_detail']['custom_address']);
        } else {
            $dataVendors['vendor_detail']['custom_address'] = 'N/A';
        }

        if (!empty($dataVendors['vendor_detail']['salutation'])) {
            $salutation = $dataVendors['vendor_detail']['salutation'];
            // $vendor_cred_notice_period = [];

            if (!empty($salutation)) {
                $sal = getSalutation($salutation);

                $dataVendors['vendor_detail']['salutation'] = $sal;
            } else {
                $dataVendors['vendor_detail']['salutation'] = 'N/A';
            }
        } else {
            $dataVendors['vendor_detail']['salutation'] = 'N/A';
        }
        if (!empty($dataVendors['vendor_detail']['gender'])) {
            $gen = $dataVendors['vendor_detail']['gender'];
            // $vendor_cred_notice_period = [];

            if (!empty($gen)) {
                $getGender=$this->getGender($gen);

                $dataVendors['vendor_detail']['gender'] = $getGender;
            } else {
                $dataVendors['vendor_detail']['gender'] = 'N/A';
            }
        } else {
            $dataVendors['vendor_detail']['gender'] = 'N/A';
        }


        if (isset($dataVendors['vendor_detail']['hobbies']) && !empty($dataVendors['vendor_detail']['hobbies'])) {
            $hobbies = $dataVendors['vendor_detail']['hobbies'];
            $hobby_name = '';

            if ($hobbies != 'N/A' ) {
                foreach ($hobbies as $key => $value) {
                    $query = 'SELECT hobby FROM hobbies WHERE id=' . $value;
                    $groupData = $this->companyConnection->query($query)->fetch();
                    $name = $groupData['hobby'];
                    $hobby_name .= $name . ' , ';

                }
                $dataVendors['vendor_detail']['hobbies'] = str_replace_last(', ', '',
                    $hobby_name);

//                $dataVendors['vendor_detail']["hobbies"] = $hobby_name;
            } else {
                $dataVendors['vendor_detail']["hobbies"] = "N/A";
            }
        } else {
            $dataVendors['vendor_detail']["hobbies"] = "N/A";
        }

        $dataVendors['vendor_detail']['ssn_sin_id'] = $dataVendors['vendor_detail']['ssn_sin_id'];
        $fullname = $dataVendors['vendor_detail']['first_name'] . ' ' . $dataVendors['vendor_detail']['last_name'];
        $sql_query2 = "SELECT vendor_additional_detail. *,
                        account_name_details.account_name as acc_name,
                        company_chart_of_accounts.account_name as account_names,
                        company_vendor_type.vendor_type as vendor_types,
                        company_vendor_type.id as vendor_typess
                         FROM vendor_additional_detail 
                         LEFT JOIN account_name_details ON vendor_additional_detail.account_name=account_name_details.id
                         LEFT JOIN company_chart_of_accounts ON vendor_additional_detail.default_GL=company_chart_of_accounts.id
                          LEFT JOIN company_vendor_type ON vendor_additional_detail.vendor_type_id=company_vendor_type.id
                         WHERE vendor_additional_detail.vendor_id=$vendor_id";

        $dataVendor['vendor_additional_detail'] = $this->companyConnection->query($sql_query2)->fetch();
        $dataVendor['vendor_additional_detail']['email'] = unserialize($dataVendor['vendor_additional_detail']['email']);
//dd($dataVendor['vendor_additional_detail']['acc_name']);

        $randomnumner = $dataVendor['vendor_additional_detail']['vendor_random_id'];

        $vendor_detaildob=(isset($dataVendors['vendor_detail']['dob']) && $dataVendors['vendor_detail']['dob'] != 'N/A' && !empty($dataVendors['vendor_detail']['dob']))?dateFormatUser($dataVendors['vendor_detail']['dob'],'1',$this->companyConnection):'N/A';

        $viewEmergencyInfo = $this->getViewEmergencyInfo($id);

        $viewCredentialInfo = $this->getViewcredentialInfo($id);

//        $sqlphone = "SELECT * FROM  company_vendor_type WHERE `id` =".$dataVendor['vendor_additional_detail'][''];
//        $dataphone = $this->companyConnection->query($sqlphone)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'dataVendors' => $dataVendors, 'dataVendor' => $dataVendor, 'fullname' => $fullname, 'randomnumner' => $randomnumner, 'message' => 'Record retrieved Successfully.', 'emergencyInfo' => $viewEmergencyInfo, 'viewCredentialInfo' => $viewCredentialInfo,'vendor_detaildob'=>$vendor_detaildob);
    }

    public function getViewEmergencyInfo($tenant_id) {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();


        $html = "";


        if (empty($getEmergencyInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                    <span></span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
        } else {

            foreach ($getEmergencyInfo as $key => $value) {


                if (!empty($value['emergency_relation'])) {
                    $e_relation = $value['emergency_relation'];
                    // $vendor_cred_notice_period = [];

                    if (!empty($e_relation)) {
                        $emergency_rel = emergencyRelation($e_relation);

                        $value['emergency_relation'] = $emergency_rel;
                    } else {
                        $value['emergency_relation'] = 'N/A';
                    }
                } else {
                    $value['emergency_relation'] = 'N/A';
                }

                $value = getUnserializedData($value,'true');


                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $value['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>+' . $value['emergency_country_code'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $value['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                   <span>' . $value['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $value['emergency_relation'] . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
            }
        }

        return $html;
    }

    public function getViewcredentialInfo($tenant_id) {
        $getcredentialInfo = $this->companyConnection->query("SELECT tenant_credential. *, 
                                   tenant_credential_type.credential_type as credential_types                                   
                                FROM tenant_credential                               
                                  LEFT JOIN tenant_credential_type ON tenant_credential.credential_type=tenant_credential_type.id
                                 WHERE tenant_credential.user_id ='" . $tenant_id . "' ")->fetchAll();

        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if (empty($getcredentialInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="col-xs-12">
                                        <label class="text-right">Credential Name :</label>
                                        <span>N/A</span>
                                    </div>
                                    <div class="col-xs-12">
                                        <label class="text-right">Credential Type  :</label>
                                        <span>N/A</span>
                                    </div>
                                    <div class="col-xs-12">
                                        <label class="text-right"> Acquire Date:</label>
                                         <span>N/A</span>
                                    </div>
                                                                                   
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-xs-12">
                                        <label class="text-right">Expiration Date :</label>
                                       <span>N/A</span>
                                    </div>
                                    <div class="col-xs-12">
                                        <label class="text-right"> Notice Period :</label>
                                         <span>N/A</span>
                                    </div>
                                                                                     
                                </div>
                            </div>
                        </div>';
        } else {


            foreach ($getcredentialInfo as $key => $value) {

                if (!empty($value['notice_period'])) {
                    $cred_notice_period = $value['notice_period'];

                    if (!empty($cred_notice_period)) {
                        $cred_period = noticeperiod($cred_notice_period);

                        $value['notice_period'] = $cred_period;
                    } else {
                        $value['notice_period'] = 'N/A';
                    }
                } else {
                    $value['notice_period'] = 'N/A';
                }
                $value = getUnserializedData($value,'true');
                $val1 = (isset($value['acquire_date']) && $value['acquire_date'] != 'N/A' && !empty($value['acquire_date']))?dateFormatUser($value['acquire_date'],'1',$this->companyConnection):'';
                $val2 = (isset($value['expire_date']) && $value['expire_date'] != 'N/A' && !empty($value['expire_date']))?dateFormatUser($value['expire_date'],'1',$this->companyConnection):'';

                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Name :</label>
                                                                    <span>' . $value['credential_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Type  :</label>
                                                                    <span>' . $value['credential_types'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right"> Acquire Date:</label>
                                                                     <span>' . $val1 . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Expiration Date :</label>
                                                                   <span>' . $val2 . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right"> Notice Period :</label>
                                                                     <span>' . $value['notice_period'] . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
            }
        }



        return $html;
    }

    public function create_flag() {
        try {
//            dd($_POST);

            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $flag_id = !empty($data['id']) ? $data['id'] : null;
                unset($data['id']);
                $data['date'] = !empty($data['date']) ? mySqlDateFormat($data['date'], null, $this->companyConnection) : null;
                $data['object_type'] = 'vendor';
                $data['object_id'] = $_POST['vendor_id'];
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                if (empty($flag_id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO flags (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($data);
                    $message = 'Record Added Successfully.';
                } else {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['completed'] = ($data['completed'] == 0) ? '0' : '1';
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE flags SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $flag_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($sqlData['data']);
                    $message = 'Record updated successfully.';
                }
                if ($exe) {
                    $updateData = [];
                    $updateData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlUpdateCase($updateData);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id=".$_POST['vendor_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $message);
                } else {
                    return array('code' => 500, 'status' => 'error', 'message' => 'Internal Server Error!');
                }
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }


    /**
     * fuinction to fetch complaint type
     * @return array
     */


    public function fetchAllcomplaintType() {
        $html = '';
        $sql = "SELECT * FROM complaint_types";
        $data = $this->companyConnection->query($sql)->fetchAll();

        $html = '<option value="">Select Complaint Type</option>';
        foreach ($data as $k=>$v) {
//DD($v['status']);

            if ($v['id'] == '0') {


                $html.= "<option value='" . $v['id'] . "' selected>" . $v['complaint_type'] . "</option>";
            } else {
                $html.= "<option value='" . $v['id'] . "'>" . $v['complaint_type'] . "</option>";
            }
        }
        return array('data' => $html, 'status' => 'success');
    }




    public function addComplaintType() {
        try {
            $vendor_id = $_POST['vendor_id'];

            $data = $_POST['form'];

            $data = postArray($data, 'true');
            $insert = [];
            $insert['user_id'] = $vendor_id;

            $insert['complaint_type'] = $data['complaint_type'];

//                $insert['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
            $insert['status'] = '1';
            $insert['deleted_at'] = date('Y-m-d H:i:s');
            $insert['created_at'] = date('Y-m-d H:i:s');
            $insert['updated_at'] = date('Y-m-d H:i:s');
            $id=null;
            //check if custom field already exists or not
            $duplicate = checkNameAlreadyExists($this->companyConnection, 'complaint_types', 'complaint_type', $insert['complaint_type'], $id);
            if ($duplicate['is_exists'] == 1) {
                if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                    return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Complaint Type already exists!');
                }
            }

            //Save Data in Company Database
            $sqlData = createSqlColVal($insert);
            $query = "INSERT INTO complaint_types (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($insert);
            $id = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            //}
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }




    public function addNewComplaint() {
        try {

            $vendor_id = $_POST['vendor_id'];
            $data = $_POST['form'];
            dd($_POST);
            $data = postArray($data, "false");

            $complaint_id = $data["edit_complaint_id"];
            if (isset($complaint_id) && empty($complaint_id)) {
                $complaint_data = [];
                $complaint_data["object_id"] = $vendor_id;
                $complaint_data["module_type"] = 'vendor';
                $complaint_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $complaint_data['status'] = 1;
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : null;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : null;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["created_at"] = date('Y-m-d H:i:s');
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $complaint_data["complaint_by_about"] =(isset($data['complaint_by_about']) && !empty($data['complaint_by_about'])) ? $data['complaint_by_about'] : null;
                $sqlData = createSqlColVal($complaint_data);
                //Save Data in Company Database

                $query = "INSERT INTO  complaints (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($complaint_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully');
                }
            } else {
                $complaint_data = [];
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : null;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : null;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $complaint_data["complaint_by_about"] =$data['complaint_by_about'];
                $sqlData = createSqlColValPair($complaint_data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id='$complaint_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }
    }





    public function getComplaintDetail() {
        try {
            $id = $_POST['id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');
            if (!empty($record["data"]["complaint_date"])) {
                $record["data"]["complaint_date"] = !empty($record["data"]["complaint_date"])?dateFormatUser($record["data"]["complaint_date"], null, $this->companyConnection):null;
            }
            return ['status' => 'success', 'code' => 200, 'data' => $record, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }





    public function deleteComplaint() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');

            $sql = "UPDATE complaints SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            echo json_encode(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }




    public function getComplaintsDataForPrint() {
        try {
            $vendor_id = $_POST['vendor_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $vendor_id], 'users');

            $address1 = isset($user_data['data']['address1']) ? $user_data['data']['address1'] : '';
            $address2 = isset($user_data['data']['address2']) ? $user_data['data']['address2'] : '';
            $address3 = isset($user_data['data']['address3']) ? $user_data['data']['address3'] : '';
            $address4 = isset($user_data['data']['address4']) ? $user_data['data']['address4'] : '';
            $user_data['data']['vendor_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;
            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` WHERE `id` IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();

            $print_complaints_html = '';
            foreach ($data as $key => $value) {
                $record = '';
                if(isset($value['complaint_type_id'])){
                    $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $value['complaint_type_id']], 'complaint_types');
                }

                $value['complaint_type_name'] = isset($record['data']) ? $record['data']['complaint_type'] : '';
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="../company/images/logo.png"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> '.$value['complaint_by_about'].' </td>
                      </tr>
                    </table>     
                    <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                            <tr>
                                <td width="30%" style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Name:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                   '.$user_data['data']['name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Address:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$user_data['data']['vendor_address'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Complaint Type:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$value['complaint_type_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Description:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$value['complaint_note'].'
                                </td>
                            </tr>                          
                        </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                        bgcolor="#585858">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
              </table>';
            }
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }





    public function getComplaintsDataForEmail($complaint_id, $vendor_id) {
      //  $vendor_id=$_POST['vendor_id'];
        try {
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $vendor_id], 'users');

            $address1 = isset($user_data['data']['address1']) ? $user_data['data']['address1'] : '';
            $address2 = isset($user_data['data']['address2']) ? $user_data['data']['address2'] : '';
            $address3 = isset($user_data['data']['address3']) ? $user_data['data']['address3'] : '';
            $address4 = isset($user_data['data']['address4']) ? $user_data['data']['address4'] : '';
            $user_data['data']['vendor_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;
            $complaint_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_id], 'complaints');
            $print_complaints_html = '';
            $record = '';
            if(isset($complaint_data['data']['complaint_type_id'])){
                $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_data['data']['complaint_type_id']], 'complaint_types');
            }
            $complaint_data['data']['complaint_type_name'] = isset($record['data']) ? $record['data']['complaint_type'] : '';
            $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="#logo#"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> '.$complaint_data['data']['complaint_by_about'].' </td>
                      </tr>
                    </table>     
                    <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                            <tr>
                                <td width="30%" style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Name:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                   '.$user_data['data']['name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Address:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$user_data['data']['vendor_address'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Complaint Type:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_type_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Description:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_note'].'
                                </td>
                            </tr>                          
                        </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                        bgcolor="#585858">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
              </table>';
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }




    public function SendComplaintMail()
    {
        try{
            $data = $_POST;
            $vendor_id = $data['vendor_id'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $user_detail = $_SESSION['SESSION_'.$subdomain];
            $toUsers = $user_detail['email'].','. $data['vendor_email'];
            $to_arr =  explode(",",$toUsers);
            $complaint_id = $data['complaint_ids'];
            $result = '';
            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            foreach ($complaint_id as $k => $v){
                $body = $this->getComplaintsDataForEmail($v,$vendor_id);
                $request['action']  = 'SendMailPhp';
                $request['to']      = $to_arr;
                $request['subject'] = 'Send Vendor Complaint!';
                $body['html']       = str_replace("#logo#",$logo,$body['html']);
                $request['message'] = $body['html'];
                $request['portal']  = '1';
                $result = curlRequest($request);
            }
            return ['status' => 'success', 'code' => 200, 'data' => $result];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



    /**
     * fuinction to retrive flag data
     * @return array
     */
    public function get_flags() {
        try {
            $id = $_POST['id'];
            $sql = "SELECT * FROM flags WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                $data['date'] = (!empty($data['date'])) ? dateFormatUser($data['date'], null, $this->companyConnection) : '';
                $flag_data = array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                $flag_data = array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to soft delete flag data
     * @return array
     */
    public function deleteFlag() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['deleted_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE flags SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if ($exe) {
                $flag_data = array('code' => 200, 'status' => 'success', 'message' => 'Record Deleted successfully.');
            } else {
                $flag_data = array('code' => 500, 'status' => 'error', 'message' => 'Internal Server Error!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to update completed status
     * @return array
     */
    public function flagCompleted() {
        try {
            $id = $_POST['id'];
            $data = [];
            $data['completed'] = '1';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE flags SET " . $sqlData['columnsValuesPair'] . " WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if ($exe) {
                $flag_data = array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');
            } else {
                $flag_data = array('code' => 500, 'status' => 'error', 'message' => 'Internal Server Error!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }

    public function getCountrycode() {
        try {
            $data = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $data);
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }

    public function getComplaintsData() {
        //delete
        try {
            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` WHERE `id` IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $print_complaints_html = '';
            foreach ($data as $key => $value) {
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="../company/images/logo.png"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Complaint by Building </td>
                      </tr>
                    </table>
                    <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">
                      <tr>
                        <td>Address: </td>
                        <td>Achme2</td>
                      </tr>
                      <tr>
                        <td>Name: </td>
                        <td>Dummy Address</td>
                      </tr>
                      <tr>
                        <td>Complaint Type: </td>
                        <td>Fencing Issue</td>
                      </tr>
                      <tr>
                        <td>Description: </td>
                        <td>Lorem ipsum is dummy text.</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
                    Apexlink.apexlink@yopmail.com
                    
                  </td>
                </tr>
              </table>';
            }

            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getCustomFieldData() {
        try {
            $id = $_POST['id'];
            $sql = "SELECT custom_fields FROM users WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                $custom_data = (isset($data['custom_fields']) && !empty($data['custom_fields']))?unserialize($data['custom_fields']):null;
                $flag_data = array('code' => 200, 'status' => 'success', 'data' => $custom_data, 'message' => 'Record retrieved successfully');
            } else {
                $flag_data = array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
}

$ViewVendor = new ViewVendor();
?>