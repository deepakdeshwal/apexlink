<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class editOwnerAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function getIntialData($internal = null){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['property_list'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = 1")->fetchAll();

            foreach ($data['property_list'] as $key=>$value){
                if(!empty($value)){
                    $propertyPercent = $this->companyConnection->query("SELECT SUM(property_percent_owned) FROM owner_property_owned WHERE property_id=".$value['id'])->fetch();
                    $data['property_list'][$key]['property_percent_owned'] = !empty($propertyPercent['SUM(property_percent_owned)'])?$propertyPercent['SUM(property_percent_owned)']:0;
                }
            }
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity")->fetchAll();
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users` WHERE id=$id")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types` ORDER BY complaint_type ASC")->fetchAll();
            $data['account_name'] = $this->companyConnection->query("SELECT id,account_name FROM `owner_account_name`")->fetchAll();
            $data['chart_accounts'] = $this->companyConnection->query("SELECT id,account_name,account_code,is_default FROM company_chart_of_accounts WHERE status = '1'")->fetchAll();
            $data['account_type'] = $this->companyConnection->query("SELECT id,account_type_name,range_from,range_to,status,is_default FROM `company_account_type`")->fetchAll();
//            dd($data['property_list']);
            if($internal)
            {
                return $data;
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getviewData(){
        try{

            $owner_id=$_POST['owner_id'];
            $data = [];
            $sql_query2 = "SELECT * FROM owner_details WHERE owner_details.user_id=$owner_id";

            $data['owner_detail'] =$this->companyConnection->query($sql_query2)->fetch();

            $data['owner_detail']['email']= isset($data['owner_detail']['email']) ? unserialize($data['owner_detail']['email']) : '';

            $data['owner_detail']['contact_country_code_data']=(isset($data['owner_detail']['contact_country_code']) ? unserialize($data['owner_detail']['contact_country_code']) : '');
            $data['owner_detail']['contact_phone_number_data']=(isset($data['owner_detail']['contact_phone_number']) ? unserialize($data['owner_detail']['contact_phone_number']) : '');
            $data['owner_detail']['contact_phone_type_data']=(isset($data['owner_detail']['contact_phone_type']) ? unserialize($data['owner_detail']['contact_phone_type']) : '');

            $data['owner_detail']['emergency_contact_name'] = isset($data['owner_detail']['emergency_contact_name']) ? unserialize($data['owner_detail']['emergency_contact_name']): '';
            $data['owner_detail']['emergency_other_relation'] = isset($data['owner_detail']['emergency_other_relation']) ? unserialize($data['owner_detail']['emergency_other_relation']): '';
            $data['owner_detail']['emergency_email'] = isset($data['owner_detail']['emergency_email']) ? unserialize($data['owner_detail']['emergency_email']): '';
            $data['owner_detail']['emergency_phone_number'] = isset($data['owner_detail']['emergency_phone_number']) ? unserialize($data['owner_detail']['emergency_phone_number']) : '';
            if(isset($data['owner_detail']['emergency_relation']) && !empty($data['owner_detail']['emergency_relation'])) {
                $e_relation = unserialize($data['owner_detail']['emergency_relation']);
                $owner_emergency_relation = [];
                $data['owner_detail']['emergency_relation_new'] = ($data['owner_detail']['emergency_relation']) ? unserialize($data['owner_detail']['emergency_relation']):'';
                if (!empty($e_relation)){
                    foreach ($e_relation as $k => $v){
                        $emergency_rel = emergencyRelation($v);
                        if($emergency_rel == 'Other'){
                            $emergency_rel = $data['owner_detail']['emergency_other_relation'][$k];
                        }

                        array_push($owner_emergency_relation,$emergency_rel);
                    }
                    $data['owner_detail']["emergency_relation"] = $owner_emergency_relation;
                } else {
                    $data['owner_detail']['emergency_relation'] = '';
                }



            } else {
                $data['owner_detail']['emergency_relation'] = '';
            }
            $data['owner_detail']['edit_emergency_code'] = unserialize($data['owner_detail']['emergency_country_code']);
            if(isset($data['owner_detail']['emergency_country_code']) && !empty($data['owner_detail']['emergency_country_code'])) {
                $e_country_code = unserialize($data['owner_detail']['emergency_country_code']);
                $country_code_value = [];

                if (!empty($e_country_code)){
                    foreach ($e_country_code as $k => $v){
                        if($v)
                        {
                            $query = 'SELECT code FROM countries WHERE id="'.$v.'"';
                            $country_code = $this->companyConnection->query($query)->fetch();
                            array_push($country_code_value,$country_code['code']);
                        } else {
                            array_push($country_code_value,'');
                        }

                    }

                    $data['owner_detail']["emergency_country_code"] = $country_code_value;
                } else {
                    $data['owner_detail']['emergency_country_code'] = '';
                }
            } else {
                $data['owner_detail']['emergency_country_code'] = '';
            }

            $emergency_data = [];

            if ($data['owner_detail']['emergency_contact_name']) {
                $length = count($data['owner_detail']['emergency_contact_name']);
                for ($i = 0; $i < $length; $i++) {
                    $emergency_data[$i]['emergency_contact_name'] = (isset($data['owner_detail']['emergency_contact_name'][$i])?$data['owner_detail']['emergency_contact_name'][$i]:'');
                    $emergency_data[$i]['emergency_relation'] = (isset($data['owner_detail']['emergency_relation'][$i])?$data['owner_detail']['emergency_relation'][$i]:'');
                    $emergency_data[$i]['emergency_country_code'] = (isset($data['owner_detail']['emergency_country_code'][$i])?$data['owner_detail']['emergency_country_code'][$i]:'');
                    $emergency_data[$i]['emergency_phone_number'] = (isset($data['owner_detail']['emergency_phone_number'][$i])?$data['owner_detail']['emergency_phone_number'][$i]:'');
                    $emergency_data[$i]['emergency_email'] = (isset($data['owner_detail']['emergency_email'][$i])?$data['owner_detail']['emergency_email'][$i]:'');
                }
            }


            $data['owner_detail']['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date']) ? unserialize($data['owner_detail']['owner_credential_expiration_date']) : '';
            $data['owner_detail']['owner_credential_name'] = isset($data['owner_detail']['owner_credential_name']) ? unserialize($data['owner_detail']['owner_credential_name']) : '';
            $data['owner_detail']['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date']) ? unserialize($data['owner_detail']['owner_credential_acquire_date']) : '';
            $data['owner_detail']['owner_credential_notice_period'] =  isset($data['owner_detail']['owner_credential_notice_period']) ? unserialize($data['owner_detail']['owner_credential_notice_period']) : '';
            $data['owner_detail']['owner_credential_type'] = isset($data['owner_detail']['owner_credential_type']) ? unserialize($data['owner_detail']['owner_credential_type']) : '';
            $credential_data = [];
            if ($data['owner_detail']['owner_credential_name']) {
                $len = count($data['owner_detail']['owner_credential_name']);
                for ($i = 0; $i < $len; $i++) {
//                    dd($data['owner_detail']['owner_credential_expiration_date'][$i]);
                    $credential_data[$i]['owner_credential_name'] = (isset($data['owner_detail']['owner_credential_name'][$i]) ? $data['owner_detail']['owner_credential_name'][$i] : '');
                    $credential_data[$i]['owner_credential_type'] = isset( $data['owner_detail']['owner_credential_type'][$i])? $data['owner_detail']['owner_credential_type'][$i] : '';
                    $credential_data[$i]['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_expiration_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_acquire_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_notice_period'] = isset($data['owner_detail']['owner_credential_notice_period'][$i])?$data['owner_detail']['owner_credential_notice_period'][$i]:'';
                }
            }

            $sql_query1 = "SELECT users. *,
                           tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_veteran_status.veteran as veteran_name,
                           tenant_veteran_status.veteran as veteran_name
                        FROM users 
                        LEFT JOIN tenant_ethnicity ON users.ethnicity=tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                        WHERE users.id=$owner_id";


            $data['user_detail'] =$this->companyConnection->query($sql_query1)->fetch();

//            $data['user_detail']['name'] = userName($owner_id, $this->companyConnection);
            $address1 = isset($data['user_detail']['address1']) ? $data['user_detail']['address1'] : '';
            $address2 = isset($data['user_detail']['address2']) ? $data['user_detail']['address2'] : '';
            $address3 = isset($data['user_detail']['address3']) ? $data['user_detail']['address3'] : '';
            $address4 = isset($data['user_detail']['address4']) ? $data['user_detail']['address4'] : '';
            $data['user_detail']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $data['user_detail']['phone_number_note'] = isset($data['user_detail']['phone_number_note']) ? $data['user_detail']['phone_number_note'] : 'N/A';
            $data['credential_data'] = $credential_data;


            if(isset($data['user_detail']['hobbies']) && !empty($data['user_detail']['hobbies'])) {
                $hobbies = isset($data['user_detail']['hobbies']) ? unserialize($data['user_detail']['hobbies']) : '';
                $hobby_name='';

                if(!empty($hobbies)){
                    foreach ($hobbies as $key => $value){
                        $query = 'SELECT hobby FROM hobbies WHERE id='.$value;
                        $groupData = $this->companyConnection->query($query)->fetch();
                        $name = $groupData['hobby'];
                        $hobby_name .= $name.',';
                    }
                    $data['user_detail']["hobbies"] = $hobby_name;
                }else{
                    $data['user_detail']["hobbies"] = "";
                }
            }else{
                $data['user_detail']["hobbies"] = "";
            }

            $data['banking_detail'] =$this->companyConnection->query("SELECT * FROM users WHERE id=$owner_id")->fetch();
            if(!empty($data['user_detail']) && count($data['user_detail']) > 0){
                $custom_data = !empty($data['user_detail']['custom_fields']) ? unserialize($data['user_detail']['custom_fields']) : [];
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['custom_data'] = $custom_data;
            }

            $data['user_detail']['dob'] = isset($data['user_detail']['dob']) ? dateFormatUser($data['user_detail']['dob'],null,$this->companyConnection) : '';
            $data['user_detail']['current_date'] = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
            $data['user_detail']['ssn_sin_id'] = isset($data['user_detail']['ssn_sin_id']) ? unserialize($data['user_detail']['ssn_sin_id']) : 'N/A';

            $data['intial_data'] = $this->getIntialData(true);
            //Owner Banking Information
            $data['owner_banking_information']  = [];
            $data['owner_banking_information'] =$this->companyConnection->query("SELECT * FROM owner_banking_information WHERE user_id=$owner_id")->fetchAll();

            //Owner Property Owned Information
            $data['owner_property_owned_info']  = [];
            $data['owner_property_owned_info'] =$this->companyConnection->query("SELECT * FROM owner_property_owned WHERE user_id=$owner_id")->fetchAll();

            $data['tenant_chargenote']  = [];
            $data['tenant_chargenote'] =$this->companyConnection->query("SELECT * FROM tenant_chargenote WHERE user_id=$owner_id")->fetchAll();

            $tenant_phone_query= $this->companyConnection->query("SELECT * FROM tenant_phone WHERE tenant_phone.user_id=$owner_id ORDER BY id ASC")->fetchAll();
            $data['owner_detail']['contact_carrier_data']=$tenant_phone_query;
//            $data['owner_phone_Info'] = $this->getPhoneInfo($owner_id);
            return ['code'=>200, 'status'=>'success', 'data'=>$data ,'emergency_data'=>$emergency_data,'credential_data'=>$credential_data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * fuinction to fetch complaint type
     * @return array
     */

//
//    public function fetchAllcomplaintType() {
//        $html = '';
//        $sql = "SELECT * FROM complaint_types";
//        $data = $this->companyConnection->query($sql)->fetchAll();
//
//        $html = '<option value="default">Select Complaint Type</option>';
//        foreach ($data as $k=>$v) {
////DD($v['status']);
//
//            if ($v['id'] == '0') {
//
//
//                $html.= "<option value='" . $v['id'] . "' selected>" . $v['complaint_type'] . "</option>";
//            } else {
//                $html.= "<option value='" . $v['id'] . "'>" . $v['complaint_type'] . "</option>";
//            }
//        }
//        return array('data' => $html, 'status' => 'success');
//    }


    public function fetchAllComplaintType() {
        $html = '';
        $otherHtml = '';
        $sql = "SELECT * FROM complaint_types WHERE status = 1 ORDER BY complaint_type asc ";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .= "<option value='' selected disabled hidden>Select</option>";
        foreach ($data as $d) {
            if($d["id"] == '18') {
                $otherHtml .= '<option value=' . $d["id"] . '>' . @$d['complaint_type'] . '</option>';
            } else {
                $html .= '<option value=' . $d["id"] . '>' . @$d['complaint_type'] . '</option>';
            }
        }
        $html .= $otherHtml;

        return array('data' => $html, 'status' => 'success');
    }

    public function addEditComplaint(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"false");
            $owner_edit_id = $_POST["owner_edit_id"];
            $complaint_id = $data["edit_complaint_id"];
            if(isset($complaint_id) && empty($complaint_id)) {
                $complaint_data = [];
                $complaint_data["object_id"] = $owner_edit_id;
                $complaint_data["module_type"] = 'owner';
                $complaint_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $complaint_data['status'] = 1;
                $complaint_data["complaint_by_about"] = (isset($data['complaint_by_about']) && !empty($data['complaint_by_about'])) ? $data['complaint_by_about'] : '';
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["created_at"] = date('Y-m-d H:i:s');
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($complaint_data);
                //Save Data in Company Database

                $query = "INSERT INTO  complaints (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($complaint_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record not added due to some technical error.');
                }
            } else{
                $complaint_data = [];
                $complaint_data["complaint_by_about"] = (isset($data['complaint_by_about']) && !empty($data['complaint_by_about'])) ? $data['complaint_by_about'] : '';
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                if($complaint_data["complaint_type_id"] == NULL){
                    unset($complaint_data["complaint_type_id"]);
                }
                $sqlData = createSqlColValPair($complaint_data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id='$complaint_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }
    }

    public function getComplaintDetailById() {
        try {
            $id = $_POST['id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');
            if(!empty($record["data"]["complaint_date"])){
                $record["data"]["complaint_date"] =  dateFormatUser($record["data"]["complaint_date"],null,$this->companyConnection);
            }

            return $record;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
        }
    }

    public function deleteComplaint() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql  = "UPDATE complaints SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return array('code' => 200, 'status' => 'success', 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getComplaintsDataForPrint() {
        try {
            $owner_id = $_POST['owner_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $owner_id], 'users');

            $address1 = isset($user_data['data']['address1']) ? $user_data['data']['address1'] : '';
            $address2 = isset($user_data['data']['address2']) ? $user_data['data']['address2'] : '';
            $address3 = isset($user_data['data']['address3']) ? $user_data['data']['address3'] : '';
            $address4 = isset($user_data['data']['address4']) ? $user_data['data']['address4'] : '';
            $user_data['data']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` WHERE `id` IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $print_complaints_html = '';
            foreach ($data as $key => $value) {
                $record = '';
                if(isset($value['complaint_type_id'])){
                    $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $value['complaint_type_id']], 'complaint_types');
                }
                $value['complaint_type_name'] = isset($record['data']) ? $record['data']['complaint_type'] : '';
                $print_complaints_html .= '
            <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="../company/images/logo.png"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> '.$value['complaint_by_about'].' </td>
                      </tr>
                    </table>     
                    <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                            <tr>
                                <td width="30%" style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Name:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                   '.$user_data['data']['name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Address:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$user_data['data']['owner_address'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Complaint Type:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$value['complaint_type_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Description:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$value['complaint_note'].'
                                </td>
                            </tr>                          
                        </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                        bgcolor="#585858">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
              </table>';
            }
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getComplaintsDataForEmail($complaint_id, $owner_id) {
        try {
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $owner_id], 'users');

            $address1 = isset($user_data['data']['address1']) ? $user_data['data']['address1'] : '';
            $address2 = isset($user_data['data']['address2']) ? $user_data['data']['address2'] : '';
            $address3 = isset($user_data['data']['address3']) ? $user_data['data']['address3'] : '';
            $address4 = isset($user_data['data']['address4']) ? $user_data['data']['address4'] : '';
            $user_data['data']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $complaint_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_id], 'complaints');
            $print_complaints_html = '';

            $record = '';
            if(isset($complaint_data['data']['complaint_type_id'])){
                $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_data['data']['complaint_type_id']], 'complaint_types');
            }
            $complaint_data['data']['complaint_type_name'] = isset($record['data']) ? $record['data']['complaint_type'] : '';
            $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="#logo#"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> '.$complaint_data['data']['complaint_by_about'].' </td>
                      </tr>
                    </table>     
                    <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                            <tr>
                                <td width="30%" style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Name:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                   '.$user_data['data']['name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Address:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$user_data['data']['owner_address'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Complaint Type:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_type_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Description:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_note'].'
                                </td>
                            </tr>                          
                        </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                        bgcolor="#585858">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
              </table>';
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /** Function to send complaint mail */
    public function SendComplaintMail()
    {
        try{
            $data = $_POST;
            $owner_id = $data['owner_id'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $user_detail = $_SESSION['SESSION_'.$subdomain];
            $toUsers = $user_detail['email'].','. $data['owner_email'];
            $to_arr =  explode(",",$toUsers);
            $complaint_id = $data['complaint_ids'];
            $result = '';

            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            foreach ($complaint_id as $k => $v){
                $body = $this->getComplaintsDataForEmail($v, $owner_id);
                $request['action']  = 'SendMailPhp';
                $request['to']      = $to_arr;
                $request['subject'] = 'Send Owner Complaint!';
                $body['html']       = str_replace("#logo#",$logo,$body['html']);
                $request['message'] = $body['html'];
                $request['portal']  = '1';
                $result = curlRequest($request);
            }
            return ['status' => 'success', 'code' => 200, 'data' => $result];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function ownerFileDelete()
    {
        try {
            $data=[];
            $date = date('Y-m-d H:i:s');
            $file_id = $_POST['file_id'];
            $data['deleted_at'] = $date ;

            $sqlData = createSqlColValPair($data);
            $query = "SELECT file_location FROM tenant_chargefiles WHERE id =".$file_id;
            $file_path =$this->companyConnection->query($query)->fetch();
            if($file_path)
            {
                unlink(COMPANY_DIRECTORY_URL.'/'.$file_path['file_location']);
            }

            $query1 = "DELETE FROM tenant_chargefiles WHERE id =".$file_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt1->execute();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function ownerFileAdd()
    {
        $last_id = $_REQUEST['id'];
        $this->insertFileLibrary($_FILES['file_library'],$last_id);
    }
    public function insertFileLibrary($files, $last_id, $validation_check=null)
    {

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' .$last_id . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($files)) {
            $countFiles = count($files['name']);

            if ($countFiles != 0) {

                $i = 0;
                if (isset($files)) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $files['name'][$key];
                            //   $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $last_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;

                            $query = $this->companyConnection->query("SELECT count(*) as image_counts FROM tenant_chargefiles where filename='$uniqueName' and user_id = '$last_id'");
                            $queryData = $query->fetch();
                            $ownerimage_counts = (int)$queryData['image_counts'];

                            if((int)$ownerimage_counts > (int)0)
                            {
                                $return = ['code' => 500, 'status' => 'warning', 'message' => 'File already exists.'];
                                print_r( json_encode($return) ); die;
                            }
                            if(!$validation_check)
                            {
                                $sqlData1 = createSqlColVal($data1);
                                $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                                $stmt1 = $this->companyConnection->prepare($query1);
                                $stmt1->execute($data1);
                                $tmp_name = $files["tmp_name"][$key];
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    //Directory does not exist, so lets create it.
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                            }
                        }
                        $i++;
                    }
                }
            }
        }
        $return = array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargefiles');
        print_r( json_encode($return) ); die;
    }

    public function getUsers()
{
    $user_type = $_POST['type'];

    $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

    $html = '';
    $html .= "<table class='table' border= '1px'>";
    $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

    foreach ($getUsers as $users) {

        $id = $users['id'];
        $name = $users['first_name'];
        $email = $users['email'];
        $html .= "<tr>";
        $html .= "<td><input type='checkbox' class='getEmails' data-email='".$email."'></td>";
        $html .= "<td>" . $name . "</td>";
        $html .= "<td>" . $email . "</td>";
        $html .= "</tr>";

    }

    $html .= "</table><br>";

    echo $html;
    exit;

}

    public function getCCUsers()
    {
        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getCCEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }

    public function getBCCUsers()
    {
        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getBCCEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }


    public function sendMail()
    {
        try{
            $toUsers = $_POST['to'];
            $ccUsers = $_POST['cc'];
            $bccUsers = $_POST['bcc'];

            $userEmails = explode(",",$toUsers);
            $ccEmails = explode(",",$ccUsers);
            $bccUsers = explode(",",$bccUsers);

            $subject = $_POST['subject'];
            $path = $_POST['path'];

            $data= $_POST;
            $body =$data['body'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $user_detail = $_SESSION['SESSION_'.$subdomain];
            $request['action']      = 'SendMailPhp';
            $request['to']          = $userEmails;
            if(isset($ccEmails[0]) && $ccEmails[0] !='')
            {
                $request['cc']      = $ccEmails;
            }
            if(isset($bccUsers[0]) && $bccUsers[0] !='')
            {
                $request['bcc']     = $bccUsers;
            }

            $request['subject']     = $subject;
            $request['message']     = $body;
            $request['attachment']  = $path;
            $request['portal']      = '1';

            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

    public function getPropertyListing()
    {
        try{
            $request = $_REQUEST;
            $property_id = $request['property_id'];
//            dd($property_id);
            $property_id =  implode($property_id,',');

            $data =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE id in ($property_id)")->fetchAll();

            return ['status'=>'success','code'=>200,'data'=>$data];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
}
$editOwnerAjax = new editOwnerAjax();