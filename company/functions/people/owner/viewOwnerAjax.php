<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class viewOwnerAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getIntialData($internal = null){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['property_list'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = 1")->fetchAll();

            foreach ($data['property_list'] as $key=>$value){
                if(!empty($value)){
                    $propertyPercent = $this->companyConnection->query("SELECT SUM(property_percent_owned) FROM owner_property_owned WHERE property_id=".$value['id'])->fetch();
                    $data['property_list'][$key]['property_percent_owned'] = !empty($propertyPercent['SUM(property_percent_owned)'])?$propertyPercent['SUM(property_percent_owned)']:0;
                }
            }
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity")->fetchAll();
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users` WHERE id=$id")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types` ORDER BY complaint_type ASC")->fetchAll();
            $data['account_name'] = $this->companyConnection->query("SELECT id,account_name FROM `owner_account_name`")->fetchAll();
            $data['chart_accounts'] = $this->companyConnection->query("SELECT id,account_name,account_code,is_default FROM company_chart_of_accounts WHERE status = '1'")->fetchAll();
            $data['account_type'] = $this->companyConnection->query("SELECT id,account_type_name,range_from,range_to,status,is_default FROM `company_account_type`")->fetchAll();
//            dd($data['property_list']);
            if($internal)
            {
                return $data;
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function getviewData(){
        try{

            $owner_id=$_POST['owner_id'];
            $data = [];
            $sql_query2 = "SELECT * FROM owner_details WHERE owner_details.user_id=$owner_id";

            $data['owner_detail'] =$this->companyConnection->query($sql_query2)->fetch();

            $data['owner_detail']['email']= isset($data['owner_detail']['email']) ? unserialize($data['owner_detail']['email']) : '';

            $data['owner_detail']['contact_country_code_data']=(isset($data['owner_detail']['contact_country_code']) ? unserialize($data['owner_detail']['contact_country_code']) : '');
            $data['owner_detail']['contact_phone_number_data']=(isset($data['owner_detail']['contact_phone_number']) ? unserialize($data['owner_detail']['contact_phone_number']) : '');
            $data['owner_detail']['contact_phone_type_data']=(isset($data['owner_detail']['contact_phone_type']) ? unserialize($data['owner_detail']['contact_phone_type']) : '');

            $data['owner_detail']['emergency_contact_name'] = isset($data['owner_detail']['emergency_contact_name']) ? unserialize($data['owner_detail']['emergency_contact_name']): '';
            $data['owner_detail']['emergency_other_relation'] = isset($data['owner_detail']['emergency_other_relation']) ? unserialize($data['owner_detail']['emergency_other_relation']): '';
            $data['owner_detail']['emergency_email'] = isset($data['owner_detail']['emergency_email']) ? unserialize($data['owner_detail']['emergency_email']): '';
            $data['owner_detail']['emergency_phone_number'] = isset($data['owner_detail']['emergency_phone_number']) ? unserialize($data['owner_detail']['emergency_phone_number']) : '';
            if(isset($data['owner_detail']['emergency_relation']) && !empty($data['owner_detail']['emergency_relation'])) {
                $e_relation = unserialize($data['owner_detail']['emergency_relation']);
                $owner_emergency_relation = [];
                $data['owner_detail']['emergency_relation_new'] = ($data['owner_detail']['emergency_relation']) ? unserialize($data['owner_detail']['emergency_relation']):'';
                if (!empty($e_relation)){
                    foreach ($e_relation as $k => $v){
                        $emergency_rel = emergencyRelation($v);
                        if($emergency_rel == 'Other'){
                            $emergency_rel = $data['owner_detail']['emergency_other_relation'][$k];
                        }

                        array_push($owner_emergency_relation,$emergency_rel);
                    }
                    $data['owner_detail']["emergency_relation"] = $owner_emergency_relation;
                } else {
                    $data['owner_detail']['emergency_relation'] = '';
                }



            } else {
                $data['owner_detail']['emergency_relation'] = '';
            }
            $data['owner_detail']['edit_emergency_code'] = unserialize($data['owner_detail']['emergency_country_code']);
            if(isset($data['owner_detail']['emergency_country_code']) && !empty($data['owner_detail']['emergency_country_code'])) {
                $e_country_code = unserialize($data['owner_detail']['emergency_country_code']);
                $country_code_value = [];

                if (!empty($e_country_code)){
                    foreach ($e_country_code as $k => $v){
                        if($v)
                        {
                            $query = 'SELECT code FROM countries WHERE id="'.$v.'"';
                            $country_code = $this->companyConnection->query($query)->fetch();
                            array_push($country_code_value,$country_code['code']);
                        } else {
                            array_push($country_code_value,'');
                        }

                    }

                    $data['owner_detail']["emergency_country_code"] = $country_code_value;
                } else {
                    $data['owner_detail']['emergency_country_code'] = '';
                }
            } else {
                $data['owner_detail']['emergency_country_code'] = '';
            }

            $emergency_data = [];

            if ($data['owner_detail']['emergency_contact_name']) {
                $length = count($data['owner_detail']['emergency_contact_name']);
                for ($i = 0; $i < $length; $i++) {
                    $emergency_data[$i]['emergency_contact_name'] = (isset($data['owner_detail']['emergency_contact_name'][$i])?$data['owner_detail']['emergency_contact_name'][$i]:'');
                    $emergency_data[$i]['emergency_relation'] = (isset($data['owner_detail']['emergency_relation'][$i])?$data['owner_detail']['emergency_relation'][$i]:'');
                    $emergency_data[$i]['emergency_country_code'] = (isset($data['owner_detail']['emergency_country_code'][$i])?$data['owner_detail']['emergency_country_code'][$i]:'');
                    $emergency_data[$i]['emergency_phone_number'] = (isset($data['owner_detail']['emergency_phone_number'][$i])?$data['owner_detail']['emergency_phone_number'][$i]:'');
                    $emergency_data[$i]['emergency_email'] = (isset($data['owner_detail']['emergency_email'][$i])?$data['owner_detail']['emergency_email'][$i]:'');
                }
            }


            $data['owner_detail']['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date']) ? unserialize($data['owner_detail']['owner_credential_expiration_date']) : '';
            $data['owner_detail']['owner_credential_name'] = isset($data['owner_detail']['owner_credential_name']) ? unserialize($data['owner_detail']['owner_credential_name']) : '';
            $data['owner_detail']['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date']) ? unserialize($data['owner_detail']['owner_credential_acquire_date']) : '';
            if(isset($data['owner_detail']['owner_credential_notice_period']) && !empty($data['owner_detail']['owner_credential_notice_period'])) {
                $cred_notice_period = unserialize($data['owner_detail']['owner_credential_notice_period']);
                $owner_cred_notice_period = [];

                if (is_array($cred_notice_period) && !empty($cred_notice_period)){
                    foreach ($cred_notice_period as $k => $v){
                        $cred_period = noticeperiod($v);

                        array_push($owner_cred_notice_period,$cred_period);
                    }
                    $data['owner_detail']["owner_credential_notice_period"] = $owner_cred_notice_period;
                } else {
                    $data['owner_detail']['owner_credential_notice_period'] = '';
                }
            } else {
                $data['owner_detail']['owner_credential_notice_period'] = '';
            }

            if(isset($data['owner_detail']['owner_credential_type']) && !empty($data['owner_detail']['owner_credential_type'])) {
                $owner_cred_type = unserialize($data['owner_detail']['owner_credential_type']);
                $cred_type= [];
                if (!empty($owner_cred_type)){

                    foreach ($owner_cred_type as $k => $v){

                        if(isset($v) && !empty($v))
                        {
                            $query = 'SELECT credential_type FROM tenant_credential_type WHERE id='.@$v;
                            $credential_type = $this->companyConnection->query($query)->fetch();
                            array_push($cred_type,$credential_type['credential_type'] );
                        }

                    }
                    $data['owner_detail']["owner_credential_type"] = $cred_type;
                } else {
                    $data['owner_detail']['owner_credential_type'] = '';
                }
            } else {
                $data['owner_detail']['owner_credential_type'] = '';
            }

            $credential_data = [];
            if ($data['owner_detail']['owner_credential_name']) {
                $len = count($data['owner_detail']['owner_credential_name']);
                for ($i = 0; $i < $len; $i++) {
                    $credential_data[$i]['owner_credential_name'] = (isset($data['owner_detail']['owner_credential_name'][$i]) ? $data['owner_detail']['owner_credential_name'][$i] : '');
                    $credential_data[$i]['owner_credential_type'] = isset( $data['owner_detail']['owner_credential_type'][$i])? $data['owner_detail']['owner_credential_type'][$i] : '';
                    $credential_data[$i]['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_expiration_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_acquire_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_notice_period'] = isset($data['owner_detail']['owner_credential_notice_period'][$i])?$data['owner_detail']['owner_credential_notice_period'][$i]:'';
                }
            }

            $sql_query1 = "SELECT users. *,
                           tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_veteran_status.veteran as veteran_name,
                           tenant_veteran_status.veteran as veteran_name
                        FROM users 
                        LEFT JOIN tenant_ethnicity ON users.ethnicity=tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                        WHERE users.id=$owner_id";


            $data['user_detail'] =$this->companyConnection->query($sql_query1)->fetch();

//            $data['user_detail']['name'] = userName($owner_id, $this->companyConnection);
            $address1 = isset($data['user_detail']['address1']) ? $data['user_detail']['address1'] : '';
            $address2 = isset($data['user_detail']['address2']) ? $data['user_detail']['address2'] : '';
            $address3 = isset($data['user_detail']['address3']) ? $data['user_detail']['address3'] : '';
            $address4 = isset($data['user_detail']['address4']) ? $data['user_detail']['address4'] : '';
            $data['user_detail']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $data['user_detail']['phone_number_note'] = (isset($data['user_detail']['phone_number_note']) && !empty($data['user_detail']['phone_number_note'])) ? $data['user_detail']['phone_number_note'] : 'N/A';
            $data['credential_data'] = $credential_data;


            if(isset($data['user_detail']['hobbies']) && !empty($data['user_detail']['hobbies'])) {
                $hobbies = isset($data['user_detail']['hobbies']) ? unserialize($data['user_detail']['hobbies']) : '';
                $hobby_name='';

                if(!empty($hobbies)){
                    foreach ($hobbies as $key => $value){
                        $query = 'SELECT hobby FROM hobbies WHERE id='.$value;
                        $groupData = $this->companyConnection->query($query)->fetch();
                        $name = $groupData['hobby'];
                        $hobby_name .= $name.',';
                    }
                    $data['user_detail']["hobbies"] = $hobby_name;
                }else{
                    $data['user_detail']["hobbies"] = "";
                }
            }else{
                $data['user_detail']["hobbies"] = "";
            }

            $data['banking_detail'] =$this->companyConnection->query("SELECT * FROM users WHERE id=$owner_id")->fetch();
            if(!empty($data['user_detail']) && count($data['user_detail']) > 0){
                $custom_data = !empty($data['user_detail']['custom_fields']) ? unserialize($data['user_detail']['custom_fields']) : [];
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['custom_data'] = $custom_data;
            }

            $data['user_detail']['dob'] = isset($data['user_detail']['dob']) ? dateFormatUser($data['user_detail']['dob'],null,$this->companyConnection) : '';
            $data['user_detail']['current_date'] = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
            $data['user_detail']['ssn_sin_id'] = isset($data['user_detail']['ssn_sin_id']) ? unserialize($data['user_detail']['ssn_sin_id']) : 'N/A';
            $data['intial_data'] = $this->getIntialData(true);
            //Owner Banking Information
            $data['owner_banking_information']  = [];
            $data['owner_banking_information'] =$this->companyConnection->query("SELECT * FROM owner_banking_information WHERE user_id=$owner_id")->fetchAll();

            //Owner Property Owned Information
            $data['owner_property_owned_info']  = [];
            $data['owner_property_owned_info'] =$this->companyConnection->query("SELECT * FROM owner_property_owned WHERE user_id=$owner_id")->fetchAll();

            $data['tenant_chargenote']  = [];
            $data['tenant_chargenote'] =$this->companyConnection->query("SELECT * FROM tenant_chargenote WHERE user_id=$owner_id")->fetchAll();

            $tenant_phone_query= $this->companyConnection->query("SELECT * FROM tenant_phone WHERE tenant_phone.user_id=$owner_id")->fetchAll();
            $data['owner_detail']['contact_carrier_data']=$tenant_phone_query;
//            $data['owner_phone_Info'] = $this->getPhoneInfo($owner_id);
            return ['code'=>200, 'status'=>'success', 'data'=>$data ,'emergency_data'=>$emergency_data,'credential_data'=>$credential_data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


}
$viewOwnerAjax = new viewOwnerAjax();