<?php

require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");
class calendar extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function checkloggedinuser(){
        try {
            $user_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query1 = $this->companyConnection->query("SELECT members,id FROM calendar_groups where id IN ('1','2','3','4','5','6')");
            $data = $query1->fetchAll();
            $all=[];
           foreach ($data as $key=>$value){
               $mem=explode(",",$value['members']);
               foreach ($mem as $k=>$v){
                   if($v==$user_id){
                       array_push($all, $value['id']);
                   }
               }
           }
              return array('code' => 200, 'status' => 'success', 'cal_ids'=>$all);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function checkcalpermissions(){
        try {
            $user_id=34;
            $query1 = $this->companyConnection->query("SELECT * FROM calendar_share_permission where to_user_id ='$user_id'");
            $data = $query1->fetchAll();
            return array('code' => 200, 'status' => 'success', 'cal_data'=>$data);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function savecalendardata(){
        try {
            $id=$_POST['idd'];
            $cal_user_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $cal=$_POST['calendar'];
            if($id =="0"){
            for($j=0;$j<count($cal);$j++){
            $data1['subject'] = $_POST['subject'];
            $data1['location'] = $_POST['location'];
            $data1['private'] = $_POST['private'];
            $data1['reminder'] = $_POST['reminder'];
            $data1['calendar_user_id'] = $cal_user_id;
            $data1['start_date'] = mySqlDateFormat($_POST['start_time'],null,$this->companyConnection);
            $data1['end_date'] = mySqlDateFormat($_POST['end_time'],null,$this->companyConnection);
            $data1['start_time'] = $_POST['st_time'];
            $data1['end_time'] = $_POST['e_time'];
            $data1['calendars'] = $cal[$j];
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO calendar_schedule (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
                }
           }else {
                for ($j = 0; $j < count($cal); $j++) {
                    $data1['subject'] = $_POST['subject'];
                    $data1['location'] = $_POST['location'];
                    $data1['private'] = $_POST['private'];
                    $data1['reminder'] = $_POST['reminder'];
                    $data1['calendar_user_id'] = $cal_user_id;
                    $data1['start_date'] = mySqlDateFormat($_POST['start_time'], null, $this->companyConnection);
                    $data1['end_date'] = mySqlDateFormat($_POST['end_time'], null, $this->companyConnection);
                    $data1['start_time'] = $_POST['st_time'];
                    $data1['end_time'] = $_POST['e_time'];
                    $data1['calendars'] = $cal[$j];
                    $data1['created_at'] = date('Y-m-d H:i:s');
                    $data1['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE calendar_schedule SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }
            }
            $cal=implode("','",$cal);
            $cal= "'".$cal."'";
            $id = $this->companyConnection->lastInsertId();
            $query = $this->companyConnection->query("SELECT * FROM calendar_schedule where id=".$id);
            $data = $query->fetch();
            $query2 = $this->companyConnection->query("SELECT * FROM calendar_schedule where deleted_at IS null and calendars IN ($cal) and calendar_user_id='$cal_user_id'");
            $data2 = $query2->fetchAll();
            $query1 = $this->companyConnection->query("SELECT count(id) as total FROM calendar_schedule where deleted_at IS null and calendars='1'");
            $count = $query1->fetch();
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Added successfully!','data'=>$data,'count'=>$count,'data2'=>$data2);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

     public function deletecalendardata(){
            try {

                $id=$_POST['id'];
                $data['deleted_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE calendar_schedule SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Records Deleted successfully!');
            } catch (PDOException $e) {
                echo $e->getMessage();
                printErrorLog($e->getMessage());
            }
        }

    public function getdataafterdel(){
            try {
                $cal=$_POST['calendar'];
                $cal=implode("','",$cal);
                $cal= "'".$cal."'";
                $cal_user_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
                $query2 = $this->companyConnection->query("SELECT * FROM calendar_schedule where deleted_at IS null and calendars IN ($cal) and calendar_user_id='$cal_user_id'");
                $data2 = $query2->fetchAll();

                $query1 = $this->companyConnection->query("SELECT count(id) as total FROM calendar_schedule where deleted_at IS null");
                $count = $query1->fetch();

                return array('code' => 200, 'status' => 'success', 'message' => 'Records Deleted successfully!','count'=>$count,'data2'=>$data2);
            } catch (PDOException $e) {
                echo $e->getMessage();
                printErrorLog($e->getMessage());
            }
        }


    public function updateafterdrag(){
        try {

            $id=$_POST['id'];
            $data1['start_date'] = mySqlDateFormat($_POST['start_time'],null,$this->companyConnection);
            $data1['end_date'] =   mySqlDateFormat($_POST['end_time'],null,$this->companyConnection);
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE calendar_schedule SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function compareweekdata(){
        try {
            $cal_user_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $date1=$_POST['date'];
            $date2=$_POST['next_date'];
            $query = $this->companyConnection->query("SELECT * from calendar_schedule where start_date between '$date1' AND '$date2' AND deleted_at is null and calendar_user_id='$cal_user_id' order by start_date");
       //   dd("SELECT * from calendar_schedule where start_date between '$date1' AND '$date2' AND deleted_at is null");
            $data = $query->fetchAll();
            $arr=[];
            foreach ($data as $key=>$value){
             array_push($arr,$value['start_date']);
            }
            return array('code' => 200, 'status' => 'success', 'data'=>$data,'arr'=>$arr);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function savecalendardatarecurring(){
        try {
            $cal_user_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $total_dates=$_POST['dates'];
            $cal=implode(',',$_POST['calendar']);
            for($i= 0;$i<count($total_dates);$i++){
                $data1['subject'] = $_POST['subject'];
                $data1['location'] = $_POST['location'];
                $data1['private'] = $_POST['private'];
                $data1['start_date'] = $total_dates[$i];
                $data1['calendar_user_id'] = $cal_user_id;
                $data1['end_date'] = $total_dates[$i];
                $data1['start_time'] = $_POST['st_time'];
                $data1['end_time'] = $_POST['e_time'];
                $data1['calendars'] = $cal;
                $data1['created_at'] =  date('Y-m-d H:i:s');
                $data1['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO calendar_schedule (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
            }


            return array('code' => 200, 'status' => 'success', 'message' => 'Records Added successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function getPdfContent(){
        try{
            $html=$_POST['htmls'];
            $html=$html;
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function createHTMLToPDF($report){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($report);
        $dompdf->render();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'Receipt.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        // $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'Receipt.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl,  'message' => 'Record retrieved successfully');
    }

    public function sendInvite(){


        $from_name = "from Name";
        $from_address = "tony@yopmail.com";
        $to_name = "to Name";
        $to_address = "dhallshrey@yopmail.com";
        $startTime = "11/09/2014 18:00:00";
        $endTime = "11/09/2014 19:00:00";
        $subject = "I HOPE THIS WORKS";
        $description = "I REALLY HOPE THIS WORKS";
        $location = "Joe's House";

        //mail($to_address, "Subject: $subject",$location );
    $this->sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject,
         $description, $location);
            return array('code'=>200);
    }
    function sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location)
    {
        //Create Email Headers
        $headers = "From: ".$from_name." <".$from_address.">\n";
        $domain=getDomain();
        $href="http://".$domain.".apex.local/Calendar/RequestAccepted";
         //Create Email Body (HTML)
        $message = '<html><body>';
        $message .= '<h1 style="color:#f40;">Hi Jane!</h1>';
        $message .= '<p style="color:#080;font-size:18px;">Will you marry me?</p>';
        $message .= '<p style="color:#080;font-size:18px;"><a href="'.$href.'">Accept</a></p>';
        $message .= '<p style="color:#080;font-size:18px;"><a href="'.$href.'">Decline</a></p>';
        $message .= '</body></html>';
        $request['to'][]    = $to_address;
        $request['subject'] =$subject;
        $request['message'] = $message;
        $request['portal'] = '1';
        $request['action'] = 'SendMailPhp';
      //  $request['headers'] = $headers;
        $curl_response = curlRequest($request);
       // dd($from_address);
      //  $mailsent = mail($to_address, $subject, $message, $headers);
        return array('code'=>200,'curl_response'=>$curl_response);
    }
    public function RequestAccepted(){
        $from_name = "from Name";
        $from_address = "shrey@yopmail.com";
        $to_name = "to Name";
        $to_address = "tony@yopmail.com";
        $startTime = "11/09/2014 18:00:00";
        $endTime = "11/09/2014 19:00:00";
        $subject = "I HOPE THIS WORKS";
        $description = "I REALLY HOPE THIS WORKS";
        $location = "Joe's House";
        //mail($to_address, "Subject: $subject",$location );
    $this->sendIcalEventAccpeted($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject,
         $description, $location);
            return array('code'=>200);
    }

    function sendIcalEventAccpeted($from_name, $from_address, $to_name, $to_address, $startTime, $endTime,
                           $subject, $description, $location)
    {
        //Create Email Headers
        $headers = "From: ".$from_name." <".$from_address.">\n";
        $domain=getDomain();
        $href="http://".$domain.".apex.local/Calendar/RequestAccepted";
         //Create Email Body (HTML)
        $message = '<html><body>';
        $message .= '<h1 style="color:#f40;">Hi Jane!</h1>';
        $message .= '<p style="color:#080;font-size:18px;">Request Accepted</p>';
        /*  $message .= '<p style="color:#080;font-size:18px;"><a href="'.$href.'">Accept</a></p>';
        $message .= '<p style="color:#080;font-size:18px;"><a href="'.$href.'">Decline</a></p>';*/
        $message .= '</body></html>';
        $request['to'][]    = $to_address;
        $request['subject'] =$subject;
        $request['message'] = $message;
        $request['portal'] = '1';
        $request['action'] = 'SendMailPhp';
      //  $request['headers'] = $headers;
        //dd($message);
        $curl_response = curlRequest($request);

       // dd($from_address);
      //  $mailsent = mail($to_address, $subject, $message, $headers);
        return array('code'=>200,'curl_response'=>$curl_response);
    }
    public function GetuserEmail(){
      $val=$_POST['val'];
      $query = $this->companyConnection->query("SELECT name,id,email,phone_number from users where user_type='1' AND (role='1' OR role='2') AND email LIKE '%".$val."%'");
      $data = $query->fetchAll();
      $html="";
      for($i=0;$i<count($data);$i++){
          $html.="<div class='dataEmail' data-id='".$data[$i]['id']."'>".$data[$i]['email']."</div>";
      }
      print_r($html);die;
    }


    public function update_status(){
        try {

            $id=$_POST['id'];
            $data1['status_shown'] ='1';
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE calendar_schedule SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function sendInvitation(){
        try{
            $send_to=$_POST['send_to'];
            $mesage=$_POST['message'];
            $id=$_POST['id'];
            $id=explode(' ',$id);
            for($i=0;$i<count($id);$i++){
                $data['user_id'] = $id[$i];
                $data['email'] = $send_to;
                $data['message'] = $mesage;
                $data['start_date'] = $_POST['start_date'];
                $data['start_time'] = $_POST['start_time'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['status'] = 'pending';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO send_invitation_calendar (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

            }
            print_r("Invitation sent successfully");die;

        }catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function cal_share_permissions(){
        try {
            $ids=$_POST['ids'];
            $from_id=$_SESSION[SESSION_DOMAIN]['cuser_id'];
            $cal=implode(',',$_POST['calendar']);
            for($i= 0;$i<count($ids);$i++){
                $data1['from_user_id'] = $from_id;
                $data1['to_user_id'] = $ids[$i];
                $data1['free_busy_time'] = $_POST['free_busy'];
                $data1['full_access'] = $_POST['move'];
                $data1['schedule_create_meetings'] = $_POST['create'];
                $data1['move_change_meeting'] = $_POST['full'];
                $data1['calendars'] = $cal;
                $data1['created_at'] =  date('Y-m-d H:i:s');
                $data1['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO calendar_share_permission (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
}

$TenantShortTermAjax = new calendar();

