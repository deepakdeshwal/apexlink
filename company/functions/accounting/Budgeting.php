<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class Budgeting extends DBConnection{

    /**
     * Budgeting constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Get Charts of Accounts table
     * @return array
     */
    public function getBudgetChartsOfAccounts(){
        try{
            $query = 'SELECT * FROM company_chart_of_accounts WHERE account_type_id=2 OR account_type_id=5';
            $accountData = $this->companyConnection->query($query)->fetchAll();
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'data' => $accountData, 'message' =>SUCCESS_FETCHED);
        } catch(Exception $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' =>ERROR_SERVER_MSG, 'data' =>$exception);
        }
    }

    /**
     * function to save budget Data
     * @return array
     */
    public function saveBudget(){
        try {
            $data = postArray($_POST['data']);
            $data['income_amount'] = convertToFloat($_POST['income']);
            $data['expense_amount'] = convertToFloat($_POST['expense']);
            $data['net_income'] = convertToFloat($_POST['grandTotal']);
            $budgetItemData = $_POST['budgetItemData'];
            if(!empty($budgetItemData)){
                foreach ($budgetItemData as $key => $value){
                    $budgetItemData[$key] = $this->convertMultipeArrayToSingle($value);
                }
            }
           // dd($budgetItemData);
            $id = isset($data['id'])?$data['id']:NULL;
            if(!empty($id)){

                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlUpdateCase($data);
                $query = "UPDATE company_budget SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $exe = $stmt->execute($sqlData['data']);
                //Update data in elastic search
                $budgetData = $data;
                $budgetData['id'] = $id;
                $ElasticSearchSave = insertDocument('BUDGET','UPDATE',$budgetData,$this->companyConnection);
                $message = SUCCESS_UPDATED;
            } else {
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_budget (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                //Save data in elastic search
                $budgetData = $data;
                $budgetData['id'] = $this->companyConnection->lastInsertId();
                $ElasticSearchSave = insertDocument('BUDGET','ADD',$budgetData,$this->companyConnection);
                $message = SUCCESS_ADDED;
            }
            $last_id = !empty($id)?$id:$this->companyConnection->lastInsertId();

            $this->saveBudgetItems($budgetItemData, $last_id);
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => $message);
        } catch (PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    /**
     * function to convert multidimensional array to single
     * @param $data
     * @return array
     */
    function convertMultipeArrayToSingle($data){
        $newArray = [];
        foreach ($data as $key => $value){
            foreach ($value as $Key2=>$value2){
                $newArray[$Key2] = $value2;
            }
        }
        return $newArray;
    }

    /**
     * function to save budget item items
     * @param $data
     * @param $budget_id
     * @return array
     */
    public function saveBudgetItems($data,$budget_id){
        try{
            $deleteQuery = "DELETE FROM company_budget_details WHERE budget_id=" . $budget_id;
            $stmt = $this->companyConnection->prepare($deleteQuery);
            $stmt->execute();
            foreach ($data as $key=>$value){
                $dataDetails = [];
                $dataDetails['january'] = convertToFloat($value['january']);
                $dataDetails['february'] = convertToFloat($value['february']);
                $dataDetails['march'] = convertToFloat($value['march']);
                $dataDetails['april'] = convertToFloat($value['april']);
                $dataDetails['may'] = convertToFloat($value['may']);
                $dataDetails['june'] = convertToFloat($value['june']);
                $dataDetails['july'] = convertToFloat($value['july']);
                $dataDetails['august'] = convertToFloat($value['august']);
                $dataDetails['september'] = convertToFloat($value['september']);
                $dataDetails['october'] = convertToFloat($value['october']);
                $dataDetails['november'] = convertToFloat($value['november']);
                $dataDetails['december'] = convertToFloat($value['december']);
                $dataDetails['total_fy'] = convertToFloat($value['total_fy']);
                $dataDetails['charts_of_account_id'] = $value['charts_of_account_id'];
                $dataDetails['type'] = $value['type'];
                $dataDetails['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $dataDetails['budget_id'] = $budget_id;
                $dataDetails['created_at'] = date('Y-m-d H:i:s');
                $dataDetails['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($dataDetails);
                $query = "INSERT INTO company_budget_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($dataDetails);

            }
        } catch (PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function getBudget(){
        try {
            $portfolio = $_POST['portfolio_id'];
            $property = $_POST['property_id'];
            $query = 'SELECT id,budget_name FROM company_budget WHERE portfolio_id ='.$portfolio.' AND property_id ='.$property;
            $queryData = $this->companyConnection->query($query)->fetchAll();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' => $queryData);
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function Budget(){
        try {
            $portfolio = $_POST['portfolio_id'];
            $property = $_POST['property_id'];
            $query = 'SELECT id,budget_name FROM company_budget WHERE portfolio_id ='.$portfolio.' AND property_id ='.$property;
            $queryData = $this->companyConnection->query($query)->fetchAll();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' => $queryData);
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function getBankDetails(){
        try {
            $query = 'SELECT id,bank_name FROM company_accounting_bank_account';
            $queryData = $this->companyConnection->query($query)->fetchAll();
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED, 'data' => $queryData);
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function copyBudget(){
        try{
            $postData = postArray($_POST['data']);
            $budgetData = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $postData['budget']], 'company_budget');
            $data = [];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['budget_name'] = $postData['budget_name'];
            $data['starting_month'] = $postData['starting_month'];
            $data['starting_year'] = $postData['starting_year'];
            $data['portfolio_id'] = $postData['portfolio_id'];
            $data['property_id'] = $postData['property_id'];
            $data['income_amount'] = $budgetData['data']['income_amount'];
            $data['expense_amount'] = $budgetData['data']['expense_amount'];
            $data['net_income'] = $budgetData['data']['net_income'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO company_budget (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $last_id = !empty($id)?$id:$this->companyConnection->lastInsertId();
            $this->copyBudgetDetails($postData['budget'],$last_id);
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => 'Budget copied successfully.');
        } catch(PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function copyBudgetDetails($copy_budget_id,$budget_id){
        try{
            $query = 'SELECT * FROM company_budget_details WHERE budget_id ='.$copy_budget_id;
            $budgetData = $this->companyConnection->query($query)->fetchAll();
            if(!empty($budgetData)){
                foreach ($budgetData as $key=>$value){
                    $data = $value;
                    unset($data['id']);
                    $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $data['budget_id'] = $budget_id;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_budget_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_ADDED);
        } catch (PDOException $exception) {
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function getBudgetInfo(){
        try{
            $budgetData = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $_POST['budget_id']], 'company_budget');
            if($budgetData['code'] == 200){
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED ,'data' =>$budgetData['data']);
            }
            return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND );
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function getBudgetInfoDetails(){
        try{
            $query = 'SELECT company_budget.starting_month,company_budget.starting_year,company_budget.income_amount,company_budget.expense_amount,company_budget.net_income,company_budget_details.*,company_chart_of_accounts.account_type_id,company_chart_of_accounts.account_code,company_chart_of_accounts.account_name FROM company_budget_details LEFT JOIN company_chart_of_accounts ON company_budget_details.charts_of_account_id=company_chart_of_accounts.id LEFT JOIN company_budget ON company_budget_details.budget_id=company_budget.id WHERE company_budget_details.budget_id ='.$_POST['budget_id'].' ORDER BY company_budget_details.charts_of_account_id';
            $budgetData = $this->companyConnection->query($query)->fetchAll();
            $netIncomeData = [];
            if(!empty($budgetData)){
                foreach ($budgetData as $key=>$value) {
                    $trData = $this->rearrangeMonth($value['starting_month'], $value);
                    $budgetData[$key]['trData'] = $trData;
                }
                $netIncomeData = $this->netIncomeData($budgetData);
            }
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUCCESS_FETCHED ,'data' =>$budgetData ,'data2' => $netIncomeData);
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR, 'message' => $exception);
        }
    }

    public function rearrangeMonth($month,$data){
        $month = $month-1;
        $monthArray = ['1','2','3','4','5','6','7','8','9','10','11','12'];
        $array1 = [];
        $array2 = [];
        foreach ($monthArray as $key =>$value){
            if($month <= $key ){
                array_push($array1,$value);
            }
            if($month > $key){
                array_push($array2,$value);
            }
        }
        $newArray = array_merge($array1, $array2);
        $amount = [];
        foreach ($newArray as $key=>$value){
            switch ($value) {
                case "1":
                    array_push($amount,$data['january']);
                    break;
                case "2":
                    array_push($amount,$data['february']);
                    break;
                case "3":
                    array_push($amount,$data['march']);
                    break;
                case "4":
                    array_push($amount,$data['april']);
                    break;
                case "5":
                    array_push($amount,$data['may']);
                    break;
                case "6":
                    array_push($amount,$data['june']);
                    break;
                case "7":
                    array_push($amount,$data['july']);
                    break;
                case "8":
                    array_push($amount,$data['august']);
                    break;
                case "9":
                    array_push($amount,$data['september']);
                    break;
                case "10":
                    array_push($amount,$data['october']);
                    break;
                case "11":
                    array_push($amount,$data['november']);
                    break;
                case "12":
                    array_push($amount,$data['december']);
                    break;
                default:
                    break;
            }
        }
        array_push($amount,$data['total_fy']);
        return $amount;
    }

    public function netIncomeData($data){
        $amountIncome1 = 0;
        $amountIncome2 = 0;
        $amountIncome3 = 0;
        $amountIncome4 = 0;
        $amountIncome5 = 0;
        $amountIncome6 = 0;
        $amountIncome7 = 0;
        $amountIncome8 = 0;
        $amountIncome9 = 0;
        $amountIncome10 = 0;
        $amountIncome11 = 0;
        $amountIncome12 = 0;
        $amountExpense1 = 0;
        $amountExpense2 = 0;
        $amountExpense3 = 0;
        $amountExpense4 = 0;
        $amountExpense5 = 0;
        $amountExpense6 = 0;
        $amountExpense7 = 0;
        $amountExpense8 = 0;
        $amountExpense9 = 0;
        $amountExpense10 = 0;
        $amountExpense11 = 0;
        $amountExpense12 = 0;
        foreach ($data as $key=>$value){
            foreach ($value['trData'] as $key1=>$value1){
                if($value['type'] == 'income'){
                    switch ($key1) {
                        case "0":
                            $amountIncome1 += $value1;
                            break;
                        case "1":
                            $amountIncome2 += $value1;
                            break;
                        case "2":
                            $amountIncome3 += $value1;
                            break;
                        case "3":
                            $amountIncome4 += $value1;
                            break;
                        case "4":
                            $amountIncome5 += $value1;
                            break;
                        case "5":
                            $amountIncome6 += $value1;
                            break;
                        case "6":
                            $amountIncome7 += $value1;
                            break;
                        case "7":
                            $amountIncome8 += $value1;
                            break;
                        case "8":
                            $amountIncome9 += $value1;
                            break;
                        case "9":
                            $amountIncome10 += $value1;
                            break;
                        case "10":
                            $amountIncome11 += $value1;
                            break;
                        case "11":
                            $amountIncome12 += $value1;
                            break;
                        default:
                            break;
                    }
                } elseif($value['type'] == 'expense') {
                    switch ($key1) {
                        case "0":
                            $amountExpense1 += $value1;
                            break;
                        case "1":
                            $amountExpense2 += $value1;
                            break;
                        case "2":
                            $amountExpense3 += $value1;
                            break;
                        case "3":
                            $amountExpense4 += $value1;
                            break;
                        case "4":
                            $amountExpense5 += $value1;
                            break;
                        case "5":
                            $amountExpense6 += $value1;
                            break;
                        case "6":
                            $amountExpense7 += $value1;
                            break;
                        case "7":
                            $amountExpense8 += $value1;
                            break;
                        case "8":
                            $amountExpense9 += $value1;
                            break;
                        case "9":
                            $amountExpense10 += $value1;
                            break;
                        case "10":
                            $amountExpense11 += $value1;
                            break;
                        case "11":
                            $amountExpense12 += $value1;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        $netTrData =[];
        $incomeRow = [$amountIncome1,$amountIncome2,$amountIncome3,$amountIncome4,$amountIncome5,$amountIncome6,$amountIncome7,$amountIncome8,$amountIncome9,$amountIncome10,$amountIncome11,$amountIncome12,$amountIncome1+$amountIncome2+$amountIncome3+$amountIncome4+$amountIncome5+$amountIncome6+$amountIncome7+$amountIncome8+$amountIncome9+$amountIncome10+$amountIncome11+$amountIncome12];
        $expenseRow = [$amountExpense1,$amountExpense2,$amountExpense3,$amountExpense4,$amountExpense5,$amountExpense6,$amountExpense7,$amountExpense8,$amountExpense9,$amountExpense10,$amountExpense11,$amountExpense12,$amountExpense1+$amountExpense2+$amountExpense3+$amountExpense4+$amountExpense5+$amountExpense6+$amountExpense7+$amountExpense8+$amountExpense9+$amountExpense10+$amountExpense11+$amountExpense12];
        array_push($netTrData,$incomeRow);
        array_push($netTrData,$expenseRow);
        return $netTrData;
    }


}
$Budgeting = new Budgeting();