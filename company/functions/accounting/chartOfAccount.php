<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class ChartOfAccountAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
//        print_r($_REQUEST); die();
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company unit type table
     * @return array
     */
    public function insert()
    {

        try {
            $data = $_POST['form'];
//             print_r($data);
            $required_array = ['account_type_id', 'account_code','account_name','reporting_code'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = ['range_from' => 1, 'range_to' => 1];
            //Number variable array
            $number_array = ['range_from', 'range_to'];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['chart_account_edit_id']) || empty($data['chart_account_edit_id'])){
                    unset($data['chart_account_edit_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];


                $accountCodeDuplicate =  checkNameAlreadyExists($this->companyConnection,'company_chart_of_accounts', 'account_code', $data['account_code'], '');
                if($accountCodeDuplicate['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account code already exists.');
                }
                $accountNameDuplicate =  checkNameAlreadyExists($this->companyConnection,'company_chart_of_accounts', 'account_name', $data['account_name'], '');
                if($accountNameDuplicate['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account name already exists.');
                }

                $query = $this->companyConnection->query("SELECT * FROM company_chart_of_accounts");
                $checkEmptyRecord = $query->fetchAll();
                if(empty($checkEmptyRecord) && $data['is_default'] == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'One default value is required.');
                }

                if($data['account_code']<$data['range_from'] || $data['account_code']>$data['range_to']){
                    return array('code' => 503,'status' => 'error', 'message' => 'Account code does not fall in given range '.$data['range_from'].' and '.$data['range_to'].'.');
                }
                if(isset($data['range_from']))  unset($data['range_from']);
                if(isset($data['range_to']))  unset($data['range_to']);

                if($data['status'] == 0 && $data['is_default'] == 1){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                if($data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_chart_of_accounts SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                //Save Data in Company Database
//                dd($data);
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_chart_of_accounts (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of unit type
     * @return array
     */
    public function update()
    {
        try {
//            die("hello");
            $data = $_POST['form'];

            $chart_account_edit_id = $data['chart_account_edit_id'];
            //Required variable array
            $required_array = ['account_type_id', 'account_code','account_name','reporting_code'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                 if(isset($data['chart_account_edit_id']) || !empty($data['chart_account_edit_id'])){
                    unset($data['chart_account_edit_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                $accountCodeDuplicate =  checkNameAlreadyExists($this->companyConnection,'company_chart_of_accounts', 'account_code', $data['account_code'], $chart_account_edit_id);
                if($accountCodeDuplicate['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account code already exists.');
                }
                $accountNameDuplicate =  checkNameAlreadyExists($this->companyConnection,'company_chart_of_accounts', 'account_name', $data['account_name'], $chart_account_edit_id);
                if($accountNameDuplicate['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account name already exists.');
                }

                $chartAccountData = getDataById($this->companyConnection, 'company_chart_of_accounts', $chart_account_edit_id);

                $checkStatus = $chartAccountData['data']['status'];
                if($checkStatus == 0 && $data['status'] == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                $checkIsDefault = $chartAccountData['data']['is_default'];
                if($checkIsDefault == 1 && $data['is_default'] == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'One default value is required.');
                }
                if(isset($data['range_from']))  unset($data['range_from']);
                if(isset($data['range_to']))  unset($data['range_to']);

                if($data['status'] == 0 && $data['is_default'] == 1){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                if(isset($data['is_default']) && $data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_chart_of_accounts SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_chart_of_accounts SET ".$sqlData['columnsValuesPair']." where id='$chart_account_edit_id'";
                $stmt =$this->companyConnection->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching unit type data
     * @return array
     */
    public function view() {
        try {
            $id = $_POST['id'];
            $data = getDataById($this->companyConnection, 'company_chart_of_accounts', $id);
            if(!empty($data['data']) && count($data['data']) > 0){
//                $custom_data = !empty($data['data']['custom_fields']) ? unserialize($data['data']['custom_fields']) : null;
//                if(!empty($custom_data)){
//                    foreach ($custom_data as $key=>$value){
//                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
//                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
//                        continue;
//                    }
//                }
//                $data['custom_data'] = $custom_data;
                return $data;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * function for fetching unit type data
     * @return array
     */
    public function viewAccountType(){

        try {
            $id = $_POST['id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $dataselected = $this->companyConnection->query("SELECT account_type_name,id FROM company_account_type")->fetchAll();
//            print_r($dataselected);
            foreach($dataselected as $key => $value) {
                $keyval[] = array($value['id'] => $value['account_type_name'] );
            }
            //print_r($keyval);
            if(isset($login_user_id) && !empty($login_user_id))
            {
                return getDataById($this->companyConnection, 'company_chart_of_accounts', $id,$keyval);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update chartAccount Type Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');

            $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_chart_of_accounts WHERE id=?");
            $checkSetDefault->execute([$id]);
            $checkSetDefault = $checkSetDefault->fetch();

            if(isset($checkSetDefault) || !empty($checkSetDefault)){
                $isDefault = $checkSetDefault['is_default'];

                if($isDefault == 1 && $status == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.');
                }
            }
            $sql = "UPDATE company_chart_of_accounts SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $updated_at, $id]);

            if($status == '1'){
                $_SESSION["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete Unit Type
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_chart_of_accounts SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Export Sample Excel For Unit Type
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/ChartAccount.xlsx";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
    }

    /**
     * Export Excel For Unit Type
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function exportExcel(){

        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");

        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];

        $query1 = "SELECT * FROM $table";
        $query1 = "SELECT $table.*,company_account_type.account_type_name,company_account_sub_type.account_sub_type FROM $table LEFT JOIN company_account_type ON $table.account_type_id=company_account_type.id LEFT JOIN company_account_sub_type ON $table.sub_account=company_account_sub_type.id";


        $stmt = $this->companyConnection->prepare($query1);
        $stmt->execute();
        //Set header with temp array
        $columnHeadingArr =array("AccountTypeName", "AccountCode","AccountName","ReportingCode","SubAccountType");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);
        while ($account_type_id_data = $stmt->fetch(PDO::FETCH_ASSOC))
        {
//            dd($account_type_id_data);
            $tmpArray =array();
            $account_type_id = $account_type_id_data['account_type_name'];
            array_push($tmpArray,$account_type_id);

            $account_code= $account_type_id_data['account_code'];
            array_push($tmpArray,$account_code);

            $account_name= $account_type_id_data['account_name'];
            array_push($tmpArray,$account_name);

            $reporting_code= $account_type_id_data['reporting_code'];
            $reporting_code = ($reporting_code == 1) ? 'C':(($reporting_code == 2) ? 'L': (($reporting_code == 3)? 'M':'B'));
//            dd($reporting_code);
            array_push($tmpArray,$reporting_code);

            $sub_account= $account_type_id_data['account_sub_type'];
            array_push($tmpArray,$sub_account);

            array_push($sheet,$tmpArray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
//        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ChartAccount.xlsx"');
        $objWriter->save('php://output');
    }

    /**
     * Import Unit type Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();

                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();

                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {

                                $skip_record = true;
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                $account_type_data =$this->companyConnection->query("SELECT account_type_name FROM company_account_type")->fetchAll();

                                foreach ($account_type_data as $key=>$val)
                                {
                                    $account_type_arr[] =$val['account_type_name'];
                                }
                                if ($first_row_header == 'AccountTypeName') {

                                    foreach ($single_row[0] as $key => $value) {
                                        if( $key == 0){
                                            $accountTypeData = getSingleRecord($this->companyConnection, ['column' => 'account_type_name', 'value' => $value], 'company_account_type');
                                            if($accountTypeData['code'] == 200){
                                                $single_row[0][$key] = $accountTypeData['data']['id'];
                                            } else {
                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                            }
                                            continue;
                                        }

                                        if($key == 2){
                                            $accountTypeData = getSingleRecord($this->companyConnection, ['column' => 'account_name', 'value' => $value], 'company_chart_of_accounts');
                                            if($accountTypeData['code'] == 400){
                                                $single_row[0][$key] = $value;
                                            } else {

                                                $skip_record = false;
                                                continue;
                                            }
                                        }
                                        if($key == 3){
                                            $single_row[0][$key] = reportingCode($value);
                                            continue;
                                        }
                                        if($key == 4){
                                           $accountTypeData = getSingleRecord($this->companyConnection, ['column' => 'account_sub_type', 'value' => $value], 'company_account_sub_type');
                                           if($accountTypeData['code'] == 200){
                                               $single_row[0][$key] = $accountTypeData['data']['id'];
                                           } else {
                                               $single_row[0][$key] = null;
                                           }
                                           continue;
                                        }
                                        $single_row[0][$key] = $value;

                                    }

                                    if($skip_record) {
                                        $insert = [];
                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                        $insert['account_type_id'] = $single_row[0][0];
                                        $insert['account_code'] = $single_row[0][1];
                                        $insert['account_name'] = $single_row[0][2];
                                        $insert['reporting_code'] = $single_row[0][3];
                                        $insert['sub_account'] = $single_row[0][4];
                                        $insert['posting_status'] = '1';
                                        $insert['status'] = '1';
                                        $insert['is_default'] = '0';
                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                        $sqlData = createSqlColVal($insert);
                                        $query = "INSERT INTO company_chart_of_accounts (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                        $stmt = $this->companyConnection->prepare($query);
                                        $stmt->execute($insert);
                                    }
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            return ['status'=>'success','code'=>200,'message'=>'File uploaded successfully!'];
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }


    public  function getAllAccountType(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_account_type WHERE status = '1'");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public  function getAllAccountSubType(){
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM company_account_sub_type WHERE account_type_id='$id'");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreChartOfAccount()
    {

        try {
            $data = array();
            $data['deleted_at'] = NULL ;
            //$data['id'] = $_POST['cuser_id'];

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_chart_of_accounts SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];

            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$ChartOfAccountAjax = new ChartOfAccountAjax();