<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class ownerContribution extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getOwnerDataForComboGrid() {
        try {
            $search = @$_REQUEST['q'];
            $sql = "SELECT * FROM users  where user_type='4' AND status = '1' and first_name LIKE '%$search%' order by name asc";
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['company_name'] = $user_data['company_name'];
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                    $data['rows'][$key]['email'] = $user_data['email'];
                    $data['rows'][$key]['stripe_customer_id'] = $user_data['stripe_customer_id'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function getInitialData(){
        try{
            $data = [];
            $data['chart_accounts'] = $this->companyConnection->query("SELECT id,account_name,account_code,is_default FROM company_chart_of_accounts WHERE status = '1' AND deleted_at IS NULL ORDER BY account_code ASC")->fetchAll();

            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        switch ($data_type) {
            case "owner":
                $query1 = "SELECT property_id FROM owner_property_owned WHERE user_id = ".$id;
                $propertyData = $this->companyConnection->query($query1)->fetchAll();
                $data = [];
                foreach ($propertyData as $key=>$value){
                    if(!empty($value['property_id'])){
                        $data['portfolio_data'][$key] = $this->companyConnection->query("SELECT DISTINCT cpp.id, cpp.portfolio_name FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id = cpp.id WHERE gp.id = ".$value['property_id'])->fetch();
                    }
                }

                $outputArray = array();
                $outputArray2 = array();

                foreach($data['portfolio_data'] as $inputArrayItem) {
                    foreach($outputArray as $outputArrayItem) {
                        if($inputArrayItem['id'] == $outputArrayItem) {
                            continue 2;
                        }
                    }
                    $outputArray[] = $inputArrayItem['id'];
                    $outputArray2[] = $inputArrayItem;
                }
                $data['portfolio_data'] = $outputArray2;
                if (count($data['portfolio_data']) == 1){
                    $portfolio_id = $data['portfolio_data'][0]['id'];
                    $owner_id = $id;
                    $query1 = "SELECT property_id FROM owner_property_owned WHERE user_id = ".$owner_id;
                    $propertyData = $this->companyConnection->query($query1)->fetchAll();

                    foreach ($propertyData as $key=>$value){
                        if(!empty($value['property_id'])){
                            $property_data = $this->companyConnection->query("SELECT gp.id, gp.property_name FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id = cpp.id WHERE cpp.id = $portfolio_id AND gp.id = ".$value['property_id'])->fetch();
                            $data['property_data'][$key] = $property_data;
                        }
                    }
//                    dd(count($data['property_data']));
                    if (count($data['property_data']) == 1){
                        $query1 = "SELECT caba.id, caba.bank_name, caba.bank_account_number FROM property_bank_details pbd JOIN company_accounting_bank_account caba ON pbd.account_name = caba.id WHERE pbd.property_id = ".$data['property_data'][0]['id'];
                        $data['bank_account_data'] = $this->companyConnection->query($query1)->fetchAll();
                    }
                }
                break;
            case "portfolio":
                $owner_id = $prev_id;
                $query1 = "SELECT property_id FROM owner_property_owned WHERE user_id = ".$owner_id;
                $propertyData = $this->companyConnection->query($query1)->fetchAll();
//                dd($propertyData);
                $data['property_data'] = [];
                foreach ($propertyData as $key=>$value){
                    if(!empty($value['property_id'])){
                        $property_data = $this->companyConnection->query("SELECT gp.id, gp.property_name FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id = cpp.id WHERE cpp.id = $id AND gp.id = ".$value['property_id'])->fetch();
                        if (empty($property_data)){
//                            dd($property_data);
                            continue;
                        } else {
                            array_push($data['property_data'], $property_data);
                        }
                    }
                }
//                dd($data['property_data']);
                if (count($data['property_data']) == 1){
                    $query1 = "SELECT caba.id, caba.bank_name, caba.bank_account_number FROM property_bank_details pbd JOIN company_accounting_bank_account caba ON pbd.account_name = caba.id WHERE pbd.property_id = ".$data['property_data'][0]['id'];
                    $data['bank_account_data'] = $this->companyConnection->query($query1)->fetchAll();
                }
//                     dd($data);
                break;
            case "property":
                $query1 = "SELECT caba.id, caba.bank_name, caba.bank_account_number FROM property_bank_details pbd JOIN company_accounting_bank_account caba ON pbd.account_name = caba.id WHERE pbd.property_id = $id";
                $data['bank_account_data'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            case "all_owner_listing":
                $query1 = "SELECT u.id, u.name FROM users u  JOIN owner_details od ON od.user_id = u.id WHERE u.user_type = '4' AND od.deleted_at IS NULL AND od.status = '1' ORDER BY u.name";
                $data['owner_listing'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            case "all_property_listing":
                $query1 = "SELECT id,property_id, property_name FROM general_property WHERE status = 1 AND deleted_at IS NULL ORDER BY property_name";
                $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            case "property_by_owner":
                $query1 = "SELECT gp.id,gp.property_id, gp.property_name FROM general_property As gp JOIN owner_property_owned AS opo ON opo.property_id= gp.id WHERE gp.status = 1 AND gp.deleted_at IS NULL AND opo.user_id = $id ORDER BY gp.property_name";
                $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            default:
                $table = '';
                $query = '';
                break;
        }

        if (!empty($query)) {
            $data = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Data loaded successfully!');
    }


    public function ownercontributionData(){
        try{

            $owner_id=$_POST['owner_id'];
            $data = [];


            $sql_query1 = "SELECT users.name FROM users WHERE user_type = '4' and status = '1'";


            $data['owner_detail'] =$this->companyConnection->query($sql_query1)->fetchAll();


            $sql_query2="SELECT company_property_portfolio.portfolio_name FROM company_property_portfolio where company_property_portfolio.user_id=$owner_id";

            $portfolio_data['portfolio_detail'] =$this->companyConnection->query($sql_query2)->fetchAll();

            $sql_query3="SELECT owner_property_owned.property_id, general_property.property_name
    FROM owner_property_owned LEFT JOIN general_property ON general_property.id=owner_property_owned.property_id
    where owner_property_owned.user_id=$owner_id";

            $property_data['property_data'] =$this->companyConnection->query($sql_query3)->fetchAll();
            //            dd( $property_data['property_data']);

            return ['code'=>200, 'status'=>'success', 'data'=>$data ];
            return ['code'=>200, 'status'=>'success', 'data'=>$data, 'portfolio_data'=>$portfolio_data,'property_data'=>$property_data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function checkForPayment(){
        try {
            $user_id = $_POST['user_id'];
            $payment_type = $_POST['payment_type'];
//            dd($user_id);

            if ($payment_type == 'Credit Card/Debit Card' || $payment_type == 'ACH') {
                $getData = $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
//                dd($getData );
                $customer_id = $getData['stripe_customer_id'];
                if ($customer_id == "") {
                    return array('status' => 'false', 'message' => "Please add $payment_type Information first!");
                }

                $checkDefaultSource = $this->checkDefaultSource($customer_id, $payment_type);
                if ($checkDefaultSource['status'] == 'false') {
                    return array('status' => 'false', 'message' => $checkDefaultSource['message']);
                } else {
                    return array('status' => 'true', 'customer_id' => $customer_id, 'payment_type'=>$checkDefaultSource['payment_type'], 'data'=>$checkDefaultSource['data']);
                }
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function checkDefaultSource($customer_id,$payment_type){
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            if($payment_type=='Credit Card/Debit Card') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'card') === false) {
                    return array('status'=>'false','message'=>'Please set card as a default source');
                } else {
                    $card_id = $customer['default_source'];
                    $card_data = \Stripe\Customer::retrieveSource($customer_id, $card_id);
                    $card_data['exp_month'] = date("F", mktime(0, 0, 0, $card_data['exp_month'], 10));
                    return array('status'=>'true', 'data'=> $card_data, 'payment_type'=>'card');
                }
            } else if($payment_type=='ACH') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'ba_') === false) {
                    return array('status'=>'false','message'=>'Please set ACH account as a default source');
                } else {
                    $ach_id = $customer['default_source'];
                    $ach_data = \Stripe\Customer::retrieveSource($customer_id, $ach_id);
//                dd($ach_data);
                    return array('status'=>'true', 'data'=> $ach_data, 'payment_type'=>'ACH');
                }
            } else {
                return array('status'=>'true');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function createPaymentOnSubmit(){
        try {
            $receivePaymentCheck = $this->checkForReceivePayment();    /*this is to check if PM is eligible for payment or not*/
            if($receivePaymentCheck['account_status'] == 'false') {
                return array('status'=>'false','message'=>'Currently PM Account is not verified');
            } else {
                $account_id = $receivePaymentCheck['account_data']['id'];
            }

            $payment_type = $_POST['payment_type'];
            $paid_amount  = $_POST['total_amount'];
            $customer_id  = $_POST['owner_customer_id'];


            $user_id  = $_POST['selected_owner_id'];
            if($payment_type=='card' || $payment_type=='ACH' && $paid_amount>=1) {
                $return = $this->createCharge($customer_id,$account_id,$user_id,'user');
                return $return;
            } else {
               return $this->addTransactionData();
            }

        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addTransactionData(){
        try{

            $check_number = '';
            $reference_no = '';
            if ($_POST['transaction_type'] == 'Check'){
                $check_ref = $_POST['check_number'];
                $payment_type = 'check';
                $check_number = $_POST['check_number'];
            } else {
                $check_ref = $_POST['reference_number'];
                $payment_type = 'cash';
                $reference_no = $_POST['reference_number'];
            }

            /*Save data in transactions table*/
            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data['user_id'] = $_POST['selected_owner_id'];
            $data['property_id'] = $_POST['property_id'];
            $data['user_type'] = 'OWNER';
            $data['amount'] = $this->parse_number($_POST['total_amount']);
            $data['total_charge_amount'] = $this->parse_number($_POST['total_amount']);
            $data['payment_type'] = $payment_type;
            $data['type'] = 'OWNER_CONTRIBUTION';
            $data['reference_no'] = $reference_no;
            $data['check_number'] = $check_number;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

//            dd($data);
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            /*Save data in transactions table*/

           /*Save data in owner_contribution table*/
            $bank_account_id = $_POST['bank_account'];
            if (empty($bank_account_id) && $bank_account_id =='') {
                $sqlQuery = 'SELECT * FROM company_accounting_bank_account WHERE is_default="1"';
                $data = $this->companyConnection->query($sqlQuery)->fetch();
                if (!empty($data)) {
                    $bank_account_id = $data['id'];
                } else {
                    $bank_account_id = '';
                }
            }

            $contribution_data['date'] = mySqlDateFormat($_POST['contribution_date'],null,$this->companyConnection);
            $contribution_data['owner_id'] = $_POST['selected_owner_id'];
            $contribution_data['portfolio_id'] = $_POST['portfolio_id'];
            $contribution_data['property_id'] = $_POST['property_id'];
            $contribution_data['bank_account_id'] = $bank_account_id;
            $contribution_data['chart_of_account_id'] = $_POST['chart_of_account_id'];
            $contribution_data['amount'] = $_POST['total_amount'];
            $contribution_data['transation_type'] = $_POST['transaction_type'];
            $contribution_data['check_ref'] = $check_ref;
            $contribution_data['remarks'] = $_POST['remarks'];
            $contribution_data['created_at'] = date('Y-m-d H:i:s');
            $contribution_data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData1 = createSqlColVal($contribution_data);
            $qry = "INSERT INTO `owner_contribution` (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($qry);
            $stmt1->execute($contribution_data);
            /*Save data in owner_contribution table*/
            return array('code' => 200, 'status' => 'true','message' => 'Charge added successfully!');
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function parse_number($number, $dec_point=null) {
        $number = str_replace(',','',$number);
        return floatval($number);
    }

    public function createCharge($customer_id,$account_id,$user_id,$chargeType){
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {
            $paid_amount = $_POST['total_amount'];
            $paid_amount = $this->parse_number($paid_amount);

            if ($customer_id == "") {
                return array('status' => 'false', 'message' => "Stripe account not created for this user!");
            }
//            dd($account_id);
            $property_id = $_POST['property_id'];
            $bank_id = getBankSourceId($this->companyConnection, $property_id);
            if ($bank_id == ''){
                $bank_id = $account_id;
            }
            $charge = \Stripe\Charge::create([
                "amount" => $paid_amount * 100,
                "currency" => 'usd',
                "customer"=> $customer_id,
                "destination" => array("account"  => $bank_id)
            ]);

            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status  = $charge['status'];
            $payment_response  = 'Payment has been done successfully.';

            if ($stripe_status == 'succeeded'){
                $contribution_data['date'] = mySqlDateFormat($_POST['contribution_date'],null,$this->companyConnection);
//                $contribution_data['date'] = $_POST['contribution_date'];
                $contribution_data['owner_id'] = $user_id;
                $contribution_data['portfolio_id'] = $_POST['portfolio_id'];
                $contribution_data['property_id'] = $_POST['property_id'];
                $contribution_data['bank_account_id'] = $_POST['bank_account'];
                $contribution_data['chart_of_account_id'] = $_POST['chart_of_account_id'];
                $contribution_data['amount'] = $_POST['total_amount'];
                $contribution_data['transation_type'] = $_POST['transaction_type'];
                $contribution_data['check_ref'] = $_POST['reference_number'];
                $contribution_data['remarks'] = $_POST['remarks'];
                $contribution_data['created_at'] = date('Y-m-d H:i:s');
                $contribution_data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData1 = createSqlColVal($contribution_data);
                $qry = "INSERT INTO `owner_contribution` (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($qry);
                $stmt1->execute($contribution_data);
            }

            $data['charge_id'] = $charge_id;
            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data['transaction_id'] = $transaction_id;
            $data['user_id'] = $user_id;
            $data['bank_id'] = getPropertyBankId($this->companyConnection, $property_id);
            $data['property_id'] = $_POST['property_id'];
            $data['user_type'] = 'OWNER';
            $data['amount'] = $paid_amount;
            $data['type'] = 'OWNER_CONTRIBUTION';
            $data['payment_type'] =  $_POST['payment_type'];
            $data['reference_no'] = $_POST['reference_number'];;
            $data['stripe_status'] = $stripe_status;
            $data['stripe_response'] = $charge;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'true','message' => 'Charge added successfully!');
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function checkForReceivePayment(){
        try {
            $request = [];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
            if(!empty($user_data["data"])){
                if(!empty($user_data["data"]["stripe_account_id"])) {
                    $account_id = $user_data["data"]["stripe_account_id"];
                    $request["account_id"] = $account_id;
                    $stripe_account_array["stripe_account_id"] = $account_id;
                    $connected_account_detail = getConnectedAccount($request);
                    if($connected_account_detail['account_data']["business_type"]=="company"){
                        $get_person_detail = getPerson($request);
                        $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "false";
                    }else {
                        $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "false";
                    }
                } else{
                    $connected_account_detail["account_data"] = '';
                    $status =  "false";
                }
            } else{
                $status =  "false";
                $connected_account_detail["account_data"] = '';
            }
            return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],   'message' => 'Account status fetched successfully.');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }




}
$ownerContribution = new ownerContribution();