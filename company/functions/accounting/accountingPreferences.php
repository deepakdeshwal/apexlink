<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class accountingPreferences extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Fetch data from account preferences table
     * @return array
     */
    public function getOnLoadData()
    {
        $query = $this->companyConnection->query("SELECT * FROM company_accounting_preferences");
        $checkRecordExistOrNot = $query->fetch();
        return array('code' => 200, 'status' => 'success', 'data' => $checkRecordExistOrNot,'message' => 'Record fetched successfully.');
    }
    /**
     * Add/Edit data of account preferences
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = ['eft_generate_date'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Min length variable array*/
            $minlength_array = [];
            /*Number variable array*/
            $number_array = [ ];

            /*Server side validation*/
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }

                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['warning_past_status'] = isset($data['warning_past_status']) ? $data['warning_past_status'] : 0 ;
                $data['warning_future_status'] = isset($data['warning_future_status']) ? $data['warning_future_status'] : 0 ;

                $data['warn_transaction_days_past'] = (isset($data['warn_transaction_days_past']) && ($data['warn_transaction_days_past'] == 0)) ? '' : $data['warn_transaction_days_past'] ;
                $data['warn_transaction_days_future'] = (isset($data['warn_transaction_days_future']) && ($data['warn_transaction_days_future'] ==0)) ? '' : $data['warn_transaction_days_future']  ;

                $query = $this->companyConnection->query("SELECT * FROM company_accounting_preferences");
                $checkRecordExistOrNot = $query->fetch();
                if(empty($checkRecordExistOrNot)){
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    /*Save Data in Company Database*/
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_accounting_preferences (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');
                } else {
                    $edit_id = $checkRecordExistOrNot['id'];
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    /*Update Data in Company Database*/
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE company_accounting_preferences SET ".$sqlData['columnsValuesPair']." where id='$edit_id'";
                    $stmt =$this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record updated successfully.');
                }

            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching account preference data for edit
     * @return array
     */
    public function viewForEdit(){
        try {
            $id = $_POST['id'];
            $data = '';
            if(isset($id) && !empty($id))
            {
                $data = getDataById($this->companyConnection, 'company_accounting_preferences', $id);
                return $data;
            } else {
                return ['code' => 200, 'status' => 'success', 'data' => $data];
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching account preference data
     * @return array
     */
    public function view(){
        try {
            $query = $this->companyConnection->query("SELECT * FROM company_accounting_preferences");
            $checkRecordExistOrNot = $query->fetch();
            $data = [];
            if(!empty($checkRecordExistOrNot)){
                $id = $checkRecordExistOrNot['id'];
                if(isset($id) && !empty($id))
                {
                    $data = getDataById($this->companyConnection, 'company_accounting_preferences', $id);

                    $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE status = '1'");
                    $chargeCodeData =  $query->fetchAll();
                    $query1 = $this->companyConnection->query("SELECT * FROM company_credit_accounts");
                    $creditAccountData =  $query1->fetchAll();

                    $credit_account_data = [];
                    foreach ($creditAccountData as $key1=>$value1) {
                        $credit_account_data[$value1['id']] = $value1['credit_accounts'] ;
                    }
                    if(isset($credit_account_data)) {
                        foreach ($credit_account_data as $k1 => $v1) {
                            if($data['data']['cash_account_credit_bill_posting'] == $k1){ $data['data']['cash_account_credit_bill_posting'] = $v1; }
                            if($data['data']['discount_account_id'] == $k1){ $data['data']['discount_account_id'] = $v1; }
                        }
                    }

                    $charge_code_data= [];
                    foreach ($chargeCodeData as $key=>$value) {
                        $charge_code_data[$value['id']] = $value['charge_code'].' - '.$value['description'] ;
                    }
                    if(isset($charge_code_data))
                    {
                        foreach ($charge_code_data as $k=>$v) {
                            if($data['data']['rent_charges'] == $k){ $data['data']['rent_charges'] = $v;  }
                            if($data['data']['check_receive_charge'] == $k){ $data['data']['check_receive_charge'] = $v; }
                            if($data['data']['debit_type_receive_payment_charge'] == $k){ $data['data']['debit_type_receive_payment_charge'] = $v; }
                            if($data['data']['waiveoff_on_payment_received_charge'] == $k){ $data['data']['waiveoff_on_payment_received_charge'] = $v; }
                            if($data['data']['write_off_charge'] == $k){ $data['data']['write_off_charge'] = $v; }
                            if($data['data']['auto_debit_payment_receive_charge'] == $k){ $data['data']['auto_debit_payment_receive_charge'] = $v; }
                            if($data['data']['nsf_charge'] == $k){ $data['data']['nsf_charge'] = $v; }
                            if($data['data']['check_issue_vendor_tenant_charge'] == $k){ $data['data']['check_issue_vendor_tenant_charge'] = $v; }
                            if($data['data']['interest_received_from_bank_charge'] == $k){ $data['data']['interest_received_from_bank_charge'] = $v; }
                            if($data['data']['concession_charge'] == $k){ $data['data']['concession_charge'] = $v; }
                            if($data['data']['late_fee_charge'] == $k){ $data['data']['late_fee_charge'] = $v; }
                            if($data['data']['cam_charges'] == $k){ $data['data']['cam_charges'] = $v; }
                            if($data['data']['sd_refund_charge'] == $k){ $data['data']['sd_refund_charge'] = $v; }
                            if($data['data']['vacancy_charge'] == $k){ $data['data']['vacancy_charge'] = $v; }
                            if($data['data']['bad_debit_charge'] == $k){ $data['data']['bad_debit_charge'] = $v; }
                            if($data['data']['money_order_payment_received_charge'] == $k){ $data['data']['money_order_payment_received_charge'] = $v; }
                            if($data['data']['cash_payment_received_charge'] == $k){ $data['data']['cash_payment_received_charge'] = $v; }
                            if($data['data']['check_bounce_charge'] == $k){ $data['data']['check_bounce_charge'] = $v; }
                            if($data['data']['cash_payment_received_charge'] == $k){ $data['data']['cash_payment_received_charge'] = $v; }
                            if($data['data']['refund_charge'] == $k){ $data['data']['refund_charge'] = $v; }
                            if($data['data']['bank_adjustment_fee_charge'] == $k){ $data['data']['bank_adjustment_fee_charge'] = $v; }
                            if($data['data']['eft_charge'] == $k){ $data['data']['eft_charge'] = $v; }
                        }
                    }
                    return $data;
                }
            } else {
                return ['code' => 200, 'status' => 'success', 'data' => $data];;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }



}

$accountingPreferences = new accountingPreferences();