<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class TaxTypeAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company unit type table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];
            $required_array = ['tax_name'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['tax_type_id'])){
                    unset($data['tax_type_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['status'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['tax_name'] = (isset($data['tax_name']) && !empty($data['tax_name'])? $data['tax_name'] :'');
                $data['tax_type'] = (isset($data['tax_type']) && !empty($data['tax_type'])? $data['tax_type'] :'');
                $data['value'] = (isset($data['value']) && !empty($data['value'])? $data['value'] :'');

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_accounting_tax_setup', 'tax_name', $data['tax_name'], '');

                if($checkExists['is_exists'] == '1') {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Tax Type already exists.');
                }

                /*Save Data in Company Database*/
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_accounting_tax_setup (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of unit type
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];

            $tax_type_id = $data['tax_type_id'];
            /*Required variable array*/
            $required_array = ['tax_name'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['tax_type_id']) && !empty($data['tax_type_id'])){
                    unset($data['tax_type_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['tax_name'] = (isset($data['tax_name']) && !empty($data['tax_name'])? $data['tax_name'] :'');
                $data['tax_type'] = (isset($data['tax_type']) && !empty($data['tax_type'])? $data['tax_type'] :'');
                $data['value'] = (isset($data['value']) && !empty($data['value'])? $data['value'] :'');

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_accounting_tax_setup', 'tax_name', $data['tax_name'], $tax_type_id);

                if($checkExists['is_exists'] == '1') {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Tax type already exists.');
                }

                $unitTypeData = getDataById($this->companyConnection, 'company_accounting_tax_setup', $tax_type_id);

                $checkStatus = $unitTypeData['data']['status'];
                if($checkStatus == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_accounting_tax_setup SET ".$sqlData['columnsValuesPair']." where id='$tax_type_id'";
                $stmt =$this->companyConnection->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching unit type data
     * @return array
     */
    public function view(){

        try {
            $id = $_POST['id'];

            $currency = isset( $_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'';
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            if(isset($login_user_id) && !empty($login_user_id))
            {
                $data = $this->companyConnection->query("SELECT * FROM company_accounting_tax_setup WHERE id =" . $id)->fetch();
                return ['code' => 200, 'status' => 'success', 'data' => $data, 'currency' => $currency];
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update Unit Type Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $sql = "UPDATE company_accounting_tax_setup SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $id]);

            if($status == '1'){
                $_SESSION["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete Unit Type
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_accounting_tax_setup SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreTaxSetup()
    {

        try {

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_accounting_tax_setup SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];
            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $query, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$TaxTypeAjax = new TaxTypeAjax();