<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class Paybills extends DBConnection{

    /**
     * Budgeting constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Get Charts of Accounts table
     * @return array
     */
    public function payBillTransaction(){
        try{
            $postData = postArray($_POST['data']);
            $bills = $_POST['bills'];
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $payment_response  = 'Payment has been done successfully.';
            $data['user_id'] = $user_id;
            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data['user_type'] = 'PM';
            $data['amount'] = $postData['amount'];
            $data['total_charge_amount'] = $postData['amount'];
            $data['payment_mode'] = 'one_time';
            $data['payment_module'] = 'paybills';
            $data['payment_status'] = 'SUCCESS';
            $data['payment_response'] = $payment_response;
            $data['check_number'] = (isset($postData['check_number']) && !empty($postData['check_number']))?$postData['check_number']:NULL;
            $data['bank_id'] = (isset($postData['bank']) && !empty($postData['bank']))?$postData['bank']:NULL;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $id = $this->companyConnection->lastInsertId();
            $this->changeBillStatus($bills,$id);
            return array('status' => SUCCESS, 'code' => SUCCESS_CODE, 'message' =>"Payment has been done successfully.");
        } catch(Exception $exception) {
            return array('status' => ERROR, 'code' => ERROR_SERVER_CODE, 'message' =>ERROR_SERVER_MSG, 'data' =>$exception);
        }
    }

    public function payBillCheckout()
    {
        try {
            $token_id = $_REQUEST['token_id'];
            $currency = $_REQUEST['currency'];
            $amount = $_REQUEST['amount'];
            $vendor_id = $_REQUEST['vendor_id'];
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_type = $_REQUEST['user_type'];
            $dataVendor = $_REQUEST['data'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
            $query = 'SELECT stripe_account_id FROM users WHERE id='.$vendor_id;
            $vendorData = $this->companyConnection->query($query)->fetch();
            if(empty($vendorData['stripe_account_id'])){
                return  array('code' => ERROR_CODE, 'status' => ERROR,'message' => 'This user has not entered his Vendor infomation');
            }

            $charge = \Stripe\Charge::create([
                "amount" => $amount*100,
                "currency" => $currency,
                "source" => $token_id,
                "transfer_data" => [
                    "destination" => $vendorData['stripe_account_id'],
                ]
            ]);
            //dd($charge);
            $charge_id =$charge['id'];
            $transaction_id =$charge['balance_transaction'];
            $stripe_status  = $charge;
            $payment_response  = 'Payment has been done successfully.';
            $data['transaction_id'] = $charge_id;
            $data['charge_id'] = $transaction_id;
            $data['user_id'] = $user_id;
            $data['user_type'] = 'PM';
            $data['amount'] = $amount;
            $data['total_charge_amount'] = $amount;
            $data['payment_mode'] = 'one_time';
            $data['payment_module'] = 'paybills';
            $data['payment_status'] = 'SUCCESS';
            $data['stripe_status'] = $stripe_status;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $this->changeBillStatus($dataVendor);
            return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");

        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     * function to change bill status to paid
     */
    public function changeBillStatus($billArray,$id){
        try{
            foreach ($billArray as $key=>$value){
                $upateData =  "UPDATE company_bills SET status='1',transaction_id='$id'where id='$value'";
                $stmt = $this->companyConnection->prepare($upateData);
                $stmt->execute();
            }
        } catch(PDOException $exception){
            return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());
        }
    }

}
$Paybills = new Paybills();