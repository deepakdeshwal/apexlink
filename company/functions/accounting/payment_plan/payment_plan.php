<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class paymentplan extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function getpayorname(){
        $type=$_POST['id'];
        $query="";
        $data="";
        $html="";
        $html2="";
        switch ($type){
            case "1":
                $query = $this->companyConnection->query("SELECT id,name from users u where u.user_type='2' ORDER BY name");
                $data = $query->fetchAll();
                break;
            case "2":
                $query = $this->companyConnection->query("SELECT id,name from users u where u.user_type='4' ORDER BY name");
                $data = $query->fetchAll();
                break;
            case "3":
                $query = $this->companyConnection->query("SELECT id,name from users u where u.user_type='3' ORDER BY name");
                $data = $query->fetchAll();
                break;
            case "4":
                $query = $this->companyConnection->query("SELECT id,name from users u where u.user_type='8' ORDER BY name");
                $data = $query->fetchAll();
                break;
            case "5":
                $html2=" <input class=\"form-control add-input capital payor_name\" name=\"payor_name\" type=\"text\">";
                break;
        }
            if(!empty($data)){
                $html2 .= "<option value='0' selected>Select</option>";
                foreach ($data as $key=>$value){
                    $html2 .="<option value='".$value['id']."'>".$value['name']."</option>";
                }
            }
            //dd($html2);
        return ['code' => 200, 'status' => 'success', 'html' => $html2,'type'=>$type];
    }

     public function getpaymentdetails(){
         $id=$_POST['id'];
         $query = $this->companyConnection->query("SELECT ppd.id,pp.user_id,ppd.payment_id,ppd.dates,ppd.is_paid,ppd.amount_paid,ppd.extra_amount,ppd.balance_amount,pp.amount_due,pp.payor_type,pp.other_name,pp.principal_amount,pp.interest,pp.installments,pp.amount_due,pp.frequency,pp.start_date,pp.next_payment_amount from payment_plan_dates_is_paid ppd left join payment_plan pp on pp.id=ppd.payment_id where ppd.payment_id=".$id);
         $data = $query->fetchAll();
        // dd($data);
         $count=count($data);
         $query1 = $this->companyConnection->query("SELECT count(is_paid) as total from payment_plan_dates_is_paid where is_paid='Yes' AND payment_id=".$id);
         $data1 = $query1->fetchAll();
       //  dd($data1);
            return ['code' => 200, 'status' => 'success','data'=>$data,'count'=>$count,'data1'=>$data1[0]['total']];
        }

    public function savepayordata(){
        try {
            $dates=$_POST['dates'];
            $data = $_POST['formdata'];
            $data= postArray($data);
           // dd($data);
            if(is_numeric($data['payor_name']))
            {
                $data1['user_id'] = $data['payor_name'];
            }
            else{
                $data1['user_id'] = 0;
            }
            //dd($data['payor_name'][1]);
            $data1['payor_type'] = $data['payor_type'];
            $data1['other_name'] = $data['payor_name'][1];
            $data1['principal_amount'] = $data['principal_amt'];
            $data1['interest'] = $data['interest_rate'];
            $data1['installments'] = floatval($data['installments']);
            $data1['amount_due'] = floatval($data['amount_due']);
            $data1['frequency'] = $data['frequency'];
            $data1['start_date'] = mySqlDateFormat($data['start_date'],null,$this->companyConnection);
            if(!empty($data['next_date']) || $data['next_date'] !=="")  $data1['next_payment_date'] = mySqlDateFormat($data['next_date'],null,$this->companyConnection);
            $data1['next_payment_amount'] = $data['next_amt'];
            $data1['status'] = $data['status'];
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO payment_plan (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
           //dd($stmt);
            $last_id=$this->companyConnection->lastInsertId();
           // dd(count($dates));
           for ($i=0;$i<count($dates);$i++){
          //  $data2['balance_amount'] = floatval($data['amount_due']);
            $data2['dates'] = $dates[$i];
            $data2['amount_paid'] = floatval($data['next_amt']);;
            $data2['extra_amount'] = 0;
            $data2['is_paid'] = 2;
            $data2['payment_id'] = $last_id;
            $data2['created_at'] =  date('Y-m-d H:i:s');
            $data2['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData1 = createSqlColVal($data2);
            $query1 = "INSERT INTO payment_plan_dates_is_paid (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($data2);
           }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Added successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function searchForId($id, $array) {
        foreach ($array as $key => $val) {
           // dd($val);
            if ($val['id'] == $id) {
                //dd($key);
                return $key;
            }
        }
        return null;
    }

    public function updatepaydetails()
    {
        try {
            $id = $_POST['id'];
           // dd($id);
            $inst_amt=$_POST['inst_amt'];
            $final_amt=$_POST['final_amt'];
            $formdata = $_POST['formdata'];
            $data1 = postArray($formdata);
           // dd($_POST['list_amt']);
            $query1 = $this->companyConnection->query("select payment_id from payment_plan_dates_is_paid where id=" . $id);
            $data4 = $query1->fetch();
            $query4 = $this->companyConnection->query("select id from payment_plan_dates_is_paid where payment_id=" . $data4['payment_id']);
            $data5 = $query4->fetchAll();
          //  dd($data1);
            $query3 = $this->companyConnection->query("select amount_due from payment_plan where id=" . $data4['payment_id']);
            $data3 = $query3->fetch();
            $data['is_paid'] = 'Yes';
            $data['amount_paid'] = $_POST['list_amt'];
            $data['extra_amount'] = $data1['extra_amt'];
            $data['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE payment_plan_dates_is_paid SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            //dd($stmt);
            $stmt->execute();
            $a=$this->searchForId($id,$data5);
            //dd($a);
            //die();
            for($i=$a+1;$i<count($data5);$i++){
                $data6['is_paid'] = 'No';
                $data6['amount_paid'] = $inst_amt;
                $data6['extra_amount'] = 0;
                $data6['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData7 = createSqlColValPair($data6);
              //  dd(count($data5));
                $query6 = "UPDATE payment_plan_dates_is_paid SET " . $sqlData7['columnsValuesPair'] . " where id=".$data5[$i]['id'];
                $stmt6 = $this->companyConnection->prepare($query6);
                $stmt6->execute();
            }
            $data2['amount_due'] = $final_amt;
            $sqlData1 = createSqlColValPair($data2);
            $query5 = "UPDATE payment_plan SET " . $sqlData1['columnsValuesPair'] . " where id=" . $data4['payment_id'];
            $stmt4 = $this->companyConnection->prepare($query5);
            $stmt4->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        }
        catch (PDOException $e) {
        echo $e->getMessage();
        printErrorLog($e->getMessage());
    }
       }


    public function deleteplan() {
        try {
            $data=[];
            $id = $_POST['id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE payment_plan SET ".$sqlData['columnsValuesPair']." where id='$id'";
//                $query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function updatepayorinfo(){
        try {
            $id=$_POST['id'];
            $dates=$_POST['dates'];
            $data = $_POST['formdata'];
            $data= postArray($data);
          //   dd($data);
            $data1['user_id'] = $data['payor_namee'];
            $data1['payor_type'] = $data['payor_type'];
            $data1['other_name'] = $data['payor_namee'];
            $data1['principal_amount'] = $data['principal_amt'];
            $data1['interest'] = $data['interest_rate'];
            $data1['installments'] = floatval($data['installments']);
            $data1['amount_due'] = floatval($data['amount_due']);
            $data1['frequency'] = $data['frequency'];
            $data1['start_date'] = mySqlDateFormat($data['start_date'],null,$this->companyConnection);
            if(!empty($data['next_date']) || $data['next_date'] !=="")  $data1['next_payment_date'] = mySqlDateFormat($data['next_date'],null,$this->companyConnection);
            $data1['next_payment_amount'] = $data['next_amt'];
            $data1['status'] = $data['status'];
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE payment_plan SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
            //dd($query);
            $stmt = $this->companyConnection->prepare($query);
           // dd($stmt);
            $stmt->execute();
            //dd($stmt);
            // dd(count($dates));
            $query2 = $this->companyConnection->query("select id from payment_plan_dates_is_paid where payment_id=" . $id);
            $data4 = $query2->fetchAll();
           // dd(count($data4));
            for ($i=0;$i<count($data4);$i++){
                $data2['dates'] = $dates[$i];
                //dd($data2['dates']);
                $data2['amount_paid'] = floatval($data['next_amt']);;
                $data2['extra_amount'] = 0;
                $data2['is_paid'] = 2;
                $data2['payment_id'] = $id;
                $data2['created_at'] =  date('Y-m-d H:i:s');
                $data2['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData1 = createSqlColValPair($data2);
                $query1 = "UPDATE payment_plan_dates_is_paid SET " . $sqlData1['columnsValuesPair'] . " where id=" . $data4[$i]['id'];
             //   dd($query);
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute();
                }
            for ($j=count($data4);$j<count($dates);$j++){
                $data3['dates'] = $dates[$j];
                $data3['amount_paid'] = floatval($data['next_amt']);
                $data3['extra_amount'] = 0;
                $data3['is_paid'] = 2;
                $data3['payment_id'] = $id;
                $data3['created_at'] =  date('Y-m-d H:i:s');
                $data3['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData1 = createSqlColVal($data3);
                $query1 = "INSERT INTO payment_plan_dates_is_paid (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($data3);
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getpaymentdetailsreceipt(){
        $id=$_POST['id'];
        $query = $this->companyConnection->query("SELECT ppd.id,pp.user_id,ppd.payment_id,ppd.dates,ppd.is_paid,ppd.amount_paid,ppd.extra_amount,ppd.balance_amount,pp.amount_due,pp.payor_type,pp.other_name,pp.principal_amount,pp.interest,pp.installments,pp.amount_due,pp.frequency,pp.start_date,pp.next_payment_amount,ppd.updated_at from payment_plan_dates_is_paid ppd left join payment_plan pp on pp.id=ppd.payment_id where ppd.is_paid='yes' and ppd.payment_id=".$id);
        $data = $query->fetchAll();
        $dates=[];
        $sum=0;
        foreach ($data as $key=>$val){
            $val['updated_at']=dateFormatUser($val['updated_at'],null,$this->companyConnection);
        array_push($dates,$val['updated_at']);
        $sum =$val['amount_paid'] + $sum;
        }
        $data2="";
        if(!empty($data)){
        $query2 = $this->companyConnection->query("SELECT name from users where id=".$data[0]['user_id']);
        $data2 = $query2->fetch();
        }
        $count=count($data);
        $query1 = $this->companyConnection->query("SELECT count(is_paid) as total from payment_plan_dates_is_paid where is_paid='Yes' AND payment_id=".$id);
        $data1 = $query1->fetchAll();
        return ['code' => 200, 'status' => 'success','data'=>$data,'count'=>$count,'data1'=>$data1[0]['total'],'dates'=>$dates,'sum'=>$sum,'data2'=>$data2];
    }


    public function sendemail()
    {
        try{
             $data = $_POST;
             $request['action']  = 'SendMailPhp';
             $request['to'][]      = $_SESSION[SESSION_DOMAIN]['email'];
             $request['subject'] = 'Print Receipt';
             $request['message'] = 'Hello User! Please find the attached Pdf for Payment Details.';
             $request['portal']  = '1';
             $request["attachments"][] = SUBDOMAIN_URL.'/company/uploads/Receipt.pdf';
             $response = curlRequest($request);
            return ['status' => 'success', 'code' => 200, 'data' => $response];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPdfContent(){
        try{
            $html=$_POST['htmls'];
            $html='<html>'.$html.'</table>';
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function createHTMLToPDF($report){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($report);
        $dompdf->render();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'Receipt.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
       // $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'Receipt.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl,  'message' => 'Record retrieved successfully');
    }

}

$TenantShortTermAjax = new paymentplan();

