<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class bankReconcile extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Get data on add new announcement page load
     * @return array
     */
    public function getAllAccountDDl(){
        try{
            $bankAccountList = $this->companyConnection->query("SELECT id,portfolio, bank_name FROM company_accounting_bank_account WHERE status = '1' AND deleted_at IS NULL ORDER BY bank_name")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$bankAccountList];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }
    public function getAllPortfolioDDl(){
        try{
            $portfolioList = $this->companyConnection->query("SELECT id, is_default, portfolio_name FROM company_property_portfolio WHERE status = '1' AND deleted_at IS NULL ORDER BY portfolio_name")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$portfolioList];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        switch ($data_type) {
            case "property":
                $table = 'building_detail';
                $query = "SELECT id, building_name FROM `$table` WHERE property_id = ".$id;
                break;
            case "building":
                $table = 'unit_details';
                $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE building_id = ".$id;
                break;
            case "unit":
                $table = 'users';
                $query = "SELECT u.id, u.name  FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.unit_id=$id AND t.record_status = '1'";
                break;
            default:
                $table = '';
                $query = '';
                break;
        }

        if (!empty($query)) {
            $userData = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
    }

    /**
     * Create/Update/Copy/Publish an announcement
     * @return array
     * @throws Exception
     */
    public function addUtilityBills() {
        try {
            $data = $_POST;
            $firstDayNextMonth = date('Y-m-d', strtotime('first day of next month'));
            $lastDayNextMonth = date('Y-m-t',strtotime('last day of next month'));
            for ($i = 0;$i < count($data['user_id']);$i++){
                $queryData['user_id']          =  $data['user_id'][$i];
                $queryData['utility_type']     =  $data['utility'][$i];
                $queryData['start_date']       =  $firstDayNextMonth;
                $queryData['end_date']         =  $lastDayNextMonth;
                $queryData['utility_amount']   =  $data['utility_amount'][$i];
                $queryData['status']           =  'Pending';
                $queryData['created_at']       = date('Y-m-d H:i:s');
                $queryData['updated_at']       = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($queryData);
                $query = "INSERT INTO utility_billing (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($queryData);
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records created successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function editUtilityBills() {
        try {
            $data = $_POST;
            $utility_bill_id               =  $data['utility_bill_id'];

            $queryData['start_date']       =  mySqlDateFormat($data['start_date'], null, $this->companyConnection);
            $queryData['end_date']         =  mySqlDateFormat($data['end_date'], null, $this->companyConnection);
            $queryData['utility_amount']   =  $data['edit_utility_amount'];

            $sqlData = createSqlColValPair($queryData);
            $query = "UPDATE utility_billing SET ".$sqlData['columnsValuesPair']." where id=".$utility_bill_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'Records created successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Get announcement by id
     * @return array
     */
    public function delete(){
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM utility_billing WHERE id = '$id'");
            $utilityData = $query->fetch();
            if($utilityData) {
                $update_data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($update_data);
                $query = "UPDATE utility_billing SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('status' => 'success', 'data' => $utilityData, 'code' => 200);
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getDataByIdUtility(){
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM utility_billing WHERE id = '$id'");
            $utilityData = $query->fetch();
            if ($utilityData){
                $utilityData['start_date'] = dateFormatUser($utilityData['start_date'], null, $this->companyConnection);
                $utilityData['end_date'] = dateFormatUser($utilityData['end_date'], null, $this->companyConnection);
            }
            return array('status' => 'success', 'data' => $utilityData, 'code' => 200, 'message' => 'Record fetched successfully.');
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function checkPartialReconcileExist(){
        try {
            $bank_name_id = $_POST['bank_name'];
            $query = $this->companyConnection->query("SELECT * FROM bank_reconciliation WHERE bank_account = '$bank_name_id'");
            $utilityData = $query->fetch();
            if ($utilityData){
                return array('status' => 'success', 'data' => $utilityData, 'code' => 200, 'message' => 'Record found!.');
            } else {
                return array('status' => 'warning', 'data' => $utilityData, 'code' => 503, 'message' => 'No record found!.');
            }

        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addAdjustmentMoney(){
        try {
//            dd($_POST['formData']);
            $data = $_POST['formData'];

//            dd($data);
            if ($data['adjustment_type'] == '1'){
                $type = 'Withdrawal';
            } else {
                $type = 'Deposits';
            }

//            dd($type);
            $adjustmentMoneyData['date'] = dateFormatUser(date('Y-m-d H:i:s'), null, $this->companyConnection);

            $adjustmentMoneyData['period'] = dateFormatUser(date('Y-m-d H:i:s'), null, $this->companyConnection);
            $adjustmentMoneyData['reference_number'] = $data['reference_number'];
            $adjustmentMoneyData['type'] = $type;
            $adjustmentMoneyData['description'] = $data['adjustment_description'];
            $adjustmentMoneyData['amount'] = $data['adjustment_amount'];


            if ($adjustmentMoneyData){
                return array('status' => 'success', 'data' => $adjustmentMoneyData, 'code' => 200, 'message' => 'Record found!.');
            } else {
                return array('status' => 'warning', 'data' => $adjustmentMoneyData, 'code' => 503, 'message' => 'No record found!.');
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


}
$accountReconcile = new bankReconcile();