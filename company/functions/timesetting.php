<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
include_once( ROOT_URL."/company/helper/helper.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/company/helper/ddl.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

class TimeSettingsAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        //echo $action; die;
        echo json_encode($this->$action());
    }


    /**
     *  function to create a settings for super admin
     */
    public function addtimeSettings(){
        try {
            $data = postArray($_POST['form']);
            //Required variable array
            $required_array = ['timezone','time_to_send_notices','time_to_send_email_reminders'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $user = getSingleRecord($this->companyConnection,['column'=>'user_id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']],'company_time_setting');

                if($user['code'] == 200){
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE company_time_setting SET ".$sqlData['columnsValuesPair']." where user_id=".$data['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $query = $this->companyConnection->query("SELECT timezone FROM company_time_setting");
                    $data1 = $query->fetch();
                    $query = $this->companyConnection->query("SELECT code FROM timezone WHERE id=" . $data1['timezone']);
                    $data2 = $query->fetch();
                    $checktime = (explode('UTC', $data2['code']));
                    $trimcheck = rtrim($checktime[1], ')');
                    $checktime_hour_minute = (explode(':', $trimcheck));
                    $hour = $checktime_hour_minute[0];
                    $minute = $checktime_hour_minute[1];
                    $t=date('d-m-Y h:i A');
                    $newDateTime = date('h:i A', strtotime($t));
                    $new_time = date("Y-m-d h:i ", strtotime($hour." hours ".$minute." minutes"));
                    $newDateTime = date('Y-m-d H:i:s', strtotime($new_time));
                    // $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser($newDateTime, $user['id']);
                    $_SESSION[SESSION_DOMAIN]['actual_date'] = $newDateTime;
                    $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser($newDateTime, $_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);

                    $msg = 'Records Updated successfully';
                }else{
                   // print_r($data);die;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_time_setting (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
//                    print_r($query);die;
                    //print_r($query);die;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);

                    $msg = 'Records Added successfully';
                }

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $msg);
            }
        }
        catch (PDOException $e) {
            echo '<pre>';
                 print_r($e);die;
            echo $e;
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to get post data array
     */
    public function ddlOperations(){
        $returnData = DDL::insert($this->companyConnection);
        $alterColumn = array(
            array('col'=>'company_id2', 'data_type'=>'varchar', 'length'=>'255',"new_col" => ''),
            array('col'=>'c_id2', 'data_type'=>'varchar', 'length'=>'255',"new_col" => ''),
        );
        $dropColumn = array('company_id2','c_id2');
        //$returnData = DDL::update($this->conn, array('default_date_clock_settings2'), 'add', $alterColumn);
        $returnData = DDL::delete($this->conn, array('default_date_clock_settings2'));


        echo "<pre>";
        print_r($returnData);
        die();

    }

    /**
     *  function to get post data array
     */
    public function postArray($post){
        $data = [];
        foreach ($post as $key=>$value){
            if(!empty($value['value'])){
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$value['name']] = $dataValue;
        }
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     *  column value pair for insert query
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key=>$value){
            $columns .= $key.',';
            $columnsValues .= ':'."$key".',';
        }
        $columns = substr_replace($columns ,"",-1);
        $columnsValues = substr_replace($columnsValues ,"",-1);
        $sqlData = ['columns'=>$columns,'columnsValues'=>$columnsValues];
        return $sqlData;
    }
    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'c_id'){
                //do nothing
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
            }

            //$columnsValuesPair .= $key = ".$value.".',';
        }
        $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    /**
     * Random String!
     * Use this endpoint to generate the eight number random string.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function randomString($length = 8) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    /**
     *  function for listing of plans
     */
    public function timeviewSettings(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query = $this->companyConnection->query("SELECT * FROM timezone");
            $settings = $query->fetchAll();
            if($settings){
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for listing of plans
     */
    public function viewTimeSettings(){

        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query = $this->companyConnection->query("SELECT * FROM timezone");
            $settings = $query->fetchAll();
            $Userquery = $this->companyConnection->query("SELECT * FROM company_time_setting where  user_id='$user_id'");
            $userData = $Userquery->fetch();

            if(!empty($userData)){
                $timezoneId= $userData['timezone'];
            }else{
                $timezoneId='';
            }
            if($settings){
                return array('status' => 'success', 'data' => $settings,'timezoneId'=>$timezoneId);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    /**
     *  function to create a clock settings for super admin
     */
    public function addTimesSettings(){
        try {


            $data = $_POST['form'];
            $data = $this->postArray($data);

            $err_array = [];
//            $err_array = validation();
            //Checking server side validation
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                $data['company_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                if($data['user_id'] > 0 ){
                    $id = $data['user_id'];
                    $sqlData = $this->createSqlColValPair($data);
                    $query = "UPDATE company_time_setting SET ".$sqlData['columnsValuesPair']." where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Added successfully');
                }else{
                    $sqlData = $this->createSqlColVal($data);
                    $query = "INSERT INTO company_time_setting (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Added successfully');
                }

            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


}

$user = new TimeSettingsAjax();
