<?php

ini_set('memory_limit', '1024M'); // or you could use 1G
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
include_once( ROOT_URL."/helper/globalHelper.php");
class subscription extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getCompanyUserData()
    {
        try {
            $data = $_POST;
            $company_details = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$data['id']], 'users');
            return array('status' => 'success', 'code'=>'200',  'message' => 'Data fetched successfully.','data'=>$company_details);
        } catch (Exception $exception) {
            print_r($exception->getMessage()); die;
        }
    }

    public function validateCompanyFinancialData()
    {
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','fcurrency','frouting_number','fcaddress','fcaddress','fczipcode','fcstate','fcname','furl','company_document_id','fday','fmonth','fyear'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->companyConnection,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function validateFinancialData()
    {
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','fcurrency','frouting_number','fday','fmonth','fyear'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->companyConnection,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function uploadCompanyDocument(){
        try {
            $request=[];
            if ($_FILES["company_document"]["error"] == UPLOAD_ERR_OK)
            {
                $file = $_FILES["company_document"]["tmp_name"];
                // now you have access to the file being uploaded
                // $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
                $uploadPath = ROOT_URL . '/uploads/company_documents/' . $_FILES["company_document"]["name"];
                //perform the upload operation.
                if (!is_dir(ROOT_URL . '/uploads/company_documents')) {
                    //Directory does not exist, so lets create it.
                    mkdir(ROOT_URL . '/uploads/company_documents', 0777, true);
                }
                if( move_uploaded_file ( $file, $uploadPath)){
                    $request["path"] = $uploadPath;
                    $upload_documents = uploadDocuments($request);
                    if($upload_documents["code"] == 200){
                        unlink($uploadPath);
                        return array('code' => 200, 'status' => 'success', 'message' => "Document Uploaded Successfully",'document_id'=>$upload_documents["document_id"]);
                    }else{
                        return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                    }
                }else{
                    return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                }
            }
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }

    public function createUserConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
            $fdata = postArray($fdata);
            if(isset($_POST["company_id"])) {
                $company_id = $_POST["company_id"];
            }else{
                $company_id = 1;
            }

            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => '', 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'business_type'=>$fdata['fbusiness']];
            $createPmAccount = createAccount($stripe_account_array);
            if ($createPmAccount["code"] == 200 && $createPmAccount["status"] == "success") {

                $stripe_account_array["stripe_account_id"] = $createPmAccount['account_id'];
                $enablePmAccount = enableAccount($stripe_account_array);
                $domain = getDomain();
                if($enablePmAccount["code"] == 200){
                    $dbDetails = $this->companyConnection->query("SELECT * FROM users where id=".$company_id)->fetch();
                    // $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                    $data["stripe_account_id"]=$createPmAccount['account_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='".$company_id."'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $query1 = "UPDATE users SET ".$sqlData['columnsValuesPair']." where domain_name='".$domain."'";
                    $stmt1 = $this->conn->prepare($query1);
                    $stmt1->execute();
                    // $alldata['stripe_account_id'] = $createPmAccount['account_id'];

                    $stripe_account_detail_array = ["user_id"=>$company_id,"email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $fdata['furl'], 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'business_type'=>$fdata['fbusiness'],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')];

                    $save_account_detail = saveUserAccountDetail($this->companyConnection,$stripe_account_detail_array);
                }else{
                    $account = \Stripe\Account::retrieve($createPmAccount['account_id']);
                    $account->delete();
                }



                return $enablePmAccount;
            }else{
                return $createPmAccount;
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
        }

    }

    public function createCompanyConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
            $fdata = postArray($fdata);
            $company_id = $_POST["company_id"];
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => $fdata['company_document_id'], 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_line1'=>$address1,'company_line2'=>$address2,'company_country'=>'US','company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'company_tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone'=>$fdata['fcphone_number']];
            $createPmAccount = createAccount($stripe_account_array);
            if ($createPmAccount["code"] == 200 && $createPmAccount["status"] == "success") {
                $stripe_account_array["stripe_account_id"] = $createPmAccount['account_id'];
                $enablePmAccount = enableCompanyAccount($stripe_account_array);
                $domain = getDomain();
                if($enablePmAccount["code"]== 200){

                    $data["stripe_account_id"]=$createPmAccount['account_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='".$company_id."'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $query1 = "UPDATE users SET ".$sqlData['columnsValuesPair']." where domain_name='".$domain."'";
                    $stmt1 = $this->conn->prepare($query1);
                    $stmt1->execute();
                    $alldata['stripe_account_id'] = $createPmAccount['account_id'];
                    $stripe_account_detail_array = ["user_id"=>$company_id,"email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $website_url, 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_address1'=>$address1,'company_address2'=>$address2,'company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone_number'=>$fdata['fcphone_number'],'company_document_id'=>$fdata['company_document_id']];

                    $save_account_detail = saveUserAccountDetail($this->companyConnection,$stripe_account_detail_array);

                }else{
                    $account = \Stripe\Account::retrieve($createPmAccount['account_id']);
                    $account->delete();
                }
                return $enablePmAccount;
            }else{
                return $createPmAccount;
            }
        }catch (Exception $exception) {
            dd($exception);
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }

    public function createIndividualConnectedAccount(){
        try {

            $fdata = $_POST;
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['url'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['phone']) && !empty($fdata['phone'])) ? str_replace(["-", "–"], '', $fdata['phone']) : '';
            $stripe_account_array = ["email" => $fdata['email'], "first_name" => $fdata['first_name'], "last_name" => $fdata['last_name'], "city" => $fdata['city'], "state" => $fdata['state'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['postal_code'], 'day' => $fdata['day'], 'month' => $fdata['month'], 'year' => $fdata['year'], 'ssn_last_4' => $fdata['ssn_last'], 'phone' => $phone, 'document_front' => '', 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['email'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["routing_number"], 'account_number' => $fdata["account_number"], 'mcc' => $fdata["mcc"],'business_type'=>'individual'];
            $createPmAccount = createAccount($stripe_account_array);
            if ($createPmAccount["code"] == 200 && $createPmAccount["status"] == "success") {

                $stripe_account_array["stripe_account_id"] = $createPmAccount['account_id'];
                $enablePmAccount = enableAccount($stripe_account_array);
                if($enablePmAccount["code"] == 200){
                    return $enablePmAccount;
                }else{
                    $account = \Stripe\Account::retrieve($createPmAccount['account_id']);
                    $account->delete();
                }



                return $enablePmAccount;
            }else{
                return $createPmAccount;
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
        }

    }


    //create PDF Download
    /**
     * function to create daily log pdf
     */
    public function createTransactionPdf(){
        try{
            $html = '';
            $plan_id = $_POST['plan_id'];
            $sql =  "SELECT * FROM transactions WHERE ( transactions.plan_id IS NOT NULL AND transactions.id =$plan_id) ORDER BY transactions.id DESC";
//            dd($sql);
            $data = $this->companyConnection->query($sql)->fetch();
//            dd($data);
            if(!empty($data)) {
//                dd($data);
//                dd($_SESSION[SESSION_DOMAIN]);
                $html = $this->createPdfHtml($data);
                $this->createHTMLToPDF($html);
             //   print_r( $html);die();
            } else {
                return array('code' => ERROR_CODE, 'status' => ERROR, 'message' => ERROR_NO_RECORD_FOUND);
            }
        } catch(Exception $exception){
            dd($exception);
        }
    }

    /**
     * @param $replaceText
     * @return array
     */
    public function createHTMLToPDF($replaceText){
        // ini_set('memory_limit','512');
        //define( 'WP_MAX_MEMORY_LIMIT', '256M' );
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $domain = getDomain();
        $path = "upload";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/payment.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/payment.pdf';
        echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully'));
        die();
    }


    public function createPdfHtml($data){
        $public_url = COMPANY_DIRECTORY_URL.'/images/';
        $html = '<html>
                    <head>
                    <meta charset="utf-8">
                    <title>Apexlink</title>
                    
                    </head>
                    <img src="'.$public_url.'logo.png">

                    <body style="padding-top:10px; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif; font-size:13px; line-height:19px; margin:0;">
                    
                    <table width="500" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" style="padding: 10px 0;">
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="background: #00359d; color: #fff;">
                          <h2 style="margin: 10px 0;">Billing & Subscription</h2>
                        </td>
                      </tr>
                       <tr>
                        <td style="font-weight: bold; padding: 20px 0 5px 0; font-size: 16px;">Billed To: </td>
                      </tr>
                      <tr>
                        <td style="font-size: 16px; padding: 2px 0;">'.$_SESSION[SESSION_DOMAIN]['company_name'].' </td>
                      </tr>
                      <tr>
                        <td style="font-size: 16px; padding: 2px 0;">'.$_SESSION[SESSION_DOMAIN]['default_zipcode'].' </td>
                      </tr>
                      <tr>
                        <td style="font-size: 16px; padding: 2px 0;">'.$_SESSION[SESSION_DOMAIN]['default_city'].'</td>
                      </tr>
                      <tr>
                        <td style=" padding: 20px 0;">
                    
                          <table width="100%" align="center" border="1"  cellspacing="0" cellpadding="5">
                            <thead>
                              <tr>
                                <th colspan="3" style="background: #00359d; color: #fff;">
                                  <h2 style="margin:5px 0; text-align: left;">
                                    Subscription Details
                                  </h2>
                                </th>
                              </tr>
                              <tr style="background: #e5f6fe;">
                                <th style="font-size: 15px; font-weight: bold;">Date </th>
                                <th style="font-size: 15px; font-weight: bold;">Item</th>
                                <th style="font-size: 15px; font-weight: bold;">Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td style="font-size: 18px; padding: 10px;">'.subscriptionDate($data['created_at']).'</td>
                                <td style="font-size: 18px; padding: 10px;">Subscription Payment statement '.subscriptionDate($data['created_at']).'</td>
                                <td style="font-size: 18px; padding: 10px;">'.$data['total_charge_amount'].'</td>
                              </tr>
                             
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style="border-top: 1px dashed #000; border-bottom: 1px dashed #000; color: #fff; padding: 1px; text-align: center;">
                          
                        </td>
                      </tr>
                      <tr>
                        <td style="color: #000; font-style: italic; font-size: 16px; padding: 10px 0;">
                          Thank you for subscribing to Apexlink
                        </td>
                      </tr>
                    </table> 
                    </body>
                    </html>';
        return $html;
    }

}


$paymentStripe = new subscription();
?>