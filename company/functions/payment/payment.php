<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
include_once( ROOT_URL."/helper/globalHelper.php");
class payment extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function fetchAllPlanAjax(){
        try {
            $sql = "SELECT * FROM plans_history WHERE user_id='".$_SESSION[SESSION_DOMAIN]['cuser_id']."'";
            $data = $this->companyConnection->query($sql)->fetch();
            $planDetail = $this->conn->query("SELECT * FROM plans where id=".$data['subscription_plan'])->fetch();
            $GetSourceDetail=$this->GetSourceDetail();
            $proratedCalculaion=$this->proratedCalculaion($data['pay_plan_price']);

            $sqlupgrade = "SELECT * FROM plans_history WHERE user_id=".$_SESSION[SESSION_DOMAIN]['cuser_id']." and status ='1'";
            $dataupgrade = $this->companyConnection->query($sqlupgrade)->fetch();
            $dataupgrade["start_date"]=  dateFormatUser($dataupgrade['start_date'], null, $this->companyConnection);
            $dataupgrade["end_date"]=  dateFormatUser($dataupgrade['end_date'], null, $this->companyConnection);
            $resupgrade = $this->conn->query("SELECT * FROM plans where id=".$dataupgrade['subscription_plan'])->fetch();



            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','Result'=>$data,'planDetail'=>$planDetail,'GetSourceDetail'=>$GetSourceDetail,'dataupgrade'=>$dataupgrade,'resupgrade'=>$resupgrade);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }
    public function addRenewPlan() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                $currentdate=date('Y-m-d h:i:s');
                if($data['term_plan'] == '1'){
                    $enddate=date('Y-m-d h:i:s', strtotime('+1 months'));
                    $data['start_date']= $currentdate;
                    $data['end_date']=$enddate;
                }else{
                    $enddate=date('Y-m-d h:i:s', strtotime('+1 years'));
                    $data['start_date']=$currentdate;
                    $data['end_date']=$enddate;
                }
                $data['status'] = '1';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO plans_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchUpgardePlanAjax(){
        try {
            $id=$_POST['id'];
            $planDetail = $this->conn->query("SELECT * FROM plans where id=".$id)->fetch();
            $html='';
            $html='<input type="text" name="no_of_units" value='.$planDetail['number_of_units'].' style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanUnits" readonly>';


            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','html'=>$html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }


    public function fetchUpgardePaymenttypeAjax(){
        try {
            $termPlanId=$_POST['id'];
            $sql = "SELECT * FROM plans_history WHERE user_id=".$_SESSION[SESSION_DOMAIN]['cuser_id']." order by id desc";
            $data = $this->companyConnection->query($sql)->fetch();
            $totalprice= $this->calculationPayment($termPlanId,$data['pay_plan_price']);
            if(!empty($data['discount'])){
                $totalpricePay= $totalprice - $data['discount'];
            }else{
                $totalpricePay= $totalprice;
            }

            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','totalprice'=>$totalprice,'totalpricePay'=>$totalpricePay);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }

    public function calculationPayment($termPlan,$pay_plan_price){
        //static payments variables for monthly dedcution

        $apexlinkServiceFee=75.00;
        $stripeTransactionFee=0.25;
        $feeForACH=0.25;
        $feeForCard=1.50;
        $apexlinkAdminFee=0.20;
        $stripeAccountFee=2.00;
        $ACH ='ACH';

        //static payments variables for yearly dedcution
        $apexlinkServiceFeeYearly= 75.00 * 12;
        $stripeTransactionFeeYearly=2.94;
        $feeForCardYearly=17.63;
        if($termPlan == '1'){
            if($ACH == 'ACH'){
                $totalsubscriptionCharges=$apexlinkServiceFee + (double)$pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
            }else{
                $totalsubscriptionCharges=$apexlinkServiceFee + (double)$pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
            }
            return (double)$totalsubscriptionCharges;
        }
        else {
            $pay_plan_price=(double)$pay_plan_price * 11;
            if($ACH == 'ACH'){
                $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForACH;
            }else{
                $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForCardYearly;
            }
            return $totalsubscriptionCharges;
        }
    }

    public function addUpgradePlan() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $GetSourceDetail = $this->GetSourceDetail();
                if (empty($GetSourceDetail)) {
                    return array('code' => 500, 'status' => 'error', 'data' => $err_array, 'message' => 'Please Complete Payment Details First!');
                } else {
                    //Save Data in Company Database
                    $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    unset($data['card']);
                    $sqlStatus = "SELECT * FROM plans_history WHERE user_id=" . $_SESSION[SESSION_DOMAIN]['cuser_id'] . " order by id ASC";
                    $dataStatus = $this->companyConnection->query($sqlStatus)->fetchAll();
                    if (!empty($dataStatus)) {
                        $queryStatus = "UPDATE plans_history SET status ='0' where user_id=".$_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $stmtStatus = $this->companyConnection->prepare($queryStatus);
                        $stmtStatus->execute();
                    }

                    $currentdate = date('Y-m-d h:i:s');
                    if ($data['term_plan'] == '1') {
                        $enddate = date('Y-m-d h:i:s', strtotime('+1 months'));
                        $data['start_date'] = $currentdate;
                        $data['end_date'] = $enddate;
                    } else {
                        $enddate = date('Y-m-d h:i:s', strtotime('+1 years'));
                        $data['start_date'] = $currentdate;
                        $data['end_date'] = $enddate;
                    }
                    $data['status'] = '1';
                    $discount = "SELECT discount_price FROM users WHERE id=" . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $datadiscount = $this->companyConnection->query($discount)->fetch();
                    if(!empty($datadiscount['discount_price'])){
                        $data['discount']=$datadiscount['discount_price'];
                    }
                    else{
                        $data['discount']=0;
                    }
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO plans_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();


                    $sqlusers = "SELECT stripe_customer_id FROM users WHERE id=" . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $datausers = $this->companyConnection->query($sqlusers)->fetch();
                    $request = [];
                    $request["customer_id"] = $datausers['stripe_customer_id'];
                    if ($datausers['stripe_customer_id'] != '') {
                        $customer_id = $datausers['stripe_customer_id'];
                        $cards = Allsource($customer_id, $_POST['card']);
                        foreach ($cards['data'] as $cc) {
                            updateCustomer($customer_id, $cc['id']);
                        }
                    }

                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                }
            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function createCustomer($user_id)
    {
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer    =     \Stripe\Customer::create(array(
                'email' => $email
            ));
            $customer_id =        $customer['id'];

            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";

            $stmt = $this->companyConnection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }
    public function GetSourceDetail(){
        $sqlusers = "SELECT * FROM users WHERE id=".$_SESSION[SESSION_DOMAIN]['cuser_id'];
        $datausers = $this->companyConnection->query($sqlusers)->fetch();
        $request=[];
        $request["customer_id"] = $datausers['stripe_customer_id'];
        if(empty($request["customer_id"])){
            $getDetailUser= $this->createCustomer($_SESSION[SESSION_DOMAIN]['cuser_id']);
            $request["customer_id"] = $getDetailUser;
            $getDetailUser= getCustomer($request);
        }else{
            $getDetailUser= getCustomer($request);
        }
        $sources = $getDetailUser['customer_data']['default_source'];
        if(isset($sources) && !empty($sources)) {
            if (strpos($sources, 'card') !== false) {

                $card = 'yes';
            } else {
                $card = 'no';
            }
        }else{
            $card ="";
        }
        return $card;
    }

    public function paymentCalculateForPM(){
        try {
            //static payments variables for monthly dedcution
            $apexlinkServiceFee=75.00;
            $apexlinkAdminFee=0.20;
            $stripeAccountFee=2.00;
            //static payments variables for yearly dedcution
            $apexlinkServiceFeeYearly= 75.00 * 12;


            $getPlanD=  $this->companyConnection->query("SELECT subscription_plan,term_plan,plan_price,pay_plan_price FROM plans_history WHERE user_id=".$_SESSION[SESSION_DOMAIN]['cuser_id']." and status='1'")->fetch();
            $GetSourceDetail=$this->GetSourceDetail();
            if(!empty($getPlanD)){
                $pay_plan_price=$getPlanD['pay_plan_price'];

                if($getPlanD['term_plan'] == '1'){
                    $stripeTransactionFee=$getPlanD['pay_plan_price'] * 0.25/100;
                    $feeForACH= $getPlanD['pay_plan_price'] * 0.25/100;
                    $feeForCard= $getPlanD['pay_plan_price']  * 1.5/100;
                    if($GetSourceDetail == 'no'){
                        $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
                    }else if($GetSourceDetail == 'yes'){
                        $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    }else{
                        $totalsubscriptionCharges =$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    }

                    $pricetotal= round($totalsubscriptionCharges);
                    return array('code' => 400, 'status' => 'success','TSC'=>$pricetotal);
                }
                else if($getPlanD['term_plan'] == '4'){
                    $stripeTransactionFee=$getPlanD['pay_plan_price'] * 2.94/100;
                    $feeForACH= $getPlanD['pay_plan_price'] * 0.25/100;
                    $feeForCard= $getPlanD['pay_plan_price']  * 17.63/100;
                    $pay_plan_price=(double)$pay_plan_price * 11;
                    if($GetSourceDetail == 'no'){
                        $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
                    }else if($GetSourceDetail == 'yes'){
                        $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    }else{
                        $totalsubscriptionCharges = $apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    }

                    $pricetotal= number_format(round($totalsubscriptionCharges, 2),2);
                    return array('status' => 'success','TSC'=>$pricetotal);
                }

                else{
                    return array('status' => 'error','message'=>'Something Went Wrong');
                }

            }


        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }
    public function proratedCalculaion(){
        $getPlanD=  $this->companyConnection->query("SELECT subscription_plan,term_plan,plan_price,pay_plan_price FROM plans_history WHERE user_id=1 and status='1'")->fetch();
         $pay_plan_price= $getPlanD['pay_plan_price'];



        $feeForACH=0.25;

        $apexlinkAdminFee=0.20;
        $stripeAccountFee=2.00;
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
        $nextAmount = $default_symbol.number_format($getPlanD['plan_price'],2);
        $plan_id = $getPlanD['subscription_plan'];
        $getTransactionCount = $this->getTransactionCount();
        if($getPlanD['term_plan']==4)
        {
            $stripeTransactionFee=$getPlanD['pay_plan_price'] * 2.94/100;
            $feeForCard=$getPlanD['pay_plan_price'] * 17.63/100;
                if($getTransactionCount==0)
                {
                $apexlinkServiceFee=00.00; 
                }
                else
                {
                $apexlinkServiceFee= 75.00;

                }
            
            $enddate=date('Y-m-d h:i:s', strtotime('first day of next year'));
            $enddate =    dateFormatUser($enddate, null, $this->companyConnection);


        }
        else
        {
            $stripeTransactionFee=$getPlanD['pay_plan_price'] * 0.25/100;
            $feeForCard=$getPlanD['pay_plan_price'] * 1.50/100;
                if($getTransactionCount==0)
                {
                $apexlinkServiceFee=00.00; 
                }
                else
                {
                $apexlinkServiceFee= 75.00;

                }
            $enddate=date('Y-m-d h:i:s', strtotime('first day of next month'));
            $enddate =    dateFormatUser($enddate, null, $this->companyConnection);


        }
        $getPlans  = "SELECT plan_name FROM plans WHERE  id=" . $plan_id;
        $plans =   $this->conn->query($getPlans)->fetch();
        $units = $plans['plan_name'];
        // Get total # of days in month
        $total_days = date("t");
        $today = date('Y-m-d h:i:s');
        $lastDayOfThisMonth = date('Y-m-d h:i:s', strtotime('last day of this month'));

        $nbOfDaysRemainingThisMonth =  strtotime($lastDayOfThisMonth) - strtotime($today);
        $days_left = $nbOfDaysRemainingThisMonth /60/60/24;
        

         $pro_rate =(int) round(((double)$pay_plan_price/ $total_days) * $days_left, 2);
      
        if($pro_rate=='')
        {
         $pro_rate = 0;   
        }

        $totalsubscriptionCharges=$apexlinkServiceFee + $pro_rate + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;

        $sqlusers = "SELECT stripe_customer_id FROM users WHERE id=1";
        $customerData = $this->companyConnection->query($sqlusers)->fetch();
        $customer_id = $customerData['stripe_customer_id'];

        if($customer_id!='')
        {
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
            $customer = \Stripe\Customer::retrieve($customer_id);

            $sources = $customer['default_source'];
            if(isset($sources) && !empty($sources)) {
                if (strpos($sources, 'card') !== false) {

                    $card = 'card';
                }else if(strpos($sources, 'ba_') !== false) {
                    $card = 'bank';
                }else{

                    $card = 'No Payment Method';
                }
            }else{
                $card = 'No Payment Method';
            }
        }
        $paymentCalculateForPM=$this->paymentCalculateForPM();
        $nxtAmt = $paymentCalculateForPM['TSC'];
        return array('status' => 'error','prorated_plan'=>number_format($totalsubscriptionCharges,2),'apexlinkAdminFee'=>number_format($apexlinkAdminFee,2),'stripeAccountFee'=>number_format($stripeAccountFee,2),'service_fee'=>number_format($apexlinkServiceFee,2),'stripeTranactionFee'=>number_format($stripeTransactionFee,2),'fee_for_card'=>$feeForCard,'remaining_days'=>$days_left,'plan_id'=>$plan_id,'next_amount'=>$nxtAmt,'due_date'=>$enddate,'default_source'=>$card,'units'=>$units,'pro_rate'=>$pro_rate,'term_plan'=>$getPlanD['term_plan']);

    }



    public function getTransactionCount()
    {
        $query = "SELECT id FROM transactions WHERE user_id='1'";
        $get = $this->companyConnection->query($query)->fetchAll();
        return count($get);

    }

    public function fetchPlanAjax(){
        try {
            $sql = "SELECT * FROM plans WHERE status ='1' and id >=".$_POST['id'];
            $data = $this->conn->query($sql)->fetchAll();
            $discount = "SELECT discount_price FROM users WHERE id=" . $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $datadiscount = $this->companyConnection->query($discount)->fetch();
            if(!empty($datadiscount['discount_price'])){
                $discount=$datadiscount['discount_price'];
            }
            else{
                $discount=0;
            }
            $html='';
//            $html = '<option value="default">Select</option>';
            foreach ($data as $dd){
                $html .="<option  price= ".$dd['month_rate']." units =".$dd['number_of_units']." value= " . $dd['id'] . ">" . $dd['plan_name'] . "</option>";

            }



            return array('status' => 'success', 'code' => 200, 'html' => $html,'discount'=>$discount);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }





}


$paymentStripe = new payment();
?>