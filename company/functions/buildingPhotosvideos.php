<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class buildingPhotosvideos extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function upload_photos_videos(){
        try {
            $files = $_FILES;
            $markteingData = json_decode($_POST['marketing_site']);
            if(!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
                    $fileData = getSingleRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], 'building_file_uploads');
                    if($fileData['code'] == '200'){
                        return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['property_id'] = '12345';
                    $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $data['file_name'] = $file_name;
                    $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['file_location'] = $path . '/' . $name . '.' . $ext;
                    $data['file_extension'] = $ext;
                    $data['codec'] = $value['type'];
                    $data['marketing_site'] = $this->compareArray($value,$markteingData);
                    $data['file_type'] = strstr($value['type'], "video/")?3:1;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO building_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $query = "INSERT INTO building_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function deleteFile(){
        try {
            $id = $_POST['id'];
            $data = getSingleRecord($this->companyConnection,['column'=>'id','value'=>$id],'building_file_uploads');
            $query = "DELETE FROM building_file_uploads WHERE id=$id";
            $stmt = $this->companyConnection->prepare($query);
            if($stmt->execute()){
                $path = ROOT_URL . '/company/'.$data['data']['file_location'];
                unlink($path);
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents deleted successfully.');
            } else {
                return array('code' => 500, 'status' => 'error', 'message' => 'Error occured while deleting document.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function compareArray($data,$compare){
        foreach ($compare as $key=>$value){
            if($data['name'] == $value->name && $data['size'] == $value->size){
                return $value->siteMain;
            }
        }
        return '0';
    }

}

$buildingDetail = new buildingPhotosvideos();
?>