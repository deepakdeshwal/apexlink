<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class UserAjax extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function for super admin user login
     * @return array
     */
    public function login() {
        // printErrorLog('error');
        try {
//              print_r($_POST);die();
            $page_url = $_SERVER['HTTP_REFERER'];
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $timezone = isset($_POST['timezone']) && !empty($_POST['timezone'])?$_POST['timezone']:'';

            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $domain = array_shift($domain);

            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
            $user = $query->fetch();
            /*dd($user);*/
            $query = $this->companyConnection->query("SELECT * FROM active_inactive_status"); 
            $active_inactive_status = $query->fetch();

            if ($user['id']) {

                if ($user['parent_id'] == 0) {
//                    dd($user['parent_id']);
                    $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
                    $super_admin_user = $query->fetch();

                    if ($super_admin_user['status'] == 0) {
                        return array('status' => 'error', 'code' => 503, 'message' => 'Account deactivated, please contact the system administrator.');
                    }
                } else {
                    $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type!='0'");
                    $main_company_admin_user = $query->fetch();

                    if ($main_company_admin_user['status'] == 0) {
                        return array('status' => 'error', 'code' => 503, 'message' => 'Account deactivated, please contact the system administrator.');
                    }
                }
          
                //If company login is from go to site link
                checkPermission($user['id'],$user['role'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];
                $_SESSION[SESSION_DOMAIN]['timezone'] = $timezone;
                $_SESSION[SESSION_DOMAIN]['default_name'] = $user['first_name'].' '.$user['last_name'];
                $_SESSION[SESSION_DOMAIN]['name'] = userName($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['role'] = $user['role'];
                $_SESSION[SESSION_DOMAIN]['company_name'] = $user['company_name'];
                $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];
                $_SESSION[SESSION_DOMAIN]['phone_number'] = $user['phone_number'];
                $_SESSION[SESSION_DOMAIN]['domain_name'] = $user['domain_name'];


                $system_date=$_POST['dateformats'];
                // print_r($system_date);die;
                $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser($system_date, $user['id'], $this->companyConnection);

                $_SESSION[SESSION_DOMAIN]['actual_date'] = $system_date;
                $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';
                $_SESSION[SESSION_DOMAIN]['admindb_id'] = isset($user['admindb_id']) ? $user['admindb_id'] : '';

                //Default setting session data
                $_SESSION[SESSION_DOMAIN]['default_notice_period'] = defaultNoticePeriod($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_rent'] = defaultRent($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_application_fee'] = defaultApplicationfee($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_zipcode'] = defaultZipCode($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_city'] = defaultCity($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_currency'] = defaultCurrency($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN]['default_currency'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['pagination'] = pagination($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['payment_method'] = defaultPaymentMethod($user['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['property_size'] = defaultPropertySize($user['id'],$this->companyConnection);
                $query = 'SELECT COUNT(id) FROM flags where completed = "0" AND deleted_at IS NULL';
                $data = $this->companyConnection->query($query)->fetch();
                $_SESSION[SESSION_DOMAIN]['total_flag_count'] = $data['COUNT(id)'];

                $var1 = strrpos($page_url, '?');
                $check_subscription = $this->checkSubscription();
                if($check_subscription['code'] == SUCCESS_CODE && $check_subscription['message'] == SUBSCRIPTION_EXPIRED){
                    return array('code'=>502,'status' => 'error_sus', 'message' => SUBSCRIPTION_EXPIRED);
                }

                if($check_subscription['freePlan'] == '1'){
                    $countDown = $this->countDownTimer($check_subscription);
                    $_SESSION[SESSION_DOMAIN]['countDownTimer'] = $this->countDownTimer($check_subscription);
                }

                if (!empty($var1) || $user['enabled_2fa'] == 0) {

//                  $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];

                    if (!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                        $remember_login_data['email'] = $user["email"];
                        $remember_login_data['password'] = $user["actual_password"];
                        $remember_login_data['remember_token'] = $user["remember_token"];

                        $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                        setcookie("remember_login", serialize($remember_login_data), $expire);
                    } else {     
                        setcookie("remember_login", '');
                    }

                    //insert user detail in audit trail
                    $this->audiTrail($user,'login');
 
                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status, 'go_to_site_link' => 'on');
                } else {
                    if ($user['enabled_2fa'] == 1) {
                        $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];

                        if (!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                            $remember_login_data['email'] = $user["email"];
                            $remember_login_data['password'] = $user["actual_password"];
                            $remember_login_data['remember_token'] = $user["remember_token"];

                            $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                            setcookie("remember_login", serialize($remember_login_data), $expire);
                        } else {
                            setcookie("remember_login", '');
                        }

                        //2Fa Authorization mail for OTP
                        $this->sendOTP($user);

                        //insert user detail in audit trail
                        return array('status' => 'success', 'data' => $user, 'active_inactive_status' => $active_inactive_status);

                    }

                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status);
                }
            } else {
                return array('status' => 'error', 'message' => 'Invalid Email or password');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Send OTP
     * @param $user
     */
    public function sendOTP($user){
        $OTP = randomTokenString(6);
        $OTP_mail = $this->sendMailForOTP($user, $OTP);
//        dd($OTP_mail);
        if($OTP_mail->code == 200){
            $token_2fa = md5($OTP);
            $token_2fa_expiry = date('Y-m-d H:i:s');
            $update_token_company= $this->companyConnection
                ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                ->execute([$token_2fa, $token_2fa_expiry, $user['id']]);

            $update_token_superadmin= $this->conn
                ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                ->execute([$token_2fa, $token_2fa_expiry, $user['id']]);
        }
    }

    /**
     * Send mail to the user for OTP
     * @param $userData
     * @param $OTP
     * @return array|mixed|string
     */
    public function sendMailForOTP($userData, $OTP)
    {
        try{
//            dd($userData);
            $user_name = userName($userData['id'], $this->companyConnection);
            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/two_fa_email.php');
            $body = str_replace("#logo#",$logo,$body);
            $body = str_replace("#user_name#",$user_name,$body);
            $body = str_replace("#token_2fa#",$OTP,$body);

            $request['to'][]    = $userData['email'];
            $request['action']  = 'SendMailPhp';
            $request['subject'] = '2Fa OTP for Apexlink';
            $request['portal']  = '1';
            $request['message'] = $body;
            $response = curlRequest($request);
            return $response;
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Check OTP is valid or not
     * @return array
     */
    public function checkTwoFaOTP( )
    {
        try{
            $token_2fa = md5($_POST['two_fa_otp']);
            $email = $_SESSION[SESSION_DOMAIN]['email'];
            $timezone = $_SESSION[SESSION_DOMAIN]['timezone'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $domain = array_shift($domain);

            $query = $this->companyConnection->query("SELECT * FROM users WHERE email='$email' AND token_2fa= '$token_2fa' AND domain_name='$domain' AND user_type='1'");
            $user = $query->fetch();
            if ($user['id']){

                $token_2fa_expiry_time = $user['token_2fa_expiry'];
                $minutes_to_add = 5;
                $time = new DateTime($token_2fa_expiry_time);
                $token_2fa_expiry_tme = $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

                $token_2fa_expiry_tme = $token_2fa_expiry_tme->format('Y-m-d H:i:s');
                $current_date = date('Y-m-d H:i:s');

                if($current_date > $token_2fa_expiry_tme){
                    $this->sendOTP($user);
                    return array('code' => 504, 'status' => 'error','message' => 'Your OTP has been expired! Please check your mail.');
                } else {
                    $_SESSION[SESSION_DOMAIN]['cuser_id'] = $user['id'];
                    $_SESSION[SESSION_DOMAIN]['timezone'] = $timezone;
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($user['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['role'] = $user['role'];
                    $_SESSION[SESSION_DOMAIN]['company_name'] = $user['company_name'];
                    $_SESSION[SESSION_DOMAIN]['email'] = $user['email'];
                    $_SESSION[SESSION_DOMAIN]['phone_number'] = $user['phone_number'];
                    $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $user['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($user['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['super_admin_name'] = '';
                    //$connection = DBConnection::dynamicDbConnection($user['host'], $user['database_name'], $user['db_username'], $user['db_password']);

                    $query = $this->companyConnection->query("SELECT * FROM active_inactive_status");

                    $active_inactive_status = $query->fetch();

                    $update_token_company= $this->companyConnection
                        ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                        ->execute([null, null, $user['id']]);

                    $update_token_superadmin= $this->conn
                        ->prepare("UPDATE users SET token_2fa=?, token_2fa_expiry=? WHERE id=?")
                        ->execute([null, null, $user['id']]);

                    $this->audiTrail($user,'login');

                    return array('status' => 'success', 'code' => 200, 'data' => $user, 'active_inactive_status' => $active_inactive_status);
                }

            } else  {
                return array('code' => 504, 'status' => 'error','message' => 'Your OTP is incorrect! Please check your mail.');
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * Function for company user logout
     * @return array
     */
    public function logout(){
       /* if(strpos($_POST['data'],"/Reporting/")=="0" && strpos($_POST['data'],"/Reporting/")!=""){
            $url="/Reporting/Reporting";
        }else{
            $url=$_POST['data'];
        }*/
        $url=$_POST['data'];
        $condition = ['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']];
        $user = getSingleRecord($this->companyConnection,$condition,'users');
        if(!empty($url)){
            $update_last_url_company= $this->companyConnection
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['cuser_id']]);

            $update_last_url_super_admin= $this->conn
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['cuser_id']]);
        }
        $this->audiTrail($user['data'],'logout');
        unset($_SESSION[SESSION_DOMAIN]['cuser_id']);
        return array('status' => 'success', 'data' =>[$update_last_url_company, $update_last_url_super_admin]);
    }
    /**
     *  function for company user forgot password
     */
    public function forgotPassword() {
        try {
            $email = $_POST['email'];
            $domain = ADMIN_URL;
            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND domain_name='$domain' AND user_type='1'");
            $user = $query->fetch();

            if ($user) {
                $forgot_password_token = randomTokenString(50);
                $email = $user['email'];

                $update_forgot_password_token = $this->conn
                    ->prepare("UPDATE users SET reset_password_token=? WHERE email=?")
                    ->execute([$forgot_password_token, $email]);

                $reset_password_link = SITE_URL.'User/ResetPassword?token='.$forgot_password_token;
                $logo = SUBDOMAIN_URL."/company/images/logo.png";
                $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/forgot_password.php');
//                $user_name = (!empty($user['nick_name'])) ? $user['nick_name'] : $user['first_name'];
//                print_r($user['id']); die();
                $user_name = userName($user['id'],$this->companyConnection);;

                $body = str_replace("#logo#",$logo,$body);
                $body = str_replace("#user_name#",$user_name,$body);
                $body = str_replace("#reset_password_link#",$reset_password_link,$body);


                $user['message'] = $body;

                return (['status' => 'success',$user, $update_forgot_password_token]);
            } else {
                return array('status' => 'error', 'message' => 'Details corresponding to this email-Id does not exist.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for super admin user reset password
     */
    public function resetPassword() {
        try {
            $actual_password = $_POST['password'];
            $password = md5($_POST['password']);
            $forgot_password_token = $_POST['forgot_password_token'];
            $empty_forgot_password_token = NULL;
            $query = $this->conn->query("SELECT * FROM users WHERE reset_password_token='$forgot_password_token' ");
            $user_email = $query->fetch();
            $email = $user_email['email'];
            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' ");
            $forgot_password_user = $query->fetch();

            if ($user_email) {
                if ($forgot_password_user) {
                    $email = $forgot_password_user['email'];
                    $forgot_password_token = $password;

                    $update_forgot_password = $this->conn
                        ->prepare("UPDATE users SET password=?, actual_password=? WHERE email=?")
                        ->execute([$forgot_password_token, $actual_password, $email]);

                    if ($update_forgot_password) {
                        $_SESSION[SESSION_DOMAIN]["message"] = "Password changed successfully.";
                        return array('status' => 'success', 'message' => 'Password changed successfully.');
                    } else {
                        return array('status' => 'warning', 'message' => 'Password not changed successfully.');
                    }
                    //return json_decode($response);
                } else {
                    return array('status' => 'error', 'message' => 'This password reset token is invalid.');
                }
            } else {
                return array('status' => 'error', 'message' => 'No user exists with this email-Id.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * function to create a user for super admin
     * @return array
     */
    public function createUser() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name','email','status','mobile_number'];
            /* Max length variable array */
            $maxlength_array = ['first_name'=>20,
                'mi'=>2,
                'last_name'=>20,
                'maiden_name'=>20,
                'nick_name'=>20,
                'email'=>50,
                'work_phone'=>12,
                'mobile_number'=>12,
                'fax'=>12];
            //Number variable array
            $number_array = ['work_phone', 'mobile_number', 'fax'];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
              //  print_r($_POST); die;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['domain_name'] = $_SERVER['REQUEST_URI'];
                $password = randomString();
                $data['actual_password'] = $password;
                $data['password'] = md5($password);
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['user_type'] = 0;
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to update a user for super admin
     * @return array
     */
    public function updateUser() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name','status','mobile_number'];
            /* Max length variable array */
            $maxlength_array = ['first_name'=>20,
                'last_name'=>20,
                'status'=>1,
                'mobile_number'=>12];
            //Number variable array
            $number_array = ['work_phone', 'mobile_number', 'fax'];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['domain_name'] = $_SERVER['REQUEST_URI'];
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['user_type'] = 0;
                $user_id = $data['id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$user_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to get post data array
     * @param $post
     * @return array
     */
    public function getUserDetails(){
        $data = $_POST['user_id'];
        $query = $this->conn->query("SELECT * FROM users WHERE id='$data'");
        $user = $query->fetch();
        echo json_encode($user);
        die();
    }

    /**
     * Delete User!
     * Use this endpoint to delete the user.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function deleteManageUser() {
        try {
            $data=[];
            $manageuser_id = $_POST['manageuser_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$manageuser_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update user password!
     * Use this endpoint to update the user password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changePassword(){
        try {
            $data=[];
            if ($_POST['id'] == "") {
                $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            }else{
                $data['id'] = $_POST['id'];
            }
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];


            //Required variable array
            $required_array = ['npassword','cpassword'];
            /* Max length variable array */
            $maxlength_array = ['npassword'=>50,'cpassword'=>50];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = createSqlColValPair($record);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Create Apexlink List!
     * Use this endpoint to create the apexlink list.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return array
     * @throws Exception
     */
    public function createApexlinkList(){
        try {
            $data=[];
            //$data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['title'] = $_POST['title'];
            $data['date'] = mySqlDateFormat($_POST['date'],$_SESSION[SESSION_DOMAIN]['user_id']);
            $data['time'] = mySqlTimeFormat($_POST['time']);
            $data['status'] = 1;
            $data['description'] = $_POST['desc'];
            $data['created_at'] =  date('Y-m-d H:i:s');
            $data['updated_at'] =  date('Y-m-d H:i:s');


            //Required variable array
            $required_array = ['title','date','time'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $id = $_SESSION[SESSION_DOMAIN]['user_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO apexlink_list (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * validate user current password!
     * Use this endpoint to validate the user current password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function checkCurrentPassword(){
        try {
            $data=[];
            $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['password'] = $_POST['password'];

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
                $id = $data['id'];

                $query = $this->conn->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Password do not match!');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update status!
     * Use this endpoint to update the user status.
     * @author Parvesh
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changeStatusManageUser() {
        try {
            $data=[];
            $manageuser_id = $_POST['manageuser_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Deactivate") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$manageuser_id'";

                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Server side Validations
     * @author Deepak
     * @param $data
     * @param $db
     * @param array $required_array
     * @param array $maxlength_array
     * @param array $number_array
     * @return array
     */
    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

    /**
     * @param $data
     * @param $type
     */
    public function audiTrail($data,$type){
        $insert = [];
        $insert['user_id'] = $data['id'];
        $insert['username'] = $data['name'];
        $insert['role'] = getRole($data['user_type']);
        $insert['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $insert['action'] = $type;
        $insert['created_at'] =  date('Y-m-d H:i:s');
        $insert['updated_at'] =  date('Y-m-d H:i:s');
        $insert['description'] = ($type == 'login')?'Login : System Login':'Login : System Logout';
        $sqlData = createSqlColVal($insert);
        $query = "INSERT INTO audit_trial (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($insert);
    }

    /**
     * function to check user subscription
     * @return array
     */
    public function checkSubscription(){
        try {
            $userData = getSingleRecord($this->companyConnection,['column'=>'id','value'=>1], 'users');
           // dd($userData);
            if($userData['code'] == 200){
                if(($userData['data']['free_plan'] == '0' || empty($userData['data']['free_plan'])) && ($userData['data']['payment_status'] == '0' || empty($userData['data']['payment_status']))){
                    return array('code' => SUCCESS_CODE, 'status' => SUCCESS,'message' => SUBSCRIPTION_EXPIRED);
                } else if($userData['data']['free_plan'] == '1') {
                    $date = strtotime(date('Y-m-d'));
                    if(!empty($userData['data']['expiration_date'])) {
                        $expirationDate = strtotime($userData['data']['expiration_date']);
                        if ($date >= $expirationDate) {
                            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUBSCRIPTION_EXPIRED);
                        }
                    }
                    if(!empty($userData['data']['remaining_days'])) {
                        $created_at = $userData['data']['created_at'];
                        $expirationDate = date('Y-m-d', strtotime($created_at. ' + '.$userData['data']['remaining_days'].' days'));
                        if ($date >= strtotime($expirationDate)) {
                            return array('code' => SUCCESS_CODE, 'status' => SUCCESS, 'message' => SUBSCRIPTION_EXPIRED);
                        }
                    }
                }
                return array('code' => SUCCESS_CODE, 'status' => SUCCESS,'message' => SUBSCRIPTION_VALID, 'expirationDate' => $userData['data']['expiration_date'],'remainingDays' => $userData['data']['remaining_days'], 'extendedDays' => $userData['data']['extended_days'],'freePlan' => $userData['data']['free_plan']);
            }
            return array('code' => ERROR_CODE, 'status' => ERROR,'message' => ERROR_NO_RECORD_FOUND);
        } catch(Exception $exception){
            return array('code' => ERROR_CODE, 'status' => ERROR,'message' => $exception->getMessage());
        }
    }

    /**
     * function to calculate count Down Timer
     * @return array
     */
    public function countDownTimer($userData){
        try{
            if(!empty($userData['expirationDate'])){
                $expDate = date('M d, Y H:m:i', strtotime($userData['expirationDate']));
                return $expDate;
            }
            $remainingDays = !empty($userData['remainingDays'])?$userData['remainingDays']:0;
            $extendedDays = !empty($userData['extendedDays'])?$userData['extendedDays']:0;
            $totalDays = $remainingDays+$extendedDays;
            return $totalDays;
        } catch (Exception $exception) {
            return array('code' => ERROR_CODE, 'status' => ERROR,'message' => $exception->getMessage());
        }
    }

}

$user = new UserAjax();
