<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class roleAuthorization extends DBConnection {

    /**
     * manageUserRoles constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Insert data to company users table
     * @return array|void
     * @throws Exception
     */
    public function insert(){
        try {
            $data = $_POST['authorization_arr'];
            $role_id = $_POST['role_id'];

            /*Will contain SQL snippets.*/
            $rowsSQL = array();

            /*Will contain the values that we need to bind.*/
            $toBind = array();


            foreach($data as $key => $val){
                $data[$key]['created_at'] = date('Y-m-d H:i:s');
                $data[$key]['updated_at'] = date('Y-m-d H:i:s');
            }

            /*Get a list of column names to use in the SQL statement.*/
            $columnNames = array_keys($data[0]);

            //Loop through our $data array.
            foreach($data as $arrayIndex => $row){
                $params = array();
                foreach($row as $columnName => $columnValue){
                    $param = ":" . $columnName . $arrayIndex;
                    $params[] = $param;
                    $toBind[$param] = $columnValue;
                }
                $rowsSQL[] = "(" . implode(", ", $params) . ")";
            }
//            dd($columnNames);

            //Construct our SQL statement
            $sql = "INSERT INTO user_role_authorizations (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

            //Prepare our PDO statement.
            $pdoStatement = $this->companyConnection->prepare($sql);

            //Bind our values.
            foreach($toBind as $param => $val){
                $pdoStatement->bindValue($param, $val);
            }

            //Execute our statement (i.e. insert the data).
            $result = $pdoStatement->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $result, 'message' => 'This record saved successfully.');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }

    /**
     * Get all users
     * @return array
     */
    public function getAllUsers() {
        try {
            $role_id = $_POST['role_id'];
            $data = $this->companyConnection->query("SELECT * FROM users WHERE role = ".$role_id." AND status='1' AND deleted_at IS NULL AND id != '1'")->fetchAll();
            foreach ($data as $key => $value) {
                $data[$key]['user_name'] = userName($value['id'], $this->companyConnection);
            }

            $properties = $this->companyConnection->query("SELECT * FROM general_property WHERE status = 1")->fetchAll();
            return array('code' => 200, 'status' => 'success', 'users' => $data, 'properties' => $properties, 'message' => 'All users fetched successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Delete User
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE users SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update User Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];

            $sql = "UPDATE users SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $id]);

            if($status == '1'){
                $status_message='Record activated successfully.';
            }
            if($status == '0'){
                $status_message='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $status_message);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Get user by id
     * @return array
     */
    public function getUserById() {
        try {
            $id = $_POST['id'];
            $data = getDataById($this->companyConnection, 'users', $id);
            if(!empty($data['data']) && count($data['data']) > 0){
                $custom_data = !empty($data['data']['custom_fields']) ? unserialize($data['data']['custom_fields']) : null;
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['custom_data'] = $custom_data;
                return $data;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


}



$roleAuthorization = new roleAuthorization();
