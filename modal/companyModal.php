<style>
    li{
        margin: 5px 0;
        font-weight:bold;
    }
    .right-detail{
        position:relative;
        left:+400px;
    }
    /*#companyAnnouncementAttachment { display: block !important;}*/
    #companyAnnouncement .modal-header {
        padding: 12px 12px;
    }
    #companyAnnouncement .modal-body {
        padding-bottom: 20px !important;
    }
    #companyAnnouncement .modal-body h3 {
        margin-top: 20px;
        margin-bottom: 20px;
        font-weight: 700;
    }
    #companyAnnouncement .modal-body label, #companyAnnouncement .modal-body p {
        margin: 0;
    }
    #companyAnnouncement .form-outer {
        margin-bottom: 0px;
    }
    #companyAnnouncement .modal-footer ,#companyAnnouncementAttachment .modal-footer {
        background: #585858;
    }
    #companyAnnouncement .modal-body h5 {
        margin-bottom: 30px;
        font-weight: 700;
        color: #585858;
    }
    #companyAnnouncementAttachment #companyAnnouncementTitle {
        text-transform: uppercase;
        margin: 10px 0px 15px 0px;
        font-weight: 700;
        color: #585858;
        font-size: 14px;
    }
    #companyAnnouncementAttachment .view_ann_attachment_list a {
        margin-bottom: 5px;
    }
    #companyAnnouncementAttachment .modal-body .show-attachment {
        text-decoration: underline;
        color: blue;
    }
    #companyAnnouncement .modal-body .view-attachment {
        color: #ff3c3c;
        margin-bottom: 15px;
        text-decoration: underline;
        font-weight: 600;
    }

</style>
<div class="modal fade" id="SystemMaintenance" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <input type="hidden" id="announcementSys_id" value="">

                <div class="col-sm-12">
                    <div class="col-sm-12 maintenance-logo">
                        <div id="underLogo">
                            <img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo.png" alt="logo" style="height: auto;">
                        </div>
                    </div>
                    <div class="col-sm-12 logo-txt">
                        <h1>SYSTEM </h1>
                        <h1>MAINTAINANCE SCHEDULED</h1>
                    </div>
                </div>
            </div>
            <div class="modal-body">

                <p>
                    Dear Valued Client,
                </p>
                <p>
                    In our ongoing effort to continually improve the Apexlink experience, our Engineering
                    Department has scheduled a maintenance window for <span id="SysstartDate">
                        Tuesday July 12th 2016
                    </span> from <span id="SysstartTime">3:00am</span>
                    to <span id="SysendTime">6:00am</span> Central Standard Time to perform
                    updates/server upgrades.
                </p>
                <p>
                    While the maintenance window is set for 2 hours, the expected downtime is approximately
                    30-45 minutes. You should prepare for intermittent service disruptions during this
                    maintenance window.
                </p>
                <p>
                    As always, use the Request Support link or call the helpdesk at 772-212-1950 for
                    any questions you may have. This notice can be closed by clicking the close icon
                    in the upper right corner.
                </p>
                <h4>
                    Thank You<br />
                    Apexlink Team
                </h4>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="announcement" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="font-size: 12px;">
            <div class="modal-header">
                <a class="close closeAnnouncementmodal closeAnnouncement pop-close closeOtherAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <input type="hidden" id="announcement_id" value="">
                <h4 class="modal-title" style="font-weight: bold;color: #1c6fcc;">Announcement</h4>
            </div>
            <div class="modal-body" style="padding: 0px 15px;">
                <h3><span id="announcementTitle">INDEPENDENCE DAY</span></h3>
                <div class="row">
                    <div class="col-md-4">
                        <label>Start Date</label>
                        <p><span id="announcementStartDate">July 10, 2018 (Tue.)</span></p>
                    </div>
                    <div class="col-md-8">
                        <label>End Date</label>
                        <p><span id="announcementEndDate">July 10, 2018 (Tue.)</span></p>
                    </div>

                    <div class="col-md-4">
                        <label>Start Time</label>
                        <p><span id="announcementStartTime">3:00 pm</span></p>
                    </div>
                    <div class="col-md-8">
                        <label>End Time</label>
                        <p><span id="announcementEndTime">3:30 pm</span></p>
                    </div>

                </div>
                <h5><span id="announcementDescription">Happy Independence day to all...</span></h5>
            </div>
            <div class="modal-footer">
                <h5 style="text-align: center; font-weight: bold; color: #fff;">Please click the close button(X) on the top right to continue!</h5>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="companyAnnouncement" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="font-size: 12px;">
            <div class="modal-header">
                <button type="button" class="close closeCompanyAnnouncement" data-dismiss="modal">×</button>
                <input type="hidden" id="company_announcement_id" value="">
                <h4 class="modal-title" style="font-weight: bold;color: #2d3091;">Announcement</h4>
            </div>
            <div class="modal-body" style="padding: 0px 15px;">
                <h3><span id="companyAnnouncementTitle">INDEPENDENCE DAY</span></h3>
                <div class="form-outer">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Start Date</label>
                            <p><span id="companyAnnouncementStartDate">July 10, 2018 (Tue.)</span></p>
                        </div>
                        <div class="col-md-8">
                            <label>End Date</label>
                            <p><span id="companyAnnouncementEndDate">July 10, 2018 (Tue.)</span></p>
                        </div>
                    </div>
                </div>

                <div class="row mb-15">
                    <div class="col-md-4">
                        <label>Start Time</label>
                        <p><span id="companyAnnouncementStartTime">3:00 pm</span></p>
                    </div>
                    <div class="col-md-8">
                        <label>End Time</label>
                        <p><span id="companyAnnouncementEndTime">3:30 pm</span></p>
                    </div>

                </div>
                <h5>
                    <span id="companyAnnouncementDescription">Happy Independence day to all...</span>
                </h5>
                <div class="view_ann_attachment" style="display: none;">
                    <a class="view-attachment" href="#companyAnnouncementAttachment" data-toggle="modal" data-dismiss="modal"> Click Here to View Images</a>
                </div>
            </div>
            <div class="modal-footer">
                <h5 style="text-align: center; font-weight: bold; color: #fff;">Please click the close button(X) on the top right to continue!</h5>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="companyAnnouncementAttachment" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="font-size: 12px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <input type="hidden" id="company_announcement_id" value="">
                <h4 class="modal-title" style="font-weight: bold;color: #2d3091;">Announcement</h4>
            </div>
            <div class="modal-body" style="padding: 0px 15px;">
                <div id="companyAnnouncementTitle">Click On the link to see Attachments</div>
                <div class="view_ann_attachment_list mb-15">
                    <a class="show-attachment" href="javascript:;" target="_blank"> Click Here to View Images</a><br>
                    <a class="show-attachment" href="javascript:;"> Click Here to View Images</a><br>
                    <a class="show-attachment" href="javascript:;"> Click Here to View Images</a><br>
                </div>
            </div>
            <div class="modal-footer">
                <h5 style="text-align: center; font-weight: bold; color: #fff;">Please click the close button(X) on the top right to continue!</h5>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="PrintEnvelope" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close  " data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <input type="button" class="blue-btn pull-left" id="print_building_enevelope" onclick="PrintElem('#print_envelope_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body" id="print_envelope_content">
                <div>
                    <div class="left-detail">
                        <ul>
                            <li class="padding-top bold mt-n1" id="company_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="address2">

                            </li>
                            <li class="padding-top bold mt-n1" id="address3">

                            </li >
                            <li class="padding-top bold mt-n1" id="address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span>, <span id="state" class="state" ></span> <span id="postal_code" class="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                    <div class="right-detail ">
                        <ul>
                            <li class="padding-top bold mt-n1" id="building_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="building_address">

                            </li>

                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span>, <span class="state" id="state" ></span> <span class="postal_code" id="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
