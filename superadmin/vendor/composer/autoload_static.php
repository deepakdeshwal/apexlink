<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitba3e94edc7a3d44bf6dfa3ccc2b411fb
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitba3e94edc7a3d44bf6dfa3ccc2b411fb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitba3e94edc7a3d44bf6dfa3ccc2b411fb::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
