<?php
try{
    $data = array(
        array('logo' => '', 'template' => '<section class="flyer-template-content">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr property-hdr-black">
                    <div class="row">
                      <div class="col-sm-6">Property Name</div>
                      <div class="col-sm-6 text-right">{{propertyPrice}}</div>
                    </div>
                  </div>
                  <div class="property-list-content">
                    <div class="row">
                      <div class="col-sm-4">
                            <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                            </div>
                            <div class="col-sm-12">                          
                                <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                                </div>
                            </div>
                            </div>
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-sm-12 pr-none grid-design">
                            <div class="property-name">
                              <label>Property Name - </label>
                              <span>{{propertyName}}</span>
                            </div>
                            <div class="property-name">
                              <label>Year Built - </label>
                              <span>{{propertyYear}}</span>
                            </div>
                            <div class="property-name">
                              <label>Listing Type - </label>
                              <span>{{listingType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Type -</label>
                              <span>{{propertyType}}</span>
                            </div>
                             <div class="property-name">
                              <label>Posting Title - </label>
                              <span>{{postingTitle}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. of Buildings - </label>
                              <span>{{numberOfBuilding}}</span>
                            </div>
                             <div class="property-name">
                              <label>Address - </label>
                              <span>{{propertyAddress}}</span>
                            </div>
                            <div class="property-name">
                              <label>Unit Types - </label>
                              <span>{{unitType}}</span>
                              <label class="clear">Property Style - </label>
                              <span>{{propertyStyle}}</span>
                            </div>
                             <div class="property-name">
                              <label>No. of Units - </label>
                              <span>{{numberOfUnits}}</span>
                            </div>
                             <div class="property-name">
                              <label>Vacant Units - </label>
                              <span>{{vacantUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Pets Allowed - </label>
                              <span>{{petAllowed}}</span>
                            </div>
                             <div class="property-name">
                              <label>Parking - </label>
                              <span>{{parking}}</span>
                            </div>
                          </div>
                                               
                        </div>
                         
                      </div> 
  
                    </div>
                    {{applyOnline}}
                    {{map}}
                    
                    <div class="multi-color-design">
                    <div class="property-listing multi">
                      <h4>Units</h4>
                      <ul>
                      {{units}}
                      </ul>
                    </div>
                    <div class="property-listing multi">
                      <h4>Description</h4>
                      <p class="lh25">{{description}}</p>
                    </div>
                   
                    <div class="property-listing multi">
                      <h4>Features &amp; Amenities</h4>
                      <ul>
                        {{amenities}}
                       </ul>
                    </div>
                    
                    <div class="listing-adress multi">
                      <h4>Listing Agents</h4>
                      <ul>
                        <li>
                          <p><b>{{name}}</b></p>
                          <p>Ph No. {{phoneNumber}}</p>
                          <p><a href="javascript:;">{{email}}</a></p>
                        </li>                       
                      </ul>
                      
                    </div>
                  </div>

                  </div>
           </div>
    </section>',
            'status' => '1','Template1','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        array('logo' => '', 'template' => '<section class="flyer-template-content">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr">
                    <div class="row">
                      <div class="col-sm-6">Property List</div>
                      <div class="col-sm-6 text-right">{{propertyPrice}}</div>
                    </div>
                  </div>
                  <div class="property-list-content">
                    <div class="row">
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-sm-6 separator-rt">
                            <div class="property-name">
                              <label>Property Name -</label>
                              <span>{{propertyName}}</span>
                            </div>
                            <div class="property-name">
                              <label>Listing Type -</label>
                              <span>{{listingType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Type -</label>
                              <span>{{propertyType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Unit Types -</label>
                              <span>{{unitType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Pets Allowed -</label>
                              <span>{{petAllowed}}</span>
                            </div>
                            <div class="property-name">
                              <label>Vacant Units -</label>
                              <span>{{vacantUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Address -</label>
                              <span>{{propertyAddress}}</span>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="property-name">
                              <label>Year Built -</label>
                              <span>{{propertyYear}}</span>
                            </div>
                            <div class="property-name">
                              <label>Posting Title -</label>
                              <span>{{postingTitle}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. of buildings -</label>
                              <span>{{numberOfBuilding}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. Of Units -</label>
                              <span>{{numberOfUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Style -</label>
                              <span>{{propertyStyle}}</span>
                            </div>
                            <div class="property-name">
                              <label>Parking -</label>
                              <span>{{parking}}</span>
                            </div>
                          </div>                         
                        </div>
                         
                      </div> 
                      <div class="col-sm-4">
                      <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                            </div>
                      </div>
                      <div class="col-sm-12">  
                        <div class="flyer-divider"></div>
                      </div>
  
                    </div>
                    <div class="property-listing">
                      <h4>Description</h4>
                      <p class="lh25">{{description}}</p>
                    </div>
                    <div class="unit-features">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="property-listing">
                            <h4>Units</h4>
                            <ul>
                             {{units}}
                            </ul>
                          </div>
                        </div>
                        <div class="col-sm-6 separator-lt">
                          <div class="property-listing">
                            <h4>Features & Amenities</h4>
                            <ul>
                             {{amenities}}
                            </ul>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="photo-gallery">
                      <h4>Photo Gallery</h4>
                      <div class="col-sm-12">                          
                                <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                                </div>
                            </div>
                            
                            
                     </div>
                         <div class="applyformAndMap">
                                {{applyOnline}}
                                 {{map}}   
                           </div>
                     <div class="flyer-divider"></div>
                    
                    <div class="listing-adress">
                      <h4>Listing Agents</h4>
                      <ul>
                        <li>
                          <p><b>{{name}}</b></p>
                          <p>Ph No. {{phoneNumber}}</p>
                          <p><a href="javascript:;">{{email}}</a></p>
                        </li>
                       
                       
                      </ul>
                      
                    </div>

                  </div>
           </div>
    </section>', 'status' => '0','Template2','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        array('logo' => '', 'template' => '<section class="flyer-template-content">
           <div class="container">
            <div class="property-list">
                 
                  <div class="property-list-content bg-transparent temp3">
                    <div class="row">
                      <div class="col-sm-4">
                        <div>
                           <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                            </div>
                            <div class="col-sm-12">                          
                                <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                                </div>
                            </div>
                        </div>
                         <div class="agents-white-box">
                          <div class="listing-adress">
                            <h4>Listing Agents</h4>
                            <ul>
                              <li>
                                <p><b>{{name}}</b></p>
                                <p>Ph No. {{phoneNumber}}</p>
                                <p><a href="javascript:;">{{email}}</a></p>
                              </li>                            
                             
                            </ul>
                            
                          </div>
                          
                           <div class="applyformAndMap">
                                {{applyOnline}}
                                 {{map}}   
                           </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                         <div class="property-hdr property-hdr-white">
                            <div class="row">
                              <div class="col-xs-6 col-sm-6">{{propertyName}}</div>
                              <div class="col-xs-6 col-sm-6 text-right">{{propertyPrice}}</div>
                            </div>
                          </div>
                        
                          <div class="property-dec-blue-box">
                            <div class="row">
                              <div class="col-sm-6 separator-rt">
                                <div class="property-name">
                                  <label>Year Built - </label>
                                  <span>{{propertyYear}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Listing Type - </label>
                                  <span>{{listingType}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Posting Title - </label>
                                  <span>{{postingTitle}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Property Type - </label>
                                  <span>{{propertyType}}</span>
                                </div>
                                <div class="property-name">
                                  <label>No. of buildings - </label>
                                  <span>{{numberOfBuilding}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Unit Types - </label>
                                  <span>{{unitType}}</span>
                                </div>
                                <div class="property-name">
                                  <label>No. Of Units - </label>
                                  <span>{{numberOfUnits}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Pets Allowed - </label>
                                  <span>{{petAllowed}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Property Style - </label>
                                  <span>{{propertyStyle}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Vacant Units - </label>
                                  <span>{{vacantUnits}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Parking - </label>
                                  <span>{{parking}}</span>
                                </div>
                                <div class="property-name">
                                  <label>Address - </label>
                                  <span>{{propertyAddress}}</span>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <p>
                                 {{description}}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="property-listing">
                            <h4>Units</h4>
                            <ul>
                             {{units}}
                            </ul>
                          </div>
                            <div class="flyer-divider"></div>
                          <div class="property-listing">
                            <h4>Features &amp; Amenities</h4>
                            <ul>
                              {{amenities}}
                            </ul>
                          </div>
                            <div class="flyer-divider"></div>
                         
                      </div> 
  
                    </div>
                   

                  </div>
           </div>
    </section>', 'status' => '0','Template3','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        array('logo' => '', 'template' => '<section class="flyer-template-content temp4">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr property-hdr-transparent">
                    <div class="row">
                      <div class="col-xs-6 col-sm-6">{{propertyName}}</div>
                      <div class="col-xs-6  col-sm-6 text-right">{{propertyPrice}}</div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="flyer-divider"></div>
                  </div>
                  <div class="property-list-content bg-transparent">
                    <div class="row">
                      
                      <div class="col-sm-4">
                        <div>
                        <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                            </div>
                        </div>
                        </div>
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-xs-6 wd100">
                            <div class="property-name">
                              <label>Year Built -</label>
                              <span>{{propertyYear}}</span>
                            </div>
                            <div class="property-name">
                              <label>Posting Title -</label>
                              <span>{{postingTitle}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. of buildings -</label>
                              <span>{{numberOfBuilding}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. Of Units -</label>
                              <span>{{numberOfUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Style -</label>
                              <span>{{propertyStyle}}</span>
                            </div>
                            <div class="property-name">
                              <label>Parking -</label>
                              <span>{{parking}}</span>
                            </div>
                          </div> 
                          <div class="col-xs-6 wd100">
                            <div class="property-name">
                              <label>Property Name -</label>
                              <span>{{propertyName}}</span>
                            </div>
                            <div class="property-name">
                              <label>Listing Type -</label>
                              <span>{{listingType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Type -</label>
                              <span>{{propertyType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Unit Types -</label>
                              <span>{{unitType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Pets Allowed -</label>
                              <span>{{petAllowed}}</span>
                            </div>
                            <div class="property-name">
                              <label>Vacant Units -</label>
                              <span>{{vacantUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Address -</label>
                              <span>{{propertyAddress}}</span>
                            </div>

                          </div>
                          <div class="col-sm-12">
                            <div class="flyer-divider"></div>
                          </div>
                          <div class="col-sm-12">
                            <div class="property-listing ">
                              <h4 class="text-black">Units</h4>
                              <ul>
                             {{units}}
                              </ul>
                            </div>
                          </div>

                                                  
                        </div>
                         
                      </div> 

  
                    </div>
                    <div class="flyer-divider"></div>

                     <div class="row">
                        <div class="col-sm-9">
                          <div class="photo-gallery">
                            <h4>Photo Gallery</h4>
                             <div class="col-sm-12">
                                  <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                                </div>
                            </div>
                            
                             <div class="applyformAndMap">
                                {{applyOnline}}
                                 {{map}}   
                           </div>
                          </div>
                          <div class="property-listing">
                            <h4>Description</h4>
                            <p class="lh25">{{description}}</p>
                          </div>
                          <div class="property-listing">
                            <h4>Features & Amenities</h4>
                            <ul>
                       {{amenities}}
                            </ul>
                          </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="listing-adress listing-adress2">
                              <h4>Listing Agents</h4>
                              <ul>
                                <li>
                                  <p><b>{{name}}</b></p>
                                  <p>Ph No. {{phoneNumber}}</p>
                                  <p><a href="javascript:;">{{email}}</a></p>
                                </li>                            
                               
                              </ul>
                            </div>
                        </div>
                      </div>
                      
                
           </div>
    </section>', 'status' => '0','Template4','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        array('logo' => '', 'template' => '<section class="flyer-template-content temp5">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr property-hdr-transparent">
                    <div class="row">
                      <div class="col-sm-6">{{propertyName}}</div>
                      <div class="col-sm-6 text-right">{{propertyPrice}}</div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="flyer-divider"></div>
                  </div>
                  <div class="property-list-content bg-transparent">
                    <div class="row">
                      
                      <div class="col-sm-4">
                        <div>
                        <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                         </div>
                        </div>
                        <div class="property-price">
                          <h4>{{propertyPrice}}</h4>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="property-name">
                              <label>Year Built -</label>
                              <span>{{propertyYear}}</span>
                            </div>
                            <div class="property-name">
                              <label>Posting Title -</label>
                              <span>{{postingTitle}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. of buildings -</label>
                              <span>{{numberOfBuilding}}</span>
                            </div>
                            <div class="property-name">
                              <label>No. Of Units -</label>
                              <span>{{numberOfUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Style -</label>
                              <span>{{propertyStyle}}</span>
                            </div>
                            <div class="property-name">
                              <label>Parking -</label>
                              <span>{{parking}}</span>
                            </div>
                          </div> 
                          <div class="col-sm-6">
                            <div class="property-name">
                              <label>Property Name -</label>
                              <span>{{propertyName}}</span>
                            </div>
                            <div class="property-name">
                              <label>Listing Type -</label>
                              <span>{{listingType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Property Type -</label>
                              <span>{{propertyType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Unit Types -</label>
                              <span>{{unitType}}</span>
                            </div>
                            <div class="property-name">
                              <label>Pets Allowed -</label>
                              <span>{{petAllowed}}</span>
                            </div>
                            <div class="property-name">
                              <label>Vacant Units -</label>
                              <span>{{vacantUnits}}</span>
                            </div>
                            <div class="property-name">
                              <label>Address -</label>
                              <span>{{propertyAddress}}</span>
                            </div>

                          </div>
                          <div class="col-sm-12">
                            <p class="lh25">{{description}}</p>
                          </div>

                                                  
                        </div>
                         
                      </div> 
                  
  
                    </div>

                    <p>&nbsp;</p>
                      <div class="photo-gallery">
                        <h4>Photo Gallery</h4>
                        <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                         </div>
                         
                         
                          <div class="applyformAndMap">
                                {{applyOnline}}
                                 {{map}}   
                           </div>
                       </div>
                       <div class="row">
                        <div class="col-sm-5">
                          <div class="agents-white-box">
                          <div class="listing-adress">
                            <h4>Listing Agents</h4>
                            <ul>
                              <li>
                                <p><b>{{name}}</b></p>
                                <p>Ph No. {{phoneNumber}}</p>
                                <p><a href="javascript:;">{{email}}</a></p>
                              </li>
                            
                             
                            </ul>
                            
                          </div>
                        </div>
                        </div>
                        <div class="col-sm-7">
                          


                          <div class="property-listing mt20">
                            <h4>Units</h4>
                            <ul>
                               {{units}}
                            </ul>
                          </div>
                          <div class="flyer-divider"></div>
                          <div class="property-listing">
                            <h4>Features & Amenities</h4>
                            <ul>
                            {{amenities}}
                            </ul>
                          </div>
                        
                        </div>
                       

                  
                    
                   

                  </div>
           </div>
    </section>', 'status' => '0','Template5','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        array('logo' => '', 'template' => '<section class="flyer-template-content">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr">
                    <div class="row">
                      <div class="col-sm-6">Property List</div>
                      <div class="col-sm-6"></div>
                    </div>
                  </div>
                  <div class="property-list-content">
                    <div class="row">
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-sm-6">
                            <h4 class="lh30">{{bedroom}}-{{bathroom}}</h4>
                            <h4 class="lh30">Sq Footage : {{sgfoot}}</h4>
                            <h4 class="lh30">Year Built : {{propertyYear}}</h4>
                          </div>
                          <div class="col-sm-6 text-right">
                            <h3 class="lh30">{{propertyPrice}}</h3>
                          
                          </div>
                          <div class="col-sm-12">  
                            <div class="flyer-divider"></div>
                          </div>
                         
                        </div>
                         <p class="lh25">{{description}}</p>
                      </div> 
                      <div class="col-sm-4">
                        <div>
                              <div class="col-sm-12 activeImageDivClass">
                                <img id="activeImageID" src="{{activeImage}}">
                            </div>
                            <div class="col-sm-12">                          
                                <div class="owl-carousel owl-theme">
                                    {{propertyImages}}
                                </div>
                            </div>
                        </div>
                        <h5 class="property-img-tagline">Call Janell today fro your personal tour 1-800-881-5139</h5>
                       <div class="applyformAndMap">
                        {{applyOnline}}
                        {{map}}
                        </div>
                      </div>
                      
                      <div class="col-sm-12">  
                        <div class="flyer-divider"></div>
                      </div>
  
                    </div>
                    <div class="property-listing">
                      <h4>Units</h4>
                      <ul>
                       {{units}}
                      </ul>
                    </div>
                    <div class="flyer-divider"></div>
                    <div class="property-listing">
                      <h4>Features & Amenities</h4>
                      <ul>
                       {{amenities}}
                      </ul>
                    </div>
                    <div class="flyer-divider"></div>
                    <div class="listing-adress">
                      <h4>Listing Agents</h4>
                      <ul>
                        <li>
                          <p><b>John Smith</b></p>
                          <p>Ph No. 888-907-8686</p>
                          <p><a href="javascript:;">jsmith@gmail.com</a></p>
                        </li>
                      </ul>
                      
                    </div>

                  </div>
           </div>
    </section>', 'status' => '0','Template6','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))


    );
    $stm = $connection->prepare("TRUNCATE TABLE flyer_posts");

    $stm->execute();

    foreach($data as $rows)
    {
        $query = "INSERT INTO `flyer_posts`( `logo`,`template`,`status`,`created_at`,`updated_at`) VALUES
        ('{$rows['logo']}','{$rows['template']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";
        $stm = $connection->prepare($query);
        $stm->execute();
    }
    return array('status' => 'success', 'code'=> 200);
    print_r("sfa");
}
catch (Exception $exception)
{
    print_r($exception);
}
?>