<?php
/**
 * Created by PhpStorm.
 * User: ChughRaghav
 * Date: 5/20/2019
 * Time: 6:26 PM
 */

use PHPUnit\Framework\TestCase;

class AnnouncementAjaxTest extends TestCase
{

    public function testAddAnnouncement()
    {
        try {
            $connection = connection();
            $postData = [
                'unit_type' => 'new Data',
                'description' => 'new Description',
                'is_default' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => '1',
                'is_editable' => '1',
                'user_id' => '3'
            ];

            //insert data
            $data = $postData;
            //Save Data in Company Database
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO company_unit_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $connection->prepare($query);
            $stmt->execute($data);

            if ($stmt->execute($data)) {
                $result = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');
            } else {
                $result = array('code' => 400, 'status' => 'error', 'data' => $data,'message' => 'Error occured.');
            }
            $this->assertEquals(200, $result['code']);
        }catch (PDOException $e) {
            $result = array('code' => 400, 'status' => 'error', 'data' => $data,'message' => 'Error occured.');
            $this->assertEquals(200, $result['code']);
        }
    }

    public function testEditAnnouncement()
    {

    }

    public function testDeleteAnnouncement()
    {

    }




}
