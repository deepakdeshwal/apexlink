<?php
/**
 * Created by PhpStorm.
 * User: DeviSarita
 * Date: 1/23/2019
 * Time: 4:59 PM
 */
//include_once('../config.php');
include_once('constants.php');

include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    //$url = BASE_URL."login";
    header('Location: '.BASE_URL);
};

class ManageSupportTeamAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
//         print_r($action);die;
        echo json_encode($this->$action());
    }

    /**
     * Function for add new support team member
     * @return array
     */
    public function insert(){
        try {

//          return $this->testQuery();
//            print_r($_POST);die;
            $data = $_POST['form'];
            $data = postArray($data);
//            print_r($data);die;
            //Server side validation
            //Required variable array
            $required_array = ['first_name','email'];
            /* Max length variable array */
            $maxlength_array = [
                'middle_name'=>1,
                'last_name'=>20,
                'work_phone'=>12,
                'mobile_number'=>12,
                'fax'=>12];

            //Number variable array
            $number_array = ['phone_number'];

            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                unset($data['edit_support_id']);
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
//                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $sqlData = createSqlColVal($data);


                /*Check email already exists or not*/
                $sql = "SELECT email FROM support_team WHERE email = '" .$data['email'] ."'";
                $check_email= $this->conn->query($sql)->fetch();

                if ($check_email >  0) {
                    return array('code' => 500, 'status' => 'warning', 'data' => $check_email, 'message' => 'Member already exists.');
                }
                else {
                    $query = "INSERT INTO support_team (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
//                    print_r($query);die;
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record saved successfully.');
                }
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for view Support Team Member
     * @return array
     */
    public function view(){
        try {
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM support_team where  id='$id'");
            $supportTeam= $query->fetch();
            if($supportTeam){
                return array('status' => 'success', 'data' => $supportTeam);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update Support Team Member
     * @return array
     */
    public function update(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $edit_support_id = $data['edit_support_id'];

            //Required variable array
            $required_array = ['first_name','email'];

            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];

            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($edit_support_id) && !empty($edit_support_id)){
                    unset($data['edit_support_id']);
                }
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE support_team SET ".$sqlData['columnsValuesPair']." where id='$edit_support_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
//                print_r($stmt);die('asasa');
                $_SESSION[SESSION_DOMAIN]["message"]='This record updated successfully';
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record updated successfully.');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Delete support Team Member
     * @return array
     */
    public function delete(){
        try {
            $id = $_POST['id'];

            if($id){
                $data = date('Y-m-d H:i:s');
                $sql = "UPDATE support_team SET deleted_at=? WHERE id=?";
                $query = $this->conn->prepare($sql);
                $response = $query->execute([$data, $id]);

                if($response){
                    return array('status' => 'success', 'code'=> 200);
                }else{
                    return array('status' => 'error', 'code'=> 400, 'message' => 'No Records Found');
                }
            } else {
                return array('status' => 'error', 'code'=> 400, 'message' => 'No Records Found');
            }

        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$user = new ManageSupportTeamAjax();