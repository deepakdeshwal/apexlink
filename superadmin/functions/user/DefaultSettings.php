<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/superadmin/functions/FlashMessage.php");
include_once( ROOT_URL."/superadmin/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class DefaultSettings extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    public function  view(){
        $datepickerFormat = getDatePickerFormat($_SESSION[SESSION_DOMAIN]['user_id']);
        $redirect = VIEW."settings/apexlink_new/add.php";
        return require $redirect;
    }

}

$user = new DefaultSettings();
