<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/superadmin/helper/helper.php");

class groupMessageAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    //fetch recipients data
    public function getUserList()
    {
        try {
            $user_type = $_POST['id'];
            $company_data = $_POST['company_data'];
            $users = [];
            if(!empty($company_data)){
                foreach ($company_data as $value){
                    $dbDetails = $this->conn->query("SELECT * FROM users where id='$value' and deleted_at is null")->fetch();
                    $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                    $userData = $connection->query("SELECT id,name,email,company_name,phone_number FROM users where user_type='$user_type' and deleted_at is null")->fetchAll();
                    array_push($users,$userData);
                }
            }

            $singleD = array_reduce($users, 'array_merge', array());

            return $singleD;
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    public function delete()
    {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE communication_email SET deleted_at=? WHERE id=?";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    //fetch company records
    public function getCompany(){
        try {

            $connection = "SELECT id,company_name FROM users WHERE user_type='1'";
            $companyData = $this->conn->query($connection)->fetchAll();
            $company_users = '';
            foreach($companyData as $key => $val){
                $company_users .= '<option value="'.$val["id"].'">'.$val["company_name"].'</option>';
            }
            return ['status' => 'success', 'code' => 200,  'data'=>$company_users, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function saveEmail()
    {
        try {
            $response = $_REQUEST;
            $to = [];
            $to_data = isset($_REQUEST['user_data'])?json_decode($_REQUEST['user_data']):NULL;
            $company_name = '';
            $phone_number = '';
            $email_string = '';
            $phone_address = [];
            $email_address = [];
            $email = [];
            if(!empty($to_data)){
                foreach($to_data as $value){
                    if(!empty($value->company_name)) {
                        $company_name .= $value->company_name.', ';
                    }
                    if(!empty($value->phone_number)) {
                        $phone = getPhoneCarrier('+1',$value->phone_number,$this->conn);
                        if($phone['code'] == 200){
                            array_push($phone_address,$phone['address']);
                            $phone_number .= $phone['address'].', ';
                        }
                    }
                    if(!empty($value->email)){
                        array_push($email_address,$value->email);
                        $email_string .= $value->email.', ';
                    }
                }
                $company_name = str_replace_last(',','',$company_name);
                $phone_number = str_replace_last(',','',$phone_number);
                $email_string = str_replace_last(',','',$email_string);
            }

            if(isset($response['subrecepient'][0]) && $response['subrecepient'][0] == 'All')
            {
                unset($response['subrecepient'][0]);
            }
            $user_type =  $response['recepient'];

            if($user_type == 'all')
            {
                $company_data_id = $response['company_name'];
                $userDataInfo = [];
                if(!empty($company_data_id)){
                    foreach ($company_data_id as $value){
                        $dbDetails = $this->conn->query("SELECT * FROM users where id='$value' and deleted_at is null")->fetch();
                        $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                        $userData = $connection->query("SELECT id,name,email,company_name,sms_carrier,country_code,phone_number FROM users where user_type IN ('2','3','4','5','6','7','8')")->fetchAll();
                        array_push($userDataInfo,$userData);
                    }
                }
                $singleD = array_reduce($userDataInfo, 'array_merge', array());
                if(!empty($singleD)){
                    foreach($singleD as $value){
                        if(!empty($value['company_name'])) {
                            $company_name .= $value['company_name'].', ';
                        }
                        if(!empty($value['phone_number'])) {
                            $phone = getPhoneCarrier('+1',$value['phone_number'],$this->conn);
                            if($phone['code'] == 200){
                                array_push($phone_address,$phone['address']);
                                $phone_number .= $phone['address'].', ';
                            }
                        }
                        if(!empty($value['email'])){
                            array_push($email_address,$value['email']);
                            $email_string .= $value['email'].', ';
                        }
                    }
                    $company_name = str_replace_last(',','',$company_name);
                    $phone_number = str_replace_last(',','',$phone_number);
                    $email_string = str_replace_last(',','',$email_string);
                }
                $all_mails = ($response['gtype'] == '2')?implode(',',$phone_address):$email_string;
            } else {
                if($response['gtype'] == '2' && empty($phone_address))
                {
                    return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please number not valid!');
                } elseif($response['gtype'] == '2' && empty($email_string)) {
                    return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please select atleast one email address');
                }
                $all_mails = ($response['gtype'] == '2')?implode(',',$phone_address):$email_string;
            }

            if($response['gtype'] == '2')
            {
                $emails = $phone_address;
                if(!$emails)
                {
                    $counting = count($emails);
                    if($counting <= 0)
                    {
                        return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please number not valid!');
                    }
                }
                $data['gtype'] = 'T';
            } else {
                $emails = $email_address;
                $data['gtype'] = 'E';
            }



            if(!isset($all_mails))
            {
                return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please select atleast one recepient');
            }

            $user_id = $_SESSION[SESSION_DOMAIN]['user_id'];
            $user_data = getDataById($this->conn, 'users', $user_id);

            $data['email_to'] = $all_mails;

            if($response['gtype'] == '1')
            {
                $data['email_subject'] = $response['subject'];
                $data['email_message'] = $response['mesgbody'];
            }else{
                $data['email_subject'] = '-';
                $data['email_message'] = $response['tmesgbody'];
            }

            $data['company_id'] = !empty($response['company_name'])?serialize($response['company_name']):NULL;
            $data['user_id']    = $user_id;
            $data['user_type']  = $user_type;
            $data['company_name'] = $company_name;
            $data['email_from'] = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $edit_id = $response['edit_id'];
            if ($response['mail_type'] == 'send') {
                $request['action'] = 'SendMailPhp';
                $request['to'] = $emails;
                $request['subject'] = (isset($data['email_subject']))?$data['email_subject']:'text message';
                $request['message'] = $data['email_message'];
                $request['portal'] = '1';
                $curl_response = json_decode(curlRequest($request));

                if (isset($curl_response)) {
                    if (isset($curl_response->error)) {
                        $error = $curl_response->error;
                        if (isset($error->to)) {
                            return array('code' => 503, 'status' => 'failed', 'data' => $data, 'message' => 'Please enter valid email.');
                        }
                        $data['status'] = '0';
                    }
                    if ($curl_response->code == '200') {
                        $data['status'] = '1';
                    } else {
                        $data['status'] = '0';
                    }
                } else {
                    $data['status'] = '0';
                }
            }

            $data['type'] = 'G';
            $data['selected_user_type'] = $response['recepient'];

            if ($edit_id) {
                $edit_id = $_REQUEST['edit_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE communication_email SET " . $sqlData['columnsValuesPair'] . " where id='$edit_id'";

                $stmt = $this->conn->prepare($query);
                $stmt->execute();
            } else {
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                $lastInsertId = $this->conn->lastInsertId();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully.');
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function getComposeForView()
    {
        try {
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['created_at'] = dateFormatUser($data['created_at'], null, $this->conn);
            $data['company_id'] = !empty($data['company_id'])?unserialize($data['company_id']):'';
            $data['user_data'] = array_map('trim', explode(',', $data['email_to']));
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function checkUserType()
    {
        try {
//            dd('adasda');
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['created_at'] = dateFormatUser($data['created_at'], null, $this->conn);
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
}
$groupMessageAjax = new groupMessageAjax();
?>