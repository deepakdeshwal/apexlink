<?php
include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/superadmin/helper/helper.php");

class composeMailAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    //start for save email functionality
    public function saveEmail()
    {
        try {

            $response = $_REQUEST;
            $to = [];
            $cc = [];
            $bcc = [];

            if (isset($response['to'])) {
                $to = explode(',', $response['to']);
            }
            if (isset($response['cc'])) {
                $cc = explode(',', $response['cc']);
            }
            if (isset($response['bcc'])) {
                $bcc = explode(',', $response['bcc']);
            }
            $all_mails = array_filter(array_merge($to, $cc, $bcc));

            if (count($all_mails) > 0) {
                foreach ($all_mails as $key => $email) {
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        return array('code' => 503, 'status' => 'failed', 'data' => '', 'message' => 'Please enter valid email.');
                    }
                }
            }
            $user_id = $_SESSION[SESSION_DOMAIN]['user_id'];
            $user_data = getDataById($this->conn, 'users', $user_id);

            $data['email_to'] = $response['to'];
            $data['email_cc'] = $response['cc'];
            $data['email_bcc'] = $response['bcc'];
            $data['email_subject'] = $response['subject'];
            $data['email_message'] = $response['mesgbody'];
            $data['status'] = '2';   // Drafted mail

            $tempArray  = [];
            $postArray = [];
            $i = 0;
            if (!empty($_FILES['file_library']['name'][0])) {
                foreach ($_FILES['file_library']['name'] as $key=>$file) {
                    $t = time();
                    $file_name = $_FILES['file_library']['name'][$i];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $file_name_ex = explode(".", $file_name);
                    $removeSpace = str_replace(" ","-",$file_name_ex[0]);
                    $new_file_name = $removeSpace . '_' . $t . '.' . $file_name_ex[1];
                    move_uploaded_file($_FILES['file_library']['tmp_name'][$i], $_SERVER['DOCUMENT_ROOT'] . "/uploads/email/sentemails/$new_file_name");
                    $url = SITE_URL . "/uploads/email/sentemails/$new_file_name";
                    $tempArray['url'] = $url;
                    $tempArray['name'] = $new_file_name;
                    $tempArray['ext'] = $file_name_ex[1];
                    $tempArray['size'] = $_FILES['file_library']['size'][$key];
                    $postArray[] = $tempArray;
                    $i++;
                }
            }

            $data['user_id'] = $user_id;
            $data['user_type'] = $user_data['data']['user_type'];
            $data['email_from'] = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['type'] = 'E';
            $to = explode(',',$data['email_to']);

            if(isset($response['edit_id'])){
                $edit_id = $response['edit_id'];
            }

            if ($response['mail_type'] == 'send') {
                $request['action'] = 'SendMailPhp';
                $request['to'] = $to;
                if (isset($cc[0]) && !empty($cc[0])) {
                    $request['cc'] = $cc;
                }
                if (isset($bcc[0]) && !empty($bcc[0])) {
                    $request['bcc'] = $bcc;
                }
                $request['subject'] = $data['email_subject'];
                $request['message'] = $data['email_message'];
                $request['portal'] = '1';

                $curl_response = json_decode(curlRequest($request));
                if (isset($curl_response)) {
                    if (isset($curl_response->error)) {
//                        dd($curl_response);
                        $error = $curl_response->error;
                        if (isset($error->to)) {
                            return array('code' => 503, 'status' => 'failed', 'data' => $data, 'message' => 'Please enter valid email.');
                        }
                        $data['status'] = '0';
                    }
                    if ($curl_response->code == 200) {
                        $data['status'] = '1';
                    } else {
                        $data['status'] = '0';
                    }
                } else {
                    $data['status'] = '0';
                }
            }

            if (isset($edit_id) && $edit_id != "") {
                $edit_id = $_REQUEST['edit_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                foreach ($to as $key => $mail) {
                    $data['email_to'] = $mail;
                    if ($key == '0') {
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE communication_email SET " . $sqlData['columnsValuesPair'] . " where id='$edit_id'";

                        $stmt = $this->conn->prepare($query);
                        $stmt->execute();
                    } else {
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->conn->prepare($query);
                        $stmt->execute($data);
                    }

                }

            } else {

                foreach ($to as $key => $mail) {
                    $data['email_to'] = $mail;
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute($data);
                }

                $lastInsertId = $this->conn->lastInsertId();
                $this->insertFileLibrary($postArray, $lastInsertId);
            }
            echo json_encode( array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully.'));
            die();
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
    //end for save email functionality

    //File upload Functionality
    public function insertFileLibrary($postArray, $last_id){
        if (!empty($postArray)){
            foreach ($postArray as $val){
                $data1['email_id'] = $last_id;
                $data1['file_name'] = $val['name'];
                $data1['file_url'] = $val['url'];
                $data1['file_extension'] = $val['ext'];
                $data1['file_size'] = $this->isa_convert_bytes_to_specified($val['size'], 'K').'kb';
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO communication_email_attachments (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data1);
            }
            return array('status' => 'success', 'message' => 'Files added successfully.', 'code' => 201);
        }
        return array('status' => 'error', 'message' => '', 'code' => 500);
    }

    //File upload Functionality

    //File upload Delete Functionality
    public function deleteFile(){
        try {
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM communication_email_attachments WHERE id=" . $id);
            $data = $query->fetch();
            $path = $_SERVER['DOCUMENT_ROOT'].'/uploads/email/sentemails/'.$data['file_name'];
            unlink($path);
            $query = "DELETE FROM communication_email_attachments WHERE id=$id";
            $stmt = $this->conn->prepare($query);
            if($stmt->execute()){
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents deleted successfully.');
            } else {
                return array('code' => 500, 'status' => 'error', 'message' => 'Error occured while deleting document.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     * Convert bytes to the unit specified by the $to parameter.
     *
     * @param integer $bytes The filesize in Bytes.
     * @param string $to The unit type to convert to. Accepts K, M, or G for Kilobytes, Megabytes, or Gigabytes, respectively.
     * @param integer $decimal_places The number of decimal places to return.
     *
     * @return integer Returns only the number of units, not the type letter. Returns 0 if the $to unit type is out of scope.
     *
     */
    function isa_convert_bytes_to_specified($bytes, $to, $decimal_places = 1) {
        $formulas = array(
            'K' => number_format($bytes / 1024, $decimal_places),
            'M' => number_format($bytes / 1048576, $decimal_places),
            'G' => number_format($bytes / 1073741824, $decimal_places)
        );
        return isset($formulas[$to]) ? $formulas[$to] : 0;
    }


    //File upload Delete Functionality

    // start Draft Mail Delete functionality
    public function delete()
    {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE communication_email SET deleted_at=? WHERE id=?";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    // End Draft Mail Delete functionality

    public function getComposeForView(){
        try {
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $query2 = $this->conn->query("SELECT * FROM communication_email_attachments WHERE email_id=" . $id . " AND deleted_at IS NULL");
            $data2 = $query2->fetchAll();
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'data2'=>$data2, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
        }
    }

    public function getRecievedEmail()
    {
        try {

            $user_email = $_SESSION[SESSION_DOMAIN]['email'];
            $user_id = $_SESSION[SESSION_DOMAIN]['user_id'];

            $user_type = $this->companyConnection->query("SELECT user_type from users WHERE id='$user_id'");
            $user_type = $user_type->fetch();

            $userType = $user_type['user_type'];
            if ($_SESSION[SESSION_DOMAIN]['current_user_type'] == 'Communication_Portal') {

                $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email JOIN users On users.email = communication_email.email_from WHERE communication_email.deleted_at IS NULL and communication_email.status='1' and (communication_email.email_parent_id IS NULL OR communication_email.email_parent_id = '') and type='E' ORDER BY communication_email.created_at desc");
            } else {

                $portal_type = $_SESSION[SESSION_DOMAIN]['current_user_type'];

                $portal = $_SESSION[SESSION_DOMAIN][$portal_type];

                $email = $portal['email'];
                if ($portal_type == 'Owner_Portal') {
                    $user_type = '4';
                } elseif ($portal_type == 'Vendor_Portal') {
                    $user_type = '';
                } elseif ($portal_type == 'Tenant_Portal') {
                    $user_type = '';
                }

                $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email JOIN users On users.email = communication_email.email_to WHERE email_to='$email' AND users.user_type='$user_type' AND communication_email.deleted_at IS NULL ORDER BY communication_email.created_at");
            }

            $data = $query->fetchAll();

            if (isset($data)) {
                foreach ($data as $key => $value) {
                    $data[$key]['created_at'] = dateFormatUser($value['created_at'], null, $this->companyConnection);
                }
            }

            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getSearchUsers()
    {
        $user_search = $_POST['search'];
        $user_type = $_POST['user_type'];
        $userTypeQuery = '';
        if ($user_type) {
            $userTypeQuery = 'and user_type="' . $user_type . '"';
        }
        $getUsers = $this->companyConnection->query("SELECT id,name,first_name,email FROM users where (name like '%" . $user_search . "%' OR first_name like '%" . $user_search . "%' OR email like '%" . $user_search . "%' OR last_name like '%" . $user_search . "%' ) " . $userTypeQuery)->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        if ($_POST['id_type'] == 'bccSearch') {
            $typeClass = 'getBCCEmails';
        } else if ($_POST['id_type'] == 'ccSearch') {
            $typeClass = 'getCCEmails';
        } else if ($_POST['id_type'] == 'toSearch') {
            $typeClass = 'getEmails';
        }

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='" . $typeClass . "' data-email='" . $email . "'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";
        }

        $html .= "</table><br>";

        echo $html;
        exit;
    }

    public function getRecievedEmailByID()
    {
        try {

            $communication_id = $_REQUEST['id'];
            $query = $this->companyConnection->query("SELECT communication_email.*,users.name FROM communication_email LEFT JOIN users On users.email = communication_email.email_from WHERE communication_email.id = $communication_id OR communication_email.email_parent_id = $communication_id");
            $data = $query->fetchAll();

            if(isset($data))
            {
                foreach ($data as $key=>$value)
                {
                    $time = timeFormat($value['created_at'], null, $this->companyConnection);
                    $data[$key]['created_at'] = dateFormatUser($value['created_at'], null, $this->companyConnection).' '.$time;
                }
            }


            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function replyMail()
    {
        try {

            if (empty($_REQUEST['mesgbody'])) {
                return ['status' => 'error', 'code' => 503, 'message' => 'Comment is required'];
            }

            $reply_id = $_REQUEST['reply_id'];
            $query = $this->companyConnection->query("SELECT * FROM communication_email WHERE id=" . $reply_id . " AND deleted_at IS NULL");
            $dataCommunication = $query->fetch();

            $userid = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $userQuery = $this->companyConnection->query("SELECT * FROM users WHERE id=" . $userid . " AND deleted_at IS NULL");
            $userData = $userQuery->fetch();

            $userType = $userData['user_type'];
            $data['email_parent_id']= $dataCommunication['id'];

            //$data['email_to']       = $dataCommunication['email_from'];
            $data['email_subject']  = $dataCommunication['email_subject'];
            $data['email_message']  = $_REQUEST['mesgbody'];
            $data['user_id']        = $userid;
            $data['user_type']      = $userType;
            $data['status']         = '1';
            //  $data['email_from']     = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at']     = date('Y-m-d H:i:s');
            //    unset($data['email_to'],);
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $last_id = $this->companyConnection->lastInsertId();

            if(isset($_FILES['file_library']))
            {
                $this->insertFileLibrary($_FILES['file_library'],$userid,$last_id);
            }

            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Message Sent Successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
        }
    }


    public function getUsersSearch()
    {
        $user_search = $_REQUEST['search'];
        $userTypeQuery = '';

        $getUsers = $this->companyConnection->query("SELECT email FROM users where (name like '%" . $user_search . "%' OR first_name like '%" . $user_search . "%' OR email like '%" . $user_search . "%' OR last_name like '%" . $user_search . "%' ) ")->fetchAll();
        $list = [];
        if(isset($getUsers))
        {
            foreach ($getUsers as $key=>$values)
            {
                $list[] = $values['email'];
            }
        }

        $cars['options'] = $list;
        $cars = (object)$cars;

        return $cars;
    }

    public function getUsers()
    {

        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,name,first_name,email FROM users where user_type='$user_type' ORDER BY name ASC")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        </tr>";
        if(!$getUsers)
        {
            $html .= "<td colspan=\"3\" align=\"center\"  bgcolor=\"#f7f7f7\">
                                                No Record Found
                                            </td>";
            $html .= "</tr>";
        }
        foreach ($getUsers as $users) {

            $id = $users['id'];
            /*$name = $users['first_name'];*/
            $name = $users['name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td bgcolor=\"#f7f7f7\"><input type='checkbox' class='getEmails' data-email='".$email."'></td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $name . "</td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }

    public function getCCUsers()
    {
        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,name,first_name,email FROM users where user_type='$user_type' ORDER BY name ASC")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        </tr>";
        if(!$getUsers)
        {
            $html .= "<td colspan=\"3\" align=\"center\"  bgcolor=\"#f7f7f7\">
                                                No Record Found
                                            </td>";
            $html .= "</tr>";
        }
        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td bgcolor=\"#f7f7f7\"><input type='checkbox' class='getCCEmails' data-email='".$email."'></td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $name . "</td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }

    public function getBCCUsers()
    {
        $user_type = $_POST['type'];

        $getUsers = $this->companyConnection->query("SELECT id,name,first_name,email FROM users where user_type='$user_type' ORDER BY name ASC")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        </tr>";
        if(!$getUsers)
        {
            $html .= "<td colspan=\"3\" align=\"center\"  bgcolor=\"#f7f7f7\">
                                                No Record Found
                                            </td>";
            $html .= "</tr>";
        }
        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td bgcolor=\"#f7f7f7\"><input type='checkbox' class='getBCCEmails' data-email='".$email."'></td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $name . "</td>";
            $html .= "<td bgcolor=\"#f7f7f7\">" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }

//    public function getuseremail()
//    {
//        try {
//            $id = $_REQUEST['id'];
//
//            $email = $this->companyConnection->query("SELECT email FROM users where users.id=" . $id)->fetch();
//
//            return ['status' => 'success', 'code' => 200, 'email' => $email];
//        }
//        catch (Exception $exception) {
//            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
//        }
//    }

    public function updateUserTable()
    {
        try {
            $userId = $_REQUEST['userId'];
            $cur_date = date('Y-m-d H:i:s');
            $sql = "UPDATE users SET updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$cur_date,$userId]);
            return ['status' => 'success', 'code' => 200];
        }
        catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
        }
    }

}
$composeMailAjax = new composeMailAjax();
?>