<?php

include(ROOT_URL."/config.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/MigrationCusers.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/SeederCusers.php");
include_once( ROOT_URL."/helper/globalHelper.php");

class payment extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function fetchAllPlanAjax(){
        try {

            $dbDetails = $this->conn->query("SELECT * FROM users where id=".$_POST['id'])->fetch();
            $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
            $sql = "SELECT * FROM plans_history WHERE user_id='1'";
            $data = $connection->query($sql)->fetch();
//            dd($data);
            $planDetail = $this->conn->query("SELECT * FROM plans where id=".$data['subscription_plan'])->fetch();
            $GetSourceDetail=$this->GetSourceDetail($connection);
//            $proratedCalculaion=$this->proratedCalculaion($data['pay_plan_price']);

            $sqlupgrade = "SELECT * FROM plans_history WHERE user_id=1 and status ='1'";

            $dataupgrade = $connection->query($sqlupgrade)->fetch();

            $dataupgrade["start_date"]= (isset($dataupgrade['start_date']))? dateFormatUser($dataupgrade['start_date'], null) : null;
            $dataupgrade["end_date"]=(isset($dataupgrade['end_date']))? dateFormatUser($dataupgrade['end_date'], null) : null;
            $resupgrade = $this->conn->query("SELECT * FROM plans where id=".$dataupgrade['subscription_plan'])->fetch();



            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','Result'=>$data,
                'planDetail'=>$planDetail,'GetSourceDetail'=>$GetSourceDetail,'dataupgrade'=>$dataupgrade,'resupgrade'=>$resupgrade);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }
    public function addRenewPlan() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //Save Data in Company Database
                $data['user_id'] = 1;

                $currentdate=date('Y-m-d h:i:s');
                if($data['term_plan'] == '1'){
                    $enddate=date('Y-m-d h:i:s', strtotime('+1 months'));
                    $data['start_date']= $currentdate;
                    $data['end_date']=$enddate;
                }else{
                    $enddate=date('Y-m-d h:i:s', strtotime('+1 years'));
                    $data['start_date']=$currentdate;
                    $data['end_date']=$enddate;
                }
                $data['status'] = '1';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO plans_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record Added successfully');
            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchUpgardePlanAjax(){
        try {
            $id=$_POST['id'];
            $planDetail = $this->conn->query("SELECT * FROM plans where id=".$id)->fetch();
            $html='';
            $html='<input type="text" name="no_of_units" value='.$planDetail['number_of_units'].' style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanUnits" readonly>';


            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','html'=>$html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }


    public function fetchUpgardePaymenttypeAjax(){
        try {
            $termPlanId=$_POST['id'];
            $sql = "SELECT * FROM plans_history WHERE user_id=1 order by id desc";
            $data = $this->companyConnection->query($sql)->fetch();
            $totalprice= $this->calculationPayment($termPlanId,$data['pay_plan_price']);
            if(!empty($data['discount'])){
                $totalpricePay= $totalprice - $data['discount'];
            }else{
                $totalpricePay= $totalprice;
            }

            return array('status' => 'success', 'code' => 200, 'message' => 'Record fetch successfully','totalprice'=>$totalprice,'totalpricePay'=>$totalpricePay);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }

    public function calculationPayment($termPlan,$pay_plan_price){
        //static payments variables for monthly dedcution

        $apexlinkServiceFee=75.00;
        $stripeTransactionFee=0.25;
        $feeForACH=0.25;
        $feeForCard=1.50;
        $apexlinkAdminFee=0.20;
        $stripeAccountFee=2.00;
        $ACH ='ACH';

        //static payments variables for yearly dedcution
        $apexlinkServiceFeeYearly= 75.00 * 12;
        $stripeTransactionFeeYearly=2.94;
        $feeForCardYearly=17.63;
        if($termPlan == '1'){
            if($ACH == 'ACH'){
                $totalsubscriptionCharges=$apexlinkServiceFee + (double)$pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
            }else{
                $totalsubscriptionCharges=$apexlinkServiceFee + (double)$pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
            }
            return (double)$totalsubscriptionCharges;
        }
        else {
            $pay_plan_price=(double)$pay_plan_price * 11;
            if($ACH == 'ACH'){
                $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForACH;
            }else{
                $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForCardYearly;
            }
            return $totalsubscriptionCharges;
        }
    }

    public function addUpgradePlan() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $dbDetails = $this->conn->query("SELECT * FROM users where id=".$_POST['id'])->fetch();

                $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);

                $GetSourceDetail = $this->GetSourceDetail($connection);
                if (empty($GetSourceDetail)) {
                    return array('code' => 500, 'status' => 'error', 'data' => $err_array, 'message' => 'Please Complete Payment Details First!');
                } else {
                    //Save Data in Company Database
                    $data['user_id'] = 1;
                    unset($data['card']);
                    $sqlStatus = "SELECT * FROM plans_history WHERE user_id=1 order by id ASC";
                    $dataStatus = $connection->query($sqlStatus)->fetchAll();
                    if (!empty($dataStatus)) {
                        $queryStatus = "UPDATE plans_history SET status ='0' where user_id=1";
                        $stmtStatus = $connection->prepare($queryStatus);
                        $stmtStatus->execute();
                    }

                    $currentdate = date('Y-m-d h:i:s');
                    if ($data['term_plan'] == '1') {
                        $enddate = date('Y-m-d h:i:s', strtotime('+1 months'));
                        $data['start_date'] = $currentdate;
                        $data['end_date'] = $enddate;
                    } else {
                        $enddate = date('Y-m-d h:i:s', strtotime('+1 years'));
                        $data['start_date'] = $currentdate;
                        $data['end_date'] = $enddate;
                    }
                    $data['status'] = '1';
                    $discount = "SELECT discount_price FROM users WHERE id=1";
                    $datadiscount = $connection->query($discount)->fetch();
                    if(!empty($datadiscount['discount_price'])){
                        $data['discount']=$datadiscount['discount_price'];
                    }
                    else{
                        $data['discount']=0;
                    }
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO plans_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $connection->prepare($query);
                    $stmt->execute($data);
                    $id = $connection->lastInsertId();


                    $sqlusers = "SELECT stripe_customer_id FROM users WHERE id=1";
                    $datausers = $connection->query($sqlusers)->fetch();
                    $request = [];
                    $request["customer_id"] = $datausers['stripe_customer_id'];
                    if ($datausers['stripe_customer_id'] != '') {
                        $customer_id = $datausers['stripe_customer_id'];
                        $cards = Allsource($customer_id, $_POST['card']);
                        foreach ($cards['data'] as $cc) {
                            updateCustomer($customer_id, $cc['id']);
                        }
                    }

                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record Added successfully');
                }
            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function createCustomer($user_id)
    {
        $dbDetails = $this->conn->query("SELECT * FROM users where id=".$_POST['id'])->fetch();
        $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
        $getData =  $connection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer    =     \Stripe\Customer::create(array(
                'email' => $email
            ));
            $customer_id =        $customer['id'];

            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";

            $stmt = $connection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }
    public function GetSourceDetail($connection){


        $sqlusers = "SELECT * FROM users WHERE id=1";
        $datausers = $connection->query($sqlusers)->fetch();
        $request=[];
        $request["customer_id"] = $datausers['stripe_customer_id'];
        if(empty($request["customer_id"])){
            $getDetailUser= $this->createCustomer(1);
        }else{
            $getDetailUser= getCustomer($request);
        }
        $sources = $getDetailUser['customer_data']['default_source'];

        if(isset($sources) && !empty($sources)) {
            if (strpos($sources, 'card') !== false) {

                $card = 'yes';
            } else {
                $card = 'no';
            }
        }else{
            $card ="";
        }
        return $card;
    }



    public function fetchPlanAjax(){
        try {
            $sql = "SELECT * FROM plans WHERE status ='1'";
            $data = $this->conn->query($sql)->fetchAll();

            $discount = "SELECT discount_price FROM users WHERE id=" . $_POST['id'];
            $datadiscount = $this->conn->query($discount)->fetch();
            if(!empty($datadiscount['discount_price'])){
                $discount=$datadiscount['discount_price'];
            }
            else{
                $discount=0;
            }
            $html='';
            $html = '<option value="default">Select</option>';
            foreach ($data as $dd){
                $html .="<option  price= ".$dd['month_rate']." units =".$dd['number_of_units']." value= " . $dd['id'] . ">" . $dd['plan_name'] . "</option>";

            }


            return array('status' => 'success', 'code' => 200, 'html' => $html,'discount'=>$discount);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }

    }





}


$paymentStripe = new payment();

?>