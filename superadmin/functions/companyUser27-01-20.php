<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/MigrationCusers.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/SeederCusers.php");
include_once( ROOT_URL."/helper/globalHelper.php");
class CompanyUserAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  Insert Data to Company Users
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $free_plan = @$_POST['free_plan'];
            $data['discount_price'] = isset($data['discount'])?$data['discount']:0;
            $data['plan_price']=str_replace(',','',$data['plan_price']);
            $data['pay_plan_price'] = (double)$data['plan_price'] - (double)$data['discount_price'];
            if(isset($data['eft_country']) && !empty($data['eft_country']))
            {
                if($data['eft_country'] == 'UnitedStates')
                {
                    $formrequiredkeys = ['password_confirmation','first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                        'state','country','default_currency','subscription_plan','term_plan','domain_name','routing_number','origin','trace_number'
                    ];
                } else {
                    $formrequiredkeys = ['password_confirmation','first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                        'state','country','default_currency','subscription_plan','term_plan','domain_name','originator_id_number','cpa_code','originator_name','transit_number',
                        'account_number','due_date'
                    ];
                }
            } else {
                $formrequiredkeys = ['password_confirmation','first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                    'state','country','default_currency','subscription_plan','term_plan','domain_name'
                ];
            }

            $err_array = [];
            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(in_array('routing_number',$formrequiredkeys))
            {   //die(strlen($data['routing_number']));
                if(isset($data['routing_number']))
                {
                    $route_len = strlen($data['routing_number']); ;
                    if((int)$route_len < 9 and (int)$route_len > 0)
                    {
                        $err_array['routing_numberErr'] = "* Minimum 9 characters allowed";
                    }
                }
            }

            if(in_array('trace_number',$formrequiredkeys))
            {
                if(isset($data['trace_number']))
                {
                    $trace_len = strlen($data['trace_number']); ;

                    if((int)$trace_len < 15 and (int)$trace_len > 0)
                    {
                        $err_array['trace_numberErr'] = "* Minimum 15 characters allowed";
                    }
                }
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                foreach ($data as $key=>$value){
                    switch ($key) {
                        case 'default_date_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Default Date format is required";
                            }
                            break;
                        case 'date_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Date format is required";
                            }
                            break;
                        case 'default_clock_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Clock Format is required";
                            }
                            break;
                        case 'password_confirmation' :
                            if($value != $data['password']) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Password does not match with Confirm Password";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $alldata = $data;
                if(!empty($data['subscription_plan'])){
                    $plan = $this->conn->query("SELECT * FROM plans where id=".$data['subscription_plan'])->fetch();
                    $alldata['number_of_units'] = $plan['number_of_units'];
                    $alldata['plan_size'] = $plan['max_units'];
                }
                if($free_plan)
                {
                    $alldata['free_plan'] = $data['free_plan'] = 1;
                    // $alldata['status'] = $data['status'] = 0;
                } else {
                    $alldata['free_plan'] = $data['free_plan']  = 0;
                    // $alldata['status'] = $data['status'] = 2;
                }
                // $alldata['status'] = $data['status'] = 4;
                unset($data['password_confirmation'],$alldata['password_confirmation'],$data['phone_number_prefix'],$data['zipcode'],$data['discount'],$data['address4'],$data['remaining_days'],$data['extended_days'],$data['eft_country'],$data['originator_id_number'],$data['cpa_code'],$data['originator_name'],$data['transit_number'],$data['account_number'],$data['due_date'],$data['routing_number'],$data['origin'],$data['trace_number'],$data['default_currency'],$data['eft_country']);

                $alldata['actual_password'] = $data['actual_password']= $data['password'];
                $alldata['password'] = $data['password']= md5($data['password']);
                $alldata['name'] = $data['name']= $data['first_name'].' '.$data['last_name'];
                $alldata['user_type'] = $data['user_type'] = 1;
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $alldata['parent_id'] = 0;
                $alldata['expiration_date'] = $data['expiration_date'];
                $alldata['last_user_url'] = '/Dashboard/Dashboard';




                $alldata['updated_by_user'] = $data['updated_by_user'] = $_SESSION[SESSION_DOMAIN]['user_id'];

                //Currently its empty but will use further.
//                $alldata['sms_carrier'] = $data['sms_carrier'] = '';
//                $alldata['send_owners_package'] = $data['send_owners_package'] = '';
                $alldata['status'] = $data['status'] = '4';
//                $alldata['expiration_date'] = $data['expiration_date'] = mySqlDateFormat($data['expiration_date']);
                // echo '<pre>'; print_r($data); echo '</pre>';
                //   echo '<pre>'; print_r($alldata); echo '</pre>'; die;
                if(isset($data['expiration_date']) && !empty($data['expiration_date']))
                {

                } else {
                    unset($alldata['expiration_date'],$data['expiration_date']);
                    // $alldata['expiration_date'] = $data['expiration_date'] = NULL;
                }


                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO users (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                $lastInsertId = $this->conn->lastInsertId();
                //  unset($data['discount_price']);
                //do not remove this code

                $alldata['admindb_id'] = $lastInsertId;
                //CREATE DATABASE
                $this->createDatabase($lastInsertId);
                $dbDetails = $this->conn->query("SELECT * FROM users where id=".$lastInsertId)->fetch();
                $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                $migration = MigrationCusers::runMigrations($connection);
                $this->addPlanHistory($data,$connection,$lastInsertId);
                $request['company_id']  = $lastInsertId;
                $request['email']       = $data['email'];

                //Add Values in Default Settings
//                $sql1 = "UPDATE users SET status=? WHERE id=?";
//                $stmt1= $connection->prepare($sql1);
//                $stmt1->execute([0,1]);

                $defaultSettingData = array(
                    'zip_code'          =>$alldata['zipcode'],
                    'country'           =>$alldata['country'],
                    'state'             =>$alldata['state'],
                    'city'              =>$alldata['city'],
                    'currency'          =>$alldata['default_currency'],
                    'user_id'           =>1,
                    'property_size'     =>'Sq ft',
                    'page_size'         =>10,
                    'notice_period'     =>4,
                    'timeout'           =>'15',

                );
                $sqlData = createSqlColVal($defaultSettingData);
                $queryData = "INSERT INTO default_settings (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmtData = $connection->prepare($queryData);
                $stmtData->execute($defaultSettingData);
                if($migration['code'] == 200)
                {
                    $alldata['id'] = 1;

                    SeederCusers::runSeeders($connection);
                    //Save Data in Company Database
                    // echo '<pre>'; print_r($alldata); echo '</pre>'; die;
                    $sqlData = createSqlColVal($alldata);
                    $query = "INSERT INTO users (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $connection->prepare($query);
                    $stmt->execute($alldata);


                    //Send Mail After Create New User
                    $this->sendMail($lastInsertId);

                } else {
                    return array('code' => 400, 'status' => 'failed','message' => "Something went wrong! Please contact to Administrator.");
                }

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Added successfully');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Insert Data to Update Users
     */
    public function update()
    {
        try {
            $data = $_POST['form'];
            $free_plan = @$_POST['free_plan'];
            $account_type = @$_POST['account_type'];
            $data = postArray($data);

            if(isset($data['eft_country']) && !empty($data['eft_country']))
            {
                if($data['eft_country'] == 'UnitedStates')
                {
                    $formrequiredkeys = ['first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                        'state','country','default_currency','subscription_plan','term_plan','domain_name','routing_number','origin','trace_number'
                    ];
                } else {
                    $formrequiredkeys = ['first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                        'state','country','default_currency','subscription_plan','term_plan','domain_name','originator_id_number','cpa_code','originator_name','transit_number',
                        'account_number','due_date'
                    ];
                }
            } else {
                $formrequiredkeys = ['first_name','last_name','email','password','company_name','phone_number','zipcode','city',
                    'state','country','default_currency','subscription_plan','term_plan','domain_name'
                ];
            }
            $err_array = [];

            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(in_array('routing_number',$formrequiredkeys))
            {
                if(isset($data['routing_number']))
                {
                    $route_len = strlen($data['routing_number']); ;
                    if((int)$route_len < 9 and (int)$route_len > 0)
                    {
                        $err_array['routing_numberErr'] = "* Minimum 9 check allowed";
                    }
                }
            }

            if(in_array('trace_number',$formrequiredkeys))
            {
                if(isset($data['trace_number']))
                {
                    $trace_len = strlen($data['trace_number']); ;

                    if((int)$trace_len < 15 and (int)$trace_len > 0)
                    {
                        $err_array['trace_numberErr'] = "* Minimum 15 characters allowed";
                    }
                }
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                foreach ($data as $key=>$value){
                    switch ($key) {
                        case 'default_date_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Default Date format is required";
                            }
                            break;
                        case 'date_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Date format is required";
                            }
                            break;
                        case 'default_clock_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Clock Format is required";
                            }
                            break;
                        case 'default_clock_format' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Clock Format is required";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

//            if()

            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $alldata = $data;
                unset($data['phone_number_prefix'],$data['zipcode'],$data['discount'],$data['address4'],$data['remaining_days'],$data['extended_days'],$data['eft_country'],$data['originator_id_number'],$data['cpa_code'],$data['originator_name'],$data['transit_number'],$data['account_number'],$data['due_date'],$data['routing_number'],$data['origin'],$data['trace_number'],$data['default_currency'],$data['free_plan'],$data['eft_country']);

                if($free_plan)
                {
                    $alldata['free_plan'] = $data['free_plan'] = '1';
                    $data['status'] = 1;
                    // $alldata['status'] = $data['status'] = 1;
                } else {
                    $alldata['free_plan'] = $data['free_plan'] = '0';
                    $data['status'] = 2;
                    // $alldata['status'] = $data['status'] = 2;
                }
                $id = $data['id'];
                $userdata = $this->conn->query("SELECT status FROM users where id='$id' ")->fetch();
                if($userdata['status'] == 4 )
                {
                    $alldata['status'] = $data['status'] = 4;
                }


                $alldata['account_type']    = $account_type;
                $alldata['updated_by_user'] = $data['updated_by_user'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                if(!$data['expiration_date'])
                {
                    unset($data['expiration_date']);
                } else {

                }

                if(isset($data['expiration_date']) && !empty($data['expiration_date']))
                {

                } else {
                    unset($alldata['expiration_date'],$data['expiration_date']);
                    // $alldata['expiration_date'] = $data['expiration_date'] = NULL;
                }
                $sqlData = createSqlUpdateCase($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";

                $stmt = $this->conn->prepare($query);
                $stmt->execute($sqlData['data']);
                $alldata['id'] = 1;
                $connection = getCompanyConnection($id);
                $sqlData1 = createSqlUpdateCase($alldata);
                $query1 = "UPDATE users SET ".$sqlData1['columnsValuesPair']." where id=1";
//                  dd($query1);
                $stmt1 = $connection->prepare($query1);
                $stmt1->execute($sqlData1['data']);

//                $sql = "UPDATE users SET free_plan=?";
//                $stmt= $connection->prepare($sql);
//                $stmt->execute([$alldata['free_plan']]);

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Updated successfully');

            }
        }
        catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for fetch Default Currency List
     */
    public function getOnLoadCompanyUserData()
    {
        try {
            $data = [];
            $data['default_currency'] =$this->conn->query("SELECT * FROM default_currency ")->fetchAll();
            $plan_status = 1;
            $data['plans'] =$this->conn->query("SELECT * FROM plans WHERE status='$plan_status'")->fetchAll();

            if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
            {
                $connection = getCompanyConnection($_REQUEST['id']);
                $data['company_data'] =$connection->query("SELECT * FROM users")->fetch();
                if(!empty($data['company_data'])){
                    if(!empty($data['company_data']['expiration_date'])) $data['company_data']['expiration_date_label'] = dateFormatUser($data['company_data']['expiration_date'],null).' '.timeFormat($data['company_data']['expiration_date'],null);
                }

                $now = time(); // or your date as well

                $your_date = strtotime($data['company_data']['expiration_date']);
                $datediff = $your_date - $now;
                $datediff2 = round($datediff / (60 * 60 * 24));

                $data['company_data']['remaining_days'] = $datediff2;

            }
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to Check Domain or Company Exists or Not
     */
    public function CheckDomainCompanyExist()
    {
        try{
            $company_name = $_REQUEST['company_name'];
            $type = $_REQUEST['type'];
            if($type == 'domain')
            {
                $data = $this->conn->query("SELECT * FROM users WHERE domain_name='$company_name' ")->fetch();
            } else if($type == 'email')
            {
                $data = $this->conn->query("SELECT * FROM users WHERE email='$company_name' ")->fetch();
            } else {
                $data = $this->conn->query("SELECT * FROM users WHERE company_name='$company_name' ")->fetch();
            }
            if($data)
            {
                return ['status'=>'failed','code'=>204,'data'=>'','message'=>$type.' Already Exists'];
            } else {
                return ['status'=>'success','code'=>200,'data'=>''];
            }
        }   catch (PDOException $e) {
            return ['status'=>'failed','code'=>503,'data'=>$e->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    function validations($data)
    {

        try{
            foreach ($data as $key=>$value){
                switch ($key) {
                    case 'company_name' :
                        if(empty($value)) {
                            $errName = $key . 'Err';
                            $err_array[$errName] = "company name is required";
                        }
                        break;
                    case 'zip_code' :
                        if(empty($value)) {
                            $errName = $key . 'Err';
                            $err_array[$errName] = "Zip Code is required";
                        }
                        break;
                    case 'timeout' :
                        if(empty($value)) {
                            $errName = $key . 'Err';
                            $err_array[$errName] = "Timeout is required";
                        }
                        break;
                    case 'address1' :
                        if(empty($value)) {
                            $errName = $key . 'Err';
                            $err_array[$errName] = "Address 1 is required";
                        }
                        break;
                    default:
                        break;
                }
            }
            return $err_array;
        }   catch (PDOException $e) {
            return ['status'=>'failed','code'=>503,'data'=>$e->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Insert Data to Update Users
     */
    public function createDatabase($lastInsertId=null)
    {

        $random = randomString(9);
        $company_id = $this->conn->query("SELECT * FROM users where id=".$lastInsertId)->fetch();
        //echo '</pre>'; print_r($company_id['id']); echo '</pre>'; die;
        $user='user_'.$company_id['id'].'_'.$company_id['domain_name'];
        $pass=$random;
        $db="apexlink_pm_".$company_id['id'];
        $host = self::CLIENT_DB_HOST;
        $port = self::CLIENT_DB_PORT;
        $username = self::CLIENT_DB_USERNAME;
        $password = self::CLIENT_DB_PASSWORD;

        try {

            $dbh = new PDO("mysql:port=$port;host=".$host, $username, $password);

            $dbh->exec("CREATE DATABASE `$db`;
                CREATE USER '$user'@'$host' IDENTIFIED BY '$pass';
                GRANT ALL PRIVILEGES ON `$db`.* TO '$user'@'%' IDENTIFIED BY '$pass';             
                FLUSH PRIVILEGES;")
            or die(print_r($dbh->errorInfo(), true));

            $sql = "UPDATE users SET host=?, db_username=?, db_password=?,database_name=?,port=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([$host, $user, $pass, $db,$port, $lastInsertId]);

        } catch (PDOException $e) {
            die("DB ERROR: ". $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Activate Company
     */
    public function activate(){
        try{
            $company_id = $_REQUEST['id'];
            $sql = "UPDATE users SET status=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([1,$company_id]);

            //company changes
            $dbDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
            $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
            $sql1 = "UPDATE users SET status=? WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([1,1]);

            return ['status'=>'success','code'=>200,'data'=>'Company activated successfully'];
        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Subscription cancel
     */
    public function cancelSubscription(){
        try{
            $company_id = $_REQUEST['id'];
            $sql = "UPDATE users SET payment_status=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            if($stmt->execute(['0',$company_id])) {
                return ['status' => 'success', 'code' => 200, 'data' => 'Subscription cancelled successfully'];
            }else{
                return ['status' => 'failed', 'code' => 400, 'data' => 'Subscription not cancelled'];
            }
        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Deactivate Company
     */
    public function deactivate(){
        try{
            $company_id = $_REQUEST['id'];
            $sql = "UPDATE users SET status=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([0,$company_id]);

            //company changes
            $dbDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
            $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
            $sql1 = "UPDATE users SET status=? WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([0,1]);

            return ['status'=>'success','code'=>200,'data'=>'Company deactivated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Delete Company
     */
    public function delete(){
        try{
            $company_id = $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE users SET deleted_at=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([$data,$company_id]);

            //company changes
            $dbDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
            $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
            $sql1 = "UPDATE users SET deleted_at=? WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([$data,1]);

            return ['status'=>'success','code'=>200,'data'=>'Company deleted successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function launch(){
        try{
            $company_id = $_REQUEST['id'];

            //company changes
            $userDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
            //  echo '<pre>'; print_r($userDetails); echo '</pre>'; die;
            if($userDetails['free_plan'])
            {
                $status = '1';
            } else {
                $status = '2';
            }
            // if($userDetails['free_plan'])
            $sql = "UPDATE users SET status=? WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([$status,$company_id]);

            $connection = DBConnection::dynamicDbConnection($userDetails['host'],$userDetails['database_name'],$userDetails['db_username'],$userDetails['db_password']);
            $sql1 = "UPDATE users SET status=? WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([$status,1]);

            return ['status'=>'success','code'=>200,'data'=>'Company launch successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
//    public function launch(){
//        try{
//            $company_id = $_REQUEST['id'];
//            $data = '1';
//            $sql = "UPDATE users SET status=? WHERE id=?";
//            $stmt= $this->conn->prepare($sql);
//            $stmt->execute([$data,$company_id]);
//
//            return ['status'=>'success','code'=>200,'data'=>'Company launch successfully'];
//        }catch (Exception $exception)
//        {
//            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
//            printErrorLog($exception->getMessage());
//        }
//    }

    public function sendMail($userid = null)
    {
        try{
            $userid = (isset($userid) ? $userid : $_REQUEST['id']);
            $user_details = $this->conn->query("SELECT * FROM users where id=".$userid)->fetch();
            $name = userName($user_details['id'], 'users');
            $url = 'https://'.$user_details['domain_name'].'.'.COMPANY_DOMAIN_URL;
            $body = file_get_contents(SUPERADMIN_DIRECTORY_URL.'/views/Emails/welcome.php');
            $body = str_replace("#name#",ucfirst($name),$body);
            $body = str_replace("#email#",$user_details['email'],$body);
            $body = str_replace("#password#",$user_details['actual_password'],$body);
            $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            $body = str_replace("#website#",$url,$body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];

            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
            $request['attachments'][] = SITE_URL.'uploads/email_attachments/WelcomeMailAttachment.docx';

            curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function goToSite()
    {
        try{
//            print_r($_SESSION[SESSION_DOMAIN]); die();
            $data =$this->conn->query("SELECT * FROM users where id=".$_REQUEST['id'])->fetch();
            $token = array('u'=>$data['email'],'p'=>$data['actual_password'], 'sa_name'=>$_SESSION[SESSION_DOMAIN]['name']);
            $token = json_encode($token);
            $token = base64_encode($token);

            $port = ($_SERVER['SERVER_PORT'])? ':'.$_SERVER['SERVER_PORT'] : '';
            $domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$data['domain_name'].".".COMPANY_DOMAIN_URL.$port;
            $url = $domain.'/?prefilled='.$token;
            return ['status'=>'success','code'=>200,'data'=>array('url'=>$url)];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function validateFinancialData()
    {
      //  echo 'aman';
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','fcurrency','frouting_number','furl','fday','fmonth','fyear'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function validateCompanyFinancialData()
    {
        try{
            $fdata = $_REQUEST;
            $formrequiredkeys = ['ffirst_name','flast_name','femail','fphone_number','fbirth_date','fzipcode','fcity','fstate','faddress','fbusiness','fssn','faccount_holder','faccount_holdertype',
                'faccount_number','fiso','fcurrency','frouting_number','fcaddress','fcaddress','fczipcode','fcstate','fcname','furl','company_document_id','fday','fmonth','fyear'
            ];
            $data = [];
            if(isset($fdata['data']))
            {
                foreach ($fdata['data'] as $key=>$value)
                {
                    if(isset($value['name']))
                    {
                        $data[$value['name']] = @$value['value'];
                    }
                }
            }
            $err_array = validation($data,$this->conn,$formrequiredkeys);
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                return ['status'=>'success','code'=>200,'data'=>$data];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function ajaxDateFormatSet()
    {
        try{

            if(isset($_REQUEST['dateValue']))
            {
                return dateFormatUser($_REQUEST['dateValue'],null).' '.timeFormat($_REQUEST['dateValue'],null);
            }

        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function paymentStatus()
    {
        try{
            $company_data = $_REQUEST['data'];

            $company_status = isset($company_data[0]['value'])? $company_data[0]['value'] : '';
            $company_id = isset($company_data[1]['value'])? $company_data[1]['value'] : '';

            $sql = "UPDATE users SET payment_type=?, status='1' WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([$company_status,$company_id]);

            $connection = getCompanyConnection($company_id);
            $sql1 = "UPDATE users SET payment_type=?, status='1' WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([$company_status,'1']);


            return ['status'=>'success','code'=>200,'data'=>'Company Activated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function createPmAccount()
    {
        try{
            $company_data = $_REQUEST['data'];

            $company_status = isset($company_data[0]['value'])? $company_data[0]['value'] : '';
            $company_id = isset($company_data[1]['value'])? $company_data[1]['value'] : '';

            $sql = "UPDATE users SET payment_type=?, status='1' WHERE id=?";
            $stmt= $this->conn->prepare($sql);
            $stmt->execute([$company_status,$company_id]);

            $connection = getCompanyConnection($company_id);
            $sql1 = "UPDATE users SET payment_type=?, status='1' WHERE id=?";
            $stmt1= $connection->prepare($sql1);
            $stmt1->execute([$company_status,'1']);


            return ['status'=>'success','code'=>200,'data'=>'Company Activated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function addPlanHistory($data,$connection,$lastInsertId)
    {
       try{
           $planArray=[];
           $plan = $this->conn->query("SELECT * FROM plans where id=".$data['subscription_plan'])->fetch();
           $planArray['subscription_plan']=$data['subscription_plan'];
           $planArray['term_plan']=$data['term_plan'];
           $planArray['plan_price']=$data['plan_price'];
           if(!empty($data['discount_price'])){
               $planArray['discount']=$data['discount_price'];
           }else{
               $planArray['discount']=0;
           }

           $planArray['pay_plan_price']=$data['pay_plan_price'];
           $planArray['status']='1';
           $planArray['user_id']=1;
           $planArray['created_at'] = date('Y-m-d H:i:s');
           $planArray['updated_at'] = date('Y-m-d H:i:s');
           $planArray['no_of_units']=$plan['number_of_units'];
           if($data['free_plan'] == '1'){
               $planArray['start_date']=date('Y-m-d');
               $planArray['end_date']=$data['expiration_date'];
               $planArray['trial_acc']='1';
           }else{
               $planArray['trial_acc']='0';
               if($data['term_plan'] == '1'){
                   $planArray['start_date']=date('Y-m-d');
                   $planArray['end_date']=date('Y-m-d', strtotime('+1 months'));
               }else{
                   $planArray['start_date']=date('Y-m-d');
                   $planArray['end_date']=date('Y-m-d', strtotime('+1 years'));
               }
           }
           $sqlData = createSqlColVal($planArray);
           $queryplan= "INSERT INTO  plans_history (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
           $stmtplan = $connection->prepare($queryplan);
           $stmtplan->execute($planArray);
       }catch (Exception $exception)
       {
           print_r($exception->getMessage()); die;
       }
    }



    public function getCompanyUserData()
    {
        try{
            $data = $_POST;
            $company_details = getSingleRecord($this->conn ,['column'=>'id','value'=>$data['id']], 'users');
           // $userid = $company_details['data']['id'];
            //$record = getSingleRecord($this->companyConnection, ['column' => 'admindb_id', 'value' => $userid], 'users');
          //  dd($_SESSION[SESSION_DOMAIN]);
            return array('status' => 'success', 'code'=>'200',  'message' => 'Data fetched successfully.','data'=>$company_details);
        }catch (Exception $exception)
        {
            print_r($exception->getMessage()); die;
        }
    }

    public function getMccTypes()
    {
        try {
            $data = [];
            $data['mcc_types'] =$this->conn->query("SELECT id,name, code FROM mcc_types")->fetchAll();
            return ['code' => 200, 'status' => 'success', 'data' => $data];
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }



    public function createUserConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
            $fdata = postArray($fdata);
            $company_id = $_POST["company_id"];
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => '', 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => (isset($fdata["fmcc"])&& !empty($fdata["fmcc"])?$fdata["fmcc"]: ''),'business_type'=>$fdata['fbusiness']];
            $createPmAccount = createAccount($stripe_account_array);
            if ($createPmAccount["code"] == 200 && $createPmAccount["status"] == "success") {

                $stripe_account_array["stripe_account_id"] = $createPmAccount['account_id'];
                $enablePmAccount = enableAccount($stripe_account_array);
                if($enablePmAccount["code"] == 200){
                    $dbDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
                    $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                    $data["stripe_account_id"]=$createPmAccount['account_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='1'";
                    $stmt = $connection->prepare($query);
                    $stmt->execute();
                    $query1 = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id=$company_id";
                    $stmt1 = $this->conn->prepare($query1);
                    $stmt1->execute();
                    // $alldata['stripe_account_id'] = $createPmAccount['account_id'];

                    $stripe_account_detail_array = ["user_id"=>1,"email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $fdata['furl'], 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => !empty($fdata["fmcc"])?$fdata["fmcc"]:'','business_type'=>$fdata['fbusiness'],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')];

                    $save_account_detail = saveUserAccountDetail($connection,$stripe_account_detail_array);
                }else{
                    $account = \Stripe\Account::retrieve($createPmAccount['account_id']);
                    $account->delete();
                }



                return $enablePmAccount;
            }else{
                return $createPmAccount;
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }






    public function createCompanyConnectedAccount(){
        try {

            $fdata = $_POST['financial_data'];
            $fdata = postArray($fdata);
            $company_id = $_POST["company_id"];
            //$website_url = "https://www.apexlink.com/";
            $website_url = $fdata['furl'];
            $address1 = "address_full_match"; /*$fdata['faddress1'];*/
            $address2 = ""; /*$fdata['faddress2'];*/
            $phone = (isset($fdata['fphone_number']) && !empty($fdata['fphone_number'])) ? str_replace(["-", "–"], '', $fdata['fphone_number']) : '';
            $stripe_account_array = ["email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "line1" => $address1, "line2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last_4' => $fdata['fssn'], 'ssn_last_4' => $fdata['fssn'], 'phone' => $phone, 'document_front' => $fdata['company_document_id'], 'document_back' => '', 'website_url' => $website_url, 'product_description' => '', 'support_email' => $fdata['femail'], 'country' => 'US', 'currency' => 'USD', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_line1'=>$address1,'company_line2'=>$address2,'company_country'=>'US','company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'company_tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone'=>$fdata['fcphone_number']];
            $createPmAccount = createAccount($stripe_account_array);
            if ($createPmAccount["code"] == 200 && $createPmAccount["status"] == "success") {
                $stripe_account_array["stripe_account_id"] = $createPmAccount['account_id'];
                $enablePmAccount = enableCompanyAccount($stripe_account_array);
                if($enablePmAccount["code"]== 200){
                    $dbDetails = $this->conn->query("SELECT * FROM users where id=".$company_id)->fetch();
                    $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
                    $data["stripe_account_id"]=$createPmAccount['account_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='1'";
                    $stmt = $connection->prepare($query);
                    $stmt->execute();
                    $query1 = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id=$company_id";
                    $stmt1 = $this->conn->prepare($query1);
                    $stmt1->execute();
                    $alldata['stripe_account_id'] = $createPmAccount['account_id'];
                    $stripe_account_detail_array = ["user_id"=>1,"email" => $fdata['femail'], "first_name" => $fdata['ffirst_name'], "last_name" => $fdata['flast_name'], "city" => $fdata['fcity'], "state" => $fdata['fstate'], "address_1" => $address1, "address_2" => $address2, 'postal_code' => $fdata['fzipcode'], 'day' => $fdata['fday'], 'month' => $fdata['fmonth'], 'year' => $fdata['fyear'], 'ssn_last' => $fdata['fssn'], 'ssn_last' => $fdata['fssn'], 'phone' => $phone, 'url' => $website_url, 'country' => 'US', 'routing_number' => $fdata["frouting_number"], 'account_number' => $fdata["faccount_number"], 'mcc' => $fdata["fmcc"],'company_name'=>$fdata['fcname'],'company_address1'=>$address1,'company_address2'=>$address2,'company_city'=>$fdata['fccity'],'company_postal_code'=>$fdata['fczipcode'],'company_state'=>$fdata['fcstate'],'tax_id'=>$fdata['fctax_id'],'business_type'=>$fdata['fbusiness'],'company_phone_number'=>$fdata['fcphone_number'],'company_document_id'=>$fdata['company_document_id']];

                    $save_account_detail = saveUserAccountDetail($connection,$stripe_account_detail_array);

                }else{
                    $account = \Stripe\Account::retrieve($createPmAccount['account_id']);
                    $account->delete();
                }
                return $enablePmAccount;
            }else{
                return $createPmAccount;
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }



    public function uploadCompanyDocument(){
        try {
            $request=[];
            if ($_FILES["company_document"]["error"] == UPLOAD_ERR_OK)
            {
                $file = $_FILES["company_document"]["tmp_name"];
                // now you have access to the file being uploaded
                // $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
                $uploadPath = ROOT_URL . '/uploads/company_documents/' . $_FILES["company_document"]["name"];
                //perform the upload operation.
                if (!is_dir(ROOT_URL . '/uploads/company_documents')) {
                    //Directory does not exist, so lets create it.
                    mkdir(ROOT_URL . '/uploads/company_documents', 0777, true);
                }
                if( move_uploaded_file ( $file, $uploadPath)){
                    $request["path"] = $uploadPath;
                    $upload_documents = uploadDocuments($request);
                    if($upload_documents["code"] == 200){
                        unlink($uploadPath);
                        return array('code' => 200, 'status' => 'success', 'message' => "Document Uploaded Successfully",'document_id'=>$upload_documents["document_id"]);
                    }else{
                        return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                    }
                }else{
                    return array('code' => 504, 'status' => 'error', 'message' => "Document Not Uploaded Successfully");
                }
            }
        }catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }

    }

    public function generateSmartMoveSecurityHeader(){
        $partnerId = 188;
        if (isset($_POST['securityKey']) && $_POST['securityKey'] != ""){
            $securityKey = $_POST['securityKey'];
        }else{
            $securityKey = '';
        }
        
        $serverTime = $_POST['serverTime'];
        $message = "{$partnerId}{$serverTime}";
        //Generate HMAC SHA1 hash
        $hmac = hash_hmac("sha1", $message, $securityKey , true);
        $hash = base64_encode($hmac);
        echo json_encode(array('hash_key'=>$hash)); die();
    }





}

$user = new CompanyUserAjax();