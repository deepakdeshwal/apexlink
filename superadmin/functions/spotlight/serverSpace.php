<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include("$_SERVER[DOCUMENT_ROOT]/config.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/superadmin/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class serverSpace extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function isa_convert_bytes_to_specified($bytes, $to, $decimal_places = 1) {


        $formulas = array(
            'K' => number_format((float) $bytes / 1024, $decimal_places),
            'M' => number_format((float) $bytes / 1048576, $decimal_places),
            'G' => number_format((float) $bytes / 1073741824, $decimal_places)
        );
        return isset($formulas[$to]) ? $formulas[$to] : 0;
    }
    public function getSpaceGraph(){
        try {
                $dir = "/";

                // total space
                $total = disk_total_space($dir);

                // total space in kbytes
                $totlaDataSpace = $total . "\n";

                // free space
                $freespace = disk_free_space($dir);

                // total free space in kbytes
                $freespaceData = $freespace . "\n";

                $usedSpace= (float) $totlaDataSpace - (float) $freespaceData;

                $freespaceGb=$this->isa_convert_bytes_to_specified($freespace,'G',1);
                $totalspaceGb=$this->isa_convert_bytes_to_specified($totlaDataSpace,'G',1);
                $usedSpaceGb=$this->isa_convert_bytes_to_specified($usedSpace,'G',1);

//                dd($usedSpaceGb);

                return array('code' => 200, 'status' => 'success', 'freespace' => $freespaceGb,'totalspace' => $totalspaceGb,'usedSpaceGb' => $usedSpaceGb);


        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

}
$serverSpace = new serverSpace();