<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include("$_SERVER[DOCUMENT_ROOT]/config.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/superadmin/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class Spotlight extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getActiveInactiveUsers(){
        try {
            $activeSatus = $this->conn->query("SELECT count(id) AS activeStatus FROM `users` WHERE status='1'")->fetch();
            $inActiveSatus = $this->conn->query("SELECT count(id) AS inActiveStatus FROM `users` WHERE status='0' and '4'")->fetch();

            $totalActive = $activeSatus['activeStatus'] + $inActiveSatus['inActiveStatus'];

            /*$new_width1 = ($vacaentUnit['vacent'] / 100) * $totalUnit;*/

            $percentActive = $activeSatus['activeStatus']/$totalActive;
            $percent_active = number_format( $percentActive * 100 );
            //dd($percent_active);
            $percentInactive = $inActiveSatus['inActiveStatus']/$totalActive;
            $percent_inactive = number_format( $percentInactive * 100 );
            //$totaData = array($percent_vacant,$percent_occupiad);
            //dd($percent_inactive);


            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPaidFreeUsers(){
        try {
            $paidUsers = $this->conn->query("SELECT count(id) AS paidUsers FROM `users` WHERE status='1' AND free_plan='0'")->fetch();
            $freeUsers = $this->conn->query("SELECT count(id) AS freeUsers FROM `users` WHERE free_plan='1' And status='1'")->fetch();

            $totalActive = $paidUsers['paidUsers'] + $freeUsers['freeUsers'];

            /*$new_width1 = ($vacaentUnit['vacent'] / 100) * $totalUnit;*/

            $percentpaidUsers = $paidUsers['paidUsers']/$totalActive;
            $percent_paidUsers = number_format( $percentpaidUsers * 100 );
            //dd($percent_active);
            $percentfreeUsers = $freeUsers['freeUsers']/$totalActive;
            $percent_freeUsers = number_format( $percentfreeUsers * 100 );
            //$totaData = array($percent_vacant,$percent_occupiad);
            //dd($percent_inactive);


            return array('code' => 200, 'status' => 'success','datapaidUsers' => $percent_paidUsers,'datafreeUsers' => $percent_freeUsers, 'message' => 'Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPlansUsers(){
        try {
            $paidUsers = $this->conn->query("SELECT COUNT(u.id) as totalUsers,p.plan_name FROM plans p JOIN users u ON p.id = u.subscription_plan WHERE u.status = '1' group by p.plan_name")->fetchAll();

            $sum = 0;
            $totalUsers = [];

            $planName = [];
            foreach($paidUsers as $key=>$value)
            {
                $sum+= $value['totalUsers'];
            }
             //   print_r($paidUsers);
            foreach($paidUsers as $key=>$value)
            {
                $percentfreeUsers = $value['totalUsers']/$sum;
                $percent_freeUsers = number_format( $percentfreeUsers * 100 );
             //   $paidUsers[$key]['totalUsers'] = $percent_freeUsers;
                array_push($planName,''.$value['plan_name'].'');
                array_push($totalUsers,$percent_freeUsers);
            }
            return array('code' => 200,'status' => 'success','Users' => $totalUsers,'Plans'=>$planName, 'message' => "Plan Users record fetched");
        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public  function GetCountries()
    {
        try {
            $Countries = $this->conn->query("SELECT DISTINCT(country) as name FROM `users` WHERE status ='1'")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $Countries, 'message' => "Countries fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function GetStateByCountry(){
        try {
            $id = $_POST['id'];
            $States = $this->conn->query("SELECT DISTINCT(state) as name FROM `users` WHERE status ='1' AND country = '$id'")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $States, 'message' => "Countries fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function GetCompaniesAndPlans(){
        try {
            //$id = $_POST['id'];
            $data['Company']= $this->conn->query("SELECT company_name FROM `users` WHERE status ='1' ")->fetchAll();
            $data['Plans']= $this->conn->query("SELECT * FROM `plans` WHERE status ='1' ")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Countries fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getRevenueGenerateUsers(){
        try {
            $data['Revenue']= $this->conn->query("SELECT DISTINCT(users.country),users.id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM `users` WHERE users.country <> 'null' AND users.country <> '' GROUP BY users.country ")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Revenue generated fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

}
$spotlight = new Spotlight();