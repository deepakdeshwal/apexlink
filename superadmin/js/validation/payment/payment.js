
$(document).ready(function () {
    $(document).on("click", "#cancel_payment", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href='/Payment/List';
            }
        });
    });

    $("#hdnFrom").datepicker();
    $("#hdnTo").datepicker();
});

/** Add/Edit new payment
$('#addPaymentForm').on('submit',function(e){
    e.preventDefault();

    var formData = $('#addPaymentForm').serializeArray();
    var hidden_payment_id = $('#hidden_payment_id').val();
    var action ;

    if(hidden_payment_id)
    {
        action = 'update';
    } else {
        action = 'insert';
    }
    $.ajax({
        type: 'post',
        url: '/Payment/PaymentAjax',
        data: {
            class  : 'PaymentAjax',
            action : action,
            form   : formData
        },
        success : function(response){
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                window.location.href='/Payment/List';
                localStorage.setItem("rowcolor", 'add colour');
                // toastr.success('This record saved successfully.');
            } else if(response.status == 'error' && response.code == 400){
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#'+key).text(value);
                });
            }
        }
    });
});

/**  List Action Functions
$(document).on('change', '.select_options', function(){
    var opt = $(this).val();
    var id = $(this).attr('data_id');

    if(opt == 'View' || opt == 'VIEW'){
        window.location.href = '/Payment/ViewPayment/'+id;
    } else if(opt == 'Edit' || opt == 'EDIT'){
        window.location.href = '/Payment/Edit/'+id;
    }else if(opt == 'Deactivate' ||opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE'){

        opt = opt.toLowerCase();
        var data_attr = $(this).find('option:selected').attr('class');
        bootbox.confirm("Are you sure want to "+opt+" this record ?", function(result) {
            if(result == true){
                var status = opt == 'activate' ? '1': '0';

                $.ajax({
                    type: 'post',
                    url: '/Payment/PaymentAjax',
                    data: {
                        class  : 'PaymentAjax',
                        action : 'updateStatus',
                        status : status,
                        id     : id
                    },
                    success : function(response){
                        var response = JSON.parse(response);
                        if(response.status == 'success' && response.code == 200){
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                        $('#Payment-table').trigger( 'reloadGrid' );
                    }
                });
            } else {
                $('#Payment-table').trigger( 'reloadGrid' );
            }
        });

    }else{
    }

});*/
