//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "Please select non-default value.");
//add rule for select drop down
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");

$.validator.addMethod("password_regex", function(value, element) {
    var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
    return password_regex;
}, "* Atleast one aphanumeric and a numeric character required");


//add propertySetup form client side validations
$("#addUserForm").validate({
    rules: {
        first_name: {
            maxlength: 20,
            required:true
        },
        mi: {
            //maxlength: 2
        },
        last_name: {
            maxlength: 20,
            required:true
        },
        maiden_name: {
            //maxlength: 20
        },
        nick_name: {
            //maxlength: 20
        },
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        status: {
            valueNotEquals: "default"
        },
        work_phone: {
           // checkNumber: 12
        },
        country: {
            //maxlength: 20,
            valueNotEquals: "default",
            //required: true
        },
        mobile_number: {
            required:true,
            checkNumber:12
        },
        fax: {
           // checkNumber: 12
        }
    }
});

$("#updateUserForm").validate({
    rules: {
        first_name: {
            maxlength: 20,
            required:true
        },
        last_name: {
            maxlength: 20,
            required:true
        },
        /*email: {
            required: true,
            email: true,
            maxlength: 50
        },*/
        status: {
            valueNotEquals: "default"
        },
        country: {
            required: true,
            valueNotEquals: "default"
        },
        mobile_number: {
            required:true,
            checkNumber:12
        },
    }
});



$(document).ready(function(){
    $(document).on("click","#cancel_modal1 .modal-footer .yes-cancel",function(){
        window.location.href = "/Setting/ManageUser";
    });
});


$( "#change_modal_password" ).validate({
    rules: {
        new_password:{
            required:true,
            minlength: 8,
            password_regex : true
        },
        confirm_password:{
            required:true,
            minlength: 8,
            password_regex : true,
            equalTo :'#new_password'
        }
    },
    messages: {
        newpassword: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirmpassword: {
            equalTo: "* Fields do not match",
            minlength: "* Minimum 8 characters allowed",
        },

    }

});