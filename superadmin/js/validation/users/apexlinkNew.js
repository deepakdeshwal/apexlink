//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "Please select non-default value.");
//add rule for select drop down
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");


//add propertySetup form client side validations
$("#addapexnewform").validate({
    rules: {
        title: {
            required:true,
            maxlength: 150
        },
        date: {
            required:true
        },
        time: {
            required:true
        }
    }
});

//add propertySetup form client side validations
$("#edit_apexlink_new").validate({
    rules: {
        title: {
            required:true,
            maxlength: 150
        },
        date: {
            required:true
        },
        time: {
            required:true
        }
    }
});

$("#updateUserForm").validate({
    rules: {
        first_name: {
            maxlength: 20,
            required:true
        },
        last_name: {
            maxlength: 20,
            required:true
        },
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        status: {
            valueNotEquals: "default"
        },
        country: {
           // maxlength: 20
        },
        mobile_number: {
            required:true,
            checkNumber:12
        },
    }
});
 

