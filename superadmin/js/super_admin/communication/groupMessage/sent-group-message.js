$(function() {
    $('.nav-tabs').responsiveTabs();
});

$("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
$("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});

$(document).ready(function(){
    $(".slide-toggle").click(function(){
        $(".box").animate({
            width: "toggle"
        });
    });
});

// hide on change text message table
$(document).on('click','.groupTxtMsg', function() {

    $(".table1").hide();
    $(".table2").show();
});

$(document).on('click','.groupTxtMail', function() {

    $(".table1").show();
    $(".table2").hide();
});

$(document).ready(function(){
    $(".slide-toggle2").click(function(){
        $(".box2").animate({
            width: "toggle"
        });
    });
    $("#communication_top").addClass("active");
});

jqGrid();
function jqGrid(status) {

    var table = 'communication_email';
    var columns = ['Name','To', 'User','Company','Subject','Date','email_parent_id'];
    var select_column = ['Delete','Open'];
    var joins = [{table:'communication_email',column:'email_to',primary:'email',on_table:'users'}];
    //var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['communication_email.updated_at','communication_email.created_at'];
    var extra_where = [{column: 'type', value: 'G', condition: '='},{column: 'gtype', value: 'E', condition: '='}];
    var status=1;
    var columns_options = [
        {name:'Name',index:'user_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: userType},
        {name:'To',index:'email_to', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: visiblityChecker},
        {name:'User',index:'user_type', width:80, align:"center",table:table,classes: 'pointer',formatter: userType},
        {name:'Company',index:'company_name', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Subject',index:'email_subject', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Date',index:'created_at', width:80, align:"center",table:table},
        {name:'parent',hidden:true,index:'email_parent_id', width:80, align:"center",table:table},
    ];
    var ignore_array = [];
    jQuery("#communication-sent-table-email").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Sent Group Emails",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false}
    );
}

jqGrid2();
function jqGrid2(status) {
    var table = 'communication_email';
    var columns = ['Name','To', 'User', 'Message','email_parent_id', 'Date'];
    var select_column = ['Delete','Open'];
    var joins = [{table:'communication_email',column:'email_to',primary:'email',on_table:'users'}];
    //var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['communication_email.updated_at','communication_email.created_at'];
    var extra_where = [{column: 'type', value: 'G', condition: '='},{column: 'gtype', value: 'T', condition: '='}];
    var status=1;
    var columns_options = [
        {name:'Name',index:'user_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: visiblityChecker},
        {name:'To',index:'email_to', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: visiblityChecker},
        {name:'User',index:'selected_user_type', width:80, align:"center",table:table,classes: 'pointer',formatter: userTypeText},
        {name:'Message',index:'email_subject', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'parent',hidden:true,index:'email_parent_id', width:80, align:"center",table:table},
        {name:'Date',index:'created_at', width:80, align:"center",table:table},
    ];
    var ignore_array = [];
    jQuery("#communication-sent-table-text").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Sent Group Text Messages",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false}
    );
}

function userType(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
            console.log(cellValue);
            switch (cellValue) {
                case '2':
                    user = "Tenant";
                    break;
                case '4':
                    user = "Owner";
                    break;
                case '3':
                    user = "Vendor";
                    break;
                case '5':
                    user = "Other Contacts";
                    break;
                case '6':
                    user = "Guest Card";
                    break;
                case '7':
                    user = "Rental Application";
                case '8':
                    user = "Employee";
                    break;
                default:
                    user = "";


        }
        return user;
    }

}
function userTypeText(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var fieldValue = rowObject.parent;

        if(fieldValue)
        {
            return '';
        } else {
            var user_type = rowObject['User'];

            var user;
            switch (user_type) {
                case '2':
                    user = "Tenant";
                    break;
                case '4':
                    user = "Owner";
                    break;
                case '3':
                    user = "Vendor";
                    break;
                case '5':
                    user = "Other Contacts";
                    break;
                case '6':
                    user = "Guest Card";
                    break;
                case '7':
                    user = "Rental Application";
                case '8':
                    user = "Employee";
                    break;
                default:
                    user = "";
            }
            return user;
        }
    }

}
function deliveryType(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        var status = rowObject.Delivery;
        var fieldValue = rowObject.parent;

        if(fieldValue)
        {
            return '';
        } else {
            var statusV;
            switch (status) {
                case '0':
                    statusV = "<span style='color:red;'>Message Failed</span>";
                    break;
                case '1':
                    statusV = "<span style='color:green;'>Message Delivered Successfully</span>";
                    break;
                case '2':
                    statusV = "Draft";
                    break;
                case '3':
                    statusV = "Archive";
                    break;
                case '4':
                    statusV = "Deleted";
                    break;
                default:
                    statusV = "";
            }
            return statusV;
        }


    }
}

function visiblityChecker(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        var fieldValue = rowObject.parent;

        if(fieldValue)
        {
            return '';
        } else {
            return cellValue;
        }

    }
}

$(document).on('click','#communication-sent-table-email tr td:not(:last-child),#communication-sent-table-text tr td:not(:last-child)',function(e){
    e.preventDefault();

    var compose_mail_id = $(this).closest("tr").attr('id');

    $.ajax({
        url:'/Communication/ComposeMailAjax',
        method: 'post',
        data: {
            class: "PropertyUnitAjax",
            action: "getComposeForView",
            'id': compose_mail_id,
        },
        success: function (data) {
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;

                if(res.email_to)
                {
                    $('.modal_to').text(res.email_to);
                }

                if(res.email_cc)
                {
                    $('.modal_cc').text(res.email_cc);
                }

                if(res.email_bcc)
                {
                    $('.modal_bcc').text(res.email_bcc);
                }
                $('.modal_from').text(res.email_from);
                $('.modal_date').text(res.created_at);
                $('.modal_subject').text(res.email_subject);
                $('.modal_message').html(res.email_message);
                $('#compose_mail_id').val(res.id);
            }
        },

    });

    $('#sentMailModal').modal('show');

    $(document).on('click','#cancel_communication',function () {
        $('#sentMailModal').modal('hide');
    });
    $(document).on('click','.forward-email-btn',function () {
        var compose_mail_id = $('#compose_mail_id').val();
        localStorage.setItem('composer_mail_id',compose_mail_id);
        localStorage.setItem('composer_mail_type','forward');
        window.location.href='/Communication/AddGroupMessage'
    });

    // window.location.href = base_url + '/People/ViewOwner?id='+id;
});


$('.text_tab').hide();

$(document).on('click','.sent_mail',function () {

    $('.email_tab').show();
    $('.text_tab').hide();
});

$(document).on('click','.sent_message',function () {

    $('.email_tab').hide();
    $('.text_tab').show();
})

var gEmailTabSelecttion = localStorage.getItem('gEmailTabSelecttion');
if(gEmailTabSelecttion)
{
    $('.email_tab').hide();
    $('.text_tab').show();
    localStorage.removeItem('gEmailTabSelecttion');
}