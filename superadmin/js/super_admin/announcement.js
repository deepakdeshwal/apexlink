$(document).ready(function () {
    var base_url = window.location.origin;

    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        console.log(selected);
        $('#announcement-table').jqGrid('GridUnload');
        jqGrid(selected);
    });

    //intializing jqGrid
    jqGrid('1');

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var table = 'announcements';
        var columns = ['Announcement Title','Start Date', 'Start Time', 'End Date','End Time','Status','Action'];
        var select_column = ['Edit','Copy','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['announcements.status'];
        var columns_options = [
            {name:'Announcement Title',index:'title', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Start Date',index:'start_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Start Time',index:'start_time', width:80, align:"right",searchoptions: {sopt: conditions},table:table,change_type:'time'},
            {name:'End Date',index:'end_date', width:80, align:"right",searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'End Time',index:'end_time', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'time'},
            {name:'Status',index:'status', width:80,align:"right",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false}

        ];
        var ignore_array = [];
        jQuery("#announcement-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "announcements",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 10,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Announcements",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid");
    }

    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == '0')
            return "InActive";
        else
            return '';
    }

    $(document).on("click", "#announcement_cancel", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                //return; //listing red
              window.location.href =  window.location.origin+'/Announcement/Announcements'
            } else {
                //building redirect
                window.location.href =  window.location.origin+'/Announcement/Announcements'
            }
        });
    });




    $(document).on("change", ".select_options", function (e) {
        var action = this.value;
        var id = $(this).attr('data_id');
        switch(action) {
            case "Edit":
                // redirect to edit announcement page
                window.location.href = base_url+'/Announcement/EditAnnouncement?id='+id;
                break;
            case "Copy":
                // redirect to copy announcement page
                window.location.href = base_url+'/Announcement/CopyAnnouncement?id='+id;
                break;
            case "Delete":
                //delete announcement
                bootbox.confirm("Do you want to delete this announcement?", function (result) {
                    if (result == true) {
                        deleteAnnouncement(id);

                    } else {
                        //return; //listing red
                        window.location.href =  window.location.origin+'/Announcement/Announcements'
                    }
                });

                break;
            default:
                window.location.href = base_url+'/Announcement/Announcements';
        }
    });

    function deleteAnnouncement(id) {
        $.ajax
        ({
            type: 'post',
            url: '/announcement-ajax',
            data: {
                class: "AnnouncementAjax",
                action: "deleteAnnouncement",
                annoucement_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    localStorage.setItem("Message", "Announcement Deleted Successfully")
                    // toastr.success("Announcement Created Successfully.");
                    window.location.href = '/Announcement/Announcements';
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on('click','.clearForm',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                $('#add_announcement')[0].reset();
            }
        });

    });


    $(document).on('click','.ResetForm',function () {
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                location.reload();
            }
        });

    });

});