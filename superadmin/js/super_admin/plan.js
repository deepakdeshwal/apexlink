
$(document).ready(function () {
    $(document).on("click", "#cancel_plan", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href='/Plans/List';
            }
        });
    });
});

/** Add/Edit new plan */
// $('#addPlanForm').on('submit',function(e){
$("#addPlanForm").validate({
    rules: {
        plan_name: {
            required: true
        },
        number_of_units: {
            required: true
        }
    },
    submitHandler: function () {

        var no_of_units = $('#number_of_units').val();

        if (no_of_units == 1000) {
            toastr.warning('Please call at 800-467-0814 for quote.');
        } else {
            var formData = $('#addPlanForm').serializeArray();
            var hidden_plan_id = $('#hidden_plan_id').val();
            var action;

            if (hidden_plan_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/Plans/PlanAjax',
                data: {
                    class: 'PlanAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.code == 2002){
                      toastr.warning('Plan Name Already exist');
                    }
                    if (response.status == 'success' && response.code == 200) {
                        window.location.href = '/Plans/List';
                        localStorage.setItem("rowcolor", 'add colour');
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    }
                }
            });
        }
    }
});

/**  List Action Functions  */
$(document).on('change', '.select_options', function(){
    setTimeout(function(){ $(".select_options").val("default"); }, 200);
    var opt = $(this).val();
    var id = $(this).attr('data_id');

    if(opt == 'View' || opt == 'VIEW'){
        window.location.href = '/Plans/ViewPlan/'+id;
    } else if(opt == 'Edit' || opt == 'EDIT'){
        $.ajax({
            type: 'post',
            url: '/Plans/PlanAjax',
            data: {
                class  : 'PlanAjax',
                action : 'checkPlanInUse',
                id     : id
            },
            success : function(response){
                response = $.parseJSON(response);
                if(response.status == 'success' && response.code == 200){
                    toastr.error(response.message);
                } else {
                    window.location.href = '/Plans/Edit/'+id;
                }
            }
        });
    }else if(opt == 'Deactivate' ||opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE'){
        opt = opt.toLowerCase();
        var data_attr = $(this).find('option:selected').attr('class');
        bootbox.confirm("Are you sure want to "+opt+" this record ?", function(result) {
            if(result == true){
                var status = opt == 'activate' ? '1': '0';

                $.ajax({
                    type: 'post',
                    url: '/Plans/PlanAjax',
                    data: {
                        class  : 'PlanAjax',
                        action : 'updateStatus',
                        status : status,
                        id     : id
                    },
                    success : function(response){
                        var response = JSON.parse(response);
                        if(response.status == 'success' && response.code == 200){
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                        $('#Plans-table').trigger( 'reloadGrid' );
                    }
                });
            } else {
                $('#Plans-table').trigger( 'reloadGrid' );
            }
        });

    }else{
    }


});

$(document).on('click','.clearForm',function () {
   $('#addPlanForm')[0].reset();
});

$(document).on('click','.ResetForm',function () {
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});

