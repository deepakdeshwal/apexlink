jQuery(document).ready(function($) {
    $( 'input,textarea' ).not('.bootstrap-tagsinput input,input[type="emergency_email"],input[type="password"],input[type="email"],input[type="image"],input[name="email"],input[name="email[]"],input[name="femail"],input[name="account_admin_email_address"],input[name="support_email"],input[name="additional_email[]"],input[name="emergency_email[]"],input[name="other_email"],input[name="newpassword"],input[name="confirmpassword"]').addClass("capital");
$('.number_only').removeClass('capital');
    if (localStorage.getItem("Message")) {
        var message = localStorage.getItem("Message");
        toastr.success(message);
        localStorage.removeItem('Message');
    }
    if (localStorage.getItem("rowcolor")) {
        setTimeout(function() {
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            localStorage.removeItem('rowcolor');
        }, 700);
    }
    if (localStorage.getItem("apexlink_new_row_colour")) {
        setTimeout(function() {
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
        }, 300);
        localStorage.removeItem('rowcolor');
    }
    $(document).on('keydown', '.capital', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });


    $(document).on('keydown', '.number_only', function (e) {
        if (e.which != 8 && e.which != 110 && e.which != 190 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    $(document).on('keydown', '.superadmin_phoneformat', function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=9) {
            return false;
        }
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    systemLogout();

});


function systemLogout(){
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: { class: 'DefaultSettingsAjax', action: 'systemLogout'},
        success: function (response) {
            var data = $.parseJSON(response);
            var IDLE_TIMEOUT = data*60; //seconds
            var _idleSecondsTimer = null;
            var _idleSecondsCounter = 0;
            document.onclick = function() {
                _idleSecondsCounter = 0;
            };

            document.onmousemove = function() {
                _idleSecondsCounter = 0;
            };

            document.onkeypress = function() {
                _idleSecondsCounter = 0;
            };
            _idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);
            function CheckIdleTime() {
                _idleSecondsCounter++;
                var oPanel = document.getElementById("SecondsUntilExpire");
                if (oPanel) oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
                if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                    window.clearInterval(_idleSecondsTimer);
                    $.ajax
                    ({
                        type: 'post',
                        url: '/settings-ajax',
                        data: {
                            class: "DefaultSettingsAjax",
                            action: "logout",
                            data :window.location.pathname+window.location.search
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success")
                            {
                                localStorage.removeItem("announcement_ids");
                                window.location.href = "/";
                            } 
                            else
                            {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                }
            }

        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function onTop(rowdata) {
    if (rowdata) {
        setTimeout(function() {
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
        }, 300);
    }
}


function changeToFloat(amount){
    if(amount != '' && amount.indexOf(".") == -1) {
        var bef = amount.replace(/,/g, '');
        var value = numberWithCommas(addZeroes(bef));
        return value;
    } else {
        var bef = amount.replace(/,/g, '');
        return numberWithCommas(addZeroes(bef));
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function addZeroes(num) {
    // Convert input string to a number and store as a variable.
    var value = Math.round(Number(num) * 100) / 100;
    // Split the input string into two arrays containing integers/decimals
    var res = num.split(".");
    // If there is no decimal point or only one decimal place found.
    if(res.length == 1 || res[1].length < 3) {
        // Set the number to two decimal places
        value = value.toFixed(2);
    }
    // Return updated or original number.
    return value;
}


setTimeout(function(){
    jQuery('.ui-jqgrid-btable tbody tr td').css('text-align','left');
},100);

// $(document).on('click', '.help-menu-apex', function () {
//    alert('hjhj');
// });
//
// jQuery(function() {
//     jQuery('.help-menu-apex').on('click',function(e) {
//         e.preventDefault();
//         jQuery(this).next('.submenus-apex').toggle();
//     });
// });

// $("#help_id").click(function(){$("#helpThirds").modal('show')});

$(".clsSuggestion").click(function(){$("#helpThirds").modal('show')});