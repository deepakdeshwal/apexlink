jqGrid('All');
/**
 * jqGrid Initialization function
 * @param status
 */
function jqGrid(status) {
    var table = 'transactions';
    var columns = ['Transaction ID','Date','Payer Name','Plan','Amount($)'];
    var extra_dropdown = [];
    var select_column = [];
    var ignore_array = [];
    var joins = [{table:'transactions',column:'user_id',primary:'id',on_table:'users'},{table:'transactions',column:'plan_id',primary:'id',on_table:'plans'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'user_type',value:'PM',condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name:'Transaction ID',index:'id', width:200,searchoptions: {sopt: conditions},table:table,classes:'cursor'},
        {name:'Date',index:'created_at', width:200,align:"center",searchoptions: {sopt: conditions},table:table,classes:'cursor'},
        {name:'Payer Name',index:'name', width:200,align:"center",searchoptions: {sopt: conditions},table:'users',classes:'cursor'},
        {name:'Plan',index:'plan_name' ,width:200,align:"center",searchoptions: {sopt: conditions},table:'plans',formatter:planFormatter},
        {name:'Amount($)',index:'total_charge_amount',width:200,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'number_format',formatter:priceFormatter}
    ];
    jQuery("#Payment-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'no',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'transactions.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 30,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Payments",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}



function priceFormatter(cellValue, options, rowObject){
    return '$'+cellValue;
}

function planFormatter(cellValue, options, rowObject){
    return cellValue+' Plan';
}
//
$("#hdnFrom").datepicker({
onClose: function (selectedDate) {
    $(".ins_end_date").datepicker("option", "minDate", selectedDate);
}}).datepicker("setDate", new Date());

$("#hdnTo").datepicker({
onClose: function (selectedDate) {
    $(".ins_end_date").datepicker("option", "minDate", selectedDate);
}}).datepicker("setDate", new Date());

$(document).on('change','.dateFilter',function(){
    var grid = $("#Payment-table"),f = [];
    var from = $('#hdnFrom').val();
    var to = $('#hdnTo').val();
    f.push({field: "transactions.created_at", op: "dateBetween", data: from, data2:to});
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
});