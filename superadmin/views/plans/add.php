<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}

include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
<!-- HTML Start -->


<div id="wrapper">

    <!-- Top navigation start -->
    <?php include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/top_navigation.php");?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-data apxpg-allcontent">
                        <form action="" method="POST" id="addPlanForm">
                            <!--single add-form-box-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Add New Plan</strong>
                                    <a class="back right" href="/Plans/List"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                </div>
                                <div class="apx-adformbox-content">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Plan Name <em class="red-star">*</em></label>
                                                <input class="form-control" placeholder="Plan Name" type="text" name="plan_name" id="plan_name" value="">
                                                <span class="error" id="plan_nameErr"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Number of Units <em class="red-star">*</em> </label>
                                                <select class="form-control" name="number_of_units" id="number_of_units">
                                                    <option value="">Select</option>
                                                    <option value="1-25">1-25</option>
                                                    <option value="26-50">26-50</option>
                                                    <option value="51-100">51-100</option>
                                                    <option value="101-200">101-200</option>
                                                    <option value="201-300">201-300</option>
                                                    <option value="301-400">301-400</option>
                                                    <option value="401-500">401-500</option>
                                                    <option value="501-600">501-600</option>
                                                    <option value="601-700">601-700</option>
                                                    <option value="701-800">701-800</option>
                                                    <option value="801-900">801-900</option>
                                                    <option value="901-1000">901-1000</option>
                                                    <option value="1000">More than 1000 units</option>
                                                </select>
                                                <span class="error" id="number_of_unitsErr"></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End single add-form-box-->

                            <div class="apex-btn-block text-right">
                                <input type="submit" name="save" value="Save" class="blue-btn" id="savePlanBtnId"/>
                                <input type='button'  value="Clear" class="clear-btn clearForm" >
                                <input  type="button" value="Cancel" class="grey-btn" id="cancel_plan"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<script src="<?php echo SUPERADMIN_SITE_URL ?>/js/super_admin/plan.js"></script>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<script>
    $('.plan-top').addClass('active');
    $(document).on('change', '#number_of_units', function() {
        var opt = $(this).val();
        if (opt == 1000){
            toastr.warning('Please call at 800-467-0814 for quote.');
        }
    });

</script>

</body>
</html>
<!-- HTML END -->


