<div class="modal fade" id="changepassword" role="dialog">
    <div class="modal-dialog modal-md">
        <form id="change_modal_password">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-sm-11">
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="close modal_changepass" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                <div class="modal-body">
                
                    <input type="hidden" name="id">
                    <input type="hidden" name="email">
                    <label>New Password:</label>
                    <input type="password" name="new_password" id="new_password" class="form-control">
                    <span id="npasswordErr"></span>
                    <br><br>
                    <label>Confirm Password:</label>
                    <input type="password" name="confirm_password" class="form-control">
                    <span id="passwordErr"></span>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="blue-btn change_password">Update</button>
                </div>
            </div>
        </form>

    </div>
</div>

<div class="modal fade" id="cancel_modal1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">
                <p><img src="<?php echo SUPERADMIN_SITE_URL.'/images/notice.png';?>"> Do you want to cancel this action now?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:;"><input type="button" class="btn btn-default yes-cancel" value="Yes"></a>
                <button type="button" class="btn btn-default no-cancel" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cancel_modal_add" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">
                <p><img src="<?php echo SUPERADMIN_SITE_URL.'/images/notice.png';?>"> Do you want to cancel this action now?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:;"><input type="button" class="btn btn-default yes-cancel" value="Yes"></a>
                <button type="button" class="btn btn-default no-cancel" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>