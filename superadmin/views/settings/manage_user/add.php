<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php"); ?>
<?php include_once(ROOT_URL . "/superadmin/views/settings/manage_user/modal.php");?>
<!-- HTML Start -->
<?php
global $countryArray;
$countryArray = array(
    'AB' => array('name' => 'Abkhazia', 'code' => '7840 '),
    'AD' => array('name' => 'ANDORRA', 'code' => '376'),
    'AF' => array('name' => 'AFGHANISTAN', 'code' => '93'),
    'AG' => array('name' => 'ANTIGUA AND BARBUDA', 'code' => '1268'),
    'AI' => array('name' => 'ANGUILLA', 'code' => '1264'),
    'AL' => array('name' => 'ALBANIA', 'code' => '355'),
    'AM' => array('name' => 'ARMENIA', 'code' => '374'),
    'AN' => array('name' => 'NETHERLANDS ANTILLES', 'code' => '599'),
    'AO' => array('name' => 'ANGOLA', 'code' => '244'),
    'AQ' => array('name' => 'ANTARCTICA', 'code' => '672'),
    'AR' => array('name' => 'ARGENTINA', 'code' => '54'),
    'AS' => array('name' => 'AMERICAN SAMOA', 'code' => '1684'),
    'AT' => array('name' => 'AUSTRIA', 'code' => '43'),
    'AU' => array('name' => 'AUSTRALIA', 'code' => '61'),
    'AW' => array('name' => 'ARUBA', 'code' => '297'),
    'AZ' => array('name' => 'AZERBAIJAN', 'code' => '994'),
    'BA' => array('name' => 'BOSNIA AND HERZEGOVINA', 'code' => '387'),
    'BB' => array('name' => 'BARBADOS', 'code' => '1246'),
    'BD' => array('name' => 'BANGLADESH', 'code' => '880'),
    'BE' => array('name' => 'BELGIUM', 'code' => '32'),
    'BF' => array('name' => 'BURKINA FASO', 'code' => '226'),
    'BG' => array('name' => 'BULGARIA', 'code' => '359'),
    'AE' => array('name' => 'UNITED ARAB EMIRATES', 'code' => '971'),
    'BH' => array('name' => 'BAHRAIN', 'code' => '973'),
    'BI' => array('name' => 'BURUNDI', 'code' => '257'),
    'BJ' => array('name' => 'BENIN', 'code' => '229'),
    'BL' => array('name' => 'SAINT BARTHELEMY', 'code' => '590'),
    'BM' => array('name' => 'BERMUDA', 'code' => '1441'),
    'BN' => array('name' => 'BRUNEI DARUSSALAM', 'code' => '673'),
    'BO' => array('name' => 'BOLIVIA', 'code' => '591'),
    'BR' => array('name' => 'BRAZIL', 'code' => '55'),
    'BS' => array('name' => 'BAHAMAS', 'code' => '1242'),
    'BT' => array('name' => 'BHUTAN', 'code' => '975'),
    'BW' => array('name' => 'BOTSWANA', 'code' => '267'),
    'BY' => array('name' => 'BELARUS', 'code' => '375'),
    'BZ' => array('name' => 'BELIZE', 'code' => '501'),
    'CA' => array('name' => 'CANADA', 'code' => '1'),
    'CC' => array('name' => 'COCOS (KEELING) ISLANDS', 'code' => '61'),
    'CD' => array('name' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'code' => '243'),
    'CF' => array('name' => 'CENTRAL AFRICAN REPUBLIC', 'code' => '236'),
    'CG' => array('name' => 'CONGO', 'code' => '242'),
    'CH' => array('name' => 'SWITZERLAND', 'code' => '41'),
    'CI' => array('name' => 'COTE D IVOIRE', 'code' => '225'),
    'CK' => array('name' => 'COOK ISLANDS', 'code' => '682'),
    'CL' => array('name' => 'CHILE', 'code' => '56'),
    'CM' => array('name' => 'CAMEROON', 'code' => '237'),
    'CN' => array('name' => 'CHINA', 'code' => '86'),
    'CO' => array('name' => 'COLOMBIA', 'code' => '57'),
    'CR' => array('name' => 'COSTA RICA', 'code' => '506'),
    'CU' => array('name' => 'CUBA', 'code' => '53'),
    'CV' => array('name' => 'CAPE VERDE', 'code' => '238'),
    'CX' => array('name' => 'CHRISTMAS ISLAND', 'code' => '61'),
    'CY' => array('name' => 'CYPRUS', 'code' => '357'),
    'CZ' => array('name' => 'CZECH REPUBLIC', 'code' => '420'),
    'DE' => array('name' => 'GERMANY', 'code' => '49'),
    'DJ' => array('name' => 'DJIBOUTI', 'code' => '253'),
    'DK' => array('name' => 'DENMARK', 'code' => '45'),
    'DM' => array('name' => 'DOMINICA', 'code' => '1767'),
    'DO' => array('name' => 'DOMINICAN REPUBLIC', 'code' => '1809'),
    'DZ' => array('name' => 'ALGERIA', 'code' => '213'),
    'EC' => array('name' => 'ECUADOR', 'code' => '593'),
    'EE' => array('name' => 'ESTONIA', 'code' => '372'),
    'EG' => array('name' => 'EGYPT', 'code' => '20'),
    'ER' => array('name' => 'ERITREA', 'code' => '291'),
    'ES' => array('name' => 'SPAIN', 'code' => '34'),
    'ET' => array('name' => 'ETHIOPIA', 'code' => '251'),
    'FI' => array('name' => 'FINLAND', 'code' => '358'),
    'FJ' => array('name' => 'FIJI', 'code' => '679'),
    'FK' => array('name' => 'FALKLAND ISLANDS (MALVINAS)', 'code' => '500'),
    'FM' => array('name' => 'MICRONESIA, FEDERATED STATES OF', 'code' => '691'),
    'FO' => array('name' => 'FAROE ISLANDS', 'code' => '298'),
    'FR' => array('name' => 'FRANCE', 'code' => '33'),
    'GA' => array('name' => 'GABON', 'code' => '241'),
    'GB' => array('name' => 'UNITED KINGDOM', 'code' => '44'),
    'GD' => array('name' => 'GRENADA', 'code' => '1473'),
    'GE' => array('name' => 'GEORGIA', 'code' => '995'),
    'GH' => array('name' => 'GHANA', 'code' => '233'),
    'GI' => array('name' => 'GIBRALTAR', 'code' => '350'),
    'GL' => array('name' => 'GREENLAND', 'code' => '299'),
    'GM' => array('name' => 'GAMBIA', 'code' => '220'),
    'GN' => array('name' => 'GUINEA', 'code' => '224'),
    'GQ' => array('name' => 'EQUATORIAL GUINEA', 'code' => '240'),
    'GR' => array('name' => 'GREECE', 'code' => '30'),
    'GT' => array('name' => 'GUATEMALA', 'code' => '502'),
    'GU' => array('name' => 'GUAM', 'code' => '1671'),
    'GW' => array('name' => 'GUINEA-BISSAU', 'code' => '245'),
    'GY' => array('name' => 'GUYANA', 'code' => '592'),
    'HK' => array('name' => 'HONG KONG', 'code' => '852'),
    'HN' => array('name' => 'HONDURAS', 'code' => '504'),
    'HR' => array('name' => 'CROATIA', 'code' => '385'),
    'HT' => array('name' => 'HAITI', 'code' => '509'),
    'HU' => array('name' => 'HUNGARY', 'code' => '36'),
    'ID' => array('name' => 'INDONESIA', 'code' => '62'),
    'IE' => array('name' => 'IRELAND', 'code' => '353'),
    'IL' => array('name' => 'ISRAEL', 'code' => '972'),
    'IM' => array('name' => 'ISLE OF MAN', 'code' => '44'),
    'IN' => array('name' => 'INDIA', 'code' => '91'),
    'IQ' => array('name' => 'IRAQ', 'code' => '964'),
    'IR' => array('name' => 'IRAN, ISLAMIC REPUBLIC OF', 'code' => '98'),
    'IS' => array('name' => 'ICELAND', 'code' => '354'),
    'IT' => array('name' => 'ITALY', 'code' => '39'),
    'JM' => array('name' => 'JAMAICA', 'code' => '1876'),
    'JO' => array('name' => 'JORDAN', 'code' => '962'),
    'JP' => array('name' => 'JAPAN', 'code' => '81'),
    'KE' => array('name' => 'KENYA', 'code' => '254'),
    'KG' => array('name' => 'KYRGYZSTAN', 'code' => '996'),
    'KH' => array('name' => 'CAMBODIA', 'code' => '855'),
    'KI' => array('name' => 'KIRIBATI', 'code' => '686'),
    'KM' => array('name' => 'COMOROS', 'code' => '269'),
    'KN' => array('name' => 'SAINT KITTS AND NEVIS', 'code' => '1869'),
    'KP' => array('name' => 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF', 'code' => '850'),
    'KR' => array('name' => 'KOREA REPUBLIC OF', 'code' => '82'),
    'KW' => array('name' => 'KUWAIT', 'code' => '965'),
    'KY' => array('name' => 'CAYMAN ISLANDS', 'code' => '1345'),
    'KZ' => array('name' => 'KAZAKSTAN', 'code' => '7'),
    'LA' => array('name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'code' => '856'),
    'LB' => array('name' => 'LEBANON', 'code' => '961'),
    'LC' => array('name' => 'SAINT LUCIA', 'code' => '1758'),
    'LI' => array('name' => 'LIECHTENSTEIN', 'code' => '423'),
    'LK' => array('name' => 'SRI LANKA', 'code' => '94'),
    'LR' => array('name' => 'LIBERIA', 'code' => '231'),
    'LS' => array('name' => 'LESOTHO', 'code' => '266'),
    'LT' => array('name' => 'LITHUANIA', 'code' => '370'),
    'LU' => array('name' => 'LUXEMBOURG', 'code' => '352'),
    'LV' => array('name' => 'LATVIA', 'code' => '371'),
    'LY' => array('name' => 'LIBYAN ARAB JAMAHIRIYA', 'code' => '218'),
    'MA' => array('name' => 'MOROCCO', 'code' => '212'),
    'MC' => array('name' => 'MONACO', 'code' => '377'),
    'MD' => array('name' => 'MOLDOVA, REPUBLIC OF', 'code' => '373'),
    'ME' => array('name' => 'MONTENEGRO', 'code' => '382'),
    'MF' => array('name' => 'SAINT MARTIN', 'code' => '1599'),
    'MG' => array('name' => 'MADAGASCAR', 'code' => '261'),
    'MH' => array('name' => 'MARSHALL ISLANDS', 'code' => '692'),
    'MK' => array('name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'code' => '389'),
    'ML' => array('name' => 'MALI', 'code' => '223'),
    'MM' => array('name' => 'MYANMAR', 'code' => '95'),
    'MN' => array('name' => 'MONGOLIA', 'code' => '976'),
    'MO' => array('name' => 'MACAU', 'code' => '853'),
    'MP' => array('name' => 'NORTHERN MARIANA ISLANDS', 'code' => '1670'),
    'MR' => array('name' => 'MAURITANIA', 'code' => '222'),
    'MS' => array('name' => 'MONTSERRAT', 'code' => '1664'),
    'MT' => array('name' => 'MALTA', 'code' => '356'),
    'MU' => array('name' => 'MAURITIUS', 'code' => '230'),
    'MV' => array('name' => 'MALDIVES', 'code' => '960'),
    'MW' => array('name' => 'MALAWI', 'code' => '265'),
    'MX' => array('name' => 'MEXICO', 'code' => '52'),
    'MY' => array('name' => 'MALAYSIA', 'code' => '60'),
    'MZ' => array('name' => 'MOZAMBIQUE', 'code' => '258'),
    'NA' => array('name' => 'NAMIBIA', 'code' => '264'),
    'NC' => array('name' => 'NEW CALEDONIA', 'code' => '687'),
    'NE' => array('name' => 'NIGER', 'code' => '227'),
    'NG' => array('name' => 'NIGERIA', 'code' => '234'),
    'NI' => array('name' => 'NICARAGUA', 'code' => '505'),
    'NL' => array('name' => 'NETHERLANDS', 'code' => '31'),
    'NO' => array('name' => 'NORWAY', 'code' => '47'),
    'NP' => array('name' => 'NEPAL', 'code' => '977'),
    'NR' => array('name' => 'NAURU', 'code' => '674'),
    'NU' => array('name' => 'NIUE', 'code' => '683'),
    'NZ' => array('name' => 'NEW ZEALAND', 'code' => '64'),
    'OM' => array('name' => 'OMAN', 'code' => '968'),
    'PA' => array('name' => 'PANAMA', 'code' => '507'),
    'PE' => array('name' => 'PERU', 'code' => '51'),
    'PF' => array('name' => 'FRENCH POLYNESIA', 'code' => '689'),
    'PG' => array('name' => 'PAPUA NEW GUINEA', 'code' => '675'),
    'PH' => array('name' => 'PHILIPPINES', 'code' => '63'),
    'PK' => array('name' => 'PAKISTAN', 'code' => '92'),
    'PL' => array('name' => 'POLAND', 'code' => '48'),
    'PM' => array('name' => 'SAINT PIERRE AND MIQUELON', 'code' => '508'),
    'PN' => array('name' => 'PITCAIRN', 'code' => '870'),
    'PR' => array('name' => 'PUERTO RICO', 'code' => '1'),
    'PT' => array('name' => 'PORTUGAL', 'code' => '351'),
    'PW' => array('name' => 'PALAU', 'code' => '680'),
    'PY' => array('name' => 'PARAGUAY', 'code' => '595'),
    'QA' => array('name' => 'QATAR', 'code' => '974'),
    'RO' => array('name' => 'ROMANIA', 'code' => '40'),
    'RS' => array('name' => 'SERBIA', 'code' => '381'),
    'RU' => array('name' => 'RUSSIAN FEDERATION', 'code' => '7'),
    'RW' => array('name' => 'RWANDA', 'code' => '250'),
    'SA' => array('name' => 'SAUDI ARABIA', 'code' => '966'),
    'SB' => array('name' => 'SOLOMON ISLANDS', 'code' => '677'),
    'SC' => array('name' => 'SEYCHELLES', 'code' => '248'),
    'SD' => array('name' => 'SUDAN', 'code' => '249'),
    'SE' => array('name' => 'SWEDEN', 'code' => '46'),
    'SG' => array('name' => 'SINGAPORE', 'code' => '65'),
    'SH' => array('name' => 'SAINT HELENA', 'code' => '290'),
    'SI' => array('name' => 'SLOVENIA', 'code' => '386'),
    'SK' => array('name' => 'SLOVAKIA', 'code' => '421'),
    'SL' => array('name' => 'SIERRA LEONE', 'code' => '232'),
    'SM' => array('name' => 'SAN MARINO', 'code' => '378'),
    'SN' => array('name' => 'SENEGAL', 'code' => '221'),
    'SO' => array('name' => 'SOMALIA', 'code' => '252'),
    'SR' => array('name' => 'SURINAME', 'code' => '597'),
    'ST' => array('name' => 'SAO TOME AND PRINCIPE', 'code' => '239'),
    'SV' => array('name' => 'EL SALVADOR', 'code' => '503'),
    'SY' => array('name' => 'SYRIAN ARAB REPUBLIC', 'code' => '963'),
    'SZ' => array('name' => 'SWAZILAND', 'code' => '268'),
    'TC' => array('name' => 'TURKS AND CAICOS ISLANDS', 'code' => '1649'),
    'TD' => array('name' => 'CHAD', 'code' => '235'),
    'TG' => array('name' => 'TOGO', 'code' => '228'),
    'TH' => array('name' => 'THAILAND', 'code' => '66'),
    'TJ' => array('name' => 'TAJIKISTAN', 'code' => '992'),
    'TK' => array('name' => 'TOKELAU', 'code' => '690'),
    'TL' => array('name' => 'TIMOR-LESTE', 'code' => '670'),
    'TM' => array('name' => 'TURKMENISTAN', 'code' => '993'),
    'TN' => array('name' => 'TUNISIA', 'code' => '216'),
    'TO' => array('name' => 'TONGA', 'code' => '676'),
    'TR' => array('name' => 'TURKEY', 'code' => '90'),
    'TT' => array('name' => 'TRINIDAD AND TOBAGO', 'code' => '1868'),
    'TV' => array('name' => 'TUVALU', 'code' => '688'),
    'TW' => array('name' => 'TAIWAN, PROVINCE OF CHINA', 'code' => '886'),
    'TZ' => array('name' => 'TANZANIA, UNITED REPUBLIC OF', 'code' => '255'),
    'UA' => array('name' => 'UKRAINE', 'code' => '380'),
    'UG' => array('name' => 'UGANDA', 'code' => '256'),
    'US' => array('name' => 'UNITED STATES', 'code' => '1'),
    'UY' => array('name' => 'URUGUAY', 'code' => '598'),
    'UZ' => array('name' => 'UZBEKISTAN', 'code' => '998'),
    'VA' => array('name' => 'HOLY SEE (VATICAN CITY STATE)', 'code' => '39'),
    'VC' => array('name' => 'SAINT VINCENT AND THE GRENADINES', 'code' => '1784'),
    'VE' => array('name' => 'VENEZUELA', 'code' => '58'),
    'VG' => array('name' => 'VIRGIN ISLANDS, BRITISH', 'code' => '1284'),
    'VI' => array('name' => 'VIRGIN ISLANDS, U.S.', 'code' => '1340'),
    'VN' => array('name' => 'VIET NAM', 'code' => '84'),
    'VU' => array('name' => 'VANUATU', 'code' => '678'),
    'WF' => array('name' => 'WALLIS AND FUTUNA', 'code' => '681'),
    'WS' => array('name' => 'SAMOA', 'code' => '685'),
    'XK' => array('name' => 'KOSOVO', 'code' => '381'),
    'YE' => array('name' => 'YEMEN', 'code' => '967'),
    'YT' => array('name' => 'MAYOTTE', 'code' => '262'),
    'ZA' => array('name' => 'SOUTH AFRICA', 'code' => '27'),
    'ZM' => array('name' => 'ZAMBIA', 'code' => '260'),
    'ZW' => array('name' => 'ZIMBABWE', 'code' => '263')
);
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar start-->
                <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/sidebar.php"); ?>
                <!-- Sidebar end -->
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018
                            </div>
                            <div class="col-xs-3">
                                <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content-rt">

                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Admin &gt;&gt; <span>Manage User</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#default-settings" aria-controls="home" role="tab" data-toggle="tab">Manage User</a></li>
                                    <li role="presentation"><a href="#clock-settings" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="default-settings">

                                        <!--single add-form-box-->
                                        <div class="apx-adformbox shadow">
                                            <div class="apx-adformbox-title">
                                                <strong class="left">New User</strong>

                                            </div>

                                        <form method="post" id="addUserForm">
                                            <div class="apx-adformbox-content">
                                                <div class="row">
                                                        <div class="form-outer">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>First Name <em class="red-star">*</em></label>
                                                            <input class="form-control" placeholder="Eg: Jason" name="first_name" type="text"/>
                                                            <span class="first_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>MI</label>
                                                            <input class="form-control" name="mi" type="text" maxlength="1"/>
                                                            
                                                            <span class="miErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Last Name <em class="red-star">*</em></label>
                                                            <input class="form-control" placeholder="Eg: Holder" name="last_name" type="text"/>
                                                            <span class="last_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Maiden Name</label>
                                                            <input class="form-control" name="maiden_name" type="text"/>
                                                            <span class="maiden_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Nick Name</label>
                                                            <input class="form-control" name="nick_name" type="text"/>
                                                            <span class="nick_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Email <em class="red-star">*</em></label>
                                                            <input class="form-control" placeholder="Eg: jholder@gmail.com" name="email" type="text"/>
                                                            <span class="emailErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Status <em class="red-star">*</em></label>
                                                            <select class="form-control" name="status">
                                                                <option value="default">Select</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">InActive</option>
                                                            </select>
                                                            <span class="statusErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Work Phone</label>
                                                            <input maxlength="12" placeholder="Eg: 154-175-4301" class="form-control number_format" name="work_phone" type="text"/>
                                                            <span class="work_phoneErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Home Phone</label>
                                                            <input maxlength="12" placeholder="Eg: 154-175-4301" class="form-control number_format" name="home_phone" type="text"/>
                                                            <span class="home_phoneErr error red-star"></span>
                                                        </div>
                                                        <!--<div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Country</label>
                                                            <select class="form-control" name="country" id="countryCode">
                                                                <option value="default">Select Country Code</option>
                                                            </select>
                                                            <span class="countryErr error"></span>
                                                        </div>-->
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Country Code <em class="red-star">*</em></label>
                                                            <select class='form-control' name="country" id="countryCode">
                                                                <option value="default">Select Country Code</option>
                                                                <?php
                                                                foreach ($countryArray as $code => $country) {
                                                                    $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                    echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $countryName . " (+" . $country["code"] . ")</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <span class="countryErr error"></span>
                                                            <span class="country_codeErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Mobile Number <em class="red-star">*</em></label>
                                                            <input maxlength="12" placeholder="Eg: 154-175-4301" class="form-control number_format" name="mobile_number" type="text"/>
                                                            <span class="mobile_numberErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Fax</label>
                                                            <input maxlength="12" placeholder="Eg: 4032972706" class="form-control number_format" name="fax" type="text"/>
                                                            <span class="faxErr error red-star"></span>
                                                        </div>
                                                        </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--End single add-form-box-->

                                        <div class="btn-outer apex-btn-block left text-right">
                                            <a href="javascript:;"><input type="submit" class="blue-btn" value="Save"></a>
                                            <!--<input type='button' id="cancel_frm" value="Cancel" class="blue-btn" style="background: #563503 !important">-->
                                            <input type='button'  value="Clear" class="clear-btn clearFormReset" >
                                            <!--<button class="grey-btn">Cancel</button>-->
                                            <a href="javascript:;">
                                                <!--<input type="button" class="grey-btn cancel_action" data-toggle="modal" data-target="#cancel_modal_add" data-backdrop="static" data-keyboard="false" value="Cancel">-->
                                                <input type="button" class="grey-btn yes-cancel-time" value="Cancel">
                                            </a>
                                        </div>
                                    </form>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="clock-settings">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="title-color">Change Password</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <form id="change_pass_form" name="change_pass_form" method="post">
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>Current Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" id="current_password" name="password" class="form-control" placeholder="Type in your current password">
                                                                                    <span class="password"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>New Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" name="newpassword" class="form-control" placeholder="Type in your new password" id="newpassword2">
                                                                                    <span class="newpassword"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                    <label>Confirm New Password <em class="red-star">*</em></label>
                                                                                    <input type="password" maxlength="25" name="confirmpassword" class="form-control" placeholder="Please confirm your new password">
                                                                                    <span class="confirmpassword"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn changePassBtn">Update</button>
                                                                    <input type='button'  value="Reset" class="clear-btn FormReset" >
                                                                    <button class="grey-btn yes-cancel-time"  onclick="resetForm();">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content End-->
    </main>
</div>
<?php include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php"); ?>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/users/manageUser.js" type="text/javascript"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/manageUsers.js" type="text/javascript"></script>
<script>

</script>
<script>
    $('#leftnav2').addClass('in');
    $('.manage_user').addClass('active');


    function resetForm() {
        document.getElementById("change_pass_form").reset();
        var validator = $( "#change_pass_form" ).validate();
        validator.resetForm();
    }
</script>

</body>
</html>
