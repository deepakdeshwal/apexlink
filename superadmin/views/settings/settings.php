<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/24/2019
 * Time: 12:00 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_header.php");
include_once(SUPERADMIN_DIRECTORY_URL . "/views/settings/modal.php");
?>
<!-- HTML Start -->

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
        <!-- Content Start-->
        <section class="main-content super-admin">
            <div class="container-fluid">
                <div class="row flex">
                    <!-- Sidebar start-->
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/sidebar.php");
                    ?>
                    <!-- Sidebar end -->
                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="welcome-text visible-xs">
                            <div class="welcome-text-inner">
                                <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018
                                </div>
                                <div class="col-xs-3">
                                    <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                    <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content-rt">


                            <div class="bread-search-outer">
                                <div class="col-md-8">
                                    <div class="breadcrumb-outer">
                                        Admin >> <span>Default Settings</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text"/>
                                    </div>
                                </div>

                            </div>

                            <div class="content-data">

                                <!-- Main tabs -->
                                <div class="main-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#default-settings" aria-controls="home" role="tab" data-toggle="tab">Default Settings</a></li>
                                        <li role="presentation"><a href="#clock-settings" aria-controls="profile" role="tab" data-toggle="tab">Default Date and Clock Settings</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="default-settings">
                                            <!--single add-form-box-->
                                            <form id="settingsForm" method="post">
                                                <input type="hidden" id="id" name="id" value="0">
                                                <div class="apx-adformbox shadow">
                                                    <div class="apx-adformbox-title">
                                                        <strong class="left">Default Settings</strong>

                                                    </div>
                                                    <div class="apx-adformbox-content">

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Company Name <em class="red-star">*</em></label>
                                                                    <input id="company_name" name="company_name" class="form-control" type="text"/>
                                                                    <span class="company_nameErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Time Out</label>
                                                                    <select id="timeout" name="timeout" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="15">15 Minutes</option>
                                                                        <option value="30">30 Minutes</option>
                                                                        <option value="45">45 Minutes</option>
                                                                        <option value="60">60 Minutes</option>
                                                                        <option value="90">90 Minutes</option>
                                                                    </select>
                                                                    <span class="timeoutErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Zip Code <em class="red-star">*</em></label>
                                                                    <input id="zip_code" name="zip_code" class="form-control" type="text"/>
                                                                    <span class="zip_codeErr error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>City</label>
                                                                    <input id="city" name="city" class="form-control" type="text"/>

                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>State</label>
                                                                    <input id="state" name="state" class="form-control" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Country</label>
                                                                    <input id="country" name="country" class="form-control" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address 1<em class="red-star">*</em></label>
                                                                    <input id="address1" name="address1" class="form-control" type="text"/>
                                                                    <span class="address1Err error"></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address 2</label>
                                                                    <input id="address2" name="address2" class="form-control" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address 3</label>
                                                                    <input id="address3" name="address3" class="form-control" type="text"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Phone Number</label>
                                                                    <input id="phone_number" name="phone_number" class="form-control" type="text" data-mask="000-00-0000" data-mask-reverse="true"/>
                                                                </div>

                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                                <!--End single add-form-box-->
                                                <div class="btn-outer apex-btn-block left text-right">
                                                    <button type="submit" class="blue-btn">Update</button>
                                                    <input type='button'  value="Reset" class="clear-btn ResetForm" >
                                                    <button type="button" class="grey-btn cancel_action" >Cancel</button>
                                                </div>

                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="clock-settings">
                                            <form id="clocksettingsForm" method="post">
                                                <input type="hidden" id="c_id" name="c_id" value="0">
                                                <!--single add-form-box-->
                                                <div class="apx-adformbox shadow">
                                                    <div class="apx-adformbox-title">
                                                        <strong class="left">Default Date & Clock Settings</strong>

                                                    </div>
                                                    <div class="apx-adformbox-content">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Default Date Format <em class="red-star">*</em></label>
                                                                    <select name="default_date_format" id="default_date_format" class="form-control">
                                                                        <option value="1">MONTH DAY YEAR </option>
                                                                        <option value="2">DAY MONTH YEAR</option>
                                                                        <option value="3">YEAR MONTH DAY</option>
                                                                        <option value="4">YEAR DAY MONTH</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Date Format</label>
                                                                    <select name="date_format" id="date_format" class="form-control">
                                                                        <option value="1">MM/DD/YYYY (Day)</option>
                                                                        <option value="2">MM-DD-YYYY (Day)</option>
                                                                        <option value="3">MM.DD.YYYY (Day)</option>
                                                                        <option value="13">MONTH DD, YYYY (Day)</option>

                                                                        <option value="4">DD/MM/YYYY (Day)</option>
                                                                        <option value="5">DD-MM-YYYY (Day)</option>
                                                                        <option value="6">DD.MM.YYYY (Day)</option>
                                                                        <option value="14">DD MONTH, YYYY (Day)</option>

                                                                        <option value="7">YYYY/MM/DD (Day)</option>
                                                                        <option value="8">YYYY-MM-DD (Day)</option>
                                                                        <option value="9">YYYY.MM.DD (Day)</option>

                                                                        <option value="10">YYYY/DD/MM (Day)</option>
                                                                        <option value="11">YYYY-DD-MM (Day)</option>
                                                                        <option value="12">YYYY.DD.MM (Day)</option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Default Clock Format <em class="red-star">*</em></label>
                                                                    <select name="default_clock_format" id="default_clock_format" class="dclock_format form-control">
                                                                        <option value="12">12 HOUR CLOCK </option>
                                                                        <option value="24">24 HOUR CLOCK </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>&nbsp;</label>
                                                                    <span class="time-format">12 Hour Clock 08:10:21</span>
                                                                    <!-- <input class="time-format" disabled="" class="form-control" type="text"/> -->
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End single add-form-box-->
                                                <div class="btn-outer apex-btn-block left text-right">
                                                    <button type="submit" class="blue-btn">Update</button>
                                                    <input type='button'  value="Reset" class="clear-btn ResetFormdateClock" >
                                                    <button type="button" class="grey-btn cancel_action">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content End-->
    </main>
</div>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php");
?>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/users/defaultSettings.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.default-sidebar').trigger('click');
        $('.default_settings').addClass('active');

        $('#leftnav2').addClass('aria-expanded',true);
            $('#leftnav2').addClass('in');
            $('#leftnav2').css('height','90px');

    });
</script>
</body>
</html>
<!-- HTML END -->


