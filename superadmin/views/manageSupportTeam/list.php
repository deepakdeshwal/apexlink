<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    include_once ('../../constants.php');
    $url = BASE_URL."login";
    header('Location: '.$url);
};

if(isset($_SESSION[SESSION_DOMAIN]['user_id'])){
    if(isset($_POST['logout'])){
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        $url = BASE_URL."plan";
        header('Location: '.$url);
    }
}else{
    $url = BASE_URL."login";
    header('Location: '.$url);
}
?>
    <?php
    include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php");
    ?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->
        <main class="apxpg-main">
            <section class="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="bread-search-outer apxpg-top-search">
                            <div class="row">
<!--                                <div class="col-md-8 col-sm-8 col-xs-12">-->
<!--                                    <strong class="apxpg-title">Title goes here...</strong>-->
<!--                                </div>-->
                                <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content-data apxpg-allcontent">
                            <!--Tabs Starts -->
                            <!-- Main tabs -->
                            <div class="main-tabs apx-tabs">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="default-settings">
                                        <!--Filters Bar-->
                                        <div class="white-btn-outer">
                                                <div class="col-sm-12">
                                        <a class="blue-btn pull-right" href="/Support/AddSupportMember/0">Add Team Member</a>

                                                </div>

                                        </div>
                                        <!--End Filters Bar-->
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <!--Apex table-->
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="SupportTeam-table" class="table table-bordered nowrap"></table>
                                                                            </div>
                                                                        </div>
                                                                        <!--End Apex Table-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>

    <?php
    include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_footer.php");
    ?>

    <!-- HTML END -->
    <script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/manageSupportTeam/manageSupportTeam.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/manageSupportTeam/manageSupportTeam.js"></script>
    <script>
        $(document).ready(function(){

            //jqGrid status
            $('#jqGridStatus').on('change',function(){
                var selected = this.value;
                console.log(selected);
                $('#Plans-table').jqGrid('GridUnload');
                jqGrid(selected);
            });

            //intializing jqGrid
            jqGrid('All',true);

            /**
             * jqGrid Intialization function
             * @param status
             */
            function jqGrid(status,deleted_at) {
                var columns_data =  ['User Name','Email','Phone','Created Date','Action'];
                var table = 'support_team';
                var columns = ['User Name','Email','Phone','Created Date','Action'];
                var select_column =['EDIT','DELETE'];// ['add','edit','delete','status'];
                var joins = [];
                var conditions = ["eq","bw","ew","cn","in"];
                var extra_columns = ['support_team.deleted_at'];
                var columns_options = [
                    { name:'User Name',index:'first_name', width:80, align:"left", searchoptions: {sopt: conditions},table:table,change_type:"name"},
                    { name:'Email',index:'email', width:80,align:"left", searchoptions: {sopt: conditions},table:table},
                    { name:'Phone',index:'phone_number', width:80,align:"center", searchoptions: {sopt: conditions},table:table},
                    { name:'Created Date',index:'created_at', width:80,align:"left", searchoptions: {sopt: conditions},table:table},
                    { name:'Action',index:'select', title: false, width:80,align:"center",sortable:false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'SELECT', edittype: 'select',search:false}
                ];
                var ignore_array = [];
                jQuery("#SupportTeam-table").jqGrid({
                    url: '/Companies/List/jqgrid',
                    emptyrecords: "No records to view",
                    datatype: "json",
                    height: '100%',
                    autowidth: true,
                    colNames: columns,
                    colModel: columns_options,
                    pager: true,
                    mtype: "POST",
                    postData: {
                        q: 1,
                        class: 'jqGrid',
                        action: "listing_ajax",
                        table: table,
                        select: select_column,
                        columns_options: columns_options,
                        status: status,
                        ignore:ignore_array,
                        joins:joins,
                        extra_columns:extra_columns,
                        deleted_at:deleted_at
                    },
                    viewrecords: true,
                    sortname: 'updated_at',
                    sortorder: "desc",
                    sortIconsBeforeText: true,
                    headertitles: true,
                    rowNum: 10,
                    rowList: [5, 10, 20, 30, 50, 100, 200],
                    caption: "List of Members ",
                    pginput: true,
                    pgbuttons: true,
                    navOptions: {
                        edit: false,
                        add: false,
                        del: false,
                        search: true,
                        filterable: true,
                        refreshtext: "Refresh",
                        reloadGridOptions: {fromServer: true}
                    }
                }).jqGrid("navGrid");
            }



            /**
             * Function for view plan on clicking row
             */
            $(document).on('click','#Plans-table tr td:not(:last-child)',function(){
                var id = $(this).closest('tr').attr('id')
                window.location.href = '/Plans/ViewPlan/'+id;
            })


            $('.support-team-top').addClass('active');

        });
    </script>
<?php
if (isset($_SESSION[SESSION_DOMAIN]["message"])) {
    $message = $_SESSION[SESSION_DOMAIN]["message"];
//    print_r($message); exit;
    ?>
    <script>
        toastr.success("<?php echo $message ?>");
    </script>
    <?php
    unset($_SESSION[SESSION_DOMAIN]["message"]);
}
?>
    </body>
</html>
