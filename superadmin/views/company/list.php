<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <style>
        .activedigit {
            background:#05A0E4 ! important;
          color:#fff ! important;
        }

    </style>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--Tabs Starts -->
                        <!-- Main tabs -->
                        <div class="main-tabs apx-tabs">
                            <!-- Tab panes -->
                            <div class="">
                                <div role="tabpanel" class="tab-pane active" id="default-settings">
                                    <!--Filters Bar-->
                                    <div class=" white-btn-outer">

                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select id="jqGridStatus" class="fm-txt form-control">
                                                <option >All</option>
                                                <option value="1" selected>Active</option>
                                                <option value="0">InActive</option>
                                                <option value="2">Pending Payment</option>
                                                <option value="3">Delete</option>
                                                <option value="4">Not Activated</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-10 text-right">
                                            <button onclick="window.location.href='/Companies/Add'" class="blue-btn">Add New Company / User</button>

                                            <div class="atoz-outer2">
                                                        <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                                                        <span class="AtoZ"></span></span><span class="AZ" id="AZ">A-Z</span>
                                                <span id="allAlphabet" style="cursor:pointer;">All</span>
                                            </div>
                                        </div>

                                    </div>

                                    <!--End Filters Bar-->
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <!--Apex table-->
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive overflow-unset">
                                                                            <table id="companyUser-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!--End Apex Table-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content End-->
    </main>
</div>
<!-- Wrapper Ends -->
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo SUPERADMIN_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>
<div class="container">
    <div class="modal fade" id="financial-infotype" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="financial-infotype-radio">
                        <label class="radio-inline">
                            <input type="radio" name="companytype" checked value="individual"> Individual
                        </label>
                        <input type="hidden" value="0" id="hidden_company_id" >
                        <label class="radio-inline">
                            <input type="radio" name="companytype" value="company" >Company
                        </label>
                    </div>

                    <div class="financial-infotype-btn text-right">
                        <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="financialInfo">

                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fmcc">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3 furl">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="furl" name="furl" required>
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" readonly>
                                        <!--                                        <select class="form-control" id="fbusiness" name="fbusiness" readonly="">-->
                                        <!--                                            <option value="individual" selected>Individual</option>-->
                                        <!--                                        </select>-->
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy number_only" id="faccount_number"  maxlength="12" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="date-outer-form">
                                    <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                    <div class="col-sm-12 col-md-3 fphone_num">
<!--                                        <label> Day <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fday" name="fday" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fdayErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
<!--                                        <label>Month <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fmonth" name="fmonth" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
<!--                                        <label>Year <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fyear" name="fyear" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>
                                        </div>


                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="modal fade" id="financial-company-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="financialCompanyInfo">
                            <input type="hidden" id="company_document_id" name="company_document_id" value="">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl" required>
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fcaddress2" name="fcaddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em> </label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>



                                </div>
                            </div>

                            <div class="row">
                                <div class="form-hdr">
                                    <h3>Person</h3>
                                </div>
                                <div class="form-outer mt-15">
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fpfirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fplast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="fpemail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="date-outer-form">
                                    <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                    <div class="col-sm-12 col-md-3 fphone_num">
<!--                                        <label> Day <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fpday" name="fday" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fdayErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
<!--                                        <label>Month <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fpmonth" name="fmonth" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
<!--                                        <label>Year <em class="red-star">*</em></label>-->
                                        <select class="form-control"  id="fpyear" name="fyear" >
                                            <option value="">Select</option>
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>
                                </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fpaddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fpaddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>City <em class="red-star">*</em></label>
                                    <input type="text" class="form-control" id="fpcity" name="fcity" value="" >
                                    <span class="fcityErr error red-star"></span>
                                </div>
                            </div>
                    </div>
                    <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>
    $(document).ready(function(){
        //jqGrid status
        $('#jqGridStatus').on('change',function(){
            var selected = this.value;
            var deleted_at = true;
            $('#companyUser-table').jqGrid('GridUnload');
            if(selected == 4) deleted_at = false;
            jqGrid(selected,deleted_at);
        });
    });

    //intializing jqGrid
    jqGrid('1',true);

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status,deleted_at) {
        var columns_data =  ['View','Edit','status','Delete','Go To Site','Renew Plan','Upgrade Plan','Resend Welcome Mail','Cancel Subscription'];
        var table = 'users';
        var columns = ['Company ID','Stripe Account Id','Payment Status','Domain Name', 'Name', 'Plan Name','Plan Price($)','No of Hits','Last Login','Email','Company Name','Country','Created Date','Started Date','A/C Type','Status','Action'];
        var select_column =columns_data;// ['add','edit','delete','status'];
        var joins = [{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['users.status'];
        var columns_options = [
            {name:'Company ID',index:'id', width:55,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'AccountId',index:'stripe_account_id',hidden:true, width:90,searchoptions: {sopt: conditions},table:table},
            {name:'PaymentStatus',index:'payment_status',hidden:true, width:90,searchoptions: {sopt: conditions},table:table},
            {name:'Domain Name',index:'domain_name', width:90,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'name', width:100,searchoptions: {sopt: conditions},table:table,change_type:'name'},
            {name:'Plan Name',index:'plan_name', width:80, align:"right",searchoptions: {sopt: conditions},table:'plans'},
            {name:'Plan Price($)',index:'plan_price', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'No of Hits',index:'no_of_hits', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'no_of_hits'},
            {name:'Last Login',index:'last_login', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'last_login'},
            {name:'Email',index:'email', width:80,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Company Name',index:'company_name', width:80,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Country',index:'country', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Created Date',index:'created_at', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Started Date',index:'updated_at', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'A/C Type',index:'account_type', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:accountTypeFmatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', title:false, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter}
        ];
        var ignore_array = [{column:'users.user_type',value:'1'}];
        jQuery("#companyUser-table").jqGrid({
            url: '/Companies/List/jqgrid',
            emptyrecords: "No records to view",
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 10,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Companies/Users",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            },
            loadComplete : function () {
                if(localStorage.getItem("rowcolor_new"))
                {
                    jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    localStorage.removeItem('rowcolor_new');
                }
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:false,resize:false} // search options
        );
    }

    jQuery(document).ready(function () {

        jQuery(document).on('change','.select_options',function () {
            var select_options = $(this).val();
            var data_id = $(this).attr('data_id');

            if(select_options == 'View')
            {
                window.location.href= '/Companies/View?id='+data_id;
            }else if(select_options == 'Edit')
            {
                window.location.href= '/Companies/Edit?id='+data_id;
            }else if(select_options == 'Deactivate')
            {
                bootbox.confirm("Do you want to deactivate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deactivated successfully.');
                                    triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                                $('.select_options').prop('selectedIndex',0);
                            }
                        });
                    }
                    // triggerReload();
                });

            }else if(select_options == 'Go To Site')
            {
                $.ajax({
                    type: 'post',
                    url: '/company-user-ajax',
                    data: {class: 'CompanyUserAjax', action: 'goToSite', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            var url = response.data.url;
                            $('.select_options').prop('selectedIndex',0);
                            window.open(url,'_blank');
                            //    toastr.success('Company activated successfully');
                        } else {
                            toastr.warning('Site not redirecting due to technical issue.');
                        }
                    }
                });
            }else if(select_options == 'Renew Plan')
            {
                // alert('Renew Plan');
                window.location.href= '/Companies/RenewPlan?id='+data_id;
            }
            else if(select_options == 'Make Payment')
            {
                // alert('Make Payment');
                window.location.href= '/Companies/makepayment?id='+data_id;
            }
            else if(select_options == 'Upgrade Plan')
            {
                //alert('Upgrade Plan');
                window.location.href= '/Companies/UpgradePlan?id='+data_id;
            }else if(select_options == 'Resend Welcome Mail')
            {
                $.ajax({
                    type: 'post',
                    url: '/company-user-ajax',
                    data: {class: 'CompanyUserAjax', action: 'sendMail', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            toastr.success('Mail sent successfully.');
                            triggerReload();
                        } else {
                            toastr.warning('Mail does not sent due to technical issue. Please contact to System Administrator');
                        }
                        $('.select_options').prop('selectedIndex',0);
                    }
                });

            }else if(select_options == 'Activate')
            {
                bootbox.confirm("Do you want to activate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company activated successfully.');
                                    triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }

                });
            }else if(select_options == 'Cancel Subscription')
            {
                bootbox.confirm("Do you want to cancel the subscription ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'cancelSubscription', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Subscription cancelled successfully.');
                                    triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }

                });
            }else if(select_options == 'Delete')
            {
                bootbox.confirm("Do you want to delete this record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deleted successfully.');
                                    triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    //   triggerReload();
                });
            }else if(select_options == 'Launch')
            {
                bootbox.confirm("Do you want to launch this record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'launch', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company launch successfully.');
                                    triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    //   triggerReload();
                });
            }else if(select_options == 'Online Payments')
            {

                $('#financial-infotype').modal('toggle');

                $("#hidden_company_id").val(data_id);

            }

            // else if(select_options == 'Launch') {
            //     bootbox.confirm({
            //         message: "Do you want to launch the company ?",
            //         buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            //         callback: function (result) {
            //             if (result == true) {
            //                 toastr.success('Company launch successfully.');
            //             } else {
            //                 toastr.warning('Company not launch due to technical issue.');
            //             }
            //         }
            //     });
            // }
            // $.ajax({
            //     type: 'post',
            //     url: '/company-user-ajax',
            //     data: {class: 'CompanyUserAjax', action: 'Notactivated', id: data_id},
            //     success : function(response){
            //         var response =  JSON.parse(response);
            //         if(response.status == 'success' && response.code == 200) {
            //             var url = response.data.url;
            //             $('.select_options').prop('selectedIndex',0);
            //             window.open(url,'_blank');
            //                toastr.success('Do You Want To Launch the Company');
            //         } else {
            //             toastr.warning('Site not redirecting due to technical issue.');
            //         }
            //     }
            // });
        });
        alphabeticSearch();

        $(document).on("click","#savefinancialtype",function(){
            getAccountDetails($("#hidden_company_id").val());
        });

        $.ajax({
            type: 'post',
            url: '/company-user-ajax',
            data: {
                class: "CompanyUserAjax",
                action: "getMccTypes"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {


                    if (data.data.mcc_types.length > 0) {
                        //var mccOption = "<option value='0'>Select</option>";
                        var mccOption = "";
                        $.each(data.data.mcc_types, function (key, value) {
                            mccOption += "<option value='" + value.code + "' data-id='" + value.code + "'>" + value.name + " - " + value.code + " " + "</option>";
                        });
                        $('#fmcc').html(mccOption);
                        $('#fcmcc').html(mccOption);
                    }

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

        function alphabeticSearch(){
            $.ajax({
                type: 'post',
                url:'/Companies/List/jqgrid',
                data: {class: 'jqGrid',
                    action: "alphabeticSearch",
                    table: 'users',
                    column: 'company_name'},
                success : function(response){
                    var response = JSON.parse(response);
                    if(response.code == 200){
                        var html = '';

                        $.each(response.data, function(key,val) {
                            var color = '#05A0E4'
                            if(val == 0) color = '#c5c5c5';
                            html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                        });
                        $('.AtoZ').html(html);
                    }
                }
            });
        }

        $(document).on('click','#AZ',function(){
            $('.AZ').hide();
            $('#apex-alphafilter').show();
        });

        $(document).on('click','#allAlphabet',function(){
            var grid = $("#companyUser-table");
            $('.AZ').show();
            $('#apex-alphafilter').hide();
            grid[0].p.search = false;
            $.extend(grid[0].p.postData,{filters:""});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            //$('#companyUser-table').trigger( 'reloadGrid' );
        });

        $(document).on('click','.getAlphabet',function(){
            var grid = $("#companyUser-table"),f = [];
            var value = $(this).attr('data_id');
            var search = $(this).text();
            if(value != '0'){
                f.push({field:"company_name",op:"bw",data:search});
                grid[0].p.search = true;
                $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
                grid.trigger("reloadGrid",[{page:1,current:true}]);
            }
        })
    });

    /**
     * Function for view company on clicking row
     */
    $(document).on('click','#companyUser-table tr td:not(:last-child)',function(){
        var id = $(this).closest('tr').attr('id')
        window.location.href = '/Companies/View?id='+id;
    })

    function triggerReload(){
        var grid = $("#companyUser-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == '1')
            return "Active";
        else if(cellvalue == '0')
            return "InActive";
        else if(cellvalue == '2')
            return "Pending Payment";
        else if(cellvalue == '3')
            return "Deleted";
        else if(cellvalue == '4')
            return "Not Activated";
        else
            return '';
    }

    function accountTypeFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Cash";
        else
            return 'Accrual';
    }

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject.Status == 1 && rowObject.AccountId == '')  select = ['View','Edit','Deactivate','Go To Site','Delete','Upgrade Plan','Resend Welcome Mail','Online Payments'];
            if(rowObject.Status == 1 && rowObject.PaymentStatus == 1)  select = ['View','Edit','Deactivate','Go To Site','Delete','Upgrade Plan','Resend Welcome Mail','Online Payments','Cancel Subscription'];
            if(rowObject.Status == 1 && rowObject.AccountId != '')  select = ['View','Edit','Deactivate','Go To Site','Delete','Upgrade Plan','Resend Welcome Mail'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['View','Edit','Activate','Delete'];
            if(rowObject.Status == 2)  select = ['Edit','Make Payment','Delete'];
            // if(rowObject.Status == 2)  select = ['Edit','Launch','Delete'];
            if(rowObject.Status == 3)  select = '';
            if(rowObject.Status == 4)  select = ['Edit','Launch','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    $('.company-top').addClass('active');


    function getAccountDetails(data_id){

        $('#financial-infotype').modal('hide');
        $('.error').html('');
        var radioValue = $("input[name='companytype']:checked").val();

        $("#fmcc,#fcmcc").val(6513);


        if(radioValue == 'individual') {
            // $(".fmcc").hide();
            // $(".furl").hide();
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "getCompanyUserData",
                    id: data_id
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                     console.log(response);
                    // return false;
                    if (response.code == 200) {

                        //   var birth_date = $('#birth_date').val();
                        //  var zipcode = $('#zipcode').val();
                        //   var city = $('#city').val();
                        //   var state = $('#state').val();
                        var email = response.data.data.email;

                        $("#financialInfo [name='fcity']").val(response.data.data.city);
                        $("#financialInfo [name='faddress']").val(response.data.data.address1);
                        $("#financialInfo [name='faddress2']").val(response.data.data.address2);
                        $("#financialInfo [name='fstate']").val(response.data.data.state);
                        $("#financialInfo [name='femail']").val(email);
                        $("#financialInfo [name='fzipcode']").val('10001');
                        $("#financialInfo [name='ffirst_name']").val(response.data.data.first_name);
                        $("#financialInfo [name='flast_name']").val(response.data.data.last_name);
                        $("#financialInfo [name='fphone_number']").val(response.data.data.phone_number);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('.' + key + 'Err').text(value);
                    });
                }
            });
            //  e.preventDefault();

            var phone_prefix = $('input#phone_number_prefix').val();
            //   var address1 = $('#address1').val();
            //   var address2 = $('#address2').val();
            $('.fphone_num input#phone_number_prefix').val(phone_prefix);
            $('.fphone_num li.country').each(function (key, value) {
                if ($(this).attr('data-dial-code') == phone_prefix) {
                    var countrycode = $(this).attr('data-country-code');
                    $("#fphone_number").intlTelInput(
                        "setCountry", countrycode
                    );
                }
            });
            $("#fbusiness").val(radioValue);
            $('#financial-info').modal('toggle');
            $('#financial-info .number_only').removeClass('capital');
        }else{
            $(".fmcc").show();
            $(".furl").show();
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "getCompanyUserData",
                    id: data_id
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                    if (response.code == 200) {
                        var first_name = response.data.data.first_name;
                        var last_name = response.data.data.last_name;
                        var phone_number = response.data.data.phone_number;
                        var email = response.data.data.email;
                        var address1 = response.data.data.address1;
                        var address2 = response.data.data.address2;
                        var zipcode = response.data.data.zipcode;
                        // $('#fpfirst_name').val(first_name);
                        // $('#fplast_name').val(last_name);
                        // $('#fpphone_number').val(phone_number);
                        // $('#fpemail').val(email);
                        $("#fcbusiness").val(radioValue);
                        // $("#fpaddress").val(address1);
                        // $("#fpaddress2").val(address2);
                        // $("#fcaddress").val(address1);
                        // $("#fcaddress2").val(address2);
                        $("#financial-company-info [name='fccity']").val(response.data.data.city);
                        $("#financial-company-info [name='fcity']").val(response.data.data.city);
                        $("#financial-company-info [name='fcaddress']").val(response.data.data.address1);
                        $("#financial-company-info [name='fcaddress2']").val(response.data.data.address2);
                        $("#financial-company-info [name='fcstate']").val(response.data.data.state);
                        $("#financial-company-info [name='fstate']").val(response.data.data.state);
                        $("#financial-company-info [name='femail']").val(email);
                        $("#financial-company-info [name='fczipcode']").val('10001');
                        $("#financial-company-info [name='fzipcode']").val('10001');
                        $("#financial-company-info [name='ffirst_name']").val(response.data.data.first_name);
                        $("#financial-company-info [name='flast_name']").val(response.data.data.last_name);
                        $("#financial-company-info [name='fphone_number']").val(response.data.data.phone_number);
                        $("#financial-company-info [name='fcname']").val(response.data.data.name);
                        $("#financial-company-info [name='fcphone_number']").val(response.data.data.phone_number);
                        $("#financial-company-info [name='faddress']").val(response.data.data.address1);
                        $("#financial-company-info [name='faddress2']").val(response.data.data.address2);


                        $('#financial-company-info').modal('toggle');
                    }else{
                        toastr.error("Unable to fetch details.");
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('.' + key + 'Err').text(value);
                    });
                }
            });



        }

        $('.select_options').prop('selectedIndex',0);
    }

    /**
     *Get location on the basis of zipcode
     */

    function getAddressInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        response(addr);
                    } else {
                        response({success:false});
                    }
                } else {
                    response({success:false});
                }
            });
        } else {
            response({success:false});
        }
    }

    function getAddressInfoByZipforFinance(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        responseFinance(addr);
                    } else {
                        responseFinance({success:false});
                    }
                } else {
                    responseFinance({success:false});
                }
            });
        } else {
            responseFinance({success:false});
        }
    }
    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function responseFinance(obj){
        //console.log(obj);
        if(obj.success){
            $('#fcity').val(obj.city);
            $('#fstate').val(obj.state);
            // $('#fcountry').val(obj.country);
        } else {
            $('#fcity').val('');
            $('#fstate').val('');
            // $('#fcountry').val('');
        }
    }

    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function response(obj){
        //console.log(obj);
        if(obj.success){
            $('#city').val(obj.city);
            $('#state').val(obj.state);
            $('#country').val(obj.country);
        } else {
            $('#city').val('');
            $('#state').val('');
            $('#country').val('');
        }
    }

    $( "#fzipcode" ).focusout(function() {
        getAddressInfoByZipforFinance($(this).val());
    });
    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $( "#fzipcode" ).keydown(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13') {
            getAddressInfoByZip($(this).val());
        }
    });

    $(document).ready(function(){
        $(document).on('click','#savefinancial',function (e) {
            e.preventDefault();
          var SSN =  $('#financial-info #fssn').val();

            var financial_data = $( "#financialInfo" ).serializeArray();
          //  console.log(financial_data);
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "validateFinancialData",
                    data:financial_data
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                    if(response.code == 400)
                    {
                        $.each(response.data, function (key) {
                            $('.' + key).text('* This field is required');
                        });
                    }else{
                        if(SSN.length < 4){
                            toastr.warning('SSN length shoulb be 4 digit');
                            return;
                        }
                        localStorage.setItem('financial_data',JSON.stringify(financial_data));


                        toastr.clear();
                        createUserAccount(financial_data);

                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('.' + key + 'Err').text(value);
                    });
                }
            });




        });


        $(document).on('click','#savecompanyfinancial',function (e) {
            e.preventDefault();
            var financial_data = $( "#financialCompanyInfo" ).serializeArray();
            console.log(financial_data);


            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "validateCompanyFinancialData",
                    data:financial_data
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                    if(response.code == 400)
                    {
                        $.each(response.data, function (key) {
                            $('.' + key).text('* This field is required');
                        });
                    }else{
                        localStorage.setItem('financial_data',JSON.stringify(financial_data));


                        toastr.clear();
                        createCompanyAccount(financial_data);

                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('.' + key + 'Err').text(value);
                    });
                }
            });




        });
    });

    $('#addCompanyForm').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $(document).ready(function(){
        $('input').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#phone_number').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).parents('.intl-tel-input').siblings('.error').html('');
            }
        });
        $('select').on('change',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#fphone_number').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).parents('.intl-tel-input').siblings('.error').html('');
            }
        });
        $('#fbirth_date').on('change',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#faddress').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });

    });


    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    // var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }





    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fpyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var min = 1,
        max =31,
        select = document.getElementById('fpday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    // var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $("#phone_number,#fphone_number,#fssn,#fpphone_number,#fpssn").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });





    function createUserAccount(financial_data){
        $('#loadingmessage').show();
        var company_id = $("#hidden_company_id").val();
        $.ajax({
            type: 'post',
            url: '/company-user-ajax',
            data: {class: 'CompanyUserAjax', action: "createUserConnectedAccount",financial_data:financial_data,"company_id":company_id },
            success : function(response){
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('#loadingmessage').hide();
                    localStorage.removeItem("financial_data");
                    $('#financial-info').modal('toggle');
                    toastr.success("Account Created Successfully.");
                    triggerReload();
                } else if(response.status == 'error' && response.code == 400){
                    if(response.message == 'online payment error')
                    {
                        toastr.error('Please fill all online payment required fields.');
                    }else{
                        console.log()
                    }
                    $('#loadingmessage').hide();
                    $('.error').html('');
                }
                else if(response.status == 'failed' && response.code == 400){
                    toastr.error(response.message);
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            },
            error: function (response) {
                // console.log(response);
            }
        });
    }


    function createCompanyAccount(financial_data){
        $('#loadingmessage').show();
        var company_id = $("#hidden_company_id").val();
        $.ajax({
            type: 'post',
            url: '/company-user-ajax',
            data: {class: 'CompanyUserAjax', action: "createCompanyConnectedAccount",financial_data:financial_data,"company_id":company_id },
            success : function(response){
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('#loadingmessage').hide();
                    localStorage.removeItem("financial_data");
                    $('#financial-company-info').modal('toggle');
                    toastr.success("Account Created Successfully.");
                    triggerReload();
                } else if(response.status == 'error' && response.code == 400){
                    if(response.message == 'online payment error')
                    {
                        toastr.error('Please fill all online payment required fields.');
                    }else{
                        console.log()
                    }
                    $('#loadingmessage').hide();
                    $('.error').html('');
                }
                else if(response.status == 'failed' && response.code == 400){
                    toastr.error(response.message);
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            },
            error: function (response) {
                // console.log(response);
            }
        });
    }



    /* google zip code by  company */
    $( "#fczipcode" ).focusout(function() {
        getAddressInfoByZipforCompanyFinance($(this).val());
    });
    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $( "#fczipcode" ).keydown(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13') {
            getAddressCompanyInfoByZip($(this).val());
        }
    });


    function getAddressInfoByZipforCompanyFinance(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        responseCompanyFinance(addr);
                    } else {
                        responseCompanyFinance({success:false});
                    }
                } else {
                    responseCompanyFinance({success:false});
                }
            });
        } else {
            responseCompanyFinance({success:false});
        }
    }
    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function responseCompanyFinance(obj){
        //console.log(obj);
        if(obj.success){
            $('#fccity').val(obj.city);
            $('#fcstate').val(obj.state);
            // $('#fcountry').val(obj.country);
        } else {
            $('#fccity').val('');
            $('#fcstate').val('');
            // $('#fcountry').val('');
        }
    }

    /**
     *Get location on the basis of zipcode
     */

    function getAddressCompanyInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        responseCompanyFinance(addr);
                    } else {
                        responseCompanyFinance({success:false});
                    }
                } else {
                    responseCompanyFinance({success:false});
                }
            });
        } else {
            responseCompanyFinance({success:false});
        }
    }





    /* google zip code by  person */
    $( "#fpzipcode" ).focusout(function() {
        getAddressInfoByZipforPersonFinance($(this).val());
    });
    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $( "#fpzipcode" ).keydown(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13') {
            getAddressPersonInfoByZip($(this).val());
        }
    });


    function getAddressInfoByZipforPersonFinance(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        responsePersonFinance(addr);
                    } else {
                        responsePersonFinance({success:false});
                    }
                } else {
                    responsePersonFinance({success:false});
                }
            });
        } else {
            responsePersonFinance({success:false});
        }
    }
    /**
     * Set values in the city, state and country when focus loose from zipcode field.
     */
    function responsePersonFinance(obj){
        //console.log(obj);
        if(obj.success){
            $('#fpcity').val(obj.city);
            $('#fpstate').val(obj.state);
            // $('#fcountry').val(obj.country);
        } else {
            $('#fpcity').val('');
            $('#fpstate').val('');
            // $('#fcountry').val('');
        }
    }

    /**
     *Get location on the basis of zipcode
     */

    function getAddressPersonInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                                addr.city =  results[0].address_components[ii].short_name ;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        /* for (name in addr){
                             console.log('### google maps api ### ' + name + ': ' + addr[name] );
                         }*/
                        responsePersonFinance(addr);
                    } else {
                        responsePersonFinance({success:false});
                    }
                } else {
                    responsePersonFinance({success:false});
                }
            });
        } else {
            responsePersonFinance({success:false});
        }
    }
    // $("#myImg").hide();
    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                file = this.files[0];
                formdata = new FormData();
                formdata.append("company_document", file);
                formdata.append("action",'uploadCompanyDocument');
                formdata.append("class",'CompanyUserAjax');
                $.ajax({
                    url: '/company-user-ajax',
                    enctype: 'multipart/form-data',
                    data: formdata,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        // setting a timeout
                        $('#loadingmessage').show();
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        if(response.status == 'success' && response.code == 200){
                            // $('#company_document').val('');
                            $('#loadingmessage').hide();
                            $("#company_document_id").val(response.document_id);
                            toastr.success(response.message);
                        } else {
                            $('#loadingmessage').hide();
                            $('#company_document').val('')
                            toastr.error(response.message);
                        }
                    },
                    error: function (response) {
                        // console.log(response);
                    }
                });
            }
        });
    });

    $('#financial-info,#financial-company-info').modal({backdrop: 'static', keyboard: false,show: false});

    $('#financial-info,#financial-company-info').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })

    /* To check phone format */
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});

    $(document).on('click','.AtoZ .getAlphabet',function(){

        var id = $(this).attr('class');
        if(id =='getAlphabet'){
            $('.getAlphabet').removeClass('activedigit');
            $(this).addClass('activedigit');
        }
       $('#allAlphabet').css('background','#fff');
       $('#allAlphabet').css('color','grey');
    });;

    $(document).on('click','#allAlphabet',function () {
        $('.getAlphabet').removeClass('activedigit');
        $(this).addClass('activedigit');
    })

</script>
</body>

</html>