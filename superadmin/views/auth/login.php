<?php

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = DOMAIN_URL . "/Dashboard/Dashboard";
    header('Location: ' . $url);
} else {
    if($request)
    {
        header('Location: /');
    }
}

include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php");

$email='';
$pass='';
$check='';
if(isset($_COOKIE['remember_login'])) {
    $data = unserialize($_COOKIE['remember_login']);
    $email=$data['email'];
    $pass=$data['password'];
    $check='checked';
}
?>

<div class="apxpg-login">
    <div id="wrapper">
        <div class="ie_wrapper">
            <div class="outdated-browser-warning" id="outdatedBrowserWarning" style="display: none; opacity: 1;">
                <h1 class="outdated-browser-warning__title">Your browser is not supported</h1>
                <p class="outdated-browser-warning__close">
                    <a class="outdated-browser-warning__close-button" id="outdatedBrowserWarningClose" href="#">×</a>
                </p>
                <p class="outdated-browser-warning__contents">

                    Apexlink no longer supports Internet Explorer.
                    To avoid potential issues, we recommend using a supported internet browser.
                    <br>
                </p>
                <a class="btn btn-secondary outdated-browser-warning__update-link" href="#" target="_blank">Find a Supported Browser</a>
            </div>
        </div>
        <div class="login-bg">
            <div class="login-outer">
                <div class="login-logo"><img src="<?php echo SUPERADMIN_SITE_URL;?>/images/logo-login.png"/></div>
                <div class="login-inner">
                    <form name="login" id="login" action="../../user.php" method="post" enctype="multipart/form-data" >
                        <h2>SIGN IN TO YOUR APEXLINK ACCOUNT</h2>
                        <div class="login-data">
                            <label>
                                <input class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email" type="text"/>
                            </label>

                            <label>
                                <input class="form-control email-psw-input" id="password" name="password" value="<?php echo $pass; ?>" placeholder="Password" id="password" type="password"/>
                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" ></span>
                            </label>
                            <div class="remember-psw">
                                <div class="remember-psw-lt"><input type="checkbox" name='remember' id="remember"  <?php echo $check; ?> /> Remember Me</div>
                                <div class="remember-psw-rt"><a href="/MasterData/ForgotPassword">Forgot password?</a></div>
                            </div>
                            <div class="btn-outer">
                                <input type="submit" name="Sign In" value="Sign In" class="blue-btn"  id="login_button"/>
                                <!--<input  type="button" value="Cancel" class="grey-btn" id="cancel"/>-->
                            </div>
                            <br><br>
                            <div class="btn-outer-link">
                                By signing in, you agree to ApexLink’s <a class="blue-text" target="_blank" href="https://www.apexlink.com/terms_use/">terms of use agreement</a>
                                and <a class="blue-text" target="_blank" href="https://www.apexlink.com/terms_privacy.html">privacy statement</a>.
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->
    <?php include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_footer.php"); ?>
    <!-- Footer Ends -->

    <!-- Jquery Starts -->
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/validation/users/users.js"></script>
    <script type="text/javascript">
        $(document).on('click','.toggle-password',function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>

    <?php
    if (isset($_SESSION[SESSION_DOMAIN]["message"])) {
        $message = $_SESSION[SESSION_DOMAIN]["message"];
//    print_r($message); exit;
        ?>
        <script>
            toastr.success("<?php echo $message ?>");
        </script>
        <?php
        unset($_SESSION[SESSION_DOMAIN]["message"]);
    }
    ?>

<script>
    $(document).ready(function () {
        $('a#outdatedBrowserWarningClose').on('click',function () {
            $('#outdatedBrowserWarning').hide();
            setCookie("outdatedBrowser", true, 365);
        })
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }


        if (isIE()){
            var get_cookie = getCookie('outdatedBrowser');
            if(!get_cookie)
            {
                $('#outdatedBrowserWarning').show();
            }
        }
    })
</script>

</div>
</body>

</html>