<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    include_once ('../../constants.php');
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};
//echo $key ;
$token = explode('token=', $_SERVER["REQUEST_URI"]);
$token = end($token);

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = BASE_URL . "Dashboard/Dashboard";
    header('Location: ' . $url);
} else {
    // write your code here
}
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_header.php");

?>
<div class="apxpg-login">
    <div id="wrapper">
        <div class="login-bg">
            <div class="login-outer">
                <div class="login-logo"><img src="<?php echo SUPERADMIN_SITE_URL;?>/images/logo-login.png"/></div>
                <div class="login-inner">
                    <form name="reset_password" id="reset_password" action="../../user.php" method="post" enctype="multipart/form-data" >
                        <h2>RESET PASSWORD</h2>
                        <div class="login-data">
                            <label>
                                <input class="form-control" type="password" name="new_password" placeholder="New Password" id="new_password" />
                            </label>
                            <label>
                                <input class="form-control" type="password" name="confirm_password" placeholder="Confirm Password" id="confirm_passowrd"/>
                            </label>
                            <input class="form-control" type="hidden" value="<?php echo $token ?>" name="forgot_password_token" placeholder="Confirm Password" id="forgot_password_token"/>
                            <div class="btn-outer">
                                <input type="submit" name="reset" value="Reset" class="blue-btn" id="reset"/>
                                <input  type="button" value="Cancel" class="grey-btn" id="reset_cancel"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Wrapper Ends -->
    <?php
    include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
    ?>
    <!-- Footer Ends -->
    <!-- Jquery Starts -->   
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/validation/users/users.js"></script>
</div>
</body>

</html>