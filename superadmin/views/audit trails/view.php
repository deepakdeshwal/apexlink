<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--Tabs Starts -->
                        <!-- Main tabs -->
                        <div class="main-tabs apx-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="default-settings">
                                    <!--Filters Bar-->
                                    <div class="property-status apx-filtersbar">

                                        <div class="col-sm-2">
                                            <label>Audit Trial</label>
                                        </div>


                                    </div>
                                    <!--End Filters Bar-->
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <!--Apex table-->
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="auditTrial-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!--End Apex Table-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content End-->
    </main>
</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>
    $(document).ready(function(){
        //jqGrid status
       /* $('#jqGridStatus').on('change',function(){
            var selected = this.value;
            var deleted_at = true;
            console.log(selected);
            $('#companyUser-table').jqGrid('GridUnload');
            if(selected == 4) deleted_at = false;
            jqGrid(selected,deleted_at);
        });*/
    })
    //intializing jqGrid
    jqGrid('All',true);

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status,deleted_at) {
        var columns_data =  [];
        var columns = ['User Name ', 'IpAddress', 'Action','Action Time','Description'];
        var table = 'audit_trial';
        var select_column = columns_data;
        var joins = [{table:'audit_trial',column:'user_id',primary:'id',on_table:'users'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var columns_options = [
            {name:'User Name',index:'name', width:55,align:"center",searchoptions: {sopt: conditions},table:'users'},
            {name:'IpAddress',index:'ip_address', width:90,searchoptions: {sopt: conditions},table:table},
            {name:'User Action',index:'action', width:90,searchoptions: {sopt: conditions},table:table},
            {name:'Action Time',index:'created_at', width:80, align:"right",searchoptions: {sopt: conditions},default:'yes',table:table},
            {name:'Description',index:'description', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            ];
        var ignore_array = [];
        jQuery("#auditTrial-table").jqGrid({
            url: '/Companies/List/jqgrid',
            emptyrecords: "No records to view",
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'audit_trial.created_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 10,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Audit Trial",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid");
    }

    $('.audit-trial-top').addClass('active');

</script>
</body>

</html>