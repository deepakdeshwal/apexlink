<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--Tabs Starts -->
                        <!-- Main tabs -->
                        <div class="main-tabs apx-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="default-settings">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" ><a href="/Communication/SentEmails" >Email</a></li>
                                        <li role="presentation" ><a href="/Communication/TextMessage" >Text Message</a></li>
                                        <li role="presentation" id="text-message-hide" class="active" ><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                                    </ul>

                                    <!--Filters Bar-->
                                    <div class="tab-content">
                                        <div class="panel-heading">

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="receivables">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="content-section">
                                                                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                                            <!-- Sub Tabs Starts-->
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                    </div>
                                                                                    <div class="col-sm-10">
                                                                                        <div class="btn-outer text-right">
                                                                                            <a href="/Communication/DraftedGroupMessages" class="blue-btn margin-right">Drafts</a>
                                                                                            <a href="/Communication/AddGroupMessage" class="blue-btn margin-right compose-mail">Compose New Group Message</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Regular Rent Ends -->

                                            </div>
                                        </div>
                                        <!-- Sub tabs ends-->
                                    </div>

                                    <div class="white-btn-outer">

                                        <div class="col-sm-12">
                                            <a href="javascript:void(0)" class="blue-btn pull-right groupTxtMsg">Sent Group Text Messages</a>

                                            <a href="javascript:void(0)" class="blue-btn pull-right groupTxtMail">Sent Group Emails</a>
                                        </div>
                                    </div>

                                    <!--End Filters Bar-->
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <!--Apex table-->
                                                                    <div class="apx-table">
                                                                        <div class="table1">
                                                                        <div class="table-responsive">
                                                                            <table id="communication-sent-table-email" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                        <div class="table2" style="display:none;">
                                                                        <div class="table-responsive">
                                                                            <table id="communication-sent-table-text" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--End Apex Table-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content End-->
    </main>
</div>
<div class="modal fade" id="sentMailModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

                <a  href="javascript:void(0)"><h4>Sent Message </h4></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>From: </strong>
                        <span class="modal_from"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>To: </strong>
                        <span class="modal_to"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Subject: </strong>
                        <span class="modal_subject"></span>
                    </div>
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Message</strong>-->
<!--                        <span class="modal_message"></span>-->
<!--                    </div>-->
                    <div class="col-sm-12"></div>
                    <input type="hidden" id="compose_mail_id">
                    <div class="col-sm-12 text-right">
                        <!--                        <a class="blue-btn forward-email-btn new_classs">Forward</a>-->
                        <button class="blue-btn forward-email-btn">Forward</button>
                        <a class="grey-btn" id="cancel_communication" href="javascript:void(0)">
                            Cancel
                        </a>
<!--                        <a class="blue-btn" id="message_recall" href="javascript:void(0)">-->
<!--                            Recall-->
<!--                        </a>-->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>
    // $(document).ready(function(){
    //     //jqGrid status
    //     $('#jqGridStatus').on('change',function(){
    //         var selected = this.value;
    //         var deleted_at = true;
    //         console.log(selected);
    //         $('#companyUser-table').jqGrid('GridUnload');
    //         if(selected == 4) deleted_at = false;
    //         jqGrid(selected,deleted_at);
    //     });
    // })

    //intializing jqGrid
    // jqGrid('All',true);

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status,deleted_at) {
        var columns_data =  ['View','Edit','status','Delete','Go To Site','Renew Plan','Upgrade Plan','Resend Welcome Mail'];
        var table = 'users';
        var columns = ['Company ID','Domain Name', 'Name', 'Plan Name','Plan Price','No of Hits','Last Login','Email','Company Name','Country','Created Date','Started Date','A/C Type','Status','Action'];
        var select_column =columns_data;// ['add','edit','delete','status'];
        var joins = [{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['users.status'];
        var columns_options = [
            {name:'Company ID',index:'id', width:55,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Domain Name',index:'domain_name', width:90,searchoptions: {sopt: conditions},table:table},
            {name:'Name',index:'name', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Plan Name',index:'plan_name', width:80, align:"right",searchoptions: {sopt: conditions},table:'plans'},
            {name:'Plan Price',index:'plan_price', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'No of Hits',index:'sms_carrier', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Last Login',index:'send_owners_package', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Email',index:'email', width:80,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Company Name',index:'company_name', width:80,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Country',index:'country', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Created Date',index:'created_at', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Started Date',index:'updated_at', width:80,align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'A/C Type',index:'account_type', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:accountTypeFmatter},
            {name:'Status',index:'status', width:80,align:"right",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', title:false, editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter}
        ];
        var ignore_array = [{column:'users.id',value:'1'}];
        jQuery("#companyUser-table").jqGrid({
            url: '/Companies/List/jqgrid',
            emptyrecords: "No records to view",
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'id',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 10,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Sent Messages",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid");
    }

    jQuery(document).ready(function () {

        jQuery(document).on('change','.select_options',function () {
            var select_options = $(this).val();
            var data_id = $(this).attr('data_id');

            if(select_options == 'View')
            {
                window.location.href= '/Companies/View?id='+data_id;
            }else if(select_options == 'Edit')
            {
                window.location.href= '/Companies/Edit?id='+data_id;
            }else if(select_options == 'Deactivate')
            {
                bootbox.confirm("Do you want to deactivate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company activated successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });

            }else if(select_options == 'Go To Site')
            {
                alert('Go To Site');
            }else if(select_options == 'Renew Plan')
            {
                alert('Renew Plan');
            }else if(select_options == 'Upgrade Plan')
            {
                alert('Upgrade Plan');
            }else if(select_options == 'Resend Welcome Mail')
            {
                alert('Resend Welcome Mail');
            }else if(select_options == 'Activate')
            {
                bootbox.confirm("Do you want to activate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company activated successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });
            }else if(select_options == 'Delete')
            {
                bootbox.confirm("Do you want to delete this record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deleted successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });
            }
        });
        alphabeticSearch();

        function alphabeticSearch(){
            $.ajax({
                type: 'post',
                url:'/Companies/List/jqgrid',
                data: {class: 'jqGrid',
                    action: "alphabeticSearch",
                    table: 'users',
                    column: 'company_name'},
                success : function(response){
                    var response = JSON.parse(response);
                    if(response.code == 200){
                        var html = '';

                        $.each(response.data, function(key,val) {
                            var color = '#05A0E4'
                            if(val == 0) color = '#c5c5c5';
                            html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                        });
                        $('.AtoZ').html(html);
                    }
                }
            });
        }

        $(document).on('click','#AZ',function(){
            $('.AZ').hide();
            $('#apex-alphafilter').show();
        });

        $(document).on('click','#allAlphabet',function(){
            var grid = $("#companyUser-table");
            $('.AZ').show();
            $('#apex-alphafilter').hide();
            grid[0].p.search = false;
            $.extend(grid[0].p.postData,{filters:""});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            //$('#companyUser-table').trigger( 'reloadGrid' );
        });

        $(document).on('click','.getAlphabet',function(){
            var grid = $("#companyUser-table"),f = [];
            var value = $(this).attr('data_id');
            var search = $(this).text();
            if(value != '0'){
                f.push({field:"company_name",op:"bw",data:search});
                grid[0].p.search = true;
                $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
                grid.trigger("reloadGrid",[{page:1,current:true}]);
            }
        })
    });

    /**
     * Function for view company on clicking row
     */
    // $(document).on('click','#companyUser-table tr td:not(:last-child)',function(){
    //     var id = $(this).closest('tr').attr('id')
    //     window.location.href = '/Companies/View?id='+id;
    // });

    /**
     * Function for view Group Message listing on clicking row (listing model clickable)
     */
    $(document).on('click','#communication-sent-table-email tr td:not(:last-child)',function(){
        var compose_mail_id = $(this).closest("tr").attr('id');

        $.ajax({
            url:'/Communication/GroupMessageAjax',
            method: 'post',
            data: {
                class: "groupMessageAjax",
                action: "getComposeForView",
                'id': compose_mail_id,
            },
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){
                    var res = info.data;

                    if(res.email_to)
                    {
                        $('.modal_to').text(res.email_to);
                    }

                    if(res.email_cc)
                    {
                        $('.modal_cc').text(res.email_cc);
                    }

                    if(res.email_bcc)
                    {
                        $('.modal_bcc').text(res.email_bcc);
                    }
                    $('.modal_from').text(res.email_from);
                    $('.modal_date').text(res.created_at);
                    $('.modal_subject').text(res.email_subject);
                    $('.modal_message').html(res.email_message);
                    $('#compose_mail_id').val(res.id);
                }
            },

        });

        $('#sentMailModal').modal('show');
        //  window.location.href = '/Companies/View?id='+id;
    });

    function triggerReload(){
        var grid = $("#companyUser-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == '0')
            return "Not Activated";
        else if(cellvalue == 2)
            return "Pending Payment";
        else if(cellvalue == 3)
            return "Deleted";
        else
            return '';
    }

    function accountTypeFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Cash";
        else
            return 'Accural';
    }


    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            console.log(rowObject.Status);
            var select = '';
            if(rowObject.Status == 1)  select = ['View','Edit','Deactivate','Go To Site','Delete','Renew Plan','Upgrade Plan','Resend Welcome Mail'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['View','Edit','Activate','Delete'];
            if(rowObject.Status == 2)  select = ['Edit','Launch','Delete'];
            if(rowObject.Status == 3)  select = '';
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    $('.communication-top').addClass('active');

</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/groupMessage/sent-group-message.js"></script>
</body>

</html>
