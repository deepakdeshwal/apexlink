<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php

include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_header.php");

?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication &gt;&gt; <span>Email </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

            </div>
            <div class="col-sm-12">
                <div class="content-section">
<!--                    --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <div class="main-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="/Communication/SentEmails" >Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage" >Text Message</a></li>
                            <li role="presentation"><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                        </ul>
                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                            <!-- Sub Tabs Starts-->
                            <div class="property-status" style="margin-top: 50px;">
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <a class="blue-btn" href="/Communication/DraftedMails">Drafts</a>
                                            <a class="blue-btn" href="/Communication/SentEmails">Sent Mails</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-form">
                                <div class="accordion-outer">
                                    <form id="composeEmail">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">New Email</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body compose-email2">
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <a class="blue-btn compose-email-admin-btn" style="float: left;">
                                                                        Send
                                                                    </a>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <input class="form-control mail_type" name="mail_type" type="hidden" value="draft" />
                                                                        <div class="col-sm-2">
                                                                            <label>To<em class="red-star">*</em></label>
                                                                        </div>
                                                                        <div class="col-sm-10 to_field">
                                                                                <span>
                                                                                    <input class="form-control to" name="to" autofocus type="text"/>
                                                                                    <label style="display:none;" id="to-error" class="error" for="to">* This field is required.</label>
                                                                                </span>
                                                                        </div>

                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-3">-->
<!--                                                                    <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>-->
<!--                                                                </div>-->
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Cc
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                                <span>
                                                                                    <input class="form-control cc" name="cc" type="text"/>
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-3">-->
<!--                                                                    <a class="add-recipient-link addCcRecepent"-->
<!--                                                                       href="#"> Add Recipients</a>-->
<!--                                                                </div>-->
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Bcc </label>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <span>
                                                                                <input class="form-control bcc" name="bcc" type="text"/>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-3">-->
<!--                                                                    <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>-->
<!--                                                                </div>-->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>
                                                                                Subject <em class="red-star">*</em>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                                <span>
                                                                                    <input class="form-control subject" name="subject" type="text"/>
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Body
                                                                                <em class="red-star">*</em></label>
                                                                        </div>
                                                                        <input type="hidden" value="" id="compose_mail_id" name="edit_id">
                                                                        <div class="col-sm-10">
                                                                                <span>
                                                                                    <textarea class="form-control summernote" name="mesgbody"></textarea>
                                                                                </span>
                                                                            <div class="row">
                                                                                <div class="form-outer5">
                                                                                         <div class="col-sm-12 mb-15 text-right">
                                                                                           <label>Attached Files</label>
<!--                                                                                    <form runat="server">-->
                                                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                                           <input id="file_library" type="file" name="file_library[]" multiple style="display: none;">
<!--                                                                                         <img id="blah" src="#" alt="your image" />-->
                                                                                        <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                                                        </div>
                                                                                     <div class="col-sm-12">
                                                                                        <div class="row" id="file_library_uploads">
                                                                                        </div>
                                                                                     </div>
<!--                                                                                    </form>-->
                                                                                </div>
                                                                            </div>
                                                                            <div class="btn-outer text-right">
                                                                                <a class="blue-btn compose-email-admin-save-btn">Save</a>
                                                                                <button type="button" class="clear-btn email_clear">Clear</button>
                                                                                <a class="grey-btn" id="cancel_email" href="javascript:void(0)">
                                                                                    Cancel
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Accordian Ends -->
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>
    </main>
</div>
<!-- Wrapper Ends -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php");
?>
<script>
    var upload_url = "<?php echo SITE_URL; ?>";


    function toTitleCase(str) {
        console.log(str.length);
        if(str.length == 0) {
            $('.capital123 p').text('');
            return str.toUpperCase();
        }
    }
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="https://rawgit.com/bassjobsen/Bootstrap-3-Typeahead/master/bootstrap3-typeahead.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/email/common.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/email/compose-emails.js"></script>

</body>
<style>
    .userDetails {
        float: left;
        width: 100%;
        margin-bottom: 14px;
    }

</style>
