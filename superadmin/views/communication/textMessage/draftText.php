<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>Text Message </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
<!--                        --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="/Communication/SentEmails" >Email</a></li>
                                <li role="presentation" class="active"><a href="/Communication/TextMessage" >Text Message</a></li>
                                <li role="presentation"><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">
                                <div class="panel-heading">

                                    <!-- Nav tabs -->
<!--                                    <ul class="nav nav-tabs" role="tablist">-->
<!--                                    </ul>-->
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="receivables">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="content-section">
                                                            <div class="main-tabs">
                                                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                                    <!-- Sub Tabs Starts-->
                                                                    <div class="property-status">
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-10">
                                                                                <div class="btn-outer text-right">
                                                                                    <a data-urltype="" href="/Communication/AddTextMessage" class="blue-btn margin-right compose-text">Compose Message</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-outer" >
                                                                        <div class="form-hdr">
                                                                            <h3>Drafts</h3>
                                                                        </div>
                                                                        <input type="hidden" value="" id="compose_mail_id" name="edit_id">
                                                                        <div class="form-data">
                                                                            <div class="col-sm-12">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table ">
                                                                                            <div class="table-responsive">
                                                                                                <table id="communication-draft-table2" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>



                            <!-- Sub Tabs Starts-->

                            <!-- Sub tabs ends-->
                        </div>

                    </div>
                </div>

                <!--Tabs Ends -->

            </div>
</div>
</div>
</section>
        <div class="container">
            <div class="modal fade" id="torecepents" role="dialog">
                <div class="modal-dialog modal-sm" style="width: 600px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Add Recipients </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-outer" style="float: none;height:auto;min-height: 300px;">
                                <div class="col-sm-6">
                                    <label>Select <em class="red-star">*</em></label>
                                    <select class="form-control selectUsers">
                                        <option value="">Select</option>
                                        <option value="2">Tenant</option>
                                        <option value="4">Owner</option>
                                        <option value="3">Vendor</option>
                                        <option value="5">Other Contacts</option>
                                        <option value="6">Guest Card</option>
                                        <option value="10">Rental Application</option>
                                        <option value="8">Employee</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label></label>
                                    <br/>
                                    <div class="blue-search">
                                        <input type="text" value="" class="form-control" id="toSearch">
                                        <span class="icon">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="userDetails"><table class="table" border="1px"><tbody><tr>
                                                <th>id</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">
                                                    No Record Found
                                                </td></tr></tbody></table></div>
                                </div>
                                <div class="popup_values" style="display: none;">
                                    <input class="form-control popup_to" type="text"/>
                                </div>
                                <div class="col-sm-12">
                                    <button class="blue-btn" id="SendselectToUsers">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $("#sendEmail .label-info").text('');
        $("#sendEmail #mesgbody").val('');
        $("#sendEmail .subject").val('');
        //  resetFormClear('#sendEmail',[''],'form',true);
    });
</script>
<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/textMessage/draft-message.js"></script>
</body>
</html>
