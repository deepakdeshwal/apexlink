<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication  &gt;&gt; <span> New Group Message/Email </span>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-data">
<!--                --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                <!--Tabs Starts -->
                <div class="main-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                        <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                        <li role="presentation" class="active"><a href="/communication/GroupMessage">Group Message/Email</a></li>
                    </ul>

                    <!-- Tab panes -->

                    <div class="tab-content">
                        <div class="panel-heading">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="receivables">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <a href="/Communication/DraftedGroupMessages"><input type="button" class="blue-btn" value="Drafts"></a>
                                                    <a class="blue-btn"  id="composer_group" href="/Communication/AddGroupMessage">Compose New Group Message</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- Regular Rent Ends -->

                            </div>

                            <div class="accordion-form">
                                <div class="accordion-outer">
                                    <form id="sendEmail">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">New Group Message/Email</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body compose-email2">
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <a class="blue-btn compose-email-btn" style="float: left;">
                                                                        Send
                                                                    </a>
                                                                </div>
                                                                <div class="col-sm-8 compose-text">
                                                                    <div class="row">
                                                                        <input class="form-control mail_type" name="mail_type" type="hidden" value="draft" />
                                                                        <div class="col-sm-2">
                                                                            <label>Company <em class="red-star">*</em></label>
                                                                        </div>
                                                                        <div class="col-sm-10 to_field outer-button-custom">
                                                                                <span >
                                                                                    <select  style="width: 30%; margin-right:10px; float: left" class="form-control selectcompany" name="company_name[]" multiple="multiple">
                                                                                    </select>

                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <input class="form-control mail_type" name="mail_type" type="hidden" value="draft" />
                                                                        <div class="col-sm-2">
                                                                            <label>Recipients <em class="red-star">*</em></label>
                                                                        </div>
                                                                        <div class="col-sm-10 to_field">
                                                                                <span>
                                                                                    <select  style="width: 30%; margin-right:10px; float: left" class="form-control selectRecepient" name="recepient">
                                                                                        <option value="all">Select All</option>
                                                                                    <option value="2">Tenant</option>
                                                                                    <option value="4">Owner</option>
                                                                                    <option value="3">Vendor</option>
                                                                                    <option value="5">Other Contacts</option>
                                                                                    <option value="6">Guest Card</option>
                                                                                    <option value="10">Rental Application</option>
                                                                                    <option value="8">Employee</option>
                                                                                </select>
                                                                                <select  style="width: 30%; float: left; display:none;" class="form-control selectSubRecepient" name="subrecepient[]" multiple  multiple data-live-search="true" data-selected-text-format="count>0">
                                                                                </select>
                                                                                </span>
                                                                        </div>


                                                                    </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-2">
                                                                                    <label>Type<em class="red-star">*</em></label>
                                                                                </div>
                                                                                <div class="col-sm-10">
                                                                                <span>
                                                                               <select  style="width: 30%; float: left;" class="form-control selectType" name="gtype">
                                                                                   <option value="">Select</option>
                                                                                    <option value="1">Email</option>
                                                                                    <option value="2">Text Message</option>
                                                                                </select>
                                                                                </span>
                                                                                </div>
                                                                            </div>

                                                                </div>

                                                                <div class="col-sm-3">
                                                                    <input type="hidden" value="" id="compose_mail_id" name="edit_id">
                                                                </div>
                                                            </div>



                                                            <div class="row email_container" style="display:none;">
                                                                <br/><br/>
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>
                                                                                Subject <em class="red-star">*</em>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <span>
                                                                                <input class="form-control subject" name="subject" type="text"/>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>

                                                            <div class="row email_container" style="display:none;">
                                                                <br/><br/><div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Body
                                                                                <em class="red-star">*</em></label>
                                                                        </div>

                                                                        <div class="col-sm-10">
                                                                            <span>
                                                                                <textarea class="form-control summernote" name="mesgbody"></textarea>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>

                                                            <div class="row text_container" style="display:none;">
                                                                <br/><br/><div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Message
                                                                                <em class="red-star">*</em></label>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <span>
                                                                                <textarea style="height: 150px;" class="form-control capital" name="tmesgbody" id="msgbody"></textarea>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <br/><br/><div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="btn-outer text-right">
                                                                                <a class="blue-btn compose-email-save-btn">Save</a>
                                                                                <button type="button" class="clear-btn email_clear">Clear</button>
                                                                                <a class="grey-btn" id="cancel_email" href="javascript:void(0)">
                                                                                    Cancel
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!-- Accordian Ends -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Sub tabs ends-->
                </div>



                <!-- Sub Tabs Starts-->

                <!-- Sub tabs ends-->
            </div>

        </div>
    </div>

    <!--Tabs Ends -->

    </div>
    </div>
    </div>
</section>
</div>
<!-- Wrapper Ends -->

<!-- for clear all form data-->
<script>
    $(document).on("click",".email_clear",function () {
        // window.location.reload();
        $(".note-editable1 > p").text('');
        $('.selectRecepient').val('all');
        $('.selectType').val('');
        $('#msgbody').val('');
        // $(".selectcompany").multiselect('refresh');
        $(".selectcompany").multiselect("clearSelection");
        $("#sendEmail .note-editable p, #sendEmail .note-editable").text('');
        $(".selectSubRecepient").hide();
        $(".subject").val('');

    });
</script>

<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/groupMessage/compose-group-message.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/communication/email/common.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>

<style>
    span.filter-option.pull-left::before {
        content: ""!important;
    }
    .bootstrap-select .dropdown-toggle .filter-option {
        padding-left: 10px;
    }
    .selectSubRecepient {
        width: 30%!important;
        display: none;
    }
    form#sendEmail ul.dropdown-menu.inner li a:hover {
        border: none;
        background: none;
    }

</style>
</body>

</html>
