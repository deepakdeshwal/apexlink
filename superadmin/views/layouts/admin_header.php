<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ApexLink</title>
    <meta name="author" content="APEXLINK">
    <meta name="description" content="APEXLINK">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo SUPERADMIN_SITE_URL;?>/images/favicon.ico" type="image/ico" sizes="16x16">

    <!-- Bootstrap -->


    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/font-awesome.min.css"/>
    <link href="<?php echo SUPERADMIN_SITE_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.5/css/ui.jqgrid.min.css">


    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">

    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/multiselect.css" />
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/main.css"/>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/summernote.min.js" defer></script>

    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/jquery-3.3.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/bootstrap.min.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- datatable cdn -->
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/bootbox.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/jquery.mask.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.5/jquery.jqgrid.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL;?>/js/calculator.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL;?>/js/common.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL;?>/js/additional-methods.min.js"></script>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/bootstrap-select.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
    <script defer>
        jQuery.extend(jQuery.validator.messages, {
            required: "* This field is required",
        });
        var formated_date = "<?php echo @$_SESSION[SESSION_DOMAIN]['formated_date']; ?>";
        var datepicker_format = "<?php echo @$_SESSION[SESSION_DOMAIN]['datepicker_format']?>";
        //Toast Messages
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

    </script>
</head>
<body>
