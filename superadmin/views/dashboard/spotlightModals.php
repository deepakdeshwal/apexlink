<div class="modal fade" id="UserReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="user_report_filter" id="user_report_filter" >
                    <label class="mrg-btm-modal">Country </label>
                    <select class="is_required form-control multiSelectfield country" name="country"  >
                        <option value="all">Select State</option>
                    </select>
                    <label class="mrg-btm-modal">State </label>
                    <select class="is_required form-control multiSelectfield " name="state"  id="state" >
                        <option value="all">Select State</option>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn " >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="countryfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="UserPlanReportFilter" role="dialog">
<div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Filters</h4>
        </div>
        <div class="modal-body" style="padding:8px;">
            <form name="userplan_report_filter" id="userplan_report_filter" >
                <label class="mrg-btm-modal">Company Name </label>
                <select class="is_required form-control multiSelectfield company" name="company"  >
                </select>
                <label class="mrg-btm-modal">Plan </label>
                <select class="is_required form-control multiSelectfield " name="plans"  id="plans" >
                    <option value="">Select Plan</option>
                </select>
                <br>
                <div class="text-right">
                <button type="submit" class="blue-btn plan_report_filter" >Submit </button>
                <button type="button" class="clear-btn reset_btn" data-id="companyfilterby" >Reset </button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="RevenueReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="revenue_report_filter" id="revenue_report_filter" >
                    <label class="mrg-btm-modal">Country </label>
                    <select class="is_required form-control multiSelectfield country" name="country"  >
                        <option value="all">Select Country</option>
                    </select>
                    <br>
                    <div class="text-right">
                        <button type="submit" class="blue-btn Revenuegenerated_report_filter" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="revenuefilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>