<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
$default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

?>
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/dashboard/spotlightModals.php");

?>
<link rel="stylesheet" type="text/css" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/Chart.css">
<!-- HTML Start -->

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row bread-search-outer">
                <div class="col-xs-8">
                    <!--<div class="dash-setting">
                        <a href="javascript:;" data-toggle="modal" data-target="#myModal">
                            <img class="setting-image" src="/superadmin/images/setting_image.png" alt="" title="">
                        </a>
                    </div>-->

                </div>
                <div class="col-xs-4">
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="dashboard-flex-graph">
                    <div class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Users</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 text-right mob-change">
                                    <button class="blue-btn filterby" data-id="usersfilterby" data-toggle="modal" data-target="#UserReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="usersChart" data-tableid="ActiveUserTable" tableshowID ="ajaxActiveUsersTable">View Report</button>
                                    <button class="blue-btn chartbtn" data-id="usersChart" data-tableid="ActiveUserTable" tableshowID ="ajaxActiveUsersTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="usersChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li><a id="download-pdf2" class="download-pdf2">Download PDF image</a></li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                    <!-- <li><a  id="download1111" class="download1111">Download SVG vector image</a><div id="drawing"></div></li>-->

                                </ul>
                            </div>
                            <canvas id="usersChart" class="chartjs-render-monitor"></canvas>
                            <div class="accordion-grid ajaxActiveUsersTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="ActiveUserTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Active Users</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 text-right mob-change">
                                    <button class="blue-btn filterby" data-id="paidFreefilterby" data-toggle="modal" data-target="#UserReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn"  data-id="paidFreeChart" data-tableid="FreePaidUserTable" tableshowID ="ajaxFreePaidUserTable">View Report</button>
                                    <button class="blue-btn chartbtn"  data-id="paidFreeChart" data-tableid="FreePaidUserTable" tableshowID ="ajaxFreePaidUserTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="paidFreeChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li><a id="download-pdf2" class="download-pdf2">Download PDF image</a></li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                    <!-- <li><a  id="download1111" class="download1111">Download SVG vector image</a><div id="drawing"></div></li>-->

                                </ul>
                            </div>
                            <canvas id="paidFreeChart" class="chartjs-render-monitor"></canvas>
                            <div class="accordion-grid ajaxFreePaidUserTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="FreePaidUserTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Plan</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 text-right mob-change">
                                    <button class="blue-btn filterby" data-id="useplanfilterby" data-toggle="modal" data-target="#UserPlanReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn"  data-id="planUsersDataCharts" data-tableid="PlanTable" tableshowID ="ajaxPlanTable">View Report</button>
                                    <button class="blue-btn chartbtn"  data-id="planUsersDataCharts" data-tableid="PlanTable" tableshowID ="ajaxPlanTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="planUsersDataCharts">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li><a id="download-pdf2" class="download-pdf2">Download PDF image</a></li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                    <!-- <li><a  id="download1111" class="download1111">Download SVG vector image</a><div id="drawing"></div></li>-->
                                </ul>
                            </div>
                            <canvas id="planUsersDataCharts" class="chartjs-render-monitor"></canvas>
                            <div class="accordion-grid ajaxPlanTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="PlanTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Revenue Generated</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 text-right mob-change">
                                    <button class="blue-btn filterby" data-id="revenuefilterby" data-toggle="modal" data-target="#RevenueReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn"  data-id="revenueGeneratedChart" data-tableid="RevenueGenerateTable" tableshowID ="ajaxRevenueTable">View Report</button>
                                    <button class="blue-btn chartbtn"  data-id="revenueGeneratedChart" data-tableid="RevenueGenerateTable" tableshowID ="ajaxRevenueTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="revenueGeneratedChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li><a id="download-pdf2" class="download-pdf2">Download PDF image</a></li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                    <!-- <li><a  id="download1111" class="download1111">Download SVG vector image</a><div id="drawing"></div></li>-->
                                </ul>
                            </div>
                            <canvas id="revenueGeneratedChart" class="chartjs-render-monitor"></canvas>
                            <div class="accordion-grid ajaxRevenueTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="RevenueGenerateTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content End-->
</div>
<script  src="<?php echo SUPERADMIN_SITE_URL; ?>/js/spotlight/Chart.js"></script>
<script  src="<?php echo SUPERADMIN_SITE_URL; ?>/js/spotlight/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script  src="<?php echo SUPERADMIN_SITE_URL; ?>/js/spotlight/spotlight.js"></script>
<script> var default_symbol = "<?php echo $default_symbol; ?>";</script>
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php");
?>
<script>

    $('.spotlight-top').addClass('active');
</script>
</body>
</html>
<!-- HTML END -->


