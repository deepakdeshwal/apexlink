<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (isset($_SESSION[SESSION_DOMAIN]['user_id'])) {
    if (isset($_POST['logout'])) {
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        $url = BASE_URL . "login";
        header('Location: ' . $url);
    }
} else {
    $url = BASE_URL . "login";
    header('Location: ' . $url);
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'] ;
}else{
    $url = BASE_URL . "Announcement/Announcements";
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php");
?>
<!-- HTML Start -->

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
<main class="apxpg-main">
    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer apxpg-top-search ">
                    <div class="row">
                        <!--<div class="col-md-8 col-sm-8 col-xs-12">
                            <strong class="apxpg-title">Title goes here...</strong>
                        </div>-->
                        <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="content-data apxpg-allcontent">
                    <!--single add-form-box-->
                    <div class="apx-adformbox">
                        <div class="apx-adformbox-title">
                            <strong class="left"> Edit Announcement</strong>

                        </div>
                        <div class="apx-adformbox-content">
                            <form name="edit_announcement" id="edit_announcement" action="../../function/announcement.php" method="post" enctype="multipart/form-data" >
                                <input type = "hidden" value="<?php echo $id; ?>" id="announcementID" name="announcementID">
                                <div class="form-data">
                                    <div class="checkbox-outer">
                                        <label><input type="checkbox" name="ism" id="ism" class="ism"> &nbsp;&nbsp;</label>
                                        <span>Is System Maintenance Announcement</span>

                                    </div>
                                    <div class="div-full">

                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Announcement Title <em class="red-star">*</em></label>
                                            <input class="title form-control" id="title" onkeyup="javascript:capitalize(this.value);" name="title" type="text"/>
                                            <span class="titleErr error"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Announcement Start Date <em class="red-star">*</em></label>
                                            <input style="background: none;" readonly class="start_date form-control calander" id="start_date" name="start_date" type="text" value="" />
                                            <span class="start_dateErr error"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Announcement Start Time <em class="red-star">*</em></label>
                                            <input style="background: none;" readonly class="start_time form-control" id="start_time" name="start_time" type="text" value="" />
                                            <span class="start_timeErr error"></span>
                                        </div>
                                    </div>
                                    <div class="div-full">
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Announcement End Date <em class="red-star">*</em></label>
                                            <input style="background: none;" readonly class="end_date form-control" id="end_date" name="end_date" type="text" value="" />
                                            <span class="end_dateErr error"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label>Announcement End Time <em class="red-star">*</em></label>
                                            <input style="background: none;" readonly class="end_time form-control" id="end_time" name="end_time" type="text" value="" />
                                            <span class="end_timeErr error"></span>
                                        </div>
                                    </div>
                                    <div class="div-full">
                                        <div class="col-md-6">
                                            <label class="descr-label">Description <em class="red-star">*</em></label>
                                            <textarea rows="5" class="form-control description" id="description" name="description"></textarea>
                                            <span class="descriptionErr error"></span>
                                        </div>
                                    </div>
                                </div>


                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <!--End single add-form-box-->
                    <div class="btn-outer apex-btn-block text-right">
                        <input type="submit" value="Update" class="blue-btn" id="save_announcement"/>
                        <input type='button'  value="Reset" class="clear-btn ResetForm" >
                        <input  type="button" value="Cancel" class="grey-btn" id="announcement_cancel"/>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
</div>

<!-- Footer Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->


<!-- Jquery Starts -->
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/announcement/announcement.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/announcement.js"></script>
<script type="text/javascript">
    $('.announcement-top').addClass('active');
</script>
<!-- Jquery Ends -->

</body>

</html>
