<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    include_once ('../../constants.php');
    $url = BASE_URL."login";
    header('Location: '.$url);
};

if(isset($_SESSION[SESSION_DOMAIN]['user_id'])){
    if(isset($_POST['logout'])){
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        $url = BASE_URL."payment";
        header('Location: '.$url);
    }
}else{
    $url = BASE_URL."login";
    header('Location: '.$url);
}
?>

    <?php
    include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php");
    ?>
    <!-- HTML Start -->

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->
        <main class="apxpg-main">
            <section class="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="bread-search-outer apxpg-top-search">
                            <div class="row">
                                <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                    <strong class="apxpg-title">Title goes here...</strong>
                                </div>-->
                                <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content-data apxpg-allcontent">
                            <!--Tabs Starts -->
                            <!-- Main tabs -->
                            <div class="main-tabs apx-tabs">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="default-settings">
                                        <!--Filters Bar-->
                                        <div class="div-datepicker-from-to col-md-12 col-sm-12 col-xs-12 white-btn-outer">
                                            <div class="form-data-range col-md-2 col-sm-2 col-xs-2">
                                                    <label>From</label>
                                                    <span>
                                                        <input type="text" readonly="readonly" id="hdnFrom" spellcheck="true" class="hdnFromCls dateFilter" value="">
                                                    </span>
                                            </div>
                                                <div class="form-data-range to-range col-md-2 col-sm-2 col-xs-2">
                                                <label>
                                                    To</label>
                                                <span>
                                                    <input type="text" readonly="readonly" id="hdnTo" spellcheck="true" class="hdnToCls dateFilter" value="">
                                                </span>
                                            </div>
                                        </div>
                                        <!--End Filters Bar-->
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <!--Apex table-->
                                                                        <div class="apx-table apx-paymenttable">
                                                                            <div class="table-responsive">
                                                                                <table id="Payment-table" class="table table-bordered nowrap"></table>
                                                                            </div>
                                                                        </div>
                                                                        <!--End Apex Table-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>

    <?php
    include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_footer.php");
    ?>
    <script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/payment/listing.js"></script>
    </body>
</html>
