<?php
error_reporting(E_ALL);
ini_set("display_errors",1);
curlRequest();
function curlRequest() {
    $serverTime = getServerTime();
    $serverTime = trim($serverTime, '"');
    $serverTime = explode(".",$serverTime)[0];
    $token = generateSmartMoveSecurityHeader($serverTime);
    $getRenterObj = getRenterObject();

    $getRenterObj = (object)$getRenterObj;

    //$url = "https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/Renter";
    $url = "https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/Renter";
    try {
        $headr = getHeaders($serverTime,$token, $getRenterObj);

        $resturnRes = get_url( $url, $headr, $getRenterObj);
        echo "<pre>";
        echo "ServerTime<br>";
        print_r($serverTime);
        echo "<br><br><br>Token<br>";
        print_r($token);
        echo "<br><br><br>ResturnRes<br>";
        print_r($resturnRes);
        echo "<br><br><br>Header<br>";
        print_r($headr);
        die();

    } catch (Exception $exception) {
        echo "<pre>"; print_r($exception->getMessage()); die();
    }
}

function getHeaders($serverTime,$token, $getRenterObj){
    $headers = array();
    $headers[] = 'Connection: Keep-Alive';
    $headers[] = 'Content-Length: '.strlen(http_build_query($getRenterObj));
    $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
    $headers[] = 'Accept: application/json, */*; q=0.01';
    $headers[] = 'Accept-Encoding: gzip, deflate';
    $headers[] = 'Accept-Language: en-US,en;q=0.5';
    $headers[] = 'Authorization: smartmovepartner partnerId="212", serverTime="'.$serverTime.'", securityToken="'.$token.'"';
    $headers[] = 'Host: smlegacygateway-integration.mysmartmove.com';
    $headers[] = 'User-Agent: '.$_SERVER['HTTP_USER_AGENT'];

    return $headers;
}

function generateSmartMoveSecurityHeader($serverTime){
    $partnerId = 212;
    $securityKey = "387KmgTwB6eUZx8Uwqllp9aao5nNGtMNBRiEYGZc+wAX383gfieO/L5HiNvEE5Lf7DH448ujhItBuU7BJ8CvqA==";
    $message = "{$partnerId}{$serverTime}";
    $hmac = hash_hmac("sha1", $message, $securityKey , true);
    return base64_encode($hmac);
}

function getServerTime(){
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => 'https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/ServerTime',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_TIMEOUT => 200000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET'
    ));
    return curl_exec($ch);
}

function getRenterObject(){
    $renterObj = [];
    $renterObj['Email'] = "swiftttttaa@yopmail.com";
    $renterObj['FirstName'] = "ammaam";
    $renterObj['MiddleName'] = "";
    $renterObj['LastName'] = "swiftt";
    $renterObj['DateOfBirth'] = "1982-06-12";
    $renterObj['SocialSecurityNumber'] = "985689446";
    $renterObj['EmploymentStatus'] = "Employed";
    $renterObj['StreetAddressLineOne'] = "Addres66";
    $renterObj['StreetAddressLineTwo'] = "House77";
    $renterObj['City'] = "Anchorage";
    $renterObj['State'] = "AK";
    $renterObj['Zip'] = "99501";
    $renterObj['HomePhoneNumber'] = "4234242342";
    $renterObj['OfficePhoneNumber'] = "";
    $renterObj['OfficePhoneExtension'] = "";
    $renterObj['MobilePhoneNumber'] = "";
    $renterObj['Income'] = 2000.00;
    $renterObj['IncomeFrequency'] = "Monthly";
    $renterObj['OtherIncome'] = 200.00;
    $renterObj['OtherIncomeFrequency'] = "Monthly";
    $renterObj['AssetValue'] = 1000.00;
    $renterObj['FcraAgreementAccepted'] = true;
    return $renterObj;
}

function get_url( $url, $headr, $getRenterObj, $javascript_loop = 0, $timeout = 180 ){
    $url = str_replace( "&amp;", "&", urldecode(trim($url)) );

    $cookie = tempnam ("/tmp", "CURLCOOKIE");
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_ENCODING, "" );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 100 );
    curl_setopt( $ch, CURLOPT_HEADER, 1 );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headr );
    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
    curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt($ch, CURLOPT_FAILONERROR, true); // Required for HTTP error codes to be reported via our call to curl_error($ch)
    curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($getRenterObj) );

    $content = curl_exec( $ch );
    $response = curl_getinfo( $ch );
    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
        return array( $content, $response,$error_msg);
    }
    curl_close ( $ch );
    return array( $content, $response);
}