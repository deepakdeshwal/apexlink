<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Starts -->

 <!-- MAIN Navigation Ends -->

        
        <section class="main-content">
                <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                      <div class="welcome-text-inner">
                        <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 </div>
                        <div class="col-xs-3">
                          <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> 
                          <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                        </div>
                      </div>
                    </div>
                    <div class="content-rt">
                        <!--- Right Quick Links ---->


                        <!--- Right Quick Links ---->
                        
                        <div class="bread-search-outer">
                          <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                              Marketing >> <span>Flyer</span>
                            </div>

                          </div>
                          <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                          </div>
                        </div>

                        <div class="content-data">
                
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Flyers</h3></div>
                                <div class="form-data">
                                  <div class="flyer-outer">
                                    <div class="row">
                                      <div class="col-sm-6">
                                        <h4>Flyer Template 1</h4>
                                        <div class="flyer-links">
                                          <a href="javascript:;">Set as Default</a>
                                          <a href="javascript:;">Preview</a>
                                        </div>
                                      </div>
                                      <div class="col-sm-6 text-right"> <img src="./images/flyer-img.png"/></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Flyers</h3>
                                </div>
                                <div class="form-data">
                                  <div class="flyer-outer">
                                    <div class="row">
                                      <div class="col-sm-4">
                                        <label>Upload Logo</label>
                                        <span><input type="file"/></span>
                                      </div>
                                      <div class="col-sm-3">
                                        <label></label>
                                        <button class="blue-btn">Upload</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                                                
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
                    </div>
                    </div>
        </section>
    </div>
        <!-- Wrapper Ends -->



    <!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
     
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });
 
        <!--- Main Nav Responsive --> 
        $("#show").click(function(){
    $("#bs-example-navbar-collapse-2").show();
});
         $("#close").click(function(){
    $("#bs-example-navbar-collapse-2").hide();
});
         <!--- Main Nav Responsive -->
        
        
        $(document).ready(function(){
          $(".slide-toggle").click(function(){
            $(".box").animate({
              width: "toggle"
            });
          });
        });
        
        $(document).ready(function(){
          $(".slide-toggle2").click(function(){
            $(".box2").animate({
              width: "toggle"
            });
          });
        });
        
       
        
    </script>
    
    


    <!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>