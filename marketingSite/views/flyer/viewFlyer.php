<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_top_navigation.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/flyer/font-awesome.min.css" />
<link href="<?php echo COMPANY_SITE_URL;?>/css/flyer/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/flyer/main.css" />
<link rel="stylesheet" href="<?php echo WEBSITE_URL; ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo WEBSITE_URL; ?>/css/owl.theme.default.min.css"/>


<div class="flyerTemplateClass">


</div>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_footer.php");
?>



<div class="container">
    <div class="modal fade" id="mapModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">

                                        <div id="map" style="height: 400px; width: 100%;"></div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="applyonlineForm" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="padding: 0">
                    <div class="form-hdr">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="hdrtxt">Apply Online</h4>

                    </div>
                </div>

                <div class="modal-body">
                    <form id="apply_online_id">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtFirstNameApply">
                                        First Name<span style="color: Red;"> *</span>
                                    </label>
                                    <input name="first_name" id="txtFirstNameApply" type="text" class="form-control validate[required] clsCapitaliseChr clsPreviewInput" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtLastNameApply">
                                        Last Name<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="last_name" id="txtLastNameApply" type="text" class="form-control validate[required] clsCapitaliseChr clsPreviewInput" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Email<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="email" id="txtEmailApplyOnline" type="text" maxlength="50" class="form-control validate[required] validate[custom[email]] clsPreviewInput clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">
                                        Carrier<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="carrier" class="form-control additional_carrier customCarriervalidations"
                                            data_required="true"><option>Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Country Code<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="country" class="form-control additional_country"
                                            data_required="true"><option>Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone_number_id">
                                        Phone Number<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="phone_number" id="phone_number_id" type="text" maxlength="10" class="form-control validate[required] add-input PreviewClsRenterPhone validate[custom[number]] clsPreviewInput clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtSSNSNINDApplyOnline">
                                        <em class="ss-spacing">SS</em>N/SIN/ID<font style="color: Red;"> *</font>
                                    </label>
                                    <input id="txtSSNSNINDApplyOnline" name="ssn_sin_id" type="text" class="form-control validate[required] AddSSNSINIDValPreview clsPreviewInput clsCapitaliseChr" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRentalApplicantGenderApplyOnline">
                                        Gender <font style="color: Red;"> *</font>
                                    </label>

                                    <select id="ddlRentalApplicantGenderApplyOnline" class="form-control validate[required]" name="gender">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Prefer Not To Say">Prefer Not To Say</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6" id="otherGenderDivApplyOnlne" style="display: none">
                                <div class="form-group" >
                                    <label for="txtOtherGenderApplyOnline">
                                        Other Gender <font class="red spnredstarPreview"> *</font>
                                    </label>
                                    <input type="text" id="txtOtherGenderApplyOnline" class="form-control validate[required] clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label id="ddlApplyProperty">
                                        Select Property<font style="color: Red;"> *</font>
                                    </label>
                                    <input id="ddlApplyProperty" name="prop_id" type="text" class="form-control validate[required] AddSSNSINIDValPreview clsPreviewInput clsCapitaliseChr ddlApplyPropertyClassfb" maxlength="50" spellcheck="true" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlApplyBuilding">
                                        Select Building<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="build_id" id="ddlApplyBuilding" class="form-control validate[required]"><option value="">Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label id="ddlApplyUnit">
                                        Select Unit<font style="color: Red;"> *</font>
                                    </label>
                                    <select id="ddlApplyUnit" class="form-control validate[required]" name="unit_id">
                                        <option value="">Select Unit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="move-in-date">
                                        Expected Move-In Date<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="exp_move_in" type="text" id="move-in-date" class="form-control datepick validate[required] clsCapitaliseChr hasDatepicker" readonly="readonly" spellcheck="true">
                                    <input type="hidden" id="propertyIdHidden" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRequstedLeaseTermApplyonline">
                                        Requested Lease Term<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="lease_term" onpaste="return false;" ondrop="return false;" id="ddlRequstedLeaseTermApplyonline" type="text" class="form-control validate[required] validate[custom[number]] clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRequstedLeaseYearMonth">
                                        &nbsp;
                                    </label>
                                    <select class="form-control" name="lease_tenure" id="ddlRequstedLeaseYearMonth" style="margin: 2px 0 15px;">
                                        <option value="1">Months</option>
                                        <option value="2">Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="btn-outer text-center">
                                        <input id="btnApplyOnlineRentalSave" type="submit" value="Submit" spellcheck="true" style="float: none">
                                        <input id="btnApplyCancel" type="button" value="Cancel" spellcheck="true"  style="float: none">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo WEBSITE_URL; ?>/js/flyer/flyer.js" type="text/javascript"></script>
<script src="<?php echo WEBSITE_URL; ?>/js/properties/properties.js" type="text/javascript"></script>
<script defer src="<?php echo WEBSITE_URL; ?>/js/owl.carousel.js"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww&callback=initMap"></script>
<script>
    $(document).ready(function () {
        setTimeout(function(){
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                autoplay:false,
                autoplayTimeout:1500,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 1,
                        nav: true
                    },
                    1020: {
                        items: 2,
                        nav: true,
                        loop: true,
                        margin: 20
                    },
                    1199: {
                        items: 3,
                        nav: true,
                        loop: true,
                        margin: 20
                    }
                }
            })
        }, 1000);

        $(document).on('click','.clickedImage',function(){
            var src = $(this).attr('src');
            $('#activeImageID').attr('src',src);
        });
    })

</script>