<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Apexlink</title>

    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/flyer/font-awesome.min.css" />
    <link href="<?php echo COMPANY_SITE_URL;?>/css/flyer/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/flyer/main.css" />

</head>

<body>
    <header class="flyer-template-hdr">
         <div class="container">
               <div class="row">
                <div class="col-md-4 pull-left">
                  <div class="logo-flyer">
                    <img alt="" src="<?php echo COMPANY_SITE_URL;?>/images/logo-realestate.png"/>
                  </div>
                </div>
                 <div class="col-md-8 pull-right">
                  <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <i class="fa fa-bars"></i>
                        </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a>Home</a></li>
                            <li><a>Our Properties</a></li>
                            <li><a>Campaigns</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                        </div>
                    </div>
                  </nav>
                 </div>
               </div>
         </div>
    </header>
    <section class="flyer-template-content">
           <div class="container">
            <div class="property-list">
                  <div class="property-hdr property-hdr-black">
                    <div class="row">
                      <div class="col-sm-6">Property Name</div>
                      <div class="col-sm-6 text-right">$1000.00</div>
                    </div>
                  </div>
                  <div class="property-list-content">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="property-img">
                          <img src="<?php echo COMPANY_SITE_URL;?>/images/no-img.jpg"/>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="row">
                          <div class="col-sm-12 pr-none grid-design">
                            <div class="property-name">
                              <label>Property Name - </label>
                              <span>Wellington Heights</span>
                            </div>
                            <div class="property-name">
                              <label>Year Built - </label>
                              <span>2015</span>
                            </div>
                            <div class="property-name">
                              <label>Listing Type - </label>
                              <span>Lorem ipsum</span>
                            </div>
                            <div class="property-name">
                              <label>Property Type -</label>
                              <span>Residential</span>
                            </div>
                             <div class="property-name">
                              <label>Posting Title - </label>
                              <span>Amazing Property</span>
                            </div>
                            <div class="property-name">
                              <label>No. of Buildings - </label>
                              <span>3</span>
                            </div>
                             <div class="property-name">
                              <label>Address - </label>
                              <span>Near hm Blvd, Missouri Missouri</span>
                            </div>
                            <div class="property-name">
                              <label>Unit Types - </label>
                              <span>3</span>
                              <label class="clear">Property Style - </label>
                              <span>Flats</span>
                            </div>
                             <div class="property-name">
                              <label>No. of Units - </label>
                              <span>4</span>
                            </div>
                             <div class="property-name">
                              <label>Vacant Units - </label>
                              <span>4</span>
                            </div>
                            <div class="property-name">
                              <label>Pets Allowed - </label>
                              <span>Yes</span>
                            </div>
                             <div class="property-name">
                              <label>Parking - </label>
                              <span>Yes</span>
                            </div>
                          </div>
                                               
                        </div>
                         
                      </div> 
  
                    </div>
                    <div class="multi-color-design">
                    <div class="property-listing multi">
                      <h4>Units</h4>
                      <ul>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Unit 1091 <a href="javascript:;">View</a></li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Unit 1091 <a href="javascript:;">View</a></li>
                      </ul>
                    </div>
                    <div class="property-listing multi">
                      <h4>Description</h4>
                      <p class="lh25">After the successful completion and deliverance of Wellington Heights-1 & Wellington Heights-11, TDI brings two new towers having exclusive 64 units of spacious & luxurious 3&4 BHK apartments. A distinct sense of place amidst the grandeur of mountain and city views all around. Impactful unit finishes create a vision of a life extraordinary. Savor the grandeur of living at the pinnacle of modern city life every day at Wellington Heights Residences.</p>
                    </div>
                   
                    <div class="property-listing multi">
                      <h4>Features &amp; Amenities</h4>
                      <ul>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Air Conditioning</li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Fitness Centre</li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Covered Parking</li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Swimming Pool</li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Balcony</li>
                        <li><img src="<?php echo COMPANY_SITE_URL;?>/images/li-bg.png">Gym</li>
                      </ul>
                    </div>
                    
                    <div class="listing-adress multi">
                      <h4>Listing Agents</h4>
                      <ul>
                        <li>
                          <p><b>John Smith</b></p>
                          <p>Ph No. 888-907-8686</p>
                          <p><a href="javascript:;">jsmith@gmail.com</a></p>
                        </li>
                        <li>
                          <p><b>John Smith</b></p>
                          <p>Ph No. 888-907-8686</p>
                          <p><a href="javascript:;">jsmith@gmail.com</a></p>
                        </li>
                        <li>
                          <p><b>John Smith</b></p>
                          <p>Ph No. 888-907-8686</p>
                          <p><a href="javascript:;">jsmith@gmail.com</a></p>
                        </li>
                      </ul>
                      
                    </div>
                  </div>

                  </div>
           </div>
    </section>
    <footer class="footer-flyer">
          <div class="container">
            &copy; apexLink Inc. 2019.
            All rights reserved
          </div> 
    </footer>
   <div id="property-apply" class="modal fade property-apply-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><img src="images/cross3.png" class="fr" alt="" height="21" width="22" id="btnCloseApplyOnlineRental"></button>
        <h4 class="modal-title">Apply Online</h4>
      </div>
      <div class="modal-body">
        <div class="real-estate-property-form">
              <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>First Name <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control" >
                       </div>
                  </div>
                   <div class="col-sm-6">
                      <div class="form-group">
                         <label>Last Name  <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control">
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Email  <span class="red-astrick"> *</span></label>
                        <input type="email" class="form-control">
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Carrier  <span class="red-astrick"> *</span></label>
                        <select id="ddlCountryCodeApplyOnline" class="form-control" title="* This field is required"><option value="">Select Country Code</option><option value="Abkhazia" ccode="+7 840">Abkhazia ( +7 840 )</option><option value="Afghanistan" ccode="+93">Afghanistan ( +93 )</option><option value="Albania" ccode="+355">Albania ( +355 )</option><option value="Algeria" ccode="+213">Algeria ( +213 )</option><option value="American Samoa" ccode="+1 684">American Samoa ( +1 684 )</option><option value="Andorra" ccode="+376">Andorra ( +376 )</option><option value="Angola" ccode="+244">Angola ( +244 )</option><option value="Anguilla" ccode="+1 264">Anguilla ( +1 264 )</option><option value="Antigua and Barbuda" ccode="+1 268">Antigua and Barbuda ( +1 268 )</option><option value="Argentina" ccode="+54">Argentina ( +54 )</option><option value="Armenia" ccode="+374">Armenia ( +374 )</option><option value="Aruba" ccode="+297">Aruba ( +297 )</option><option value="Ascension" ccode="+247">Ascension ( +247 )</option><option value="Australia" ccode="+61">Australia ( +61 )</option><option value="Austria" ccode="+43">Austria ( +43 )</option><option value="Azerbaijan" ccode="+994">Azerbaijan ( +994 )</option><option value="Bahamas" ccode="+1 242">Bahamas ( +1 242 )</option><option value="Bahrain" ccode="+973">Bahrain ( +973 )</option><option value="Bangladesh" ccode="+880">Bangladesh ( +880 )</option><option value="Barbados" ccode="+1 246">Barbados ( +1 246 )</option><option value="Belarus" ccode="+375">Belarus ( +375 )</option><option value="Belgium" ccode="+32">Belgium ( +32 )</option><option value="Belize" ccode="+501">Belize ( +501 )</option><option value="Benin" ccode="+229">Benin ( +229 )</option><option value="Bermuda" ccode="+1 441">Bermuda ( +1 441 )</option><option value="Bhutan" ccode="+975">Bhutan ( +975 )</option><option value="Bolivia" ccode="+591">Bolivia ( +591 )</option><option value="Bosnia and Herzegovina" ccode="+387">Bosnia and Herzegovina ( +387 )</option><option value="Botswana" ccode="+267">Botswana ( +267 )</option><option value="Brazil" ccode="+55">Brazil ( +55 )</option><option value="British Indian Ocean Territory" ccode="+246">British Indian Ocean Territory ( +246 )</option><option value="British Virgin Islands" ccode="+1 284">British Virgin Islands ( +1 284 )</option><option value="Brunei" ccode="+673">Brunei ( +673 )</option><option value="Bulgaria" ccode="+359">Bulgaria ( +359 )</option><option value="Burkina Faso" ccode="+226">Burkina Faso ( +226 )</option><option value="Burundi" ccode="+257">Burundi ( +257 )</option><option value="Cambodia" ccode="+855">Cambodia ( +855 )</option><option value="Cameroon" ccode="+237">Cameroon ( +237 )</option><option value="Canada" ccode="+ 1">Canada ( + 1 )</option><option value="Cape Verde" ccode="+238">Cape Verde ( +238 )</option><option value="Cayman Islands" ccode="+ 345">Cayman Islands ( + 345 )</option><option value="Central African Republic" ccode="+236">Central African Republic ( +236 )</option><option value="Chad" ccode="+235">Chad ( +235 )</option><option value="Chile" ccode="+56">Chile ( +56 )</option><option value="China" ccode="+86">China ( +86 )</option><option value="Cocos-Keeling Islands" ccode="+891">Cocos-Keeling Islands ( +891 )</option><option value="Colombia" ccode="+57">Colombia ( +57 )</option><option value="Comoros" ccode="+269">Comoros ( +269 )</option><option value="Congo" ccode="+242">Congo ( +242 )</option><option value="Congo, Dem. Rep. of (Zaire)" ccode="+243">Congo, Dem. Rep. of (Zaire) ( +243 )</option><option value="Cook Islands" ccode="+682">Cook Islands ( +682 )</option><option value="Costa Rica" ccode="+506">Costa Rica ( +506 )</option><option value="Croatia" ccode="+385">Croatia ( +385 )</option><option value="Cuba" ccode="+53">Cuba ( +53 )</option><option value="Curacao" ccode="+599">Curacao ( +599 )</option><option value="Cyprus" ccode="+537">Cyprus ( +537 )</option><option value="Czech Republic" ccode="+420">Czech Republic ( +420 )</option><option value="Denmark" ccode="+45">Denmark ( +45 )</option><option value="Djibouti" ccode="+253">Djibouti ( +253 )</option><option value="Dominica" ccode="+1 767">Dominica ( +1 767 )</option><option value="Dominican Republic" ccode="+1 809">Dominican Republic ( +1 809 )</option><option value="Easter Island" ccode="+56">Easter Island ( +56 )</option><option value="Ecuador" ccode="+593">Ecuador ( +593 )</option><option value="Egypt" ccode="+20">Egypt ( +20 )</option><option value="El Salvador" ccode="+503">El Salvador ( +503 )</option><option value="Equatorial Guinea" ccode="+240">Equatorial Guinea ( +240 )</option><option value="Eritrea" ccode="+291">Eritrea ( +291 )</option><option value="Estonia" ccode="+372">Estonia ( +372 )</option><option value="Ethiopia" ccode="+251">Ethiopia ( +251 )</option><option value="Falkland Islands" ccode="+500">Falkland Islands ( +500 )</option><option value="Faroe Islands" ccode="+298">Faroe Islands ( +298 )</option><option value="Fiji" ccode="+679">Fiji ( +679 )</option><option value="Finland" ccode="+358">Finland ( +358 )</option><option value="France" ccode="+33">France ( +33 )</option><option value="French Antilles" ccode="+596">French Antilles ( +596 )</option><option value="French Guiana" ccode="+594">French Guiana ( +594 )</option><option value="French Polynesia" ccode="+689">French Polynesia ( +689 )</option><option value="Gabon" ccode="+241">Gabon ( +241 )</option><option value="Gambia" ccode="+220">Gambia ( +220 )</option><option value="Georgia" ccode="+995">Georgia ( +995 )</option><option value="Germany" ccode="+49">Germany ( +49 )</option><option value="Ghana" ccode="+233">Ghana ( +233 )</option><option value="Gibraltar" ccode="+350">Gibraltar ( +350 )</option><option value="Greece" ccode="+30">Greece ( +30 )</option><option value="Greenland" ccode="+299">Greenland ( +299 )</option><option value="Grenada" ccode="+1 473">Grenada ( +1 473 )</option><option value="Guadeloupe" ccode="+590">Guadeloupe ( +590 )</option><option value="Guam" ccode="+1 671">Guam ( +1 671 )</option><option value="Guatemala" ccode="+502">Guatemala ( +502 )</option><option value="Guinea" ccode="+224">Guinea ( +224 )</option><option value="Guinea-Bissau" ccode="+245">Guinea-Bissau ( +245 )</option><option value="Guyana" ccode="+595">Guyana ( +595 )</option><option value="Haiti" ccode="+509">Haiti ( +509 )</option><option value="Honduras" ccode="+504">Honduras ( +504 )</option><option value="Hong Kong SAR China" ccode="+852">Hong Kong SAR China ( +852 )</option><option value="Hungary" ccode="+36">Hungary ( +36 )</option><option value="Iceland" ccode="+354">Iceland ( +354 )</option><option value="India" ccode="+91">India ( +91 )</option><option value="Indonesia" ccode="+62">Indonesia ( +62 )</option><option value="Iran" ccode="+98">Iran ( +98 )</option><option value="Iraq" ccode="+964">Iraq ( +964 )</option><option value="Ireland" ccode="+353">Ireland ( +353 )</option><option value="Israel" ccode="+972">Israel ( +972 )</option><option value="Italy" ccode="+39">Italy ( +39 )</option><option value="Ivory Coast" ccode="+225">Ivory Coast ( +225 )</option><option value="Jamaica" ccode="+1 876">Jamaica ( +1 876 )</option><option value="Japan" ccode="+81">Japan ( +81 )</option><option value="Jordan" ccode="+962">Jordan ( +962 )</option><option value="Kazakhstan" ccode="+7 7">Kazakhstan ( +7 7 )</option><option value="Kenya" ccode="+254">Kenya ( +254 )</option><option value="Kiribati" ccode="+686">Kiribati ( +686 )</option><option value="Kuwait" ccode="+965">Kuwait ( +965 )</option><option value="Kyrgyzstan" ccode="+996">Kyrgyzstan ( +996 )</option><option value="Laos" ccode="+856">Laos ( +856 )</option><option value="Latvia" ccode="+371">Latvia ( +371 )</option><option value="Lebanon" ccode="+961">Lebanon ( +961 )</option><option value="Lesotho" ccode="+266">Lesotho ( +266 )</option><option value="Liberia" ccode="+231">Liberia ( +231 )</option><option value="Libya" ccode="+218">Libya ( +218 )</option><option value="Liechtenstein" ccode="+423">Liechtenstein ( +423 )</option><option value="Lithuania" ccode="+370">Lithuania ( +370 )</option><option value="Luxembourg" ccode="+352">Luxembourg ( +352 )</option><option value="Macau SAR China" ccode="+853">Macau SAR China ( +853 )</option><option value="Macedonia" ccode="+389">Macedonia ( +389 )</option><option value="Madagascar" ccode="+261">Madagascar ( +261 )</option><option value="Malawi" ccode="+265">Malawi ( +265 )</option><option value="Malaysia" ccode="+60">Malaysia ( +60 )</option><option value="Maldives" ccode="+960">Maldives ( +960 )</option><option value="Mali" ccode="+223">Mali ( +223 )</option><option value="Malta" ccode="+356">Malta ( +356 )</option><option value="Marshall Islands" ccode="+692">Marshall Islands ( +692 )</option><option value="Martinique" ccode="+596">Martinique ( +596 )</option><option value="Mauritania" ccode="+222">Mauritania ( +222 )</option><option value="Mauritius" ccode="+230">Mauritius ( +230 )</option><option value="Mayotte" ccode="+262">Mayotte ( +262 )</option><option value="Mexico" ccode="+52">Mexico ( +52 )</option><option value="Micronesia" ccode="+691">Micronesia ( +691 )</option><option value="Midway Island" ccode="+1 808">Midway Island ( +1 808 )</option><option value="Moldova" ccode="+373">Moldova ( +373 )</option><option value="Monaco" ccode="+377">Monaco ( +377 )</option><option value="Mongolia" ccode="+976">Mongolia ( +976 )</option><option value="Montenegro" ccode="+382">Montenegro ( +382 )</option><option value="Montserrat" ccode="+1664">Montserrat ( +1664 )</option><option value="Morocco" ccode="+212">Morocco ( +212 )</option><option value="Myanmar" ccode="+95">Myanmar ( +95 )</option><option value="Namibia" ccode="+264">Namibia ( +264 )</option><option value="Nauru" ccode="+674">Nauru ( +674 )</option><option value="Nepal" ccode="+977">Nepal ( +977 )</option><option value="Netherlands" ccode="+31">Netherlands ( +31 )</option><option value="Netherlands Antilles" ccode="+599">Netherlands Antilles ( +599 )</option><option value="Nevis" ccode="+1 869">Nevis ( +1 869 )</option><option value="New Caledonia" ccode="+687">New Caledonia ( +687 )</option><option value="New Zealand" ccode="+64">New Zealand ( +64 )</option><option value="Nicaragua" ccode="+505">Nicaragua ( +505 )</option><option value="Niger" ccode="+227">Niger ( +227 )</option><option value="Nigeria" ccode="+234">Nigeria ( +234 )</option><option value="Niue" ccode="+683">Niue ( +683 )</option><option value="Norfolk Island" ccode="+672">Norfolk Island ( +672 )</option><option value="North Korea" ccode="+850">North Korea ( +850 )</option><option value="Northern Mariana Islands" ccode="+1 670">Northern Mariana Islands ( +1 670 )</option><option value="Norway" ccode="+47">Norway ( +47 )</option><option value="Oman" ccode="+968">Oman ( +968 )</option><option value="Pakistan" ccode="+92">Pakistan ( +92 )</option><option value="Palau" ccode="+680">Palau ( +680 )</option><option value="Palestinian Territory" ccode="+970">Palestinian Territory ( +970 )</option><option value="Panama" ccode="+507">Panama ( +507 )</option><option value="Papua New Guinea" ccode="+675">Papua New Guinea ( +675 )</option><option value="Paraguay" ccode="+595">Paraguay ( +595 )</option><option value="Peru" ccode="+51">Peru ( +51 )</option><option value="Philippines" ccode="+63">Philippines ( +63 )</option><option value="Poland" ccode="+48">Poland ( +48 )</option><option value="Portugal" ccode="+351">Portugal ( +351 )</option><option value="Puerto Rico" ccode="+1 787">Puerto Rico ( +1 787 )</option><option value="Qatar" ccode="+974">Qatar ( +974 )</option><option value="Reunion" ccode="+ 262">Reunion ( + 262 )</option><option value="Romania" ccode="+40">Romania ( +40 )</option><option value="Russia" ccode="+7">Russia ( +7 )</option><option value="Rwanda" ccode="+250">Rwanda ( +250 )</option><option value="Samoa" ccode="+685">Samoa ( +685 )</option><option value="San Marino" ccode="+378">San Marino ( +378 )</option><option value="Saudi Arabia" ccode="+966">Saudi Arabia ( +966 )</option><option value="Senegal" ccode="+221">Senegal ( +221 )</option><option value="Serbia" ccode="+381">Serbia ( +381 )</option><option value="Seychelles" ccode="+248">Seychelles ( +248 )</option><option value="Sierra Leone" ccode="+232">Sierra Leone ( +232 )</option><option value="Singapore" ccode="+65">Singapore ( +65 )</option><option value="Slovakia" ccode="+421">Slovakia ( +421 )</option><option value="Slovenia" ccode="+386">Slovenia ( +386 )</option><option value="Solomon Islands" ccode="+677">Solomon Islands ( +677 )</option><option value="South Africa" ccode="+27">South Africa ( +27 )</option><option value="South Georgia and the South Sandwich Islands" ccode="+ 500">South Georgia and the South Sandwich Islands ( + 500 )</option><option value="South Korea" ccode="+82">South Korea ( +82 )</option><option value="Spain" ccode="+34">Spain ( +34 )</option><option value="Sri Lanka" ccode="+94">Sri Lanka ( +94 )</option><option value="Sudan" ccode="+249">Sudan ( +249 )</option><option value="Suriname" ccode="+597">Suriname ( +597 )</option><option value="Swaziland" ccode="+268">Swaziland ( +268 )</option><option value="Sweden" ccode="+46">Sweden ( +46 )</option><option value="Switzerland" ccode="+41">Switzerland ( +41 )</option><option value="Syria" ccode="+963">Syria ( +963 )</option><option value="Taiwan" ccode="+886">Taiwan ( +886 )</option><option value="Tajikistan" ccode="+992">Tajikistan ( +992 )</option><option value="Tanzania" ccode="+255">Tanzania ( +255 )</option><option value="Thailand" ccode="+66">Thailand ( +66 )</option><option value="Timor Leste" ccode="+670">Timor Leste ( +670 )</option><option value="Togo" ccode="+228">Togo ( +228 )</option><option value="Tokelau" ccode="+690">Tokelau ( +690 )</option><option value="Tonga" ccode="+676">Tonga ( +676 )</option><option value="Trinidad and Tobago" ccode="+1 868">Trinidad and Tobago ( +1 868 )</option><option value="Tunisia" ccode="+216">Tunisia ( +216 )</option><option value="Turkey" ccode="+90">Turkey ( +90 )</option><option value="Turkmenistan" ccode="+993">Turkmenistan ( +993 )</option><option value="Turks and Caicos Islands" ccode="+1 649">Turks and Caicos Islands ( +1 649 )</option><option value="Tuvalu" ccode="+688">Tuvalu ( +688 )</option><option value="U.S. Virgin Islands" ccode="+1 340">U.S. Virgin Islands ( +1 340 )</option><option value="Uganda" ccode="+256">Uganda ( +256 )</option><option value="Ukraine" ccode="+380">Ukraine ( +380 )</option><option value="United Arab Emirates" ccode="+971">United Arab Emirates ( +971 )</option><option value="United Kingdom" ccode="+44">United Kingdom ( +44 )</option><option value="United States" ccode="+1">United States ( +1 )</option><option value="Uruguay" ccode="+598">Uruguay ( +598 )</option><option value="Uzbekistan" ccode="+998">Uzbekistan ( +998 )</option><option value="Vanuatu" ccode="+678">Vanuatu ( +678 )</option><option value="Venezuela" ccode="+58">Venezuela ( +58 )</option><option value="Vietnam" ccode="+84">Vietnam ( +84 )</option><option value="Wake Island" ccode="+1 808">Wake Island ( +1 808 )</option><option value="Wallis and Futuna" ccode="+681">Wallis and Futuna ( +681 )</option><option value="Yemen" ccode="+967">Yemen ( +967 )</option><option value="Zambia" ccode="+260">Zambia ( +260 )</option><option value="Zimbabwe" ccode="+263">Zimbabwe ( +263 )</option></select>
                       </div>
                  </div>
                 <div class="col-sm-6">
                      <div class="form-group">
                         <label>Country Code  <span class="red-astrick"> *</span></label>
                        <input type="email" class="form-control">
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Phone Number  <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control">
                       </div>
                  </div>
                 <div class="col-sm-6">
                      <div class="form-group">
                         <label>SSN/SIN/ID  <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control">
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Gender  <span class="red-astrick"> *</span></label>
                        <select id="ddlRentalApplicantGenderApplyOnline" class="form-control" title="* This field is required">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Prefer Not To Say">Prefer Not To Say</option>
                                        <option value="Other">Other</option>
                                    </select>
                       </div>
                  </div>
                <div class="col-sm-6">
                      <div class="form-group">
                         <label>Select Property   <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control">
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Select Building  <span class="red-astrick"> *</span></label>
                        <select id="ddlApplyRentalBuilding" class="form-control" title="* This field is required"><option value="">Select</option><option value="120">101 7th Avenue building</option></select>
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Select Unit   <span class="red-astrick"> *</span></label>
                        <select id="ddlApplyRentalUnit" class="form-control">
                                        <option value="0">Select Unit</option>
                        </select>
                       </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <label>Expected Move-In Date  <span class="red-astrick"> *</span></label>
                        <input type="text" class="form-control">
                       </div>
                  </div>
                   <div class="col-sm-12">
                      <div class="form-group">
                         <label>Requested Lease Term  <span class="red-astrick"> *</span></label>
                         <div class="row">
                              <div class="col-sm-6">
                                <input type="text" class="form-control">
                              </div>
                             <div class="col-sm-6">
                                <select name="ddlRequstedLeaseYearMonth" id="ddlRequstedLeaseYearMonth" class="form-control  country-dropdown">
                                        <option value="1">Months</option>
                                        <option value="2">Year</option>
                                    </select>
                              </div>
                         </div>
                       </div>
                  </div>
                   <div class="col-sm-12">
                      <div class="form-group text-center">
                          <button type="button" class="btn blue-btn">Submit</button>
                          <button type="button" class="btn blue-btn" data-dismiss="modal">Cancel</button>
                       </div>
                  </div>
              </div>
        </div>
      </div>
    </div>

  </div>
</div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
</body>

</html>