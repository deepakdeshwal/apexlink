<?php


include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_top_navigation.php");
?>



    <section class="real-state-main-content">

    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="real-estate-property-form">
                    <div class="section-preference"><h2 class="heading-small">Search Your Preferences</h2></div>
                    <div class="form-data">
                        <form method="post" action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input type="text" class="form-control"  placeholder="Price" id="price">
                                    </div>
                                    <div class="form-group">
                                        <label>Bedrooms</label>
                                        <select id="Bedrooms" class="form-control">
                                            <?php for($i=1; $i<=100; $i++){
                                                echo '<option value="'.$i.'" text="'.$i.'">'.$i.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Amenities</label>

                                        <select name="amenties_id" multiple="multiple" class="form-control 3col active" id="amenties_box1">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Zip/Postal Code</label>
                                        <input type="text" class="form-control"  placeholder="Zip/Postal Code" id="zip_code_search">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Property Type</label>
                                        <select id="property_type_options" class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Bathrooms</label>
                                        <select id="Bathrooms" class="form-control"><option value="1" text="1">1</option><option value="2" text="1½">1½</option><option value="3" text="2">2</option><option value="4" text="2½">2½</option><option value="5" text="3">3</option><option value="6" text="3½">3½</option><option value="7" text="4">4</option><option value="8" text="4½">4½</option><option value="9" text="5">5</option><option value="10" text="5½">5½</option><option value="11" text="6">6</option><option value="12" text="6½">6½</option><option value="13" text="7">7</option><option value="14" text="7½">7½</option><option value="15" text="8">8</option><option value="16" text="8½">8½</option><option value="17" text="9">9</option><option value="18" text="9½">9½</option><option value="19" text="10">10</option><option value="20" text="10½">10½</option><option value="21" text="11">11</option><option value="22" text="11½">11½</option><option value="23" text="12">12</option><option value="24" text="12½">12½</option><option value="25" text="13">13</option><option value="26" text="13½">13½</option><option value="27" text="14">14</option><option value="28" text="14½">14½</option><option value="29" text="15">15</option><option value="30" text="15½">15½</option><option value="31" text="16">16</option><option value="32" text="16½">16½</option><option value="33" text="17">17</option><option value="34" text="17½">17½</option><option value="35" text="18">18</option><option value="36" text="18½">18½</option><option value="37" text="19">19</option><option value="38" text="19½">19½</option><option value="39" text="20">20</option><option value="40" text="20½">20½</option><option value="41" text="21">21</option><option value="42" text="21½">21½</option><option value="43" text="22">22</option><option value="44" text="22½">22½</option><option value="45" text="23">23</option><option value="46" text="23½">23½</option><option value="47" text="24">24</option><option value="48" text="24½">24½</option><option value="49" text="25">25</option><option value="50" text="25½">25½</option><option value="51" text="26">26</option><option value="52" text="26½">26½</option><option value="53" text="27">27</option><option value="54" text="27½">27½</option><option value="55" text="28">28</option><option value="56" text="28½">28½</option><option value="57" text="29">29</option><option value="58" text="29½">29½</option><option value="59" text="30">30</option><option value="60" text="30½">30½</option><option value="61" text="31">31</option><option value="62" text="31½">31½</option><option value="63" text="32">32</option><option value="64" text="32½">32½</option><option value="65" text="33">33</option><option value="66" text="33½">33½</option><option value="67" text="34">34</option><option value="68" text="34½">34½</option><option value="69" text="35">35</option><option value="70" text="35½">35½</option><option value="71" text="36">36</option><option value="72" text="36½">36½</option><option value="73" text="37">37</option><option value="74" text="37½">37½</option><option value="75" text="38">38</option><option value="76" text="38½">38½</option><option value="77" text="39">39</option><option value="78" text="39½">39½</option><option value="79" text="40">40</option><option value="80" text="40½">40½</option><option value="81" text="41">41</option><option value="82" text="41½">41½</option><option value="83" text="42">42</option><option value="84" text="42½">42½</option><option value="85" text="43">43</option><option value="86" text="43½">43½</option><option value="87" text="44">44</option><option value="88" text="44½">44½</option><option value="89" text="45">45</option><option value="90" text="45½">45½</option><option value="91" text="46">46</option><option value="92" text="46½">46½</option><option value="93" text="47">47</option><option value="94" text="47½">47½</option><option value="95" text="48">48</option><option value="96" text="48½">48½</option><option value="97" text="49">49</option><option value="98" text="49½">49½</option><option value="99" text="50">50</option><option value="100" text="50½">50½</option><option value="101" text="51">51</option><option value="102" text="51½">51½</option><option value="103" text="52">52</option><option value="104" text="52½">52½</option><option value="105" text="53">53</option><option value="106" text="53½">53½</option><option value="107" text="54">54</option><option value="108" text="54½">54½</option><option value="109" text="55">55</option><option value="110" text="55½">55½</option><option value="111" text="56">56</option><option value="112" text="56½">56½</option><option value="113" text="57">57</option><option value="114" text="57½">57½</option><option value="115" text="58">58</option><option value="116" text="58½">58½</option><option value="117" text="59">59</option><option value="118" text="59½">59½</option><option value="119" text="60">60</option><option value="120" text="60½">60½</option><option value="121" text="61">61</option><option value="122" text="61½">61½</option><option value="123" text="62">62</option><option value="124" text="62½">62½</option><option value="125" text="63">63</option><option value="126" text="63½">63½</option><option value="127" text="64">64</option><option value="128" text="64½">64½</option><option value="129" text="65">65</option><option value="130" text="65½">65½</option><option value="131" text="66">66</option><option value="132" text="66½">66½</option><option value="133" text="67">67</option><option value="134" text="67½">67½</option><option value="135" text="68">68</option><option value="136" text="68½">68½</option><option value="137" text="69">69</option><option value="138" text="69½">69½</option><option value="139" text="70">70</option><option value="140" text="70½">70½</option><option value="141" text="71">71</option><option value="142" text="71½">71½</option><option value="143" text="72">72</option><option value="144" text="72½">72½</option><option value="145" text="73">73</option><option value="146" text="73½">73½</option><option value="147" text="74">74</option><option value="148" text="74½">74½</option><option value="149" text="75">75</option><option value="150" text="75½">75½</option><option value="151" text="76">76</option><option value="152" text="76½">76½</option><option value="153" text="77">77</option><option value="154" text="77½">77½</option><option value="155" text="78">78</option><option value="156" text="78½">78½</option><option value="157" text="79">79</option><option value="158" text="79½">79½</option><option value="159" text="80">80</option><option value="160" text="80½">80½</option><option value="161" text="81">81</option><option value="162" text="81½">81½</option><option value="163" text="82">82</option><option value="164" text="82½">82½</option><option value="165" text="83">83</option><option value="166" text="83½">83½</option><option value="167" text="84">84</option><option value="168" text="84½">84½</option><option value="169" text="85">85</option><option value="170" text="85½">85½</option><option value="171" text="86">86</option><option value="172" text="86½">86½</option><option value="173" text="87">87</option><option value="174" text="87½">87½</option><option value="175" text="88">88</option><option value="176" text="88½">88½</option><option value="177" text="89">89</option><option value="178" text="89½">89½</option><option value="179" text="90">90</option><option value="180" text="90½">90½</option><option value="181" text="91">91</option><option value="182" text="91½">91½</option><option value="183" text="92">92</option><option value="184" text="92½">92½</option><option value="185" text="93">93</option><option value="186" text="93½">93½</option><option value="187" text="94">94</option><option value="188" text="94½">94½</option><option value="189" text="95">95</option><option value="190" text="95½">95½</option><option value="191" text="96">96</option><option value="192" text="96½">96½</option><option value="193" text="97">97</option><option value="194" text="97½">97½</option><option value="195" text="98">98</option><option value="196" text="98½">98½</option><option value="197" text="99">99</option><option value="198" text="99½">99½</option><option value="199" text="100">100</option><option value="200" text="100½">100½</option><option value="201" text="Other">Other</option></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Square Feet</label>
                                        <input type="text" class="form-control"  placeholder="Square Feet"  id="sq_feet">
                                    </div>
                                </div>
                            </div>
                            <div class="button-group">
                                <button type="button" class="btn blue-btn searchDataBtn">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="real-estate-slider-outer">
                    <a class="prev"></a>
                    <a class="next"></a>
                </div>
                <div class="real-estate-feature-sct">
                    <h2 class="heading-small">Featured Listing</h2>
                    <ul class="real-estate-list-fatr" id="lstFeaturedProperites">



                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_footer.php");
?>
<script type="text/javascript">


</script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.responsivetabs.js"></script>
<script src="<?php echo WEBSITE_URL; ?>/js/homePage/homePage.js" type="text/javascript"></script>
<script src="<?php echo WEBSITE_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>
