

<script>
    /**
     * Change status of Grid
     * @param status
     */
    function changeGridStatus(status){
        $.ajax
        ({
            type: 'post',
            url: '/common-ajax',
            data: {
                class: "CommonAjax",
                action: "changeStatus",
                status:status
            },
            success: function (response) {
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    localStorage.setItem('active_inactive_status',response.data)
                    return false;
                } else {

                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

</script>
<footer class="real-estate-footer">
    <div class="container">
        <div class="copy-right">
            &copy; ApexLink Inc. 2019.
            All rights reserved
        </div>
    </div>
</footer>
<div class="container">
    <div class="modal fade" style=" top: 50%; bottom: auto; transform: translateY(-50%);" id="homebtn_srtct" role="dialog">
        <div class="modal-dialog modal-lg shortkeys-popup">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Use Apexlink Shortcut Keys</h4>
                </div>
                <div class="modal-body">
                    <form id="add_contact_popup" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <h5>Faster way to work</h5>
                            <p>Press the <em>Home</em> key on the keyboard while you are login the Apexlink program to see which letter is used for each button</p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="short-key-outer">
                                        <label>
                                            t
                                        </label>
                                        <span>
                                            Enters Current Date, + Will move the Date Forward
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            y
                                        </label>
                                        <span>
                                            Enters Yesterday's Date, - Will move the Date Backwards
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt C
                                        </label>
                                        <span>
                                            Opens the Calendar Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            ~
                                        </label>
                                        <span>
                                            Easy Search Field
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt P
                                        </label>
                                        <span>
                                            Opens the People Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt B
                                        </label>
                                        <span>
                                            Enters  Opens the Accounting Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 1
                                        </label>
                                        <span>
                                            Opens the Lease Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 2
                                        </label>
                                        <span>
                                            Opens the Tenant Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 3
                                        </label>
                                        <span>
                                            Opens the Property Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 4
                                        </label>
                                        <span>
                                            Open the Communication Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 6
                                        </label>
                                        <span>
                                            Opens the Maintenance Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 7
                                        </label>
                                        <span>
                                            Opens the Marketing Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt 8
                                        </label>
                                        <span>
                                            Log off the Program
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="short-key-outer">
                                        <label>
                                            F9
                                        </label>
                                        <span>
                                            Beginning of the Year (January of the Current Year)
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            F10
                                        </label>
                                        <span >
                                            End of the Current Year (December of the Current Year)
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            W
                                        </label>
                                        <span>
                                            Beginning of the Current Week
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            K
                                        </label>
                                        <span>
                                            End of the Current Week
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt M
                                        </label>
                                        <span>
                                            Beginning of the Current Month
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt N
                                        </label>
                                        <span>
                                            End of the Current Month
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt Z
                                        </label>
                                        <span>
                                            Opens the Spotlight Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt H
                                        </label>
                                        <span >
                                            Opens the Help Center Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt R
                                        </label>
                                        <span>
                                            Re-log in the System
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl Alt O
                                        </label>
                                        <span>
                                            Opens the Admin Module
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            Ctrl X
                                        </label>
                                        <span>
                                            Jump from one Module to the next going left to right
                                        </span>
                                    </div>
                                    <div class="short-key-outer">
                                        <label>
                                            ESC
                                        </label>
                                        <span>
                                            Close Shortcut List
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="apxlink-logo">
                                <img src="<?php echo COMPANY_SITE_URL; ?>/images/logo.png" height="60" width="160">
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer Ends -->
