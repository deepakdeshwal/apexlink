<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_top_navigation.php");
?>
    <section class="real-state-main-content">
           <div class="container">
                  <div class="row">
                        <div class="col-sm-12">
                            <div class="real-estate-feature-sct">
                                <h2 class="heading-small">Contact Us</h2>
                                <div class="propty1-inner">
                                      <div class="row">
                                             <div class="col-sm-8">
                                                 <div id="map" style="height: 400px;width: 100%;"></div>
                                             </div>
                                            <div class="col-sm-4">
                                                   <div class="real-estate-contact-address">
                                                       <h4>Visit us at:</h4>
                                                       <p id="Address" class="fullAddress">, Alexandria OH United States</p>
                                                   </div>
                                                    <div class="real-estate-contact-address">
                                                       <h4>Contact us at:</h4>
                                                       <p id="Address"></p>
                                                       <p id="emailAddress"></p>
                                                       <p id="Phone"></p>
                                                   </div>
                                                    <div class="real-estate-contact-address">
                                                       <h4>Hours of operation</h4>
                                                       <p id="hours_operation"></p>
                                                   </div>
                                            </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                  </div>
                 
           </div>
    </section>
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_footer.php");
    ?>
    <script src="<?php echo WEBSITE_URL; ?>/js/contact/contact.js" type="text/javascript"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww&callback=initMap"></script>