<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_top_navigation.php");
?>
    <section class="real-state-main-content">
           <div class="container">
                  <div class="row">
                        <div class="col-sm-12">
                            <div class="real-estate-feature-sct">
                                <h2 class="heading-small">Search Result <a class="clsMrktSearchResultBack" href="/">Back</a></h2>
                                <div class="grid-outer listinggridDiv">
                                    <div class="apx-table apxtable-bdr-none">
                                        <div class="table-responsive">
                                            <table id="jqGrid-table" class="table table-bordered">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
           </div>
    </section>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_footer.php");
?>


<script src="<?php echo WEBSITE_URL; ?>/js/homePage/searchResult.js" type="text/javascript"></script>
<script src="<?php echo WEBSITE_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>