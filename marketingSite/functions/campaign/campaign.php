<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class campaign extends DBConnection {

    /**
     * forgotPassword constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for company user forgot password
     */
    public function getAllCampaigns() {
        try{
            $query = $this->companyConnection->query("SELECT id,source_name,compaign_name,	start_date,end_date,call_track_number FROM campaign ");
            $data = $query->fetchAll();
            $html='';
            if(!empty($data)) {
                foreach ($data as $d) {
                    $startdate = date('M d, Y', strtotime($d['start_date']));
                    $endtdate = date('M d, Y', strtotime($d['end_date']));
                    $html .= '<h3>' . ucfirst($d['compaign_name']) . '</h3>
                          <div class="compaign-inner-table table-responsive">
                               <table class="table">
                                   <thead>
                                       <tr>
                                           <th>Campaign Name</th>
                                           <th>Start Date</th>
                                           <th>End Date</th>
                                           <th>Call Tracking No.</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                       <tr>
                                           <td>' . $d['compaign_name'] . '</td>
                                           <td>' . $startdate . '</td>
                                           <td>' . $endtdate . '</td>
                                           <td>' . $d['call_track_number'] . '</td>
                                        </tr>
                                   </tbody>
                               </table>
                          </div>';

                }
            }
            return array('status' => 'success', 'code' => 200, 'data' => $html);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    /**
     *  function for super admin user reset password
     */


}

$campaign = new campaign();
