<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class properties extends DBConnection {

    /**
     * forgotPassword constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for company user forgot password
     */
    public function fetchProperty() {
        try{
            $page = $_POST['pagination'];
            $query1 = $this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes'");
            $dataCount = $query1->fetchAll();

            $count = count($dataCount);
            if($page == '0'){
                $offset = 0;
            } else {
                $offset = $page*5-5;
            }
            if($offset > $count) $offset = $count;


            $query = $this->companyConnection->query("SELECT gp.id,gp.	online_application,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes' ORDER BY gp.property_name ASC LIMIT $offset,5");
            $data = $query->fetchAll();

            $property_images_data = '' ;
            foreach ($data as $key => $val) {
                $query = "SELECT pfu.file_extension, pfu.file_location FROM property_file_uploads AS pfu WHERE pfu.property_id ='".$val['id']."' AND pfu.file_type = 1 AND pfu.deleted_at IS NULL";
                $data[$key]['property_images_data'] =  $this->companyConnection->query($query)->fetchALL();

            }

            $pages = count($dataCount)/5;
            if (strpos($pages, '.')) {
                $pages = explode('.', $pages);
                $total_pages = $pages[0] + 1;
            } else {
                $total_pages = $pages;
            }
            return array('status' => 'success', 'code' => 200,'data'=>$data,'total_pages'=>$total_pages);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function searchByStateAjax(){
       try {
           $page = $_POST['pagination'];
           if($_POST['val'] != ''){
               $data = $this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes' AND gp.state LIKE '" . $_POST['val'] . "'");
               $dataCount = $data->fetchAll();
               $count = count($dataCount);
               if ($page == '0') {
                   $offset = 0;
               } else {
                   $offset = $page * 5 - 5;
               }
               if ($offset > $count) $offset = $count;

               $query = $this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes' AND gp.state LIKE '" . $_POST['val'] . "' ORDER BY gp.property_name ASC LIMIT $offset,5");
               $data = $query->fetchAll();
           }else{

               $data = $this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes'");
               $dataCount = $data->fetchAll();
               $count = count($dataCount);
               if ($page == '0') {
                   $offset = 0;
               } else {
                   $offset = $page * 5 - 5;
               }
               if ($offset > $count) $offset = $count;

               $query = $this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.property_price,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4,gp.zipcode,gp.city,gp.state,gp.country FROM general_property AS gp JOIN marketing_posts AS mp ON gp.id = mp.property_id WHERE mp.is_published = 'yes'  ORDER BY gp.property_name ASC LIMIT $offset,5");
               $data = $query->fetchAll();
           }


           foreach ($data as $key => $val) {
               $query = "SELECT pfu.file_extension, pfu.file_location FROM property_file_uploads AS pfu WHERE pfu.property_id ='" . $val['id'] . "' AND pfu.file_type = 1 AND pfu.deleted_at IS NULL";
               $data[$key]['property_images_data'] = $this->companyConnection->query($query)->fetchALL();

           }
           $pages = count($dataCount) / 5;
           if (strpos($pages, '.')) {
               $pages = explode('.', $pages);
               $total_pages = $pages[0] + 1;
           } else {
               $total_pages = $pages;
           }
       }
       catch (PDOException $e) {
           echo $e->getMessage();
       }
        return array('status' => 'success', 'code' => 200,'data'=>$data,'total_pages'=>$total_pages);

    }
    public function fetchAllCarrierAjax() {
        $html = '';
        $sql = "SELECT id,carrier FROM carrier";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html ='<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . '>' . $d['carrier'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }
    public function fetchAllCountryAjax() {
        $html = '';
        $sql = "SELECT id,name,code FROM countries";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html ='<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . '>' . $d['name']." (".$d['code'].")". '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }
    public function getBuildingAjax() {
        $html = '';
        $sql = "SELECT id,building_name FROM building_detail WHERE property_id=".$_POST['property_id'];
        $data = $this->companyConnection->query($sql)->fetchAll();

        $property = "SELECT property_name FROM general_property WHERE id=".$_POST['property_id'];
        $propertydata = $this->companyConnection->query($property)->fetch();
        $html ='<option value="">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . '>' . $d['building_name'] . '</option>';
        }

        return array('data' => $html, 'status' => 'success','propertydata'=>$propertydata);
    }
    public function getUnitAjax() {
        $html = '';
        $sql = "SELECT id,unit_prefix,unit_type_id,unit_no FROM unit_details WHERE building_id=".$_POST['id'];
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html ='<option value="">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . '>' . $d['unit_prefix']." ".$d['unit_no'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }
    public function addApplyOnline() {
        try {
            $data = $_POST['form'];

            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $users=[];
                $rentalApp=[];
                $users['first_name'] = $data['first_name'];
                $users['last_name'] =  $data['last_name'];
                $users['email'] =  $data['email'];
                $users['carrier'] =  $data['carrier'];
                $users['country'] =  $data['country'];
                $users['phone_number'] =  $data['phone_number'];
                $users['ssn_sin_id'] =  $data['ssn_sin_id'];
                $users['gender'] = $data['gender'];
                $users['user_type'] = '10';
                $users['created_at'] = date('Y-m-d H:i:s');
                $users['updated_at'] = date('Y-m-d H:i:s');


                $rentalApp['prop_id'] =  $data['prop_id'];
                $rentalApp['build_id'] =  $data['build_id'];
                $rentalApp['unit_id'] =  $data['unit_id'];
                $rentalApp['lease_term'] =  $data['lease_term'];
                $rentalApp['lease_tenure'] = $data['lease_tenure'];

                $rentalApp['exp_move_in']=date('m-d-y', strtotime($data['exp_move_in']));


                    $sqlData = createSqlColVal($users);
                    $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($users);
                    $id = $this->companyConnection->lastInsertId();

                $rentalApp['user_id']=$id;


                   $sqlData1 = createSqlColVal($rentalApp);
                    $query1 = "INSERT INTO company_rental_applications (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($rentalApp);

//                    $this->sendMail();
                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record Added successfully');



            }

        } catch (PDOException $e) {

            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function sendMail() {
        try {
            $server_name = 'https://' . $_SERVER['HTTP_HOST'];
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=1")->fetch();
            $user_name = userName($user_details['id'], $this->companyConnection);
            $body = getEmailTemplateData($this->companyConnection, 'newVendorWelcome_Key');
            $body = str_replace("{Firstname}", $user_name, $body);
            $body = str_replace("{Username}", $user_details['email'], $body);
            $body = str_replace("{Url}", $server_name, $body);
            $body = str_replace("{website}", $server_name, $body);
            $logo_url = SITE_URL . 'company/images/logo.png';
            $image = '<img alt="Apexlink" src="' . $logo_url . '" width="150" height="50">';
            $body = str_replace("{CompanyLogo}", $image, $body);
            $request['action'] = 'SendMailPhp';
            $request['to[]'] = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal'] = '1';
            curlRequest($request);
            return ['status' => 'success', 'code' => 200, 'data' => $request, 'message' => 'Email send successfully'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  function for super admin user reset password
     */


}

$properties = new properties();
