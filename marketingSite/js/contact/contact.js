$(document).ready(function () {
    getContactDetailAjax();
    initMap();
})
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.380422, lng: -100.658859},
        zoom: 4
    });
}
function getContactDetailAjax() {
    $.ajax({
        type: 'post',
        url: '/contact-Ajax',
        data: {
            class: 'contact',
            action: 'getContactDetail'},
        success: function (response) {
            var res = JSON.parse(response);
            var fullAddress=res.data.address;
            console.log(res);
            $("#Phone").html('<span>Contact No: </span>'+res.data.phone_number);
            $("#emailAddress").html('<span>Email Us:</span> <a title="" href="mailto:" id="Email"></a>'+res.data.email);
            $(".fullAddress").html(fullAddress);
            $("#hours_operation").html(res.data.hours_of_operation);

            var lat =parseFloat(res.data.latitude);
            var long =parseFloat(res.data.longitude);

            if(lat !== null && long !== null) {
                var position = {lat: lat, lng: long};

                var infowindow = new google.maps.InfoWindow({
                    content: 'title',
                    maxWidth: 200
                });
                var marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: 'title'
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }
        },
    });

}
