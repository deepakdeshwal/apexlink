$(document).ready(function () {

    fetchAllPropertytype();
    fetchAllAmenities();
    featuredListing();
})

function setParams(){
    var price=$("#price").val()==''?'all':$("#price").val();
    var property_type_options=$("#property_type_options").val()==''?'all':$("#property_type_options").val();;
    var Bedrooms=$("#Bedrooms").val()==''?'all':$("#Bedrooms").val();
    var Bathrooms=$("#Bathrooms").val()==''?'all':$("#Bathrooms").val();
    var amenties=$("#amenties_box1").val()==''?'':$("#amenties_box1").val();
    var sq_feet=$("#sq_feet").val()==''?'all':$("#sq_feet").val();
    var zip_code_search=$("#zip_code_search").val()==''?'all':$("#zip_code_search").val();

    localStorage.setItem("price",price);
    localStorage.setItem("property_type_options",property_type_options);
    localStorage.setItem("Bedrooms",Bedrooms);
    localStorage.setItem("Bathrooms",Bathrooms);
    localStorage.setItem("amenties",amenties);
    localStorage.setItem("sq_feet",sq_feet);
    localStorage.setItem("zip_code_search",zip_code_search);




    window.location.href="/Home/SearchResult";

}
$(document).on("click",".searchDataBtn",function () {
    setParams();
});
function fetchAllPropertytype() {
    $.ajax({
        type: 'post',
        url: '/HomePage-Ajax',
        data: {
            class: 'homePage',
            action: 'fetchPropertytype'},
        success: function (response) {
            var res = JSON.parse(response);

            $('#property_type_options').html(res.data);
        },
    });

}
function fetchAllAmenities() {
    $.ajax({
        type: 'post',
        url: '/HomePage-Ajax',
        data: {
            class: 'homePage',
            action: 'fetchAllAmenities'},
        success: function (response) {
            var res = JSON.parse(response);
            var element = $("#amenties_box1");
            element.html(res.data).multiselect("destroy").multiselect({
                includeSelectAllOption: true,
                nonSelectedText: 'Select'
            });
        },
    });

}
function featuredListing() {
    $.ajax({
        type: 'post',
        url: '/HomePage-Ajax',
        data: {
            class: 'homePage',
            action: 'featuredListingAjax'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#lstFeaturedProperites').html(res.html);

        },
    });

}