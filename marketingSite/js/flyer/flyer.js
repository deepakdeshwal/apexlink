$(document).ready(function () {
    getTemplateAjax();
    initMap();
        })
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
function getTemplateAjax() {
    var id =  getParameterByName('id');

    $.ajax({
        type: 'post',
        url: '/flyer-Ajax',
        data: {
            class: 'flyer',
            action: 'getTemplate',
            id:id},
        success: function (response) {
            var res = JSON.parse(response);
            $('.flyerTemplateClass').html(res.data);

        },
    });

}
$(document).on("click",".showMapClass",function () {
   $("#mapModal").modal('show');
    mapLatLong();
});
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.380422, lng: -100.658859},
        zoom: 8
    });
}
function mapLatLong(){
    var id =  getParameterByName('id');
    $.ajax({
        type: 'post',
        url: '/flyer-Ajax',
        data: {
            class: 'flyer',
            action: 'mapLatLongAjax',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            // console.log(res);
            var lat =parseFloat(res.data.latitude);
            var long =parseFloat(res.data.longitude);
            var property_name=res.data.property_name;

            if(lat !== null && long !== null) {
                var position = {lat: lat, lng: long};mi

                var infowindow = new google.maps.InfoWindow({
                    content:property_name,
                    maxWidth: 200
                });

                var bounds = new google.maps.LatLngBounds();
                var marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: 'title'
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
                map.fitBounds(bounds);
            }
        },
    });
}

$(document).on("click",".applyOnlineClass",function(){
    $("#applyonlineForm").modal('show');
})
