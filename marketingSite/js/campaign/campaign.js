$(document).ready(function () {
    getAllCampaignsAjax();
})

function getAllCampaignsAjax() {
    $.ajax({
        type: 'post',
        url: '/campaign-Ajax',
        data: {
            class: 'campaign',
            action: 'getAllCampaigns'},
        success: function (response) {
            var res = JSON.parse(response);
           $(".real-estate-compaign-content").html(res.data);
        },
    });

}
