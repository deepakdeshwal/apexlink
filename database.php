<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 4/22/2019
 * Time: 10:00 AM
 */

// include("$_SERVER[DOCUMENT_ROOT]/config.php");
 include_once( "$_SERVER[DOCUMENT_ROOT]/superadmin/helper/ddl.php");
$result = [];
$this_ddl = new DDL();
$insert_default_settings = $this_ddl->insert($this_ddl->conn);

if(!empty($insert_default_settings)){
    $result['code']= 200;
    $result['message']='Default settings table inserted successfully';
    echo json_encode($result);
}else{
    $result['code']= 400;
    $result['message']='Default settings table not inserted successfully';
    echo json_encode($result);
}

